#ifndef CSPBOARD_H
#define CSPBOARD_H

#include <QWidget>
#include <QMainWindow>
#include <idix_gui.h>
#include <e2r_every_report.h>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrlQuery>
#include <server_setting.h>
#include <logsrc/etching_analysor.h>
#include <logsrc/nikon_log_err_main.h>
#include <logsrc/hanile_log_main.h>
#include <temp_humi_widget.h>
#include <logsrc/asml_err_log_main.h>
#include <worksupport/worst_search_excel_main.h>
#include <logsrc/nikon_time_log.h>
#include <operatingratio/operating_ratio_main.h>
#include <bridge_chart/bridge_chart_widget.h>
#include <NIS/nismainwindow.h>
#include <EDU/edumain.h>
#include <Thin_film_manager/thin_film_mainwindows.h>
#include <Thin_film_manager/thin_monitering_tool.h>
#include <Cost_Reduction/cost_reduction_main.h>
#include <logsrc/act_log_analysor.h>
#include <QtWidgets/QApplication>

#include <deshboard/deshboardmain.h>
#include <Cdsemmanager/cdsemmain.h>
#include <logsrc/shincron_log.h>
#include <production_app/production_main.h>
#include <EatchingSPC/escp_main.h>
#include <Bridge_img_check/bridge_img_check_main.h>
#include <SOMCO/somco_main.h>
#include <suggestion_system/suggestion_main.h>
#include <QDesktopWidget>
#include <QSettings>

namespace Ui {
class CSPBoard;
}

class CSPBoard : public QWidget
{
    Q_OBJECT

public:
    explicit CSPBoard(QWidget *parent = 0);
    ~CSPBoard();

private slots:
    void on_infrom_entet_btn_clicked();

    void on_Cost_reduce_btn_clicked();

    void on_online_fdc_statue_btn_clicked();

    void on_SOP_manual_btn_clicked();

    void on_suggestion_system_btn_clicked();

    void on_Dashboard_btn_clicked();

    void on_OI_startup_clicked();

    void on_NIS_btn_clicked();

    void on_production_app_btn_clicked();

    void on_multi_depostion_work_sheet_btn_clicked();

    void yield_pcver_trigger(bool value);
    void yield_webver_trigger(bool value);

    void oi_ver1_trigger(bool value);
    void oi_ver2_trigger(bool value);


private:
    Ui::CSPBoard *ui;
};

#endif // CSPBOARD_H
