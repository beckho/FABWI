#include "nis_user_from.h"
#include "ui_nis_user_from.h"

NIS_user_from::NIS_user_from(QString area, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NIS_user_from)
{
    ui->setupUi(this);
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    server_ip = settings.value("serverip").toString();
    db_port = settings.value("dbport").toString();
    settings.endGroup();
    this->area =area;

    QString now_datetime =QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");

    QString mydb_name = QString("MY_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }
    maintable = new QSqlTableModel(this,my_mesdb);
    maintable->setTable("NIS_user_group");
    if(this->area == "CSP"){
         maintable->setFilter("`area` IN ('CSP') OR `area` IS NULL ");
    }else if(this->area == "WLP"){
        maintable->setFilter("`area` IN ('WLP') ");
    }else if(this->area == "BPROJECT"){
        maintable->setFilter("`area` IN ('BPROJECT') ");
    }

    maintable->select();
    ui->maintableview->setModel(maintable);
    connect(ui->maintableview->horizontalHeader(),SIGNAL(sectionClicked(int)),this,SLOT(header_click(int)));
    register_model = new QStandardItemModel(0,1,this);
    register_model->setHeaderData(0,Qt::Horizontal,"name");

    ui->register_view->setModel(register_model);

    QSqlQuery query(my_mesdb);
    if(this->area == "CSP"){
        query.exec("select nisgroup from NIS_user_group where `area` IN ('CSP') OR `area` IS NULL GROUP BY nisgroup");
    }else if(this->area == "WLP"){
        query.exec("select nisgroup from NIS_user_group where `area` IN ('WLP') GROUP BY nisgroup");
    }else if(this->area == "BPROJECT"){
        query.exec("select nisgroup from NIS_user_group where `area` IN ('BPROJECT') GROUP BY nisgroup");
    }

    while(query.next()){
        ui->groupbox->addItem(query.value("nisgroup").toString());
    }


}

QStringList NIS_user_from::getNamelist() const
{
    return namelist;
}

void NIS_user_from::setNamelist(const QStringList &value)
{
    namelist = value;
}

NIS_user_from::~NIS_user_from()
{
    delete ui;
}

void NIS_user_from::header_click(int index)
{

    maintable->setSort(index,Qt::AscendingOrder);
    maintable->select();

}

void NIS_user_from::on_move_add_btn_clicked()
{
//    qDebug()<<ui->maintableview->selectionModel()->selectedIndexes().count();
    foreach(QModelIndex index,ui->maintableview->selectionModel()->selectedIndexes()){

       QString name  = maintable->index(index.row(),1).data().toString();
       QStandardItem *item = new QStandardItem(name);
       register_model->appendRow(item);
    }

}

void NIS_user_from::on_groupbox_currentIndexChanged(const QString &arg1)
{
    if(arg1 != ""){
        QSqlQuery query(my_mesdb);
        if(this->area == "CSP"){
            query.exec(QString("select * from NIS_user_group where nisgroup = '%1' AND (`area` IN ('CSP') OR `area` is NULL) ").arg(arg1));
        }else if(this->area == "WLP"){
            query.exec(QString("select * from NIS_user_group where nisgroup = '%1' AND `area` IN ('WLP')").arg(arg1));
        }else if(this->area == "BPROJECT"){
            query.exec(QString("select * from NIS_user_group where nisgroup = '%1' AND `area` IN ('WLP')").arg(arg1));
        }

        while(query.next()){
            QString name  = query.value("name").toString();
            QStandardItem *item = new QStandardItem(name);
            register_model->appendRow(item);
        }
    }
}

void NIS_user_from::on_remvoe_btn_clicked()
{
    foreach(QModelIndex index,ui->register_view->selectionModel()->selectedIndexes()){
        register_model->removeRow(index.row());
    }
}

void NIS_user_from::on_add_id_btn_clicked()
{
    NIS_user_add *dialog = new NIS_user_add(area);
    if(dialog->exec()==QDialog::Accepted){
        maintable->select();
    }

}

void NIS_user_from::on_del_id_btn_clicked()
{
    foreach(QModelIndex index,ui->maintableview->selectionModel()->selectedIndexes()){

       maintable->removeRow(index.row());
    }
}

void NIS_user_from::on_dialog_btn_accepted()
{

    for(int i= 0;i<register_model->rowCount();i++){
       namelist.append(register_model->index(i,0).data().toString());
    }


}
