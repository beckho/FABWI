#ifndef NISMAINWINDOW_H
#define NISMAINWINDOW_H

#include <QWidget>
#include <NIS/niswrite_widget.h>
#include <QSqlQueryModel>
#include <QProcess>

namespace Ui {
class NISmainwindow;
}

class NISmainwindow : public QWidget
{
    Q_OBJECT

public:
    explicit NISmainwindow(QString area,QWidget *parent = 0);
    QSqlQueryModel *conent_model;
    QSqlDatabase my_mesdb;
    QString area;
    QString server_ip;
    QString db_port;
    QString ftp_port;
    ~NISmainwindow();

private slots:
    void on_write_btn_clicked();

    void on_content_list_view_doubleClicked(const QModelIndex &index);

    void on_search_btn_clicked();

    void write_close_event();

    void on_name_search_btn_clicked();

    void on_select_team_currentIndexChanged(const QString &arg1);

private:
    Ui::NISmainwindow *ui;
};

#endif // NISMAINWINDOW_H
