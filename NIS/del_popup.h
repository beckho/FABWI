#ifndef DEL_POPUP_H
#define DEL_POPUP_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QSettings>
#include <QDateTime>
#include <global_define.h>
namespace Ui {
class del_popup;
}

class del_popup : public QDialog
{
    Q_OBJECT

public:
    explicit del_popup(QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    bool result;
    ~del_popup();

    bool getResult() const;
    void setResult(bool value);

private slots:
    void on_buttonBox_accepted();

private:
    Ui::del_popup *ui;
};

#endif // DEL_POPUP_H
