#include "nismainwindow.h"
#include "ui_nismainwindow.h"

NISmainwindow::NISmainwindow(QString area, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NISmainwindow)
{
    ui->setupUi(this);
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    server_ip = settings.value("serverip").toString();
    db_port = settings.value("dbport").toString();
    ftp_port = settings.value("ftpport").toString();
    settings.endGroup();
    this->area = area;
    QProcess process;
    process.setWorkingDirectory(qApp->applicationDirPath());
    process.start("RegAsm.exe NS_Core_Com.dll");
    process.waitForFinished();
    QString output(process.readAllStandardOutput());
    qDebug()<<output;

    QString mydb_name = QString("MY_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }
    QDateTime start_search_time;
    start_search_time.setDate(QDate::currentDate().addDays(-2));
    start_search_time.setTime(QTime(8,0,0));
    ui->start_search_time->setDateTime(start_search_time);
    ui->end_search_time->setDateTime(QDateTime::currentDateTime());

    conent_model = new QSqlQueryModel(this);
    if(this->area == "CSP"){
        conent_model->setQuery(QString("select idx,team,process,"
                                       "write_time,document_name,"
                                       "writer_name,readusernumber,readuserremindnumber "
                                       "from NIS_document where write_time between '%1' AND '%2'  AND (`area` IN ('CSP') OR `area` is NULL) order by write_time desc")
                                       .arg(ui->start_search_time->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                                       .arg(ui->end_search_time->dateTime().toString("yyyy-MM-dd hh:mm:ss")),my_mesdb);
    }else if(this->area == "WLP"){
        conent_model->setQuery(QString("select idx,team,process,"
                                       "write_time,document_name,"
                                       "writer_name,readusernumber,readuserremindnumber "
                                       "from NIS_document where write_time between '%1' AND '%2'  AND `area` IN ('WLP')  order by write_time desc")
                                       .arg(ui->start_search_time->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                                       .arg(ui->end_search_time->dateTime().toString("yyyy-MM-dd hh:mm:ss")),my_mesdb);
    }else if(this->area == "BPROJECT"){
        conent_model->setQuery(QString("select idx,team,process,"
                                       "write_time,document_name,"
                                       "writer_name,readusernumber,readuserremindnumber "
                                       "from NIS_document where write_time between '%1' AND '%2'  AND `area` IN ('BPROJECT')  order by write_time desc")
                                       .arg(ui->start_search_time->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                                       .arg(ui->end_search_time->dateTime().toString("yyyy-MM-dd hh:mm:ss")),my_mesdb);
    }

    qDebug()<<conent_model->lastError().text();
    conent_model->submit();
    ui->content_list_view->setModel(conent_model);
    conent_model->setHeaderData(1,Qt::Horizontal,tr("team"));
    conent_model->setHeaderData(2,Qt::Horizontal,tr("process"));
    conent_model->setHeaderData(3,Qt::Horizontal,tr("write_time"));
    conent_model->setHeaderData(4,Qt::Horizontal,tr("doc_name"));
    conent_model->setHeaderData(5,Qt::Horizontal,tr("writer_name"));
    conent_model->setHeaderData(6,Qt::Horizontal,tr("total_read"));
    conent_model->setHeaderData(7,Qt::Horizontal,tr("remind_read"));

    ui->content_list_view->horizontalHeader()->resizeSection(0,50);
    ui->content_list_view->horizontalHeader()->resizeSection(1,100);
    ui->content_list_view->horizontalHeader()->resizeSection(2,50);
    ui->content_list_view->horizontalHeader()->resizeSection(3,150);
    ui->content_list_view->horizontalHeader()->resizeSection(4,400);
    ui->content_list_view->horizontalHeader()->resizeSection(5,50);
    ui->content_list_view->horizontalHeader()->resizeSection(6,50);
    ui->content_list_view->horizontalHeader()->resizeSection(7,50);

    QSqlQuery query(my_mesdb);
    if(this->area == "CSP"){
        query.exec("select team from People_information where `area` IN ('CSP') OR `area` IS NULL GROUP by team;");
    }else if(this->area == "WLP"){
        query.exec("select team from People_information where `area` IN ('WLP') GROUP by team;");
    }else if(this->area == "BPROJECT"){
        query.exec("select team from People_information where `area` IN ('BPROJECT') GROUP by team;");
    }

    ui->select_team->addItem(" ");
    while(query.next()){
        ui->select_team->addItem(query.value("team").toString());
    }



}

NISmainwindow::~NISmainwindow()
{
    delete ui;
}

void NISmainwindow::on_write_btn_clicked()
{
    NISwrite_widget *write_widget = new NISwrite_widget(0,true,area);
    connect(write_widget,SIGNAL(close_signal()),this,SLOT(write_close_event()));
    write_widget->show();
}

void NISmainwindow::on_content_list_view_doubleClicked(const QModelIndex &index)
{
    int row = index.row();
    int idx = conent_model->index(row,0).data().toInt();
    NISwrite_widget *write_widget = new NISwrite_widget(idx,false,area);
    connect(write_widget,SIGNAL(close_signal()),this,SLOT(write_close_event()));
    write_widget->show();
}

void NISmainwindow::on_search_btn_clicked()
{
    QString filtertxt = "";
    if(ui->select_team->currentText().trimmed()!=""){
        filtertxt.append(QString("AND team = '%1'").arg(ui->select_team->currentText()));
    }
    if(ui->select_process->currentText()!=""){
        filtertxt.append(QString("AND process = '%1'").arg(ui->select_process->currentText()));
    }
    if(ui->select_name->currentText()!=""){
        filtertxt.append(QString("AND writer_name = '%1'").arg(ui->select_name->currentText()));
    }
    if(ui->LE_docname->text()!=""){
        filtertxt.append(QString("AND document_name LIKE '%%1%'").arg(ui->LE_docname->text()));
    }
    if(this->area == "CSP"){
        conent_model->setQuery(QString("select idx,team,process,"
                                       "write_time,document_name,"
                                       "writer_name,readusernumber,readuserremindnumber "
                                       "from NIS_document where write_time between '%1' AND '%2' %3 AND (`area` IN ('CSP') OR `area` is NULL) order by write_time desc")
                                       .arg(ui->start_search_time->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                                       .arg(ui->end_search_time->dateTime().toString("yyyy-MM-dd hh:mm:ss")).arg(filtertxt)
                                            ,my_mesdb);
    }else if(this->area == "WLP"){
        conent_model->setQuery(QString("select idx,team,process,"
                                       "write_time,document_name,"
                                       "writer_name,readusernumber,readuserremindnumber "
                                       "from NIS_document where write_time between '%1' AND '%2' %3 AND `area` IN ('WLP') order by write_time desc")
                                       .arg(ui->start_search_time->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                                       .arg(ui->end_search_time->dateTime().toString("yyyy-MM-dd hh:mm:ss")).arg(filtertxt)
                                            ,my_mesdb);
    }else if(this->area == "BPROJECT"){
        conent_model->setQuery(QString("select idx,team,process,"
                                       "write_time,document_name,"
                                       "writer_name,readusernumber,readuserremindnumber "
                                       "from NIS_document where write_time between '%1' AND '%2' %3 AND `area` IN ('BPROJECT') order by write_time desc")
                                       .arg(ui->start_search_time->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                                       .arg(ui->end_search_time->dateTime().toString("yyyy-MM-dd hh:mm:ss")).arg(filtertxt)
                                            ,my_mesdb);
    }

    qDebug()<<conent_model->lastError().text();
    qDebug()<<filtertxt;

}

void NISmainwindow::write_close_event()
{
    ui->end_search_time->setDateTime(QDateTime::currentDateTime().addSecs(30));
    on_search_btn_clicked();
}

void NISmainwindow::on_name_search_btn_clicked()
{
    if(this->area == "CSP"){
        conent_model->setQuery(QString("select idx,team,process,write_time,document_name,"
                                       "writer_name,readusernumber,readuserremindnumber "
                                       "from NIS_document a,NIS_read_history b where a.idx=b.document_number "
                                       "AND read_check = '0' AND user='%1' AND (a.`area` IN ('CSP') OR a.`area` is NULL) ")
                                       .arg(ui->LE_mesname->text())
                                       ,my_mesdb);

    }else if(this->area == "WLP"){
        conent_model->setQuery(QString("select idx,team,process,write_time,document_name,"
                                       "writer_name,readusernumber,readuserremindnumber "
                                       "from NIS_document a,NIS_read_history b where a.idx=b.document_number "
                                       "AND read_check = '0' AND user='%1' AND a.`area` IN ('WLP') ")
                                       .arg(ui->LE_mesname->text())
                                       ,my_mesdb);
    }else if(this->area == "BPROJECT"){
        conent_model->setQuery(QString("select idx,team,process,write_time,document_name,"
                                       "writer_name,readusernumber,readuserremindnumber "
                                       "from NIS_document a,NIS_read_history b where a.idx=b.document_number "
                                       "AND read_check = '0' AND user='%1' AND a.`area` IN ('BPROJECT') ")
                                       .arg(ui->LE_mesname->text())
                                       ,my_mesdb);
    }


}

void NISmainwindow::on_select_team_currentIndexChanged(const QString &arg1)
{
    QSqlQuery query(my_mesdb);
    QString query_txt;
    if(arg1!=""){
        ui->select_process->clear();
        ui->select_process->addItem("");
        if(this->area == "CSP"){
            query_txt = QString("select factory_process from People_information where team = '%1' AND (`area` IN ('CSP') OR `area` is NULL) group by factory_process order by factory_process asc").arg(arg1);
        }else if(this->area == "WLP"){
             query_txt = QString("select factory_process from People_information where team = '%1' AND `area` IN ('WLP') group by factory_process order by factory_process asc").arg(arg1);
        }else if(this->area == "BPROJECT"){
            query_txt = QString("select factory_process from People_information where team = '%1' AND `area` IN ('BPROJECT') group by factory_process order by factory_process asc").arg(arg1);
       }

        query.exec(query_txt);
        while(query.next()){
            ui->select_process->addItem(query.value("factory_process").toString());
        }
        ui->select_name->clear();
        ui->select_name->addItem("");
        if(this->area == "CSP"){
            query_txt = QString("select name from People_information where team = '%1' AND (`area` IN ('CSP') OR `area` is NULL) group by name order by name asc").arg(arg1);
        }else if(this->area == "WLP"){
            query_txt = QString("select name from People_information where team = '%1' AND `area` IN ('WLP') group by name order by name asc").arg(arg1);
        }else if(this->area == "BPROJECT"){
            query_txt = QString("select name from People_information where team = '%1' AND `area` IN ('BPROJECT') group by name order by name asc").arg(arg1);
        }

        query.exec(query_txt);
        while(query.next()){
            ui->select_name->addItem(query.value("name").toString());
        }
    }else {
        ui->select_process->clear();
        ui->select_process->addItem("");
        if(this->area == "CSP"){
            query_txt = QString("select factory_process from People_information where `area` IN ('CSP') OR `area` IS NULL  group by factory_process order by factory_process asc");
        }else if(this->area == "WLP"){
            query_txt = QString("select factory_process from People_information where `area` IN ('WLP') group by factory_process order by factory_process asc");
        }else if(this->area == "BPROJECT"){
            query_txt = QString("select factory_process from People_information where `area` IN ('BPROJECT') group by factory_process order by factory_process asc");
        }

        query.exec(query_txt);
        while(query.next()){
            ui->select_process->addItem(query.value("factory_process").toString());
        }
        ui->select_name->clear();
        ui->select_name->addItem("");
        if(this->area == "CSP"){
            query_txt = QString("select name from People_information where `area` IN ('CSP') OR `area`  group by name order by name");
        }else if(this->area == "WLP"){
            query_txt = QString("select name from People_information where `area` group by name order by name");
        }else if(this->area == "BPROJECT"){
            query_txt = QString("select name from People_information where `area` group by name order by name");
        }

        query.exec(query_txt);
        while(query.next()){
            ui->select_name->addItem(query.value("name").toString());
        }
    }
}
