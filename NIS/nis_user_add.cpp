#include "nis_user_add.h"
#include "ui_nis_user_add.h"

NIS_user_add::NIS_user_add(QString area, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NIS_user_add)
{
    ui->setupUi(this);
    this->area = area;
    QString now_datetime =QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    QString server_ip = settings.value("serverip").toString();
    QString db_port = settings.value("dbport").toString();
    settings.endGroup();
    QString mydb_name = QString("MY_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }

    manager = new QNetworkAccessManager(this);
    QObject::connect(manager, &QNetworkAccessManager::finished,[this](QNetworkReply *reply){
                QString userid = QString(reply->readAll());
                ui->LA_mesname->setText(userid);
                });
}

NIS_user_add::~NIS_user_add()
{
    delete ui;
}

void NIS_user_add::on_LE_mesid_editingFinished()
{
    if(this->area == "CSP"){
        QUrl url("http://10.20.10.101:8282/mes/searchusername");
        QUrlQuery  query ;
        query .addQueryItem("userid", ui->LE_mesid->text());
        url.setQuery(query);
        QNetworkRequest request(url);
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        manager->post(request,"");
    }else if(this->area == "WLP"){
        QUrl url("http://10.20.10.101:8383/mes/searchusername");
        QUrlQuery  query ;
        query .addQueryItem("userid", ui->LE_mesid->text());
        url.setQuery(query);
        QNetworkRequest request(url);
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        manager->post(request,"");
    }else if(this->area == "BPROJECT") {
        QUrl url("http://10.20.10.101:8484/mes/searchusername");
        QUrlQuery  query ;
        query .addQueryItem("userid", ui->LE_mesid->text());
        url.setQuery(query);
        QNetworkRequest request(url);
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        manager->post(request,"");
    }

}

void NIS_user_add::on_buttonBox_accepted()
{
    if(ui->LE_mesid->text() == ""){
        QMessageBox msg;
        msg.addButton(QMessageBox::Ok);

        msg.setText(tr("empty USER_ID"));

        msg.exec();
        return ;
    }
    if(ui->LA_mesname->text() == ""){
        QMessageBox msg;
        msg.addButton(QMessageBox::Ok);

        msg.setText(tr("don't have id"));

        msg.exec();
        return ;
    }
    QSqlQuery query(my_mesdb);
    query.exec(QString("INSERT INTO `FAB`.`NIS_user_group` (`id`, `name`, `nisgroup`,`area`) VALUES ('%1', '%2', '%3','%4');")
               .arg(ui->LE_mesid->text()).arg(ui->LA_mesname->text()).arg(ui->LE_group->text()).arg(area));

    return;
}
