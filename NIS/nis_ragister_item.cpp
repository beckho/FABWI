#include "nis_ragister_item.h"
#include "ui_nis_ragister_item.h"

NIS_ragister_item::NIS_ragister_item(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NIS_ragister_item)
{
    ui->setupUi(this);
}
NIS_ragister_item::NIS_ragister_item(QString name, int docnumber, QString area, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NIS_ragister_item)
{
    ui->setupUi(this);
    this->docnumber= docnumber;
    ui->LA_name->setText(name);
    this->area = area;
}

void NIS_ragister_item::set_ui_LA_name(QString name)
{
    ui->LA_name->setText(name);
}

QLabel *NIS_ragister_item::get_ui_LA_name()
{
    return ui->LA_name;
}

QCheckBox *NIS_ragister_item::get_ui_CB_readable()
{
    return ui->CB_readable;
}

NIS_ragister_item::~NIS_ragister_item()
{
    delete ui;
}

void NIS_ragister_item::on_CB_readable_clicked(bool checked)
{
    NIS_login_from *login = new NIS_login_from(ui->LA_name->text(),docnumber,area);
    if(login->exec()==QDialog::Accepted){
        ui->CB_readable->setChecked(login->getResult());
    }else{
        ui->CB_readable->setChecked(false);
    }

}
