#ifndef NIS_USER_FROM_H
#define NIS_USER_FROM_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QDateTime>
#include <QSettings>
#include <global_define.h>
#include <QDebug>
#include <QSqlError>
#include <QStandardItemModel>
#include <QSqlQuery>
#include <NIS/nis_user_add.h>
namespace Ui {
class NIS_user_from;
}

class NIS_user_from : public QDialog
{
    Q_OBJECT

public:
    explicit NIS_user_from(QString area,QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    QSqlTableModel *maintable;
    QString server_ip;
    QString area;
    QString db_port;
    QStandardItemModel *register_model;
    QStringList namelist;


    ~NIS_user_from();

    QStringList getNamelist() const;
    void setNamelist(const QStringList &value);

private:
    Ui::NIS_user_from *ui;
public slots:
    void header_click(int index);
private slots:
    void on_move_add_btn_clicked();
    void on_groupbox_currentIndexChanged(const QString &arg1);
    void on_remvoe_btn_clicked();
    void on_add_id_btn_clicked();
    void on_del_id_btn_clicked();
    void on_dialog_btn_accepted();
};

#endif // NIS_USER_FROM_H
