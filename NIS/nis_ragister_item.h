#ifndef NIS_RAGISTER_ITEM_H
#define NIS_RAGISTER_ITEM_H

#include <QWidget>
#include <QLabel>
#include <QCheckBox>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <global_define.h>
#include <NIS/nis_login_from.h>
#include <QDebug>
namespace Ui {
class NIS_ragister_item;
}

class NIS_ragister_item : public QWidget
{
    Q_OBJECT

public:
    explicit NIS_ragister_item(QWidget *parent = 0);
    NIS_ragister_item(QString name,int docnumber,QString area,QWidget *parent = 0);
    int docnumber=0;
    QString area;
    void set_ui_LA_name(QString name);
    QLabel *get_ui_LA_name();
    QCheckBox *get_ui_CB_readable();
    ~NIS_ragister_item();

private slots:
    void on_CB_readable_clicked(bool checked);

private:
    Ui::NIS_ragister_item *ui;
};

#endif // NIS_RAGISTER_ITEM_H
