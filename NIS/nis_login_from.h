#ifndef NIS_LOGIN_FROM_H
#define NIS_LOGIN_FROM_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <global_define.h>
#include <QDateTime>
#include <QMessageBox>
#include <QDebug>
#include <QSqlError>
#include <QSettings>
#include <QNetworkAccessManager>
#include <QEventLoop>
#include <QUrl>
#include <QUrlQuery>
#include <QNetworkRequest>
#include <ns_core_com/ns_core_com.h>
#include <QNetworkReply>
namespace Ui {
class NIS_login_from;
}

class NIS_login_from : public QDialog
{
    Q_OBJECT

public:
    explicit NIS_login_from(QString name,int document_number,QString area,QWidget *parent = 0);
    QString name;
    QSqlDatabase ms_mesdb;
    QString area;
    QSqlDatabase my_mesdb;
    int document_number;
    bool result;
    QEventLoop loop;
    QNetworkAccessManager *manager;
    ~NIS_login_from();

    bool getResult() const;
    void setResult(bool value);

private slots:
    void on_buttonBox_accepted();


    void on_LE_ID_editingFinished();

    void on_buttonBox_rejected();

private:
    Ui::NIS_login_from *ui;
};

#endif // NIS_LOGIN_FROM_H
