#include "nis_login_from.h"
#include "ui_nis_login_from.h"


NIS_login_from::NIS_login_from(QString name, int document_number, QString area, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NIS_login_from)
{
    ui->setupUi(this);
    this->name = name;
    this->document_number = document_number;
    this->area = area;



    QString now_datetime =QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    QString server_ip = settings.value("serverip").toString();
    QString db_port = settings.value("dbport").toString();
    settings.endGroup();
    QString mydb_name = QString("MY_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }
    result = false;
    manager = new QNetworkAccessManager(this);
    QObject::connect(manager, &QNetworkAccessManager::finished,[this](QNetworkReply *reply){
                    if(reply->url().toString().indexOf("checkuserlogin")>=0){
                        QString respone = QString(reply->readAll());
                        if(respone=="done"){
                            QSqlQuery query1(my_mesdb);
                            query1.exec(QString("UPDATE `NIS_read_history` SET `read_check`=1 WHERE  `document_number`=%1 AND `user`='%2' ")
                                        .arg(this->document_number).arg(this->name));
                            query1.exec(QString("select count(user) AS readcount from NIS_read_history where document_number = '%1'").arg(this->document_number));
                            QString read_user_total = "0";
                            if(query1.next()){
                                read_user_total = query1.value("readcount").toString();
                            }
                            query1.exec(QString("select count(user) AS readcount from NIS_read_history where document_number = '%1' AND read_check = '1'")
                                        .arg(this->document_number));
                            QString read_user_count = "0";
                            if(query1.next()){
                                read_user_count = query1.value("readcount").toString();
                            }
                            query1.exec(QString("UPDATE `NIS_document` SET `readusernumber`='%1', `readuserremindnumber`='%2' WHERE  `idx`=%3 ")
                                        .arg(read_user_total).arg(read_user_count).arg(this->document_number));
                            result = true;
                        }else {
                            result = false;
                        }
                        loop.quit();
                    }else if(reply->url().toString().indexOf("searchusername")>=0){
                         QString respone = QString(reply->readAll());
                         if(respone!=this->name){
                             QMessageBox msg;
                             msg.addButton(QMessageBox::Ok);
                             msg.setText(tr("name wrong"));
                             msg.exec();
                             ui->LE_PW->setEnabled(false);
                         }else {
                             ui->LA_NAME->setText(respone);
                             ui->LE_PW->setEnabled(true);
                         }
                    }

                });
}

bool NIS_login_from::getResult() const
{
    return result;
}

void NIS_login_from::setResult(bool value)
{
    result = value;
}

NIS_login_from::~NIS_login_from()
{
    delete ui;
}

void NIS_login_from::on_buttonBox_accepted()
{
    if(this->area=="CSP"){
        QUrl url("http://10.20.10.101:8282/mes/checkuserlogin");
        QUrlQuery  query ;
        query .addQueryItem("userid", ui->LE_ID->text());
        query .addQueryItem("pass", ui->LE_PW->text());
        url.setQuery(query);
        QNetworkRequest request(url);
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        manager->post(request,"");
        loop.exec();
    }else if(this->area=="WLP"){
        QUrl url("http://10.20.10.101:8383/mes/checkuserlogin");
        QUrlQuery  query ;
        query .addQueryItem("userid", ui->LE_ID->text());
        query .addQueryItem("pass", ui->LE_PW->text());
        url.setQuery(query);
        QNetworkRequest request(url);
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        manager->post(request,"");
        loop.exec();

        return ;
    }else if(this->area=="BPROJECT"){
        QUrl url("http://10.20.10.101:8484/mes/checkuserlogin");
        QUrlQuery  query ;
        query .addQueryItem("userid", ui->LE_ID->text());
        query .addQueryItem("pass", ui->LE_PW->text());
        url.setQuery(query);
        QNetworkRequest request(url);
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        manager->post(request,"");
        loop.exec();

        return ;
    }

}

void NIS_login_from::on_LE_ID_editingFinished()
{
    if(this->area=="CSP"){
        QUrl url("http://10.20.10.101:8282/mes/searchusername");
        QUrlQuery  query ;
        query .addQueryItem("userid", ui->LE_ID->text());
        url.setQuery(query);
        QNetworkRequest request(url);
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");


        manager->post(request,"");
    }else if(this->area=="WLP"){
        QUrl url("http://10.20.10.101:8383/mes/searchusername");
        QUrlQuery  query ;
        query .addQueryItem("userid", ui->LE_ID->text());
        url.setQuery(query);
        QNetworkRequest request(url);
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");


        manager->post(request,"");
    }else if(this->area=="BPROJECT"){
        QUrl url("http://10.20.10.101:8484/mes/searchusername");
        QUrlQuery  query ;
        query .addQueryItem("userid", ui->LE_ID->text());
        url.setQuery(query);
        QNetworkRequest request(url);
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");


        manager->post(request,"");
    }

}

void NIS_login_from::on_buttonBox_rejected()
{

}
