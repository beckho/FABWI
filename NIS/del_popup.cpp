#include "del_popup.h"
#include "ui_del_popup.h"

del_popup::del_popup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::del_popup)
{
    ui->setupUi(this);
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    QString server_ip = settings.value("serverip").toString();
    QString db_port = settings.value("dbport").toString();
    settings.endGroup();
    QString mydb_name = QString("MY_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }
    result = false;

}

bool del_popup::getResult() const
{
    return result;
}

void del_popup::setResult(bool value)
{
    result = value;
}

del_popup::~del_popup()
{
    delete ui;
}

void del_popup::on_buttonBox_accepted()
{

    QSqlQuery query(my_mesdb);
    query.exec("select * from NIS_management");
    if(query.next()){
          QString PW = query.value("delete_password").toString();
          if(PW==ui->LE_PW->text()){
              result = true;
          }else {
              result = false;
          }
    }

}
