#ifndef NISWRITE_WIDGET_H
#define NISWRITE_WIDGET_H

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <btextedit.h>
#include <QFtp>
#include <QProgressDialog>
#include <QStandardItemModel>
#include <QSettings>
#include <QTimer>
#include <global_define.h>
#include <QSqlError>
#include <QColorDialog>
#include <NIS/nis_user_from.h>
#include <NIS/nis_ragister_item.h>
#include <NIS/del_popup.h>
#include <QFileDialog>
#include <QDesktopServices>
#include <QPrinter>
#include <QPrintDialog>
namespace Ui {
class NISwrite_widget;
}

class NISwrite_widget : public QWidget
{
    Q_OBJECT

public:
    explicit NISwrite_widget(int doc_number,bool new_doc,QString area,QWidget *parent = 0);
    int doc_number;
    QSqlDatabase db ;
    QTimer time_update;
    QString area;
    BTextEdit *content_edit;
    QFtp *ftp;
    QString server_ip;
    QString db_port;
    QString ftp_port;
    QByteArray ftp_data;
    QProgressDialog *progressdialog;
    QStandardItemModel *attach_list_model;
    QVector<NIS_ragister_item *> ragister_item_list;
    QFont mainfont;
    QEventLoop loop;
    Ui::NISwrite_widget *ui;
    bool new_doc;
    ~NISwrite_widget();

private:
    void closeEvent(QCloseEvent *event);
signals:
    void close_signal();

public slots:
    void time_update_out();
    void updateDataTransferProgress(qint64 readBytes,
                                    qint64 totalBytes);
private slots:
    void on_select_team_currentIndexChanged(const QString &arg1);
    void finish_done(bool);
    void on_add_button_clicked();
    void on_Bold_btn_clicked();
    void on_underline_btn_clicked();
    void on_color_dialog_clicked();
    void on_fontsize_editingFinished();
    void on_font_type_currentTextChanged(const QString &arg1);
    void on_register_btn_clicked();
    void on_modify_btn_clicked();
    void on_del_btn_clicked();
    void on_del_btn2_clicked();
    void on_attach_btn_clicked();

    void on_attach_remove_btn_clicked();
    void on_attach_listview_doubleClicked(const QModelIndex &index);
    void on_print_btn_clicked();
};

#endif // NISWRITE_WIDGET_H
