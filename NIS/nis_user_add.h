#ifndef NIS_USER_ADD_H
#define NIS_USER_ADD_H

#include <QDialog>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDateTime>
#include <QSqlError>
#include <QMessageBox>
#include <QSettings>
#include <QNetworkAccessManager>
#include <QEventLoop>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <global_define.h>
#include <QUrl>
#include <QUrlQuery>
namespace Ui {
class NIS_user_add;
}

class NIS_user_add : public QDialog
{
    Q_OBJECT

public:
    explicit NIS_user_add(QString area,QWidget *parent = 0);
    QSqlDatabase ms_mesdb;
    QSqlDatabase my_mesdb;
    QEventLoop loop;
    QNetworkAccessManager *manager;

    QString area;
    ~NIS_user_add();

private slots:
    void on_LE_mesid_editingFinished();

    void on_buttonBox_accepted();

private:
    Ui::NIS_user_add *ui;
};

#endif // NIS_USER_ADD_H
