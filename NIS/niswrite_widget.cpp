#include "niswrite_widget.h"
#include "ui_niswrite_widget.h"

NISwrite_widget::NISwrite_widget(int doc_number, bool new_doc, QString area, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NISwrite_widget)
{
    ui->setupUi(this);
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    progressdialog = 0;
    this->area = area;
    ui->current_time->setDateTime(QDateTime::currentDateTime());
    time_update.setInterval(1000);
    time_update.start();
    connect(&time_update,SIGNAL(timeout()),this,SLOT(time_update_out()));
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    server_ip = settings.value("serverip").toString();
    db_port = settings.value("dbport").toString();
    ftp_port = settings.value("ftpport").toString();
    settings.endGroup();
    ftp = new QFtp(this);
    this->setObjectName("NISwrite_widget");

    connect(ftp, SIGNAL(done(bool)),
            this, SLOT(finish_done(bool)));
    connect(ftp, SIGNAL(dataTransferProgress(qint64,qint64)),
            this, SLOT(updateDataTransferProgress(qint64,qint64)));


    if(!db.contains("EISDB")){
       db = QSqlDatabase::addDatabase("QMYSQL","EISDB");
       db.setHostName(server_ip);
       db.setPort(db_port.toInt());
       db.setUserName(DBID);
       db.setPassword(DBPW);
       db.setDatabaseName(DBFABNAME);
    }else {
       db = QSqlDatabase::database("EISDB");
    }

    if(!db.isOpen()){
        if(!db.open()){
             int ret = QMessageBox::warning(this, tr("conntion false"),
                                               "server connection fail\n"
                                                  ""+db.lastError().text(),
                                                    QMessageBox::Close);
        }
    }
    //docnumber을 +1 한값을 데이터베이스 업로드 시켜 문서가 서로 중첩되지 않게한다.
    content_edit = new BTextEdit(&doc_number,this);
    content_edit->setObjectName("NIS_content");
    ui->content_layout->addWidget(content_edit);


    QSqlQuery query(db);
    if(this->area == "CSP"){
        query.exec("select team from People_information where `area` IN ('CSP') OR `area` IS NULL  GROUP by team;");
    }else if(this->area == "WLP"){
        query.exec("select team from People_information where `area` IN ('WLP')GROUP by team;");
    }else if(this->area == "BPROJECT"){
        query.exec("select team from People_information where `area` IN ('BPROJECT')GROUP by team;");
    }

    ui->select_team->addItem(" ");
    while(query.next()){
        ui->select_team->addItem(query.value("team").toString());
    }

    if(new_doc){
        QString str_query = QString("update NIS_management set document_number = NIS_management.document_number +1;");
        query.exec(str_query);
        query.exec("select * from NIS_management");
        query.next();
        doc_number = query.value("document_number").toInt();
        this->doc_number = doc_number;
        ui->modify_btn->setEnabled(false);
        ui->del_btn->setEnabled(false);

    }else {
        this->doc_number = doc_number;
        ui->add_button->setEnabled(false);
        QString str_query = QString("select * from NIS_document where idx = '%1'").arg(doc_number);
        query.exec(str_query);
        if(query.next()){
            content_edit->setHtml(query.value("content").toString());
            ui->document_name->setText(query.value("document_name").toString());
            ui->select_team->setCurrentText(query.value("team").toString());
            ui->select_process->setCurrentText(query.value("process").toString());
            ui->select_name->setCurrentText(query.value("writer_name").toString());
            QSqlQuery query3(db);
            query3.exec(QString("select * from `NIS_read_history` where document_number = '%1'").arg(doc_number));
            while(query3.next()){
                NIS_ragister_item * item = new NIS_ragister_item(query3.value("user").toString(),doc_number,area);
                ragister_item_list.append(item);
                int count = ui->ragister_view->rowCount();
                ui->ragister_view->insertRow(count);
                ui->ragister_view->setCellWidget(count,0,item->get_ui_CB_readable());
                ui->ragister_view->setCellWidget(count,1,item->get_ui_LA_name());
                if(query3.value("read_check").toInt()==1){
                 item->get_ui_CB_readable()->setChecked(true);
                }else if (query3.value("read_check").toInt()==0){
                  item->get_ui_CB_readable()->setChecked(false);
                }
            }

        }

    }

    attach_list_model = new QStandardItemModel();
    if(!new_doc){
        QSqlQuery query2(db);
        query2.exec(QString("select * from NIS_document where idx = '%1'").arg(doc_number));
        query2.next();
        QStringList attach_file_list = query2.value("attach_file_list").toString().split("/////");
        for(int i=0;i<attach_file_list.count()-1;i++){
            attach_list_model->insertRow(i,new QStandardItem(attach_file_list.at(i)));
        }
    }
    ui->attach_listview->setModel(attach_list_model);

}

NISwrite_widget::~NISwrite_widget()
{
    delete ui;
}

void NISwrite_widget::closeEvent(QCloseEvent *event)
{
    emit close_signal();
}

void NISwrite_widget::time_update_out()
{
    ui->current_time->setDateTime(QDateTime::currentDateTime());
}

void NISwrite_widget::updateDataTransferProgress(qint64 readBytes, qint64 totalBytes)
{
    progressdialog->setMaximum(totalBytes);
    progressdialog->setValue(readBytes);
}

void NISwrite_widget::on_select_team_currentIndexChanged(const QString &arg1)
{
    QSqlQuery query(db);
    QString query_txt;
    if(arg1!=""){
        ui->select_process->clear();
        ui->select_process->addItem("");
        if(this->area == "CSP"){
            query_txt = QString("select factory_process from People_information where team = '%1' AND (`area` IN ('CSP') OR `area` is NULL) group by factory_process order by factory_process asc").arg(arg1);
        }else if(this->area == "WLP"){
            query_txt = QString("select factory_process from People_information where team = '%1' AND `area` IN ('WLP') group by factory_process order by factory_process asc").arg(arg1);
        }else if(this->area == "BPROJECT"){
            query_txt = QString("select factory_process from People_information where team = '%1' AND `area` IN ('BPROJECT') group by factory_process order by factory_process asc").arg(arg1);
        }
        query.exec(query_txt);
        while(query.next()){
            ui->select_process->addItem(query.value("factory_process").toString());
        }
        ui->select_name->clear();
        ui->select_name->addItem("");
        if(this->area == "CSP"){
            query_txt = QString("select name from People_information where team = '%1' AND (`area` IN ('CSP') OR `area` is NULL) group by name order by name asc").arg(arg1);
        }else if(this->area == "WLP"){
            query_txt = QString("select name from People_information where team = '%1' AND `area` IN ('WLP') group by name order by name asc").arg(arg1);
        }else if(this->area == "BPROJECT"){
            query_txt = QString("select name from People_information where team = '%1' AND `area` IN ('BPROJECT') group by name order by name asc").arg(arg1);
        }

        query.exec(query_txt);
        while(query.next()){
            ui->select_name->addItem(query.value("name").toString());
        }
    }else {
        ui->select_process->clear();
        ui->select_process->addItem("");
        if(this->area == "CSP"){
            query_txt = QString("select factory_process from People_information where `area` IN ('CSP') OR `area` IS NULL group by factory_process order by factory_process asc");
        }else if(this->area == "WLP"){
            query_txt = QString("select factory_process from People_information where `area` IN ('WLP') group by factory_process order by factory_process asc");
        }else if(this->area == "BPROJECT"){
            query_txt = QString("select factory_process from People_information where `area` IN ('BPROJECT') group by factory_process order by factory_process asc");
        }

        query.exec(query_txt);
        while(query.next()){
            ui->select_process->addItem(query.value("factory_process").toString());
        }
        ui->select_name->clear();
        ui->select_name->addItem("");
        if(this->area == "CSP"){
            query_txt = QString("select name from People_information where `area` IN ('CSP') OR `area` IS NULL group by name order by name");
        }else if(this->area == "WLP"){
            query_txt = QString("select name from People_information where `area` IN ('WLP') group by name order by name");
        }else if(this->area == "BPROJECT"){
            query_txt = QString("select name from People_information where `area` IN ('BPROJECT') group by name order by name");
        }

        query.exec(query_txt);
        while(query.next()){
            ui->select_name->addItem(query.value("name").toString());
        }
    }

}

void NISwrite_widget::finish_done(bool)
{
    loop.exit();
}

void NISwrite_widget::on_add_button_clicked()
{
    QMessageBox msg;
    msg.addButton(QMessageBox::Ok);
    if(ui->select_team->currentText()==""){
        msg.setText(tr("empty team_name"));

        msg.exec();
        return ;
    }
    if(ui->select_process->currentText()==""){
        msg.setText(tr("empty process"));

        msg.exec();
        return ;
    }
    if(ui->select_name->currentText()==""){
        msg.setText(tr("empty name"));

        msg.exec();
        return ;
    }
    if(ui->document_name->text()==""){
        msg.setText(tr("empty document_name"));

        msg.exec();
        return ;
    }
    if(ftp->state()==QFtp::Unconnected){
        ftp->connectToHost(server_ip,21);

        ftp->login(QUrl::fromPercentEncoding(FTPID),FTPPW);
        loop.exec();
        ftp->setTransferMode(QFtp::Active);
    }

    QString now_datetime =ui->current_time->dateTime().toString("yyyy-MM-dd HH:mm:ss");
    QString mydb_name = QString("MY_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    QSqlDatabase my_mesdb;
    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }
    QString document_name = ui->document_name->text();
    QString team_name = ui->select_team->currentText();
    QString process = ui->select_process->currentText();
    QString write_name = ui->select_name->currentText();
    QSqlQuery query1(my_mesdb);
    query1.exec(QString("select count(user) AS readcount from NIS_read_history where document_number = '%1'").arg(doc_number));
    QString read_user_total = "0";
    if(query1.next()){
        read_user_total = query1.value("readcount").toString();
    }
    query1.exec(QString("select count(user) AS readcount from NIS_read_history where document_number = '%1' AND read_check = '1'")
                .arg(doc_number));
    QString read_user_count = "0";
    if(query1.next()){
        read_user_count = query1.value("readcount").toString();
    }
    QString attach_file_list;
    for(int i=0;i<attach_list_model->rowCount();i++){
        QString item = attach_list_model->item(i)->text();
        attach_file_list = attach_file_list + item + "/////";
    }

    QString insert_query = QString("INSERT INTO NIS_document ("
                                   "`idx`, "
                                   "`team`, "
                                   "`process`, "
                                   "`write_time`, "
                                   "`writer_name`, "
                                   "`document_name`, "
                                   "`content`, "
                                   "`readusernumber`, "
                                   "`readuserremindnumber`, "
                                   "`attach_file_list`,"
                                   "`area`"
                                   ") VALUES ("
                                   "'"+QString("%1").arg(doc_number)+"', "
                                   "'"+team_name+"',"
                                   "'"+process+"',"
                                   "'"+now_datetime+"',"
                                   "'"+write_name+"',"
                                   "'"+document_name+"',"
                                   "'"+content_edit->tosqlhtml()+"',"
                                   " "+read_user_total+","
                                   " "+read_user_count+","
                                   " '"+attach_file_list+"',"
                                   "'"+this->area+"'"
                                   ");");



    QSqlQuery query(db);
    query.exec(insert_query);
    qDebug()<<query.lastQuery();
    QString str_query = QString("update NIS_management set document_number = NIS_management.document_number +1;");
    query.exec(str_query);
    query.exec("select * from NIS_management");
    query.next();
    doc_number = query.value("document_number").toInt();
    msg.setText(tr("add complete"));

    msg.exec();
    this->close();
}

void NISwrite_widget::on_Bold_btn_clicked()
{
    QString current_stylesheet =  ui->Bold_btn->styleSheet();
    bool current_bold;
    if(current_stylesheet.indexOf("Bold_btn_activity.png")>0){
        current_bold = true;
    }else {
        current_bold = false;
    }
    QFont update_font ;
    if(current_bold){

        update_font = content_edit->currentCharFormat().font();
        update_font.setBold(false);
        ui->Bold_btn->setStyleSheet(QLatin1String("QPushButton{\n"
                                    "	border-image:url(:/img/img/Bold_btn_inactivity.png)\n"
                                    "}"));
    }else {

        update_font = content_edit->currentCharFormat().font();
        update_font.setBold(true);
        ui->Bold_btn->setStyleSheet(QLatin1String("QPushButton{\n"
                                    "	border-image:url(:/img/img/Bold_btn_activity.png)\n"
                                    "}"));
    }
    QTextCharFormat charfotmet;
    charfotmet.setFont(update_font,QTextCharFormat::FontPropertiesAll);
    charfotmet.setForeground(content_edit->currentCharFormat().foreground());
    content_edit->setCurrentCharFormat(charfotmet);
}

void NISwrite_widget::on_underline_btn_clicked()
{
    QString current_stylesheet =  ui->underline_btn->styleSheet();
    bool current_underline;
    if(current_stylesheet.indexOf("under_line_activity.png")>0){
        current_underline = true;
    }else {
        current_underline = false;
    }
    QFont update_font ;
    if(current_underline){
        update_font = content_edit->currentCharFormat().font();
        update_font.setUnderline(false);
        ui->underline_btn->setStyleSheet(QLatin1String("QPushButton{\n"
                                    "	border-image:url(:/img/img/under_line_inactivity.png)\n"
                                    "}"));
    }else {
        update_font = content_edit->currentCharFormat().font();
        update_font.setUnderline(true);
        ui->underline_btn->setStyleSheet(QLatin1String("QPushButton{\n"
                                    "	border-image:url(:/img/img/under_line_activity.png)\n"
                                    "}"));
    }
    QTextCharFormat charfotmet;
    charfotmet.setFont(update_font,QTextCharFormat::FontPropertiesAll);
    charfotmet.setForeground(content_edit->currentCharFormat().foreground());
    content_edit->setCurrentCharFormat(charfotmet);
}

void NISwrite_widget::on_color_dialog_clicked()
{
    QColor font_color = QColorDialog::getColor();
    QFont update_font;
    update_font = content_edit->currentCharFormat().font();
    QTextCharFormat charfotmet;
    charfotmet.setFont(update_font,QTextCharFormat::FontPropertiesAll);
    charfotmet.setForeground(QBrush(font_color));
    content_edit->setCurrentCharFormat(charfotmet);
}

void NISwrite_widget::on_fontsize_editingFinished()
{
    QFont update_font;
    update_font = content_edit->currentCharFormat().font();
    update_font.setPointSize(ui->fontsize->value());
    QTextCharFormat charfotmet;
    charfotmet.setFont(update_font,QTextCharFormat::FontPropertiesAll);
    charfotmet.setForeground(content_edit->currentCharFormat().foreground());
    content_edit->setCurrentCharFormat(charfotmet);
}

void NISwrite_widget::on_font_type_currentTextChanged(const QString &arg1)
{
    QFont update_font;
    update_font = content_edit->currentCharFormat().font();
    update_font.setFamily(arg1);
    QTextCharFormat charfotmet;
    charfotmet.setFont(update_font,QTextCharFormat::FontPropertiesAll);
    charfotmet.setForeground(content_edit->currentCharFormat().foreground());
    content_edit->setCurrentCharFormat(charfotmet);
}

void NISwrite_widget::on_register_btn_clicked()
{
    NIS_user_from *from = new NIS_user_from(area);
    if(from->exec()==QDialog::Accepted){
        int count = ui->ragister_view->rowCount();
        qDebug()<<count;
        for(int i=0;i<count;i++){
            ui->ragister_view->removeRow(0);
        }

        for(int i=0;i<from->getNamelist().count();i++){
           QString name = from->getNamelist().at(i);
           QSqlQuery query2(db);
           query2.exec(QString("select * from `NIS_read_history` where document_number = '%1' AND user= '%2' LIMIT 1")
                       .arg(doc_number).arg(name));
           if(query2.next()){

           }else {
               query2.exec(QString("insert into `NIS_read_history` (`document_number`, `user`,`read_check`,`area`) VALUES ('%1', '%2',0,'%3');")
                           .arg(doc_number).arg(name).arg(area));
           }
        }
        QSqlQuery query3(db);
        query3.exec(QString("select * from `NIS_read_history` where document_number = '%1'").arg(doc_number));
        while(query3.next()){
            NIS_ragister_item * item = new NIS_ragister_item(query3.value("user").toString(),doc_number,area);
            ragister_item_list.append(item);
            int count = ui->ragister_view->rowCount();
            ui->ragister_view->insertRow(count);
            ui->ragister_view->setCellWidget(count,0,item->get_ui_CB_readable());
            ui->ragister_view->setCellWidget(count,1,item->get_ui_LA_name());
            if(query3.value("read_check").toInt()==1){
             item->get_ui_CB_readable()->setChecked(true);
            }else if (query3.value("read_check").toInt()==0){
              item->get_ui_CB_readable()->setChecked(false);
            }
        }
    }else {

    }
}

void NISwrite_widget::on_modify_btn_clicked()
{
    QMessageBox msg;
    msg.addButton(QMessageBox::Ok);
    if(ui->select_team->currentText()==""){
        msg.setText(tr("empty team_name"));

        msg.exec();
        return ;
    }
    if(ui->select_process->currentText()==""){
        msg.setText(tr("empty process"));

        msg.exec();
        return ;
    }
    if(ui->select_name->currentText()==""){
        msg.setText(tr("empty name"));

        msg.exec();
        return ;
    }
    if(ui->document_name->text()==""){
        msg.setText(tr("empty document_name"));

        msg.exec();
        return ;
    }
    if(ftp->state()==QFtp::Unconnected){
        ftp->connectToHost(server_ip,21);

        ftp->login(QUrl::fromPercentEncoding(FTPID),FTPPW);
        loop.exec();
        ftp->setTransferMode(QFtp::Active);
    }

    QString mydb_name = QString("MY_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    QSqlDatabase my_mesdb;
    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }
    QString document_name = ui->document_name->text();
    QString team_name = ui->select_team->currentText();
    QString process = ui->select_process->currentText();
    QString write_name = ui->select_name->currentText();
    QSqlQuery query1(my_mesdb);
    query1.exec(QString("select count(user) AS readcount from NIS_read_history where document_number = '%1'").arg(doc_number));
    QString read_user_total = "0";
    if(query1.next()){
        read_user_total = query1.value("readcount").toString();
    }
    query1.exec(QString("select count(user) AS readcount from NIS_read_history where document_number = '%1' AND read_check = '1'")
                .arg(doc_number));
    QString read_user_count = "0";
    if(query1.next()){
        read_user_count = query1.value("readcount").toString();
    }

    QString update_query = QString("update `NIS_document` set team = '%1',"
                                   "process = '%2' , write_time = '%3', writer_name = '%4',"
                                   "document_name = '%5',content='%6',readusernumber = '%7',readuserremindnumber = '%8' "
                                   "where idx = '%9'").arg(team_name)
                                    .arg(process).arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss")).arg(write_name)
                                    .arg(document_name).arg(content_edit->tosqlhtml()).arg(read_user_total).arg(read_user_count)
                                    .arg(doc_number);



    QSqlQuery query(db);
    query.exec(update_query);
    qDebug()<<query.lastQuery();
    qDebug()<<query.lastError().text();
    msg.setText(tr("modify complete"));

    msg.exec();
    this->close();
}

void NISwrite_widget::on_del_btn_clicked()
{
    del_popup *popup = new del_popup();
    popup->exec();
    if(popup->getResult()){
        QString delquery = QString("DELETE FROM `NIS_document` WHERE  `idx`='%1'").arg(doc_number);
        QSqlQuery query(db);
        query.exec(delquery);
        QMessageBox msg;
        msg.setText(tr("del complete"));
        msg.exec();
        this->close();
    }else {
        QMessageBox msg;
        msg.setText(tr("PW worng"));
        msg.exec();
        this->close();
    }

}

void NISwrite_widget::on_del_btn2_clicked()
{
    int row  = ui->ragister_view->selectionModel()->currentIndex().row();
    QString name = ((QLabel *)ui->ragister_view->cellWidget(row,1))->text();
    ui->ragister_view->removeRow(row);
    QString delquery = QString("DELETE FROM `NIS_read_history` WHERE  `document_number`='%1' AND `user` = '%2'").arg(doc_number).arg(name);
    QSqlQuery query(db);
    query.exec(delquery);

    query.exec(QString("select count(user) AS readcount from NIS_read_history where document_number = '%1'").arg(doc_number));
    QString read_user_total = "0";
    if(query.next()){
        read_user_total = query.value("readcount").toString();
    }
    query.exec(QString("select count(user) AS readcount from NIS_read_history where document_number = '%1' AND read_check = '1'")
                .arg(doc_number));
    QString read_user_count = "0";
    if(query.next()){
        read_user_count = query.value("readcount").toString();
    }
    query.exec(QString("UPDATE `NIS_document` SET `readusernumber`='%1', `readuserremindnumber`='%2' WHERE  `idx`=%3 ")
                .arg(read_user_total).arg(read_user_count).arg(doc_number));
}

void NISwrite_widget::on_attach_btn_clicked()
{
    QFileDialog filedialog(this);
    filedialog.setFileMode(QFileDialog::AnyFile);
    filedialog.setViewMode(QFileDialog::Detail);
    if(filedialog.exec()==QFileDialog::Accepted){
        QString filename = filedialog.selectedFiles().at(0);
        if(ftp->state()==QFtp::Unconnected){
            ftp->connectToHost(server_ip,21);

            ftp->login(QUrl::fromPercentEncoding(FTPID),FTPPW);
            loop.exec();
            ftp->setTransferMode(QFtp::Active);
        }
        ftp->mkdir(QString("/home/eis/nisattach/%1").arg(doc_number));

        loop.exec();
        ftp->cd(QString("/home/eis/nisattach/%1").arg(doc_number));
        loop.exec();
        QFile *source_file = new QFile(filename);
        source_file->open(QFile::ReadWrite);
        QString upload_filename = filename.split('/').last();
        for(int i=0;i<attach_list_model->rowCount();i++){
              QString item = attach_list_model->item(i)->text();
              if(item == upload_filename){
                    QString first_name = upload_filename.split(".").at(0)+"_t";
                    QString Last_name;
                    if(upload_filename.split('.').size()>=2){
                        Last_name = "."+upload_filename.split(".").at(1);
                    }
                    upload_filename =first_name + Last_name;
              }
        }
        ftp->put(source_file,upload_filename);
        if(progressdialog == 0){
            progressdialog = new QProgressDialog(this);
        }
        progressdialog->setLabelText("upload");
        progressdialog->exec();
        attach_list_model->insertRow(attach_list_model->rowCount(),
                                     new QStandardItem(upload_filename));
        source_file->close();
    }
}

void NISwrite_widget::on_attach_remove_btn_clicked()
{
    QModelIndex index  = ui->attach_listview->currentIndex();
    QString select_file = index.data().toString();
    if(ftp->state()==QFtp::Unconnected){
        ftp->connectToHost(server_ip,21);

        ftp->login(QUrl::fromPercentEncoding(FTPID),FTPPW);
        loop.exec();
        ftp->setTransferMode(QFtp::Active);
    }
    ftp->cd(QString("/home/eis/nisattach/%1").arg(doc_number));
    loop.exec();
    ftp->remove(select_file);
    loop.exec();
    attach_list_model->removeRow(index.row());
}

void NISwrite_widget::on_attach_listview_doubleClicked(const QModelIndex &index)
{
    QFileDialog file_dialog;
    QString source_file = index.data().toString();
    QString save_filepath = file_dialog.getSaveFileName(this,tr("Save File"),index.data().toString());
    qDebug()<<"save_filepath = "<<save_filepath;
    if(save_filepath==""){
        return;
    }
    QFile *save_file = new QFile(save_filepath);
    if(ftp->state()==QFtp::Unconnected){
        ftp->connectToHost(server_ip,21);

        ftp->login(QUrl::fromPercentEncoding(FTPID),FTPPW);
        loop.exec();
        ftp->setTransferMode(QFtp::Active);
    }
    ftp->cd(QString("/home/eis/nisattach/%1").arg(doc_number));
    loop.exec();
    save_file->open(QFile::ReadWrite);
    ftp->get(source_file,save_file);
    if(progressdialog == 0){
        progressdialog = new QProgressDialog(this);
    }
    progressdialog->setLabelText("download");
    progressdialog->exec();
    save_file->close();
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, "from",
                                                                 tr("now open file ?\n"),
                                                                 QMessageBox::Cancel| QMessageBox::Yes);
    if(resBtn == QMessageBox::Yes){
        QString openfile_name  = QString("%1%2").arg("file:///").arg(save_filepath);
        QDesktopServices::openUrl(openfile_name);
    }
}

void NISwrite_widget::on_print_btn_clicked()
{
    QPrinter printer(QPrinter::HighResolution);
    printer.setFullPage(true);
    QPrintDialog printDialog(&printer);
    printDialog.setEnabledOptions(QAbstractPrintDialog::PrintPageRange);
    printDialog.setPrintRange(QAbstractPrintDialog::AllPages);
    printDialog.setWindowTitle(tr("Printer"));

    if (printDialog.exec() == QDialog::Accepted) {
        content_edit->print(&printer);
    }

}
