#include "bprojectboard.h"
#include "ui_bprojectboard.h"
#include <eismain.h>
BprojectBoard::BprojectBoard(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BprojectBoard)
{
    ui->setupUi(this);
}

BprojectBoard::~BprojectBoard()
{
    delete ui;
}

void BprojectBoard::on_infrom_entet_btn_clicked()
{
    EISmain *eismain = new EISmain("BPROJECT");
    eismain->show();
}

void BprojectBoard::on_NIS_btn_clicked()
{
    NISmainwindow *niswidget = new NISmainwindow("BPROJECT");
    niswidget->show();
}
