#include "cost_reduction_limit_view.h"
#include "ui_cost_reduction_limit_view.h"

Cost_Reduction_limit_view::Cost_Reduction_limit_view(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Cost_Reduction_limit_view)
{
    ui->setupUi(this);
}
Cost_Reduction_limit_view::Cost_Reduction_limit_view(QString process_name, QDate init_year
                                                     , QSqlDatabase &my_mes_db, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Cost_Reduction_limit_view)
{
    ui->setupUi(this);
    this->process_name = process_name;
    this->init_year = init_year;
    this->my_mes_db = my_mes_db;
    this->setWindowTitle(process_name);
    ui->search_date->setDate(init_year);
    this->my_mes_db = my_mes_db;
    QSqlQuery query(my_mes_db);
    query.exec(QString("select * from Cost_reduction_money_limit where process = '%1' AND month between '%2' AND '%3'")
               .arg(process_name).arg(init_year.toString("yyyy-01-01")).arg(init_year.toString("yyyy-12-31")));

    tablemoel = new Cost_Redction_data_table_model("limit_table",this,my_mes_db);

    tablemoel->setTable("Cost_reduction_money_limit");
    tablemoel->setFilter(QString("process = '%1' AND month between '%2' AND '%3'")
                         .arg(process_name).arg(init_year.toString("yyyy-01-01")).arg(init_year.toString("yyyy-12-31")));
    ui->limit_table->setModel(tablemoel);
    ui->limit_table->setSortingEnabled(true);
    tablemoel->select();
    tablemoel->setHeaderData(0,Qt::Horizontal,tr("process"));
    tablemoel->setHeaderData(1,Qt::Horizontal,tr("month"));
    tablemoel->setHeaderData(2,Qt::Horizontal,tr("limit_price"));
    tablemoel->sort(1,Qt::AscendingOrder);
    tablemoel->setEditStrategy(QSqlTableModel::OnFieldChange);


}

Cost_Reduction_limit_view::~Cost_Reduction_limit_view()
{
    delete ui;
}

void Cost_Reduction_limit_view::on_search_btn_clicked()
{
    tablemoel->setFilter(QString("process = '%1' AND month between '%2' AND '%3'")
                         .arg(process_name).arg(ui->search_date->date().toString("yyyy-01-01")).arg(ui->search_date->date().toString("yyyy-12-31")));
}

void Cost_Reduction_limit_view::on_add_row_btn_clicked()
{
    tablemoel->insertRow(tablemoel->rowCount());
    tablemoel->setData(tablemoel->index(tablemoel->rowCount()-1,0),process_name);
    tablemoel->setData(tablemoel->index(tablemoel->rowCount()-1,1),QDate::currentDate());

}

void Cost_Reduction_limit_view::on_del_row_btn_clicked()
{
    int count = ui->limit_table->selectionModel()->selectedIndexes().count();
    for(int i=0;i<count;i++){
        tablemoel->removeRow(ui->limit_table->selectionModel()->selectedIndexes().at(i).row());
    }
    tablemoel->select();
}
