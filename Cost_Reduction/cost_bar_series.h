#ifndef COST_BAR_SERIES_H
#define COST_BAR_SERIES_H
#include <QObject>
#include <QWidget>

#include <QtCharts/QChartView>
#include <QtWidgets/QRubberBand>
#include <QDebug>
#include <QPointF>
#include <QBarSeries>
QT_CHARTS_USE_NAMESPACE

class Cost_bar_Series : public QBarSeries
{
    Q_OBJECT
public:
    Cost_bar_Series();

};

#endif // COST_BAR_SERIES_H
