#ifndef COST_REDUECTION_LIMIT_MONEY_MODEL_H
#define COST_REDUECTION_LIMIT_MONEY_MODEL_H
#include <QObject>
#include <QWidget>
#include <QSqlTableModel>
#include <QDebug>
#include <QModelIndex>
#include <QSqlRecord>
#include <QSqlField>
#include <QDateTime>

class Cost_reduection_limit_money_model :public QSqlTableModel
{
    Q_OBJECT
public:
    Cost_reduection_limit_money_model(QObject *parent,QSqlDatabase db);
};

#endif // COST_REDUECTION_LIMIT_MONEY_MODEL_H
