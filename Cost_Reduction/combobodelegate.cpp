#include "combobodelegate.h"

ComboboDelegate::ComboboDelegate(QStringList &list, QWidget *parent): QItemDelegate(parent)
{
    this->list = list;
}

QWidget *ComboboDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    comboxedit = new QComboBox(parent);
    for(int i=0;i<list.count();i++){
        comboxedit->addItem(list.at(i));
    }
    QObject::connect(comboxedit,SIGNAL(currentTextChanged(QString)),this,SLOT(setData(QString)));

    return comboxedit;
}

void ComboboDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QVariant data = index.model()->data( index, Qt::DisplayRole );

    (static_cast<QComboBox*>( editor ))->setCurrentText(data.toString());

}

void ComboboDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    model->setData( index, static_cast<QComboBox*>( editor )->currentText() );

}

void ComboboDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry( option.rect );
}

void ComboboDelegate::setData(QString val)
{
    emit commitData(comboxedit);
}
