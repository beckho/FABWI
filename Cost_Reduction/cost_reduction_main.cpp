#include "cost_reduction_main.h"
#include "ui_cost_reduction_main.h"

cost_reduction_main::cost_reduction_main(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::cost_reduction_main)
{
    ui->setupUi(this);
    QString mydb_name = QString("MY_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    QString server_ip = settings.value("serverip").toString();
    QString db_port = settings.value("dbport").toString();
    settings.endGroup();
    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }
    data_table_edit_mode = false;
    share_list<<tr("PR")<<tr("develop")<<tr("strip")<<tr("etc")<<tr("Clean_ware")<<tr("Consumable_supplies")<<tr("stationery");
    classification_list<<tr("Maintenance Costs")<<tr("consumables")
                        <<tr("Spare Parts")<<tr("PM Parts")
                        <<tr("Tool(tool)")<<tr("Warranty")<<tr("Subpart(gasetc)")<<tr("etcpart");
    MROList<<""<<tr("MRO_1")<<tr("MRO_2");
    Inout_split_list<<""<<tr("inout_split_in")<<tr("inout_split_out");
    tab_1_init();
    tab_2_init();
    tab_3_init();
    tab_4_init();
    tab_5_init();
    tab_6_init();
}

cost_reduction_main::~cost_reduction_main()
{
    delete ui;
}

void cost_reduction_main::tab_1_init()
{
    all_total_chart = 0;
    all_total_chart_view = 0;
    all_detail_chart =0;
    all_total_chart_view = 0;
    ui->all_search_month_end->setDate(QDate::currentDate());
    ui->all_search_month_start->setDate(QDate(QDate::currentDate().year(),QDate::currentDate().month(),1));
    Cost_Redction_data_table_model *data_model  = new Cost_Redction_data_table_model("all_search_month",this,my_mesdb);
    ui->all_data_table->setModel(data_model);
    ui->all_data_table->setSortingEnabled(true);

    data_model->setTable("Cost_reduction_data");

    data_model->setFilter(QString("buy_time between '%1' AND '%2' AND `process` IN ('%3','%4','%5','%6')")
                          .arg(ui->all_search_month_start->date().toString("yyyy-MM-01"))
                          .arg(ui->all_search_month_end->date().toString("yyyy-MM-31"))
                          .arg(tr("light")).arg(tr("deposition")).arg(tr("eatching")).arg(tr("probe")));

    data_model->select();
    qDebug()<<data_model->query().lastQuery();
    data_model->setHeaderData(0,Qt::Horizontal,tr("buy_time"));
    data_model->setHeaderData(1,Qt::Horizontal,tr("process"));
    data_model->setHeaderData(2,Qt::Horizontal,tr("machine_name"));
    data_model->setHeaderData(3,Qt::Horizontal,tr("name_of_goods"));
    data_model->setHeaderData(4,Qt::Horizontal,tr("model"));
    data_model->setHeaderData(5,Qt::Horizontal,tr("classification"));
    data_model->setHeaderData(6,Qt::Horizontal,tr("unit_1"));
    data_model->setHeaderData(7,Qt::Horizontal,tr("order_amount"));
    data_model->setHeaderData(8,Qt::Horizontal,tr("quantity_provision"));
    data_model->setHeaderData(9,Qt::Horizontal,tr("monthly_amount_used"));
    data_model->setHeaderData(10,Qt::Horizontal,tr("unit price"));
    data_model->setHeaderData(11,Qt::Horizontal,tr("price"));
    data_model->setHeaderData(12,Qt::Horizontal,tr("shop"));
    data_model->setHeaderData(13,Qt::Horizontal,tr("person_in_charge"));
    data_model->setHeaderData(14,Qt::Horizontal,tr("person_in_charge_phone"));
    data_model->setHeaderData(15,Qt::Horizontal,tr("draft"));
    data_model->setHeaderData(16,Qt::Horizontal,tr("draft_calculate"));
    data_model->setHeaderData(17,Qt::Horizontal,tr("input_check"));
    data_model->setHeaderData(18,Qt::Horizontal,tr("MRO"));
    data_model->setHeaderData(19,Qt::Horizontal,tr("note"));
    data_model->setHeaderData(21,Qt::Horizontal,tr("inout_split"));
    data_model->setEditStrategy(QSqlTableModel::OnFieldChange);

    ui->all_data_table->sortByColumn(0,Qt::DescendingOrder);
    ui->all_data_table->horizontalHeader()->resizeSection(0,150);
    ui->all_data_table->horizontalHeader()->hideSection(20);
    ui->all_data_table->horizontalHeader()->hideSection(17);
    ui->all_data_table->horizontalHeader()->hideSection(2);
    ui->all_data_table->horizontalHeader()->hideSection(8);
    ui->all_data_table->horizontalHeader()->hideSection(9);
//    ui->all_data_table->horizontalHeader()->hideSection(12);
//    ui->all_data_table->horizontalHeader()->hideSection(13);
//    ui->all_data_table->horizontalHeader()->hideSection(14);
    connect(data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),
            this,SLOT(all_auto_calc_total_price(QModelIndex,QModelIndex,QVector<int>)));

    QStringList all_list;
    all_list.append(tr("deposition"));
    all_list.append(tr("light"));
    all_list.append(tr("eatching"));
    all_list.append(tr("probe"));
    ui->all_data_table->setItemDelegateForColumn(0,new Cost_Datedelegate(ui->all_data_table));
    ui->all_data_table->setItemDelegateForColumn(1,new ComboboDelegate(all_list,ui->all_data_table));
    ui->all_data_table->setItemDelegateForColumn(5,new ComboboDelegate(classification_list,ui->all_data_table));
    ui->all_data_table->setItemDelegateForColumn(18,new ComboboDelegate(MROList,ui->all_data_table));
    ui->all_data_table->setItemDelegateForColumn(21,new ComboboDelegate(Inout_split_list,ui->all_data_table));

    on_all_refresh_btn_clicked();


}

void cost_reduction_main::tab_2_init()
{
    deposition_total_chart = 0;
    deposition_total_chart_view = 0;
    deposition_detail_chart =0;
    deposition_total_chart_view = 0;
    ui->deposition_search_month_end->setDate(QDate::currentDate());
    ui->deposition_search_month_start->setDate(QDate(QDate::currentDate().year(),QDate::currentDate().month(),1));
    Cost_Redction_data_table_model *data_model  = new Cost_Redction_data_table_model("deposition_search_month",this,my_mesdb);

    ui->deposition_data_table->setModel(data_model);
    ui->deposition_data_table->setSortingEnabled(true);
    data_model->setTable("Cost_reduction_data");

    data_model->setFilter(QString("process = '%1' AND buy_time between '%2' AND '%3' ").arg(tr("deposition"))
                          .arg(ui->deposition_search_month_start->date().toString("yyyy-MM-01"))
                          .arg(ui->deposition_search_month_end->date().toString("yyyy-MM-31")));

    data_model->select();
    data_model->setHeaderData(0,Qt::Horizontal,tr("buy_time"));
    data_model->setHeaderData(1,Qt::Horizontal,tr("process"));
    data_model->setHeaderData(2,Qt::Horizontal,tr("machine_name"));
    data_model->setHeaderData(3,Qt::Horizontal,tr("name_of_goods"));
    data_model->setHeaderData(4,Qt::Horizontal,tr("model"));
    data_model->setHeaderData(5,Qt::Horizontal,tr("classification"));
    data_model->setHeaderData(6,Qt::Horizontal,tr("unit_1"));
    data_model->setHeaderData(7,Qt::Horizontal,tr("order_amount"));
    data_model->setHeaderData(8,Qt::Horizontal,tr("quantity_provision"));
    data_model->setHeaderData(9,Qt::Horizontal,tr("monthly_amount_used"));
    data_model->setHeaderData(10,Qt::Horizontal,tr("unit price"));
    data_model->setHeaderData(11,Qt::Horizontal,tr("price"));
    data_model->setHeaderData(12,Qt::Horizontal,tr("shop"));
    data_model->setHeaderData(13,Qt::Horizontal,tr("person_in_charge"));
    data_model->setHeaderData(14,Qt::Horizontal,tr("person_in_charge_phone"));
    data_model->setHeaderData(15,Qt::Horizontal,tr("draft"));
    data_model->setHeaderData(16,Qt::Horizontal,tr("draft_calculate"));
    data_model->setHeaderData(17,Qt::Horizontal,tr("input_check"));
    data_model->setHeaderData(18,Qt::Horizontal,tr("MRO"));
    data_model->setHeaderData(19,Qt::Horizontal,tr("note"));
    data_model->setHeaderData(21,Qt::Horizontal,tr("inout_split"));
    data_model->setEditStrategy(QSqlTableModel::OnFieldChange);

    ui->deposition_data_table->sortByColumn(0,Qt::DescendingOrder);
    ui->deposition_data_table->horizontalHeader()->resizeSection(0,150);
    ui->deposition_data_table->horizontalHeader()->hideSection(20);
    ui->deposition_data_table->horizontalHeader()->hideSection(17);
    ui->deposition_data_table->horizontalHeader()->hideSection(2);
    ui->deposition_data_table->horizontalHeader()->hideSection(8);
    ui->deposition_data_table->horizontalHeader()->hideSection(9);
//    ui->deposition_data_table->horizontalHeader()->hideSection(12);
//    ui->deposition_data_table->horizontalHeader()->hideSection(13);
//    ui->deposition_data_table->horizontalHeader()->hideSection(14);
    connect(data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),
            this,SLOT(deposition_auto_calc_total_price(QModelIndex,QModelIndex,QVector<int>)));
    QStringList depostion_list;
    depostion_list.append(tr("deposition"));
    ui->deposition_data_table->setItemDelegateForColumn(0,new Cost_Datedelegate(ui->deposition_data_table));
    ui->deposition_data_table->setItemDelegateForColumn(1,new ComboboDelegate(depostion_list,ui->deposition_data_table));
    ui->deposition_data_table->setItemDelegateForColumn(5,new ComboboDelegate(classification_list,ui->deposition_data_table));
    ui->deposition_data_table->setItemDelegateForColumn(18,new ComboboDelegate(MROList,ui->deposition_data_table));
    ui->deposition_data_table->setItemDelegateForColumn(21,new ComboboDelegate(Inout_split_list,ui->deposition_data_table));
    on_deposition_refresh_btn_clicked();

}

void cost_reduction_main::tab_3_init()
{
    light_total_chart = 0;
    light_total_chart_view = 0;
    light_detail_chart =0;
    light_detail_chart_view = 0;
    ui->light_search_month_end->setDate(QDate::currentDate());
    ui->light_search_month_start->setDate(QDate(QDate::currentDate().year(),QDate::currentDate().month(),1));
    Cost_Redction_data_table_model *data_model  = new Cost_Redction_data_table_model("light_search_month",this,my_mesdb);

    ui->light_data_table->setModel(data_model);
    ui->light_data_table->setSortingEnabled(true);
    data_model->setTable("Cost_reduction_data");

    data_model->setFilter(QString("process = '%1' AND buy_time between '%2' AND '%3' ").arg(tr("light"))
                          .arg(ui->light_search_month_start->date().toString("yyyy-MM-01"))
                          .arg(ui->light_search_month_end->date().toString("yyyy-MM-31")));

    data_model->select();
    data_model->setHeaderData(0,Qt::Horizontal,tr("buy_time"));
    data_model->setHeaderData(1,Qt::Horizontal,tr("process"));
    data_model->setHeaderData(2,Qt::Horizontal,tr("machine_name"));
    data_model->setHeaderData(3,Qt::Horizontal,tr("name_of_goods"));
    data_model->setHeaderData(4,Qt::Horizontal,tr("model"));
    data_model->setHeaderData(5,Qt::Horizontal,tr("classification"));
    data_model->setHeaderData(6,Qt::Horizontal,tr("unit_1"));
    data_model->setHeaderData(7,Qt::Horizontal,tr("order_amount"));
    data_model->setHeaderData(8,Qt::Horizontal,tr("quantity_provision"));
    data_model->setHeaderData(9,Qt::Horizontal,tr("monthly_amount_used"));
    data_model->setHeaderData(10,Qt::Horizontal,tr("unit price"));
    data_model->setHeaderData(11,Qt::Horizontal,tr("price"));
    data_model->setHeaderData(12,Qt::Horizontal,tr("shop"));
    data_model->setHeaderData(13,Qt::Horizontal,tr("person_in_charge"));
    data_model->setHeaderData(14,Qt::Horizontal,tr("person_in_charge_phone"));
    data_model->setHeaderData(15,Qt::Horizontal,tr("draft"));
    data_model->setHeaderData(16,Qt::Horizontal,tr("draft_calculate"));
    data_model->setHeaderData(17,Qt::Horizontal,tr("input_check"));
    data_model->setHeaderData(18,Qt::Horizontal,tr("MRO"));
    data_model->setHeaderData(19,Qt::Horizontal,tr("note"));
    data_model->setHeaderData(21,Qt::Horizontal,tr("inout_split"));
    data_model->setEditStrategy(QSqlTableModel::OnFieldChange);

    ui->light_data_table->sortByColumn(0,Qt::DescendingOrder);
    ui->light_data_table->horizontalHeader()->resizeSection(0,150);
    ui->light_data_table->horizontalHeader()->hideSection(20);
    ui->light_data_table->horizontalHeader()->hideSection(17);
    ui->light_data_table->horizontalHeader()->hideSection(2);
    ui->light_data_table->horizontalHeader()->hideSection(8);
    ui->light_data_table->horizontalHeader()->hideSection(9);
//    ui->light_data_table->horizontalHeader()->hideSection(12);
//    ui->light_data_table->horizontalHeader()->hideSection(13);
//    ui->light_data_table->horizontalHeader()->hideSection(14);
    connect(data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),
            this,SLOT(light_auto_calc_total_price(QModelIndex,QModelIndex,QVector<int>)));
    QStringList light_list;
    light_list.append(tr("light"));
    ui->light_data_table->setItemDelegateForColumn(0,new Cost_Datedelegate(ui->light_data_table));
    ui->light_data_table->setItemDelegateForColumn(1,new ComboboDelegate(light_list,ui->light_data_table));
    ui->light_data_table->setItemDelegateForColumn(5,new ComboboDelegate(classification_list,ui->light_data_table));
    ui->light_data_table->setItemDelegateForColumn(18,new ComboboDelegate(MROList,ui->light_data_table));
    ui->light_data_table->setItemDelegateForColumn(21,new ComboboDelegate(Inout_split_list,ui->light_data_table));

    on_light_refresh_btn_clicked();

}

void cost_reduction_main::tab_4_init()
{
    eatching_total_chart = 0;
    eatching_total_chart_view = 0;
    eatching_detail_chart =0;
    eatching_detail_chart_view = 0;
    ui->eatching_search_month_end->setDate(QDate::currentDate());
    ui->eatching_search_month_start->setDate(QDate(QDate::currentDate().year(),QDate::currentDate().month(),1));
    Cost_Redction_data_table_model *data_model  = new Cost_Redction_data_table_model("eatching_search_month",this,my_mesdb);

    ui->eatching_data_table->setModel(data_model);
    ui->eatching_data_table->setSortingEnabled(true);
    data_model->setTable("Cost_reduction_data");

    data_model->setFilter(QString("process = '%1' AND buy_time between '%2' AND '%3' ").arg(tr("eatching"))
                          .arg(ui->eatching_search_month_start->date().toString("yyyy-MM-01"))
                          .arg(ui->eatching_search_month_end->date().toString("yyyy-MM-31")));

    data_model->select();
    data_model->setHeaderData(0,Qt::Horizontal,tr("buy_time"));
    data_model->setHeaderData(1,Qt::Horizontal,tr("process"));
    data_model->setHeaderData(2,Qt::Horizontal,tr("machine_name"));
    data_model->setHeaderData(3,Qt::Horizontal,tr("name_of_goods"));
    data_model->setHeaderData(4,Qt::Horizontal,tr("model"));
    data_model->setHeaderData(5,Qt::Horizontal,tr("classification"));
    data_model->setHeaderData(6,Qt::Horizontal,tr("unit_1"));
    data_model->setHeaderData(7,Qt::Horizontal,tr("order_amount"));
    data_model->setHeaderData(8,Qt::Horizontal,tr("quantity_provision"));
    data_model->setHeaderData(9,Qt::Horizontal,tr("monthly_amount_used"));
    data_model->setHeaderData(10,Qt::Horizontal,tr("unit price"));
    data_model->setHeaderData(11,Qt::Horizontal,tr("price"));
    data_model->setHeaderData(12,Qt::Horizontal,tr("shop"));
    data_model->setHeaderData(13,Qt::Horizontal,tr("person_in_charge"));
    data_model->setHeaderData(14,Qt::Horizontal,tr("person_in_charge_phone"));
    data_model->setHeaderData(15,Qt::Horizontal,tr("draft"));
    data_model->setHeaderData(16,Qt::Horizontal,tr("draft_calculate"));
    data_model->setHeaderData(17,Qt::Horizontal,tr("input_check"));
    data_model->setHeaderData(18,Qt::Horizontal,tr("MRO"));
    data_model->setHeaderData(19,Qt::Horizontal,tr("note"));
    data_model->setHeaderData(21,Qt::Horizontal,tr("inout_split"));
    data_model->setEditStrategy(QSqlTableModel::OnFieldChange);

    ui->eatching_data_table->sortByColumn(0,Qt::DescendingOrder);
    ui->eatching_data_table->horizontalHeader()->resizeSection(0,150);
    ui->eatching_data_table->horizontalHeader()->hideSection(20);
    ui->eatching_data_table->horizontalHeader()->hideSection(17);
    ui->eatching_data_table->horizontalHeader()->hideSection(2);
    ui->eatching_data_table->horizontalHeader()->hideSection(8);
    ui->eatching_data_table->horizontalHeader()->hideSection(9);
    connect(data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),
            this,SLOT(eatching_auto_calc_total_price(QModelIndex,QModelIndex,QVector<int>)));
    QStringList eatching_list;
    eatching_list.append(tr("eatching"));
    ui->eatching_data_table->setItemDelegateForColumn(0,new Cost_Datedelegate(ui->eatching_data_table));
    ui->eatching_data_table->setItemDelegateForColumn(1,new ComboboDelegate(eatching_list,ui->eatching_data_table));
    ui->eatching_data_table->setItemDelegateForColumn(5,new ComboboDelegate(classification_list,ui->eatching_data_table));
    ui->eatching_data_table->setItemDelegateForColumn(18,new ComboboDelegate(MROList,ui->eatching_data_table));
     ui->eatching_data_table->setItemDelegateForColumn(21,new ComboboDelegate(Inout_split_list,ui->eatching_data_table));
    on_eatching_refresh_btn_clicked();

}

void cost_reduction_main::tab_5_init()
{
    probe_total_chart = 0;
    probe_total_chart_view = 0;
    probe_detail_chart =0;
    probe_detail_chart_view = 0;
    ui->probe_search_month_end->setDate(QDate::currentDate());
    ui->probe_search_month_start->setDate(QDate(QDate::currentDate().year(),QDate::currentDate().month(),1));
    Cost_Redction_data_table_model *data_model  = new Cost_Redction_data_table_model("probe_search_month",this,my_mesdb);

    ui->probe_data_table->setModel(data_model);
    ui->probe_data_table->setSortingEnabled(true);
    data_model->setTable("Cost_reduction_data");

    data_model->setFilter(QString("process = '%1' AND buy_time between '%2' AND '%3' ").arg(tr("probe"))
                          .arg(ui->probe_search_month_start->date().toString("yyyy-MM-01"))
                          .arg(ui->probe_search_month_end->date().toString("yyyy-MM-31")));

    data_model->select();
    data_model->setHeaderData(0,Qt::Horizontal,tr("buy_time"));
    data_model->setHeaderData(1,Qt::Horizontal,tr("process"));
    data_model->setHeaderData(2,Qt::Horizontal,tr("machine_name"));
    data_model->setHeaderData(3,Qt::Horizontal,tr("name_of_goods"));
    data_model->setHeaderData(4,Qt::Horizontal,tr("model"));
    data_model->setHeaderData(5,Qt::Horizontal,tr("classification"));
    data_model->setHeaderData(6,Qt::Horizontal,tr("unit_1"));
    data_model->setHeaderData(7,Qt::Horizontal,tr("order_amount"));
    data_model->setHeaderData(8,Qt::Horizontal,tr("quantity_provision"));
    data_model->setHeaderData(9,Qt::Horizontal,tr("monthly_amount_used"));
    data_model->setHeaderData(10,Qt::Horizontal,tr("unit price"));
    data_model->setHeaderData(11,Qt::Horizontal,tr("price"));
    data_model->setHeaderData(12,Qt::Horizontal,tr("shop"));
    data_model->setHeaderData(13,Qt::Horizontal,tr("person_in_charge"));
    data_model->setHeaderData(14,Qt::Horizontal,tr("person_in_charge_phone"));
    data_model->setHeaderData(15,Qt::Horizontal,tr("draft"));
    data_model->setHeaderData(16,Qt::Horizontal,tr("draft_calculate"));
    data_model->setHeaderData(17,Qt::Horizontal,tr("input_check"));
    data_model->setHeaderData(18,Qt::Horizontal,tr("MRO"));
    data_model->setHeaderData(19,Qt::Horizontal,tr("note"));
    data_model->setHeaderData(21,Qt::Horizontal,tr("inout_split"));
    data_model->setEditStrategy(QSqlTableModel::OnFieldChange);

    ui->probe_data_table->sortByColumn(0,Qt::DescendingOrder);
    ui->probe_data_table->horizontalHeader()->resizeSection(0,150);
    ui->probe_data_table->horizontalHeader()->hideSection(20);
    ui->probe_data_table->horizontalHeader()->hideSection(17);
    ui->probe_data_table->horizontalHeader()->hideSection(2);
    ui->probe_data_table->horizontalHeader()->hideSection(8);
    ui->probe_data_table->horizontalHeader()->hideSection(9);
//    ui->probe_data_table->horizontalHeader()->hideSection(12);
//    ui->probe_data_table->horizontalHeader()->hideSection(13);
//    ui->probe_data_table->horizontalHeader()->hideSection(14);
    connect(data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),
            this,SLOT(probe_auto_calc_total_price(QModelIndex,QModelIndex,QVector<int>)));
    QStringList probe_list;
    probe_list.append(tr("probe"));
    ui->probe_data_table->setItemDelegateForColumn(0,new Cost_Datedelegate(ui->probe_data_table));
    ui->probe_data_table->setItemDelegateForColumn(1,new ComboboDelegate(probe_list,ui->probe_data_table));
    ui->probe_data_table->setItemDelegateForColumn(5,new ComboboDelegate(classification_list,ui->probe_data_table));
    ui->probe_data_table->setItemDelegateForColumn(18,new ComboboDelegate(MROList,ui->probe_data_table));
    ui->probe_data_table->setItemDelegateForColumn(21,new ComboboDelegate(Inout_split_list,ui->probe_data_table));
    on_probe_refresh_btn_clicked();

}

void cost_reduction_main::tab_6_init()
{
    share_total_chart = 0;
    share_total_chart_view = 0;
    share_detail_chart =0;
    share_detail_chart_view = 0;
    ui->share_search_month_end->setDate(QDate::currentDate());
    ui->share_search_month_start->setDate(QDate(QDate::currentDate().year(),QDate::currentDate().month(),1));
    Cost_Redction_data_table_model *data_model = new Cost_Redction_data_table_model("share_search_month",this,my_mesdb);
    data_model->setTable("Cost_reduction_data");
    ui->share_data_table->setModel(data_model);
    data_model->setFilter(QString("`process` = '%1' AND `buy_time` between '%2' AND '%3'").arg(tr("share"))
                          .arg(ui->share_search_month_start->date().toString("yyyy-MM-01"))
                          .arg(ui->share_search_month_start->date().toString("yyyy-MM-31"))
                          );
    data_model->select();
    data_model->setHeaderData(data_model->fieldIndex("buy_time"),Qt::Horizontal,tr("buy_time"));
    data_model->setHeaderData(data_model->fieldIndex("process"),Qt::Horizontal,tr("process"));
    data_model->setHeaderData(data_model->fieldIndex("names_of_goods"),Qt::Horizontal,tr("name_of_goods"));
    data_model->setHeaderData(data_model->fieldIndex("model"),Qt::Horizontal,tr("model"));
    data_model->setHeaderData(data_model->fieldIndex("classification"),Qt::Horizontal,tr("classification"));
    data_model->setHeaderData(data_model->fieldIndex("unit"),Qt::Horizontal,tr("unit_1"));
    data_model->setHeaderData(data_model->fieldIndex("order_amount"),Qt::Horizontal,tr("order_amount"));
    data_model->setHeaderData(data_model->fieldIndex("quantity_provision"),Qt::Horizontal,tr("quantity_provision"));
    data_model->setHeaderData(data_model->fieldIndex("monthly_amount_used"),Qt::Horizontal,tr("monthly_amount_used"));
    data_model->setHeaderData(data_model->fieldIndex("unit price"),Qt::Horizontal,tr("unit price"));
    data_model->setHeaderData(data_model->fieldIndex("price"),Qt::Horizontal,tr("price"));
    data_model->setHeaderData(data_model->fieldIndex("shop"),Qt::Horizontal,tr("shop"));
    data_model->setHeaderData(data_model->fieldIndex("person_in_charge"),Qt::Horizontal,tr("person_in_charge"));
    data_model->setHeaderData(data_model->fieldIndex("person_in_charge_phone"),Qt::Horizontal,tr("person_in_charge_phone"));
    data_model->setHeaderData(data_model->fieldIndex("draft"),Qt::Horizontal,tr("draft"));
    data_model->setHeaderData(data_model->fieldIndex("draft_calculate"),Qt::Horizontal,tr("draft_calculate"));
    data_model->setHeaderData(data_model->fieldIndex("input_check"),Qt::Horizontal,tr("input_check"));
    data_model->setHeaderData(data_model->fieldIndex("MRO"),Qt::Horizontal,tr("MRO"));
    data_model->setHeaderData(data_model->fieldIndex("note"),Qt::Horizontal,tr("note"));
    data_model->setHeaderData(data_model->fieldIndex("inout_split"),Qt::Horizontal,tr("inout_split"));

    data_model->setEditStrategy(QSqlTableModel::OnFieldChange);


    ui->share_data_table->horizontalHeader()->hideSection(data_model->fieldIndex("machine_name"));
//    ui->share_data_table->horizontalHeader()->hideSection(data_model->fieldIndex("MRO"));
    ui->share_data_table->horizontalHeader()->hideSection(data_model->fieldIndex("draft_calculate"));
    ui->share_data_table->horizontalHeader()->hideSection(data_model->fieldIndex("draft"));
    ui->share_data_table->horizontalHeader()->hideSection(data_model->fieldIndex("process"));
    ui->share_data_table->horizontalHeader()->hideSection(data_model->fieldIndex("quantity_provision"));
    ui->share_data_table->horizontalHeader()->hideSection(data_model->fieldIndex("monthly_amount_used"));




    ui->share_data_table->setItemDelegateForColumn(data_model->fieldIndex("buy_time"),new Cost_Datedelegate(ui->share_data_table));
    ui->share_data_table->setItemDelegateForColumn(data_model->fieldIndex("classification"),
                                                 new ComboboDelegate(share_list,ui->share_data_table));
        ui->share_data_table->setItemDelegateForColumn(data_model->fieldIndex("MRO"),new ComboboDelegate(MROList,ui->share_data_table));

        ui->share_data_table->setItemDelegateForColumn(21,new ComboboDelegate(Inout_split_list,ui->share_data_table));


    connect(data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),
            this,SLOT(share_auto_calc_total_price(QModelIndex,QModelIndex,QVector<int>)));
    on_share_refresh_btn_clicked();


}

void cost_reduction_main::on_deposition_limit_money_btn_clicked()
{
    Cost_Reduction_limit_view *from  = new Cost_Reduction_limit_view(tr("deposition"),ui->deposition_search_month_end->date(),my_mesdb);
    from->show();
}


void cost_reduction_main::on_light_limit_money_btn_clicked()
{
    Cost_Reduction_limit_view *from  = new Cost_Reduction_limit_view(tr("light"),ui->light_search_month_end->date(),my_mesdb);
    from->show();
}

void cost_reduction_main::on_eatching_limit_money_btn_clicked()
{
    Cost_Reduction_limit_view *from  = new Cost_Reduction_limit_view(tr("eatching"),ui->eatching_search_month_end->date(),my_mesdb);
    from->show();
}
void cost_reduction_main::on_probe_limit_money_btn_clicked()
{
    Cost_Reduction_limit_view *from  = new Cost_Reduction_limit_view(tr("probe"),ui->eatching_search_month_end->date(),my_mesdb);
    from->show();
}

void cost_reduction_main::on_deposition_add_row_clicked()
{
    Cost_Redction_data_table_model *depostion_model = (Cost_Redction_data_table_model *)ui->deposition_data_table->model();
    depostion_model->insertRow(depostion_model->rowCount());

    depostion_model->setData(depostion_model->index(depostion_model->rowCount()-1,1),tr("deposition"));
    depostion_model->setData(depostion_model->index(depostion_model->rowCount()-1,5),tr("consumables"));
    depostion_model->setData(depostion_model->index(depostion_model->rowCount()-1,0),QDate::currentDate());
    depostion_model->submitAll();

}
void cost_reduction_main::on_light_add_row_clicked()
{
    Cost_Redction_data_table_model *light_model = (Cost_Redction_data_table_model *)ui->light_data_table->model();
    light_model->insertRow(light_model->rowCount());
    light_model->setData(light_model->index(light_model->rowCount()-1,1),tr("light"));
    light_model->setData(light_model->index(light_model->rowCount()-1,5),tr("consumables"));
    light_model->setData(light_model->index(light_model->rowCount()-1,0),QDate::currentDate());
    light_model->submitAll();
}

void cost_reduction_main::on_eatching__add_row_clicked()
{
    Cost_Redction_data_table_model *eatching_model = (Cost_Redction_data_table_model *)ui->eatching_data_table->model();
    eatching_model->insertRow(eatching_model->rowCount());
    eatching_model->setData(eatching_model->index(eatching_model->rowCount()-1,5),tr("consumables"));
    eatching_model->setData(eatching_model->index(eatching_model->rowCount()-1,1),tr("eatching"));
    eatching_model->setData(eatching_model->index(eatching_model->rowCount()-1,0),QDate::currentDate());
    eatching_model->submitAll();
}
void cost_reduction_main::on_probe__add_row_clicked()
{
    Cost_Redction_data_table_model *probe_model = (Cost_Redction_data_table_model *)ui->probe_data_table->model();
    probe_model->insertRow(probe_model->rowCount());
    probe_model->setData(probe_model->index(probe_model->rowCount()-1,5),tr("consumables"));
    probe_model->setData(probe_model->index(probe_model->rowCount()-1,1),tr("probe"));
    probe_model->setData(probe_model->index(probe_model->rowCount()-1,0),QDate::currentDate());
    probe_model->submitAll();
}


void cost_reduction_main::on_deposition_del_row_clicked()
{
    Cost_Redction_data_table_model *depostion_model = (Cost_Redction_data_table_model *)ui->deposition_data_table->model();
    int count = ui->deposition_data_table->selectionModel()->selectedIndexes().count();
    depostion_model->record().setGenerated("idx",true);
    for(int i=0;i<count;i++){
        depostion_model->removeRow(ui->deposition_data_table->selectionModel()->selectedIndexes().at(i).row());
    }
     depostion_model->select();
}

void cost_reduction_main::on_light_del_row_clicked()
{
    Cost_Redction_data_table_model *light_model = (Cost_Redction_data_table_model *)ui->light_data_table->model();
    int count = ui->light_data_table->selectionModel()->selectedIndexes().count();

    for(int i=0;i<count;i++){
        light_model->removeRow(ui->light_data_table->selectionModel()->selectedIndexes().at(i).row());
    }
     light_model->select();
}

void cost_reduction_main::on_eatching__del_row_clicked()
{
    Cost_Redction_data_table_model *eatching_model = (Cost_Redction_data_table_model *)ui->eatching_data_table->model();
    int count = ui->eatching_data_table->selectionModel()->selectedIndexes().count();

    for(int i=0;i<count;i++){
        eatching_model->removeRow(ui->eatching_data_table->selectionModel()->selectedIndexes().at(i).row());
    }
     eatching_model->select();
}
void cost_reduction_main::on_probe__del_row_clicked()
{
    Cost_Redction_data_table_model *probe_model = (Cost_Redction_data_table_model *)ui->probe_data_table->model();
    int count = ui->probe_data_table->selectionModel()->selectedIndexes().count();

    for(int i=0;i<count;i++){
        probe_model->removeRow(ui->probe_data_table->selectionModel()->selectedIndexes().at(i).row());
    }
     probe_model->select();
}

void cost_reduction_main::on_deposition_refresh_btn_clicked()
{
    Cost_Redction_data_table_model *depostion_model = (Cost_Redction_data_table_model *)ui->deposition_data_table->model();
    depostion_model->setFilter(QString("process = '%1' AND buy_time between '%2' AND '%3' ").arg(tr("deposition"))
                          .arg(ui->deposition_search_month_start->date().toString("yyyy-MM-01"))
                          .arg(ui->deposition_search_month_end->date().toString("yyyy-MM-31")));

    depostion_model->select();
    depostion_model->sort(0,Qt::DescendingOrder);
    qint64 total_limit_money=0;
    qint64 total_Maintenance_Costs = 0;
    qint64 total_consumables = 0;
    qint64 total_Spare_Parts = 0;
    qint64 total_PM_Parts = 0;
    qint64 total_tool = 0;
    qint64 total_Warranty = 0;
    qint64 total_Subpart = 0;
    qint64 total_etcpart = 0;

    QSqlQuery query(my_mesdb);
    QString draft_str = "";
    if(ui->depostion_draft->isChecked()){
        draft_str = "draft";
    }else if(ui->depostion_draft_calc->isChecked()){
        draft_str = "draft_calculate";
    }

    query.exec(QString("select SUM(limit_cost) as cost_sum from Cost_reduction_money_limit "
              "where `process` = '%1' AND `month` between '%2' AND '%3'").arg(tr("deposition"))
              .arg(ui->deposition_search_month_start->date().toString("yyyy-MM-01"))
              .arg(ui->deposition_search_month_end->date().toString("yyyy-MM-31")));
    if(query.next()){
        total_limit_money = query.value("cost_sum").toReal();
    }
    query.exec(QString("select sum(CASE WHEN `classification` = '%1' THEN price END) as Maintenance_Costs, "
                       "sum(CASE WHEN `classification` = '%2' THEN price END) as consumables,"
                       "sum(CASE WHEN `classification` = '%3' THEN price END) as Spare_Parts, "
                       "sum(CASE WHEN `classification` = '%4' THEN price END) as PM_Parts, "
                       "sum(CASE WHEN `classification` = '%5' THEN price END) as Tool, "
                       "sum(CASE WHEN `classification` = '%6' THEN price END) as Warranty, "
                       "sum(CASE WHEN `classification` = '%7' THEN price END) as Subpart, "
                       "sum(CASE WHEN `classification` = '%8' THEN price END) as etcpart "
                       "from Cost_reduction_data where `process` = '%9' AND buy_time between '%10' AND '%11' AND %12 = 2")
               .arg(tr("Maintenance Costs")).arg(tr("consumables"))
               .arg(tr("Spare Parts")).arg(tr("PM Parts")).arg(tr("Tool(tool)")).arg(tr("Warranty"))
               .arg(tr("Subpart(gasetc)")).arg(tr("etcpart"))
               .arg(tr("deposition"))
               .arg(ui->deposition_search_month_start->date().toString("yyyy-MM-01"))
               .arg(ui->deposition_search_month_end->date().toString("yyyy-MM-31"))
               .arg(draft_str));
    qDebug()<<query.lastQuery();
    qDebug()<<query.lastError().text();
    if(query.next()){
        total_Maintenance_Costs = query.value("Maintenance_Costs").toLongLong();
        total_consumables = query.value("consumables").toLongLong();
        total_Spare_Parts = query.value("Spare_Parts").toLongLong();
        total_PM_Parts = query.value("PM_Parts").toLongLong();
        total_tool = query.value("Tool").toLongLong();
        total_Warranty = query.value("Warranty").toLongLong();
        total_Subpart = query.value("Subpart").toLongLong();
        total_etcpart = query.value("etcpart").toLongLong();
    }
    if(deposition_total_chart == 0){
        deposition_total_chart = new Cost_Reduction_chart();
        deposition_total_chart_view = new Cost_Reduction_chart_view(deposition_total_chart);
        ui->deposition_total_chart_layuot->addWidget(deposition_total_chart_view);
    }else {
        deposition_total_chart->removeAllSeries();
        deposition_total_chart->removeAxis(deposition_total_chart->axisX());
        deposition_total_chart->removeAxis(deposition_total_chart->axisY());
    }

    QStackedBarSeries *total_bar_series = new QStackedBarSeries();
    total_bar_series->setLabelsFormat("@comma_long_value");

//    QBarSet *limit_total_remain_bar_set = new QBarSet(tr("total_remain_limit"));
//    limit_total_remain_bar_set->setLabelColor(QColor("black"));
//    limit_total_remain_bar_set->setPen(QColor("black"));
//    limit_total_remain_bar_set->setColor(QColor(Qt::transparent));

    QBarSet *total_Maintenance_Costs_bar_set = new QBarSet(tr("Maintenance Costs"));
    total_Maintenance_Costs_bar_set->setLabelColor(QColor("black"));

    QBarSet *total_consumables_bar_set = new QBarSet(tr("consumables"));
    total_consumables_bar_set->setLabelColor("black");

    QBarSet *total_environment_improvement_bar_set = new QBarSet(tr("environment improvement"));
    total_environment_improvement_bar_set->setLabelColor("black");

    QBarSet *total_Spare_Parts_bar_set = new QBarSet(tr("Spare Parts"));
    total_Spare_Parts_bar_set->setLabelColor("black");

    QBarSet *total_PM_Parts_bar_set = new QBarSet(tr("PM Parts"));
    total_PM_Parts_bar_set->setLabelColor("black");

    QBarSet *total_TOOL_bar_set = new QBarSet(tr("Tool(tool)"));
    total_TOOL_bar_set->setLabelColor("black");

    QBarSet *total_Warranty_bar_set = new QBarSet(tr("Warranty"));
    total_Warranty_bar_set->setLabelColor("black");

    QBarSet *total_Subpart_bar_set = new QBarSet(tr("Subpart(gasetc)"));
    total_Subpart_bar_set->setLabelColor("black");

    QBarSet *total_etcpart_bar_set = new QBarSet(tr("etcpart"));
    total_etcpart_bar_set->setLabelColor("black");

    total_bar_series->setLabelsVisible(true);

    qint64 remain_money = total_limit_money - (total_Maintenance_Costs+total_consumables+
                                                            total_Spare_Parts+
                                                          total_PM_Parts+total_tool+total_Warranty+total_Subpart+total_etcpart);
//    limit_total_remain_bar_set->append(remain_money);
    total_Maintenance_Costs_bar_set->append(total_Maintenance_Costs);
    total_consumables_bar_set->append(total_consumables);
    total_Spare_Parts_bar_set->append(total_Spare_Parts);
    total_PM_Parts_bar_set->append(total_PM_Parts);
    total_TOOL_bar_set->append(total_tool);
    total_Warranty_bar_set->append(total_Warranty);
    total_Subpart_bar_set->append(total_Subpart);
    total_etcpart_bar_set->append(total_etcpart);

    total_bar_series->append(total_Spare_Parts_bar_set);
    total_bar_series->append(total_PM_Parts_bar_set);
    total_bar_series->append(total_TOOL_bar_set);
    total_bar_series->append(total_Warranty_bar_set);
    total_bar_series->append(total_Subpart_bar_set);
    total_bar_series->append(total_etcpart_bar_set);
    total_bar_series->append(total_consumables_bar_set);
    total_bar_series->append(total_Maintenance_Costs_bar_set);
//    total_bar_series->append(limit_total_remain_bar_set);


    deposition_total_chart->addSeries(total_bar_series);
    QStringList total_categories;
    total_categories << "total";
    QBarCategoryAxis *total_axis = new QBarCategoryAxis();
    total_axis->append(total_categories);
    deposition_total_chart->createDefaultAxes();
    deposition_total_chart->setAxisX(total_axis, total_bar_series);

    qreal total_max_yvalue = ((QValueAxis *)deposition_total_chart->axisY())->max();
    qreal total_min_yvalue = ((QValueAxis *)deposition_total_chart->axisY())->min();
    qreal total_diff_value = total_max_yvalue - total_min_yvalue;
    deposition_total_chart->axisY()->setRange(total_min_yvalue-(total_diff_value*0.1),total_max_yvalue+(total_diff_value*0.1));


    connect(total_bar_series,SIGNAL(hovered(bool,int,QBarSet*)),this,SLOT(deposition_total_hover_slot(bool,int,QBarSet*)));

    if(deposition_detail_chart == 0){
        deposition_detail_chart = new Cost_Reduction_chart();
        deposition_detail_chart_view = new Cost_Reduction_chart_view(deposition_detail_chart);
        ui->deposition_detail_chart_layuot->addWidget(deposition_detail_chart_view);
    }else {
        deposition_detail_chart->removeAllSeries();
        deposition_detail_chart->removeAxis(deposition_detail_axis_X);
        deposition_detail_chart->removeAxis(deposition_detail_axis_Y);
    }


    QStackedBarSeries *detail_bar_series = new QStackedBarSeries();
    detail_bar_series->setLabelsFormat("@comma_long_value");
    detail_bar_series->setName("detail");
    detail_bar_series->setLabelsVisible(true);

//    QBarSet *limit_detail_remain_bar_set = new QBarSet(tr("month_remain_limit"));
//    limit_detail_remain_bar_set->setLabelColor(QColor("black"));
//    limit_detail_remain_bar_set->setPen(QColor("black"));
//    limit_detail_remain_bar_set->setColor(QColor(Qt::transparent));

    QBarSet *detail_Maintenance_Costs_bar_set = new QBarSet(tr("Maintenance Costs"));
    detail_Maintenance_Costs_bar_set->setLabelColor(QColor("black"));
    detail_Maintenance_Costs_bar_set->setColor(total_Maintenance_Costs_bar_set->color());

    QBarSet *detail_consumables_bar_set = new QBarSet(tr("consumables"));
    detail_consumables_bar_set->setLabelColor("black");
    detail_consumables_bar_set->setColor(total_consumables_bar_set->color());

    QBarSet *detail_Spare_Parts_bar_set = new QBarSet(tr("Spare Parts"));
    detail_Spare_Parts_bar_set->setLabelColor("black");
    detail_Spare_Parts_bar_set->setColor(total_Spare_Parts_bar_set->color());

    QBarSet *detail_PM_Parts_bar_set = new QBarSet(tr("PM Parts"));
    detail_PM_Parts_bar_set->setLabelColor("black");
    detail_PM_Parts_bar_set->setColor(total_PM_Parts_bar_set->color());

    QBarSet *detail_TOOL_bar_set = new QBarSet(tr("Tool(tool)"));
    detail_TOOL_bar_set->setLabelColor("black");
    detail_TOOL_bar_set->setColor(total_TOOL_bar_set->color());

    QBarSet *detail_Warranty_bar_set = new QBarSet(tr("Warranty"));
    detail_Warranty_bar_set->setLabelColor("black");
    detail_Warranty_bar_set->setColor(total_Warranty_bar_set->color());

    QBarSet *detail_Subpart_bar_set = new QBarSet(tr("Subpart(gasetc)"));
    detail_Subpart_bar_set->setLabelColor("black");
    detail_Subpart_bar_set->setColor(total_Subpart_bar_set->color());

    QBarSet *detail_etcpart_bar_set = new QBarSet(tr("etcpart"));
    detail_etcpart_bar_set->setLabelColor("black");
    detail_etcpart_bar_set->setColor(total_etcpart_bar_set->color());


    QStringList detail_categories;

    QLineSeries *detail_lineseries = new QLineSeries();
    detail_lineseries->setName(tr("use_price"));
    detail_lineseries->setPointsVisible(true);
//    detail_lineseries->setPointLabelsVisible(true);


    int linevalue_x = 0;

    int j=0;
    while(true){
        QDate start_date =  ui->deposition_search_month_start->date().addMonths(j);
        QDate end_date = ui->deposition_search_month_end->date();

        detail_categories.append(start_date.toString("yy-MM"));


        qint64 detail_limit_money=0;
        qint64 detail_Maintenance_Costs = 0;
        qint64 detail_consumables = 0;

        qint64 detail_Spare_Parts=0;
        qint64 detail_PM_Parts=0;
        qint64 detail_tool=0;
        qint64 detail_Warranty=0;
        qint64 detail_Subpart=0;
        qint64 detail_etcpart=0;




        query.exec(QString("select SUM(limit_cost) as cost_sum from Cost_reduction_money_limit "
                  "where `process` = '%1' AND `month` between '%2' AND '%3'").arg(tr("deposition"))
                  .arg(start_date.toString("yyyy-MM-01"))
                  .arg(start_date.toString("yyyy-MM-31")));
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError().text();
        if(query.next()){
            detail_limit_money = query.value("cost_sum").toLongLong();
        }
        query.exec(QString("select sum(CASE WHEN `classification` = '%1' THEN price END) as Maintenance_Costs, "
                           "sum(CASE WHEN `classification` = '%2' THEN price END) as consumables,"
                           "sum(CASE WHEN `classification` = '%3' THEN price END) as Spare_Parts, "
                           "sum(CASE WHEN `classification` = '%4' THEN price END) as PM_Parts, "
                           "sum(CASE WHEN `classification` = '%5' THEN price END) as Tool, "
                           "sum(CASE WHEN `classification` = '%6' THEN price END) as Warranty, "
                           "sum(CASE WHEN `classification` = '%7' THEN price END) as Subpart, "
                           "sum(CASE WHEN `classification` = '%8' THEN price END) as etcpart "
                           "from Cost_reduction_data where `process` = '%9' AND buy_time between '%10' AND '%11' AND %12 = 2  ")
                   .arg(tr("Maintenance Costs")).arg(tr("consumables"))
                   .arg(tr("Spare Parts")).arg(tr("PM Parts")).arg(tr("Tool(tool)")).arg(tr("Warranty"))
                   .arg(tr("Subpart(gasetc)")).arg(tr("etcpart"))
                   .arg(tr("deposition"))
                   .arg(start_date.toString("yyyy-MM-01"))
                   .arg(start_date.toString("yyyy-MM-31"))
                   .arg(draft_str));
        if(query.next()){
            detail_Maintenance_Costs = query.value("Maintenance_Costs").toLongLong();
            detail_consumables = query.value("consumables").toLongLong();
            detail_Spare_Parts = query.value("Spare_Parts").toLongLong();
            detail_PM_Parts = query.value("PM_Parts").toLongLong();
            detail_tool = query.value("Tool").toLongLong();
            detail_Warranty = query.value("Warranty").toLongLong();
            detail_Subpart = query.value("Subpart").toLongLong();
            detail_etcpart = query.value("etcpart").toLongLong();
        }
        qint64 detail_use_money  = detail_Maintenance_Costs+detail_consumables+
                                   detail_Spare_Parts+detail_PM_Parts+detail_tool+detail_Warranty+detail_Subpart+detail_etcpart;
        qint64 detail_remain_money = detail_limit_money - detail_use_money;

//        limit_detail_remain_bar_set->append(detail_remain_money);
        detail_Maintenance_Costs_bar_set->append(detail_Maintenance_Costs);
        detail_consumables_bar_set->append(detail_consumables);
        detail_Spare_Parts_bar_set->append(detail_Spare_Parts);
        detail_PM_Parts_bar_set->append(detail_PM_Parts);
        detail_TOOL_bar_set->append(detail_tool);
        detail_Warranty_bar_set->append(detail_Warranty);
        detail_Subpart_bar_set->append(detail_Subpart);
        detail_etcpart_bar_set->append(detail_etcpart);
        detail_lineseries->append(linevalue_x,detail_use_money);

        linevalue_x++;
        if((start_date.year() == end_date.year()) && (start_date.month() == end_date.month())){
            break;
        }
        j++;
    }
    detail_bar_series->append(detail_consumables_bar_set);
    detail_bar_series->append(detail_Maintenance_Costs_bar_set);
    detail_bar_series->append(detail_Spare_Parts_bar_set);
    detail_bar_series->append(detail_PM_Parts_bar_set);
    detail_bar_series->append(detail_TOOL_bar_set);
    detail_bar_series->append(detail_Warranty_bar_set);
    detail_bar_series->append(detail_Subpart_bar_set);
    detail_bar_series->append(detail_etcpart_bar_set);
//    detail_bar_series->append(limit_detail_remain_bar_set);


    detail_lineseries->setPointLabelsFormat("@yPoint");
    deposition_detail_chart->addSeries(detail_lineseries);
    deposition_detail_chart->addSeries(detail_bar_series);

    deposition_detail_axis_X = new QBarCategoryAxis();
    deposition_detail_axis_X->append(detail_categories);
    deposition_detail_chart->setAxisX(deposition_detail_axis_X, detail_bar_series);
    deposition_detail_chart->setAxisX(deposition_detail_axis_X,detail_lineseries);
    deposition_detail_axis_X->setRange(detail_categories.first(),detail_categories.last());
    deposition_detail_axis_Y= new QValueAxis();
    deposition_detail_chart->setAxisY(deposition_detail_axis_Y,detail_bar_series);
    deposition_detail_chart->setAxisY(deposition_detail_axis_Y,detail_lineseries);

    qreal max_yvalue = deposition_detail_axis_Y->max();
    qreal min_yvalue = deposition_detail_axis_Y->min();
    qreal diff_value = max_yvalue - min_yvalue;
    deposition_detail_axis_Y->setRange(min_yvalue-(diff_value*0.1),max_yvalue+(diff_value*0.1));
    connect(detail_bar_series,SIGNAL(hovered(bool,int,QBarSet*)),this,SLOT(deposition_detail_hover_slot(bool,int,QBarSet*)));
}


void cost_reduction_main::on_light_refresh_btn_clicked()
{
    Cost_Redction_data_table_model *light_model = (Cost_Redction_data_table_model *)ui->light_data_table->model();
    light_model->setFilter(QString("process = '%1' AND buy_time between '%2' AND '%3' ").arg(tr("light"))
                          .arg(ui->light_search_month_start->date().toString("yyyy-MM-01"))
                          .arg(ui->light_search_month_end->date().toString("yyyy-MM-31")));

    light_model->select();
    light_model->sort(0,Qt::DescendingOrder);

    qint64 total_limit_money=0;
    qint64 total_Maintenance_Costs = 0;
    qint64 total_consumables = 0;
    qint64 total_Spare_Parts = 0;
    qint64 total_PM_Parts = 0;
    qint64 total_tool = 0;
    qint64 total_Warranty = 0;
    qint64 total_Subpart = 0;
    qint64 total_etcpart = 0;

    QString draft_str = "";
    if(ui->light_draft->isChecked()){
        draft_str = "draft";
    }else if(ui->light_draft_calc->isChecked()){
        draft_str = "draft_calculate";
    }
    QSqlQuery query(my_mesdb);

    query.exec(QString("select SUM(limit_cost) as cost_sum from Cost_reduction_money_limit "
              "where `process` = '%1' AND `month` between '%2' AND '%3'").arg(tr("light"))
              .arg(ui->light_search_month_start->date().toString("yyyy-MM-01"))
              .arg(ui->light_search_month_end->date().toString("yyyy-MM-31")));
    if(query.next()){
        total_limit_money = query.value("cost_sum").toReal();
    }
    query.exec(QString("select sum(CASE WHEN `classification` = '%1' THEN price END) as Maintenance_Costs, "
                       "sum(CASE WHEN `classification` = '%2' THEN price END) as consumables,"
                       "sum(CASE WHEN `classification` = '%3' THEN price END) as Spare_Parts, "
                       "sum(CASE WHEN `classification` = '%4' THEN price END) as PM_Parts, "
                       "sum(CASE WHEN `classification` = '%5' THEN price END) as Tool, "
                       "sum(CASE WHEN `classification` = '%6' THEN price END) as Warranty, "
                       "sum(CASE WHEN `classification` = '%7' THEN price END) as Subpart, "
                       "sum(CASE WHEN `classification` = '%8' THEN price END) as etcpart "
                       "from Cost_reduction_data where `process` = '%9' AND buy_time between '%10' AND '%11' AND %12 = 2")
               .arg(tr("Maintenance Costs")).arg(tr("consumables"))
               .arg(tr("Spare Parts")).arg(tr("PM Parts")).arg(tr("Tool(tool)")).arg(tr("Warranty"))
               .arg(tr("Subpart(gasetc)")).arg(tr("etcpart"))
               .arg(tr("light"))
               .arg(ui->light_search_month_start->date().toString("yyyy-MM-01"))
               .arg(ui->light_search_month_end->date().toString("yyyy-MM-31"))
               .arg(draft_str));

    if(query.next()){
        total_Maintenance_Costs = query.value("Maintenance_Costs").toLongLong();
        total_consumables = query.value("consumables").toLongLong();
        total_Spare_Parts = query.value("Spare_Parts").toLongLong();
        total_PM_Parts = query.value("PM_Parts").toLongLong();
        total_tool = query.value("Tool").toLongLong();
        total_Warranty = query.value("Warranty").toLongLong();
        total_Subpart = query.value("Subpart").toLongLong();
        total_etcpart = query.value("etcpart").toLongLong();
    }
    if(light_total_chart == 0){
        light_total_chart = new Cost_Reduction_chart();
        light_total_chart_view = new Cost_Reduction_chart_view(light_total_chart);
        ui->light_total_chart_layuot->addWidget(light_total_chart_view);
    }else {
        light_total_chart->removeAllSeries();
        light_total_chart->removeAxis(light_total_chart->axisX());
        light_total_chart->removeAxis(light_total_chart->axisY());
    }

    QStackedBarSeries *total_bar_series = new QStackedBarSeries();
    total_bar_series->setLabelsFormat("@comma_long_value");

//    QBarSet *limit_total_remain_bar_set = new QBarSet(tr("total_remain_limit"));
//    limit_total_remain_bar_set->setLabelColor(QColor("black"));
//    limit_total_remain_bar_set->setPen(QColor("black"));
//    limit_total_remain_bar_set->setColor(QColor(Qt::transparent));

    QBarSet *total_Maintenance_Costs_bar_set = new QBarSet(tr("Maintenance Costs"));
    total_Maintenance_Costs_bar_set->setLabelColor(QColor("black"));

    QBarSet *total_consumables_bar_set = new QBarSet(tr("consumables"));
    total_consumables_bar_set->setLabelColor("black");

    QBarSet *total_personnel_expenses_bar_set = new QBarSet(tr("personnel expenses"));
    total_personnel_expenses_bar_set->setLabelColor("black");

    QBarSet *total_component_bar_set = new QBarSet(tr("component"));
    total_component_bar_set->setLabelColor("black");

    QBarSet *total_robot_bar_set = new QBarSet(tr("robot"));
    total_robot_bar_set->setLabelColor("black");

    QBarSet *total_pump_bar_set = new QBarSet(tr("pump"));
    total_pump_bar_set->setLabelColor("black");

    QBarSet *total_environment_improvement_bar_set = new QBarSet(tr("environment improvement"));
    total_environment_improvement_bar_set->setLabelColor("black");

    QBarSet *total_Spare_Parts_bar_set = new QBarSet(tr("Spare Parts"));
    total_Spare_Parts_bar_set->setLabelColor("black");

    QBarSet *total_PM_Parts_bar_set = new QBarSet(tr("PM Parts"));
    total_PM_Parts_bar_set->setLabelColor("black");

    QBarSet *total_TOOL_bar_set = new QBarSet(tr("Tool(tool)"));
    total_TOOL_bar_set->setLabelColor("black");

    QBarSet *total_Warranty_bar_set = new QBarSet(tr("Warranty"));
    total_Warranty_bar_set->setLabelColor("black");

    QBarSet *total_Subpart_bar_set = new QBarSet(tr("Subpart(gasetc)"));
    total_Subpart_bar_set->setLabelColor("black");

    QBarSet *total_etcpart_bar_set = new QBarSet(tr("etcpart"));
    total_etcpart_bar_set->setLabelColor("black");

    total_bar_series->setLabelsVisible(true);

    qint64 remain_money = total_limit_money - (total_Maintenance_Costs+total_consumables+
                                               total_Spare_Parts+
                                             total_PM_Parts+total_tool+total_Warranty+total_Subpart+total_etcpart);
//    limit_total_remain_bar_set->append(remain_money);
    total_Maintenance_Costs_bar_set->append(total_Maintenance_Costs);
    total_consumables_bar_set->append(total_consumables);

    total_Spare_Parts_bar_set->append(total_Spare_Parts);
    total_PM_Parts_bar_set->append(total_PM_Parts);
    total_TOOL_bar_set->append(total_tool);
    total_Warranty_bar_set->append(total_Warranty);
    total_Subpart_bar_set->append(total_Subpart);
    total_etcpart_bar_set->append(total_etcpart);

    total_bar_series->append(total_Spare_Parts_bar_set);
    total_bar_series->append(total_PM_Parts_bar_set);
    total_bar_series->append(total_TOOL_bar_set);
    total_bar_series->append(total_Warranty_bar_set);
    total_bar_series->append(total_Subpart_bar_set);
    total_bar_series->append(total_etcpart_bar_set);
    total_bar_series->append(total_consumables_bar_set);
    total_bar_series->append(total_Maintenance_Costs_bar_set);
//    total_bar_series->append(limit_total_remain_bar_set);


    light_total_chart->addSeries(total_bar_series);
    QStringList total_categories;
    total_categories << "total";
    QBarCategoryAxis *total_axis = new QBarCategoryAxis();
    total_axis->append(total_categories);
    light_total_chart->createDefaultAxes();
    light_total_chart->setAxisX(total_axis, total_bar_series);

    qreal total_max_yvalue = ((QValueAxis *)light_total_chart->axisY())->max();
    qreal total_min_yvalue = ((QValueAxis *)light_total_chart->axisY())->min();
    qreal total_diff_value = total_max_yvalue - total_min_yvalue;
    light_total_chart->axisY()->setRange(total_min_yvalue-(total_diff_value*0.1),total_max_yvalue+(total_diff_value*0.1));

    connect(total_bar_series,SIGNAL(hovered(bool,int,QBarSet*)),this,SLOT(light_total_hover_slot(bool,int,QBarSet*)));

    if(light_detail_chart == 0){
        light_detail_chart = new Cost_Reduction_chart();
        light_detail_chart_view = new Cost_Reduction_chart_view(light_detail_chart);
        ui->light_detail_chart_layuot->addWidget(light_detail_chart_view);
    }else {
        light_detail_chart->removeAllSeries();
        light_detail_chart->removeAxis(light_detail_axis_X);
        light_detail_chart->removeAxis(light_detail_axis_Y);
    }


    QStackedBarSeries *detail_bar_series = new QStackedBarSeries();
    detail_bar_series->setLabelsFormat("@comma_long_value");
    detail_bar_series->setName("detail");
    detail_bar_series->setLabelsVisible(true);

//    QBarSet *limit_detail_remain_bar_set = new QBarSet(tr("month_remain_limit"));
//    limit_detail_remain_bar_set->setLabelColor(QColor("black"));
//    limit_detail_remain_bar_set->setPen(QColor("black"));
//    limit_detail_remain_bar_set->setColor(QColor(Qt::transparent));

    QBarSet *detail_Maintenance_Costs_bar_set = new QBarSet(tr("Maintenance Costs"));
    detail_Maintenance_Costs_bar_set->setLabelColor(QColor("black"));
    detail_Maintenance_Costs_bar_set->setColor(total_Maintenance_Costs_bar_set->color());

    QBarSet *detail_consumables_bar_set = new QBarSet(tr("consumables"));
    detail_consumables_bar_set->setLabelColor("black");
    detail_consumables_bar_set->setColor(total_consumables_bar_set->color());

    QBarSet *detail_Spare_Parts_bar_set = new QBarSet(tr("Spare Parts"));
    detail_Spare_Parts_bar_set->setLabelColor("black");
    detail_Spare_Parts_bar_set->setColor(total_Spare_Parts_bar_set->color());

    QBarSet *detail_PM_Parts_bar_set = new QBarSet(tr("PM Parts"));
    detail_PM_Parts_bar_set->setLabelColor("black");
    detail_PM_Parts_bar_set->setColor(total_PM_Parts_bar_set->color());

    QBarSet *detail_TOOL_bar_set = new QBarSet(tr("Tool(tool)"));
    detail_TOOL_bar_set->setLabelColor("black");
    detail_TOOL_bar_set->setColor(total_TOOL_bar_set->color());

    QBarSet *detail_Warranty_bar_set = new QBarSet(tr("Warranty"));
    detail_Warranty_bar_set->setLabelColor("black");
    detail_Warranty_bar_set->setColor(total_Warranty_bar_set->color());

    QBarSet *detail_Subpart_bar_set = new QBarSet(tr("Subpart(gasetc)"));
    detail_Subpart_bar_set->setLabelColor("black");
    detail_Subpart_bar_set->setColor(total_Subpart_bar_set->color());

    QBarSet *detail_etcpart_bar_set = new QBarSet(tr("etcpart"));
    detail_etcpart_bar_set->setLabelColor("black");
    detail_etcpart_bar_set->setColor(total_etcpart_bar_set->color());

    QStringList detail_categories;

    QLineSeries *detail_lineseries = new QLineSeries();
    detail_lineseries->setName(tr("use_price"));
    detail_lineseries->setPointsVisible(true);
//    detail_lineseries->setPointLabelsVisible(true);

    int linevalue_x = 0;

    int j=0;
    while(true){
        QDate start_date =  ui->light_search_month_start->date().addMonths(j);
        QDate end_date = ui->light_search_month_end->date();

        detail_categories.append(start_date.toString("yy-MM"));


        qint64 detail_limit_money=0;
        qint64 detail_Maintenance_Costs = 0;
        qint64 detail_consumables = 0;

        qint64 detail_Spare_Parts=0;
        qint64 detail_PM_Parts=0;
        qint64 detail_tool=0;
        qint64 detail_Warranty=0;
        qint64 detail_Subpart=0;
        qint64 detail_etcpart=0;

        query.exec(QString("select SUM(limit_cost) as cost_sum from Cost_reduction_money_limit "
                  "where `process` = '%1' AND `month` between '%2' AND '%3'").arg(tr("light"))
                  .arg(start_date.toString("yyyy-MM-01"))
                  .arg(start_date.toString("yyyy-MM-31")));
        if(query.next()){
            detail_limit_money = query.value("cost_sum").toLongLong();
        }
        query.exec(QString("select sum(CASE WHEN `classification` = '%1' THEN price END) as Maintenance_Costs, "
                           "sum(CASE WHEN `classification` = '%2' THEN price END) as consumables,"
                           "sum(CASE WHEN `classification` = '%3' THEN price END) as Spare_Parts, "
                           "sum(CASE WHEN `classification` = '%4' THEN price END) as PM_Parts, "
                           "sum(CASE WHEN `classification` = '%5' THEN price END) as Tool, "
                           "sum(CASE WHEN `classification` = '%6' THEN price END) as Warranty, "
                           "sum(CASE WHEN `classification` = '%7' THEN price END) as Subpart, "
                           "sum(CASE WHEN `classification` = '%8' THEN price END) as etcpart "
                           "from Cost_reduction_data where `process` = '%9' AND buy_time between '%10' AND '%11' AND %12 = 2  ")
                   .arg(tr("Maintenance Costs")).arg(tr("consumables"))
                   .arg(tr("Spare Parts")).arg(tr("PM Parts")).arg(tr("Tool(tool)")).arg(tr("Warranty"))
                   .arg(tr("Subpart(gasetc)")).arg(tr("etcpart"))
                   .arg(tr("light"))
                   .arg(start_date.toString("yyyy-MM-01"))
                   .arg(start_date.toString("yyyy-MM-31"))
                   .arg(draft_str));
        if(query.next()){
            detail_Maintenance_Costs = query.value("Maintenance_Costs").toLongLong();
            detail_consumables = query.value("consumables").toLongLong();
            detail_Spare_Parts = query.value("Spare_Parts").toLongLong();
            detail_PM_Parts = query.value("PM_Parts").toLongLong();
            detail_tool = query.value("Tool").toLongLong();
            detail_Warranty = query.value("Warranty").toLongLong();
            detail_Subpart = query.value("Subpart").toLongLong();
            detail_etcpart = query.value("etcpart").toLongLong();
        }

        qint64 detail_use_money  = detail_Maintenance_Costs+detail_consumables+
                                    detail_Spare_Parts+detail_PM_Parts+detail_tool+detail_Warranty+detail_Subpart+detail_etcpart;
        qint64 detail_remain_money = detail_limit_money - detail_use_money;

//        limit_detail_remain_bar_set->append(detail_remain_money);
        detail_Maintenance_Costs_bar_set->append(detail_Maintenance_Costs);
        detail_consumables_bar_set->append(detail_consumables);
        detail_Spare_Parts_bar_set->append(detail_Spare_Parts);
        detail_PM_Parts_bar_set->append(detail_PM_Parts);
        detail_TOOL_bar_set->append(detail_tool);
        detail_Warranty_bar_set->append(detail_Warranty);
        detail_Subpart_bar_set->append(detail_Subpart);
        detail_etcpart_bar_set->append(detail_etcpart);
        detail_lineseries->append(linevalue_x,detail_use_money);

        linevalue_x++;
        if((start_date.year() == end_date.year()) && (start_date.month() == end_date.month())){
            break;
        }
        j++;
    }


    detail_bar_series->append(detail_consumables_bar_set);
    detail_bar_series->append(detail_Maintenance_Costs_bar_set);
    detail_bar_series->append(detail_Spare_Parts_bar_set);
    detail_bar_series->append(detail_PM_Parts_bar_set);
    detail_bar_series->append(detail_TOOL_bar_set);
    detail_bar_series->append(detail_Warranty_bar_set);
    detail_bar_series->append(detail_Subpart_bar_set);
    detail_bar_series->append(detail_etcpart_bar_set);
//    detail_bar_series->append(limit_detail_remain_bar_set);

    detail_lineseries->setPointLabelsFormat("@yPoint");
    light_detail_chart->addSeries(detail_lineseries);
    light_detail_chart->addSeries(detail_bar_series);

    light_detail_axis_X = new QBarCategoryAxis();
    light_detail_axis_X->append(detail_categories);
    light_detail_chart->setAxisX(light_detail_axis_X, detail_bar_series);
    light_detail_chart->setAxisX(light_detail_axis_X,detail_lineseries);
    light_detail_axis_X->setRange(detail_categories.first(),detail_categories.last());
    light_detail_axis_Y= new QValueAxis();
    light_detail_chart->setAxisY(light_detail_axis_Y,detail_bar_series);
    light_detail_chart->setAxisY(light_detail_axis_Y,detail_lineseries);

    qreal max_yvalue = light_detail_axis_Y->max();
    qreal min_yvalue = light_detail_axis_Y->min();
    qreal diff_value = max_yvalue - min_yvalue;
    light_detail_axis_Y->setRange(min_yvalue-(diff_value*0.1),max_yvalue+(diff_value*0.1));
    connect(detail_bar_series,SIGNAL(hovered(bool,int,QBarSet*)),this,SLOT(light_detail_hover_slot(bool,int,QBarSet*)));
}
void cost_reduction_main::on_eatching_refresh_btn_clicked()
{
    Cost_Redction_data_table_model *eatching_model = (Cost_Redction_data_table_model *)ui->eatching_data_table->model();
    eatching_model->setFilter(QString("process = '%1' AND buy_time between '%2' AND '%3' ").arg(tr("eatching"))
                          .arg(ui->eatching_search_month_start->date().toString("yyyy-MM-01"))
                          .arg(ui->eatching_search_month_end->date().toString("yyyy-MM-31")));

    eatching_model->select();
    eatching_model->sort(0,Qt::DescendingOrder);
    qint64 total_limit_money=0;
    qint64 total_Maintenance_Costs = 0;
    qint64 total_consumables = 0;

    qint64 total_Spare_Parts = 0;
    qint64 total_PM_Parts = 0;
    qint64 total_tool = 0;
    qint64 total_Warranty = 0;
    qint64 total_Subpart = 0;
    qint64 total_etcpart = 0;

    QSqlQuery query(my_mesdb);
    QString draft_str = "";
    if(ui->eatching_draft->isChecked()){
        draft_str = "draft";
    }else if(ui->eatching_draft_calc->isChecked()){
        draft_str = "draft_calculate";
    }

    query.exec(QString("select SUM(limit_cost) as cost_sum from Cost_reduction_money_limit "
              "where `process` = '%1' AND `month` between '%2' AND '%3'").arg(tr("eatching"))
              .arg(ui->eatching_search_month_start->date().toString("yyyy-MM-01"))
              .arg(ui->eatching_search_month_end->date().toString("yyyy-MM-31")));
    if(query.next()){
        total_limit_money = query.value("cost_sum").toReal();
    }
    query.exec(QString("select sum(CASE WHEN `classification` = '%1' THEN price END) as Maintenance_Costs, "
                       "sum(CASE WHEN `classification` = '%2' THEN price END) as consumables,"
                       "sum(CASE WHEN `classification` = '%3' THEN price END) as Spare_Parts, "
                       "sum(CASE WHEN `classification` = '%4' THEN price END) as PM_Parts, "
                       "sum(CASE WHEN `classification` = '%5' THEN price END) as Tool, "
                       "sum(CASE WHEN `classification` = '%6' THEN price END) as Warranty, "
                       "sum(CASE WHEN `classification` = '%7' THEN price END) as Subpart, "
                       "sum(CASE WHEN `classification` = '%8' THEN price END) as etcpart "
                       "from Cost_reduction_data where `process` = '%9' AND buy_time between '%10' AND '%11' AND %12 = 2")
               .arg(tr("Maintenance Costs")).arg(tr("consumables"))
               .arg(tr("Spare Parts")).arg(tr("PM Parts")).arg(tr("Tool(tool)")).arg(tr("Warranty"))
               .arg(tr("Subpart(gasetc)")).arg(tr("etcpart"))
               .arg(tr("eatching"))
               .arg(ui->eatching_search_month_start->date().toString("yyyy-MM-01"))
               .arg(ui->eatching_search_month_end->date().toString("yyyy-MM-31"))
               .arg(draft_str));
    qDebug()<<query.lastQuery();
    if(query.next()){
        total_Maintenance_Costs = query.value("Maintenance_Costs").toLongLong();
        total_consumables = query.value("consumables").toLongLong();

        total_Spare_Parts = query.value("Spare_Parts").toLongLong();
        total_PM_Parts = query.value("PM_Parts").toLongLong();
        total_tool = query.value("Tool").toLongLong();
        total_Warranty = query.value("Warranty").toLongLong();
        total_Subpart = query.value("Subpart").toLongLong();
        total_etcpart = query.value("etcpart").toLongLong();
    }
    if(eatching_total_chart == 0){
        eatching_total_chart = new Cost_Reduction_chart();
        eatching_total_chart_view = new Cost_Reduction_chart_view(eatching_total_chart);
        ui->eatching_total_chart_layuot->addWidget(eatching_total_chart_view);
    }else {
        eatching_total_chart->removeAllSeries();
        eatching_total_chart->removeAxis(eatching_total_chart->axisX());
        eatching_total_chart->removeAxis(eatching_total_chart->axisY());
    }

    QStackedBarSeries *total_bar_series = new QStackedBarSeries();
    total_bar_series->setLabelsFormat("@comma_long_value");

//    QBarSet *limit_total_remain_bar_set = new QBarSet(tr("total_remain_limit"));
//    limit_total_remain_bar_set->setLabelColor(QColor("black"));
//    limit_total_remain_bar_set->setPen(QColor("black"));
//    limit_total_remain_bar_set->setColor(QColor(Qt::transparent));

    QBarSet *total_Maintenance_Costs_bar_set = new QBarSet(tr("Maintenance Costs"));
    total_Maintenance_Costs_bar_set->setLabelColor(QColor("black"));

    QBarSet *total_consumables_bar_set = new QBarSet(tr("consumables"));
    total_consumables_bar_set->setLabelColor("black");

    QBarSet *total_Spare_Parts_bar_set = new QBarSet(tr("Spare Parts"));
    total_Spare_Parts_bar_set->setLabelColor("black");

    QBarSet *total_PM_Parts_bar_set = new QBarSet(tr("PM Parts"));
    total_PM_Parts_bar_set->setLabelColor("black");

    QBarSet *total_TOOL_bar_set = new QBarSet(tr("Tool(tool)"));
    total_TOOL_bar_set->setLabelColor("black");

    QBarSet *total_Warranty_bar_set = new QBarSet(tr("Warranty"));
    total_Warranty_bar_set->setLabelColor("black");

    QBarSet *total_Subpart_bar_set = new QBarSet(tr("Subpart(gasetc)"));
    total_Subpart_bar_set->setLabelColor("black");

    QBarSet *total_etcpart_bar_set = new QBarSet(tr("etcpart"));
    total_etcpart_bar_set->setLabelColor("black");

    total_bar_series->setLabelsVisible(true);

    qint64 remain_money = total_limit_money - (total_Maintenance_Costs+total_consumables+
                                               total_PM_Parts+total_tool+total_Warranty+total_Subpart+total_etcpart);
//    limit_total_remain_bar_set->append(remain_money);
    total_Maintenance_Costs_bar_set->append(total_Maintenance_Costs);
    total_consumables_bar_set->append(total_consumables);
    total_Spare_Parts_bar_set->append(total_Spare_Parts);
    total_PM_Parts_bar_set->append(total_PM_Parts);
    total_TOOL_bar_set->append(total_tool);
    total_Warranty_bar_set->append(total_Warranty);
    total_Subpart_bar_set->append(total_Subpart);
    total_etcpart_bar_set->append(total_etcpart);


    total_bar_series->append(total_Spare_Parts_bar_set);
    total_bar_series->append(total_PM_Parts_bar_set);
    total_bar_series->append(total_TOOL_bar_set);
    total_bar_series->append(total_Warranty_bar_set);
    total_bar_series->append(total_Subpart_bar_set);
    total_bar_series->append(total_etcpart_bar_set);
    total_bar_series->append(total_consumables_bar_set);
    total_bar_series->append(total_Maintenance_Costs_bar_set);
//    total_bar_series->append(limit_total_remain_bar_set);

    eatching_total_chart->addSeries(total_bar_series);
    QStringList total_categories;
    total_categories << "total";
    QBarCategoryAxis *total_axis = new QBarCategoryAxis();
    total_axis->append(total_categories);
    eatching_total_chart->createDefaultAxes();
    eatching_total_chart->setAxisX(total_axis, total_bar_series);

    qreal total_max_yvalue = ((QValueAxis *)eatching_total_chart->axisY())->max();
    qreal total_min_yvalue = ((QValueAxis *)eatching_total_chart->axisY())->min();
    qreal total_diff_value = total_max_yvalue - total_min_yvalue;
    eatching_total_chart->axisY()->setRange(total_min_yvalue-(total_diff_value*0.1),total_max_yvalue+(total_diff_value*0.1));

    connect(total_bar_series,SIGNAL(hovered(bool,int,QBarSet*)),this,SLOT(eatching_total_hover_slot(bool,int,QBarSet*)));

    if(eatching_detail_chart == 0){
        eatching_detail_chart = new Cost_Reduction_chart();
        eatching_detail_chart_view = new Cost_Reduction_chart_view(eatching_detail_chart);
        ui->eatching_detail_chart_layuot->addWidget(eatching_detail_chart_view);
    }else {
        eatching_detail_chart->removeAllSeries();
        eatching_detail_chart->removeAxis(eatching_detail_axis_X);
        eatching_detail_chart->removeAxis(eatching_detail_axis_Y);
    }


    QStackedBarSeries *detail_bar_series = new QStackedBarSeries();
    detail_bar_series->setLabelsFormat("@comma_long_value");
    detail_bar_series->setName("detail");
    detail_bar_series->setLabelsVisible(true);

//    QBarSet *limit_detail_remain_bar_set = new QBarSet(tr("month_remain_limit"));
//    limit_detail_remain_bar_set->setLabelColor(QColor("black"));
//    limit_detail_remain_bar_set->setPen(QColor("black"));
//    limit_detail_remain_bar_set->setColor(QColor(Qt::transparent));

    QBarSet *detail_Maintenance_Costs_bar_set = new QBarSet(tr("Maintenance Costs"));
    detail_Maintenance_Costs_bar_set->setLabelColor(QColor("black"));
    detail_Maintenance_Costs_bar_set->setColor(total_Maintenance_Costs_bar_set->color());

    QBarSet *detail_consumables_bar_set = new QBarSet(tr("consumables"));
    detail_consumables_bar_set->setLabelColor("black");
    detail_consumables_bar_set->setColor(total_consumables_bar_set->color());



    QBarSet *detail_Spare_Parts_bar_set = new QBarSet(tr("Spare Parts"));
    detail_Spare_Parts_bar_set->setLabelColor("black");
    detail_Spare_Parts_bar_set->setColor(total_Spare_Parts_bar_set->color());

    QBarSet *detail_PM_Parts_bar_set = new QBarSet(tr("PM Parts"));
    detail_PM_Parts_bar_set->setLabelColor("black");
    detail_PM_Parts_bar_set->setColor(total_PM_Parts_bar_set->color());

    QBarSet *detail_TOOL_bar_set = new QBarSet(tr("Tool(tool)"));
    detail_TOOL_bar_set->setLabelColor("black");
    detail_TOOL_bar_set->setColor(total_TOOL_bar_set->color());

    QBarSet *detail_Warranty_bar_set = new QBarSet(tr("Warranty"));
    detail_Warranty_bar_set->setLabelColor("black");
    detail_Warranty_bar_set->setColor(total_Warranty_bar_set->color());

    QBarSet *detail_Subpart_bar_set = new QBarSet(tr("Subpart(gasetc)"));
    detail_Subpart_bar_set->setLabelColor("black");
    detail_Subpart_bar_set->setColor(total_Subpart_bar_set->color());

    QBarSet *detail_etcpart_bar_set = new QBarSet(tr("etcpart"));
    detail_etcpart_bar_set->setLabelColor("black");
    detail_etcpart_bar_set->setColor(total_etcpart_bar_set->color());

    QStringList detail_categories;

    QLineSeries *detail_lineseries = new QLineSeries();
    detail_lineseries->setName(tr("use_price"));
    detail_lineseries->setPointsVisible(true);
//    detail_lineseries->setPointLabelsVisible(true);

    int linevalue_x = 0;

    int j=0;
    while(true){
        QDate start_date =  ui->eatching_search_month_start->date().addMonths(j);
        QDate end_date = ui->eatching_search_month_end->date();

        detail_categories.append(start_date.toString("yy-MM"));


        qint64 detail_limit_money=0;
        qint64 detail_Maintenance_Costs = 0;
        qint64 detail_consumables = 0;

        qint64 detail_Spare_Parts=0;
        qint64 detail_PM_Parts=0;
        qint64 detail_tool=0;
        qint64 detail_Warranty=0;
        qint64 detail_Subpart=0;
        qint64 detail_etcpart=0;

        query.exec(QString("select SUM(limit_cost) as cost_sum from Cost_reduction_money_limit "
                  "where `process` = '%1' AND `month` between '%2' AND '%3'").arg(tr("eatching"))
                  .arg(start_date.toString("yyyy-MM-01"))
                  .arg(start_date.toString("yyyy-MM-31")));
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError().text();
        if(query.next()){
            detail_limit_money = query.value("cost_sum").toLongLong();
        }
        query.exec(QString("select sum(CASE WHEN `classification` = '%1' THEN price END) as Maintenance_Costs, "
                           "sum(CASE WHEN `classification` = '%2' THEN price END) as consumables,"
                           "sum(CASE WHEN `classification` = '%3' THEN price END) as Spare_Parts, "
                           "sum(CASE WHEN `classification` = '%4' THEN price END) as PM_Parts, "
                           "sum(CASE WHEN `classification` = '%5' THEN price END) as Tool, "
                           "sum(CASE WHEN `classification` = '%6' THEN price END) as Warranty, "
                           "sum(CASE WHEN `classification` = '%7' THEN price END) as Subpart, "
                           "sum(CASE WHEN `classification` = '%8' THEN price END) as etcpart "
                           "from Cost_reduction_data where `process` = '%9' AND buy_time between '%10' AND '%11' AND %12 = 2  ")
                   .arg(tr("Maintenance Costs")).arg(tr("consumables"))
                   .arg(tr("Spare Parts")).arg(tr("PM Parts")).arg(tr("Tool(tool)")).arg(tr("Warranty"))
                   .arg(tr("Subpart(gasetc)")).arg(tr("etcpart"))
                   .arg(tr("eatching"))
                   .arg(start_date.toString("yyyy-MM-01"))
                   .arg(start_date.toString("yyyy-MM-31"))
                   .arg(draft_str));
        if(query.next()){
            detail_Maintenance_Costs = query.value("Maintenance_Costs").toLongLong();
            detail_consumables = query.value("consumables").toLongLong();
            detail_Spare_Parts = query.value("Spare_Parts").toLongLong();
            detail_PM_Parts = query.value("PM_Parts").toLongLong();
            detail_tool = query.value("Tool").toLongLong();
            detail_Warranty = query.value("Warranty").toLongLong();
            detail_Subpart = query.value("Subpart").toLongLong();
            detail_etcpart = query.value("etcpart").toLongLong();
        }
        qint64 detail_use_money  = detail_Maintenance_Costs+detail_consumables+
                                    detail_Spare_Parts+detail_PM_Parts+detail_tool+detail_Warranty+detail_Subpart+detail_etcpart;
        qint64 detail_remain_money = detail_limit_money - detail_use_money;

//        limit_detail_remain_bar_set->append(detail_remain_money);
        detail_Maintenance_Costs_bar_set->append(detail_Maintenance_Costs);
        detail_consumables_bar_set->append(detail_consumables);
        detail_Spare_Parts_bar_set->append(detail_Spare_Parts);
        detail_PM_Parts_bar_set->append(detail_PM_Parts);
        detail_TOOL_bar_set->append(detail_tool);
        detail_Warranty_bar_set->append(detail_Warranty);
        detail_Subpart_bar_set->append(detail_Subpart);
        detail_etcpart_bar_set->append(detail_etcpart);
        detail_lineseries->append(linevalue_x,detail_use_money);

        linevalue_x++;
        if((start_date.year() == end_date.year()) && (start_date.month() == end_date.month())){
            break;
        }
        j++;
    }

    detail_bar_series->append(detail_consumables_bar_set);
    detail_bar_series->append(detail_Maintenance_Costs_bar_set);
    detail_bar_series->append(detail_Spare_Parts_bar_set);
    detail_bar_series->append(detail_PM_Parts_bar_set);
    detail_bar_series->append(detail_TOOL_bar_set);
    detail_bar_series->append(detail_Warranty_bar_set);
    detail_bar_series->append(detail_Subpart_bar_set);
    detail_bar_series->append(detail_etcpart_bar_set);
//    detail_bar_series->append(limit_detail_remain_bar_set);

    detail_lineseries->setPointLabelsFormat("@yPoint");
    eatching_detail_chart->addSeries(detail_lineseries);
    eatching_detail_chart->addSeries(detail_bar_series);

    eatching_detail_axis_X = new QBarCategoryAxis();
    eatching_detail_axis_X->append(detail_categories);
    eatching_detail_chart->setAxisX(eatching_detail_axis_X, detail_bar_series);
    eatching_detail_chart->setAxisX(eatching_detail_axis_X,detail_lineseries);
    eatching_detail_axis_X->setRange(detail_categories.first(),detail_categories.last());
    eatching_detail_axis_Y= new QValueAxis();
    eatching_detail_chart->setAxisY(eatching_detail_axis_Y,detail_bar_series);
    eatching_detail_chart->setAxisY(eatching_detail_axis_Y,detail_lineseries);

    qreal max_yvalue = eatching_detail_axis_Y->max();
    qreal min_yvalue = eatching_detail_axis_Y->min();
    qreal diff_value = max_yvalue - min_yvalue;
    eatching_detail_axis_Y->setRange(min_yvalue-(diff_value*0.1),max_yvalue+(diff_value*0.1));
    connect(detail_bar_series,SIGNAL(hovered(bool,int,QBarSet*)),this,SLOT(eatching_detail_hover_slot(bool,int,QBarSet*)));
}
void cost_reduction_main::on_probe_refresh_btn_clicked()
{
    Cost_Redction_data_table_model *probe_model = (Cost_Redction_data_table_model *)ui->probe_data_table->model();
    probe_model->setFilter(QString("process = '%1' AND buy_time between '%2' AND '%3' ").arg(tr("probe"))
                          .arg(ui->probe_search_month_start->date().toString("yyyy-MM-01"))
                          .arg(ui->probe_search_month_end->date().toString("yyyy-MM-31")));

    probe_model->select();
    probe_model->sort(0,Qt::DescendingOrder);
    qint64 total_limit_money=0;
    qint64 total_Maintenance_Costs = 0;
    qint64 total_consumables = 0;
    qint64 total_Spare_Parts = 0;
    qint64 total_PM_Parts = 0;
    qint64 total_tool = 0;
    qint64 total_Warranty = 0;
    qint64 total_Subpart = 0;
    qint64 total_etcpart = 0;


    QSqlQuery query(my_mesdb);
    QString draft_str = "";
    if(ui->probe_draft->isChecked()){
        draft_str = "draft";
    }else if(ui->probe_draft_calc->isChecked()){
        draft_str = "draft_calculate";
    }
    query.exec(QString("select SUM(limit_cost) as cost_sum from Cost_reduction_money_limit "
              "where `process` = '%1' AND `month` between '%2' AND '%3'").arg(tr("probe"))
              .arg(ui->probe_search_month_start->date().toString("yyyy-MM-01"))
              .arg(ui->probe_search_month_end->date().toString("yyyy-MM-31")));
    if(query.next()){
        total_limit_money = query.value("cost_sum").toReal();
    }
    query.exec(QString("select sum(CASE WHEN `classification` = '%1' THEN price END) as Maintenance_Costs, "
                       "sum(CASE WHEN `classification` = '%2' THEN price END) as consumables,"
                       "sum(CASE WHEN `classification` = '%3' THEN price END) as Spare_Parts, "
                       "sum(CASE WHEN `classification` = '%4' THEN price END) as PM_Parts, "
                       "sum(CASE WHEN `classification` = '%5' THEN price END) as Tool, "
                       "sum(CASE WHEN `classification` = '%6' THEN price END) as Warranty, "
                       "sum(CASE WHEN `classification` = '%7' THEN price END) as Subpart, "
                       "sum(CASE WHEN `classification` = '%8' THEN price END) as etcpart "
                       "from Cost_reduction_data where `process` = '%9' AND buy_time between '%10' AND '%11' AND %12 = 2")
               .arg(tr("Maintenance Costs")).arg(tr("consumables"))
               .arg(tr("Spare Parts")).arg(tr("PM Parts")).arg(tr("Tool(tool)")).arg(tr("Warranty"))
               .arg(tr("Subpart(gasetc)")).arg(tr("etcpart"))
               .arg(tr("probe"))
               .arg(ui->probe_search_month_start->date().toString("yyyy-MM-01"))
               .arg(ui->probe_search_month_end->date().toString("yyyy-MM-31"))
               .arg(draft_str));
    qDebug()<<query.lastQuery();
    qDebug()<<query.lastError().text();
    if(query.next()){
        total_Maintenance_Costs = query.value("Maintenance_Costs").toLongLong();
        total_consumables = query.value("consumables").toLongLong();
        total_Spare_Parts = query.value("Spare_Parts").toLongLong();
        total_PM_Parts = query.value("PM_Parts").toLongLong();
        total_tool = query.value("Tool").toLongLong();
        total_Warranty = query.value("Warranty").toLongLong();
        total_Subpart = query.value("Subpart").toLongLong();
        total_etcpart = query.value("etcpart").toLongLong();
    }
    if(probe_total_chart == 0){
        probe_total_chart = new Cost_Reduction_chart();
        probe_total_chart_view = new Cost_Reduction_chart_view(probe_total_chart);
        ui->probe_total_chart_layuot->addWidget(probe_total_chart_view);
    }else {
        probe_total_chart->removeAllSeries();
        probe_total_chart->removeAxis(probe_total_chart->axisX());
        probe_total_chart->removeAxis(probe_total_chart->axisY());
    }

    QStackedBarSeries *total_bar_series = new QStackedBarSeries();
    total_bar_series->setLabelsFormat("@comma_long_value");

//    QBarSet *limit_total_remain_bar_set = new QBarSet(tr("total_remain_limit"));
//    limit_total_remain_bar_set->setLabelColor(QColor("black"));
//    limit_total_remain_bar_set->setPen(QColor("black"));
//    limit_total_remain_bar_set->setColor(QColor(Qt::transparent));

    QBarSet *total_Maintenance_Costs_bar_set = new QBarSet(tr("Maintenance Costs"));
    total_Maintenance_Costs_bar_set->setLabelColor(QColor("black"));

    QBarSet *total_consumables_bar_set = new QBarSet(tr("consumables"));
    total_consumables_bar_set->setLabelColor("black");


    QBarSet *total_Spare_Parts_bar_set = new QBarSet(tr("Spare Parts"));
    total_Spare_Parts_bar_set->setLabelColor("black");

    QBarSet *total_PM_Parts_bar_set = new QBarSet(tr("PM Parts"));
    total_PM_Parts_bar_set->setLabelColor("black");

    QBarSet *total_TOOL_bar_set = new QBarSet(tr("Tool(tool)"));
    total_TOOL_bar_set->setLabelColor("black");

    QBarSet *total_Warranty_bar_set = new QBarSet(tr("Warranty"));
    total_Warranty_bar_set->setLabelColor("black");

    QBarSet *total_Subpart_bar_set = new QBarSet(tr("Subpart(gasetc)"));
    total_Subpart_bar_set->setLabelColor("black");

    QBarSet *total_etcpart_bar_set = new QBarSet(tr("etcpart"));
    total_etcpart_bar_set->setLabelColor("black");


    total_bar_series->setLabelsVisible(true);

    qint64 remain_money = total_limit_money - (total_Maintenance_Costs+total_consumables+
                                                            total_Spare_Parts+
                                                          total_PM_Parts+total_tool+total_Warranty+total_Subpart+total_etcpart);

//    limit_total_remain_bar_set->append(remain_money);
    total_Maintenance_Costs_bar_set->append(total_Maintenance_Costs);
    total_consumables_bar_set->append(total_consumables);
    total_Spare_Parts_bar_set->append(total_Spare_Parts);
    total_PM_Parts_bar_set->append(total_PM_Parts);
    total_TOOL_bar_set->append(total_tool);
    total_Warranty_bar_set->append(total_Warranty);
    total_Subpart_bar_set->append(total_Subpart);
    total_etcpart_bar_set->append(total_etcpart);

    total_bar_series->append(total_Spare_Parts_bar_set);
    total_bar_series->append(total_PM_Parts_bar_set);
    total_bar_series->append(total_TOOL_bar_set);
    total_bar_series->append(total_Warranty_bar_set);
    total_bar_series->append(total_Subpart_bar_set);
    total_bar_series->append(total_etcpart_bar_set);
    total_bar_series->append(total_consumables_bar_set);
    total_bar_series->append(total_Maintenance_Costs_bar_set);
//    total_bar_series->append(limit_total_remain_bar_set);


    probe_total_chart->addSeries(total_bar_series);
    QStringList total_categories;
    total_categories << "total";
    QBarCategoryAxis *total_axis = new QBarCategoryAxis();
    total_axis->append(total_categories);
    probe_total_chart->createDefaultAxes();
    probe_total_chart->setAxisX(total_axis, total_bar_series);

    qreal total_max_yvalue = ((QValueAxis *)probe_total_chart->axisY())->max();
    qreal total_min_yvalue = ((QValueAxis *)probe_total_chart->axisY())->min();
    qreal total_diff_value = total_max_yvalue - total_min_yvalue;
    probe_total_chart->axisY()->setRange(total_min_yvalue-(total_diff_value*0.1),total_max_yvalue+(total_diff_value*0.1));

    connect(total_bar_series,SIGNAL(hovered(bool,int,QBarSet*)),this,SLOT(probe_total_hover_slot(bool,int,QBarSet*)));

    if(probe_detail_chart == 0){
        probe_detail_chart = new Cost_Reduction_chart();
        probe_detail_chart_view = new Cost_Reduction_chart_view(probe_detail_chart);
        ui->probe_detail_chart_layuot->addWidget(probe_detail_chart_view);
    }else {
        probe_detail_chart->removeAllSeries();
        probe_detail_chart->removeAxis(probe_detail_axis_X);
        probe_detail_chart->removeAxis(probe_detail_axis_Y);
    }


    QStackedBarSeries *detail_bar_series = new QStackedBarSeries();
    detail_bar_series->setLabelsFormat("@comma_long_value");
    detail_bar_series->setName("detail");
    detail_bar_series->setLabelsVisible(true);

//    QBarSet *limit_detail_remain_bar_set = new QBarSet(tr("month_remain_limit"));
//    limit_detail_remain_bar_set->setLabelColor(QColor("black"));
//    limit_detail_remain_bar_set->setPen(QColor("black"));
//    limit_detail_remain_bar_set->setColor(QColor(Qt::transparent));

    QBarSet *detail_Maintenance_Costs_bar_set = new QBarSet(tr("Maintenance Costs"));
    detail_Maintenance_Costs_bar_set->setLabelColor(QColor("black"));
    detail_Maintenance_Costs_bar_set->setColor(total_Maintenance_Costs_bar_set->color());

    QBarSet *detail_consumables_bar_set = new QBarSet(tr("consumables"));
    detail_consumables_bar_set->setLabelColor("black");
    detail_consumables_bar_set->setColor(total_consumables_bar_set->color());

    QBarSet *detail_Spare_Parts_bar_set = new QBarSet(tr("Spare Parts"));
    detail_Spare_Parts_bar_set->setLabelColor("black");
    detail_Spare_Parts_bar_set->setColor(total_Spare_Parts_bar_set->color());

    QBarSet *detail_PM_Parts_bar_set = new QBarSet(tr("PM Parts"));
    detail_PM_Parts_bar_set->setLabelColor("black");
    detail_PM_Parts_bar_set->setColor(total_PM_Parts_bar_set->color());

    QBarSet *detail_TOOL_bar_set = new QBarSet(tr("Tool(tool)"));
    detail_TOOL_bar_set->setLabelColor("black");
    detail_TOOL_bar_set->setColor(total_TOOL_bar_set->color());

    QBarSet *detail_Warranty_bar_set = new QBarSet(tr("Warranty"));
    detail_Warranty_bar_set->setLabelColor("black");
    detail_Warranty_bar_set->setColor(total_Warranty_bar_set->color());

    QBarSet *detail_Subpart_bar_set = new QBarSet(tr("Subpart(gasetc)"));
    detail_Subpart_bar_set->setLabelColor("black");
    detail_Subpart_bar_set->setColor(total_Subpart_bar_set->color());

    QBarSet *detail_etcpart_bar_set = new QBarSet(tr("etcpart"));
    detail_etcpart_bar_set->setLabelColor("black");
    detail_etcpart_bar_set->setColor(total_etcpart_bar_set->color());


    QStringList detail_categories;

    QLineSeries *detail_lineseries = new QLineSeries();
    detail_lineseries->setName(tr("use_price"));
    detail_lineseries->setPointsVisible(true);
//    detail_lineseries->setPointLabelsVisible(true);

    int linevalue_x = 0;

    int j=0;
    while(true){
        QDate start_date =  ui->probe_search_month_start->date().addMonths(j);
        QDate end_date = ui->probe_search_month_end->date();

        detail_categories.append(start_date.toString("yy-MM"));


        qint64 detail_limit_money=0;
        qint64 detail_Maintenance_Costs = 0;
        qint64 detail_consumables = 0;


        qint64 detail_Spare_Parts=0;
        qint64 detail_PM_Parts=0;
        qint64 detail_tool=0;
        qint64 detail_Warranty=0;
        qint64 detail_Subpart=0;
        qint64 detail_etcpart=0;


        query.exec(QString("select SUM(limit_cost) as cost_sum from Cost_reduction_money_limit "
                  "where `process` = '%1' AND `month` between '%2' AND '%3'").arg(tr("probe"))
                  .arg(start_date.toString("yyyy-MM-01"))
                  .arg(start_date.toString("yyyy-MM-31")));
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError().text();
        if(query.next()){
            detail_limit_money = query.value("cost_sum").toLongLong();
        }
        query.exec(QString("select sum(CASE WHEN `classification` = '%1' THEN price END) as Maintenance_Costs, "
                           "sum(CASE WHEN `classification` = '%2' THEN price END) as consumables,"
                           "sum(CASE WHEN `classification` = '%3' THEN price END) as Spare_Parts, "
                           "sum(CASE WHEN `classification` = '%4' THEN price END) as PM_Parts, "
                           "sum(CASE WHEN `classification` = '%5' THEN price END) as Tool, "
                           "sum(CASE WHEN `classification` = '%6' THEN price END) as Warranty, "
                           "sum(CASE WHEN `classification` = '%7' THEN price END) as Subpart, "
                           "sum(CASE WHEN `classification` = '%8' THEN price END) as etcpart "
                           "from Cost_reduction_data where `process` = '%9' AND buy_time between '%10' AND '%11' AND %12 = 2  ")
                   .arg(tr("Maintenance Costs")).arg(tr("consumables"))
                   .arg(tr("Spare Parts")).arg(tr("PM Parts")).arg(tr("Tool(tool)")).arg(tr("Warranty"))
                   .arg(tr("Subpart(gasetc)")).arg(tr("etcpart"))
                   .arg(tr("probe"))
                   .arg(start_date.toString("yyyy-MM-01"))
                   .arg(start_date.toString("yyyy-MM-31"))
                   .arg(draft_str));
        if(query.next()){
            detail_Maintenance_Costs = query.value("Maintenance_Costs").toLongLong();
            detail_consumables = query.value("consumables").toLongLong();
            detail_Spare_Parts = query.value("Spare_Parts").toLongLong();
            detail_PM_Parts = query.value("PM_Parts").toLongLong();
            detail_tool = query.value("Tool").toLongLong();
            detail_Warranty = query.value("Warranty").toLongLong();
            detail_Subpart = query.value("Subpart").toLongLong();
            detail_etcpart = query.value("etcpart").toLongLong();
        }
        qint64 detail_use_money  = detail_Maintenance_Costs+detail_consumables+
                                   detail_Spare_Parts+detail_PM_Parts+detail_tool+detail_Warranty+detail_Subpart+detail_etcpart;
        qint64 detail_remain_money = detail_limit_money - detail_use_money;

//        limit_detail_remain_bar_set->append(detail_remain_money);
        detail_Maintenance_Costs_bar_set->append(detail_Maintenance_Costs);
        detail_consumables_bar_set->append(detail_consumables);
        detail_Spare_Parts_bar_set->append(detail_Spare_Parts);
        detail_PM_Parts_bar_set->append(detail_PM_Parts);
        detail_TOOL_bar_set->append(detail_tool);
        detail_Warranty_bar_set->append(detail_Warranty);
        detail_Subpart_bar_set->append(detail_Subpart);
        detail_etcpart_bar_set->append(detail_etcpart);
        detail_lineseries->append(linevalue_x,detail_use_money);

        linevalue_x++;
        if((start_date.year() == end_date.year()) && (start_date.month() == end_date.month())){
            break;
        }
        j++;
    }
    detail_bar_series->append(detail_consumables_bar_set);
    detail_bar_series->append(detail_Maintenance_Costs_bar_set);
    detail_bar_series->append(detail_Spare_Parts_bar_set);
    detail_bar_series->append(detail_PM_Parts_bar_set);
    detail_bar_series->append(detail_TOOL_bar_set);
    detail_bar_series->append(detail_Warranty_bar_set);
    detail_bar_series->append(detail_Subpart_bar_set);
    detail_bar_series->append(detail_etcpart_bar_set);
//    detail_bar_series->append(limit_detail_remain_bar_set);

    detail_lineseries->setPointLabelsFormat("@yPoint");
    probe_detail_chart->addSeries(detail_lineseries);
    probe_detail_chart->addSeries(detail_bar_series);

    probe_detail_axis_X = new QBarCategoryAxis();
    probe_detail_axis_X->append(detail_categories);
    probe_detail_chart->setAxisX(probe_detail_axis_X, detail_bar_series);
    probe_detail_chart->setAxisX(probe_detail_axis_X,detail_lineseries);
    probe_detail_axis_X->setRange(detail_categories.first(),detail_categories.last());
    probe_detail_axis_Y= new QValueAxis();
    probe_detail_chart->setAxisY(probe_detail_axis_Y,detail_bar_series);
    probe_detail_chart->setAxisY(probe_detail_axis_Y,detail_lineseries);

    qreal max_yvalue = probe_detail_axis_Y->max();
    qreal min_yvalue = probe_detail_axis_Y->min();
    qreal diff_value = max_yvalue - min_yvalue;
    probe_detail_axis_Y->setRange(min_yvalue-(diff_value*0.1),max_yvalue+(diff_value*0.1));
    connect(detail_bar_series,SIGNAL(hovered(bool,int,QBarSet*)),this,SLOT(probe_detail_hover_slot(bool,int,QBarSet*)));
}

void cost_reduction_main::on_all_refresh_btn_clicked()
{
    Cost_Redction_data_table_model *all_model = (Cost_Redction_data_table_model *)ui->all_data_table->model();
    all_model->setFilter(QString("buy_time between '%1' AND '%2' AND `process` IN ('%3','%4','%5','%6')")
                          .arg(ui->all_search_month_start->date().toString("yyyy-MM-01"))
                          .arg(ui->all_search_month_end->date().toString("yyyy-MM-31"))
                         .arg(tr("light")).arg(tr("deposition")).arg(tr("eatching")).arg(tr("probe")));

    all_model->select();
    all_model->sort(0,Qt::DescendingOrder);
    qDebug()<<all_model->query().lastQuery();
    qint64 all_total_limit_money = 0;

    qint64 all_total_deposition_limit_money=0;
    qint64 all_total_light_limit_money=0;
    qint64 all_total_eatching_limit_money=0;
    qint64 all_total_probe_limit_money=0;

    qint64 all_depostion_money = 0;
    qint64 all_light_money = 0;
    qint64 all_eatching_money = 0;
    qint64 all_probe_money = 0;

    QStringList all_use_money_list;
    QSqlQuery query(my_mesdb);
    QString draft_str = "";
    if(ui->all_draft->isChecked()){
        draft_str = "draft";
    }else if(ui->all_draft_calc->isChecked()){
        draft_str = "draft_calculate";
    }
    query.exec(QString("select SUM(limit_cost) as cost_sum,"
                       "SUM(CASE WHEN `process` = '%1' THEN `limit_cost` END) as deposition_limit,"
                       "SUM(CASE WHEN `process` = '%2' THEN `limit_cost` END) as light_limit,"
                       "SUM(CASE WHEN `process` = '%3' THEN `limit_cost` END) as eatching_limit,"
                       "SUM(CASE WHEN `process` = '%4' THEN `limit_cost` END) as probe_limit"
                       " from Cost_reduction_money_limit"
                       " where `month` between '%5' AND '%6'")
                       .arg(tr("deposition"))
                       .arg(tr("light"))
                       .arg(tr("eatching"))
                       .arg(tr("probe"))
                       .arg(ui->all_search_month_start->date().toString("yyyy-MM-01"))
                       .arg(ui->all_search_month_end->date().toString("yyyy-MM-31")));
    if(query.next()){
        all_total_limit_money = query.value("cost_sum").toLongLong();
        all_total_deposition_limit_money = query.value("deposition_limit").toLongLong();
        all_total_light_limit_money = query.value("light_limit").toLongLong();
        all_total_eatching_limit_money = query.value("eatching_limit").toLongLong();
        all_total_probe_limit_money = query.value("probe_limit").toLongLong();
    }
    query.exec(QString("select SUM(CASE WHEN `process` = '%1' THEN `price` END) as deposition,"
                       "SUM(CASE WHEN `process` = '%2' THEN `price` END) as light,"
                       "SUM(CASE WHEN `process` = '%3' THEN `price` END) as eatching,"
                       "SUM(CASE WHEN `process` = '%4' THEN `price` END) as probe"
                       " from Cost_reduction_data"
                       " where `buy_time` between '%5' AND '%6' AND %7 = 2 ")
              .arg(tr("deposition"))
              .arg(tr("light"))
              .arg(tr("eatching"))
              .arg(tr("probe"))
              .arg(ui->all_search_month_start->date().toString("yyyy-MM-01"))
              .arg(ui->all_search_month_end->date().toString("yyyy-MM-31"))
              .arg(draft_str));
    if(query.next()){
        all_depostion_money = query.value("deposition").toLongLong();
        all_light_money = query.value("light").toLongLong();
        all_eatching_money = query.value("eatching").toLongLong();
        all_probe_money = query.value("probe").toLongLong();
    }
    if(all_total_chart == 0){
        all_total_chart = new Cost_Reduction_chart();
        all_total_chart_view = new Cost_Reduction_chart_view(all_total_chart);
        ui->all_total_chart_layout->addWidget(all_total_chart_view);
    }else {
        all_total_chart->removeAllSeries();
        all_total_chart->removeAxis(all_total_chart->axisX());
        all_total_chart->removeAxis(all_total_chart->axisY());
    }

    all_use_money_list.append(QString("%1 = %2 | %3 = %4 | %5 = %6 | %7 = %8 ")
                            .arg(tr("deposition")).arg(QLocale(QLocale::English).toString((qint64)(all_total_deposition_limit_money-all_depostion_money)))
                            .arg(tr("light")).arg(QLocale(QLocale::English).toString((qint64)(all_total_light_limit_money-all_light_money)))
                            .arg(tr("eatching")).arg(QLocale(QLocale::English).toString((qint64)(all_total_eatching_limit_money-all_eatching_money)))
                            .arg(tr("probe")).arg(QLocale(QLocale::English).toString((qint64)(all_total_probe_limit_money-all_probe_money))));

//    total_process_limit_map.insert(tr("total_remain_limit"),all_use_money_list);
    qDebug()<<QString("%1").arg(all_total_deposition_limit_money);
    total_process_limit_map.insert(tr("deposition"),QStringList(QString("%1").arg(all_total_deposition_limit_money)));
    total_process_limit_map.insert(tr("light"),QStringList(QString("%1").arg(all_total_light_limit_money)));
    total_process_limit_map.insert(tr("eatching"),QStringList(QString("%1").arg(all_total_eatching_limit_money)));
    total_process_limit_map.insert(tr("probe"),QStringList(QString("%1").arg(all_total_probe_limit_money)));


    QStackedBarSeries *all_bar_series = new QStackedBarSeries();
    all_bar_series->setLabelsFormat("@comma_long_value");
//    QBarSet *limit_remain_total_bar_set = new QBarSet(tr("total_remain_limit"));
//    limit_remain_total_bar_set->setLabelColor(QColor("black"));
//    limit_remain_total_bar_set->setPen(QColor("black"));
//    limit_remain_total_bar_set->setColor(QColor(Qt::transparent));
    QBarSet *total_deposition_bar_set = new QBarSet(tr("deposition"));
    total_deposition_bar_set->setLabelColor(QColor("black"));
    QBarSet *total_light_bar_set = new QBarSet(tr("light"));
    total_light_bar_set->setLabelColor(QColor("black"));
    QBarSet *total_eatching_bar_set = new QBarSet(tr("eatching"));
    total_eatching_bar_set->setLabelColor(QColor("black"));
    QBarSet *total_probe_bar_set = new QBarSet(tr("probe"));
    total_probe_bar_set->setLabelColor(QColor("black"));

    all_bar_series->setLabelsVisible(true);

    total_deposition_bar_set->append(all_depostion_money);
    total_light_bar_set->append(all_light_money);
    total_eatching_bar_set->append(all_eatching_money);
    total_probe_bar_set->append(all_probe_money);
    qint64 remain_money = all_total_limit_money-(all_depostion_money+all_light_money+all_eatching_money+all_probe_money);
//    limit_remain_total_bar_set->append(remain_money);


    all_bar_series->append(total_deposition_bar_set);
    all_bar_series->append(total_light_bar_set);
    all_bar_series->append(total_eatching_bar_set);
    all_bar_series->append(total_probe_bar_set);
//    all_bar_series->append(limit_remain_total_bar_set);



    all_total_chart->addSeries(all_bar_series);
    QStringList total_categories;
    total_categories << "total";
    QBarCategoryAxis *total_axis = new QBarCategoryAxis();
    total_axis->append(total_categories);
    all_total_chart->createDefaultAxes();
    all_total_chart->setAxisX(total_axis, all_bar_series);
    qreal total_max_yvalue = ((QValueAxis *)all_total_chart->axisY())->max();
    qreal total_min_yvalue = ((QValueAxis *)all_total_chart->axisY())->min();
    qreal total_diff_value = total_max_yvalue - total_min_yvalue;
    all_total_chart->axisY()->setRange(total_min_yvalue-(total_diff_value*0.1),total_max_yvalue+(total_diff_value*0.1));



    connect(all_bar_series,SIGNAL(hovered(bool,int,QBarSet*)),this,SLOT(all_total_hover_slot(bool,int,QBarSet*)));

    if(all_detail_chart == 0){
        all_detail_chart = new Cost_Reduction_chart();
        all_detail_chart_view = new Cost_Reduction_chart_view(all_detail_chart);
        ui->all_detail_chart_layuout->addWidget(all_detail_chart_view);
    }else {
        all_detail_chart->removeAllSeries();
        all_detail_chart->removeAxis(all_detail_axis_X);
        all_detail_chart->removeAxis(all_detail_axis_Y);
    }


    QStackedBarSeries *detail_all_bar_series = new QStackedBarSeries();
    detail_all_bar_series->setLabelsFormat("@comma_long_value");
    detail_all_bar_series->setName("detail");
    detail_all_bar_series->setLabelsVisible(true);
//    QBarSet *limit_detail_remain_bar_set = new QBarSet(tr("month_remain_limit"));
//    limit_detail_remain_bar_set->setLabelColor("black");
//    limit_detail_remain_bar_set->setPen(QColor("black"));
//    limit_detail_remain_bar_set->setColor(QColor(Qt::transparent));
    QStringList detail_categories;
    QBarSet *deposition_detail_bar_set = new QBarSet(tr("deposition"));
    deposition_detail_bar_set->setLabelColor("black");
    deposition_detail_bar_set->setColor(total_deposition_bar_set->color());
    QBarSet *light_detail_bar_set = new QBarSet(tr("light"));
    light_detail_bar_set->setLabelColor("black");
    light_detail_bar_set->setColor(total_light_bar_set->color());
    QBarSet *eatching_detail_bar_set = new QBarSet(tr("eatching"));
    eatching_detail_bar_set->setLabelColor("black");
    eatching_detail_bar_set->setColor(total_eatching_bar_set->color());
    QBarSet *probe_detail_bar_set = new QBarSet(tr("probe"));
    probe_detail_bar_set->setLabelColor("black");
    probe_detail_bar_set->setColor(total_probe_bar_set->color());


    QLineSeries *detail_lineseries = new QLineSeries();
    detail_lineseries->setName(tr("use_price"));
    detail_lineseries->setPointsVisible(true);
//    detail_lineseries->setPointLabelsVisible(true);

    QStringList deposition_limit_list;
    QStringList light_limit_list;
    QStringList eatching_limit_list;
    QStringList probe_limit_list;
    QStringList use_moeny_list;

    int linevalue_x = 0;
    int j=0;
    while(true){

        QDate start_date =  ui->all_search_month_start->date().addMonths(j);
        QDate end_date = ui->all_search_month_end->date();

        detail_categories.append(start_date.toString("yy-MM"));


        qreal all_detail_limit_money=0;
        qreal all_detail_deposition_limit_money=0;
        qreal all_detail_light_limit_money=0;
        qreal all_detail_eatching_limit_money=0;
        qreal all_detail_probe_limit_money=0;
        qreal all_detail_deposition_money=0;
        qreal all_detail_light_money=0;
        qreal all_detail_eatching_money=0;
        qreal all_detail_probe_money=0;

        query.exec(QString("select SUM(limit_cost) as cost_sum,"
                           "SUM(CASE WHEN `process` = '%1' THEN `limit_cost` END) as deposition_limit,"
                           "SUM(CASE WHEN `process` = '%2' THEN `limit_cost` END) as light_limit,"
                           "SUM(CASE WHEN `process` = '%3' THEN `limit_cost` END) as eatching_limit,"
                           "SUM(CASE WHEN `process` = '%4' THEN `limit_cost` END) as probe_limit"
                           " from Cost_reduction_money_limit"
                           " where `month` between '%5' AND '%6'   ")
                  .arg(tr("deposition"))
                  .arg(tr("light"))
                  .arg(tr("eatching"))
                  .arg(tr("probe"))
                  .arg(start_date.toString("yyyy-MM-01"))
                  .arg(start_date.toString("yyyy-MM-31")));
        if(query.next()){
            all_detail_limit_money = query.value("cost_sum").toReal();
            all_detail_deposition_limit_money = query.value("deposition_limit").toReal();
            all_detail_light_limit_money = query.value("light_limit").toReal();
            all_detail_eatching_limit_money = query.value("eatching_limit").toReal();
            all_detail_probe_limit_money = query.value("probe_limit").toReal();
            deposition_limit_list.append(QString("%1").arg((qint64)all_detail_deposition_limit_money));
            light_limit_list.append(QString("%1").arg((qint64)all_detail_light_limit_money));
            eatching_limit_list.append(QString("%1").arg((qint64)all_detail_eatching_limit_money));
            probe_limit_list.append(QString("%1").arg((qint64)all_detail_probe_limit_money));


        }

        query.exec(QString("select SUM(CASE WHEN `process` = '%1' THEN `price` END) as deposition_sum, "
                           "SUM(CASE WHEN `process` = '%2' THEN `price` END) as light_sum,"
                           "SUM(CASE WHEN `process` = '%3' THEN `price` END) as eatching_sum,"
                           "SUM(CASE WHEN `process` = '%4' THEN `price` END) as probe_sum "
                           "from Cost_reduction_data "
                           "where `buy_time` between '%5' AND '%6' AND %7 = 2 ")
                  .arg(tr("deposition"))
                  .arg(tr("light"))
                  .arg(tr("eatching"))
                  .arg(tr("probe"))
                  .arg(start_date.toString("yyyy-MM-01"))
                  .arg(start_date.toString("yyyy-MM-31"))
                  .arg(draft_str));

        if(query.next()){
            all_detail_deposition_money = query.value("deposition_sum").toReal();
            all_detail_light_money = query.value("light_sum").toReal();
            all_detail_eatching_money = query.value("eatching_sum").toReal();
            all_detail_probe_money = query.value("probe_sum").toReal();
        }
        use_moeny_list.append(QString("%1 = %2 | %3 = %4 | %5 = %6 | %7 = %8 ")
                                .arg(tr("deposition")).arg(QLocale(QLocale::English).toString((qint64)(all_detail_deposition_limit_money-all_detail_deposition_money)))
                                .arg(tr("light")).arg(QLocale(QLocale::English).toString((qint64)(all_detail_light_limit_money-all_detail_light_money)))
                                .arg(tr("eatching")).arg(QLocale(QLocale::English).toString((qint64)(all_detail_eatching_limit_money-all_detail_eatching_money)))
                                .arg(tr("probe")).arg(QLocale(QLocale::English).toString((qint64)(all_detail_probe_limit_money-all_detail_probe_money))));




        deposition_detail_bar_set->append(all_detail_deposition_money);
        light_detail_bar_set->append(all_detail_light_money);
        eatching_detail_bar_set->append(all_detail_eatching_money);
        probe_detail_bar_set->append(all_detail_probe_money);
//        limit_detail_remain_bar_set->append(all_detail_limit_money-(all_detail_deposition_money+all_detail_light_money
//                                                                    +all_detail_eatching_money+all_detail_probe_money));
        detail_lineseries->append(linevalue_x,all_detail_deposition_money+all_detail_light_money+all_detail_eatching_money+all_detail_probe_money);
        linevalue_x++;
        if((start_date.year() == end_date.year()) && (start_date.month() == end_date.month())){
            break;
        }
        j++;
    }

    detail_process_limit_map.insert(tr("deposition"),deposition_limit_list);
    detail_process_limit_map.insert(tr("light"),light_limit_list);
    detail_process_limit_map.insert(tr("eatching"),eatching_limit_list);
    detail_process_limit_map.insert(tr("probe"),probe_limit_list);
    detail_process_limit_map.insert(tr("month_remain_limit"),use_moeny_list);

    detail_all_bar_series->append(deposition_detail_bar_set);
    detail_all_bar_series->append(light_detail_bar_set);
    detail_all_bar_series->append(eatching_detail_bar_set);
    detail_all_bar_series->append(probe_detail_bar_set);
//    detail_all_bar_series->append(limit_detail_remain_bar_set);

    detail_lineseries->setPointLabelsFormat("@yPoint");
    all_detail_chart->addSeries(detail_lineseries);
    all_detail_chart->addSeries(detail_all_bar_series);
    all_detail_axis_X = new QBarCategoryAxis();
    all_detail_axis_X->append(detail_categories);
    all_detail_chart->setAxisX(all_detail_axis_X, detail_all_bar_series);
    all_detail_chart->setAxisX(all_detail_axis_X,detail_lineseries);
    all_detail_axis_X->setRange(detail_categories.first(),detail_categories.last());
    all_detail_axis_Y= new QValueAxis();

    all_detail_chart->setAxisY(all_detail_axis_Y,detail_all_bar_series);
    all_detail_chart->setAxisY(all_detail_axis_Y,detail_lineseries);

    qreal max_yvalue = all_detail_axis_Y->max();
    qreal min_yvalue = all_detail_axis_Y->min();
    qreal diff_value = max_yvalue - min_yvalue;
    all_detail_axis_Y->setRange(min_yvalue-(diff_value*0.1),max_yvalue+(diff_value*0.1));

    connect(detail_all_bar_series,SIGNAL(hovered(bool,int,QBarSet*)),this,SLOT(all_detail_hover_slot(bool,int,QBarSet*)));
}

void cost_reduction_main::on_depostion_total_zoom_reset_clicked()
{
    deposition_total_chart->zoomReset();
}
void cost_reduction_main::on_depostion_detail_zoom_reset_clicked()
{
    deposition_detail_chart->zoomReset();
}
void cost_reduction_main::on_light_total_zoom_reset_clicked()
{
    light_total_chart->zoomReset();
}
void cost_reduction_main::on_light_detail_zoom_reset_clicked()
{
    light_detail_chart->zoomReset();
}
void cost_reduction_main::on_eatching_total_zoom_reset_clicked()
{
    eatching_total_chart->zoomReset();
}


void cost_reduction_main::on_eatching_detail_zoom_reset_clicked()
{
    eatching_detail_chart->zoomReset();
}

void cost_reduction_main::on_probe_total_zoom_reset_clicked()
{
    probe_total_chart->zoomReset();
}

void cost_reduction_main::on_probe_detail_zoom_reset_clicked()
{
    probe_detail_chart->zoomReset();
}

void cost_reduction_main::on_all_total_zoom_reset_clicked()
{
     all_total_chart->zoomReset();
}
void cost_reduction_main::on_all_detail_zoom_reset_clicked()
{
     all_detail_chart->zoomReset();
}
void cost_reduction_main::deposition_total_hover_slot(bool status, int index, QBarSet *barset)
{
    if(status){
        ui->depostion_totaLchart_hover_label->setText(QString("%1 = %2").arg(barset->label()).arg(QLocale(QLocale::English).toString((qint64)barset->at(index))));
    }
}

void cost_reduction_main::deposition_detail_hover_slot(bool status, int index, QBarSet *barset)
{
    if(status){
        int total = 0;
        QStackedBarSeries *series = (QStackedBarSeries *)deposition_detail_chart->series().at(1);
        QList<QBarSet *> barsets = series->barSets();
        for(int i=0;i<barsets.count();i++){
            total += barsets.at(i)->at(index);
        }
        ui->depostion_detail_chart_hover_label->setText(QString("%1 = %2 , total = %3").arg(barset->label())
                                                    .arg(QLocale(QLocale::English).toString((qint64)barset->at(index)))
                                                    .arg(QLocale(QLocale::English).toString(total)));
    }
}

void cost_reduction_main::light_total_hover_slot(bool status, int index, QBarSet *barset)
{
    if(status){
        ui->light_totaLchart_hover_label->setText(QString("%1 = %2").arg(barset->label()).arg(QLocale(QLocale::English).toString((qint64)barset->at(index))));
    }
}

void cost_reduction_main::light_detail_hover_slot(bool status, int index, QBarSet *barset)
{
    if(status){
        int total = 0;
        QStackedBarSeries *series = (QStackedBarSeries *)light_detail_chart->series().at(1);
        QList<QBarSet *> barsets = series->barSets();
        for(int i=0;i<barsets.count();i++){
            total += barsets.at(i)->at(index);
        }
        ui->light_detail_chart_hover_label->setText(QString("%1 = %2 , total = %3").arg(barset->label())
                                                    .arg(QLocale(QLocale::English).toString((qint64)barset->at(index)))
                                                    .arg(QLocale(QLocale::English).toString(total)));
    }
}

void cost_reduction_main::eatching_total_hover_slot(bool status, int index, QBarSet *barset)
{
    if(status){
        ui->eatching_totaLchart_hover_label->setText(QString("%1 = %2").arg(barset->label()).arg(QLocale(QLocale::English).toString((qint64)barset->at(index))));
    }
}

void cost_reduction_main::eatching_detail_hover_slot(bool status, int index, QBarSet *barset)
{
    if(status){
        int total = 0;
        QStackedBarSeries *series = (QStackedBarSeries *)eatching_detail_chart->series().at(1);
        QList<QBarSet *> barsets = series->barSets();
        for(int i=0;i<barsets.count();i++){
            total += barsets.at(i)->at(index);
        }
        ui->eatching_detail_chart_hover_label->setText(QString("%1 = %2 , total = %3").arg(barset->label())
                                                    .arg(QLocale(QLocale::English).toString((qint64)barset->at(index)))
                                                    .arg(QLocale(QLocale::English).toString(total)));
    }
}

void cost_reduction_main::probe_total_hover_slot(bool status, int index, QBarSet *barset)
{
    if(status){
        ui->probe_totaLchart_hover_label->setText(QString("%1 = %2").arg(barset->label()).arg(QLocale(QLocale::English).toString((qint64)barset->at(index))));
    }
}

void cost_reduction_main::probe_detail_hover_slot(bool status, int index, QBarSet *barset)
{
    if(status){
        int total = 0;
        QStackedBarSeries *series = (QStackedBarSeries *)probe_detail_chart->series().at(1);
        QList<QBarSet *> barsets = series->barSets();
        for(int i=0;i<barsets.count();i++){
            total += barsets.at(i)->at(index);
        }
        ui->probe_detail_chart_hover_label->setText(QString("%1 = %2,total = %3").arg(barset->label())
                                                    .arg(QLocale(QLocale::English).toString((qint64)barset->at(index)))
                                                    .arg(QLocale(QLocale::English).toString(total)));
    }
}

void cost_reduction_main::all_total_hover_slot(bool status, int index, QBarSet *barset)
{
    if(status){
        if(barset->label() == tr("total_remain_limit")){
            if(total_process_limit_map.contains(barset->label())){
               QString process_use_moeny = total_process_limit_map.value(tr("total_remain_limit")).at(index);

                ui->all_totaLchart_hover_label->setText(QString("%1 = %2 | %3 ")
                                                          .arg(barset->label()).arg(QLocale(QLocale::English).toString((qint64)barset->at(index)))
                                                          .arg(process_use_moeny));
            }
        }else {
            if(total_process_limit_map.contains(barset->label())){
                QString process_limit_data = total_process_limit_map.value(barset->label()).at(index);
                QVariant process_limit_value = QVariant(process_limit_data);
                QString process_limit_str = QLocale(QLocale::English).toString((qint64)process_limit_value.toLongLong());
                qint64 use_money = ((qint64)process_limit_value.toLongLong()-(qint64)barset->at(index));

                ui->all_totaLchart_hover_label->setText(QString("%1 = %2 | %3 = %4 | %5 = %6 ")
                                                          .arg(barset->label()).arg(QLocale(QLocale::English).toString((qint64)barset->at(index)))
                                                          .arg(tr("give moeny")).arg(process_limit_str)
                                                          .arg(tr("can use moeny")).arg(QLocale(QLocale::English).toString(use_money)));

               }
        }

//        ui->all_totaLchart_hover_label->setText(QString("%1 = %2").arg(barset->label()).arg(QLocale(QLocale::English).toString((qint64)barset->at(index))));
    }
}

void cost_reduction_main::all_detail_hover_slot(bool status, int index, QBarSet *barset)
{
    if(status){
        if(barset->label() == tr("month_remain_limit")){
             if(detail_process_limit_map.contains(barset->label())){
                QString process_use_moeny = detail_process_limit_map.value(tr("month_remain_limit")).at(index);

                 ui->all_detail_chart_hover_label->setText(QString("%1 = %2 | %3 ")
                                                           .arg(barset->label()).arg(QLocale(QLocale::English).toString((qint64)barset->at(index)))
                                                           .arg(process_use_moeny));
             }
        }else {
            if(detail_process_limit_map.contains(barset->label())){
                QString process_limit_data = detail_process_limit_map.value(barset->label()).at(index);
                QVariant process_limit_value = QVariant(process_limit_data);
                QString process_limit_str = QLocale(QLocale::English).toString(process_limit_value.toLongLong());
                qint64 use_money = (process_limit_value.toLongLong()-(qint64)barset->at(index));

                ui->all_detail_chart_hover_label->setText(QString("%1 = %2 | %3 = %4 | %5 = %6 ")
                                                          .arg(barset->label()).arg(QLocale(QLocale::English).toString((qint64)barset->at(index)))
                                                          .arg(tr("give moeny")).arg(process_limit_str)
                                                          .arg(tr("can use moeny")).arg(QLocale(QLocale::English).toString(use_money)));

               }
        }
    }

}

void cost_reduction_main::share_total_hover_slot(bool status, int index, QBarSet *barset)
{
    if(status){
        int total = 0;
        for(int i=0; i<barset->count();i++){
            total += barset->at(i);
        }
        ui->share_totaLchart_hover_label->setText(QString("%1 = %2, total = %3 ").arg(barset->label())
                                                  .arg(QLocale(QLocale::English).toString((qint64)barset->at(index)))
                                                  .arg(total));
    }
}

void cost_reduction_main::share_detail_hover_slot(bool status, int index, QBarSet *barset)
{
    if(status){
        int total = 0;
        QStackedBarSeries *series = (QStackedBarSeries *)share_detail_chart->series().at(1);
        QList<QBarSet *> barsets = series->barSets();
        for(int i=0;i<barsets.count();i++){
            total += barsets.at(i)->at(index);
        }
        ui->share_detail_chart_hover_label->setText(QString("%1 = %2 , total = %3").arg(barset->label())
                                                    .arg(QLocale(QLocale::English).toString((qint64)barset->at(index)))
                                                    .arg(QLocale(QLocale::English).toString(total)));
    }
}

void cost_reduction_main::deposition_auto_calc_total_price(QModelIndex index1, QModelIndex index2, QVector<int> roles)
{
    Cost_Redction_data_table_model *depostion_model = (Cost_Redction_data_table_model *)ui->deposition_data_table->model();;
    if((index1.column() == 10 && index2.column() == 10)||(index1.column() == 7 && index2.column() == 7)){
        int unit_price = depostion_model->index(index1.row(),10).data().toInt();
        int amount = depostion_model->index(index1.row(),7).data().toInt();
        QSqlRecord recode_1 = depostion_model->record(index1.row());
        recode_1.setGenerated(10,true);
        recode_1.setGenerated(7,true);
        recode_1.setGenerated(11,true);
        recode_1.setValue(11,unit_price*amount);
        depostion_model->setRecord(index1.row(),recode_1);
    }
}

void cost_reduction_main::light_auto_calc_total_price(QModelIndex index1, QModelIndex index2, QVector<int> roles)
{
    Cost_Redction_data_table_model *light_model = (Cost_Redction_data_table_model *)ui->light_data_table->model();
    if((index1.column() == 10 && index2.column() == 10)||(index1.column() == 7 && index2.column() == 7)){
        int unit_price = light_model->index(index1.row(),10).data().toInt();
        int amount = light_model->index(index1.row(),7).data().toInt();
        QSqlRecord recode_1 = light_model->record(index1.row());
        recode_1.setGenerated(10,true);
        recode_1.setGenerated(7,true);
        recode_1.setGenerated(11,true);
        recode_1.setValue(11,unit_price*amount);
        light_model->setRecord(index1.row(),recode_1);
    }
}

void cost_reduction_main::eatching_auto_calc_total_price(QModelIndex index1, QModelIndex index2, QVector<int> roles)
{
    Cost_Redction_data_table_model *eatching_model = (Cost_Redction_data_table_model *)ui->eatching_data_table->model();;
    if((index1.column() == 10 && index2.column() == 10)||(index1.column() == 7 && index2.column() == 7)){
        int unit_price = eatching_model->index(index1.row(),10).data().toInt();
        int amount = eatching_model->index(index1.row(),7).data().toInt();
        QSqlRecord recode_1 = eatching_model->record(index1.row());
        recode_1.setGenerated(10,true);
        recode_1.setGenerated(7,true);
        recode_1.setGenerated(11,true);
        recode_1.setValue(11,unit_price*amount);
        eatching_model->setRecord(index1.row(),recode_1);
    }
}

void cost_reduction_main::probe_auto_calc_total_price(QModelIndex index1, QModelIndex index2, QVector<int> roles)
{
    Cost_Redction_data_table_model *probe_model = (Cost_Redction_data_table_model *)ui->probe_data_table->model();;
    if((index1.column() == 10 && index2.column() == 10)||(index1.column() == 7 && index2.column() == 7)){
        int unit_price = probe_model->index(index1.row(),10).data().toInt();
        int amount = probe_model->index(index1.row(),7).data().toInt();
        QSqlRecord recode_1 = probe_model->record(index1.row());
        recode_1.setGenerated(10,true);
        recode_1.setGenerated(7,true);
        recode_1.setGenerated(11,true);
        recode_1.setValue(11,unit_price*amount);
        probe_model->setRecord(index1.row(),recode_1);
    }
}

void cost_reduction_main::all_auto_calc_total_price(QModelIndex index1, QModelIndex index2, QVector<int> roles)
{
    Cost_Redction_data_table_model *all_model = (Cost_Redction_data_table_model *)ui->all_data_table->model();;
    if((index1.column() == 10 && index2.column() == 10)||(index1.column() == 7 && index2.column() == 7)){
        int unit_price = all_model->index(index1.row(),10).data().toInt();
        int amount = all_model->index(index1.row(),7).data().toInt();
        QSqlRecord recode_1 = all_model->record(index1.row());
        recode_1.setGenerated(10,true);
        recode_1.setGenerated(7,true);
        recode_1.setGenerated(11,true);
        recode_1.setValue(11,unit_price*amount);
        all_model->setRecord(index1.row(),recode_1);
    }
}

void cost_reduction_main::share_auto_calc_total_price(QModelIndex index1, QModelIndex index2, QVector<int> roles)
{
    Cost_Redction_data_table_model *model = (Cost_Redction_data_table_model *)ui->share_data_table->model();
    if((index1.column() == model->fieldIndex("unit price") && index2.column() == model->fieldIndex("unit price"))||
            (index1.column() == model->fieldIndex("order_amount") && index2.column() == model->fieldIndex("order_amount"))){
        int unit_price = model->index(index1.row(),model->fieldIndex("unit price")).data().toInt();
        int amount = model->index(index1.row(),model->fieldIndex("order_amount")).data().toInt();
        QSqlRecord recode_1 = model->record(index1.row());
        recode_1.setGenerated(model->fieldIndex("unit price"),true);
        recode_1.setGenerated(model->fieldIndex("order_amount"),true);
        recode_1.setGenerated(model->fieldIndex("price"),true);
        recode_1.setValue(model->fieldIndex("price"),unit_price*amount);
        model->setRecord(index1.row(),recode_1);
    }
}


void cost_reduction_main::on_share_add_row_clicked()
{
    Cost_Redction_data_table_model *model = (Cost_Redction_data_table_model *)ui->share_data_table->model();
    model->insertRow(model->rowCount());
    model->setData(model->index(model->rowCount()-1,model->fieldIndex("process")),tr("share"));
    model->setData(model->index(model->rowCount()-1,model->fieldIndex("buy_time")),QDate::currentDate());
    model->setData(model->index(model->rowCount()-1,model->fieldIndex("draft")),2);
    model->submitAll();
}

void cost_reduction_main::on_share_del_row_clicked()
{
    Cost_Redction_data_table_model *model = (Cost_Redction_data_table_model *)ui->share_data_table->model();
    int count = ui->share_data_table->selectionModel()->selectedIndexes().count();
    QVector<int> row_history;
    for(int i=0;i<count;i++){
        if(!row_history.contains(ui->share_data_table->selectionModel()->selectedIndexes().at(i).row())){
            model->removeRow(ui->share_data_table->selectionModel()->selectedIndexes().at(i).row());
            row_history.append(ui->share_data_table->selectionModel()->selectedIndexes().at(i).row());
        }else {

        }
    }
    model->select();
}

void cost_reduction_main::on_share_refresh_btn_clicked()
{
    Cost_Redction_data_table_model *model = (Cost_Redction_data_table_model *)ui->share_data_table->model();
    model->setFilter(QString("process = '%1' AND buy_time between '%2' AND '%3' ").arg(tr("share"))
                          .arg(ui->share_search_month_start->date().toString("yyyy-MM-01"))
                          .arg(ui->share_search_month_end->date().toString("yyyy-MM-31")));

    model->select();
    model->sort(model->fieldIndex("buy_time"),Qt::DescendingOrder);
    qint64 total_PR = 0;
    qint64 total_develop = 0;
    qint64 total_strip=0;
    qint64 total_etc =0;
    qint64 total_clean_ware= 0;
    qint64 total_consumable_supplies =0;
    qint64 total_stationery =0;
    share_list<<tr("PR")<<tr("develop")<<tr("strip")<<tr("etc")<<tr("Clean_ware")<<tr("Consumable_supplies");
    QSqlQuery query(my_mesdb);
    query.exec(QString("select sum(CASE WHEN `classification` = '%1' THEN price END) as PR, "
                       "sum(CASE WHEN `classification` = '%2' THEN price END) as develop,"
                       "sum(CASE WHEN `classification` = '%3' THEN price END) as strip,"
                       "sum(CASE WHEN `classification` = '%4' THEN price END) as etc,"
                       "sum(CASE WHEN `classification` = '%5' THEN price END) as Clean_ware,"
                       "sum(CASE WHEN `classification` = '%6' THEN price END) as Consumable_supplies, "
                       "sum(CASE WHEN `classification` = '%7' THEN price END) as stationery "
                       "from Cost_reduction_data where `process` = '%8' AND buy_time between '%9' AND '%10'")
               .arg(tr("PR")).arg(tr("develop")).arg(tr("strip"))
               .arg(tr("etc")).arg(tr("Clean_ware")).arg(tr("Consumable_supplies")).arg(tr("stationery"))
               .arg(tr("share"))
               .arg(ui->share_search_month_start->date().toString("yyyy-MM-01"))
               .arg(ui->share_search_month_end->date().toString("yyyy-MM-31")));
    qDebug()<<query.lastQuery();
    qDebug()<<query.lastError().text();
    if(query.next()){
        total_PR = query.value("PR").toLongLong();
        total_develop = query.value("develop").toLongLong();
        total_strip = query.value("strip").toLongLong();
        total_etc= query.value("etc").toLongLong();
        total_clean_ware = query.value("Clean_ware").toLongLong();
        total_consumable_supplies = query.value("Consumable_supplies").toLongLong();
        total_stationery = query.value("stationery").toLongLong();
    }

    if(share_total_chart == 0){
        share_total_chart = new Cost_Reduction_chart();
        share_total_chart_view = new Cost_Reduction_chart_view(share_total_chart);
        ui->share_total_chart_layuot->addWidget(share_total_chart_view);
    }else {
        share_total_chart->removeAllSeries();
        share_total_chart->removeAxis(share_total_chart->axisX());
        share_total_chart->removeAxis(share_total_chart->axisY());
    }

    QStackedBarSeries *total_bar_series = new QStackedBarSeries();
    total_bar_series->setLabelsFormat("@comma_long_value");


    QBarSet *total_PR_bar_set = new QBarSet(tr("PR"));
    total_PR_bar_set->setLabelColor(QColor("black"));

    QBarSet *total_develop_bar_set = new QBarSet(tr("develop"));
    total_develop_bar_set->setLabelColor("black");

    QBarSet *total_strip_bar_set = new QBarSet(tr("strip"));
    total_strip_bar_set->setLabelColor("black");

    QBarSet *total_etc_bar_set = new QBarSet(tr("etc"));
    total_etc_bar_set->setLabelColor("black");

    QBarSet *total_Clean_ware_bar_set = new QBarSet(tr("Clean_ware"));
    total_Clean_ware_bar_set->setLabelColor("black");

    QBarSet *total_Consumable_supplies_bar_set = new QBarSet(tr("Consumable_supplies"));
    total_Consumable_supplies_bar_set->setLabelColor("black");

    QBarSet *total_stationery_bar_set = new QBarSet(tr("stationery"));
    total_stationery_bar_set->setLabelColor("black");

    total_bar_series->setLabelsVisible(true);


    total_PR_bar_set->append(total_PR);
    total_develop_bar_set->append(total_develop);
    total_strip_bar_set->append(total_strip);
    total_etc_bar_set->append(total_etc);
    total_Clean_ware_bar_set->append(total_clean_ware);
    total_Consumable_supplies_bar_set->append(total_consumable_supplies);
    total_stationery_bar_set->append(total_stationery);



    total_bar_series->append(total_PR_bar_set);
    total_bar_series->append(total_develop_bar_set);
    total_bar_series->append(total_strip_bar_set);
    total_bar_series->append(total_etc_bar_set);
    total_bar_series->append(total_Clean_ware_bar_set);
    total_bar_series->append(total_Consumable_supplies_bar_set);
    total_bar_series->append(total_stationery_bar_set);

    share_total_chart->addSeries(total_bar_series);
    QStringList total_categories;
    total_categories << "total";
    QBarCategoryAxis *total_axis = new QBarCategoryAxis();
    total_axis->append(total_categories);
    share_total_chart->createDefaultAxes();
    share_total_chart->setAxisX(total_axis, total_bar_series);

    qreal total_max_yvalue = ((QValueAxis *)share_total_chart->axisY())->max();
    qreal total_min_yvalue = ((QValueAxis *)share_total_chart->axisY())->min();
    qreal total_diff_value = total_max_yvalue - total_min_yvalue;
    share_total_chart->axisY()->setRange(total_min_yvalue-(total_diff_value*0.1),total_max_yvalue+(total_diff_value*0.1));

    connect(total_bar_series,SIGNAL(hovered(bool,int,QBarSet*)),this,SLOT(share_total_hover_slot(bool,int,QBarSet*)));


    if(share_detail_chart == 0){
        share_detail_chart = new Cost_Reduction_chart();
        share_detail_chart_view = new Cost_Reduction_chart_view(share_detail_chart);
        ui->share_detail_chart_layuot->addWidget(share_detail_chart_view);
    }else {
        share_detail_chart->removeAllSeries();
        share_detail_chart->removeAxis(share_detail_axis_X);
        share_detail_chart->removeAxis(share_detail_axis_Y);
    }


    QStackedBarSeries *detail_bar_series = new QStackedBarSeries();
    detail_bar_series->setLabelsFormat("@comma_long_value");
    detail_bar_series->setName("detail");
    detail_bar_series->setLabelsVisible(true);


    QBarSet *detail_PR_bar_set = new QBarSet(tr("PR"));
    detail_PR_bar_set->setLabelColor(QColor("black"));
    detail_PR_bar_set->setColor(total_PR_bar_set->color());

    QBarSet *detail_develop_bar_set = new QBarSet(tr("develop"));
    detail_develop_bar_set->setLabelColor("black");
    detail_develop_bar_set->setColor(total_develop_bar_set->color());

    QBarSet *detail_strip_bar_set = new QBarSet(tr("strip"));
    detail_strip_bar_set->setLabelColor("black");
    detail_strip_bar_set->setColor(total_strip_bar_set->color());

    QBarSet *detail_etc_bar_set = new QBarSet(tr("etc"));
    detail_etc_bar_set->setLabelColor("black");
    detail_etc_bar_set->setColor(total_etc_bar_set->color());

    QBarSet *detail_Clean_ware_bar_set = new QBarSet(tr("Clean_ware"));
    detail_Clean_ware_bar_set->setLabelColor("black");
    detail_Clean_ware_bar_set->setColor(total_Clean_ware_bar_set->color());

    QBarSet *detail_Consumable_supplies_bar_set = new QBarSet(tr("Consumable_supplies"));
    detail_Consumable_supplies_bar_set->setLabelColor("black");
    detail_Consumable_supplies_bar_set->setColor(total_Consumable_supplies_bar_set->color());

    QBarSet *detail_stationery_bar_set = new QBarSet(tr("stationery"));
    detail_stationery_bar_set->setLabelColor("black");
    detail_stationery_bar_set->setColor(total_stationery_bar_set->color());

    QStringList detail_categories;

    QLineSeries *detail_lineseries = new QLineSeries();
    detail_lineseries->setName(tr("use_price"));
    detail_lineseries->setPointsVisible(true);
//    detail_lineseries->setPointLabelsVisible(true);


    int linevalue_x = 0;

    int j=0;
    while(true){
        QDate start_date =  ui->share_search_month_start->date().addMonths(j);
        QDate end_date = ui->share_search_month_end->date();

        detail_categories.append(start_date.toString("yy-MM"));

        qint64 detail_PR = 0;
        qint64 detail_develop = 0;
        qint64 detail_strip=0;
        qint64 detail_etc =0;
        qint64 detail_Clean_ware= 0;
        qint64 detail_Consumable_supplies =0;
        qint64 detail_stationery =0;

        query.exec(QString("select sum(CASE WHEN `classification` = '%1' THEN price END) as PR, "
                           "sum(CASE WHEN `classification` = '%2' THEN price END) as develop,"
                           "sum(CASE WHEN `classification` = '%3' THEN price END) as strip,"
                           "sum(CASE WHEN `classification` = '%4' THEN price END) as etc,"
                           "sum(CASE WHEN `classification` = '%5' THEN price END) as Clean_ware,"
                           "sum(CASE WHEN `classification` = '%6' THEN price END) as Consumable_supplies, "
                           "sum(CASE WHEN `classification` = '%7' THEN price END) as stationery "
                           "from Cost_reduction_data where `process` = '%8' AND buy_time between '%9' AND '%10' ")
                   .arg(tr("PR")).arg(tr("develop")).arg(tr("strip"))
                   .arg(tr("etc")).arg(tr("Clean_ware")).arg(tr("Consumable_supplies")).arg(tr("stationery"))
                   .arg(tr("share"))
                   .arg(start_date.toString("yyyy-MM-01"))
                   .arg(start_date.toString("yyyy-MM-31")));

        if(query.next()){
            detail_PR = query.value("PR").toLongLong();
            detail_develop = query.value("develop").toLongLong();
            detail_strip = query.value("strip").toLongLong();
            detail_etc= query.value("etc").toLongLong();
            detail_Clean_ware = query.value("Clean_ware").toLongLong();
            detail_Consumable_supplies = query.value("Consumable_supplies").toLongLong();
            detail_stationery = query.value("stationery").toLongLong();
        }
        qint64 detail_use_money  = detail_PR+detail_develop+
                                   detail_strip+detail_etc+
                                   detail_Clean_ware+detail_Consumable_supplies+detail_stationery;

        detail_PR_bar_set->append(detail_PR);
        detail_develop_bar_set->append(detail_develop);
        detail_strip_bar_set->append(detail_strip);
        detail_etc_bar_set->append(detail_etc);
        detail_Clean_ware_bar_set->append(detail_Clean_ware);
        detail_Consumable_supplies_bar_set->append(detail_Consumable_supplies);
        detail_stationery_bar_set->append(detail_stationery);

        detail_lineseries->append(linevalue_x,detail_use_money);

        linevalue_x++;
        if((start_date.year() == end_date.year()) && (start_date.month() == end_date.month())){
            break;
        }
        j++;
    }
    detail_bar_series->append(detail_PR_bar_set);
    detail_bar_series->append(detail_develop_bar_set);
    detail_bar_series->append(detail_strip_bar_set);
    detail_bar_series->append(detail_etc_bar_set);
    detail_bar_series->append(detail_Clean_ware_bar_set);
    detail_bar_series->append(detail_Consumable_supplies_bar_set);
    detail_bar_series->append(detail_stationery_bar_set);

    detail_lineseries->setPointLabelsFormat("@yPoint");
    share_detail_chart->addSeries(detail_lineseries);
    share_detail_chart->addSeries(detail_bar_series);

    share_detail_axis_X = new QBarCategoryAxis();
    share_detail_axis_X->append(detail_categories);
    share_detail_chart->setAxisX(share_detail_axis_X, detail_bar_series);
    share_detail_chart->setAxisX(share_detail_axis_X,detail_lineseries);
    share_detail_axis_X->setRange(detail_categories.first(),detail_categories.last());
    share_detail_axis_Y= new QValueAxis();
    share_detail_chart->setAxisY(share_detail_axis_Y,detail_bar_series);
    share_detail_chart->setAxisY(share_detail_axis_Y,detail_lineseries);

    qreal max_yvalue = share_detail_axis_Y->max();
    qreal min_yvalue = share_detail_axis_Y->min();
    qreal diff_value = max_yvalue - min_yvalue;
    share_detail_axis_Y->setRange(min_yvalue-(diff_value*0.1),max_yvalue+(diff_value*0.1));
    connect(detail_bar_series,SIGNAL(hovered(bool,int,QBarSet*)),this,SLOT(share_detail_hover_slot(bool,int,QBarSet*)));

}

void cost_reduction_main::on_share_total_zoom_reset_clicked()
{
    share_total_chart->zoomReset();
}

void cost_reduction_main::on_share_detail_zoom_reset_clicked()
{
    share_detail_chart->zoomReset();
}

void cost_reduction_main::on_LE_password_textEdited(const QString &arg1)
{
    if(arg1 == "0118"){
        ui->deposition_add_row->setEnabled(true);
        ui->deposition_del_row->setEnabled(true);
        ui->eatching__add_row->setEnabled(true);
        ui->eatching__del_row->setEnabled(true);
        ui->probe__add_row->setEnabled(true);
        ui->probe__del_row->setEnabled(true);
        ui->light_add_row->setEnabled(true);
        ui->light_del_row->setEnabled(true);
        ui->share_add_row->setEnabled(true);
        ui->share_del_row->setEnabled(true);
        ui->deposition_limit_money_btn->setEnabled(true);
        ui->eatching_limit_money_btn->setEnabled(true);
        ui->light_limit_money_btn->setEnabled(true);
        ui->probe_limit_money_btn->setEnabled(true);


        ui->all_data_table->setEditTriggers(QAbstractItemView::DoubleClicked|QAbstractItemView::EditKeyPressed|QAbstractItemView::AnyKeyPressed);
        ui->deposition_data_table->setEditTriggers(QAbstractItemView::DoubleClicked|QAbstractItemView::EditKeyPressed|QAbstractItemView::AnyKeyPressed);
        ui->eatching_data_table->setEditTriggers(QAbstractItemView::DoubleClicked|QAbstractItemView::EditKeyPressed|QAbstractItemView::AnyKeyPressed);
        ui->light_data_table->setEditTriggers(QAbstractItemView::DoubleClicked|QAbstractItemView::EditKeyPressed|QAbstractItemView::AnyKeyPressed);
        ui->probe_data_table->setEditTriggers(QAbstractItemView::DoubleClicked|QAbstractItemView::EditKeyPressed|QAbstractItemView::AnyKeyPressed);
        ui->share_data_table->setEditTriggers(QAbstractItemView::DoubleClicked|QAbstractItemView::EditKeyPressed|QAbstractItemView::AnyKeyPressed);
        ui->all_data_table->copy_flag = true;
        ui->deposition_data_table->copy_flag = true;
        ui->eatching_data_table->copy_flag = true;
        ui->light_data_table->copy_flag = true;
        ui->probe_data_table->copy_flag = true;
        ui->share_data_table->copy_flag = true;
        data_table_edit_mode = true;

    }else {
        ui->deposition_add_row->setEnabled(false);
        ui->deposition_del_row->setEnabled(false);
        ui->eatching__add_row->setEnabled(false);
        ui->eatching__del_row->setEnabled(false);
        ui->probe__add_row->setEnabled(false);
        ui->probe__del_row->setEnabled(false);
        ui->light_add_row->setEnabled(false);
        ui->light_del_row->setEnabled(false);
        ui->share_add_row->setEnabled(false);
        ui->share_del_row->setEnabled(false);

        ui->deposition_limit_money_btn->setEnabled(false);
        ui->eatching_limit_money_btn->setEnabled(false);
        ui->light_limit_money_btn->setEnabled(false);
        ui->probe_limit_money_btn->setEnabled(false);
        ui->all_data_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->deposition_data_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->eatching_data_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->light_data_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->probe_data_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->share_data_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->all_data_table->copy_flag = false;
        ui->deposition_data_table->copy_flag = false;
        ui->eatching_data_table->copy_flag = false;
        ui->light_data_table->copy_flag = false;
        ui->probe_data_table->copy_flag = false;
        ui->share_data_table->copy_flag = false;
        data_table_edit_mode = false;
    }
}
