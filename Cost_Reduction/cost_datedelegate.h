#ifndef DATEDELEGATE_H
#define DATEDELEGATE_H

#include <QItemDelegate>
#include <QDateTimeEdit>
#include <QPainter>

class Cost_Datedelegate: public QItemDelegate
{
    Q_OBJECT
public:
    Cost_Datedelegate(QWidget *parent = 0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex& index) const;
    void updateEditorGeometry( QWidget *editor,
                            const QStyleOptionViewItem &option,
                            const QModelIndex &index ) const;
    mutable QDateTimeEdit *dataTimeEdit;
private slots:

    void setData(QDateTime val);
};

#endif // DATEDELEGATE_H
