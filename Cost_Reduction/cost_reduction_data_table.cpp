#include "cost_reduction_data_table.h"

Cost_Reduction_data_table::Cost_Reduction_data_table(QWidget *parent)
{
    copy_flag = false;
}

void Cost_Reduction_data_table::keyPressEvent(QKeyEvent *event)
{
    // If Ctrl-C typed
     if (event->key() == Qt::Key_C && (event->modifiers() & Qt::ControlModifier))
     {
         QModelIndexList cells = selectedIndexes();

         qSort(cells); // Necessary, otherwise they are in column order

         QString text;

         int currentRow = 0; // To determine when to insert newlines
         int first_row=0;
         foreach (const QModelIndex& cell, cells) {
             if (text.length() == 0) {
                 // First item
                 first_row = cell.row();
             } else if (cell.row() != currentRow) {
                 // New row
                 text += '\n';
             } else {
                 // Next cell
                 text += '\t';
             }
             currentRow = cell.row();
             QString cell_data = cell.data().toString();
             cell_data = cell_data.replace("\"","/\"");
             cell_data = cell_data.replace("\r","");
             cell_data = cell_data.replace("\n","");
             text += cell_data;
         }
         QString header_data;
         foreach (const QModelIndex& cell, cells) {
             if(header_data.length() == 0){

             }else if (first_row != cell.row()){
                 header_data += '\n';
                 break;
             }else {
                 header_data += '\t';
             }
             header_data += model()->headerData(cell.column(),Qt::Horizontal).toString();
         }
         text = header_data + text;

         QApplication::clipboard()->setText(text);
         return;
     }else if(event->key() == Qt::Key_V && (event->modifiers() & Qt::ControlModifier)){
         if(!copy_flag){
             return;
         }
         qDebug()<< QApplication::clipboard()->text();
         QString cliptxt = QApplication::clipboard()->text();
         qDebug()<<"column = "<<selectionModel()->selectedIndexes().at(0).column();
         qDebug()<<"row = "<<selectionModel()->selectedIndexes().at(0).row();
         int lift_top_colum = selectionModel()->selectedIndexes().at(0).column();
         int lift_top_row = selectionModel()->selectedIndexes().at(0).row();
         if(selectionModel()->selectedIndexes().count()>0){
             QStringList rowlist = cliptxt.split("\n");
             for(int i=0;i<rowlist.count()-1;i++) {
                 if((lift_top_row+i)>model()->rowCount()){
                     break;
                 }
                 QStringList colum_item_list = rowlist.at(i).split("\t");
                 for(int j=0;j<colum_item_list.count();j++){
                     if((lift_top_colum+j)>model()->columnCount()){

                     }else {
                         QString str_data = colum_item_list.at(j);
                         if((lift_top_colum+j) == 10){
                            str_data= str_data.replace(",","");
                            str_data= str_data.trimmed();
                            QByteArray byte  = str_data.toLocal8Bit();
                            byte = byte.replace(92,"");


                            str_data = QString(byte);

                         }
                         QModelIndex index = model()->index(lift_top_row+i,lift_top_colum+j);
                         model()->setData(index,str_data);
                     }

                 }
             }
         }
         model()->submit();
     }
     QTableView::keyPressEvent(event);
}
