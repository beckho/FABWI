#include "cost_datedelegate.h"

Cost_Datedelegate::Cost_Datedelegate(QWidget *parent): QItemDelegate(parent)
{

}

QWidget *Cost_Datedelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    dataTimeEdit = new QDateTimeEdit( parent );
    dataTimeEdit->setCalendarPopup(true);
    dataTimeEdit->setDisplayFormat("yyyy-MM-dd hh:mm:ss");
    QObject::connect(dataTimeEdit,SIGNAL(dateTimeChanged(QDateTime)),this,SLOT(setData(QDateTime)));
    return dataTimeEdit;
}

void Cost_Datedelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QVariant dateTime = index.model()->data( index, Qt::DisplayRole );

    (static_cast<QDateTimeEdit*>( editor ))->setDateTime(dateTime.toDateTime());
}

void Cost_Datedelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    model->setData( index, static_cast<QDateTimeEdit*>( editor )->dateTime() );
}

void Cost_Datedelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry( option.rect );
}

void Cost_Datedelegate::setData(QDateTime val)
{
    emit commitData(dataTimeEdit);
}
