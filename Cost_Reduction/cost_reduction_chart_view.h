#ifndef COST_REDUCTION_CHART_VIEW_H
#define COST_REDUCTION_CHART_VIEW_H


#include <QObject>
#include <QWidget>

#include <QtCharts/QChartView>
#include <QtWidgets/QRubberBand>
#include <QDebug>
#include <QPointF>
QT_CHARTS_USE_NAMESPACE

class Cost_Reduction_chart_view : public QChartView
{
    Q_OBJECT
public:
    Cost_Reduction_chart_view(QChart *chart, QWidget *parent = 0);
    QChart *mchart;
signals :
    void move_value(QPointF value);
protected :
    bool viewportEvent(QEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);

private:
    bool m_isTouching;
};

#endif // COST_REDUCTION_CHART_VIEW_H
