#include "cost_reduction_chart_view.h"


Cost_Reduction_chart_view::Cost_Reduction_chart_view(QChart *chart, QWidget *parent):
    QChartView(chart, parent),
    m_isTouching(false)
{
    setRubberBand(QChartView::RectangleRubberBand);
    this->mchart = chart;

}

bool Cost_Reduction_chart_view::viewportEvent(QEvent *event)
{
       return QChartView::viewportEvent(event);
}

void Cost_Reduction_chart_view::mousePressEvent(QMouseEvent *event)
{
    QChartView::mousePressEvent(event);
}

void Cost_Reduction_chart_view::mouseReleaseEvent(QMouseEvent *event)
{
    QChartView::mouseReleaseEvent(event);
}

void Cost_Reduction_chart_view::mouseMoveEvent(QMouseEvent *event)
{

    QPointF value(chart()->mapToValue(event->pos()).x(),chart()->mapToValue(event->pos()).y());
    emit move_value(value);
    QChartView::mouseMoveEvent(event);
}

void Cost_Reduction_chart_view::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Plus:
        chart()->zoomIn();
        break;
    case Qt::Key_Minus:
        chart()->zoomOut();
        break;
    case Qt::Key_Left:
        chart()->scroll(-10, 0);
        break;
    case Qt::Key_Right:
        chart()->scroll(10, 0);
        break;
    case Qt::Key_Up:
        chart()->scroll(0, 10);
        break;
    case Qt::Key_Down:
        chart()->scroll(0, -10);

        break;
    default:
        QGraphicsView::keyPressEvent(event);
        break;
    }
    this->update();
    this->updateGeometry();
    mchart->update(mchart->geometry());
}
