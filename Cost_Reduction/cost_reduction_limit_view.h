#ifndef COST_REDUCTION_LIMIT_VIEW_H
#define COST_REDUCTION_LIMIT_VIEW_H

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDateTime>
#include <Cost_Reduction/cost_redction_data_table_model.h>
#include <Cost_Reduction/cost_reduction_data_table.h>
namespace Ui {
class Cost_Reduction_limit_view;
}

class Cost_Reduction_limit_view : public QWidget
{
    Q_OBJECT

public:
    explicit Cost_Reduction_limit_view(QWidget *parent = 0);
    explicit Cost_Reduction_limit_view(QString process_name,QDate init_year
                                       ,QSqlDatabase &my_mes_db,QWidget *parent = 0);
    QString process_name;
    QDate init_year;
    QSqlDatabase my_mes_db;
    Cost_Redction_data_table_model * tablemoel;
    ~Cost_Reduction_limit_view();

private slots:
    void on_search_btn_clicked();

    void on_add_row_btn_clicked();

    void on_del_row_btn_clicked();

private:
    Ui::Cost_Reduction_limit_view *ui;
};

#endif // COST_REDUCTION_LIMIT_VIEW_H
