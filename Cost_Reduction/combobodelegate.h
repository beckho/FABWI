#ifndef COMBOBODELEGATE_H
#define COMBOBODELEGATE_H

#include <QItemDelegate>
#include <QDateTimeEdit>
#include <QPainter>
#include <QComboBox>
class ComboboDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    ComboboDelegate(QStringList &list,QWidget *parent = 0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex& index) const;
    void updateEditorGeometry( QWidget *editor,
                            const QStyleOptionViewItem &option,
                            const QModelIndex &index ) const;
    mutable QComboBox *comboxedit;
    mutable QStringList list;
private slots:

    void setData(QString val);

};

#endif // COMBOBODELEGATE_H
