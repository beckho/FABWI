#ifndef COST_SHARE_DATA_TABLE_MODEL_H
#define COST_SHARE_DATA_TABLE_MODEL_H

#include <QObject>
#include <QWidget>
#include <QSqlTableModel>
#include <QDebug>
#include <QModelIndex>
#include <QSqlRecord>
#include <QSqlField>
#include <QDateTime>
class Cost_share_data_table_model : public QSqlTableModel
{
    Q_OBJECT
public:
    Cost_share_data_table_model(QObject *parent,QSqlDatabase db);
};

#endif // COST_SHARE_DATA_TABLE_MODEL_H
