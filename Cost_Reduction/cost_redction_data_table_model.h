#ifndef COST_REDCTION_DATA_TABLE_MODEL_H
#define COST_REDCTION_DATA_TABLE_MODEL_H

#include <QObject>
#include <QWidget>
#include <QSqlTableModel>
#include <QDebug>
#include <QModelIndex>
#include <QSqlRecord>
#include <QSqlField>
#include <QDateTime>

class Cost_Redction_data_table_model :public QSqlTableModel
{
     Q_OBJECT
public:
    Cost_Redction_data_table_model(QString callobj,QObject *parent,QSqlDatabase db);
     Qt::ItemFlags flags(const QModelIndex & index) const;
     QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const ;

     bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
     QString callobj;
     QObject *parent;
};

#endif // COST_REDCTION_DATA_TABLE_MODEL_H
