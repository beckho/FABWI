#ifndef COST_REDUCTION_CHART_H
#define COST_REDUCTION_CHART_H
#include <QObject>
#include <QWidget>
#include <QtCharts/QChart>
#include <QLineSeries>
QT_CHARTS_USE_NAMESPACE

class Cost_Reduction_chart : public QChart
{
    Q_OBJECT
public:
    Cost_Reduction_chart(QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0);
};

#endif // COST_REDUCTION_CHART_H
