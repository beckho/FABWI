#include "cost_redction_data_table_model.h"
#include "cost_reduction_main.h"
Cost_Redction_data_table_model::Cost_Redction_data_table_model(QString callobj,QObject *parent, QSqlDatabase db):QSqlTableModel(parent,db)
{
    this->callobj = callobj;
    this->parent = parent;
}

Qt::ItemFlags Cost_Redction_data_table_model::flags(const QModelIndex &index) const
{
    if(index.column() == fieldIndex("process") && callobj == "deposition_search_month" ){
        return Qt::DisplayRole|Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::EditRole;
    }
    if(index.column() == fieldIndex("process") && callobj == "light_search_month" ){
        return Qt::DisplayRole|Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::EditRole;
    }
    if(index.column() == fieldIndex("process") && callobj == "eatching_search_month" ){
        return Qt::DisplayRole|Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::EditRole;
    }
    if(index.column() == fieldIndex("process") && callobj == "probe_search_month" ){
        return Qt::DisplayRole|Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::EditRole;
    }
    if(index.column() == fieldIndex("process") && callobj == "all_search_month" ){
        return Qt::DisplayRole|Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::EditRole;
    }
    if(((index.column() == fieldIndex("draft"))||(index.column() == fieldIndex("draft_calculate"))||(index.column() == fieldIndex("input_check")) ) &&
            ( (callobj == "all_search_month") || (callobj == "deposition_search_month")
             ||(callobj == "light_search_month") ||(callobj == "eatching_search_month")
             ||(callobj == "probe_search_month"))){
        return QSqlTableModel::flags(index)|Qt::ItemIsUserCheckable;

    }
    return QSqlTableModel::flags(index);
}

QVariant Cost_Redction_data_table_model::data(const QModelIndex &index, int role) const
{
    if(index.column() == fieldIndex("buy_time") && role == Qt::DisplayRole && callobj == "deposition_search_month"){
           QDateTime datetime =  record(index.row()).value(index.column()).toDateTime();
           return datetime.toString("yyyy-MM-dd");
    }
    if(index.column() == fieldIndex("buy_time") && role == Qt::DisplayRole && callobj == "light_search_month"){
           QDateTime datetime =  record(index.row()).value(index.column()).toDateTime();
           return datetime.toString("yyyy-MM-dd");
    }
    if(index.column() == fieldIndex("buy_time") && role == Qt::DisplayRole && callobj == "eatching_search_month"){
           QDateTime datetime =  record(index.row()).value(index.column()).toDateTime();
           return datetime.toString("yyyy-MM-dd");
    }
    if(index.column() == fieldIndex("buy_time") && role == Qt::DisplayRole && callobj == "probe_search_month"){
           QDateTime datetime =  record(index.row()).value(index.column()).toDateTime();
           return datetime.toString("yyyy-MM-dd");
    }
    if(index.column() == fieldIndex("buy_time") && role == Qt::DisplayRole && callobj == "all_search_month"){
           QDateTime datetime =  record(index.row()).value(index.column()).toDateTime();
           return datetime.toString("yyyy-MM-dd");
    }
    if(callobj == "limit_table" && index.column() == fieldIndex("process") && role == Qt::DisplayRole ){
        QDateTime datetime =  record(index.row()).value(index.column()).toDateTime();
        return datetime.toString("yyyy-MM");
    }
    if((index.column() == fieldIndex("draft")||
        index.column() == fieldIndex("draft_calculate")||
        index.column() == fieldIndex("input_check"))  && role == Qt::DisplayRole){
        if(record(index.row()).value(index.column()).toBool()){
            return "O";
        }else {
            return "X";
        }
    }
    if((index.column() == fieldIndex("draft")||
        index.column() == fieldIndex("draft_calculate")||
        index.column() == fieldIndex("input_check")) && role == Qt::CheckStateRole)
   {
        if(record(index.row()).value(index.column()).toBool()){
            return Qt::Checked;
        }else {
            return Qt::Unchecked;
        }
    }
    return QSqlTableModel::data(index, role);
}

bool Cost_Redction_data_table_model::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if((index.column() == fieldIndex("draft")||
        index.column() == fieldIndex("draft_calculate")||
        index.column() == fieldIndex("input_check")) && role == Qt::CheckStateRole)
    {
        bool result = false;
        cost_reduction_main *parent_obj = (cost_reduction_main *)parent;
        if(parent_obj->data_table_edit_mode){
            if ( value == Qt::Checked ){

                QSqlRecord recode = record(index.row());
                recode.setGenerated(index.column(),true);
                recode.setGenerated("idx",true);
                recode.setValue(index.column(),Qt::Checked);
                result = setRecord(index.row(),recode);
            }
            else if(value == Qt::Unchecked) {
                QSqlRecord recode = record(index.row());
                recode.setGenerated(index.column(),true);
                recode.setGenerated("idx",true);
                recode.setValue(index.column(),Qt::Unchecked);
                result = setRecord(index.row(),recode);
            }
        }
        emit dataChanged(index, index );
        qDebug()<<result;
        return result;
    }
    return QSqlTableModel::setData(index, value, role);

}

