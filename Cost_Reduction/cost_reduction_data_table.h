#ifndef COST_REDUCTION_DATA_TABLE_H
#define COST_REDUCTION_DATA_TABLE_H

#include <QObject>
#include <QWidget>
#include <QTableView>
#include <QKeyEvent>
#include <QApplication>
#include <QClipboard>
#include <QDebug>
class Cost_Reduction_data_table : public QTableView
{
    Q_OBJECT
public:
    Cost_Reduction_data_table(QWidget *parent = 0);
    bool copy_flag;
private:
    void keyPressEvent(QKeyEvent *event);
};

#endif // COST_REDUCTION_DATA_TABLE_H
