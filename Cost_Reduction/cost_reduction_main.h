#ifndef COST_REDUCTION_MAIN_H
#define COST_REDUCTION_MAIN_H

#include <QWidget>
#include <QDateTime>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSettings>
#include <Cost_Reduction/cost_redction_data_table_model.h>
#include <Cost_Reduction/cost_reduction_data_table.h>
#include <global_define.h>
#include <QSqlError>
#include <QDebug>
#include <Cost_Reduction/cost_reduction_limit_view.h>
#include <Cost_Reduction/combobodelegate.h>
#include <Cost_Reduction/cost_datedelegate.h>
#include <Cost_Reduction/cost_reduction_chart.h>
#include <Cost_Reduction/cost_reduction_chart_view.h>
#include <QBarSeries>
#include <QBarSet>
#include <QBarCategoryAxis>
#include <QValueAxis>
#include <QStackedBarSeries>
#include <QHash>
#include <Cost_Reduction/cost_share_data_table_model.h>
#include <QSqlRecord>

namespace Ui {
class cost_reduction_main;
}

class cost_reduction_main : public QWidget
{
    Q_OBJECT

public:
    explicit cost_reduction_main(QWidget *parent = 0);
    ~cost_reduction_main();
    QSqlDatabase my_mesdb;

    Cost_Reduction_chart *deposition_total_chart;
    Cost_Reduction_chart_view *deposition_total_chart_view;
    Cost_Reduction_chart *deposition_detail_chart;
    Cost_Reduction_chart_view *deposition_detail_chart_view;
    QBarCategoryAxis *deposition_detail_axis_X;
    QValueAxis *deposition_detail_axis_Y;

    Cost_Reduction_chart *light_total_chart;
    Cost_Reduction_chart_view *light_total_chart_view;
    Cost_Reduction_chart *light_detail_chart;
    Cost_Reduction_chart_view *light_detail_chart_view;
    QBarCategoryAxis *light_detail_axis_X;
    QValueAxis *light_detail_axis_Y;

    Cost_Reduction_chart *eatching_total_chart;
    Cost_Reduction_chart_view *eatching_total_chart_view;
    Cost_Reduction_chart *eatching_detail_chart;
    Cost_Reduction_chart_view *eatching_detail_chart_view;
    QBarCategoryAxis *eatching_detail_axis_X;
    QValueAxis *eatching_detail_axis_Y;

    Cost_Reduction_chart *probe_total_chart;
    Cost_Reduction_chart_view *probe_total_chart_view;
    Cost_Reduction_chart *probe_detail_chart;
    Cost_Reduction_chart_view *probe_detail_chart_view;
    QBarCategoryAxis *probe_detail_axis_X;
    QValueAxis *probe_detail_axis_Y;

    Cost_Reduction_chart *all_total_chart;
    Cost_Reduction_chart_view *all_total_chart_view;
    Cost_Reduction_chart *all_detail_chart;
    Cost_Reduction_chart_view *all_detail_chart_view;
    QBarCategoryAxis *all_detail_axis_X;
    QValueAxis *all_detail_axis_Y;

    QHash<QString,QStringList> detail_process_limit_map;
    QHash<QString,QStringList> total_process_limit_map;

    Cost_Reduction_chart *share_total_chart;
    Cost_Reduction_chart_view *share_total_chart_view;
    Cost_Reduction_chart *share_detail_chart;
    Cost_Reduction_chart_view *share_detail_chart_view;
    QBarCategoryAxis *share_detail_axis_X;
    QValueAxis *share_detail_axis_Y;

    QStringList classification_list;
    QStringList share_list;
    QStringList MROList;
    QStringList Inout_split_list;

    bool data_table_edit_mode;
    void tab_1_init();
    void tab_2_init();
    void tab_3_init();
    void tab_4_init();
    void tab_5_init();
    void tab_6_init();

private slots:
    void on_deposition_limit_money_btn_clicked();

    void on_deposition_add_row_clicked();

    void on_deposition_del_row_clicked();

    void on_deposition_refresh_btn_clicked();

    void on_depostion_total_zoom_reset_clicked();

    void deposition_total_hover_slot(bool status, int index, QBarSet *barset);

    void deposition_detail_hover_slot(bool status, int index, QBarSet *barset);

    void light_total_hover_slot(bool status, int index, QBarSet *barset);

    void light_detail_hover_slot(bool status, int index, QBarSet *barset);

    void eatching_total_hover_slot(bool status, int index, QBarSet *barset);

    void eatching_detail_hover_slot(bool status, int index, QBarSet *barset);

    void all_total_hover_slot(bool status, int index, QBarSet *barset);

    void all_detail_hover_slot(bool status, int index, QBarSet *barset);

    void share_total_hover_slot(bool status, int index, QBarSet *barset);

    void share_detail_hover_slot(bool status, int index, QBarSet *barset);



    void probe_total_hover_slot(bool status, int index, QBarSet *barset);

    void probe_detail_hover_slot(bool status, int index, QBarSet *barset);

    void deposition_auto_calc_total_price(QModelIndex index1,QModelIndex index2,QVector<int> roles);

    void light_auto_calc_total_price(QModelIndex index1,QModelIndex index2,QVector<int> roles);

    void eatching_auto_calc_total_price(QModelIndex index1,QModelIndex index2,QVector<int> roles);

    void probe_auto_calc_total_price(QModelIndex index1,QModelIndex index2,QVector<int> roles);

    void all_auto_calc_total_price(QModelIndex index1,QModelIndex index2,QVector<int> roles);

    void share_auto_calc_total_price(QModelIndex index1,QModelIndex index2,QVector<int> roles);

    void on_depostion_detail_zoom_reset_clicked();

    void on_light_refresh_btn_clicked();

    void on_light_add_row_clicked();

    void on_light_del_row_clicked();

    void on_light_total_zoom_reset_clicked();

    void on_light_detail_zoom_reset_clicked();

    void on_eatching_refresh_btn_clicked();

    void on_eatching__add_row_clicked();

    void on_eatching__del_row_clicked();

    void on_eatching_limit_money_btn_clicked();

    void on_light_limit_money_btn_clicked();

    void on_probe_refresh_btn_clicked();

    void on_probe__add_row_clicked();

    void on_probe__del_row_clicked();

    void on_probe_limit_money_btn_clicked();

    void on_probe_total_zoom_reset_clicked();

    void on_probe_detail_zoom_reset_clicked();

    void on_all_refresh_btn_clicked();

    void on_all_total_zoom_reset_clicked();

    void on_all_detail_zoom_reset_clicked();

    void on_eatching_total_zoom_reset_clicked();

    void on_eatching_detail_zoom_reset_clicked();

    void on_share_add_row_clicked();

    void on_share_del_row_clicked();

    void on_share_refresh_btn_clicked();

    void on_share_total_zoom_reset_clicked();

    void on_share_detail_zoom_reset_clicked();

    void on_LE_password_textEdited(const QString &arg1);

private:
    Ui::cost_reduction_main *ui;
};

#endif // COST_REDUCTION_MAIN_H
