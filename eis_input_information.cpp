#include "eis_input_information.h"
#include "ui_eis_input_information.h"

EIS_input_information::EIS_input_information(QString area, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EIS_input_information)
{
    ui->setupUi(this);
    db = QSqlDatabase::database("EISDB");
    if(!db.isOpen()){
        if(!db.open()){
             int ret = QMessageBox::warning(this, tr("conntion false"),
                                               "server connection fail\n"
                                                  ""+db.lastError().text(),
                                                    QMessageBox::Close);
        }
    }
    this->area = area;
    init_combo_box();

}

void EIS_input_information::init_combo_box()
{

    QString query_txt;
    QSqlQuery query(db);
    if(this->area == "CSP"){
        query_txt = QString("select team from People_information where `area` IN ('CSP') OR `area` IS NULL group by team order by team asc");
    }else if(this->area == "WLP"){
        query_txt = QString("select team from People_information where `area` IN ('WLP') group by team order by team asc");
    }else if(this->area == "BPROJECT"){
        query_txt = QString("select team from People_information where `area` IN ('BPROJECT') group by team order by team asc");
    }

    query.exec(query_txt);
    ui->add_process_team->clear();
    ui->add_process_team->addItem("");
    ui->add_facilities_team->clear();
    ui->add_facilities_team->addItem("");
    ui->add_people_team->clear();
    ui->add_people_team->addItem("");
    while(query.next()){
        ui->add_process_team->addItem(query.value("team").toString());
        ui->add_facilities_team->addItem(query.value("team").toString());
        ui->add_people_team->addItem(query.value("team").toString());
    }
    if(this->area == "CSP"){
        query_txt = QString("select factory_process from People_information where `area` IN ('CSP') OR `area` IS NULL group by factory_process order by factory_process asc");
    }else if(this->area == "WLP"){
        query_txt = QString("select factory_process from People_information  where `area` IN ('WLP') group by factory_process order by factory_process asc");
    }else if(this->area == "BPROJECT"){
        query_txt = QString("select factory_process from People_information  where `area` IN ('BPROJECT') group by factory_process order by factory_process asc");
    }

    query.exec(query_txt);
    ui->add_facilities_process->clear();
    ui->add_facilities_process->addItem("");
    while(query.next()){
        ui->add_facilities_process->addItem(query.value("factory_process").toString());
    }
}

EIS_input_information::~EIS_input_information()
{
    delete ui;
}

void EIS_input_information::on_add_team_btn_clicked()
{
    QSqlQuery query(db);
    QString query_txt = QString("INSERT INTO `People_information` (`team`,`area`) VALUES ('%1','%2');").arg(ui->add_team->text()).arg(area);
    query.exec(query_txt);
    init_combo_box();
    QMessageBox msg;
    msg.addButton(QMessageBox::Ok);
    msg.setText(tr("add complete"));
    msg.exec();
}

void EIS_input_information::on_add_process_btn_clicked()
{

    QSqlQuery query(db);
    QString query_txt = QString("INSERT INTO `People_information` (`team`, `factory_process`,`area`) VALUES ('%1', '%2','%3');")
                                .arg(ui->add_process_team->currentText()).arg(ui->add_process->text()).arg(area);
    query.exec(query_txt);
    init_combo_box();
    QMessageBox msg;
    msg.addButton(QMessageBox::Ok);
    msg.setText(tr("add complete"));
    msg.exec();
}

void EIS_input_information::on_add_facilities_btn_clicked()
{
    QSqlQuery query(db);
    QString query_txt = QString("INSERT INTO `FAB`.`People_information` (`team`, `factory_process`, `facilities`,`area`) VALUES ('%1', '%2', '%3','%4');")
                                .arg(ui->add_facilities_team->currentText()).arg(ui->add_facilities_process->currentText()).arg(ui->add_facilities->text()).arg(area);
    query.exec(query_txt);
    init_combo_box();
    QMessageBox msg;
    msg.addButton(QMessageBox::Ok);
    msg.setText(tr("add complete"));
    msg.exec();
}

void EIS_input_information::on_add_people_btn_clicked()
{
    QSqlQuery query(db);
    QString query_txt = QString("INSERT INTO `People_information` (`team`, `name`,`area`) VALUES ('%1', '%2','%3');")
                                .arg(ui->add_people_team->currentText()).arg(ui->add_people->text()).arg(area);
    query.exec(query_txt);
    init_combo_box();
    QMessageBox msg;
    msg.addButton(QMessageBox::Ok);
    msg.setText(tr("add complete"));
    msg.exec();
}

