#include "wlpboard.h"
#include "ui_wlpboard.h"
#include "QDebug"
#include <eismain.h>

WLPboard::WLPboard(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WLPboard)
{
    ui->setupUi(this);
}

WLPboard::~WLPboard()
{
    delete ui;
}

void WLPboard::on_infrom_entet_btn_clicked()
{
    EISmain *eismain = new EISmain("WLP");
    eismain->show();
}

void WLPboard::on_NIS_btn_clicked()
{
    NISmainwindow *niswidget = new NISmainwindow("WLP");
    niswidget->show();
}

void WLPboard::on_Dashboard_btn_clicked()
{

}
