#ifndef WORST_SEARCH_TH_H
#define WORST_SEARCH_TH_H

#include <QObject>
#include <QWidget>
#include <QThread>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QSettings>
#include <global_define.h>
#include <QMessageBox>
#include <QVector>
#include <QDateTime>
#include <QApplication>
#include <QFile>
#include <QAxObject>
#include <QMutex>
#include <ns_core_com/ns_core_com.h>
#define REAL_QUERY
#define RoundOff(x, dig) (floor((x) * pow(10,dig) + 0.5) / pow(10,dig))
class rework_text_type{
public:
    QString Material;
    QString LOT_ID;
    QString operation_name;
    QString wafer_count;
    QString Defect_Qty;
    QString Commnet;
    QString Defect_name;
    QString Df_rate;

};

class defect_worst_type{
 public:
    QString Material;
    QString LOT_ID;
    QString operation_name;
    QString wafer_count;
    QString Defect_Qty;
    QString Commnet;
    QString Defect_name;
    QString Df_rate;

};


class worst_search_th : public QThread
{
    Q_OBJECT
    //배열 0은 daily, 1은 누적 ..
public:
    worst_search_th(QMutex *mutex);
    QSqlDatabase ms_mesdb;
    QSqlDatabase my_mesdb;
    QMutex *mutex;
    double low_q[2] = {0,0};
    double high_q[2] = {0,0};
    double limit_wosrt[2] = {0,0};
    double os_wosrt[2] = {0,0};
    double bw_wosrt[2] = {0,0};
    double vswr_wosrt[2] = {0,0};
    double machine_d_wosrt[2] = {0,0};
    double worker_d_wosrt[2] = {0,0};
    double ADI_wosrt[2] = {0,0};
    double ACI_wosrt[2] = {0,0};
    double paticle_wosrt[2] = {0,0};
    double woker_m_wosrt[2] = {0,0};
    double chiping[2] = {0,0};
    double lp_sf  = 0 ;
    double lp_dpx  = 0 ;
    double lp_rsm  = 0 ;
    double sf[2]  = {0,0};
    double dpx[2]  = {0,0};
    double rsm[2]  = {0,0};
    double patten_paticle[2] = {0,0};
    double pad_paticle[2] = {0,0};
    double sin_miss[2] = {0,0};
    double etc_paticle[2] = {0,0};
    double bright_pad_worst[2] = {0,0};

    double limit_X897BYF_TXBCG[2] = {0,0};
    double limit_X897BYF_TXBSG[2] = {0,0};
    double limit_XG35BYN_TXBBK[2] = {0,0};
    double limit_XG35BYN_RXBBK[2] = {0,0};
    double limit_XG80DY6_ATACR[2] = {0,0};
    double limit_XG47AYM_TXBJTU[2] = {0,0};
    double limit_XG47AYM_RXBOTU[2] = {0,0};
    double limit_XG80AYM_TXBMKU[2] = {0,0};
    double limit_XG80AYM_RXBOKU[2] = {0,0};
    double limit_TG45AU0_MBCK[2] = {0,0};
    double limit_TG89AU0_MDCK[2] = {0,0};
    double limit_DG45EA4_MACK[2] = {0,0};
    double limit_XG80DY6_ATACK[2] = {0,0};
    double limit_XG47AYM_TXBJGU[2] = {0,0};
    double limit_XG47AYM_TXSJBU[2] = {0,0};
    double limit_XG47AYM_RXBOBU[2] = {0,0};
    double limit_XG47AYM_RXBOAU[2] = {0,0};
    double limit_XG47AYM_RXBOGU[2] = {0,0};
    double limit_XG80DY6_ATACR01[2] = {0,0};


    double limit_total[2] = {0,0};
    double lp_yield[2] = {0,0};
    double yield[2] = {0,0};
    NS_Core_Com::NS_Main NS_core;
    QDate sel_date;
    QVector<rework_text_type> rework_list;
    QVector<defect_worst_type> defect_list;




signals:
    void sig_excel_work();
    void sig_debug_output(QString str);

private:
    void run();
};

#endif // WORST_SEARCH_TH_H
