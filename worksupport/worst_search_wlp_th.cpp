#include "worst_search_wlp_th.h"

worst_search_WLP_th::worst_search_WLP_th(QMutex *mutex)
{

    QString mydb_name = QString("MY_MESDB_WLP_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    QString server_ip = settings.value("serverip").toString();
    QString db_port = settings.value("dbport").toString();
    settings.endGroup();
    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }
    this->mutex = mutex;
}



void worst_search_WLP_th::run()
{
    mutex->lock();
    QSqlQuery query(my_mesdb);
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("low_q")));
    if(query.next()){
        low_q[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("low_q")));
    if(query.next()){
        low_q[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("high_q")));
    if(query.next()){
        high_q[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("high_q")));
    if(query.next()){
        high_q[1] = query.value("result").toDouble();
    }


    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("limit_wosrt")));
    if(query.next()){
        limit_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("limit_wosrt")));
    if(query.next()){
        limit_wosrt[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("os_wosrt")));
    if(query.next()){
        os_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("os_wosrt")));
    if(query.next()){
        os_wosrt[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("bw_wosrt")));
    if(query.next()){
        bw_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("bw_wosrt")));
    if(query.next()){
        bw_wosrt[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("vswr_wosrt")));
    if(query.next()){
        vswr_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("vswr_wosrt")));
    if(query.next()){
        vswr_wosrt[1] = query.value("result").toDouble();
    }


    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("vswr_wosrt")));
    if(query.next()){
        vswr_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("vswr_wosrt")));
    if(query.next()){
        vswr_wosrt[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("machine_d_wosrt")));
    if(query.next()){
        machine_d_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("machine_d_wosrt")));
    if(query.next()){
        machine_d_wosrt[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("worker_d_wosrt")));
    if(query.next()){
        worker_d_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("worker_d_wosrt")));
    if(query.next()){
        worker_d_wosrt[1] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` IN ('%2','%3','%4','%5','%6','%7','%8') ").arg(sel_date.toString("yyyy-MM-dd"))
               .arg(tr("paticle_wosrt")).arg(tr("patten_paticle")).arg(tr("pad_paticle")).arg(tr("sin_miss")).arg(tr("bright_pad_worst")).arg(tr("etc_paticle")).arg(tr("organic_compound")));
    if(query.next()){
        paticle_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` IN ('%2','%3','%4','%5','%6','%7','%8') ").arg(sel_date.toString("yyyy-MM-dd"))
               .arg(tr("paticle_wosrt")).arg(tr("patten_paticle")).arg(tr("pad_paticle")).arg(tr("sin_miss")).arg(tr("bright_pad_worst")).arg(tr("etc_paticle")).arg(tr("organic_compound")));
    if(query.next()){
        paticle_wosrt[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("woker_m_wosrt")));
    if(query.next()){
        woker_m_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("woker_m_wosrt")));
    if(query.next()){
        woker_m_wosrt[1] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("patten_paticle")));
    if(query.next()){
        patten_paticle[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("patten_paticle")));
    if(query.next()){
        patten_paticle[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("pad_paticle")));
    if(query.next()){
        pad_paticle[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("pad_paticle")));
    if(query.next()){
        pad_paticle[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("sin_miss")));
    if(query.next()){
        sin_miss[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("sin_miss")));
    if(query.next()){
        sin_miss[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("etc_paticle")));
    if(query.next()){
        etc_paticle[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("etc_paticle")));
    if(query.next()){
        etc_paticle[1] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        lp_yield[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        lp_yield[1] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` = 'yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        yield[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` = 'yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        yield[1] = query.value("result").toDouble();
    }
    qDebug()<<NS_core.init_NS_Core();


    query.exec(QString("select * from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND WORST_TYPE = 'REWORK' ").arg(sel_date.toString("yyyy-MM-dd")));
    while(query.next()){

        QStringList data_result = NS_core.get_worst_data_summary(sel_date.toString("yyyyMMdd"),sel_date.toString("yyyyMMdd"),"'A'","WLP","FAIL",
                                       query.value("WORST_CODE").toString());
        for(int i=0;i<data_result.count();i++){
            WLP_rework_text_type rework_item;

            QString itmes = data_result.at(i);
            QStringList itemlist = itmes.split(",");
            rework_item.LOT_ID = ((QString)itemlist.at(0)).split("=").at(1);
            rework_item.wafer_count = ((QString)itemlist.at(1)).split("=").at(1);
            rework_item.Defect_name = ((QString)itemlist.at(2)).split("=").at(1);
            rework_item.Material = ((QString)itemlist.at(3)).split("=").at(1);
            rework_item.Defect_Qty = ((QString)itemlist.at(4)).split("=").at(1);
            rework_item.operation_name = ((QString)itemlist.at(5)).split("=").at(1);
            rework_item.Commnet = ((QString)itemlist.at(6)).split("=").at(1);
            rework_item.Commnet = rework_item.Commnet.replace("}","");
            rework_item.Df_rate = QString("%1").arg(query.value("result").toDouble(),0,'f',2);
            rework_list.append(rework_item);
            qDebug()<<itmes;
        }
    }
    query.exec(QString("select * from V_FAB_REPORT_DAILY where (report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION is null AND date_type = 'Daily' AND WORST_TYPE = 'DEFECT') AND (type LIKE '%%2%' or type IN ('%3')) ")
               .arg(sel_date.toString("yyyy-MM-dd")).arg(tr("Defect_type")).arg(tr("chiping")));

    while(query.next()){

        QStringList data_result = NS_core.get_worst_data_summary(sel_date.toString("yyyyMMdd"),sel_date.toString("yyyyMMdd"),"'A'","WLP","DEFECT",
                                       query.value("WORST_CODE").toString());
        for(int i=0;i<data_result.count();i++){
            WLP_defect_worst_type defect_item;

            QString itmes = data_result.at(i);
            QStringList itemlist = itmes.split(",");
            defect_item.LOT_ID = ((QString)itemlist.at(0)).split("=").at(1);
            defect_item.wafer_count = ((QString)itemlist.at(1)).split("=").at(1);
            defect_item.Defect_name = ((QString)itemlist.at(2)).split("=").at(1);
            defect_item.Material = ((QString)itemlist.at(3)).split("=").at(1);
            defect_item.Defect_Qty = ((QString)itemlist.at(4)).split("=").at(1);
            defect_item.operation_name = ((QString)itemlist.at(5)).split("=").at(1);
            defect_item.Commnet = ((QString)itemlist.at(6)).split("=").at(1);
            defect_item.Commnet = defect_item.Commnet.replace("}","");
            defect_item.Df_rate = QString("%1").arg(query.value("result").toDouble(),0,'f',2);
            defect_list.append(defect_item);
            qDebug()<<itmes;
        }
    }


    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'X707ABC_ATCCGW_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_X707ABC_ATCCGW[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'X707ABC_ATCCGW_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_X707ABC_ATCCGW[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'XG50ABP_ATCCKW_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG50ABP_ATCCKW[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'XG50ABP_ATCCKW_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG50ABP_ATCCKW[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'RG17AP5_WCCK_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_RG17AP5_WCCK[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'RG17AP5_WCCK_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_RG17AP5_WCCK[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'H942AT2_WCCG_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_H942AT2_WCCG[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'H942AT2_WCCG_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_H942AT2_WCCG[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'Limit_MATERIAL_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_total[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'WLP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'Limit_MATERIAL_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_total[1] = query.value("result").toDouble();
    }

    emit sig_debug_output("complete go excel");
    emit sig_excel_work();
    mutex->unlock();
}
