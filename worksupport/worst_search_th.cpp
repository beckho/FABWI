#include "worst_search_th.h"

worst_search_th::worst_search_th(QMutex *mutex)
{

    QString mydb_name = QString("MY_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
//    ms_mesdb.setDatabaseName(serverinfo);
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    QString server_ip = settings.value("serverip").toString();
    QString db_port = settings.value("dbport").toString();

    settings.endGroup();

    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }

    this->mutex = mutex;
}



void worst_search_th::run()
{
    mutex->lock();
    QSqlQuery query(my_mesdb);

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("low_q")));
    if(query.next()){
        low_q[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("low_q")));
    if(query.next()){
        low_q[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("high_q")));
    if(query.next()){
        high_q[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("high_q")));
    if(query.next()){
        high_q[1] = query.value("result").toDouble();
    }


    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("limit_wosrt")));
    if(query.next()){
        limit_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("limit_wosrt")));
    if(query.next()){
        limit_wosrt[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("os_wosrt")));
    if(query.next()){
        os_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("os_wosrt")));
    if(query.next()){
        os_wosrt[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("bw_wosrt")));
    if(query.next()){
        bw_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("bw_wosrt")));
    if(query.next()){
        bw_wosrt[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("vswr_wosrt")));
    if(query.next()){
        vswr_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("vswr_wosrt")));
    if(query.next()){
        vswr_wosrt[1] = query.value("result").toDouble();
    }


    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("vswr_wosrt")));
    if(query.next()){
        vswr_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("vswr_wosrt")));
    if(query.next()){
        vswr_wosrt[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("machine_d_wosrt")));
    if(query.next()){
        machine_d_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("machine_d_wosrt")));
    if(query.next()){
        machine_d_wosrt[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("worker_d_wosrt")));
    if(query.next()){
        worker_d_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("worker_d_wosrt")));
    if(query.next()){
        worker_d_wosrt[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("ADI_wosrt")));
    if(query.next()){
        ADI_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("ADI_wosrt")));
    if(query.next()){
        ADI_wosrt[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("ACI_wosrt")));
    if(query.next()){
        ACI_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("ACI_wosrt")));
    if(query.next()){
        ACI_wosrt[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` IN ('%2','%3','%4','%5','%6','%7','%8') ").arg(sel_date.toString("yyyy-MM-dd"))
               .arg(tr("paticle_wosrt")).arg(tr("patten_paticle")).arg(tr("pad_paticle")).arg(tr("sin_miss")).arg(tr("bright_pad_worst")).arg(tr("etc_paticle")).arg(tr("organic_compound")));
    if(query.next()){
        paticle_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` IN ('%2','%3','%4','%5','%6','%7','%8') ").arg(sel_date.toString("yyyy-MM-dd"))
               .arg(tr("paticle_wosrt")).arg(tr("patten_paticle")).arg(tr("pad_paticle")).arg(tr("sin_miss")).arg(tr("bright_pad_worst")).arg(tr("etc_paticle")).arg(tr("organic_compound")));
    if(query.next()){
        paticle_wosrt[1] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` IN ('%2') ").arg(sel_date.toString("yyyy-MM-dd"))
               .arg(tr("chiping")));
    if(query.next()){
        chiping[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` IN ('%2') ").arg(sel_date.toString("yyyy-MM-dd"))
               .arg(tr("chiping")));
    if(query.next()){
        chiping[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("woker_m_wosrt")));
    if(query.next()){
        woker_m_wosrt[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` LIKE '%%2%' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("woker_m_wosrt")));
    if(query.next()){
        woker_m_wosrt[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'single_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        lp_sf = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'DPX_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        lp_dpx = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'RSM_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        lp_rsm = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` = 'single_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        sf[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` = 'single_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        sf[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` = 'DPX_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        dpx[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` = 'DPX_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        dpx[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` = 'RSM_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        rsm[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` = 'RSM_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        rsm[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("patten_paticle")));
    if(query.next()){
        patten_paticle[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("patten_paticle")));
    if(query.next()){
        patten_paticle[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("pad_paticle")));
    if(query.next()){
        pad_paticle[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("pad_paticle")));
    if(query.next()){
        pad_paticle[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("sin_miss")));
    if(query.next()){
        sin_miss[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("sin_miss")));
    if(query.next()){
        sin_miss[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("etc_paticle")));
    if(query.next()){
        etc_paticle[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("etc_paticle")));
    if(query.next()){
        etc_paticle[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("bright_pad_worst")));
    if(query.next()){
        bright_pad_worst[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` = '%2' ").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("bright_pad_worst")));
    if(query.next()){
        bright_pad_worst[1] = query.value("result").toDouble();
    }


    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'X897BYF_TXBCG_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_X897BYF_TXBCG[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'X897BYF_TXBCG_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_X897BYF_TXBCG[1] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'X897BYF_TXBSG_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_X897BYF_TXBSG[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'X897BYF_TXBSG_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_X897BYF_TXBSG[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'XG35BYN_TXBBK_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG35BYN_TXBBK[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'XG35BYN_TXBBK_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG35BYN_TXBBK[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'XG35BYN_RXBBK_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG35BYN_RXBBK[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'XG35BYN_RXBBK_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG35BYN_RXBBK[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'XG80DY6_ATACR_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG80DY6_ATACR[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'XG80DY6_ATACR_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG80DY6_ATACR[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'XG47AYM_TXBJTU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG47AYM_TXBJTU[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'XG47AYM_TXBJTU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG47AYM_TXBJTU[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'XG47AYM_RXBOTU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG47AYM_RXBOTU[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'XG47AYM_RXBOTU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG47AYM_RXBOTU[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'XG80AYM_TXBMKU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG80AYM_TXBMKU[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'XG80AYM_TXBMKU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG80AYM_TXBMKU[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'XG80AYM_RXBOKU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG80AYM_RXBOKU[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'XG80AYM_RXBOKU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG80AYM_RXBOKU[1] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'TG45AU0_MBCK_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_TG45AU0_MBCK[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'TG45AU0_MBCK_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_TG45AU0_MBCK[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'TG89AU0_MDCK_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_TG89AU0_MDCK[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'TG89AU0_MDCK_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_TG89AU0_MDCK[1] = query.value("result").toDouble();
    }


    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'DG45EA4_MACK_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_DG45EA4_MACK[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'DG45EA4_MACK_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_DG45EA4_MACK[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'XG80DY6_ATACK_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG80DY6_ATACK[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'XG80DY6_ATACK_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG80DY6_ATACK[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'XG47AYM_TXBJGU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG47AYM_TXBJGU[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'XG47AYM_TXBJGU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG47AYM_TXBJGU[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'XG47AYM_TXSJBU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG47AYM_TXSJBU[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'XG47AYM_TXSJBU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG47AYM_TXSJBU[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'XG47AYM_RXBOBU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG47AYM_RXBOBU[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'XG47AYM_RXBOBU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG47AYM_RXBOBU[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'XG47AYM_RXBOAU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG47AYM_RXBOAU[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'XG47AYM_RXBOAU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG47AYM_RXBOAU[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'XG47AYM_RXBOGU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG47AYM_RXBOGU[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'XG47AYM_RXBOGU_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG47AYM_RXBOGU[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'XG80DY6_ATACR01_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG80DY6_ATACR01[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'XG80DY6_ATACR01_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_XG80DY6_ATACR01[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'Limit_MATERIAL_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_total[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'Limit_MATERIAL_yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        limit_total[1] = query.value("result").toDouble();
    }

    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'Daily' AND `type` = 'yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        lp_yield[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION = 'OP08001010' AND date_type = 'accumulate' AND `type` = 'yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        lp_yield[1] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND `type` = 'yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        yield[0] = query.value("result").toDouble();
    }
    query.exec(QString("select SUM(result) as result from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'accumulate' AND `type` = 'yield' ").arg(sel_date.toString("yyyy-MM-dd")));
    if(query.next()){
        yield[1] = query.value("result").toDouble();
    }

    qDebug()<<NS_core.init_NS_Core();


    query.exec(QString("select * from V_FAB_REPORT_DAILY where report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND WORST_TYPE = 'REWORK' ").arg(sel_date.toString("yyyy-MM-dd")));
    while(query.next()){

        QStringList data_result = NS_core.get_worst_data_summary(sel_date.toString("yyyyMMdd"),sel_date.toString("yyyyMMdd"),"'A'","CSP","FAIL",
                                       query.value("WORST_CODE").toString());
        for(int i=0;i<data_result.count();i++){
            rework_text_type rework_item;

            QString itmes = data_result.at(i);
            QStringList itemlist = itmes.split(",");
            rework_item.LOT_ID = ((QString)itemlist.at(0)).split("=").at(1);
            rework_item.wafer_count = ((QString)itemlist.at(1)).split("=").at(1);
            rework_item.Defect_name = ((QString)itemlist.at(2)).split("=").at(1);
            rework_item.Material = ((QString)itemlist.at(3)).split("=").at(1);
            rework_item.Defect_Qty = ((QString)itemlist.at(4)).split("=").at(1);
            rework_item.operation_name = ((QString)itemlist.at(5)).split("=").at(1);
            rework_item.Commnet = ((QString)itemlist.at(6)).split("=").at(1);
            rework_item.Commnet = rework_item.Commnet.replace("}","");
            rework_item.Df_rate = QString("%1").arg(query.value("result").toDouble(),0,'f',2);
            rework_list.append(rework_item);
            qDebug()<<itmes;
        }
    }
    query.exec(QString("select * from V_FAB_REPORT_DAILY where (report_time = '%1' AND MATERIAL_GROUP = 'CSP' "
               "AND OPERATION is null AND date_type = 'Daily' AND WORST_TYPE = 'DEFECT') AND (type LIKE '%%2%' OR type IN ('%3'))").arg(sel_date.toString("yyyy-MM-dd")).arg(tr("Defect_type"))
               .arg(tr("chiping")));

    while(query.next()){

        QStringList data_result = NS_core.get_worst_data_summary(sel_date.toString("yyyyMMdd"),sel_date.toString("yyyyMMdd"),"'A'","CSP","DEFECT",
                                       query.value("WORST_CODE").toString());
        for(int i=0;i<data_result.count();i++){
            defect_worst_type defect_item;

            QString itmes = data_result.at(i);
            QStringList itemlist = itmes.split(",");
            defect_item.LOT_ID = ((QString)itemlist.at(0)).split("=").at(1);
            defect_item.wafer_count = ((QString)itemlist.at(1)).split("=").at(1);
            defect_item.Defect_name = ((QString)itemlist.at(2)).split("=").at(1);
            defect_item.Material = ((QString)itemlist.at(3)).split("=").at(1);
            defect_item.Defect_Qty = ((QString)itemlist.at(4)).split("=").at(1);
            defect_item.operation_name = ((QString)itemlist.at(5)).split("=").at(1);
            defect_item.Commnet = ((QString)itemlist.at(6)).split("=").at(1);
            defect_item.Commnet = defect_item.Commnet.replace("}","");
            defect_item.Df_rate = QString("%1").arg(query.value("result").toDouble(),0,'f',2);
            defect_list.append(defect_item);
            qDebug()<<itmes;
        }
    }

    emit sig_debug_output("complete go excel");
    emit sig_excel_work();
    mutex->unlock();

}
