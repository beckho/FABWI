#ifndef WORST_SEARCH_TC_SAW_TH_H
#define WORST_SEARCH_TC_SAW_TH_H

#include <QObject>
#include <QWidget>
#include <QThread>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QSettings>
#include <global_define.h>
#include <QMessageBox>
#include <QVector>
#include <QDateTime>
#include <QApplication>
#include <QFile>
#include <QAxObject>
#include <QMutex>
#include <ns_core_com/ns_core_com.h>
#define REAL_QUERY_TC_SAW
#define RoundOff(x, dig) (floor((x) * pow(10,dig) + 0.5) / pow(10,dig))
class TC_CSP_rework_text_type{
public:
    QString Material;
    QString LOT_ID;
    QString operation_name;
    QString wafer_count;
    QString Defect_Qty;
    QString Commnet;
    QString Defect_name;
    QString Df_rate;

};

class TC_CSP_defect_worst_type{
 public:
    QString Material;
    QString LOT_ID;
    QString operation_name;
    QString wafer_count;
    QString Defect_Qty;
    QString Commnet;
    QString Defect_name;
    QString Df_rate;

};

class worst_search_TC_SAW_th : public QThread
{
    Q_OBJECT
public:
    worst_search_TC_SAW_th(QMutex *mutex);
    QSqlDatabase ms_mesdb;
    QSqlDatabase my_mesdb;
    double low_q[2] = {0,0};
    double high_q[2] = {0,0};
    double limit_wosrt[2] = {0,0};
    double os_wosrt[2] = {0,0};
    double bw_wosrt[2] = {0,0};
    double vswr_wosrt[2] = {0,0};
    double machine_d_wosrt[2] = {0,0};
    double worker_d_wosrt[2] = {0,0};
    double woker_m_wosrt[2] = {0,0};
    double paticle_wosrt[2] = {0,0};
    double patten_paticle[2] = {0,0};
    double pad_paticle[2] = {0,0};
    double sin_miss[2] = {0,0};
    double etc_paticle[2] = {0,0};
    double lp_yield[2] = {0,0};
    double yield[2] = {0,0};
    double map_delte[2]={0,0};
    double etc[2]={0,0};
    QDate sel_date;
    QMutex *mutex;
    NS_Core_Com::NS_Main NS_core;
    QVector<TC_CSP_rework_text_type> rework_list;
    QVector<TC_CSP_defect_worst_type> defect_list;

    double H780AA4_PAFKU_lp[2] = {0,0};
    double X806BYH_RXBFKPU_lp[2] = {0,0};
    double X806BYH_TXBFKPU_lp[2] = {0,0};
    double X897DYT_RXBFKPU_lp[2] = {0,0};
    double X897DYT_TXBFKPU_lp[2] = {0,0};
    double X897EYT_TXBFKP_lp[2] = {0,0};
    double X898EYT_RXBFKP_lp[2] = {0,0};

    double X806BYH_RXBFKPU[2] = {0,0};
    double X806BYH_TXBFKPU[2] = {0,0};
    double X897DYT_RXBFKPU[2] = {0,0};
    double X897DYT_TXBFKPU[2] = {0,0};


signals:
    void sig_excel_work();
    void sig_debug_output(QString str);
private:
    void run();
};

#endif // WORST_SEARCH_TC_SAW_TH_H
