#ifndef OCR_MAIN_TABLE_H
#define OCR_MAIN_TABLE_H

#include <QObject>
#include <QWidget>
#include <QTableView>
#include <QKeyEvent>
#include <QApplication>
#include <QClipboard>
#include <QDebug>
class OCR_Main_table : public QTableView
{
    Q_OBJECT
public:
    OCR_Main_table(QWidget *parent = 0);
    bool copy_flag;
private:
    void keyPressEvent(QKeyEvent *event);
};

#endif // OCR_MAIN_TABLE_H
