#ifndef OCR_MAIN_H
#define OCR_MAIN_H

#include <QWidget>
#include <QSqlTableModel>
#include <QSettings>
#include <QSqlDatabase>
#include <global_define.h>
#include <Problem_Occurs_Report/ocr_main_table.h>
#include <QSqlError>
namespace Ui {
class OCR_Main;
}

class OCR_Main : public QWidget
{
    Q_OBJECT

public:
    explicit OCR_Main(QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    QSqlTableModel *main_tablemodel;
    ~OCR_Main();

private:
    Ui::OCR_Main *ui;
};

#endif // OCR_MAIN_H
