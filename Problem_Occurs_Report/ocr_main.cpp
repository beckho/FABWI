#include "ocr_main.h"
#include "ui_ocr_main.h"

OCR_Main::OCR_Main(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OCR_Main)
{
    ui->setupUi(this);
    QString mydb_name = QString("MY_MESDB_%1_ocr_main").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    QString server_ip = settings.value("serverip").toString();
    QString db_port = settings.value("dbport").toString();
    settings.endGroup();
    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }

    main_tablemodel = new QSqlTableModel(this,my_mesdb);
    main_tablemodel->setEditStrategy(QSqlTableModel::OnFieldChange);
    main_tablemodel->setTable("problem_occurs_report");
    main_tablemodel->select();

    ui->main_table->setModel(main_tablemodel);




}

OCR_Main::~OCR_Main()
{
    delete ui;
}
