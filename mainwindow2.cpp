#include "mainwindow2.h"
#include "ui_mainwindow2.h"
#include "QDebug"
#include <eismain.h>
#include <e2r_every_report.h>
Mainwindow2::Mainwindow2(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Mainwindow2)
{
    ui->setupUi(this);
    QCoreApplication::setOrganizationName("wisol");
    QCoreApplication::setOrganizationDomain("wisol");
    QCoreApplication::setApplicationName("FMS");
    settings = new QSettings("main2set.ini",QSettings::IniFormat);
    regsettings = new QSettings("EIS","EIS");
    regsettings->beginGroup("path");
    regsettings->setValue("path",qApp->applicationFilePath());
    regsettings->endGroup();
    qDebug()<<qApp->applicationFilePath();
    settings->beginGroup("mainmode");
    mainmode = settings->value("mainmode").toString();
    settings->endGroup();

    if(mainmode == "CSP"){
        ui->actionCSP->setChecked(true);
    }else if(mainmode == "WLP"){
        ui->actionWLP->setChecked(true);
    }else if(mainmode == "BPROJECT") {
        ui->actionBPROJECT->setChecked(true);
    }else {
        ui->actionCSP->setChecked(true);
    }

}


Mainwindow2::~Mainwindow2()
{
    delete ui;
}







void Mainwindow2::yield_pcver_trigger(bool value)
{
    deshboardmain *from  = new deshboardmain();
    from->show();
}

void Mainwindow2::yield_webver_trigger(bool value)
{
    QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/yield_desh/"));
}

void Mainwindow2::oi_ver1_trigger(bool value)
{
    QMessageBox *box = new QMessageBox();
    box->setText(tr("use oi system 2"));
    box->exec();
    QString OI_program_path = QString("\"%1/operating_ratio_program/OIservice.exe\"").arg(qApp->applicationDirPath());
    qDebug()<<OI_program_path;
    QProcess OI_program;
    OI_program.startDetached(OI_program_path);
}

void Mainwindow2::oi_ver2_trigger(bool value)
{
    QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/oirate/"));
}




void Mainwindow2::on_action_temp_humi_triggered()
{
    temp_humi_widget *popup = new temp_humi_widget();
    popup->show();
}

void Mainwindow2::on_action_worst_search_triggered()
{
    worst_search_excel_main *popup = new worst_search_excel_main();
    popup->show();

}

void Mainwindow2::on_Edu_action_triggered()
{
    EDUmain *eduwidget = new EDUmain();
    eduwidget->show();
}

void Mainwindow2::on_action_etching_analysor_triggered()
{
    etching_analysor *analysor = new etching_analysor();
    analysor->show();

}

void Mainwindow2::on_action_nikon_err_log_triggered()
{
    nikon_log_err_main *analysor = new nikon_log_err_main();
    analysor->show();

}

void Mainwindow2::on_action_nikon_err_time_triggered()
{
    nikon_time_log *analysor = new nikon_time_log();
    analysor->show();
}

void Mainwindow2::on_action_asml_err_triggered()
{
    ASML_err_log_main *analysor = new ASML_err_log_main();
    analysor->show();
}

void Mainwindow2::on_action_MES_bridge_chart_triggered()
{
    bridge_chart_widget *bridgewidget = new bridge_chart_widget();
    bridgewidget->show();
}

void Mainwindow2::on_actionACT_triggered()
{
    Act_log_analysor *from = new Act_log_analysor();
    from->show();
}

void Mainwindow2::on_actionCD_SEM_triggered()
{
    Cdsemmain *from = new Cdsemmain();
    from->show();
}

void Mainwindow2::on_actionShincron_triggered()
{
    Shincron_log *from = new Shincron_log();
    from->show();
}

void Mainwindow2::on_actionE_SPC_triggered()
{
    ESCP_main *from = new ESCP_main();
    from->show();
}

void Mainwindow2::on_action_change_histroy_manager_triggered()
{
    QString program_path = QString("\"%1/change_history.exe\"").arg(qApp->applicationDirPath());
    qDebug()<<program_path;
    QProcess change_history_program;
    change_history_program.startDetached(program_path);
}

void Mainwindow2::on_action_machine_lead_time_triggered()
{
    QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/machine_lead_time/"));
}

void Mainwindow2::on_action_deposition_cpcpk_triggered()
{
    QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/web_cpcpk/#/cpcpkreport/deposition"));
}

void Mainwindow2::on_actionCSP_toggled(bool arg1)
{

    if(arg1){
        settings->beginGroup("mainmode");
        settings->setValue("mainmode","CSP");
        settings->endGroup();
        clearLayout(ui->maingrid_layout);
        CSPBoard *cspboard = new CSPBoard();
        ui->maingrid_layout->addWidget(cspboard);
    }
}

void Mainwindow2::on_actionWLP_toggled(bool arg1)
{

    if(arg1){
        settings->beginGroup("mainmode");
        settings->setValue("mainmode","WLP");
        settings->endGroup();
        clearLayout(ui->maingrid_layout);
        WLPboard *wlpboard = new WLPboard();
        ui->maingrid_layout->addWidget(wlpboard);
    }
}

void Mainwindow2::clearLayout(QLayout *layout, bool deleteWidgets)
{
    while (QLayoutItem* item = layout->takeAt(0))
    {
        QWidget* widget;
        if (  (deleteWidgets)
              && (widget = item->widget())  ) {
            delete widget;
        }
        if (QLayout* childLayout = item->layout()) {
            clearLayout(childLayout, deleteWidgets);
        }
        delete item;
    }
}

void Mainwindow2::on_actionBPROJECT_toggled(bool arg1)
{
    if(arg1){
        settings->beginGroup("mainmode");
        settings->setValue("mainmode","BPROJECT");
        settings->endGroup();
        clearLayout(ui->maingrid_layout);
        BprojectBoard *bprojectboard = new BprojectBoard();
        ui->maingrid_layout->addWidget(bprojectboard);
    }
}

void Mainwindow2::on_actionTPRCalc_triggered()
{
    TPRCalcMain *from = new TPRCalcMain();
    from->show();

}
