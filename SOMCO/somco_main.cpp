#include "somco_main.h"
#include "ui_somco_main.h"

SOMCO_main::SOMCO_main(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SOMCO_main)
{
    ui->setupUi(this);
}

SOMCO_main::~SOMCO_main()
{
    delete ui;
}

void SOMCO_main::on_pushButton_clicked()
{
    QAxObject* excel = new QAxObject( "Excel.Application",0);
    excel->dynamicCall("SetVisible(bool)",true);
    QAxObject* workbooks = excel->querySubObject( "Workbooks" );
    QAxObject *workbook = workbooks->querySubObject("Open(const QString&)","C:\\RecipeEditor.xls");
    QAxObject *now_sheet = workbook->querySubObject( "ActiveSheet");
    qDebug()<<now_sheet->dynamicCall("Name");
    QAxObject *change_sell = now_sheet->querySubObject("Cells(int,int)",2,5);
    QAxObject *change_sell1 = now_sheet->querySubObject("Cells(int,int)",12,4);
     change_sell->setProperty("Value","30");
     change_sell1->setProperty("Value","30");
     QThread::sleep(2);
     change_sell->setProperty("Value","31");
     change_sell1->setProperty("Value","31");
     QThread::sleep(2);
    change_sell->setProperty("Value","32");
    change_sell1->setProperty("Value","33");

}
