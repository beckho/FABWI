#ifndef SOMCO_MAIN_H
#define SOMCO_MAIN_H

#include <QWidget>
#include <QAxObject>
#include <QDebug>
#include <QThread>
namespace Ui {
class SOMCO_main;
}

class SOMCO_main : public QWidget
{
    Q_OBJECT

public:
    explicit SOMCO_main(QWidget *parent = 0);
    ~SOMCO_main();

private slots:
    void on_pushButton_clicked();

private:
    Ui::SOMCO_main *ui;
};

#endif // SOMCO_MAIN_H
