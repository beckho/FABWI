#ifndef DESHBOARDMAIN_H
#define DESHBOARDMAIN_H

#include <QWidget>
#include <QDebug>
#include <QSqlDatabase>
#include <deshboard/desh_drilldownchart.h>
#include <deshboard/desh_drill_chartview.h>
#include <deshboard/desh_drill_downbar_series.h>
#include <QDate>
#include <QSettings>
#include <QSqlQuery>
#include <QSqlError>
#include <QBarset>
#include <QBarCategoryAxis>
#include <global_define.h>
#include <deshboard/desh_output_widget.h>
#include <QDateEdit>
#include <QPushButton>
#include <deshboard/desh_setup_spec_popup.h>
#include <QLineSeries>
#include <QLabel>
#include <QCheckBox>
#include <QProcess>
#include <deshboard/desh_csp_daily.h>

QT_CHARTS_USE_NAMESPACE

class chart_maker_data{

public:
    chart_maker_data();
    desh_drilldownchart *main_chart;
    QDateEdit *start_date;
    QDateEdit *end_date;
    QPushButton *serarch_btn;
    QStringList source_data_list;
    QStringList tag_data_list;
    QString barseriesname;
    QString type_info;
    QCheckBox *Cb_content;


};
class chart_maker_data2{

public:
     chart_maker_data2(QString source,int count,QString tag);
     QString source;
     int count;
     QString tag;

};

namespace Ui {
class deshboardmain;
}

class deshboardmain : public QWidget
{
    Q_OBJECT

public:
    explicit deshboardmain(QWidget *parent = 0);
    QSqlDatabase my_mesdb;

    desh_drilldownchart *work_miss_chart;
    Desh_drill_chartview *work_miss_chartview;
    chart_maker_data *work_miss_data;

    desh_drilldownchart *woker_defect_chart;
    Desh_drill_chartview *woker_defect_chartview;
    chart_maker_data *woker_defect_data;

    desh_drilldownchart *frequency_miss_chart;
    Desh_drill_chartview *frequency_miss_chartview;
    chart_maker_data *frequency_miss_data;

    desh_drilldownchart *paticle_miss_chart;
    Desh_drill_chartview *paticle_miss_chartview;
    chart_maker_data *paticle_miss_data;

    desh_drilldownchart *machine_destory_chart;
    Desh_drill_chartview *machine_destory_chartview;
    chart_maker_data *machine_destory_data;

    desh_drilldownchart *machine_defect_chart;
    Desh_drill_chartview *machine_defect_chartview;
    chart_maker_data *machine_defect_data;

    desh_drilldownchart *characteristic_faulty_chart;
    Desh_drill_chartview *characteristic_faulty_chartview;
    chart_maker_data *characteristic_faulty_data;

    desh_drilldownchart *machine_faulty_chart;
    Desh_drill_chartview *machine_faulty_chartview;
    chart_maker_data *machine_faulty_data;


    desh_drilldownchart *yield_chart;
    Desh_drill_chartview *yield_chartview;
    chart_maker_data *yield_data;


    desh_drilldownchart *process_faulty_chart;
    Desh_drill_chartview *process_faulty_chartview;
    chart_maker_data *process_faulty_data;

    desh_csp_daily *csp_daily_widget;


    void chart_maker_ver1(chart_maker_data *source_data);
    void chart_maker_ver2(chart_maker_data *source_data);
    void yield_chart_maker();
    Ui::deshboardmain *ui;
    ~deshboardmain();

private slots:

    void on_work_miss_search_btn_clicked();
    void work_miss_btn_enable();
    void woker_defect_btn_enable();
    void frequency_miss_btn_enable();
    void paticle_miss_btn_enable();
    void machine_destory_btn_enable();
    void machine_defect_btn_enable();
    void characteristic_faulty_btn_enable();
    void process_faulty_btn_enable();
    void machine_faulty_btn_enable();
    void yield_btn_enable();



    void on_work_miss_output_clicked();

    void on_woker_defect_search_btn_clicked();

    void on_woker_defect_output_clicked();

    void on_frequency_miss_search_btn_clicked();

    void on_frequency_miss_zoomreset_clicked();

    void on_frequency_miss_output_clicked();

    void on_paticle_miss_search_btn_clicked();

    void on_paticle_miss_zoomreset_clicked();

    void on_paticle_miss_output_clicked();

    void on_machine_destory_search_btn_clicked();

    void on_machine_destory_zoomreset_clicked();

    void on_machine_destory_output_clicked();

    void on_machine_defect_search_btn_clicked();

    void on_machine_defect_zoomreset_clicked();

    void on_machine_defect_output_clicked();

    void on_woker_defect_zoomreset_clicked();

    void on_yield_search_btn_clicked();

    void on_yield_zoomreset_clicked();

    void on_yield_output_clicked();

    void on_yelid_spec_btn_clicked();

    void on_frequency_spec_btn_clicked();

    void on_paticle_miss_spec_btn_clicked();

    void on_machine_destory_spec_btn_clicked();

    void on_work_miss_spec_btn_clicked();

    void on_woker_defect_btn_clicked();

    void on_machine_defect_btn_clicked();

    void on_process_faulty_search_btn_clicked();

    void on_process_faulty_zoomreset_clicked();

    void on_process_faulty_spec_btn_clicked();

    void on_process_faulty_output_clicked();

    void on_machine_faulty_search_btn_clicked();

    void on_machine_faulty_zoomreset_clicked();

    void on_machine_faulty_spec_btn_clicked();

    void on_machine_faulty_output_clicked();

    void on_tabWidget_currentChanged(int index);

    void on_characteristic_faulty_search_btn_clicked();

    void on_characteristic_faulty_zoomreset_clicked();

    void on_characteristic_faulty_spec_btn_clicked();

    void on_characteristic_faulty_output_clicked();

    void on_yield_start_date_dateChanged(const QDate &date);

    void on_yield_end_date_dateChanged(const QDate &date);

private:

};

#endif // DESHBOARDMAIN_H
