#include "desh_yield_detail_chart.h"


desh_yield_detail_chart::desh_yield_detail_chart(QGraphicsItem *parent, Qt::WindowFlags wFlags)
    : QChart(QChart::ChartTypeCartesian, parent, wFlags)
{
    QFont title_font;
    title_font.setBold(true);
    title_font.setPointSize(15);
    title_font.setFamily("Agency FB");

    setTitleFont(title_font);

}

