#ifndef DESH_CSP_DAILY_H
#define DESH_CSP_DAILY_H

#include <QWidget>
#include <QSettings>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QSqlDatabase>
#include <QDate>
#include <QDateTime>
#include <QFile>

namespace Ui {
class desh_csp_daily;
}

class desh_csp_daily : public QWidget
{
    Q_OBJECT

public:
    explicit desh_csp_daily(QSqlDatabase my_mesdb,QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    ~desh_csp_daily();

private slots:
    void on_search_btn_clicked();

private:
    Ui::desh_csp_daily *ui;
};

#endif // DESH_CSP_DAILY_H
