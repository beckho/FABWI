#ifndef DESH_TABLE_VIEW_H
#define DESH_TABLE_VIEW_H

#include <QObject>
#include <QWidget>
#include <QTableView>
#include <QKeyEvent>
#include <QApplication>
#include <QClipboard>

class desh_table_view : public QTableView
{
    Q_OBJECT
public:
    desh_table_view(QWidget *parent = 0);

private:
    void keyPressEvent(QKeyEvent *event);
};

#endif // DESH_TABLE_VIEW_H
