#ifndef DESH_YIELD_CHART_VIEW_H
#define DESH_YIELD_CHART_VIEW_H

#include <QObject>
#include <QWidget>
#include <QChartView>
#include <QChart>
QT_CHARTS_USE_NAMESPACE

class desh_yield_detail_chart_view : public QChartView
{
    Q_OBJECT
public:
    desh_yield_detail_chart_view(QChart *chart, QWidget *parent = 0);
    QChart *mchart;
protected:
    void keyPressEvent(QKeyEvent *event);
};

#endif // DESH_YIELD_CHART_VIEW_H
