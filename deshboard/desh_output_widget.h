#ifndef DESH_OUTPUT_WIDGET_H
#define DESH_OUTPUT_WIDGET_H

#include <QWidget>
#include <QCloseEvent>
#include <QSplitter>
namespace Ui {
class desh_output_widget;
}

class desh_output_widget : public QWidget
{
    Q_OBJECT

public:
    explicit desh_output_widget(QWidget *append,QSplitter * splitter,QWidget *parent);
    QWidget *append_widget;
    QWidget *parent_widget;
    QSplitter * splitter;
    void closeEvent(QCloseEvent *event);
    ~desh_output_widget();

private:
    Ui::desh_output_widget *ui;
};

#endif // DESH_OUTPUT_WIDGET_H
