#include "desh_drilldownchart.h"

#include <QtCharts/QBarCategoryAxis>

QT_CHARTS_USE_NAMESPACE

desh_drilldownchart::desh_drilldownchart(QGraphicsItem *parent, Qt::WindowFlags wFlags)
    : QChart(QChart::ChartTypeCartesian, parent, wFlags),
      m_currentSeries(0),m_currentsepcSeries(0)
{
    QFont title_font;
    title_font.setBold(true);
    title_font.setPointSize(15);
    title_font.setFamily("Agency FB");

    setTitleFont(title_font);
}

QSqlDatabase desh_drilldownchart::getMy_mesdb() const
{
    return my_mesdb;
}

void desh_drilldownchart::setMy_mesdb(const QSqlDatabase &value)
{
    my_mesdb = value;
}

desh_drilldownchart::~desh_drilldownchart()
{
    qDebug()<<"123";
}

QLabel *desh_drilldownchart::getHover_label() const
{
    return hover_label;
}

void desh_drilldownchart::setHover_label(QLabel *value)
{
    hover_label = value;
}

bool desh_drilldownchart::event(QEvent *event)
{
    if(event->type()==QEvent::LayoutRequest){
        emit complete();
    }
    return QChart::event(event);
}



void desh_drilldownchart::changeSeries(QBarSeries *series,int index, QBarSet *barset)
{
    if (m_currentSeries) {
        removeSeries(m_currentSeries);
    }
    if(m_currentsepcSeries){
        removeSeries(m_currentsepcSeries);
    }

    m_currentSeries = series;
    QStringList category_list;
    QSqlQuery query(my_mesdb);
    if(barset->property("stack_type") == 1){
        QList<QVariant> last_date_list = barset->property("last_date_list").toList();
        QDate last_date;
        QBarSet *temp_bar_set = new QBarSet(barset->property("tag").toString());

        temp_bar_set->setLabelColor(QColor("black"));
        if(barset->property("soruce").toString()=="'yield'"){
            last_date = last_date_list.at(barset->property("index").toInt()).toDate();
            temp_bar_set->setProperty("stack_type",4);
            temp_bar_set->setProperty("date",last_date);

        }else{
            last_date = last_date_list.at(index).toDate();
            temp_bar_set->setProperty("stack_type",2);
            temp_bar_set->setProperty("last_day_data",last_date);

        }

        int day_count  = QDate(last_date.year(),last_date.month(),1).daysTo(last_date)+1;
        temp_bar_set->setProperty("cb_content",barset->property("cb_content").toLongLong());

        temp_bar_set->setProperty("tag",barset->property("tag").toString());
        series->setName(last_date.toString("MM ")+tr("month"));

        for(int i=1;i<=day_count;i++){
            QDate input_day = QDate(last_date.year(),last_date.month(),i);
            QString query_txt;
            if(barset->property("tag").toString() == tr("total")){
                query_txt = QString("select SUM(`result`) AS result,`report_time` "
                                            "from V_FAB_REPORT_DAILY where `report_time` = '%1' AND "
                                            "`type` IN (%2) AND `date_type` IN ('Daily') AND MATERIAL_GROUP IN ('CSP') AND OPERATION is NULL ")
                                            .arg(input_day.toString("yyyy-MM-dd"))
                                            .arg(total_sum_search_txt);
                temp_bar_set->setProperty("soruce",total_sum_search_txt);

            }else {
                query_txt = QString("select SUM(`result`) AS result,`report_time` "
                                            "from V_FAB_REPORT_DAILY where `report_time` = '%1' AND "
                                            "`type` IN (%2) AND `date_type` IN ('Daily') AND MATERIAL_GROUP IN ('CSP') AND OPERATION is NULL ")
                                            .arg(input_day.toString("yyyy-MM-dd"))
                                            .arg(barset->property("soruce").toString());
                temp_bar_set->setProperty("soruce",barset->property("soruce").toString());
            }



            query.exec(query_txt);

            if(query.next()){
                temp_bar_set->append(query.value("result").toReal());
            }

            category_list.append(QString("%1").arg(i));
        }

        m_currentSeries->append(temp_bar_set);
        connect(m_currentSeries,SIGNAL(hovered(bool,int,QBarSet*)),this,SLOT(handleHover_type_1(bool,int,QBarSet*)));
    }else if(barset->property("stack_type") == 3 ){

    }else {

    }
    m_currentSeries->setLabelsFormat("@real2f");
    m_currentSeries->setLabelsVisible(true);
    m_currentSeries->setLabelsAngle(90);
    m_currentSeries->setLabelsPosition(QAbstractBarSeries::LabelsInsideBase);
    connect(m_currentSeries,SIGNAL(clicked(int,QBarSet*)),this,SLOT(handleClicked(int,QBarSet*)));

    // Reset axis
    QBarCategoryAxis *axis = new QBarCategoryAxis();
    axis->append(category_list);


    addSeries(m_currentSeries);

    createDefaultAxes();
    setAxisX(axis, m_currentSeries);


    setTitle(series->name());
}

void desh_drilldownchart::handleClicked(int index, QBarSet *barset)
{

    QCheckBox *source_cb = (QCheckBox *)barset->property("cb_content").toLongLong();
    qDebug()<<barset->property("tag").toString();
    if((source_cb != 0) && (barset->property("soruce").toString() != "'yield'") && (barset->property("soruce").toString() != tr("total"))){
        if(source_cb->isChecked()){
            if(m_currentSeries && barset->property("stack_type") == 1){
                QDate last_date = barset->property("last_date_list").toList().at(index).toDate();
                QString datestr = QString("'%1' AND '%2'").arg(last_date.toString("yyyyMM01"))
                        .arg(last_date.addDays(1).toString("yyyyMMdd"));
                QString source_data;
                if(barset->property("tag").toString() == tr("total")){
                    source_data = total_sum_search_txt;
                }else {
                    source_data = barset->property("soruce").toString();
                }

                desh_rowdata_popup *popup = new desh_rowdata_popup(last_date.toString("yyyyMM01"),last_date.toString("yyyyMMdd"),source_data,my_mesdb);
                popup->show();
                return ;
            }else if(m_currentSeries && barset->property("stack_type") == 2){
                QDate last_date = barset->property("last_day_data").toDate();
                last_date = QDate(last_date.year(),last_date.month(),index+1);
                QString datestr = QString("'%1' AND '%2'").arg(last_date.toString("yyyyMMdd"))
                        .arg(last_date.addDays(1).toString("yyyyMMdd"));
                QString source_data;
                if(barset->property("tag").toString() == tr("total")){
                    source_data = total_sum_search_txt;
                }else {
                    source_data = barset->property("soruce").toString();
                }
                desh_rowdata_popup *popup = new desh_rowdata_popup(last_date.toString("yyyyMMdd"),last_date.toString("yyyyMMdd"),source_data,my_mesdb);
                popup->show();
                return ;
            }
        }
    }


    if(m_currentSeries && barset->property("stack_type") == 2){
        disconnect(m_currentSeries,SIGNAL(clicked(int,QBarSet*)),this,SLOT(handleClicked(int,QBarSet*)));
        emit home_return_signal();
    }else if(m_currentSeries && barset->property("stack_type") == 3){
        desh_yield_detail_popup *yield_popup = new desh_yield_detail_popup(barset->property("date").toDate(),my_mesdb);
        yield_popup->show();
        return ;
    }else if(m_currentSeries && barset->property("stack_type") == 4){
        QDate last_date = barset->property("date").toDate();
        QDate select_date = QDate(last_date.year(),last_date.month(),index+1);
        desh_yield_detail_popup *yield_popup = new desh_yield_detail_popup(select_date,my_mesdb);
        yield_popup->show();
        return ;
    }else {
        QBarSeries *series = new QBarSeries();
        changeSeries(series,index,barset);
    }



}

void desh_drilldownchart::handleHover(bool hoverd, int index, QBarSet *barset)
{
    if(hoverd){
        hover_label->setText(QString("%1 = %2").arg(barset->property("tag").toString()).arg(barset->at(index)));
    }else {
        hover_label->setText("");
    }
}

void desh_drilldownchart::handleHover_type_1(bool hoverd, int index, QBarSet *barset)
{
    if(hoverd){
        hover_label->setText(QString("%1 = %2").arg(title()).arg(barset->at(index)));
    }else {
        hover_label->setText("");
    }
}

QBarSeries *desh_drilldownchart::currentSeries() const
{
    return m_currentSeries;
}

void desh_drilldownchart::setCurrentSeries(QBarSeries *currentSeries)
{
    m_currentSeries = currentSeries;
}
QLineSeries *desh_drilldownchart::getCurrentsepcSeries() const
{
    return m_currentsepcSeries;
}

void desh_drilldownchart::setCurrentsepcSeries(QLineSeries *currentsepcSeries)
{
    m_currentsepcSeries = currentsepcSeries;
}


