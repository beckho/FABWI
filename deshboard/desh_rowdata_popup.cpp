#include "desh_rowdata_popup.h"
#include "ui_desh_rowdata_popup.h"

desh_rowdata_popup::desh_rowdata_popup(QString start_date,QString end_date, QString data_name, QSqlDatabase my_mesdb, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::desh_rowdata_popup)
{
    ui->setupUi(this);
    this->date = date;
    this->data_name = data_name;
    this->my_mesdb= my_mesdb;

    query_txt = QString("select * from V_FAB_REPORT_DAILY where `report_time` between '%1' AND '%2' AND `type` = %3 "
                        "AND `MATERIAL_GROUP` = '%4' AND WORST_CODE is not NULL AND OPERATION is NULL group by WORST_CODE ; ").arg(start_date).arg(end_date).arg(data_name).arg("CSP");
    QSqlQuery query(my_mesdb);
    query.exec(query_txt);

    QString db_name = QString("local_DB_OI_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    light_db = QSqlDatabase::addDatabase("QSQLITE",db_name);
    light_db.setDatabaseName(":memory:");
    if(!light_db.open()){
        qDebug()<<light_db.lastError().text();
    }else {
        qDebug()<<"open";
    }


    QSqlQuery light_query(light_db);
    light_query.exec("CREATE TABLE [Wrost_table]( "
                     "[Grade] VARCHAR(10), "
                     "[Lot Id] VARCHAR(20), "
                     "[Comment] TEXT , "
                     "[Defect Name] VARCHAR(20), "
                     "[Wafer Id] VARCHAR(20), "
                     "[Operation] VARCHAR(25), "
                     "[Operation Name] VARCHAR(30), "
                     "[Category] VARCHAR(20), "
                     "[MATERIAL_GROUP1] VARCHAR(10), "
                     "[MATERIAL_GROUP2] VARCHAR(10), "
                     "[Material] VARCHAR(20), "
                     "[Main Equipment] VARCHAR(20), "
                     "[Main Equipment Name] VARCHAR(20), "
                     "[Defect Code] VARCHAR(20), "
                     "[Defect Qty] INT64, "
                     "[Date] VARCHAR(20), "
                     "[User Name] VARCHAR(20) "
                     ");");

    data_model2 = new QSqlTableModel(this,light_db);


    QString sql_make = "insert into Wrost_table ([Grade],[Lot Id],[Wafer Id],[Operation],"
                       "[Operation Name],[Category],[MATERIAL_GROUP1],[MATERIAL_GROUP2],[Material],"
                       "[Main Equipment],[Main Equipment Name],[Defect Code],[Defect Name],"
                       "[Defect Qty],[Date],[User Name],[Comment]) values "
                       " ";


    NS_Core_Com::NS_Main NS_core;
    qDebug()<<NS_core.init_NS_Core();

    while(query.next()){
        QString worst_type = query.value("WORST_TYPE").toString();

        if(worst_type == "REWORK"){
            worst_type = "FAIL";
        }
        qDebug()<<NS_core.get_worst_data(start_date,end_date,"'A'","CSP",worst_type,query.value("WORST_CODE").toString());
        int columns_count = 0;
        int row_count = NS_core.current_datatable_count();

        for(int i=0;i<row_count;i++){
            QStringList datalist =  NS_core.current_datatable_data(i);
            QString insert_sql;
            for(int j=0;j<datalist.count();j++){
                QString items = datalist.at(j);
                if(j == 13){
                    insert_sql.append(""+items+",");
                }else {
                    items.replace("'","''");
//                    items.replace("\"","'\"");

                    insert_sql.append("'"+items+"',");
                }

            }
            insert_sql.remove(insert_sql.length()-1,1);
            insert_sql.insert(0,"(");
            insert_sql.insert(insert_sql.length(),"),");
            sql_make.append(insert_sql);

            qDebug()<<i;


        }
    }
    sql_make.remove(sql_make.length()-1,1);
    qDebug()<<sql_make;
    light_query.exec(sql_make);


    qDebug()<<light_query.lastError().text();

    data_model2->setTable("Wrost_table");


    ui->main_data_table->setModel(data_model2);
    ui->main_data_table->setSortingEnabled(true);
    ui->main_data_table->horizontalHeader()->setSectionsMovable(true);
    ui->main_data_table->horizontalHeader()->moveSection(15,4);
    data_model2->select();

}

desh_rowdata_popup::~desh_rowdata_popup()
{
    delete ui;
}

void desh_rowdata_popup::short_change(int index, Qt::SortOrder order)
{
    QSqlRecord recode = data_model->query().record();
    QString fieldname = recode.fieldName(index);
    QString order_type;
    if(order == Qt::AscendingOrder){
        order_type = "asc";
    }else {
        order_type = "desc";
    }
    QString order_txt = QString(" order by %1 %2 ").arg(fieldname).arg(order_type);
    data_model->setQuery(QString("%1 %2").arg(query_txt).arg(order_txt),my_mesdb);
    data_model->submit();
}

void desh_rowdata_popup::closeEvent(QCloseEvent *event)
{

    light_db.close();

    this->deleteLater();
}
