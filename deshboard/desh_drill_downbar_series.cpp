#include "desh_drill_downbar_series.h"

Desh_drill_downbar_series::Desh_drill_downbar_series(QStringList categories, QObject *parent)
                : QBarSeries(parent)
{
    m_categories = categories;
}

void Desh_drill_downbar_series::mapDrilldownSeries(int index, Desh_drill_downbar_series *drilldownSeries)
{
    m_DrilldownSeries[index] = drilldownSeries;
}

Desh_drill_downbar_series *Desh_drill_downbar_series::drilldownSeries(int index)
{
    return m_DrilldownSeries[index];
}

QStringList Desh_drill_downbar_series::categories()
{
    return m_categories;
}
