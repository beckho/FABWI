#include "desh_drill_chartview.h"

Desh_drill_chartview::Desh_drill_chartview(QChart *chart, QWidget *parent):QChartView(chart, parent)
{
    setRubberBand(QChartView::RectangleRubberBand);
    this->mchart= chart;
}

void Desh_drill_chartview::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Plus:
        chart()->zoomIn();
        break;
    case Qt::Key_Minus:
        chart()->zoomOut();
        break;
    case Qt::Key_Left:
        chart()->scroll(-10, 0);
        break;
    case Qt::Key_Right:
        chart()->scroll(10, 0);
        break;
    case Qt::Key_Up:
        chart()->scroll(0, 10);
        break;
    case Qt::Key_Down:
        chart()->scroll(0, -10);
        break;
    default:
        QChartView::keyPressEvent(event);
        break;
    }
    this->update();
    this->updateGeometry();
    mchart->update(mchart->geometry());
}
