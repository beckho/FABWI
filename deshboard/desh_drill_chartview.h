#ifndef DESH_DRILL_CHARTVIEW_H
#define DESH_DRILL_CHARTVIEW_H

#include <QObject>
#include <QWidget>
#include <QChartView>

QT_CHARTS_USE_NAMESPACE

class Desh_drill_chartview : public QChartView
{
public:
    Desh_drill_chartview(QChart *chart, QWidget *parent = 0);
    QChart *mchart;
protected:
    void keyPressEvent(QKeyEvent *event);
};

#endif // DESH_DRILL_CHARTVIEW_H
