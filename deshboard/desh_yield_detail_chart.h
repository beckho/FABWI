#ifndef DESH_YIELD_DETAIL_CHART_H
#define DESH_YIELD_DETAIL_CHART_H

#include <QWidget>
#include <QtCharts/QChart>
#include <QSqlQuery>
QT_CHARTS_USE_NAMESPACE
class desh_yield_detail_chart : public QChart
{
    Q_OBJECT

public:
    explicit desh_yield_detail_chart(QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0);

private:

};

#endif // DESH_YIELD_DETAIL_CHART_H
