#ifndef DESH_DRILL_DOWNBAR_SERIES_H
#define DESH_DRILL_DOWNBAR_SERIES_H

#include <QObject>
#include <QWidget>
#include <QtCharts/QBarSeries>
#include <QtCore/QMap>

QT_CHARTS_USE_NAMESPACE

class Desh_drill_downbar_series : public QBarSeries
{
public:
    Desh_drill_downbar_series(QStringList categories, QObject *parent = 0);
    void mapDrilldownSeries(int index, Desh_drill_downbar_series *drilldownSeries);

    Desh_drill_downbar_series *drilldownSeries(int index);

    QStringList categories();
private:
    QMap<int, Desh_drill_downbar_series *> m_DrilldownSeries;
    QStringList m_categories;
};

#endif // DESH_DRILL_DOWNBAR_SERIES_H
