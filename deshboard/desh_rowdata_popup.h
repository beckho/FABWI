#ifndef DESH_ROWDATA_POPUP_H
#define DESH_ROWDATA_POPUP_H

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QTableView>
#include <deshboard/desh_table_view.h>
#include <QHeaderView>
#include <QSqlRecord>
#include <QStandardItemModel>
#include <ns_core_com/ns_core_com.h>
#include <QAxObject>
#include <QDebug>
#include <QList>
#include <vector>
#include <QSqlError>
#include <QSqlTableModel>
namespace Ui {
class desh_rowdata_popup;
}

class desh_rowdata_popup : public QWidget
{
    Q_OBJECT

public:
    explicit desh_rowdata_popup(QString start_date,QString end_date,QString data_name,QSqlDatabase my_mesdb,QWidget *parent = 0);
    QString date;
    QString data_name;
    QSqlDatabase my_mesdb;
    QSqlQueryModel *data_model;
    QSqlTableModel *data_model2;
    QVector<QString *> *row_items;
    QString query_txt;
    QHeaderView *header;

    QSqlDatabase light_db;

    ~desh_rowdata_popup();
private slots:
    void short_change(int index,Qt::SortOrder order);
private:
    Ui::desh_rowdata_popup *ui;
    void closeEvent(QCloseEvent *event);
};

#endif // DESH_ROWDATA_POPUP_H
