#include "desh_yield_detail_popup.h"
#include "ui_desh_yield_detail_popup.h"

desh_yield_detail_popup::desh_yield_detail_popup(QDate select_date, QSqlDatabase my_mesdb, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::desh_yield_detail_popup)
{
    ui->setupUi(this);
    this->select_date = select_date;
    this->my_mesdb = my_mesdb;
    main_chart = new desh_yield_detail_chart();
    main_chart_view = new desh_yield_detail_chart_view(main_chart);
    QSqlQuery query(my_mesdb);
    query.exec(QString("select `report_time`, `type`,SUM(`result`) as `result` from V_FAB_REPORT_DAILY where `report_time` = '%1' "
               "AND `date_type` = 'Daily' AND NOT `type` in ('DEFECT_SUM','REWORK_SUM') AND `type` NOT LIKE ('%yield') AND `result` > 0 "
               " AND MATERIAL_GROUP IN ('CSP') AND OPERATION is NULL group by `type` order by `result` desc ").arg(select_date.toString("yyyy-MM-dd")));

    qDebug()<<query.lastQuery();
    main_series = new QBarSeries();
    double result_sum = 0;
    while(query.next()){
        QBarSet *temp_bar = new QBarSet(query.value("type").toString());
        temp_bar->append(query.value("result").toReal());
        temp_bar->setProperty("tag",query.value("type").toString());
        temp_bar->setLabelColor("black");
        Catagory_list.append(query.value("type").toString());
        result_sum += query.value("result").toReal();
        main_series->append(temp_bar);
    }
     QBarSet *result_sum_bar = new QBarSet(tr("total"));
     result_sum_bar->append(result_sum);
     result_sum_bar->setLabel(tr("total"));
     result_sum_bar->setLabelColor("black");
     result_sum_bar->setProperty("tag",tr("total"));
     main_series->insert(0,result_sum_bar);
     main_series->setProperty("order","asc");
     main_series->setLabelsFormat("@tag @real2f ");
     main_series->setLabelsPosition(QAbstractBarSeries::LabelsInsideBase);
     main_series->setLabelsAngle(90);
     Catagory_list.insert(0,tr("total"));

     main_series->setBarWidth(1);

     main_series->setLabelsVisible(true);


     main_chart->addSeries(main_series);
     main_chart->setLayoutDirection(Qt::LeftToRight);
     main_chart->legend()->setAlignment(Qt::AlignRight);

     QBarCategoryAxis *axisX = new QBarCategoryAxis();
     axisX->append(Catagory_list);
     main_chart->setAxisX(axisX,main_series);
     main_chart->createDefaultAxes();
     ui->yield_chart_view->addWidget(main_chart_view);

     QString db_name = QString("local_DB_OI_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
     light_db = QSqlDatabase::addDatabase("QSQLITE",db_name);
     light_db.setDatabaseName(":memory:");
     if(!light_db.open()){
         qDebug()<<light_db.lastError().text();
     }else {
         qDebug()<<"open";
     }

     qDebug()<<NS_core.init_NS_Core();

     QSqlQuery light_query(light_db);
      light_query.exec("CREATE TABLE [Wrost_table]( "
                       "[Grade] VARCHAR(10), "
                       "[Lot Id] VARCHAR(20), "
                       "[Comment] TEXT , "
                       "[Defect Name] VARCHAR(20), "
                       "[Wafer Id] VARCHAR(20), "
                       "[Operation] VARCHAR(25), "
                       "[Operation Name] VARCHAR(30), "
                       "[Category] VARCHAR(20), "
                       "[MATERIAL_GROUP1] VARCHAR(10), "
                       "[MATERIAL_GROUP2] VARCHAR(10), "
                       "[Material] VARCHAR(20), "
                       "[Main Equipment] VARCHAR(20), "
                       "[Main Equipment Name] VARCHAR(20), "
                       "[Defect Code] VARCHAR(20), "
                       "[Defect Qty] INT64, "
                       "[Date] VARCHAR(20), "
                       "[User Name] VARCHAR(20) ); "
                       );
     data_model= new QSqlTableModel(this,light_db);
     ui->data_view_table->setModel(data_model);
     ui->data_view_table->setSortingEnabled(true);


     connect(main_series,SIGNAL(clicked(int,QBarSet*)),this,SLOT(bar_set_click(int,QBarSet*)));
     showMaximized();
     bar_set_click(0,main_series->barSets().at(1));

}

desh_yield_detail_popup::~desh_yield_detail_popup()
{
    delete ui;
}

void desh_yield_detail_popup::bar_set_click(int index, QBarSet *barset)
{
    Q_UNUSED(index);
    if(barset->property("tag").toString()==QString(tr("total"))){
        return;
    }

    QSqlQuery light_query(light_db);
    light_query.exec("delete from Wrost_table ");

    QString query_txt = QString("select * from V_FAB_REPORT_DAILY where `report_time` between '%1' AND '%2' AND `type` = '%3' "
                          "AND `MATERIAL_GROUP` = '%4' AND WORST_CODE is not NULL AND OPERATION is NULL group by WORST_CODE ; ").arg(select_date.toString("yyyyMMdd")).arg(select_date.toString("yyyyMMdd")).arg(barset->property("tag").toString()).arg("CSP");
      QSqlQuery query(my_mesdb);
      qDebug()<<query_txt;
      query.exec(query_txt);
      QString sql_make = "insert into Wrost_table ([Grade],[Lot Id],[Wafer Id],[Operation],"
                         "[Operation Name],[Category],[MATERIAL_GROUP1],[MATERIAL_GROUP2],[Material],"
                         "[Main Equipment],[Main Equipment Name],[Defect Code],[Defect Name],"
                         "[Defect Qty],[Date],[User Name],[Comment]) values "
                         " ";
      while(query.next()){
          QString worst_type = query.value("WORST_TYPE").toString();

          if(worst_type == "REWORK"){
              worst_type = "FAIL";
          }
          qDebug()<<NS_core.get_worst_data(select_date.toString("yyyyMMdd"),select_date.toString("yyyyMMdd"),"'A'","CSP",worst_type,query.value("WORST_CODE").toString());
          int columns_count = 0;
          int row_count = NS_core.current_datatable_count();

          for(int i=0;i<row_count;i++){
              QStringList datalist =  NS_core.current_datatable_data(i);
              QString insert_sql;
              for(int j=0;j<datalist.count();j++){
                  QString items = datalist.at(j);
                  if(j == 13){
                      insert_sql.append(""+items+",");
                  }else {
                      items.replace("'","''");
                      items.replace("\"","'\"");
                      insert_sql.append("'"+items+"',");
                  }

              }
              insert_sql.remove(insert_sql.length()-1,1);
              insert_sql.insert(0,"(");
              insert_sql.insert(insert_sql.length(),"),");
              sql_make.append(insert_sql);
              qDebug()<<i;

          }
      }
      sql_make.remove(sql_make.length()-1,1);
      light_query.exec(sql_make);
      qDebug()<<light_query.lastError().text();
      data_model->setTable("Wrost_table");
        ui->data_view_table->horizontalHeader()->setSectionsMovable(true);
        ui->data_view_table->horizontalHeader()->moveSection(15,4);
      data_model->select();



}

void desh_yield_detail_popup::on_zoom_reset_clicked()
{
    main_chart->zoomReset();
}
