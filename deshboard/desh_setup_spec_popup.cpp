#include "desh_setup_spec_popup.h"
#include "ui_desh_setup_spec_popup.h"

Desh_setup_spec_popup::Desh_setup_spec_popup(QSqlDatabase my_mesdb, QString SPEC_NAME, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Desh_setup_spec_popup)
{
    ui->setupUi(this);
    this->my_mesdb = my_mesdb;
    this->SPEC_NAME = SPEC_NAME;
    ui->LE_SPEC_VALUE->setValidator(new QDoubleValidator(-1000,1000,2));
    QSqlQuery query(my_mesdb);
    query.exec(QString("select * from V_FAB_REPORT_SPEC where SPEC_NAME = '%1' AND MATERIAL_GROUP IN ('CSP') "
                       "order by INPUT_TIME desc LIMIT 1").arg(SPEC_NAME));
    if(query.next()){
        ui->LE_SPEC_VALUE->setText(query.value("SPEC_VALUE").toString());
    }

}

Desh_setup_spec_popup::~Desh_setup_spec_popup()
{
    delete ui;
}

void Desh_setup_spec_popup::on_buttonBox_accepted()
{
    QSqlQuery query(my_mesdb);
    query.exec(QString("insert into `V_FAB_REPORT_SPEC` (`SPEC_NAME`, `SPEC_VALUE`,`MATERIAL_GROUP`) "
                       "VALUES ('%1', %2,'CSP'); ").arg(SPEC_NAME).arg(ui->LE_SPEC_VALUE->text()));

}
