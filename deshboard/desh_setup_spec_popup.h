#ifndef DESH_SETUP_SPEC_POPUP_H
#define DESH_SETUP_SPEC_POPUP_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
namespace Ui {
class Desh_setup_spec_popup;
}

class Desh_setup_spec_popup : public QDialog
{
    Q_OBJECT

public:
    explicit Desh_setup_spec_popup(QSqlDatabase my_mesdb,QString SPEC_NAME,QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    QString SPEC_NAME;
    ~Desh_setup_spec_popup();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::Desh_setup_spec_popup *ui;
};

#endif // DESH_SETUP_SPEC_POPUP_H
