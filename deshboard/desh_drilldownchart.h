#ifndef DESH_DRILLDOWNCHAR_H
#define DESH_DRILLDOWNCHAR_H
#include <QObject>
#include <QtCharts/QChart>
#include <deshboard/desh_drill_downbar_series.h>
#include <QBarSeries>
#include <QBarSet>
#include <QDate>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <QLabel>
#include <QLineSeries>
#include <QCheckBox>
#include <deshboard/desh_yield_detail_popup.h>
#include <deshboard/desh_rowdata_popup.h>
QT_CHARTS_USE_NAMESPACE

class desh_drilldownchart : public QChart
{
    Q_OBJECT
public:
    desh_drilldownchart(QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0);
    QSqlDatabase my_mesdb;
    void changeSeries(QBarSeries *series, int index, QBarSet *barset);
    QBarSeries *currentSeries() const;
    void setCurrentSeries(QBarSeries *currentSeries);
    QSqlDatabase getMy_mesdb() const;
    void setMy_mesdb(const QSqlDatabase &value);
    QString total_sum_search_txt;
    QLabel *hover_label;
    ~desh_drilldownchart();


    QLabel *getHover_label() const;
    void setHover_label(QLabel *value);



    QLineSeries *getCurrentsepcSeries() const;
    void setCurrentsepcSeries(QLineSeries *currentsepcSeries);

signals:
    void home_return_signal();
    void complete();


public Q_SLOTS:
    void handleClicked(int index, QBarSet *barset);
    void handleHover(bool hoverd,int index, QBarSet *barset);
    void handleHover_type_1(bool hoverd,int index, QBarSet *barset);
private:
    bool event(QEvent *event);

    QBarSeries *m_currentSeries;
    QLineSeries *m_currentsepcSeries;
};

#endif // DESH_DRILLDOWNCHAR_H
