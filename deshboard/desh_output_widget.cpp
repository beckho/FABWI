#include "desh_output_widget.h"
#include "ui_desh_output_widget.h"
#include <deshboard/deshboardmain.h>
#include "ui_deshboardmain.h"
desh_output_widget::desh_output_widget(QWidget *append, QSplitter *splitter, QWidget *parent) :
    ui(new Ui::desh_output_widget)
{
    ui->setupUi(this);
    append_widget = append;
    parent_widget = parent;
    ui->main_layout->addWidget(append);
    this->splitter = splitter;

}

void desh_output_widget::closeEvent(QCloseEvent *event)
{
    splitter->addWidget(append_widget);

//    QWidget::closeEvent(event);
}

desh_output_widget::~desh_output_widget()
{
    delete ui;
}
