#ifndef DESH_YIELD_DETAIL_POPUP_H
#define DESH_YIELD_DETAIL_POPUP_H

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <deshboard/desh_yield_detail_chart_view.h>
#include <deshboard/desh_table_view.h>
#include <QBarSeries>
#include <QBarSet>
#include <QDate>
#include <QDateTime>
#include <QBarSeries>
#include <QBarSet>
#include <QBarCategoryAxis>
#include <deshboard/desh_yield_detail_chart.h>
#include <QSqlQueryModel>
#include <QDateTime>
#include <QDebug>
#include <ns_core_com/ns_core_com.h>
#include <QSqlTableModel>
#include <QSqlRecord>
QT_CHARTS_USE_NAMESPACE
namespace Ui {
class desh_yield_detail_popup;
}

class desh_yield_detail_popup : public QWidget
{
    Q_OBJECT

public:
    explicit desh_yield_detail_popup(QDate select_date,QSqlDatabase my_mesdb,QWidget *parent = 0);
    QDate select_date;
    QSqlDatabase my_mesdb;
    desh_yield_detail_chart *main_chart;
    QStringList Catagory_list;
    desh_yield_detail_chart_view *main_chart_view;
    QBarSeries *main_series;
    QSqlTableModel *data_model;
    QSqlDatabase light_db;
    NS_Core_Com::NS_Main NS_core;
    ~desh_yield_detail_popup();

private:
    Ui::desh_yield_detail_popup *ui;

public slots:
    void bar_set_click(int index,QBarSet* barset);

private slots:
    void on_zoom_reset_clicked();
};

#endif // DESH_YIELD_DETAIL_POPUP_H
