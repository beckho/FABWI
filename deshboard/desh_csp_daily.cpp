#include "desh_csp_daily.h"
#include "ui_desh_csp_daily.h"

desh_csp_daily::desh_csp_daily(QSqlDatabase my_mesdb, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::desh_csp_daily)
{
    ui->setupUi(this);
    this->my_mesdb;
    ui->DE_start_date->setDate(QDate(QDate::currentDate().year(),01,01));

    ui->DE_end_date->setDate(QDate::currentDate().addDays(-1));

}

desh_csp_daily::~desh_csp_daily()
{
    delete ui;
}

void desh_csp_daily::on_search_btn_clicked()
{
    QDate start_date = ui->DE_start_date->date();
    QDate end_date = ui->DE_end_date->date();
    QList<QDate> acc_report_date;
    QList<QDate> day_report_date;
    int month_count=0;
    while(true){
        QDate temp_date = start_date.addMonths(month_count);
        if((temp_date.month() == end_date.month()) &&
               (temp_date.year() == end_date.year()) ){

            break;
        }else {

        }
        month_count++;
    }
    QString month_create_query;
    for(int i=0;i<month_count;i++){
        month_create_query = month_create_query.append(QString("%1_Month varchar(50) ").arg(start_date.addMonths(i).toString("MM")))+", \r\n ";
        acc_report_date.append(QDate(start_date.addMonths(i).year(),start_date.addMonths(i).month(),start_date.addMonths(i).daysInMonth()));
    }
    QString day_create_query;
    for(int i=1;i<=end_date.day();i++){
        day_create_query = day_create_query.append(QString("%1_day varchar(50)").arg(i))+", \r\n ";
        day_report_date.append(QDate(end_date.year(),end_date.month(),i));
    }
    QString total_create_query = QString("idx varchar(5), \r\n ")+QString("DEFECT varchar(50), \r\n ")+month_create_query+day_create_query;

    total_create_query = total_create_query.append("total varchar(50)");

    QString create_query_text = QString (" drop table IF EXISTS `CSP_table1`; \r\n "
                                         " CREATE TEMPORARY TABLE CSP_table1 ( "
                                         "%1 ) ;").arg(total_create_query);


    QString report_time_insert_query;
    for(int i=0;i<acc_report_date.count();i++){
        report_time_insert_query = acc_report_date.at(i).toString("'yyyy-MM-dd', \r\n");
    }
    report_time_insert_query = report_time_insert_query.remove(report_time_insert_query.length()-4,4);

    QString insert_query = QString("select * from V_FAB_REPORT_DAILY where `report_time` IN (%1) AND "
                                   "`type` IN('%2') AND `date_type` IN ('accumulate') AND MATERIAL_GROUP IN ('CSP')  AND OPERATION is NULL order by report_time asc ")
            .arg(report_time_insert_query).arg(tr("low_freq"));

    qDebug()<<insert_query;
    QFile file("query.sql");
    file.open(QIODevice::WriteOnly);
    QTextStream stream(&file);
    stream<<insert_query;
    stream.flush();
    file.close();






}
