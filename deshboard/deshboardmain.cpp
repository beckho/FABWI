#include "deshboardmain.h"
#include "ui_deshboardmain.h"

deshboardmain::deshboardmain(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::deshboardmain)
{
    ui->setupUi(this);
    this->showMaximized();
    QRect t1_base_rect =  ui->t1_scrollArea->geometry();

    QString mydb_name = QString("MY_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    QString server_ip = settings.value("serverip").toString();
    QString db_port = settings.value("dbport").toString();
    settings.endGroup();

    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }

    QProcess process;
    process.setWorkingDirectory(qApp->applicationDirPath());
    process.start("RegAsm.exe NS_Core_Com.dll");
    process.waitForFinished();
    QString output(process.readAllStandardOutput());
    qDebug()<<output;
    QList<int> sizes;
//    sizes.append(t1_base_rect.width()/3);
//    sizes.append(t1_base_rect.width()/3);
//    sizes.append(t1_base_rect.width()/3);
//    QList<int> sizes1;
//    sizes1.append(t1_base_rect.width()/4);
//    sizes1.append(t1_base_rect.width()/4);
//    sizes.append(t1_base_rect.width()/4);
//    ui->splitter->setSizes(sizes1);
//    ui->splitter_2->setSizes(sizes);

    sizes.clear();
    sizes.append(t1_base_rect.height()/3);
    sizes.append(t1_base_rect.height()/3);
    sizes.append(t1_base_rect.height()/3);
    sizes.append(t1_base_rect.height()/3);
    ui->splitter_3->setSizes(sizes);

    //주파수
    frequency_miss_chart = new desh_drilldownchart();
    frequency_miss_chart->setMy_mesdb(my_mesdb);
    frequency_miss_chart->setAnimationOptions(QChart::SeriesAnimations);
    frequency_miss_chart->setHover_label(ui->frequency_label);
    frequency_miss_chartview =  new Desh_drill_chartview(frequency_miss_chart);
    ui->frequency_miss_layout->addWidget(frequency_miss_chartview);
    ui->frequency_miss_start_date->setDate(QDate::currentDate().addMonths(0));
    ui->frequency_miss_end_date->setDate(QDate::currentDate());
    frequency_miss_data = new chart_maker_data();
    frequency_miss_data->main_chart = frequency_miss_chart;
    frequency_miss_data->start_date = ui->frequency_miss_start_date;
    frequency_miss_data->end_date = ui->frequency_miss_end_date;
    frequency_miss_data->type_info = "frequency";
    frequency_miss_data->Cb_content = ui->frequency_CB_content;

    QStringList frequency_miss_list;
    frequency_miss_list.append(QString("'%1'").arg(tr("frequency_miss_total")));
    frequency_miss_list.append(QString("'%1'").arg(tr("low_frequency")));
    frequency_miss_list.append(QString("'%1'").arg(tr("high_frequency")));
    frequency_miss_list.append(QString("'%1'").arg(tr("limit_defect")));
    frequency_miss_list.append(QString("'%1'").arg(tr("LIMIT")));
    frequency_miss_list.append(QString("'%1'").arg(tr("low_frequency_defect")));
    frequency_miss_list.append(QString("'%1'").arg(tr("high_frequency_defect")));
    frequency_miss_list.append(QString("'%1'").arg(tr("ch_frequency_deviation")));

    QStringList frequency_miss_tag_list;
    frequency_miss_tag_list.append(QString("%1").arg(tr("total")));
    frequency_miss_tag_list.append(QString("%1").arg(tr("low_frequency")));
    frequency_miss_tag_list.append(QString("%1").arg(tr("high_frequency")));
    frequency_miss_tag_list.append(QString("%1").arg(tr("limit_defect")));
    frequency_miss_tag_list.append(QString("'%1'").arg(tr("LIMIT")));
    frequency_miss_tag_list.append(QString("%1").arg(tr("low_frequency_defect")));
    frequency_miss_tag_list.append(QString("%1").arg(tr("high_frequency_defect")));
    frequency_miss_tag_list.append(QString("%1").arg(tr("ch_frequency_deviation")));

    frequency_miss_data->source_data_list = frequency_miss_list;
    frequency_miss_data->tag_data_list = frequency_miss_tag_list;
    frequency_miss_data->serarch_btn = ui->frequency_miss_search_btn;
    frequency_miss_data->barseriesname = tr("frequency_miss");
    connect(frequency_miss_chart,SIGNAL(home_return_signal()),this,SLOT(on_frequency_miss_search_btn_clicked()));
    connect(frequency_miss_chart,SIGNAL(complete()),this,SLOT(frequency_miss_btn_enable()));

    //이물.
    paticle_miss_chart = new desh_drilldownchart();
    paticle_miss_chart->setMy_mesdb(my_mesdb);
    paticle_miss_chart->setAnimationOptions(QChart::SeriesAnimations);
    paticle_miss_chart->setHover_label(ui->paticle_miss_label);
    paticle_miss_chartview =  new Desh_drill_chartview(paticle_miss_chart);
    ui->paticle_miss_layout->addWidget(paticle_miss_chartview);
    ui->paticle_miss_start_date->setDate(QDate::currentDate().addMonths(0));
    ui->paticle_miss_end_date->setDate(QDate::currentDate());
    paticle_miss_data = new chart_maker_data();
    paticle_miss_data->main_chart = paticle_miss_chart;
    paticle_miss_data->start_date = ui->paticle_miss_start_date;
    paticle_miss_data->end_date = ui->paticle_miss_end_date;
    paticle_miss_data->type_info = "paticle_miss";
    paticle_miss_data->Cb_content = ui->paticle_miss_CB_content;
    QStringList paticle_miss_list;
    paticle_miss_list.append(QString("'%1'").arg(tr("paticle_miss_total")));
    paticle_miss_list.append(QString("'%1'").arg(tr("paticle")));
    paticle_miss_list.append(QString("'%1'").arg(tr("organism")));
    paticle_miss_list.append(QString("'%1'").arg(tr("SiN_miss_etching")));
    paticle_miss_list.append(QString("'%1'").arg(tr("SiN_miss_etching_defect")));
    paticle_miss_list.append(QString("'%1'").arg(tr("pattern_paticle")));
    paticle_miss_list.append(QString("'%1'").arg(tr("pad_paticle")));
    paticle_miss_list.append(QString("'%1'").arg(tr("etc_paticle")));
    paticle_miss_list.append(QString("'%1'").arg(tr("bridgePAD_defect")));
    paticle_miss_list.append(QString("'%1'").arg(tr("SIN remain")));
    paticle_miss_list.append(QString("'%1'").arg(tr("PR STRIP don't have")));
    paticle_miss_list.append(QString("'%1'").arg(tr("map delete")));

    QStringList paticle_miss_tag_list;
    paticle_miss_tag_list.append(QString("%1").arg(tr("total")));
    paticle_miss_tag_list.append(QString("%1").arg(tr("paticle")));
    paticle_miss_tag_list.append(QString("%1").arg(tr("organism")));
    paticle_miss_tag_list.append(QString("%1").arg(tr("SiN_miss_etching")));
    paticle_miss_tag_list.append(QString("%1").arg(tr("SiN_miss_etching_defect")));
    paticle_miss_tag_list.append(QString("%1").arg(tr("pattern_paticle")));
    paticle_miss_tag_list.append(QString("%1").arg(tr("pad_paticle")));
    paticle_miss_tag_list.append(QString("%1").arg(tr("etc_paticle")));
    paticle_miss_tag_list.append(QString("%1").arg(tr("bridgePAD_defect")));
    paticle_miss_tag_list.append(QString("'%1'").arg(tr("SIN remain")));
    paticle_miss_tag_list.append(QString("'%1'").arg(tr("PR STRIP don't have")));
    paticle_miss_tag_list.append(QString("'%1'").arg(tr("map delete")));

    paticle_miss_data->source_data_list = paticle_miss_list;
    paticle_miss_data->tag_data_list = paticle_miss_tag_list;
    paticle_miss_data->serarch_btn = ui->paticle_miss_search_btn;
    paticle_miss_data->barseriesname = tr("paticle_miss");
    connect(paticle_miss_chart,SIGNAL(home_return_signal()),this,SLOT(on_paticle_miss_search_btn_clicked()));
    connect(paticle_miss_chart,SIGNAL(complete()),this,SLOT(paticle_miss_btn_enable()));

    //설비고장.
    machine_destory_chart = new desh_drilldownchart();
    machine_destory_chart->setMy_mesdb(my_mesdb);
    machine_destory_chart->setAnimationOptions(QChart::SeriesAnimations);
    machine_destory_chart->setHover_label(ui->machine_destory_label);
    machine_destory_chartview =  new Desh_drill_chartview(machine_destory_chart);
    ui->machine_destory_layout->addWidget(machine_destory_chartview);
    ui->machine_destory_start_date->setDate(QDate::currentDate().addMonths(0));
    ui->machine_destory_end_date->setDate(QDate::currentDate());
    machine_destory_data = new chart_maker_data();
    machine_destory_data->main_chart = machine_destory_chart;
    machine_destory_data->start_date = ui->machine_destory_start_date;
    machine_destory_data->end_date = ui->machine_destory_end_date;
    machine_destory_data->type_info = "machine_destory";
    machine_destory_data->Cb_content = ui->machine_destory_CB_content;
    QStringList machine_destory_list;
    machine_destory_list.append(QString("'%1'").arg(tr("machine_destory_total")));
    machine_destory_list.append(QString("'%1'").arg(tr("machine_destory(deposition)")));
    machine_destory_list.append(QString("'%1'").arg(tr("machine_destory(light)")));
    machine_destory_list.append(QString("'%1'").arg(tr("machine_destory(eatching)")));
    machine_destory_list.append(QString("'%1'").arg(tr("machine_destory(probe)")));


    QStringList machine_destory_tag_list;
    machine_destory_tag_list.append(QString("%1").arg(tr("total")));
    machine_destory_tag_list.append(QString("%1").arg(tr("deposition")));
    machine_destory_tag_list.append(QString("%1").arg(tr("light")));
    machine_destory_tag_list.append(QString("%1").arg(tr("eatching")));
    machine_destory_tag_list.append(QString("%1").arg(tr("probe")));

    machine_destory_data->source_data_list = machine_destory_list;
    machine_destory_data->tag_data_list = machine_destory_tag_list;
    machine_destory_data->serarch_btn = ui->machine_destory_search_btn;
    machine_destory_data->barseriesname = tr("machine_destory");
    connect(machine_destory_chart,SIGNAL(home_return_signal()),this,SLOT(on_machine_destory_search_btn_clicked()));
    connect(machine_destory_chart,SIGNAL(complete()),this,SLOT(machine_destory_btn_enable()));




    //작업미스.
    work_miss_chart = new desh_drilldownchart();
    work_miss_chart->setMy_mesdb(my_mesdb);
    work_miss_chart->setAnimationOptions(QChart::SeriesAnimations);
    work_miss_chart->setHover_label(ui->work_miss_label);
    work_miss_chartview =  new Desh_drill_chartview(work_miss_chart);
    ui->work_miss_layout->addWidget(work_miss_chartview);
    ui->work_miss_start_date->setDate(QDate::currentDate().addMonths(0));
    ui->work_miss_end_date->setDate(QDate::currentDate());
    work_miss_data = new chart_maker_data();
    work_miss_data->main_chart = work_miss_chart;
    work_miss_data->start_date = ui->work_miss_start_date;
    work_miss_data->end_date = ui->work_miss_end_date;
    work_miss_data->type_info = "work_miss";
    work_miss_data->Cb_content = ui->work_miss_CB_content;
    QStringList work_miss_list;
    work_miss_list.append(QString("'%1'").arg(tr("work_miss_total")));
    work_miss_list.append(QString("'%1'").arg(tr("W_miss(deposition)")));
    work_miss_list.append(QString("'%1'").arg(tr("W_miss(light)")));
    work_miss_list.append(QString("'%1'").arg(tr("W_miss(eatching)")));
    work_miss_list.append(QString("'%1'").arg(tr("W_miss(probe)")));
    QStringList work_miss_tag_list;
    work_miss_tag_list.append(QString("%1").arg(tr("total")));
    work_miss_tag_list.append(QString("%1").arg(tr("deposition")));
    work_miss_tag_list.append(QString("%1").arg(tr("light")));
    work_miss_tag_list.append(QString("%1").arg(tr("eatching")));
    work_miss_tag_list.append(QString("%1").arg(tr("probe")));

    work_miss_data->source_data_list = work_miss_list;
    work_miss_data->tag_data_list = work_miss_tag_list;
    work_miss_data->serarch_btn = ui->work_miss_search_btn;
    work_miss_data->barseriesname = tr("work_miss");
    connect(work_miss_chart,SIGNAL(home_return_signal()),this,SLOT(on_work_miss_search_btn_clicked()));
    connect(work_miss_chart,SIGNAL(complete()),this,SLOT(work_miss_btn_enable()));

    //작업자파손.
    woker_defect_chart = new desh_drilldownchart();
    woker_defect_chart->setMy_mesdb(my_mesdb);
    woker_defect_chart->setAnimationOptions(QChart::SeriesAnimations);
    woker_defect_chart->setHover_label(ui->woker_defect_label);
    woker_defect_chartview =  new Desh_drill_chartview(woker_defect_chart);
    ui->woker_defect_layout->addWidget(woker_defect_chartview);
    ui->woker_defect_start_date->setDate(QDate::currentDate().addMonths(0));
    ui->woker_defect_end_date->setDate(QDate::currentDate());
    woker_defect_data = new chart_maker_data();
    woker_defect_data->main_chart = woker_defect_chart;
    woker_defect_data->start_date = ui->woker_defect_start_date;
    woker_defect_data->end_date = ui->woker_defect_end_date;
    woker_defect_data->type_info = "woker_defect";
    woker_defect_data->Cb_content = ui->woker_defect_CB_content;
    QStringList woker_defect_list;
    woker_defect_list.append(QString("'%1'").arg(tr("woker_defect_miss_total")));
    woker_defect_list.append(QString("'%1'").arg(tr("woker_defect(deposition)")));
    woker_defect_list.append(QString("'%1'").arg(tr("woker_defect(light)")));
    woker_defect_list.append(QString("'%1'").arg(tr("woker_defect(eatching)")));
    woker_defect_list.append(QString("'%1'").arg(tr("woker_defect(probe)")));
    woker_defect_list.append(QString("'%1'").arg(tr("woker_defect(output)")));
    woker_defect_list.append(QString("'%1'").arg(tr("woker_defect(output_check)")));

    QStringList woker_defect_tag_list;
    woker_defect_tag_list.append(QString("%1").arg(tr("total")));
    woker_defect_tag_list.append(QString("%1").arg(tr("deposition")));
    woker_defect_tag_list.append(QString("%1").arg(tr("light")));
    woker_defect_tag_list.append(QString("%1").arg(tr("eatching")));
    woker_defect_tag_list.append(QString("%1").arg(tr("probe")));
    woker_defect_tag_list.append(QString("%1").arg(tr("output")));
    woker_defect_tag_list.append(QString("%1").arg(tr("output_check")));

    woker_defect_data->source_data_list = woker_defect_list;
    woker_defect_data->tag_data_list = woker_defect_tag_list;
    woker_defect_data->serarch_btn = ui->woker_defect_search_btn;
    woker_defect_data->barseriesname = tr("woker_defect");
    connect(woker_defect_chart,SIGNAL(home_return_signal()),this,SLOT(on_woker_defect_search_btn_clicked()));
    connect(woker_defect_chart,SIGNAL(complete()),this,SLOT(woker_defect_btn_enable()));

    //설비파손.
    machine_defect_chart = new desh_drilldownchart();
    machine_defect_chart->setMy_mesdb(my_mesdb);
    machine_defect_chart->setAnimationOptions(QChart::SeriesAnimations);
    machine_defect_chart->setHover_label(ui->machine_defect_label);
    machine_defect_chartview =  new Desh_drill_chartview(machine_defect_chart);
    ui->machine_defect_layout->addWidget(machine_defect_chartview);
    ui->machine_defect_start_date->setDate(QDate::currentDate().addMonths(0));
    ui->machine_defect_end_date->setDate(QDate::currentDate());
    machine_defect_data = new chart_maker_data();
    machine_defect_data->main_chart = machine_defect_chart;
    machine_defect_data->start_date = ui->machine_defect_start_date;
    machine_defect_data->end_date = ui->machine_defect_end_date;
    machine_defect_data->type_info = "machine_defect";
    machine_defect_data->Cb_content = ui->machine_defect_CB_content;
    QStringList machine_defect_list;
    machine_defect_list.append(QString("'%1'").arg(tr("machine_defect_miss_total")));
    machine_defect_list.append(QString("'%1'").arg(tr("machine_defect(deposition)")));
    machine_defect_list.append(QString("'%1'").arg(tr("machine_defect(light)")));
    machine_defect_list.append(QString("'%1'").arg(tr("machine_defect(eatching)")));
    machine_defect_list.append(QString("'%1'").arg(tr("machine_defect(probe)")));


    QStringList machine_defect_tag_list;
    machine_defect_tag_list.append(QString("%1").arg(tr("total")));
    machine_defect_tag_list.append(QString("%1").arg(tr("deposition")));
    machine_defect_tag_list.append(QString("%1").arg(tr("light")));
    machine_defect_tag_list.append(QString("%1").arg(tr("eatching")));
    machine_defect_tag_list.append(QString("%1").arg(tr("probe")));


    machine_defect_data->source_data_list = machine_defect_list;
    machine_defect_data->tag_data_list = machine_defect_tag_list;
    machine_defect_data->serarch_btn = ui->machine_defect_search_btn;
    machine_defect_data->barseriesname = tr("machine_defect");
    connect(machine_defect_chart,SIGNAL(home_return_signal()),this,SLOT(on_machine_defect_search_btn_clicked()));
    connect(machine_defect_chart,SIGNAL(complete()),this,SLOT(machine_defect_btn_enable()));

    //특성불량
    characteristic_faulty_chart = new desh_drilldownchart();
    characteristic_faulty_chart->setMy_mesdb(my_mesdb);
    characteristic_faulty_chart->setAnimationOptions(QChart::SeriesAnimations);
    characteristic_faulty_chart->setHover_label(ui->characteristic_faulty_label);
    characteristic_faulty_chartview =  new Desh_drill_chartview(characteristic_faulty_chart);
    ui->characteristic_faulty_layout->addWidget(characteristic_faulty_chartview);
    ui->characteristic_faulty_start_date->setDate(QDate::currentDate().addMonths(0));
    ui->characteristic_faulty_end_date->setDate(QDate::currentDate());
    characteristic_faulty_data = new chart_maker_data();
    characteristic_faulty_data->main_chart = characteristic_faulty_chart;
    characteristic_faulty_data->start_date = ui->characteristic_faulty_start_date;
    characteristic_faulty_data->end_date = ui->characteristic_faulty_end_date;
    characteristic_faulty_data->type_info = "characteristic_faulty";
    characteristic_faulty_data->Cb_content = ui->characteristic_faulty_CB_content;
    QStringList characteristic_faulty_list;
    characteristic_faulty_list.append(QString("'%1'").arg(tr("characteristic_faulty_total")));
    characteristic_faulty_list.append(QString("'%1'").arg(tr("OS poor")));
    characteristic_faulty_list.append(QString("'%1'").arg(tr("IL poor")));
    characteristic_faulty_list.append(QString("'%1'").arg(tr("BW poor")));
    characteristic_faulty_list.append(QString("'%1'").arg(tr("VSWR poor")));
    characteristic_faulty_list.append(QString("'%1'").arg(tr("peak low")));


    QStringList characteristic_faulty_tag_list;
    characteristic_faulty_tag_list.append(QString("%1").arg(tr("total")));
    characteristic_faulty_tag_list.append(QString("%1").arg(tr("OS poor")));
    characteristic_faulty_tag_list.append(QString("%1").arg(tr("IL poor")));
    characteristic_faulty_tag_list.append(QString("%1").arg(tr("BW poor")));
    characteristic_faulty_tag_list.append(QString("%1").arg(tr("VSWR poor")));
    characteristic_faulty_tag_list.append(QString("%1").arg(tr("peak low")));


    characteristic_faulty_data->source_data_list = characteristic_faulty_list;
    characteristic_faulty_data->tag_data_list = characteristic_faulty_tag_list;
    characteristic_faulty_data->serarch_btn = ui->characteristic_faulty_search_btn;
    characteristic_faulty_data->barseriesname = tr("characteristic_faulty");
    connect(characteristic_faulty_chart,SIGNAL(home_return_signal()),this,SLOT(on_characteristic_faulty_search_btn_clicked()));
    connect(characteristic_faulty_chart,SIGNAL(complete()),this,SLOT(characteristic_faulty_btn_enable()));



    //공정 불량.
    process_faulty_chart = new desh_drilldownchart();
    process_faulty_chart->setMy_mesdb(my_mesdb);
    process_faulty_chart->setAnimationOptions(QChart::SeriesAnimations);
    process_faulty_chart->setHover_label(ui->process_faulty_label);
    process_faulty_chartview =  new Desh_drill_chartview(process_faulty_chart);
    ui->process_faulty_layout->addWidget(process_faulty_chartview);
    ui->process_faulty_start_date->setDate(QDate::currentDate().addMonths(0));
    ui->process_faulty_end_date->setDate(QDate::currentDate());
    process_faulty_data = new chart_maker_data();
    process_faulty_data->main_chart = process_faulty_chart;
    process_faulty_data->start_date = ui->process_faulty_start_date;
    process_faulty_data->end_date = ui->process_faulty_end_date;
    process_faulty_data->type_info = "process_faulty";
    process_faulty_data->Cb_content = ui->process_faulty_CB_content;
    QStringList process_faulty_list;
    process_faulty_list.append(QString("'%1'").arg(tr("process_faulty_total")));
    process_faulty_list.append(QString("'%1'").arg(tr("Metal CD small")));
    process_faulty_list.append(QString("'%1'").arg(tr("Metal CD big")));
    process_faulty_list.append(QString("'%1'").arg(tr("PR CD small")));
    process_faulty_list.append(QString("'%1'").arg(tr("PR CD big")));
    process_faulty_list.append(QString("'%1'").arg(tr("bridge shape abnormality")));
    process_faulty_list.append(QString("'%1'").arg(tr("bridge thickness low")));
    process_faulty_list.append(QString("'%1'").arg(tr("corrosion")));
    process_faulty_list.append(QString("'%1'").arg(tr("PR feeling")));
    process_faulty_list.append(QString("'%1'").arg(tr("PAD desquamation")));
    process_faulty_list.append(QString("'%1'").arg(tr("embossing faulty")));
    process_faulty_list.append(QString("'%1'").arg(tr("bridge diffusion faulty")));
    process_faulty_list.append(QString("'%1'").arg(tr("diffusion faulty")));

    QStringList process_faulty_tag_list;
    process_faulty_tag_list.append(QString("%1").arg(tr("total")));
    process_faulty_tag_list.append(QString("%1").arg(tr("Metal CD small")));
    process_faulty_tag_list.append(QString("%1").arg(tr("Metal CD big")));
    process_faulty_tag_list.append(QString("%1").arg(tr("PR CD small")));
    process_faulty_tag_list.append(QString("%1").arg(tr("PR CD big")));
    process_faulty_tag_list.append(QString("%1").arg(tr("bridge shape abnormality")));
    process_faulty_tag_list.append(QString("%1").arg(tr("bridge thickness low")));
    process_faulty_tag_list.append(QString("%1").arg(tr("corrosion")));
    process_faulty_tag_list.append(QString("%1").arg(tr("PR feeling")));
    process_faulty_tag_list.append(QString("%1").arg(tr("PAD desquamation")));
    process_faulty_tag_list.append(QString("%1").arg(tr("embossing faulty")));
    process_faulty_tag_list.append(QString("%1").arg(tr("bridge diffusion faulty")));
    process_faulty_tag_list.append(QString("%1").arg(tr("diffusion faulty")));

    process_faulty_data->source_data_list = process_faulty_list;
    process_faulty_data->tag_data_list = process_faulty_tag_list;
    process_faulty_data->serarch_btn = ui->process_faulty_search_btn;
    process_faulty_data->barseriesname = tr("process_faulty");
    connect(process_faulty_chart,SIGNAL(home_return_signal()),this,SLOT(on_process_faulty_search_btn_clicked()));
    connect(process_faulty_chart,SIGNAL(complete()),this,SLOT(process_faulty_btn_enable()));

    //설비 불량
    machine_faulty_chart = new desh_drilldownchart();
    machine_faulty_chart->setMy_mesdb(my_mesdb);
    machine_faulty_chart->setAnimationOptions(QChart::SeriesAnimations);
    machine_faulty_chart->setHover_label(ui->machine_faulty_label);
    machine_faulty_chartview =  new Desh_drill_chartview(machine_faulty_chart);
    ui->machine_faulty_layout->addWidget(machine_faulty_chartview);
    ui->machine_faulty_start_date->setDate(QDate::currentDate().addMonths(0));
    ui->machine_faulty_end_date->setDate(QDate::currentDate());
    machine_faulty_data = new chart_maker_data();
    machine_faulty_data->main_chart = machine_faulty_chart;
    machine_faulty_data->start_date = ui->machine_faulty_start_date;
    machine_faulty_data->end_date = ui->machine_faulty_end_date;
    machine_faulty_data->type_info = "machine_faulty";
    machine_faulty_data->Cb_content = ui->machine_faulty_CB_content;
    QStringList machine_faulty_list;
    machine_faulty_list.append(QString("'%1'").arg(tr("machine_faulty_total")));
    machine_faulty_list.append(QString("'%1'").arg(tr("PAD deposition error")));
    machine_faulty_list.append(QString("'%1'").arg(tr("PR don't have")));
    machine_faulty_list.append(QString("'%1'").arg(tr("PR remain")));
    machine_faulty_list.append(QString("'%1'").arg(tr("PR spread")));
    machine_faulty_list.append(QString("'%1'").arg(tr("film state error")));
    machine_faulty_list.append(QString("'%1'").arg(tr("EPD error")));
    machine_faulty_list.append(QString("'%1'").arg(tr("develop faulty")));
    machine_faulty_list.append(QString("'%1'").arg(tr("light faulty")));
    machine_faulty_list.append(QString("'%1'").arg(tr("uneatching")));
    machine_faulty_list.append(QString("'%1'").arg(tr("align twist")));
    machine_faulty_list.append(QString("'%1'").arg(tr("align can't")));
    machine_faulty_list.append(QString("'%1'").arg(tr("coting faulty")));
    machine_faulty_list.append(QString("'%1'").arg(tr("pattern don't have")));

    QStringList machine_faulty_tag_list;
    machine_faulty_tag_list.append(QString("%1").arg(tr("total")));
    machine_faulty_tag_list.append(QString("%1").arg(tr("PAD deposition error")));
    machine_faulty_tag_list.append(QString("%1").arg(tr("PR don't have")));
    machine_faulty_tag_list.append(QString("%1").arg(tr("PR remain")));
    machine_faulty_tag_list.append(QString("%1").arg(tr("PR spread")));
    machine_faulty_tag_list.append(QString("%1").arg(tr("film state error")));
    machine_faulty_tag_list.append(QString("%1").arg(tr("EPD error")));
    machine_faulty_tag_list.append(QString("%1").arg(tr("develop faulty")));
    machine_faulty_tag_list.append(QString("%1").arg(tr("light faulty")));
    machine_faulty_tag_list.append(QString("%1").arg(tr("uneatching")));
    machine_faulty_tag_list.append(QString("%1").arg(tr("align twist")));
    machine_faulty_tag_list.append(QString("%1").arg(tr("align can't")));
    machine_faulty_tag_list.append(QString("%1").arg(tr("coting faulty")));
    machine_faulty_tag_list.append(QString("%1").arg(tr("pattern don't have")));

    machine_faulty_data->source_data_list = machine_faulty_list;
    machine_faulty_data->tag_data_list = machine_faulty_tag_list;
    machine_faulty_data->serarch_btn = ui->machine_faulty_search_btn;
    machine_faulty_data->barseriesname = tr("machine_faulty");
    connect(machine_faulty_chart,SIGNAL(home_return_signal()),this,SLOT(on_machine_faulty_search_btn_clicked()));
    connect(machine_faulty_chart,SIGNAL(complete()),this,SLOT(machine_faulty_btn_enable()));




    //수율.
    yield_chart = new desh_drilldownchart();
    yield_chart->setMy_mesdb(my_mesdb);
    yield_chart->setAnimationOptions(QChart::SeriesAnimations);
    yield_chart->setHover_label(ui->yield_label);
    ui->yield_label->setVisible(false);
    yield_chartview =  new Desh_drill_chartview(yield_chart);
    ui->yield_layout->addWidget(yield_chartview);
    ui->yield_start_date->setDate(QDate::currentDate().addMonths(0));
    ui->yield_end_date->setDate(QDate::currentDate());
    yield_data = new chart_maker_data();
    yield_data->main_chart = yield_chart;
    yield_data->start_date = ui->yield_start_date;
    yield_data->end_date = ui->yield_end_date;
    yield_data->type_info = "yield";
    QStringList yield_list;
    yield_list.append(QString("'%1'").arg("yield"));

    QStringList yield_tag_list;
    yield_tag_list.append(QString("%1").arg("yield"));


    yield_data->source_data_list = yield_list;
    yield_data->tag_data_list = yield_tag_list;
    yield_data->serarch_btn = ui->yield_search_btn;
    yield_data->barseriesname = tr("yield");
    connect(yield_chart,SIGNAL(home_return_signal()),this,SLOT(on_yield_search_btn_clicked()));
    connect(yield_chart,SIGNAL(complete()),this,SLOT(yield_btn_enable()));

    on_yield_search_btn_clicked();
    on_work_miss_search_btn_clicked();
    on_woker_defect_search_btn_clicked();
    on_frequency_miss_search_btn_clicked();
    on_paticle_miss_search_btn_clicked();
    on_machine_destory_search_btn_clicked();
    on_machine_defect_search_btn_clicked();
    on_process_faulty_search_btn_clicked();
    on_characteristic_faulty_search_btn_clicked();
    on_machine_faulty_search_btn_clicked();

    csp_daily_widget = 0;

}


void deshboardmain::chart_maker_ver1(chart_maker_data *source_data)
{
    source_data->serarch_btn->setEnabled(false);
    source_data->main_chart->removeAllSeries();
    QDate start_date = source_data->start_date->date();
    QDate end_date = source_data->end_date->date();
    QVector<QDate> month_list;
    int i=0;
    while(true){
        if((start_date.addMonths(i).year()==end_date.year())
                &&start_date.addMonths(i).month()==end_date.month() ){
            month_list.append(start_date.addMonths(i));
            break;
        }else {
            month_list.append(start_date.addMonths(i));
            i++;
        }
        if(i>300){
            qDebug()<<"error";
            return;
        }
    }
    QBarSeries *barserise = new QBarSeries();
    QLineSeries *spec_lineserise = new QLineSeries();
    QVector<QBarSet *> barset_list;

    QStringList month_list_str1;
    QList<QVariant> last_date_list1;
    QMap<QString,chart_maker_data2 *> source_zone_find_map;

    QSqlQuery query(my_mesdb);
    query.exec("select * from V_FAB_REPORT_DAILY where MATERIAL_GROUP IN ('CSP') group by report_time desc LIMIT 1");
    QDate last_save_time = QDate::currentDate().addDays(-1);
    if(query.next()){
        last_save_time = query.value("report_time").toDate();
    }

    for(int i=0;i<month_list.count();i++){
        month_list_str1.append(month_list.at(i).toString("MM ")+tr("month"));
        QDate query_date = month_list.at(i);
        QDate last_query_date;
        if(last_save_time.year() == query_date.year()
                && last_save_time.month() == query_date.month()){
            last_query_date = QDate(last_save_time.year(),last_save_time.month(),last_save_time.day());
        }else {
            last_query_date = QDate(query_date.year(),query_date.month(),query_date.daysInMonth());
        }
        last_date_list1.append(last_query_date);
        double sum_result = 0;

        for(int j=1;j<source_data->source_data_list.count();j++){
            query.exec(QString("select SUM(result) as reslut_sum  from V_FAB_REPORT_DAILY "
                               "where `report_time` = '%1' AND `type` IN (%2) AND `date_type` IN ('accumulate') AND MATERIAL_GROUP IN ('CSP') AND OPERATION is NULL")
                       .arg(last_query_date.toString("yyyy-MM-dd")).arg(source_data->source_data_list.at(j)));
            if(query.next()){
                    query.value("reslut_sum").toReal();
                    sum_result += query.value("reslut_sum").toReal();
                    if(query.value("reslut_sum").toReal() != 0){
                        int temp_count = 1;
                        if(source_zone_find_map.contains(source_data->source_data_list.at(j))){
                             temp_count =source_zone_find_map.value(source_data->source_data_list.at(j))->count;
                             temp_count++;
                        }
                        chart_maker_data2 *data = new chart_maker_data2(source_data->source_data_list.at(j),temp_count,source_data->tag_data_list.at(j));
                        source_zone_find_map.insert(source_data->source_data_list.at(j),data);
                    }

            }
        }
    }
    QStringList non_zero_source_datalist;
    QStringList non_zero_tag_datalist;
    QStringList keylist = source_zone_find_map.keys();
    for(int i=0;i<keylist.count();i++){
        if(source_zone_find_map.value(keylist.at(i))->count != 0){
            non_zero_source_datalist.append(source_zone_find_map.value(keylist.at(i))->source);
            non_zero_tag_datalist.append(source_zone_find_map.value(keylist.at(i))->tag);
        }
    }
    non_zero_source_datalist.insert(0,source_data->source_data_list.at(0));
    non_zero_tag_datalist.insert(0,source_data->tag_data_list.at(0));


    for(int i=0;i<non_zero_source_datalist.count();i++){
        QBarSet *temp_bar = new QBarSet(non_zero_source_datalist.at(i));
        barset_list.append(temp_bar);
    }

    query.exec(QString("select * from V_FAB_REPORT_SPEC where SPEC_NAME = '%1' AND MATERIAL_GROUP IN ('CSP') "
                       "order by INPUT_TIME desc LIMIT 1").arg(source_data->type_info));

    double spec_value =0;
    if(query.next()){
        spec_value = query.value("SPEC_VALUE").toDouble();
    }

    QStringList month_list_str2;
    QList<QVariant> last_date_list2;
    for(int i=0;i<month_list.count();i++){
        month_list_str2.append(month_list.at(i).toString("MM ")+tr("month"));

        QDate query_date = month_list.at(i);
        QDate last_query_date;
        if(last_save_time.year() == query_date.year()
                && last_save_time.month() == query_date.month()){
            last_query_date = QDate(last_save_time.year(),last_save_time.month(),last_save_time.day());
        }else {
            last_query_date = QDate(query_date.year(),query_date.month(),query_date.daysInMonth());
        }
        last_date_list2.append(last_query_date);
        double sum_result = 0;
        QFont font;
        font.setPointSize(8);
        font.setFamily("Gulim");

        for(int j=1;j<non_zero_source_datalist.count();j++){
            query.exec(QString("select SUM(result) as reslut_sum  from V_FAB_REPORT_DAILY "
                               "where `report_time` = '%1' AND `type` IN (%2) AND `date_type` IN ('accumulate') AND MATERIAL_GROUP IN ('CSP') AND OPERATION is NULL")
                       .arg(last_query_date.toString("yyyy-MM-dd")).arg(non_zero_source_datalist.at(j)));
            if(query.next()){
                    barset_list.at(j)->append(query.value("reslut_sum").toReal());
                    barset_list.at(j)->setProperty("soruce",non_zero_source_datalist.at(j));
                    barset_list.at(j)->setProperty("tag",non_zero_tag_datalist.at(j));
                    barset_list.at(j)->setLabelColor("black");
                    barset_list.at(j)->setLabelFont(font);
                    barset_list.at(j)->setProperty("last_date",last_query_date);
                    barset_list.at(j)->setProperty("stack_type",1);
                    barset_list.at(j)->setProperty("cb_content",(qint64)source_data->Cb_content);
                    barset_list.at(j)->setProperty("last_date_list",last_date_list2);
                    barset_list.at(j)->setLabel(non_zero_tag_datalist.at(j));
                    sum_result += query.value("reslut_sum").toReal();
            }
        }
        barset_list.at(0)->append(sum_result);
        barset_list.at(0)->setProperty("tag",QString("%1").arg(tr("total")));
        barset_list.at(0)->setLabelColor("black");
        barset_list.at(0)->setLabelFont(font);
        barset_list.at(0)->setProperty("last_date",last_query_date);
        barset_list.at(0)->setProperty("cb_content",(qint64)source_data->Cb_content);
        barset_list.at(0)->setProperty("last_date_list",last_date_list2);
        barset_list.at(0)->setLabel(non_zero_tag_datalist.at(0));
        barset_list.at(0)->setProperty("stack_type",1);
    }

    barserise->setProperty("order","asc");
    barserise->append(barset_list.at(0));


    for(int i=1;i<barset_list.count();i++){
        barserise->append(barset_list.at(i));
        spec_lineserise->append(i,spec_value);
    }
    if(spec_lineserise->points().count() == 1){
        spec_lineserise->append(1,spec_value);
        spec_lineserise->append(2,spec_value);
    }
    spec_lineserise->setColor(QColor("red"));

    barserise->setName(source_data->barseriesname);
    source_data->main_chart->setTitle(barserise->name());
    barserise->setLabelsVisible(true);
    barserise->setLabelsFormat("@tag @real2f ");
    barserise->setLabelsPosition(QAbstractBarSeries::LabelsInsideBase);
    barserise->setLabelsAngle(90);

    barserise->setBarWidth(1);
    QString total_search_txt;
    for(int i=1;i<non_zero_source_datalist.count();i++){
        if(i==(non_zero_source_datalist.count()-1)){
            total_search_txt.append(QString("%1").arg(non_zero_source_datalist.at(i)));
        }else {
            total_search_txt.append(QString("%1,").arg(non_zero_source_datalist.at(i)));
        }
    }
    QPen spec_pen(QColor("red"));
    spec_pen.setWidthF(3);
    spec_lineserise->setPen(spec_pen);
    spec_lineserise->setProperty("no_legend","yes");

    source_data->main_chart->total_sum_search_txt = total_search_txt;
    source_data->main_chart->setCurrentSeries(barserise);
    source_data->main_chart->setCurrentsepcSeries(spec_lineserise);

    source_data->main_chart->addSeries(barserise);
    source_data->main_chart->addSeries(spec_lineserise);

    source_data->main_chart->setLayoutDirection(Qt::LeftToRight);

    QBarCategoryAxis *axisX = new QBarCategoryAxis();
    axisX->append(month_list_str2);

    source_data->main_chart->legend()->setAlignment(Qt::AlignRight);
    source_data->main_chart->createDefaultAxes();
    source_data->main_chart->setAxisX(axisX,barserise);
    source_data->main_chart->axisX(spec_lineserise)->setVisible(false);



    connect(barserise,SIGNAL(clicked(int,QBarSet*)),source_data->main_chart,SLOT(handleClicked(int,QBarSet*)));
    connect(barserise,SIGNAL(hovered(bool,int,QBarSet*)),source_data->main_chart,SLOT(handleHover(bool,int,QBarSet*)));

}

void deshboardmain::chart_maker_ver2(chart_maker_data *source_data)
{
    source_data->serarch_btn->setEnabled(false);
    source_data->main_chart->removeAllSeries();
    QDate start_date = source_data->start_date->date();
    QDate end_date = source_data->end_date->date();
    QVector<QDate> month_list;
    int i=0;
    while(true){
        if((start_date.addMonths(i).year()==end_date.year())
                &&start_date.addMonths(i).month()==end_date.month() ){
            month_list.append(start_date.addMonths(i));
            break;
        }else {
            month_list.append(start_date.addMonths(i));
            i++;
        }
        if(i>300){
            qDebug()<<"error";
            return;
        }
    }
    QBarSeries *barserise = new QBarSeries();
    QLineSeries *spec_lineserise = new QLineSeries();
    QVector<QBarSet *> barset_list;


    QSqlQuery query(my_mesdb);
    query.exec("select * from V_FAB_REPORT_DAILY where MATERIAL_GROUP IN ('CSP') AND OPERATION is NULL group by report_time desc LIMIT 1");
    QDate last_save_time = QDate::currentDate().addDays(-1);
    if(query.next()){
        last_save_time = query.value("report_time").toDate();
    }

    query.exec(QString("select * from V_FAB_REPORT_SPEC where SPEC_NAME = '%1' AND MATERIAL_GROUP IN ('CSP') "
                       "order by INPUT_TIME desc LIMIT 1").arg(source_data->type_info));
    double spec_value =0;
    if(query.next()){
        spec_value = query.value("SPEC_VALUE").toDouble();
    }
    QStringList catagory_list_str;
    QList<QVariant> last_date_list;


    for(int i=0;i<month_list.count();i++){
        QBarSet *temp_bar = new QBarSet(month_list.at(i).toString("MM ")+tr("month"));
        barset_list.append(temp_bar);
        catagory_list_str.append(month_list.at(i).toString("MM ")+tr("month"));


        QDate query_date = month_list.at(i);
        QDate last_query_date;
        if(last_save_time.year() == query_date.year()
                && last_save_time.month() == query_date.month()){
            last_query_date = QDate(last_save_time.year(),last_save_time.month(),last_save_time.day());
        }else {
            last_query_date = QDate(query_date.year(),query_date.month(),query_date.daysInMonth());
        }
        last_date_list.append(last_query_date);
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setFamily("Gulim");

        for(int j=0;j<source_data->source_data_list.count();j++){
            query.exec(QString("select SUM(result) as reslut_sum  from V_FAB_REPORT_DAILY "
                               "where `report_time` = '%1' AND `type` IN (%2) AND `date_type` IN ('accumulate') AND MATERIAL_GROUP IN ('CSP') AND OPERATION is NULL ")
                       .arg(last_query_date.toString("yyyy-MM-dd")).arg(source_data->source_data_list.at(j)));
            if(query.next()){
                    temp_bar->append(query.value("reslut_sum").toReal());
                    temp_bar->setProperty("soruce",source_data->source_data_list.at(j));
                    temp_bar->setProperty("tag",QString("%1 %2").arg(last_query_date.month()).arg(tr("month")));
                    temp_bar->setLabelColor("black");
                    temp_bar->setLabelFont(font);
                    temp_bar->setProperty("last_date",last_query_date);
                    temp_bar->setProperty("stack_type",1);
                    temp_bar->setProperty("last_date_list",last_date_list);
                    temp_bar->setProperty("index",i);
                    temp_bar->setLabel("");
            }
        }
    }
    QDate last_month_date = last_date_list.last().toDate();
    int last_month_day = last_month_date.day();
    for(int i=1;i<=last_month_day;i++){
        QBarSet *temp_bar = new QBarSet(QString("%1").arg(i));
        QDate tmpe_last_month_date = QDate(last_month_date.year(),last_month_date.month(),i);
        barset_list.append(temp_bar);
        catagory_list_str.append(QString("%1 %2").arg(i).arg(tr("day")));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setFamily("Gulim");
        for(int j=0;j<source_data->source_data_list.count();j++){
            query.exec(QString("select SUM(result) as reslut_sum  from V_FAB_REPORT_DAILY "
                               "where `report_time` = '%1' AND `type` IN (%2) AND `date_type` IN ('Daily') AND MATERIAL_GROUP IN ('CSP') AND OPERATION is NULL ")
                       .arg(tmpe_last_month_date.toString("yyyy-MM-dd")).arg(source_data->source_data_list.at(j)));
            if(query.next()){
                    temp_bar->append(query.value("reslut_sum").toReal());
                    temp_bar->setProperty("soruce",source_data->source_data_list.at(j));
                    temp_bar->setProperty("tag",QString("%1 %2").arg(tmpe_last_month_date.day()).arg(tr("day")));
                    temp_bar->setProperty("date",tmpe_last_month_date);
                    temp_bar->setLabelColor("black");
                    temp_bar->setLabelFont(font);
                    temp_bar->setProperty("stack_type",3);
                    temp_bar->setLabel("");
                    temp_bar->setColor(QColor("#55ffff"));
            }
        }
    }
    barserise->setProperty("order","only_bar");
    for(int i=0;i<barset_list.count();i++){
        barserise->append(barset_list.at(i));
        spec_lineserise->append(i,spec_value);
    }
    if(spec_lineserise->points().count() == 1){
        spec_lineserise->append(1,spec_value);
    }
    QPen spec_pen(QColor("red"));
    spec_pen.setWidthF(2);
    spec_lineserise->setPen(spec_pen);



    barserise->setName(source_data->barseriesname);
    source_data->main_chart->setTitle(barserise->name());
    source_data->main_chart->legend()->hide();
    barserise->setLabelsVisible(true);
    barserise->setLabelsFormat("@real2f");
    barserise->setLabelsPosition(QAbstractBarSeries::LabelsInsideEnd);

    barserise->setBarWidth(1);
    source_data->main_chart->setCurrentSeries(barserise);
    source_data->main_chart->setCurrentsepcSeries(spec_lineserise);

    source_data->main_chart->addSeries(barserise);
    source_data->main_chart->addSeries(spec_lineserise);

    source_data->main_chart->setLayoutDirection(Qt::LeftToRight);

    QBarCategoryAxis *axisX = new QBarCategoryAxis();
    axisX->append(catagory_list_str);

    source_data->main_chart->legend()->setAlignment(Qt::AlignRight);
    source_data->main_chart->createDefaultAxes();
    source_data->main_chart->setAxisX(axisX,barserise);
    source_data->main_chart->axisY()->setRange(50,100);
    source_data->main_chart->axisX(spec_lineserise)->setVisible(false);

    connect(barserise,SIGNAL(clicked(int,QBarSet*)),source_data->main_chart,SLOT(handleClicked(int,QBarSet*)));
}


deshboardmain::~deshboardmain()
{
    delete ui;
}

void deshboardmain::on_work_miss_search_btn_clicked()
{
     chart_maker_ver1(work_miss_data);
}


void deshboardmain::work_miss_btn_enable()
{
    ui->work_miss_search_btn->setEnabled(true);
}


void deshboardmain::on_work_miss_output_clicked()
{
    desh_output_widget *temp_widget  = new desh_output_widget(ui->work_miss_widget,ui->splitter_2,this);
    temp_widget->show();
}

void deshboardmain::woker_defect_btn_enable()
{
    ui->woker_defect_search_btn->setEnabled(true);
}


void deshboardmain::on_woker_defect_search_btn_clicked()
{
    chart_maker_ver1(woker_defect_data);
}


void deshboardmain::on_woker_defect_output_clicked()
{
    desh_output_widget *temp_widget  = new desh_output_widget(ui->woker_defect_widget,ui->splitter_2,this);
    temp_widget->show();
}


void deshboardmain::on_woker_defect_zoomreset_clicked()
{
       woker_defect_chart->zoomReset();
}



chart_maker_data::chart_maker_data()
{
    Cb_content = 0;
}


void deshboardmain::on_frequency_miss_search_btn_clicked()
{
    chart_maker_ver1(frequency_miss_data);
}

void deshboardmain::on_frequency_miss_zoomreset_clicked()
{
    frequency_miss_chart->zoomReset();
}

void deshboardmain::on_frequency_miss_output_clicked()
{
    desh_output_widget *temp_widget  = new desh_output_widget(ui->frequency_miss_widget,ui->splitter,this);
    temp_widget->show();
}
void deshboardmain::frequency_miss_btn_enable()
{
    ui->frequency_miss_search_btn->setEnabled(true);
}


void deshboardmain::on_paticle_miss_search_btn_clicked()
{
    chart_maker_ver1(paticle_miss_data);
}

void deshboardmain::on_paticle_miss_zoomreset_clicked()
{
    paticle_miss_chart->zoomReset();
}

void deshboardmain::on_paticle_miss_output_clicked()
{
    desh_output_widget *temp_widget  = new desh_output_widget(ui->paticle_miss_widget,ui->splitter,this);
    temp_widget->show();
}
void deshboardmain::paticle_miss_btn_enable()
{
    ui->paticle_miss_search_btn->setEnabled(true);
}

void deshboardmain::machine_destory_btn_enable()
{
    ui->machine_destory_search_btn->setEnabled(true);
}


void deshboardmain::on_machine_destory_search_btn_clicked()
{
    chart_maker_ver1(machine_destory_data);
}

void deshboardmain::on_machine_destory_zoomreset_clicked()
{
    machine_destory_chart->zoomReset();
}

void deshboardmain::on_machine_destory_output_clicked()
{
    desh_output_widget *temp_widget  = new desh_output_widget(ui->machine_destory_widget,ui->splitter,this);
    temp_widget->show();
}

void deshboardmain::machine_defect_btn_enable()
{
    ui->machine_defect_search_btn->setEnabled(true);
}

void deshboardmain::characteristic_faulty_btn_enable()
{
    ui->characteristic_faulty_search_btn->setEnabled(true);
}


void deshboardmain::process_faulty_btn_enable()
{
    ui->process_faulty_search_btn->setEnabled(true);
}

void deshboardmain::machine_faulty_btn_enable()
{
     ui->machine_faulty_search_btn->setEnabled(true);
}

void deshboardmain::yield_btn_enable()
{
    ui->yield_search_btn->setEnabled(true);
}

void deshboardmain::on_machine_defect_search_btn_clicked()
{
    chart_maker_ver1(machine_defect_data);
}

void deshboardmain::on_machine_defect_zoomreset_clicked()
{
    machine_defect_chart->zoomReset();
}

void deshboardmain::on_machine_defect_output_clicked()
{
    desh_output_widget *temp_widget  = new desh_output_widget(ui->machine_defect_widget,ui->splitter_2,this);
    temp_widget->show();
}



void deshboardmain::on_yield_search_btn_clicked()
{
    chart_maker_ver2(yield_data);
    on_work_miss_search_btn_clicked();
    on_woker_defect_search_btn_clicked();
    on_frequency_miss_search_btn_clicked();
    on_paticle_miss_search_btn_clicked();
    on_machine_destory_search_btn_clicked();
    on_machine_defect_search_btn_clicked();
    on_process_faulty_search_btn_clicked();
    on_characteristic_faulty_search_btn_clicked();
    on_machine_faulty_search_btn_clicked();
}

void deshboardmain::on_yield_zoomreset_clicked()
{
    yield_chart->zoomReset();
}

void deshboardmain::on_yield_output_clicked()
{
    desh_output_widget *temp_widget  = new desh_output_widget(ui->yelid_widget,ui->splitter_3,this);
    temp_widget->show();
}

void deshboardmain::on_yelid_spec_btn_clicked()
{
    Desh_setup_spec_popup popup(my_mesdb,yield_data->type_info);
    if(popup.exec()==QDialog::Accepted){
        on_yield_search_btn_clicked();
    }
}

void deshboardmain::on_frequency_spec_btn_clicked()
{
    Desh_setup_spec_popup popup(my_mesdb,frequency_miss_data->type_info);
    if(popup.exec()==QDialog::Accepted){
        on_frequency_miss_search_btn_clicked();
    }

}

void deshboardmain::on_paticle_miss_spec_btn_clicked()
{
    Desh_setup_spec_popup popup(my_mesdb,paticle_miss_data->type_info);
    if(popup.exec()==QDialog::Accepted){
        on_paticle_miss_search_btn_clicked();
    }
}

void deshboardmain::on_machine_destory_spec_btn_clicked()
{
    Desh_setup_spec_popup popup(my_mesdb,machine_destory_data->type_info);
    if(popup.exec()==QDialog::Accepted){
        on_machine_destory_search_btn_clicked();
    }
}

void deshboardmain::on_work_miss_spec_btn_clicked()
{
    Desh_setup_spec_popup popup(my_mesdb,work_miss_data->type_info);
    if(popup.exec()==QDialog::Accepted){
        on_work_miss_search_btn_clicked();
    }
}

void deshboardmain::on_woker_defect_btn_clicked()
{
    Desh_setup_spec_popup popup(my_mesdb,woker_defect_data->type_info);
    if(popup.exec()==QDialog::Accepted){
        on_woker_defect_search_btn_clicked();
    }
}

void deshboardmain::on_machine_defect_btn_clicked()
{
    Desh_setup_spec_popup popup(my_mesdb,machine_defect_data->type_info);
    if(popup.exec()==QDialog::Accepted){
        on_machine_defect_search_btn_clicked();
    }
}

void deshboardmain::on_process_faulty_search_btn_clicked()
{
      chart_maker_ver1(process_faulty_data);
}

void deshboardmain::on_process_faulty_zoomreset_clicked()
{
    process_faulty_chart->zoomReset();
}

void deshboardmain::on_process_faulty_spec_btn_clicked()
{
    Desh_setup_spec_popup popup(my_mesdb,process_faulty_data->type_info);
    if(popup.exec()==QDialog::Accepted){
        on_process_faulty_search_btn_clicked();
    }
}

void deshboardmain::on_process_faulty_output_clicked()
{
    desh_output_widget *temp_widget  = new desh_output_widget(ui->process_faulty_widget,ui->splitter,this);
    temp_widget->show();
}

void deshboardmain::on_machine_faulty_search_btn_clicked()
{
    chart_maker_ver1(machine_faulty_data);
}

void deshboardmain::on_machine_faulty_zoomreset_clicked()
{
    machine_faulty_chart->zoomReset();
}

void deshboardmain::on_machine_faulty_spec_btn_clicked()
{
    Desh_setup_spec_popup popup(my_mesdb,machine_faulty_data->type_info);
    if(popup.exec()==QDialog::Accepted){
        on_process_faulty_search_btn_clicked();
    }
}

void deshboardmain::on_machine_faulty_output_clicked()
{
    desh_output_widget *temp_widget  = new desh_output_widget(ui->machine_faulty_widget,ui->splitter_4,this);
    temp_widget->show();
}

void deshboardmain::on_characteristic_faulty_search_btn_clicked()
{
    chart_maker_ver1(characteristic_faulty_data);
}

void deshboardmain::on_characteristic_faulty_zoomreset_clicked()
{
    characteristic_faulty_chart->zoomReset();
}

void deshboardmain::on_characteristic_faulty_spec_btn_clicked()
{
    Desh_setup_spec_popup popup(my_mesdb,characteristic_faulty_data->type_info);
    if(popup.exec()==QDialog::Accepted){
        on_characteristic_faulty_search_btn_clicked();
    }
}

void deshboardmain::on_characteristic_faulty_output_clicked()
{
    desh_output_widget *temp_widget  = new desh_output_widget(ui->characteristic_faulty_widget,ui->splitter_2,this);
    temp_widget->show();
}

void deshboardmain::on_tabWidget_currentChanged(int index)
{
    if(index == 1){
        if(csp_daily_widget == 0){
            csp_daily_widget = new desh_csp_daily(my_mesdb);
            ui->CSP_daily_layout->addWidget(csp_daily_widget);
        }
    }
}

chart_maker_data2::chart_maker_data2(QString source, int count, QString tag)
{
    this->source = source;
    this->count = count;
    this->tag = tag;
}


void deshboardmain::on_yield_start_date_dateChanged(const QDate &date)
{
    ui->frequency_miss_start_date->setDate(date);
    ui->paticle_miss_start_date->setDate(date);
    ui->machine_destory_start_date->setDate(date);
    ui->process_faulty_start_date->setDate(date);
    ui->work_miss_start_date->setDate(date);
    ui->characteristic_faulty_start_date->setDate(date);
    ui->woker_defect_start_date->setDate(date);
    ui->machine_defect_start_date->setDate(date);
    ui->machine_faulty_start_date->setDate(date);
}

void deshboardmain::on_yield_end_date_dateChanged(const QDate &date)
{
    ui->frequency_miss_end_date->setDate(date);
    ui->paticle_miss_end_date->setDate(date);
    ui->machine_destory_end_date->setDate(date);
    ui->process_faulty_end_date->setDate(date);
    ui->work_miss_end_date->setDate(date);
    ui->characteristic_faulty_end_date->setDate(date);
    ui->woker_defect_end_date->setDate(date);
    ui->machine_defect_end_date->setDate(date);
    ui->machine_faulty_end_date->setDate(date);
}
