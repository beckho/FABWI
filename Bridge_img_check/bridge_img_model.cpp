#include "bridge_img_model.h"

bridge_img_model::bridge_img_model(int rows, int columns, QObject *parent)
    :QStandardItemModel(rows,columns,parent)
{

}

Qt::ItemFlags bridge_img_model::flags(const QModelIndex &index) const
{
    if(index.column() == 1){
        return QStandardItemModel::flags(index)|Qt::ItemIsUserCheckable;
    }
    return QStandardItemModel::flags(index);
}

QVariant bridge_img_model::data(const QModelIndex &index, int role) const
{
    if(index.column() == 1 && role == Qt::CheckStateRole)
   {
        qDebug()<<index.data().toString();
        if(index.data().toString() ==  "OK"){
            return Qt::Checked;
        }else {
            return Qt::Unchecked;
        }
    }
    return QStandardItemModel::data(index, role);
}

bool bridge_img_model::setData(const QModelIndex &index1, const QVariant &value, int role)
{
    if(index1.column() == 1 && role == Qt::CheckStateRole)
    {
        qDebug()<<value.toString();
        if ( value.toInt() == Qt::Checked ){
            QString file_name= this->index(index1.row(),0).data().toString();
            QFile file(QString("%1/%2").arg(root_path).arg(file_name));
            QStringList filename_list = file_name.split(".");
            file.rename(QString("%1/%2_OK.%3").arg(root_path).arg(filename_list.at(0)).arg(filename_list.at(1)));
            QModelIndex index2 = this->index(index1.row(),0);

            QStandardItemModel::setData(index2,QString("%1_OK.%2").arg(filename_list.at(0)).arg(filename_list.at(1)));
            return QStandardItemModel::setData(index1,"OK");
        }
        else if(value == Qt::Unchecked) {
            QString file_name= this->index(index1.row(),0).data().toString();
            QFile file(QString("%1/%2").arg(root_path).arg(file_name));
            QStringList filename_list = file_name.split(".");
            QString new_file_name = filename_list.at(0);
            new_file_name = new_file_name.replace("_OK","");
            qDebug()<<QString("%1/%2.%3").arg(root_path).arg(new_file_name).arg(filename_list.at(1));
            file.rename(QString("%1/%2.%3").arg(root_path).arg(new_file_name).arg(filename_list.at(1)));
            QModelIndex index2 = this->index(index1.row(),0);

            QStandardItemModel::setData(index2,QString("%1.%2").arg(new_file_name).arg(filename_list.at(1)));
            return QStandardItemModel::setData(index1,"NG");
        }
        emit dataChanged(index1, index1 );

    }
    return QStandardItemModel::setData(index1, value, role);
}
