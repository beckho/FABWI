#include "bridge_img_check_main.h"
#include "ui_bridge_img_check_main.h"

Bridge_img_check_main::Bridge_img_check_main(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Bridge_img_check_main)
{
    ui->setupUi(this);
    model = new bridge_img_model(0,2,this);
    img_view = new ImageViewer();
    ui->img_view_grid->addWidget(img_view);
    ui->splitter->setStretchFactor(1,QSizePolicy::Minimum);
    QSettings setting("bridge_img.ini",QSettings::IniFormat);
    setting.beginGroup("settings");
    file_path = setting.value("bridge_path").toString();
    setting.endGroup();
    ui->LE_file_path->setText(file_path);
    root_dir = QDir(file_path);
    model->root_path = ui->LE_file_path->text();
    model->setHorizontalHeaderItem(0,new QStandardItem("file_name"));
    model->setHorizontalHeaderItem(1,new QStandardItem("pass"));

    ui->img_table->setModel(model);

    fileinfos = root_dir.entryInfoList(QDir::NoDotAndDotDot|QDir::AllEntries);
    for(int i=0;i<fileinfos.count();i++){
        qDebug()<<fileinfos.at(i).fileName();
        QString filename = fileinfos.at(i).fileName();
        if(filename.indexOf("_")>=0){

        }else {
            QList<QStandardItem *> items  ;
            items.append(new QStandardItem(fileinfos.at(i).fileName()));
            items.append(new QStandardItem("NO"));
            model->insertRow(model->rowCount(),items);
        }
    }
    connect(ui->img_table->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),this,SLOT(selection_chenge(QModelIndex,QModelIndex)));


}

Bridge_img_check_main::~Bridge_img_check_main()
{
    delete ui;
}

void Bridge_img_check_main::on_toolButton_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                "C:\\",
                                                QFileDialog::ShowDirsOnly
                                                | QFileDialog::DontResolveSymlinks);
    ui->LE_file_path->setText(dir);

    QSettings setting("bridge_img.ini",QSettings::IniFormat);
    setting.beginGroup("settings");
    setting.setValue("bridge_path",dir);
    setting.endGroup();

    model->root_path = ui->LE_file_path->text();
    model->clear();
    fileinfos = root_dir.entryInfoList(QDir::NoDotAndDotDot|QDir::AllEntries);
    for(int i=0;i<fileinfos.count();i++){
        qDebug()<<fileinfos.at(i).fileName();
        QList<QStandardItem *> items  ;
        items.append(new QStandardItem(fileinfos.at(i).fileName()));
        items.append(new QStandardItem("NO"));
        model->insertRow(i,items);
    }

}

void Bridge_img_check_main::on_img_table_clicked(const QModelIndex &index)
{
//    QString Filename = model->index(index.row(),0).data().toString();

//    QString filepath = QString("%1/%2").arg(ui->LE_file_path->text()).arg(Filename);
//    img_view->loadFile(filepath);
}

void Bridge_img_check_main::selection_chenge(QModelIndex index1, QModelIndex index2)
{
    QString Filename = model->index(index1.row(),0).data().toString();

    QString filepath = QString("%1/%2").arg(ui->LE_file_path->text()).arg(Filename);
    img_view->loadFile(filepath);
}
