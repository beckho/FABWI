#ifndef BRIDGE_IMG_CHECK_MAIN_H
#define BRIDGE_IMG_CHECK_MAIN_H

#include <QWidget>
#include <Bridge_img_check/imageviewer.h>
#include <QFileDialog>
#include <QSettings>
#include <QDebug>
#include <Bridge_img_check/bridge_img_model.h>
namespace Ui {
class Bridge_img_check_main;
}

class Bridge_img_check_main : public QWidget
{
    Q_OBJECT

public:
    explicit Bridge_img_check_main(QWidget *parent = 0);
    ImageViewer *img_view;
    QString file_path;
    QDir root_dir;
    QFileInfoList fileinfos;
    bridge_img_model *model;
    QList<QStandardItem *> column_list;
    ~Bridge_img_check_main();

private slots:
    void on_toolButton_clicked();

    void on_img_table_clicked(const QModelIndex &index);

    void selection_chenge(QModelIndex index1,QModelIndex index2);

private:
    Ui::Bridge_img_check_main *ui;
};

#endif // BRIDGE_IMG_CHECK_MAIN_H
