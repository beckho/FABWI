#ifndef BRIDGE_IMG_MODEL_H
#define BRIDGE_IMG_MODEL_H

#include <QObject>
#include <QWidget>
#include <QStandardItemModel>
#include <QDebug>
#include <QFile>
class bridge_img_model : public QStandardItemModel
{
    Q_OBJECT
public:
    QString root_path;
    bridge_img_model(int rows, int columns, QObject *parent = Q_NULLPTR);
    Qt::ItemFlags flags(const QModelIndex & index) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const ;
    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);


};

#endif // BRIDGE_IMG_MODEL_H
