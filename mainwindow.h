#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <idix_gui.h>
#include <e2r_every_report.h>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrlQuery>
#include <server_setting.h>
#include <logsrc/etching_analysor.h>
#include <logsrc/nikon_log_err_main.h>
#include <logsrc/hanile_log_main.h>
#include <temp_humi_widget.h>
#include <logsrc/asml_err_log_main.h>
#include <worksupport/worst_search_excel_main.h>
#include <logsrc/nikon_time_log.h>
#include <operatingratio/operating_ratio_main.h>
#include <bridge_chart/bridge_chart_widget.h>
#include <NIS/nismainwindow.h>
#include <EDU/edumain.h>
#include <Thin_film_manager/thin_film_mainwindows.h>
#include <Thin_film_manager/thin_monitering_tool.h>
#include <Cost_Reduction/cost_reduction_main.h>
#include <logsrc/act_log_analysor.h>
#include <QtWidgets/QApplication>

#include <deshboard/deshboardmain.h>
#include <Cdsemmanager/cdsemmain.h>
#include <logsrc/shincron_log.h>
#include <production_app/production_main.h>
#include <EatchingSPC/escp_main.h>
#include <Bridge_img_check/bridge_img_check_main.h>
#include <SOMCO/somco_main.h>
#include <suggestion_system/suggestion_main.h>
#include "mainwindow2.h"
//#define TEST
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    QString testhagl;

    ~MainWindow();

private slots:
    void on_action_everyday_report_triggered();

    void on_actionEISystem_triggered();

    void on_actionMessage_triggered();

    void on_action_server_setting_triggered();

    void on_infrom_entet_btn_clicked();

//    void on_testbtn_clicked();

    void mail_contionfail(QByteArray msg);
    void mail_fail(int a,int b,QByteArray msg);

    void on_action_etching_analysor_triggered();

    void on_action_nikon_err_log_triggered();

    void on_action_hanile_triggered();

    void on_action_EVATEC_triggered();

    void on_action_temp_humi_triggered();

    void on_action_asml_err_triggered();

    void on_action_worst_serarch_triggered();

    void on_OI_startup_clicked();

    void on_action_nikon_err_time_triggered();

    void on_actionMES_bridge_chart_triggered();

    void on_NIS_clicked();


    void on_Thin_film_manager_triggered();

    void on_thin_monitering_tool_triggered();

    void on_Cost_reduction_triggered();

    void on_Cost_reduce_btn_clicked();

    void on_multi_depostion_work_sheet_btn_clicked();

    void on_actionACT_triggered();

    void on_actionTEST_triggered();

    void on_actionDeshboard_triggered();

    void on_Dashboard_btn_clicked();

    void on_actionCD_SEM_triggered();

    void on_actionShincron_triggered();

    void on_production_moniter_action_triggered();

    void on_production_app_btn_clicked();

    void on_actionE_SPC_triggered();

    void on_change_histroy_manager_triggered();

    void on_action_bridge_img_check_triggered();

    void on_actionSAMCO_triggered();

    void on_suggestion_system_btn_clicked();

    void on_action_summary_triggered();

    void on_operation_ratio_btn_clicked();

    void on_Edu_action_triggered();

    void on_online_fdc_statue_btn_clicked();

    void on_pushButton_clicked();

    void on_Dashboard_btn2_clicked();

    void on_OI_startup2_clicked();

    void on_actionVersion2_UI_triggered();

    void on_action_depostion_cpcpk_triggered();

private:
    void closeEvent(QCloseEvent *event);
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
