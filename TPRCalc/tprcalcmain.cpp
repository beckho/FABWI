#include "tprcalcmain.h"
#include "ui_tprcalcmain.h"

TPRCalcMain::TPRCalcMain(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TPRCalcMain)
{
    ui->setupUi(this);
}

TPRCalcMain::~TPRCalcMain()
{
    delete ui;
}

void TPRCalcMain::on_Clacbtn_clicked()
{
    double M_TRP_HRPM = ui->M_TRP_HRPM->text().toDouble();
    double M_TRP = ui->M_TRP->text().toDouble();
    double T_TRP = ui->T_TRP->text().toDouble();
    double result = ((M_TRP * sqrt(M_TRP_HRPM))/T_TRP)*((M_TRP * sqrt(M_TRP_HRPM))/T_TRP);
    ui->ResultValue->setText(QString("%1").arg(int(result)));
}
