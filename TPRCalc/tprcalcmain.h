#ifndef TPRCALCMAIN_H
#define TPRCALCMAIN_H

#include <QWidget>

namespace Ui {
class TPRCalcMain;
}

class TPRCalcMain : public QWidget
{
    Q_OBJECT

public:
    explicit TPRCalcMain(QWidget *parent = 0);
    ~TPRCalcMain();

private slots:
    void on_Clacbtn_clicked();

private:
    Ui::TPRCalcMain *ui;
};

#endif // TPRCALCMAIN_H
