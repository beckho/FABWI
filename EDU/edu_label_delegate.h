#ifndef EDU_LABEL_DELEGATE_H
#define EDU_LABEL_DELEGATE_H

#include <QObject>
#include <QWidget>
#include <QItemDelegate>
#include <QLabel>


class EDU_label_delegate : public QItemDelegate
{
     Q_OBJECT
public:
    EDU_label_delegate(QObject *parent = 0);
//    void paint( QPainter *painter,
//                        const QStyleOptionViewItem &option,
//                        const QModelIndex &index ) const;
    QWidget *createEditor( QWidget *parent,
                        const QStyleOptionViewItem &option,
                        const QModelIndex &index ) const;
    void updateEditorGeometry( QWidget *editor,
                        const QStyleOptionViewItem &option,
                        const QModelIndex &index ) const;
    void setModelData( QWidget *editor,
                        QAbstractItemModel *model,
                        const QModelIndex &index ) const;
    void setEditorData( QWidget *editor,
                        const QModelIndex &index ) const;
    mutable QString textdate;
    mutable QLabel *thelabel;

};

#endif // EDU_LABEL_DELEGATE_H
