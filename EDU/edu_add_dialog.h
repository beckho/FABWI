#ifndef EDU_ADD_DIALOG_H
#define EDU_ADD_DIALOG_H

#include <QDialog>
#include <QDateTime>
#include <QSettings>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <global_define.h>
#include <EDU/edu_detail_paln_dialog.h>
#include <QSqlQueryModel>
#include <QMessageBox>
#include <QCloseEvent>
#include <QDebug>
#include <QEvent>
#include <EDU/edu_user_from.h>
#include <EDU/edu_checkboxdelegate.h>
#include <EDU/edu_ragister_item.h>
#include <EDU/edu_pw_dialog.h>
namespace Ui {
class EDU_add_dialog;
}

class EDU_add_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit EDU_add_dialog(QWidget *parent = 0);
    explicit EDU_add_dialog(QSqlDatabase &my_mesdb, int edu_number, bool newflag, QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    bool newflag;
    bool modifymode;
    int edu_number;

    QSqlQueryModel *detail_tablemodel;
    QSqlTableModel *ragister_table_model;


    void ragister_table_search();


    ~EDU_add_dialog();

private slots:
    void on_buttonBox_accepted();

    void on_detail_plan_addbtn_clicked();

    void on_ragister_btn_clicked();

    void on_detail_plan_delbtn_clicked();

    void on_remove_btn_clicked();

    void on_modify_btn_clicked();

private:
    Ui::EDU_add_dialog *ui;
};

#endif // EDU_ADD_DIALOG_H
