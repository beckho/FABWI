#ifndef EDU_CHECKBOXDELEGATE_H
#define EDU_CHECKBOXDELEGATE_H

#include <QObject>
#include <QWidget>
#include <QItemDelegate>
#include <QPainter>
#include <QCheckBox>
#include <QStyleOptionViewItem>
#include <QLayout>
class EDU_CheckBoxDelegate : public QItemDelegate
{
     Q_OBJECT
public:
    void paint( QPainter *painter,
                        const QStyleOptionViewItem &option,
                        const QModelIndex &index ) const;
    EDU_CheckBoxDelegate(QObject *parent = 0);


    QWidget *createEditor( QWidget *parent,
                        const QStyleOptionViewItem &option,
                        const QModelIndex &index ) const;

    void setEditorData( QWidget *editor,
                        const QModelIndex &index ) const;

    void setModelData( QWidget *editor,
                        QAbstractItemModel *model,
                        const QModelIndex &index ) const;

    void updateEditorGeometry( QWidget *editor,
                        const QStyleOptionViewItem &option,
                        const QModelIndex &index ) const;


    mutable QCheckBox * theCheckBox;
    mutable QWidget * pWidget;


private slots:

    void setData(bool val);


};

#endif // EDU_CHECKBOXDELEGATE_H
