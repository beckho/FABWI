#include "edu_detail_paln_dialog.h"
#include "ui_edu_detail_paln_dialog.h"

EDU_detail_paln_dialog::EDU_detail_paln_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EDU_detail_paln_dialog)
{
    ui->setupUi(this);
}

EDU_detail_paln_dialog::EDU_detail_paln_dialog(int edu_number,QSqlDatabase &mymesdb, QWidget *parent):
    QDialog(parent),
    ui(new Ui::EDU_detail_paln_dialog)
{
    ui->setupUi(this);
    this->edu_number = edu_number;
    this->mymesdb = mymesdb;
    ui->detail_start_time->setDate(QDate::currentDate());
    ui->detail_end_time->setDate(QDate::currentDate());


}

EDU_detail_paln_dialog::~EDU_detail_paln_dialog()
{
    delete ui;
}

void EDU_detail_paln_dialog::on_buttonBox_accepted()
{
    QSqlQuery query(mymesdb);
    query.exec(QString("INSERT INTO `FAB`.`EDU_detail_date` "
                       "(`edu_number`, `EDU_date_start_time`, `EDU_date_end_time`,`note`) "
                       "VALUES ('%1', '%2', '%3','%4');").arg(edu_number)
                       .arg(ui->detail_start_time->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                       .arg(ui->detail_end_time->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                       .arg(ui->LE_note->text()));

}

void EDU_detail_paln_dialog::closeEvent(QCloseEvent *)
{
    this->deleteLater();
}
