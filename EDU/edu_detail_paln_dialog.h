#ifndef EDU_DETAIL_PALN_DIALOG_H
#define EDU_DETAIL_PALN_DIALOG_H

#include <QDialog>
#include <QDateTime>
#include <QSqlDatabase>
#include <QSqlQuery>

namespace Ui {
class EDU_detail_paln_dialog;
}

class EDU_detail_paln_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit EDU_detail_paln_dialog(QWidget *parent = 0);
    explicit EDU_detail_paln_dialog(int edu_number, QSqlDatabase &mymesdb, QWidget *parent = 0);
    int edu_number;
    QSqlDatabase mymesdb;
    ~EDU_detail_paln_dialog();

private slots:
    void on_buttonBox_accepted();

private:
    void closeEvent(QCloseEvent *);
    Ui::EDU_detail_paln_dialog *ui;
};

#endif // EDU_DETAIL_PALN_DIALOG_H
