#include "edu_pw_dialog.h"
#include "ui_edu_pw_dialog.h"

edu_pw_dialog::edu_pw_dialog(QSqlDatabase my_mesdb,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::edu_pw_dialog)
{
    ui->setupUi(this);
    this->my_mesdb = my_mesdb;
    flag =false;
}

edu_pw_dialog::~edu_pw_dialog()
{
    delete ui;
}

void edu_pw_dialog::on_buttonBox_accepted()
{
    QSqlQuery query(my_mesdb);
    query.exec("select * from EDU_management");
    if(query.next()){
        QString pw = query.value("delete_password").toString();
        if(ui->LE_PW->text() == pw){
            flag=true;
        }
    }
    QDialog::accept();
}
