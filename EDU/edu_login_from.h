#ifndef EDU_LOGIN_FROM_H
#define EDU_LOGIN_FROM_H

#include <QDialog>

namespace Ui {
class EDU_login_from;
}

class EDU_login_from : public QDialog
{
    Q_OBJECT

public:
    explicit EDU_login_from(QWidget *parent = 0);
    ~EDU_login_from();

private:
    Ui::EDU_login_from *ui;
};

#endif // EDU_LOGIN_FROM_H
