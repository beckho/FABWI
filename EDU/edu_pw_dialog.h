#ifndef EDU_PW_DIALOG_H
#define EDU_PW_DIALOG_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
namespace Ui {
class edu_pw_dialog;
}

class edu_pw_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit edu_pw_dialog(QSqlDatabase my_mesdb,QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    bool flag;
    ~edu_pw_dialog();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::edu_pw_dialog *ui;
};

#endif // EDU_PW_DIALOG_H
