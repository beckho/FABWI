#ifndef EDU_USER_ADD_H
#define EDU_USER_ADD_H

#include <QDialog>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDateTime>
#include <QSqlError>
#include <QMessageBox>
#include <QSettings>
#include <global_define.h>

namespace Ui {
class EDU_user_add;
}

class EDU_user_add : public QDialog
{
    Q_OBJECT

public:
    explicit EDU_user_add(QWidget *parent = 0);
    explicit EDU_user_add(QSqlDatabase &my_mesdb,QWidget *parent = 0);
    QSqlDatabase ms_mesdb;
    QSqlDatabase my_mesdb;
    ~EDU_user_add();

private slots:
    void on_LE_mesid_editingFinished();

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::EDU_user_add *ui;
};

#endif // EDU_USER_ADD_H
