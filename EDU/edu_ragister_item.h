#ifndef EDU_RAGISTER_ITEM_H
#define EDU_RAGISTER_ITEM_H

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <EDU/edu_pw_dialog.h>
namespace Ui {
class EDU_ragister_item;
}

class EDU_ragister_item : public QWidget
{
    Q_OBJECT

public:
    explicit EDU_ragister_item(int edu_number,QSqlDatabase &my_mesdb,QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    int edu_number;
    Ui::EDU_ragister_item *ui;
    ~EDU_ragister_item();


private slots:
    void on_CB_requset_check_clicked(bool checked);

    void on_CB_fail_check_clicked(bool checked);

    void on_LE_note_editingFinished();

private:

};

#endif // EDU_RAGISTER_ITEM_H
