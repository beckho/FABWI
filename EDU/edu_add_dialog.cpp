#include "edu_add_dialog.h"
#include "ui_edu_add_dialog.h"
#include "ui_edu_ragister_item.h"
EDU_add_dialog::EDU_add_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EDU_add_dialog)
{
    ui->setupUi(this);
}

EDU_add_dialog::EDU_add_dialog(QSqlDatabase &my_mesdb,int edu_number,bool newflag, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EDU_add_dialog)
{
    ui->setupUi(this);

    this->edu_number = 0;
    this->newflag = newflag;
    modifymode= false;
    ui->EDU_start_date->setDate(QDate::currentDate());
    ui->EDU_end_date->setDate(QDate::currentDate());
    ui->reserve_start_date->setDate(QDate::currentDate());
    ui->reserve_end_date->setDate(QDate::currentDate());

    this->my_mesdb = my_mesdb;
    QSqlQuery query(my_mesdb);
    if(newflag){
        query.exec("update EDU_management set edu_number=edu_number+1 ");
        query.exec("select edu_number from EDU_management");
        if(query.next()){
            this->edu_number = query.value("edu_number").toInt();
        }
    }else {
        this->edu_number = edu_number;
    }
    detail_tablemodel = new QSqlQueryModel();




    ui->detail_time_table->setModel(detail_tablemodel);

    detail_tablemodel->setQuery(QString("select EDU_date_start_time,EDU_date_end_time,note from EDU_detail_date "
                                "where edu_number = '%1' order by EDU_date_start_time asc ").arg(this->edu_number),my_mesdb);
    detail_tablemodel->setHeaderData(0,Qt::Horizontal,QString(tr("edu_start_time")));
    detail_tablemodel->setHeaderData(1,Qt::Horizontal,QString(tr("edu_end_time")));
    detail_tablemodel->setHeaderData(2,Qt::Horizontal,QString(tr("edu_note")));

    ui->detail_time_table->horizontalHeader()->resizeSection(0,150);
    ui->detail_time_table->horizontalHeader()->resizeSection(1,150);
    ui->detail_time_table->horizontalHeader()->resizeSection(2,150);
    ui->modify_btn->setVisible(false);



    ragister_table_search();
    if(!newflag){
        query.exec(QString("select * from EDU_register where edu_number = '%1'").arg(this->edu_number));
        if(query.next()){
            ui->LE_name->setText(query.value("name").toString());
            ui->LE_name->setReadOnly(true);
            ui->EDU_start_date->setDateTime(query.value("start_date").toDateTime());
            ui->EDU_start_date->setReadOnly(true);
            ui->EDU_end_date->setDateTime(query.value("end_date").toDateTime());
            ui->EDU_end_date->setReadOnly(true);
            ui->LE_establishment->setText(query.value("establishment").toString());
            ui->LE_establishment->setReadOnly(true);
            ui->reserve_start_date->setDateTime(query.value("reserve_start_date").toDateTime());
            ui->reserve_start_date->setReadOnly(true);
            ui->reserve_end_date->setDateTime(query.value("reserve_end_date").toDateTime());
            ui->reserve_end_date->setReadOnly(true);
            ui->LE_site->setText(query.value("site").toString());
            ui->LE_site->setReadOnly(true);
            ui->TE_note->setText(query.value("note").toString());
            ui->TE_note->setReadOnly(true);


        }
        ui->detail_plan_addbtn->setEnabled(false);
        ui->detail_plan_delbtn->setEnabled(false);
        ui->modify_btn->setVisible(true);
    }

}

void EDU_add_dialog::ragister_table_search()
{
    while(ui->ragister_table_view->rowCount()){
        ui->ragister_table_view->removeRow(0);
    }
    QSqlQuery query(my_mesdb);
    query.exec(QString("select * from EDU_check_history where edu_number = '%1'").arg(edu_number));
    while(query.next()){
        QString id = query.value("id").toString();
        QString name = query.value("name").toString();
        bool request_check = query.value("requset_check").toBool();
        bool fail_check = query.value("fail_check").toBool();
        QString note = query.value("note").toString();
        int count = ui->ragister_table_view->rowCount();
        ui->ragister_table_view->insertRow(count);
        EDU_ragister_item *item = new EDU_ragister_item(edu_number,my_mesdb);
        item->ui->LA_ID->setText(id);
        item->ui->LA_name->setText(name);
        item->ui->CB_requset_check->setChecked(request_check);
        item->ui->CB_fail_check->setChecked(fail_check);
        item->ui->LE_note->setText(note);
        ui->ragister_table_view->setCellWidget(count,0,item->ui->LA_ID);
        ui->ragister_table_view->setCellWidget(count,1,item->ui->LA_name);
        ui->ragister_table_view->setCellWidget(count,2,item->ui->CB_requset_check);
        ui->ragister_table_view->setCellWidget(count,3,item->ui->CB_fail_check);
        ui->ragister_table_view->setCellWidget(count,4,item->ui->LE_note);
    }
}

EDU_add_dialog::~EDU_add_dialog()
{
    delete ui;
}

void EDU_add_dialog::on_buttonBox_accepted()
{
    QSqlQuery query(my_mesdb);
    if(ui->LE_name->text()==""){
        QMessageBox msg;
        msg.addButton(QMessageBox::Ok);
        msg.setText(tr("empty name"));
        msg.exec();
        return ;
    }

    if(newflag){
        query.exec(QString("INSERT INTO `FAB`.`EDU_register` (`edu_number`, `name`,"
                           " `start_date`, `end_date`, `establishment`, `reserve_start_date`,"
                           " `reserve_end_date`, `site`, `note`, `money`)"
                           " VALUES "
                           "('%1', '%2', '%3', '%4', '%5', '%6',"
                           " '%7', '%8', '%9', '%10');").arg(edu_number).arg(ui->LE_name->text())
                           .arg(ui->EDU_start_date->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                           .arg(ui->EDU_end_date->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                           .arg(ui->LE_establishment->text())
                           .arg(ui->reserve_start_date->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                           .arg(ui->reserve_end_date->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                           .arg(ui->LE_site->text()).arg(ui->TE_note->toPlainText()).arg("0")
                   );
    }else if(modifymode){
        query.exec(QString("UPDATE `FAB`.`EDU_register` set name = '%1',start_date = '%2',end_date = '%3',establishment='%4'"
                           ",reserve_start_date = '%5',reserve_end_date='%6',site='%7',note='%8' where edu_number = '%9'")
                            .arg(ui->LE_name->text()).arg(ui->EDU_start_date->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                            .arg(ui->EDU_end_date->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                            .arg(ui->LE_establishment->text())
                            .arg(ui->reserve_start_date->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                            .arg(ui->reserve_end_date->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                            .arg(ui->LE_site->text()).arg(ui->TE_note->toPlainText()).arg(this->edu_number)
                   );
    }

}

void EDU_add_dialog::on_detail_plan_addbtn_clicked()
{
    EDU_detail_paln_dialog *detail_dialog = new EDU_detail_paln_dialog(edu_number,my_mesdb);
    detail_dialog->exec();

    detail_tablemodel->setQuery(QString("select EDU_date_start_time,EDU_date_end_time,note from EDU_detail_date "
                                "where edu_number = '%1' order by EDU_date_start_time asc ").arg(edu_number),my_mesdb);
    detail_dialog->deleteLater();
}


void EDU_add_dialog::on_ragister_btn_clicked()
{
    EDU_user_from *user_from = new EDU_user_from(my_mesdb,edu_number);
    if(user_from->exec()==QDialog::Accepted){
         ragister_table_search();
    }
}

void EDU_add_dialog::on_detail_plan_delbtn_clicked()
{
    ui->detail_time_table->setSelectionMode(QAbstractItemView::SingleSelection);
    int current = ui->detail_time_table->selectionModel()->currentIndex().row();
    QString starttime = detail_tablemodel->index(current,0).data().toDateTime().toString("yyyy-MM-dd hh:mm:ss");
    QString endtime = detail_tablemodel->index(current,1).data().toDateTime().toString("yyyy-MM-dd hh:mm:ss");
    QString note = detail_tablemodel->index(current,2).data().toString();
    QSqlQuery query(my_mesdb);
    query.exec(QString("delete from `EDU_detail_date` where `edu_number`=%1 "
                       "AND `EDU_date_start_time`='%2' "
                       "AND `EDU_date_end_time`='%3' "
                       "AND `note`='%4' "
                       "LIMIT 1;").arg(edu_number).arg(starttime).arg(endtime).arg(note));

    detail_tablemodel->setQuery(QString("select EDU_date_start_time,EDU_date_end_time,note from EDU_detail_date "
                                        "where edu_number = '%1' order by EDU_date_start_time asc ").arg(edu_number),my_mesdb);
}

void EDU_add_dialog::on_remove_btn_clicked()
{
    edu_pw_dialog *popup = new edu_pw_dialog(my_mesdb);
    if(popup->exec()==QDialog::Accepted){
        if(popup->flag){
            ui->ragister_table_view->setSelectionMode(QAbstractItemView::SingleSelection);
            int current =  ui->ragister_table_view->selectionModel()->currentIndex().row();
            QLabel *temp = (QLabel *)ui->ragister_table_view->cellWidget(current,0);
            QSqlQuery query(my_mesdb);
            query.exec(QString("delete from `EDU_check_history` where `edu_number` = %1 AND "
                               "id = '%2'").arg(edu_number).arg(temp->text()));
            ragister_table_search();
        }
    }
    popup->deleteLater();
}

void EDU_add_dialog::on_modify_btn_clicked()
{
    edu_pw_dialog *popup = new edu_pw_dialog(my_mesdb);
    if(popup->exec()==QDialog::Accepted){
        if(popup->flag){
            modifymode = true;
            ui->LE_name->setReadOnly(false);
            ui->EDU_start_date->setReadOnly(false);
            ui->EDU_end_date->setReadOnly(false);
            ui->LE_establishment->setReadOnly(false);
            ui->reserve_start_date->setReadOnly(false);
            ui->reserve_end_date->setReadOnly(false);
            ui->LE_site->setReadOnly(false);
            ui->TE_note->setReadOnly(false);
            ui->detail_plan_addbtn->setEnabled(true);
            ui->detail_plan_delbtn->setEnabled(true);
        }
    }
    popup->deleteLater();


}
