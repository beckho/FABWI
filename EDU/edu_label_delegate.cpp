#include "edu_label_delegate.h"

EDU_label_delegate::EDU_label_delegate(QObject *parent):QItemDelegate(parent)
{

}
//void EDU_label_delegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
//{
//    drawDisplay(painter,option,option.rect,index.data().toString());
//    drawFocus(painter,option,option.rect);
//}

QWidget *EDU_label_delegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    thelabel = new QLabel((QWidget *) parent );

    return thelabel;
}
void EDU_label_delegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QVariant val = index.model()->data( index, Qt::DisplayRole );

    textdate = index.data().toString();
//    (static_cast<QLabel*>( editor ))->setText(val.toString());
}
void EDU_label_delegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
     model->setData( index, textdate);
}
void EDU_label_delegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry( option.rect );
}
