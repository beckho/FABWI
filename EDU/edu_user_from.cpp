#include "edu_user_from.h"
#include "ui_edu_user_from.h"

EDU_user_from::EDU_user_from(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EDU_user_from)
{
    ui->setupUi(this);
}
EDU_user_from::EDU_user_from(QSqlDatabase &my_mesdb,int edu_number,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EDU_user_from)
{
    ui->setupUi(this);
    this->my_mesdb = my_mesdb;
    this->edu_number = edu_number;

    maintable = new QSqlTableModel(this,this->my_mesdb);
    maintable->setTable("EDU_user_group");
    maintable->select();
    ui->maintableview->setModel(maintable);
    connect(ui->maintableview->horizontalHeader(),SIGNAL(sectionClicked(int)),this,SLOT(header_click(int)));
    register_model = new QStandardItemModel(0,2,this);
    register_model->setHeaderData(0,Qt::Horizontal,"id");
    register_model->setHeaderData(1,Qt::Horizontal,"name");

    ui->register_view->setModel(register_model);

    QSqlQuery query(my_mesdb);
    query.exec("select edu_group from EDU_user_group GROUP BY edu_group");
    while(query.next()){
        ui->groupbox->addItem(query.value("edu_group").toString());
    }

}

QStringList EDU_user_from::getNamelist() const
{
    return namelist;
}

void EDU_user_from::setNamelist(const QStringList &value)
{
    namelist = value;
}

void EDU_user_from::header_click(int index)
{

    maintable->setSort(index,Qt::AscendingOrder);
    maintable->select();

}
EDU_user_from::~EDU_user_from()
{
    delete ui;
}

void EDU_user_from::on_move_add_btn_clicked()
{
    foreach(QModelIndex index,ui->maintableview->selectionModel()->selectedIndexes()){
       QString id  = maintable->index(index.row(),0).data().toString();
       QString name  = maintable->index(index.row(),1).data().toString();
       QStandardItem *item1 = new QStandardItem(id);
       QStandardItem *item2 = new QStandardItem(name);
       int rowcount = register_model->rowCount();
       register_model->appendRow(item1);
       register_model->setItem(rowcount,1,item2);
    }
}

void EDU_user_from::on_remvoe_btn_clicked()
{
    foreach(QModelIndex index,ui->register_view->selectionModel()->selectedIndexes()){
        register_model->removeRow(index.row());
    }
}

void EDU_user_from::on_dialog_btn_accepted()
{
    QSqlQuery query(my_mesdb);
    for(int i=0;i<register_model->rowCount();i++){
        query.exec(QString("INSERT INTO `FAB`.`EDU_check_history` "
                           "(`edu_number`, `id`, `name`, `requset_check`, `fail_check`, `note`) "
                           "VALUES "
                           "('%1', '%2', '%3', '%4', '%5', '%6');").arg(edu_number)
                           .arg(register_model->index(i,0).data().toString())
                           .arg(register_model->index(i,1).data().toString())
                           .arg("0")
                           .arg("0")
                           .arg("")
                   );
    }

    QDialog::accept();
}

void EDU_user_from::on_add_id_btn_clicked()
{
    EDU_user_add *dialog = new EDU_user_add(my_mesdb);
    if(dialog->exec()==QDialog::Accepted){
        maintable->select();
    }
    dialog->deleteLater();
}

void EDU_user_from::on_del_id_btn_clicked()
{
    foreach(QModelIndex index,ui->maintableview->selectionModel()->selectedIndexes()){

       maintable->removeRow(index.row());
    }
}

void EDU_user_from::on_groupbox_currentTextChanged(const QString &arg1)
{
    if(arg1 != ""){
        QSqlQuery query(my_mesdb);
        query.exec(QString("select * from EDU_user_group where edu_group = '%1'").arg(arg1));
        while(query.next()){
            QString id  = query.value("id").toString();
            QString name  = query.value("name").toString();
            QStandardItem *item1 = new QStandardItem(id);
            QStandardItem *item2 = new QStandardItem(name);
            int rowcount = register_model->rowCount();
            register_model->appendRow(item1);
            register_model->setItem(rowcount,1,item2);
        }
    }
}

void EDU_user_from::on_dialog_btn_rejected()
{
    QDialog::reject();
}
