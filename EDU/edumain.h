#ifndef EDUMAIN_H
#define EDUMAIN_H

#include <QWidget>
#include <EDU/edu_add_dialog.h>
#include <QSqlDatabase>
#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QSettings>
#include <global_define.h>
#include <EDU/edu_label_delegate.h>
#include <EDU/edu_main_table_view.h>
namespace Ui {
class EDUmain;
}

class EDUmain : public QWidget
{
    Q_OBJECT

public:
    explicit EDUmain(QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    QString server_ip;
    QString db_port;
    QSqlTableModel *maintablemodel;
    QSqlQueryModel *maintable_model;
    ~EDUmain();


private slots:
    void on_edu_addbtn_clicked();

    void on_edu_search_btn_clicked();

    void on_noreserve_find_btn_clicked();

    void on_fail_find_btn_clicked();

    void on_main_list_view_doubleClicked(const QModelIndex &index);

    void on_edu_delbtn_clicked();

    void on_LE_PW_textChanged(const QString &arg1);

private:
    Ui::EDUmain *ui;
};

#endif // EDUMAIN_H
