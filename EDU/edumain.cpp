#include "edumain.h"
#include "ui_edumain.h"

EDUmain::EDUmain(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EDUmain)
{
    ui->setupUi(this);

    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    server_ip = settings.value("serverip").toString();
    db_port = settings.value("dbport").toString();
    settings.endGroup();

    QString now_datetime =QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");

    QString mydb_name = QString("MY_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }
    ui->start_date->setDate(QDate::currentDate());
    ui->end_date->setDate(QDate(ui->start_date->date().year(),12,31));
    ui->end_date->setTime(QTime(23,59,59));
    maintablemodel = new QSqlTableModel(this,my_mesdb);
    maintable_model = new QSqlTableModel();

    ui->main_list_view->setItemDelegate(new EDU_label_delegate());
    ui->main_list_view->setSortingEnabled(true);
    maintablemodel->setTable("EDU_register_view");

    ui->main_list_view->setModel(maintablemodel);
    ui->main_list_view->horizontalHeader()->hideSection(0);
    ui->main_list_view->horizontalHeader()->resizeSection(1,100);
    ui->main_list_view->horizontalHeader()->resizeSection(4,100);
    ui->main_list_view->horizontalHeader()->resizeSection(7,100);
    ui->main_list_view->horizontalHeader()->resizeSection(8,100);
    ui->main_list_view->horizontalHeader()->resizeSection(9,100);

    maintablemodel->setFilter(QString("start_date between '%1' AND '%2'")
                              .arg(ui->start_date->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                              .arg(ui->end_date->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    maintablemodel->select();
    maintablemodel->setSort(1,Qt::AscendingOrder);
    maintablemodel->setHeaderData(1,Qt::Horizontal,tr("edu_name"));
    maintablemodel->setHeaderData(2,Qt::Horizontal,tr("edu_start_date"));
    maintablemodel->setHeaderData(3,Qt::Horizontal,tr("edu_end_date"));
    maintablemodel->setHeaderData(4,Qt::Horizontal,tr("edu_establishment"));
    maintablemodel->setHeaderData(5,Qt::Horizontal,tr("edu_reserve_start_date"));
    maintablemodel->setHeaderData(6,Qt::Horizontal,tr("edu_reserve_end_date"));
    maintablemodel->setHeaderData(7,Qt::Horizontal,tr("edu_total_count"));
    maintablemodel->setHeaderData(8,Qt::Horizontal,tr("edu_requset_count"));
    maintablemodel->setHeaderData(9,Qt::Horizontal,tr("edu_fail_count"));




}

EDUmain::~EDUmain()
{
    delete ui;
}

void EDUmain::on_edu_addbtn_clicked()
{
    EDU_add_dialog *add_dialog = new EDU_add_dialog(my_mesdb,0,true);
    if(add_dialog->exec()==QDialog::Accepted){
        on_edu_search_btn_clicked();
    }
    maintablemodel->select();


}

void EDUmain::on_edu_search_btn_clicked()
{
    maintablemodel->setFilter(QString("start_date between '%1' AND '%2'")
                              .arg(ui->start_date->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                              .arg(ui->end_date->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    maintablemodel->select();
    maintablemodel->setSort(1,Qt::AscendingOrder);

}

void EDUmain::on_noreserve_find_btn_clicked()
{
    maintablemodel->setFilter(QString("start_date between '%1' AND '%2' AND requset_check_count > 0")
                              .arg(ui->start_date->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                              .arg(ui->end_date->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    maintablemodel->select();
    maintablemodel->setSort(1,Qt::AscendingOrder);


}

void EDUmain::on_fail_find_btn_clicked()
{
    maintablemodel->setFilter(QString("start_date between '%1' AND '%2' AND fail_check_count > 0")
                              .arg(ui->start_date->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                              .arg(ui->end_date->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    maintablemodel->select();
    maintablemodel->setSort(1,Qt::AscendingOrder);

}

void EDUmain::on_main_list_view_doubleClicked(const QModelIndex &index)
{
    int edu_nubmer_temp = maintablemodel->index(index.row(),0).data().toInt();
    EDU_add_dialog *temp = new EDU_add_dialog(my_mesdb,edu_nubmer_temp,false);
    temp->exec();
    temp->deleteLater();

}

void EDUmain::on_edu_delbtn_clicked()
{

    int row = ui->main_list_view->selectionModel()->currentIndex().row();
    maintablemodel->removeRow(row);
    maintablemodel->select();

}

void EDUmain::on_LE_PW_textChanged(const QString &arg1)
{
    if(arg1=="0118"){
        ui->edu_addbtn->setEnabled(true);
        ui->edu_delbtn->setEnabled(true);
    }else {
        ui->edu_addbtn->setEnabled(false);
        ui->edu_delbtn->setEnabled(false);
    }
}
