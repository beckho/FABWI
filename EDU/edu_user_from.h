#ifndef EDU_USER_FROM_H
#define EDU_USER_FROM_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlTableModel>
#include <QStandardItemModel>
#include <EDU/edu_user_add.h>

namespace Ui {
class EDU_user_from;
}

class EDU_user_from : public QDialog
{
    Q_OBJECT

public:
    explicit EDU_user_from(QWidget *parent = 0);
    explicit EDU_user_from(QSqlDatabase &my_mesdb, int edu_number, QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    QSqlTableModel *maintable;
    QStringList namelist;
    QStandardItemModel *register_model;
    int edu_number;
    ~EDU_user_from();

    QStringList getNamelist() const;
    void setNamelist(const QStringList &value);
public slots:
    void header_click(int index);
private slots:
    void on_move_add_btn_clicked();

    void on_remvoe_btn_clicked();

    void on_dialog_btn_accepted();

    void on_add_id_btn_clicked();

    void on_del_id_btn_clicked();

    void on_groupbox_currentTextChanged(const QString &arg1);

    void on_dialog_btn_rejected();

private:
    Ui::EDU_user_from *ui;
};

#endif // EDU_USER_FROM_H
