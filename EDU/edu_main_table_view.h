#ifndef EDU_MAIN_TABLE_VIEW_H
#define EDU_MAIN_TABLE_VIEW_H

#include <QObject>
#include <QTableView>
#include <QKeyEvent>
#include <QApplication>
#include <QClipboard>
class edu_main_table_view : public QTableView
{
    Q_OBJECT
public:
    explicit edu_main_table_view(QObject *parent = 0);
private:
   void keyPressEvent(QKeyEvent *event);

};

#endif // EDU_MAIN_TABLE_VIEW_H
