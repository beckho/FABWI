#include "edu_user_add.h"
#include "ui_edu_user_add.h"

EDU_user_add::EDU_user_add(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EDU_user_add)
{
    ui->setupUi(this);
}
EDU_user_add::EDU_user_add(QSqlDatabase &my_mesdb,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EDU_user_add)
{
    ui->setupUi(this);
    this->my_mesdb =my_mesdb;
//    QString msdb_name = QString("MS_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
//    ms_mesdb = QSqlDatabase::addDatabase("QODBC",msdb_name);
//    QString serverinfo = QString("DRIVER={SQL Server};Server=%1;Database=%2;Uid=%3;Port=1433;Pwd=%4").arg(DBMESSERVERIP).arg(DBMESNAME).arg(DBMESUSERNAME).arg(DBMESPW);
//    ms_mesdb.setDatabaseName(serverinfo);
//    if(!ms_mesdb.open()){
//        qDebug()<<"fasle";
//        qDebug()<<ms_mesdb.lastError().text();
//    }else {
//        qDebug()<<"open";
//    }
}

EDU_user_add::~EDU_user_add()
{
    delete ui;
}

void EDU_user_add::on_LE_mesid_editingFinished()
{
//    QSqlQuery query(ms_mesdb);
//    query.exec(QString("select USER_ID,USER_NAME from V_NB_USERS where USER_ID = '%1'").arg(ui->LE_mesid->text()));
//    if(query.next()){
//        ui->LA_mesname->setText(query.value("USER_NAME").toString());
//        ui->LE_mesid->setReadOnly(true);
//    }else {
//        ui->LA_mesname->setText("");
//    }
}

void EDU_user_add::on_buttonBox_accepted()
{
    if(ui->LE_mesid->text() == ""){
        QMessageBox msg;
        msg.addButton(QMessageBox::Ok);

        msg.setText(tr("empty USER_ID"));

        msg.exec();
        QDialog::accept();
        return ;
    }
    if(ui->LE_mesname->text() == ""){
        QMessageBox msg;
        msg.addButton(QMessageBox::Ok);

        msg.setText(tr("don't have id"));

        msg.exec();
        QDialog::accept();
        return ;
    }
    QSqlQuery query(my_mesdb);
    query.exec(QString("INSERT INTO `FAB`.`EDU_user_group` (`id`, `name`, `edu_group`) VALUES ('%1', '%2', '%3');")
               .arg(ui->LE_mesid->text()).arg(ui->LE_mesname->text()).arg(ui->LE_group->text()));
    QDialog::accept();

    return;
}

void EDU_user_add::on_buttonBox_rejected()
{
    QDialog::reject();

}
