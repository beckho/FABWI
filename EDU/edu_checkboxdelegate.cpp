#include "edu_checkboxdelegate.h"

EDU_CheckBoxDelegate::EDU_CheckBoxDelegate(QObject *parent):QItemDelegate(parent)
{

}
void EDU_CheckBoxDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    drawCheck(painter,option,option.rect,index.model()->data(index,Qt::DisplayRole).toBool()?Qt::Checked:Qt::Unchecked);

//    drawFocus(painter,option,option.rect);
}


QWidget *EDU_CheckBoxDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    theCheckBox = new QCheckBox();
    QObject::connect(theCheckBox,SIGNAL(toggled(bool)),this,SLOT(setData(bool)));
    return theCheckBox;
}

void EDU_CheckBoxDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    int val = index.model()->data( index, Qt::DisplayRole ).toInt();

    (static_cast<QCheckBox*>( editor ))->setChecked(val);
}

void EDU_CheckBoxDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    model->setData( index, (int)(static_cast<QCheckBox*>( editor )->isChecked() ) );
}

void EDU_CheckBoxDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry( option.rect );
}

void EDU_CheckBoxDelegate::setData(bool val)
{
    emit commitData(theCheckBox);
}
