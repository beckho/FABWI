#include "edu_ragister_item.h"
#include "ui_edu_ragister_item.h"

EDU_ragister_item::EDU_ragister_item(int edu_number,QSqlDatabase &my_mesdb,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EDU_ragister_item)
{
    ui->setupUi(this);
    this->edu_number = edu_number;
    this->my_mesdb = my_mesdb;

}

EDU_ragister_item::~EDU_ragister_item()
{
    delete ui;
}

void EDU_ragister_item::on_CB_requset_check_clicked(bool checked)
{
    QSqlQuery query(my_mesdb);
    if(!checked){
        edu_pw_dialog *popup = new edu_pw_dialog(my_mesdb);
        if(popup->exec() == QDialog::Accepted){
            if(popup->flag){
                query.exec(QString("update EDU_check_history set requset_check = '%1' where edu_number = '%2' AND id = '%3'")
                           .arg((int)checked).arg(edu_number).arg(ui->LA_ID->text()));
            }else {
                ui->CB_requset_check->setChecked(true);
            }
        }else {
           ui->CB_requset_check->setChecked(!checked);
        }
        popup->deleteLater();
    }else {
        query.exec(QString("update EDU_check_history set requset_check = '%1' where edu_number = '%2' AND id = '%3'")
                   .arg((int)checked).arg(edu_number).arg(ui->LA_ID->text()));
    }


}

void EDU_ragister_item::on_CB_fail_check_clicked(bool checked)
{
    QSqlQuery query(my_mesdb);
    if(!checked){
        edu_pw_dialog *popup = new edu_pw_dialog(my_mesdb);
        if(popup->exec() == QDialog::Accepted){
            if(popup->flag){
                query.exec(QString("update EDU_check_history set fail_check = '%1' where edu_number = '%2' AND id = '%3'")
                           .arg((int)checked).arg(edu_number).arg(ui->LA_ID->text()));
            }else {
                ui->CB_fail_check->setChecked(true);
            }
        }else {
            ui->CB_fail_check->setChecked(!checked);
        }
        popup->deleteLater();
    }else {
        query.exec(QString("update EDU_check_history set fail_check = '%1' where edu_number = '%2' AND id = '%3'")
                   .arg((int)checked).arg(edu_number).arg(ui->LA_ID->text()));
    }

}

void EDU_ragister_item::on_LE_note_editingFinished()
{
    QSqlQuery query(my_mesdb);
    query.exec(QString("update EDU_check_history set note = '%1' where edu_number = '%2' AND id = '%3'")
               .arg(ui->LE_note->text()).arg(edu_number).arg(ui->LA_ID->text()));
}
