#ifndef SUGGESTION_MAAIN_H
#define SUGGESTION_MAAIN_H

#include <QWidget>


namespace Ui {
class suggestion_main;
}

class suggestion_main : public QWidget
{
    Q_OBJECT

public:
    explicit suggestion_main(QWidget *parent = 0);


    ~suggestion_main();

private:
    Ui::suggestion_main *ui;
};

#endif // SUGGESTION_MAAIN_H
