#ifndef SHINCRON_LOG_H
#define SHINCRON_LOG_H

#include <QWidget>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDateTime>
#include <QStringList>
#include <QSqlDriver>
#include <QFileInfo>
#include <QSqlQueryModel>
#include <logsrc/shincron_chartview.h>
#include <QChart>
#include <QLineSeries>
#include <QScatterSeries>
#include <QCategoryAxis>
#include <QValueAxis>
#include <logsrc/shincron_log_viewer.h>
#include <QCheckBox>
#include <QProgressBar>
QT_CHARTS_USE_NAMESPACE
namespace Ui {
class Shincron_log;
}

class Shincron_log : public QWidget
{
    Q_OBJECT

public:
    explicit Shincron_log(QWidget *parent = 0);
    void dropEvent(QDropEvent *event);
    void analysor(QSqlDatabase light_db, QList<QUrl> urls);
    ~Shincron_log();
    QSqlQueryModel *sqlquery_model;

    QString db_name ;
    QSqlDatabase light_db;
    void chart_make(QSqlDatabase light_db,int layer,QChart *layer_chart,QCheckBox *Film, QCheckBox *Rate, QCheckBox *Depostion, QCheckBox *press);
    QChart *layer1_chart;
    Shincron_chartview *layer1_chart_view;
    QChart *layer2_chart;
    Shincron_chartview *layer2_chart_view;
    QChart *layer3_chart;
    Shincron_chartview *layer3_chart_view;
    QChart *layer4_chart;
    Shincron_chartview *layer4_chart_view;

    QLineSeries *Film_line[5];
    QScatterSeries * Film_scatter_series[5];
    QLineSeries *Rate_line[5];
    QScatterSeries * Rate_scatter_series[5];
    QLineSeries *Depostion_line[5];
    QScatterSeries * Depostion_scatter_series[5];
    QLineSeries *press_line[5];
    QScatterSeries * press_scatter_series[5];

    void range_make(QCheckBox *Film,QCheckBox *Rate,QCheckBox *Depostion,QCheckBox *press,int layer,QChart *chart_item);


private slots:
    void on_CB_Film_1_clicked(bool checked);

    void on_CB_rate_1_clicked(bool checked);

    void on_CB_depostion_1_clicked(bool checked);

    void on_CB_Press_1_clicked(bool checked);

    void on_PB_label_view_1_clicked();

    void on_PB_zomm_init_1_clicked();



    void on_PB_outside1_clicked();

    void on_CB_Film_2_clicked(bool checked);

    void on_CB_rate_2_clicked(bool checked);

    void on_CB_depostion_2_clicked(bool checked);

    void on_CB_Press_2_clicked(bool checked);

    void on_PB_label_view_2_clicked();

    void on_PB_zomm_init_2_clicked();

    void on_PB_outside2_clicked();



    void on_CB_rate_3_clicked(bool checked);

    void on_CB_Film_3_clicked(bool checked);

    void on_CB_depostion_3_clicked(bool checked);

    void on_CB_Press_3_clicked(bool checked);

    void on_PB_label_view_3_clicked();

    void on_PB_zomm_init_3_clicked();

    void on_PB_outside3_clicked();

    void on_CB_Film_4_clicked(bool checked);

    void on_CB_rate_4_clicked(bool checked);

    void on_CB_depostion_4_clicked(bool checked);

    void on_CB_Press_4_clicked(bool checked);

    void on_PB_label_view_4_clicked();

    void on_PB_zomm_init_4_clicked();

    void on_PB_outside4_clicked();

    void on_CB_layer1_view_clicked(bool checked);

    void on_CB_layer2_view_clicked(bool checked);

    void on_CB_layer3_view_clicked(bool checked);

    void on_CB_layer4_view_clicked(bool checked);

private:
    Ui::Shincron_log *ui;
    void dragEnterEvent(QDragEnterEvent *event);
};

#endif // SHINCRON_LOG_H
