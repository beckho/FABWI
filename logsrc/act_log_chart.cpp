#include "act_log_chart.h"


act_log_chart::act_log_chart(QGraphicsItem *parent, Qt::WindowFlags wFlags)
{
    setTitle("");
    setAcceptDrops(true);
    setAcceptHoverEvents(true);
}

void act_log_chart::dropEvent(QGraphicsSceneDragDropEvent *event)
{
    emit drop_event_signal((QMimeData *)event->mimeData());
    QChart::dropEvent(event);
}
