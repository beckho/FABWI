#include "shincron_log_viewer.h"
#include "ui_shincron_log_viewer.h"

shincron_log_viewer::shincron_log_viewer(QWidget *chartview, QWidget *return_view, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::shincron_log_viewer)
{
    ui->setupUi(this);
    this->chartview = chartview;
    this->return_view = return_view;
    ui->main_layout->addWidget(chartview);
}

shincron_log_viewer::~shincron_log_viewer()
{
    delete ui;
}

void shincron_log_viewer::closeEvent(QCloseEvent *event)
{
    return_view->layout()->addWidget(chartview);
    QWidget::closeEvent(event);
}
