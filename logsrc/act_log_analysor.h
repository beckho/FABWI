#ifndef ACT_LOG_ANALYSOR_H
#define ACT_LOG_ANALYSOR_H

#include <QWidget>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDateTime>
#include <QStringList>
#include <QSqlDriver>
#include <logsrc/act_log_chart.h>
#include <logsrc/act_log_chartview.h>
#include <QDateTimeAxis>
#include <QLineSeries>
#include <QXYSeries>
#include <QValueAxis>
#include <QBarSeries>
#include <QStackedBarSeries>
#include <QBarSet>
#include <QBarCategoryAxis>
#include <QHorizontalStackedBarSeries>
#include <QDebug>
#include <QHash>
#include <QStandardItem>
#include <QStandardItemModel>
#define UNIT_NUMBER 0
#define CURRENT_UNIT_NUMBER 0
#define RECIPE_INFO 1
#define IN_DATE 2
#define IN_TIME 3
#define OUT_DATE 4
#define OUT_TIME 5
#define START_PROCESS 6
#define END_PROCESS 7


namespace Ui {
class Act_log_analysor;
}

class Act_log_analysor : public QWidget
{
    Q_OBJECT

public:
    explicit Act_log_analysor(QWidget *parent = 0);
    QStringList sourceline_list;
    void analysor(QSqlDatabase light_db, QString file_path);
    act_log_chart *main_chart;
    act_log_chartview *main_chart_view;
    QBarCategoryAxis  *axisY ;
    QValueAxis  *axisX ;
    QHash<QString,QVector<QBarSet *> *> bar_set_map;
    void dropEvent(QDropEvent *event);
    ~Act_log_analysor();

private:
    Ui::Act_log_analysor *ui;

    void dragEnterEvent(QDragEnterEvent *event);



private slots:
    void color_map_data_change(QModelIndex index1, QModelIndex index2, QVector<int> role);
    void hover_item(bool flag,int data1,QBarSet * barset);
    void chart_drop_event(QMimeData *source);



    void on_zoom_reset_clicked();
};

#endif // ACT_LOG_ANALYSOR_H
