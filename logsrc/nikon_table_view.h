#ifndef NIKON_TABLE_VIEW_H
#define NIKON_TABLE_VIEW_H

#include <QObject>
#include <QWidget>
#include <QTableWidget>
#include <QKeyEvent>
#include <QApplication>
#include <QClipboard>
#include <QLabel>
class nikon_table_view : public QTableWidget
{
    Q_OBJECT
public:
    nikon_table_view(QWidget *parent = 0);

private:
      void keyPressEvent(QKeyEvent *event);
};

#endif // NIKON_TABLE_VIEW_H
