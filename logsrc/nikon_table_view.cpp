#include "nikon_table_view.h"

nikon_table_view::nikon_table_view(QWidget *parent):QTableWidget(parent)
{

}

void nikon_table_view::keyPressEvent(QKeyEvent *event)
{
    // If Ctrl-C typed
     if (event->key() == Qt::Key_C && (event->modifiers() & Qt::ControlModifier))
     {
         QModelIndexList cells = selectedIndexes();


         qSort(cells); // Necessary, otherwise they are in column order

         QString text;

         int currentRow = 0; // To determine when to insert newlines
         int first_row=0;
         foreach (const QModelIndex& cell, cells) {
             if (text.length() == 0) {
                 // First item
                 first_row = cell.row();
             } else if (cell.row() != currentRow) {
                 // New row
                 text += '\n';
             } else {
                 // Next cell
                 text += '\t';
             }
             currentRow = cell.row();
             if((cell.column() == 0) || (cell.column() == 1)){
                 text += " ";
             }else {
                QLabel *cellw = (QLabel *)cellWidget(cell.row(),cell.column());
                text += cellw->text();
             }



         }
         QString header_data;
         foreach (const QModelIndex& cell, cells) {
             if(header_data.length() == 0){

             }else if (first_row != cell.row()){
                 header_data += '\n';
                 break;
             }else {
                 header_data += '\t';
             }

              header_data += model()->headerData(cell.column(),Qt::Horizontal).toString();

         }
         text = header_data + text;

         QApplication::clipboard()->setText(text);
         return;
     }
     QTableView::keyPressEvent(event);
}
