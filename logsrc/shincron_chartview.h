#ifndef SHINCRON_CHARTVIEW_H
#define SHINCRON_CHARTVIEW_H

#include <QObject>
#include <QWidget>
#include <QtCharts/QChartView>
#include <QtWidgets/QRubberBand>
#include <QDebug>
QT_CHARTS_USE_NAMESPACE
class Shincron_chartview : public QChartView
{
    Q_OBJECT
public:
    Shincron_chartview(QChart *chart, QWidget *parent = 0);
    QChart *mchart;
protected:
    void keyPressEvent(QKeyEvent *event);
};

#endif // SHINCRON_CHARTVIEW_H
