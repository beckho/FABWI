#include "act_log_analysor.h"
#include "ui_act_log_analysor.h"

Act_log_analysor::Act_log_analysor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Act_log_analysor)
{
    ui->setupUi(this);

    main_chart = new act_log_chart();
    main_chart_view = new act_log_chartview(main_chart);
    axisY = new QBarCategoryAxis();
    main_chart->addAxis(axisY, Qt::AlignLeft);
    axisX = new QValueAxis();
    main_chart->addAxis(axisX, Qt::AlignBottom);


    ui->m_chart_view_layout->addWidget(main_chart_view);
    connect(main_chart,SIGNAL(drop_event_signal(QMimeData*)),this,SLOT(chart_drop_event(QMimeData*)));


}



Act_log_analysor::~Act_log_analysor()
{
    delete ui;
}

void Act_log_analysor::dropEvent(QDropEvent *event)
{
    QMimeData *source = (QMimeData *)event->mimeData();
    QString file_name;

    if(source->hasUrls()){
         sourceline_list.clear();
        QString db_name = QString("local_DB_OI_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
        QSqlDatabase light_db = QSqlDatabase::addDatabase("QSQLITE",db_name);
        light_db.setDatabaseName(":memory:");
        if(!light_db.open()){
            qDebug()<<light_db.lastError().text();
        }else {
            qDebug()<<"open";
        }
        qDebug()<<source->urls().at(0).toLocalFile();


        analysor(light_db,source->urls().at(0).toLocalFile());


    }

    QWidget::dropEvent(event);
}

void Act_log_analysor::chart_drop_event(QMimeData *source)
{
    QString file_name;

    if(source->hasUrls()){
         sourceline_list.clear();
        QString db_name = QString("local_DB_OI_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
        QSqlDatabase light_db = QSqlDatabase::addDatabase("QSQLITE",db_name);
        light_db.setDatabaseName(":memory:");
        if(!light_db.open()){
            qDebug()<<light_db.lastError().text();
        }else {
            qDebug()<<"open";
        }
        qDebug()<<source->urls().at(0).toLocalFile();

        analysor(light_db,source->urls().at(0).toLocalFile());

    }

}


void Act_log_analysor::analysor(QSqlDatabase light_db, QString file_path)
{
    QSqlQuery query(light_db);
    query.exec("DROP TABLE row_data ");
    query.exec("DROP TABLE row_data_view1 ");
    query.exec("DROP TABLE row_data_view2 ");
    query.exec("DROP TABLE row_data_view3 ");
    query.exec("DROP TABLE Base_info ");
    query.exec("CREATE TABLE [row_data]( "
               " [slot_number] INT, "
               " [unit_name] NVARCHAR(50), "
               " [recipe_info] NVARCHAR(50), "
               " [IN_TIME] TIMESTAMP, "
               " [OUT_TIME] TIMESTAMP, "
               " [Start_time] TIMESTAMP, "
               " [End_time] TIMESTAMP);");
    query.exec("CREATE TABLE [Base_info]( "
               " [Lot_name] NVARCHAR(50), "
               " [Recipe_name] NVARCHAR(50), "
               "[Slot_total] INT,"
               "[start_time] TIMESTAMP)");
    query.exec("CREATE VIEW [row_data_view1] AS "
               "SELECT [slot_number], "
                      "[unit_name], "
                      "[recipe_info], "
                      "[IN_TIME], "
                      "[OUT_TIME], "
                      "[Start_time], "
                      "[End_time], "
                      "((JULIANDAY ([OUT_TIME]) - JULIANDAY ([IN_TIME])) * 86400.0) AS [Duration], "
                      "(SELECT [B].[OUT_TIME] FROM   [row_data] [B] WHERE  [A].[slot_number] = [B].[slot_number] AND [B].[OUT_TIME] < [A].[IN_TIME] "
                      "ORDER  BY [OUT_TIME] desc LIMIT 1) AS [before] FROM [row_data] [A];");
    query.exec("CREATE VIEW [row_data_view2] AS "
               "SELECT [slot_number], "
                      "[unit_name], "
                      "[recipe_info], "
                      "[IN_TIME], "
                      "[OUT_TIME], "
                      "[Start_time], "
                      "[End_time], "
                      "[Duration], "
                      "((JULIANDAY ([End_time]) - JULIANDAY ([Start_time])) * 86400.0) AS [process_Duration],"
                      "CASE WHEN ([before] IS NULL) THEN (SELECT [start_time]"
                      "FROM   [Base_Info]) ELSE [before] END AS [before_1], "
                      "((JULIANDAY ([IN_TIME]) - JULIANDAY ([before])) * 86400.0) AS [space_time] "
                      "FROM   [row_data_view1];");
    query.exec("CREATE VIEW [row_data_view3] AS "
               "SELECT [slot_number], "
                      "[unit_name],  "
                      "[recipe_info], "
                      "[IN_TIME], "
                      "[OUT_TIME], "
                      "[Start_time], "
                      "[End_time], "
                      "[Duration], "
                      "[process_Duration],"
                      "[before_1], "
                      "CASE WHEN ([space_time] IS NULL) THEN (JULIANDAY ([IN_TIME]) - JULIANDAY ([before_1])) * 86400.0 ELSE [space_time] END AS [space_time_1] "
                      " FROM   [row_data_view2];");
    QFile source_file(file_path);
    if(!source_file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug()<<"File open error";
    }else {
        qDebug()<<"File open";
    }
    QStringList source_list;
    while(!source_file.atEnd())
    {
        QString read_line = QString(source_file.readLine());
        source_list<<read_line;
    }
    QString lot_name;
    lot_name = source_list.at(0);
    QString row_data = source_list.at(3);
    QString total_slot_number = row_data.split(",").at(0);
    QString recipe_name = row_data.split(",").at(2);
    row_data = source_list.at(2);
    QDate lot_start_date = QDate::fromString(row_data.split(",").at(0),"yyyy/MM/dd");
    QTime lot_start_time = QTime::fromString(row_data.split(",").at(1),"hh:mm:ss-z");
    QDateTime start_time;
    start_time.setDate(lot_start_date);
    start_time.setTime(lot_start_time);
    QString insert_Base_info = QString("insert into Base_info(Lot_name,Recipe_name,Slot_total,start_time) values "
                                       "('%1','%2','%3','%4')").arg(lot_name).arg(recipe_name).arg(total_slot_number)
                                        .arg(start_time.toString("yyyy-MM-dd hh:mm:ss.z"));
    query.exec(insert_Base_info);
    light_db.driver()->beginTransaction();
    int current_slot = 0;
    for(int i=5;i<source_list.count();i++){
        row_data = source_list.at(i);
        QString start_process = row_data.split(",").at(START_PROCESS);
        if(start_process == "Normal end\n"){
            current_slot = QString(row_data.split(",").at(CURRENT_UNIT_NUMBER)).toInt();
            continue;
        }else if (start_process == "Warning\n"){
            current_slot = QString(row_data.split(",").at(CURRENT_UNIT_NUMBER)).toInt();
            continue;
        }
        QString Unit = row_data.split(",").at(UNIT_NUMBER);
        QString Recipe_info = row_data.split(",").at(RECIPE_INFO);
        QDate IN_date = QDate::fromString(row_data.split(",").at(IN_DATE),"yyyy/MM/dd");
        QTime IN_Time = QTime::fromString(row_data.split(",").at(IN_TIME),"hh:mm:ss-z");
        QDate OUT_date = QDate::fromString(row_data.split(",").at(OUT_DATE),"yyyy/MM/dd");
        QTime OUT_Time = QTime::fromString(row_data.split(",").at(OUT_TIME),"hh:mm:ss-z");
        QTime Start_process_Time = QTime::fromString(row_data.split(",").at(START_PROCESS),"hh:mm:ss-z");
        QTime END_process_Time = QTime::fromString(row_data.split(",").at(END_PROCESS),"hh:mm:ss-z\n");
        QDateTime IN_date_time;
        QDateTime OUT_date_time;
        QDateTime Start_process_datetime;
        QDateTime END_process_datetime;
        IN_date_time.setDate(IN_date);
        IN_date_time.setTime(IN_Time);
        OUT_date_time.setDate(OUT_date);
        OUT_date_time.setTime(OUT_Time);
        Start_process_datetime.setDate(IN_date);
        Start_process_datetime.setTime(Start_process_Time);
        END_process_datetime.setDate(OUT_date);
        END_process_datetime.setTime(END_process_Time);

        QString query_str = QString("insert into row_data(slot_number,unit_name,recipe_info,IN_time,OUT_Time,"
                                    "Start_time,End_time) values('%1','%2','%3','%4','%5','%6','%7')")
                                    .arg(current_slot).arg(Unit).arg(Recipe_info)
                                    .arg(IN_date_time.toString("yyyy-MM-dd hh:mm:ss.z"))
                                    .arg(OUT_date_time.toString("yyyy-MM-dd hh:mm:ss.z"))
                                    .arg(Start_process_datetime.toString("yyyy-MM-dd hh:mm:ss.z"))
                                    .arg(END_process_datetime.toString("yyyy-MM-dd hh:mm:ss.z"));
        query.exec(query_str);
    }
    light_db.driver()->commitTransaction();
    //init
    main_chart->removeAllSeries();
    main_chart->removeAxis(axisY);
//    main_chart->removeAxis(axisX);
    main_chart->legend()->hide();


    QHorizontalStackedBarSeries *series = new QHorizontalStackedBarSeries();
    int k =0;
    QStringList categories;

    query.exec("select unit_name from row_data_view3 group by unit_name");
    QHash<QString,QColor> unit_color_map;


    QStandardItemModel *model = new QStandardItemModel();
    ui->unit_color_list->setModel(model);

    //genaration ramdom code
    while(query.next()){
        unit_color_map.insert(query.value("unit_name").toString(),QColor::fromHsv(rand()%254, rand()%254, rand()%254, 200));
        QPixmap pixmap(100,100);
        pixmap.fill((QColor)unit_color_map.value(query.value("unit_name").toString()));
        QStandardItem *item = new QStandardItem(QIcon(pixmap),query.value("unit_name").toString());
        item->setCheckable(true);
        item->setCheckState(Qt::Checked);
        model->appendRow(item);
        QVector<QBarSet *> *tempbar_list = new QVector<QBarSet *> ();
        bar_set_map.insert(query.value("unit_name").toString(),tempbar_list);
    }
    connect(model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),this,SLOT(color_map_data_change(QModelIndex,QModelIndex,QVector<int>)));
    for(int i=0;i<24;i++){
        query.exec(QString("select * from row_data_view3 where slot_number = '%1'").arg(i+1));
        categories<<QString("%1").arg(i+1);
        while(query.next()){
            QBarSet *temp_bar = new QBarSet(QString("%1").arg(k));
            QBarSet *temp_bar_1 = new QBarSet(QString("s%1").arg(k));
            k++;
            for(int j=0;j<24;j++){
                if(i==j){
                    temp_bar->append(query.value("space_time_1").toReal());
                    temp_bar->setColor(QColor("white"));
                    temp_bar_1->setProperty("value_data","");
                    temp_bar->setProperty("tag","");
                    temp_bar->setProperty("unit_name","");
                    temp_bar->setProperty("process_time","");
                    temp_bar->setProperty("recipeinfo","");
                    //
                    temp_bar_1->append(query.value("Duration").toReal());
                    temp_bar_1->setColor((QColor)unit_color_map.value(query.value("unit_name").toString()));
                    temp_bar_1->setProperty("value_data",QString("%1").arg(query.value("Duration").toReal(),0,'f',2));
                    temp_bar_1->setProperty("tag",query.value("unit_name").toString());
                    temp_bar_1->setProperty("unit_name",(query.value("unit_name").toString()));
                    temp_bar_1->setProperty("process_time",QString("%1").arg(query.value("process_Duration").toReal(),0,'f',2));
                    temp_bar_1->setProperty("recipeinfo",(query.value("recipe_info").toString()));
                    bar_set_map.value(query.value("unit_name").toString())->append(temp_bar_1);
                }else {
                    temp_bar->append(0);

                    temp_bar_1->append(0);
                }
            }
            temp_bar->setLabelColor(QColor(0,0,0,0));
            temp_bar_1->setLabelColor(QColor("black"));
            series->append(temp_bar);
            series->append(temp_bar_1);
        }
    }
    connect(series,SIGNAL(hovered(bool,int,QBarSet*)),this,SLOT(hover_item(bool,int,QBarSet*)));

    main_chart->addSeries(series);
    axisY = new QBarCategoryAxis();
    axisY->append(categories);
    main_chart->addAxis(axisY, Qt::AlignLeft);
    main_chart->createDefaultAxes();
//    axisX = new QValueAxis();
//    main_chart->addAxis(axisX,Qt::AlignBottom);
//    series->attachAxis(axisX);
    series->setLabelsFormat("@tag <br> @real2f");

    series->setLabelsVisible(true);


}

void Act_log_analysor::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();


}

void Act_log_analysor::color_map_data_change(QModelIndex index1, QModelIndex index2, QVector<int> role)
{
    qDebug()<<index1.data(Qt::CheckStateRole);
    qDebug()<<index1.data(Qt::DisplayRole).toString();

     QVector<QBarSet *> *bar_list = bar_set_map.value(index1.data(Qt::DisplayRole).toString());
     qDebug()<<"barlist count = "<<bar_list->count();
     if(index1.data(Qt::CheckStateRole).toInt() == 2){
         for(int i=0;i<bar_list->size();i++){
            bar_list->at(i)->setLabelColor(QColor("balck"));
         }
     }else {
         for(int i=0;i<bar_list->size();i++){
            bar_list->at(i)->setLabelColor(QColor(0,0,0,0));
         }
     }


}

void Act_log_analysor::hover_item(bool flag, int data1, QBarSet *barset)
{
    if(flag){
        QString label_text = QString("unit_name = %1,time = %2,process_time= %3,recipe info = %4")
                .arg(barset->property("tag").toString()).arg(barset->property("value_data").toString())
                .arg(barset->property("process_time").toString()).arg(barset->property("recipeinfo").toString());
       ui->text_label->setText(label_text);
    }else {
        ui->text_label->setText("");
    }

}



void Act_log_analysor::on_zoom_reset_clicked()
{
    main_chart->zoomReset();
}
