#ifndef SHINCRON_LOG_VIEWER_H
#define SHINCRON_LOG_VIEWER_H

#include <QWidget>
#include <QCloseEvent>

namespace Ui {
class shincron_log_viewer;
}

class shincron_log_viewer : public QWidget
{
    Q_OBJECT

public:
    explicit shincron_log_viewer(QWidget *chartview, QWidget *return_view, QWidget *parent = 0);
    QWidget *chartview;
    QWidget *return_view;
    ~shincron_log_viewer();

private:
    Ui::shincron_log_viewer *ui;
    void closeEvent(QCloseEvent *event);
};

#endif // SHINCRON_LOG_VIEWER_H
