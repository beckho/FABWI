#ifndef ACT_LOG_CHARTVIEW_H
#define ACT_LOG_CHARTVIEW_H

#include <QObject>
#include <QWidget>
#include <QtCharts/QChartView>
#include <QtWidgets/QRubberBand>
#include <QDebug>
#include <QPointF>
QT_CHARTS_USE_NAMESPACE

class act_log_chartview : public QChartView
{
    Q_OBJECT
public:
    act_log_chartview(QChart *chart, QWidget *parent = 0);
signals :
    void move_value(QPointF value);
protected:
    void keyPressEvent(QKeyEvent *event);

};

#endif // ACT_LOG_CHARTVIEW_H
