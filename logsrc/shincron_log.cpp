#include "shincron_log.h"
#include "ui_shincron_log.h"

Shincron_log::Shincron_log(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Shincron_log)
{
    ui->setupUi(this);
    db_name = QString("local_DB_OI_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    light_db = QSqlDatabase::addDatabase("QSQLITE",db_name);
    light_db.setDatabaseName(":memory:");
    if(!light_db.open()){
        qDebug()<<light_db.lastError().text();
    }else {
        qDebug()<<"open";
    }
    sqlquery_model = new QSqlQueryModel();
    ui->file_list_view->setModel(sqlquery_model);

    layer1_chart = new QChart();
    layer1_chart_view = new Shincron_chartview(layer1_chart);
    layer1_chart->setTitle("layer1(Cr)");
    QFont title_font = layer1_chart->titleFont();
    title_font.setPointSize(15);
    layer1_chart->setTitleFont(title_font);
    ui->layer1_chart_layout->addWidget(layer1_chart_view);

    layer2_chart = new QChart();
    layer2_chart_view = new Shincron_chartview(layer2_chart);
    layer2_chart->setTitle("layer2(Ag)");
    layer2_chart->setTitleFont(title_font);
    ui->layer2_chart_layout->addWidget(layer2_chart_view);

    layer3_chart = new QChart();
    layer3_chart_view = new Shincron_chartview(layer3_chart);
    layer3_chart->setTitle("layer3(Cu)");
    layer3_chart->setTitleFont(title_font);
    ui->layer3_chart_layout->addWidget(layer3_chart_view);

    layer4_chart = new QChart();
    layer4_chart_view = new Shincron_chartview(layer4_chart);
    layer4_chart->setTitle("layer4(Cr)");
    layer4_chart->setTitleFont(title_font);
    ui->layer4_chart_layout->addWidget(layer4_chart_view);


    QList <int> sizes;
    sizes.append(200);
    sizes.append(400);
    sizes.append(400);
    sizes.append(400);
    sizes.append(400);
    ui->splitter->setSizes(sizes);

}

void Shincron_log::dropEvent(QDropEvent *event)
{
    QString file_name;
    const QMimeData *source =  event->mimeData();


    if(source->hasUrls()){
        analysor(light_db,source->urls());
    }
}

void Shincron_log::analysor(QSqlDatabase light_db,QList<QUrl> urls)
{
    QSqlQuery query(light_db);
    query.exec("CREATE TABLE IF NOT EXISTS [shinecron_table]( "
               "[File_name] tEXT, "
               "[File_time] DATETIME, "
               "[Data_time] INT, "
               "[Film] doUBLE, "
               "[Rate] DOUBLE, "
               "[Depostion] DOUBLE, "
               "[Presure] DOUBLE, "
               "[Layer] INT);");
    query.exec("CREATE INDEX IF NOT EXISTS [shinecron_index1] "
               "ON [shinecron_table]("
                   "[File_name] COLLATE [BINARY] ASC, "
                   "[Layer] COLLATE [BINARY] ASC);");

    ui->progressBar->setRange(0,urls.count());

    for(int i=0;i<urls.count();i++){
        light_db.transaction();
       QFile source_file(urls.at(i).toLocalFile());
       if(!source_file.open(QIODevice::ReadOnly | QIODevice::Text)){
           qDebug()<<"File open error";
       }else {
           qDebug()<<"File open";
       }
       QStringList source_list;


       int Layer =0;
       while(!source_file.atEnd())
       {
           QString read_line = QString(source_file.readLine());


           if(read_line.indexOf("Executin layer")>=0){
               Layer =  QString("%1").arg(read_line.at(17)).toInt();
           }else if(read_line.indexOf("LOTID")>=0){

           }else if(read_line.indexOf("CASSETTEID")>=0){

           }else if(read_line.indexOf("\n")==0){

           }else if(read_line.indexOf("Time[sec]")>=0){

           }else {
               QFileInfo fileinfo(source_file);

               read_line = read_line.replace("\n","");
               QStringList split_data = read_line.split("\t");
               int data_time = QString("%1").arg(split_data.at(0)).trimmed().toInt();
               double Film = QString("%1").arg(split_data.at(1)).trimmed().toDouble();
               QString Film_str = QString("%1").arg(Film,0,'f',10);
               double Rate = QString("%1").arg(split_data.at(2)).trimmed().toDouble();
               QString Rate_str = QString("%1").arg(Rate,0,'f',10);
               double Depostion = QString("%1").arg(split_data.at(3)).trimmed().toDouble();
               QString Depostion_str = QString("%1").arg(Depostion,0,'f',10);
               double Presstion = QString("%1").arg(split_data.at(4)).trimmed().toDouble();
               QString presstion_str= QString("%1").arg(Presstion,0,'f',10);
               QString query_txt = QString("insert into shinecron_table "
                                           "('File_name','File_time','Data_time','Film','Rate','Depostion','Presure','Layer')"
                                           " VALUES('%1','%2',%3,%4,%5,%6,%7,%8);")
                       .arg(urls.at(i).fileName()).arg(fileinfo.lastModified().toString("yyyy-MM-dd hh:mm:ss"))
                       .arg(data_time).arg(Film_str).arg(Rate_str).arg(Depostion_str).arg(presstion_str).arg(Layer);
                query.exec(query_txt);
           }
       }
       light_db.commit();
       ui->progressBar->setValue(i+1);
    }

    sqlquery_model->setQuery("select File_name,File_time from shinecron_table group by File_name order by File_time asc",light_db);
    chart_make(light_db,1,layer1_chart,ui->CB_Film_1,ui->CB_rate_1,ui->CB_depostion_1,ui->CB_Press_1);
    chart_make(light_db,2,layer2_chart,ui->CB_Film_2,ui->CB_rate_2,ui->CB_depostion_2,ui->CB_Press_2);
    chart_make(light_db,3,layer3_chart,ui->CB_Film_3,ui->CB_rate_3,ui->CB_depostion_3,ui->CB_Press_3);
    chart_make(light_db,4,layer4_chart,ui->CB_Film_4,ui->CB_rate_4,ui->CB_depostion_4,ui->CB_Press_4);
    ui->CB_layer1_view->setChecked(true);
    ui->CB_layer2_view->setChecked(true);
    ui->CB_layer3_view->setChecked(true);
    ui->CB_layer4_view->setChecked(true);




}

Shincron_log::~Shincron_log()
{
    delete ui;
}

void Shincron_log::chart_make(QSqlDatabase light_db, int layer, QChart *layer_chart, QCheckBox *Film, QCheckBox *Rate, QCheckBox *Depostion, QCheckBox *press)
{

    QSqlQuery query(light_db);
    QSqlQuery query1(light_db);
    QSqlQuery query2(light_db);
    QList<QVariant> xpointList;

    query.exec("select File_name,File_time from shinecron_table group by File_name order by File_time asc");
    QCategoryAxis *axisX = new QCategoryAxis();
    layer_chart->addAxis(axisX,Qt::AlignBottom);
    QValueAxis *axisY = new QValueAxis();
    layer_chart->addAxis(axisY,Qt::AlignLeft);
    int max_rowcount=0;
    Film_line[layer] = new QLineSeries();

    Rate_line[layer] = new QLineSeries();

    Depostion_line[layer] = new QLineSeries();

    press_line[layer] = new QLineSeries();


    while(query.next()){

        QString query_txt = QString("select * from shinecron_table where File_name = '%1' AND Layer = %2 order by Data_time asc ").arg(query.value("File_name").toString()).arg(layer);

        query1.exec(query_txt);
        int row_count = 0;
        while(query1.next()){
            Film_line[layer]->append(max_rowcount+query1.value("Data_time").toInt(),query1.value("Film").toDouble());

            Rate_line[layer]->append(max_rowcount+query1.value("Data_time").toInt(),query1.value("Rate").toDouble());

            Depostion_line[layer]->append(max_rowcount+query1.value("Data_time").toInt(),query1.value("Depostion").toDouble());

            press_line[layer]->append(max_rowcount+query1.value("Data_time").toInt(),query1.value("Presure").toDouble());

            xpointList.append(query1.value("Data_time").toString());
            row_count++;
        }


        max_rowcount+=row_count;
        axisX->append(query.value("File_name").toString(),max_rowcount);
        qDebug()<<query.value("File_name").toString();

    }
    layer_chart->addSeries(Film_line[layer]);
    layer_chart->addSeries(Rate_line[layer]);
    layer_chart->addSeries(Depostion_line[layer]);
    layer_chart->addSeries(press_line[layer]);

    Film_line[layer]->attachAxis(axisX);
    Rate_line[layer]->attachAxis(axisX);
    Depostion_line[layer]->attachAxis(axisX);
    press_line[layer]->attachAxis(axisX);

    Film_line[layer]->setProperty("tag",xpointList);
    Rate_line[layer]->setProperty("tag",xpointList);
    Depostion_line[layer]->setProperty("tag",xpointList);
    press_line[layer]->setProperty("tag",xpointList);


    Film_line[layer]->setPointLabelsFormat("(@tag,@ydouble)");
    Rate_line[layer]->setPointLabelsFormat("(@tag,@ydouble)");
    Depostion_line[layer]->setPointLabelsFormat("(@tag,@ydouble)");
    press_line[layer]->setPointLabelsFormat("(@tag,@ydouble)");
    Film_line[layer]->setName("Film");
    Rate_line[layer]->setName("Rate");
    Depostion_line[layer]->setName("Depostion");
    press_line[layer]->setName("press");


    Film_line[layer]->attachAxis(axisY);
    Film_line[layer]->setPointsVisible(true);

    Rate_line[layer]->attachAxis(axisY);
    Rate_line[layer]->setPointsVisible(true);

    Depostion_line[layer]->attachAxis(axisY);
    Depostion_line[layer]->setPointsVisible(true);

    press_line[layer]->attachAxis(axisY);
    press_line[layer]->setPointsVisible(true);

    query2.exec("select MAX(Depostion)as max_depostion from shinecron_table");
    if(query2.next()){
         axisX->setRange(0,max_rowcount);
         axisY->setRange(0,query2.value("max_depostion").toDouble());
    }

    Film->setChecked(true);
    Depostion->setChecked(true);
    press->setChecked(true);
    Rate->setChecked(true);
}

void Shincron_log::range_make(QCheckBox *Film, QCheckBox *Rate, QCheckBox *Depostion, QCheckBox *press, int layer, QChart *chart_item)
{
    double maxrange=0;
    QSqlQuery query(light_db);
    query.exec(QString("select max(Film) as Film,max(Rate) as Rate,max(Depostion) as Depostion,max(Presure) as Presure "
                       "from shinecron_table where Layer = %1").arg(layer));
    if(query.next()){
        if(Film->isChecked()){
            if(maxrange<= query.value("Film").toDouble()){
                maxrange = query.value("Film").toDouble();
            }
        }
        if(Rate->isChecked()){
            if(maxrange<= query.value("Rate").toDouble()){
                maxrange = query.value("Rate").toDouble();
            }
        }
        if(Depostion->isChecked()){
            if(maxrange<= query.value("Depostion").toDouble()){
                maxrange = query.value("Depostion").toDouble();
            }
        }
        if(press->isChecked()){
            if(maxrange<= query.value("Presure").toDouble()){
                maxrange = query.value("Presure").toDouble();
            }
        }
    }
    chart_item->axisY()->setRange(0,maxrange);

}

void Shincron_log::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();
    QWidget::dragEnterEvent(event);
}

void Shincron_log::on_CB_Film_1_clicked(bool checked)
{

     Film_line[1]->setVisible(checked);
     range_make(ui->CB_Film_1,ui->CB_rate_1,ui->CB_depostion_1,ui->CB_Press_1,1,layer1_chart);

}

void Shincron_log::on_CB_rate_1_clicked(bool checked)
{
    Rate_line[1]->setVisible(checked);
    range_make(ui->CB_Film_1,ui->CB_rate_1,ui->CB_depostion_1,ui->CB_Press_1,1,layer1_chart);
}

void Shincron_log::on_CB_depostion_1_clicked(bool checked)
{
    Depostion_line[1]->setVisible(checked);
    range_make(ui->CB_Film_1,ui->CB_rate_1,ui->CB_depostion_1,ui->CB_Press_1,1,layer1_chart);
}

void Shincron_log::on_CB_Press_1_clicked(bool checked)
{
    press_line[1]->setVisible(checked);
    range_make(ui->CB_Film_1,ui->CB_rate_1,ui->CB_depostion_1,ui->CB_Press_1,1,layer1_chart);
}

void Shincron_log::on_PB_label_view_1_clicked()
{
    if(Film_line[1]->pointLabelsVisible()){
        Film_line[1]->setPointLabelsVisible(false);
        Rate_line[1]->setPointLabelsVisible(false);
        Depostion_line[1]->setPointLabelsVisible(false);
        press_line[1]->setPointLabelsVisible(false);
    }else {
        Film_line[1]->setPointLabelsVisible(true);
        Rate_line[1]->setPointLabelsVisible(true);
        Depostion_line[1]->setPointLabelsVisible(true);
        press_line[1]->setPointLabelsVisible(true);
    }

}
void Shincron_log::on_PB_zomm_init_1_clicked()
{
    layer1_chart->zoomReset();
}

void Shincron_log::on_PB_outside1_clicked()
{
    shincron_log_viewer *from = new shincron_log_viewer(ui->layer1_in_widget,ui->layer1_widget);
    from->show();
}


void Shincron_log::on_CB_Film_2_clicked(bool checked)
{
    Film_line[2]->setVisible(checked);
    range_make(ui->CB_Film_2,ui->CB_rate_2,ui->CB_depostion_2,ui->CB_Press_2,2,layer2_chart);

}

void Shincron_log::on_CB_rate_2_clicked(bool checked)
{
    Rate_line[2]->setVisible(checked);
    range_make(ui->CB_Film_2,ui->CB_rate_2,ui->CB_depostion_2,ui->CB_Press_2,2,layer2_chart);
}

void Shincron_log::on_CB_depostion_2_clicked(bool checked)
{
    Depostion_line[2]->setVisible(checked);
    range_make(ui->CB_Film_2,ui->CB_rate_2,ui->CB_depostion_2,ui->CB_Press_2,2,layer2_chart);
}

void Shincron_log::on_CB_Press_2_clicked(bool checked)
{
    press_line[2]->setVisible(checked);
    range_make(ui->CB_Film_2,ui->CB_rate_2,ui->CB_depostion_2,ui->CB_Press_2,2,layer2_chart);
}

void Shincron_log::on_PB_label_view_2_clicked()
{
    if(Film_line[2]->pointLabelsVisible()){
        Film_line[2]->setPointLabelsVisible(false);
        Rate_line[2]->setPointLabelsVisible(false);
        Depostion_line[2]->setPointLabelsVisible(false);
        press_line[2]->setPointLabelsVisible(false);
    }else {
        Film_line[2]->setPointLabelsVisible(true);
        Rate_line[2]->setPointLabelsVisible(true);
        Depostion_line[2]->setPointLabelsVisible(true);
        press_line[2]->setPointLabelsVisible(true);
    }
}

void Shincron_log::on_PB_zomm_init_2_clicked()
{
    layer2_chart->zoomReset();
}

void Shincron_log::on_PB_outside2_clicked()
{
    shincron_log_viewer *from = new shincron_log_viewer(ui->layer2_in_widget,ui->layer2_widget);
    from->show();
}

void Shincron_log::on_CB_Film_3_clicked(bool checked)
{
    Film_line[3]->setVisible(checked);
    range_make(ui->CB_Film_3,ui->CB_rate_3,ui->CB_depostion_3,ui->CB_Press_3,3,layer3_chart);
}

void Shincron_log::on_CB_rate_3_clicked(bool checked)
{
    Rate_line[3]->setVisible(checked);
    range_make(ui->CB_Film_3,ui->CB_rate_3,ui->CB_depostion_3,ui->CB_Press_3,3,layer3_chart);
}

void Shincron_log::on_CB_depostion_3_clicked(bool checked)
{
    Depostion_line[3]->setVisible(checked);
    range_make(ui->CB_Film_3,ui->CB_rate_3,ui->CB_depostion_3,ui->CB_Press_3,3,layer3_chart);
}

void Shincron_log::on_CB_Press_3_clicked(bool checked)
{
    press_line[3]->setVisible(checked);
    range_make(ui->CB_Film_3,ui->CB_rate_3,ui->CB_depostion_3,ui->CB_Press_3,3,layer3_chart);
}

void Shincron_log::on_PB_label_view_3_clicked()
{
    if(Film_line[3]->pointLabelsVisible()){
        Film_line[3]->setPointLabelsVisible(false);
        Rate_line[3]->setPointLabelsVisible(false);
        Depostion_line[3]->setPointLabelsVisible(false);
        press_line[3]->setPointLabelsVisible(false);
    }else {
        Film_line[3]->setPointLabelsVisible(true);
        Rate_line[3]->setPointLabelsVisible(true);
        Depostion_line[3]->setPointLabelsVisible(true);
        press_line[3]->setPointLabelsVisible(true);
    }
}

void Shincron_log::on_PB_zomm_init_3_clicked()
{
    layer3_chart->zoomReset();
}

void Shincron_log::on_PB_outside3_clicked()
{
    shincron_log_viewer *from = new shincron_log_viewer(ui->layer3_in_widget,ui->layer3_widget);
    from->show();
}

void Shincron_log::on_CB_Film_4_clicked(bool checked)
{
    Film_line[4]->setVisible(checked);
    range_make(ui->CB_Film_4,ui->CB_rate_4,ui->CB_depostion_4,ui->CB_Press_4,4,layer4_chart);
}

void Shincron_log::on_CB_rate_4_clicked(bool checked)
{
    Rate_line[4]->setVisible(checked);
    range_make(ui->CB_Film_4,ui->CB_rate_4,ui->CB_depostion_4,ui->CB_Press_4,4,layer4_chart);
}

void Shincron_log::on_CB_depostion_4_clicked(bool checked)
{
    Depostion_line[4]->setVisible(checked);
    range_make(ui->CB_Film_4,ui->CB_rate_4,ui->CB_depostion_4,ui->CB_Press_4,4,layer4_chart);
}

void Shincron_log::on_CB_Press_4_clicked(bool checked)
{
    press_line[4]->setVisible(checked);
    range_make(ui->CB_Film_4,ui->CB_rate_4,ui->CB_depostion_4,ui->CB_Press_4,4,layer4_chart);
}

void Shincron_log::on_PB_label_view_4_clicked()
{
    if(Film_line[4]->pointLabelsVisible()){
        Film_line[4]->setPointLabelsVisible(false);
        Rate_line[4]->setPointLabelsVisible(false);
        Depostion_line[4]->setPointLabelsVisible(false);
        press_line[4]->setPointLabelsVisible(false);
    }else {
        Film_line[4]->setPointLabelsVisible(true);
        Rate_line[4]->setPointLabelsVisible(true);
        Depostion_line[4]->setPointLabelsVisible(true);
        press_line[4]->setPointLabelsVisible(true);
    }
}

void Shincron_log::on_PB_zomm_init_4_clicked()
{
    layer4_chart->zoomReset();
}

void Shincron_log::on_PB_outside4_clicked()
{
    shincron_log_viewer *from = new shincron_log_viewer(ui->layer4_in_widget,ui->layer4_widget);
    from->show();
}

void Shincron_log::on_CB_layer1_view_clicked(bool checked)
{
    layer1_chart_view->setVisible(checked);
}

void Shincron_log::on_CB_layer2_view_clicked(bool checked)
{
    layer2_chart_view->setVisible(checked);
}

void Shincron_log::on_CB_layer3_view_clicked(bool checked)
{
    layer3_chart_view->setVisible(checked);
}

void Shincron_log::on_CB_layer4_view_clicked(bool checked)
{
    layer4_chart_view->setVisible(checked);
}
