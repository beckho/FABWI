#ifndef ACT_LOG_CHART_H
#define ACT_LOG_CHART_H

#include <QObject>
#include <QWidget>
#include <QtCharts/QChart>
#include <QLineSeries>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>

#include <QGraphicsSceneDragDropEvent>
QT_CHARTS_USE_NAMESPACE

class act_log_chart : public QChart
{
    Q_OBJECT
public:
    act_log_chart(QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0);
private:
    void dropEvent(QGraphicsSceneDragDropEvent *event);
signals:
    void drop_event_signal(QMimeData *);
};

#endif // ACT_LOG_CHART_H
