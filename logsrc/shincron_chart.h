#ifndef SHINCRON_CHART_H
#define SHINCRON_CHART_H

#include <QObject>
#include <QWidget>
#include <QtCharts/QChart>

QT_CHARTS_USE_NAMESPACE



class Shincron_chart : public QChart
{
    Q_OBJECT
public:
    Shincron_chart(QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0);
};

#endif // SHINCRON_CHART_H
