#include "input_lot_item.h"
#include "ui_input_lot_item.h"
#include "ui_input_item.h"
input_lot_item::input_lot_item(QSqlDatabase db,input_item *inputitem,QString machine_code,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::input_lot_item)
{
    ui->setupUi(this);
    this->machine_code = machine_code;
    ui->LE_Run_number->setValidator(new QIntValidator(0, 10000, this));
    ui->LE_ashing1->setValidator(new QIntValidator(0, 100000, this));
    ui->LE_ashing2->setValidator(new QIntValidator(0, 100000, this));
    ui->LE_ashing3->setValidator(new QIntValidator(0, 100000, this));
    ui->LE_wafer_count->setValidator(new QIntValidator(0, 1000, this));
    this->inputitem = inputitem;
//    if(!ms_mesdb.open()){
//        qDebug()<<"fasle";
//        qDebug()<<ms_mesdb.lastError().text();
//    }else {
//        qDebug()<<"open";
//    }
    manager = new QNetworkAccessManager(this);

}

void input_lot_item::clear_data()
{
    ui->LE_Lot_id->clear();
    ui->LE_wafer_count->clear();
    ui->LE_ashing1->clear();
    ui->LE_ashing2->clear();
    ui->LE_ashing3->clear();
    ui->LE_ashing_machine->clear();
    ui->LE_machine_type->clear();
    ui->LE_wafer_angle->clear();


}

input_lot_item::~input_lot_item()
{
    delete ui;
}

void input_lot_item::on_LE_Lot_id_editingFinished()
{
    if(ui->LE_Lot_id->text() != ""){

        //171109수정.
        QString LOT_ID = ui->LE_Lot_id->text();

//        if(query.next()){
//            ui->LE_machine_type->setText(query.value("MATERIAL_ID").toString());
//            ui->LE_ashing1->setText(query.value("VALUE1").toString());
//            ui->LE_ashing2->setText(query.value("VALUE2").toString());
//            ui->LE_ashing3->setText(query.value("VALUE3").toString());
//        }

        QUrl url("http://10.20.10.101:8282/mes/get_deposition_LOT_EDC");
        QUrlQuery  query ;
        query.addQueryItem("cassetteId",LOT_ID);
        url.setQuery(query);
        QNetworkRequest request(url);
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        QNetworkReply *netReply = manager->post(request,"");
        connect(netReply,SIGNAL(finished()),&loop,SLOT(quit()));
        loop.exec();
        QString rev = netReply->readAll();
        QJsonDocument json_doc = QJsonDocument::fromJson(rev.toLocal8Bit());
        int size = json_doc.array().size();
        foreach (QJsonValue var , json_doc.array()) {
            QString VALUE1 = var.toObject().value("VALUE1").toString();
            QString VALUE2 = var.toObject().value("VALUE2").toString();
            QString VALUE3 = var.toObject().value("VALUE3").toString();
            ui->LE_ashing1->setText(VALUE1);
            ui->LE_ashing2->setText(VALUE2);
            ui->LE_ashing3->setText(VALUE3);
            break;
        }

        QUrl url1("http://10.20.10.101:8282/mes/get_deposition_LOT_ID_History");
        QUrlQuery  query1 ;
        query1.addQueryItem("cassetteId",LOT_ID);
        url1.setQuery(query);
        QNetworkRequest request1(url1);
        request1.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        QNetworkReply *netReply1 = manager->post(request1,"");
        connect(netReply1,SIGNAL(finished()),&loop,SLOT(quit()));
        loop.exec();
        QString rev1 = netReply1->readAll();
        QJsonDocument json_doc1 = QJsonDocument::fromJson(rev1.toLocal8Bit());
        size = json_doc1.array().size();
        foreach (QJsonValue var , json_doc1.array()) {
            QString ASHING_EQUIPMENT_NAME = var.toObject().value("EQUIPMENT_NAME").toString();
            ui->LE_ashing_machine->setText(ASHING_EQUIPMENT_NAME);
            break;
        }

        QUrl url2("http://10.20.10.101:8282/mes/get_deposition_LOT_ID_Info");
        QUrlQuery  query2 ;
        query2.addQueryItem("cassetteId",LOT_ID);
        url2.setQuery(query2);
        QNetworkRequest request2(url2);
        request2.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        QNetworkReply *netReply2 = manager->post(request2,"");
        connect(netReply2,SIGNAL(finished()),&loop,SLOT(quit()));
        loop.exec();
        QString rev2 = netReply2->readAll();
        QJsonDocument json_doc2 = QJsonDocument::fromJson(rev2.toUtf8());
        size = json_doc2.array().size();
        QString MATERIAL_ID ="";
        foreach (QJsonValue var , json_doc2.array()) {
            MATERIAL_ID = var.toObject().value("MATERIAL_ID").toString();
            QString WAFER_MATERIAL_NAME = var.toObject().value("WAFER_MATERIAL_NAME").toString();
            ui->LE_machine_type->setText(MATERIAL_ID);
            ui->LE_wafer_angle->setText(WAFER_MATERIAL_NAME);
            break;
        }

        QUrl url3("http://10.20.10.101:8282/mes/getDepositionRecipe");
        QUrlQuery  query3 ;
        query3.addQueryItem("LOTID",LOT_ID);
        query3.addQueryItem("MATERIAL_ID",MATERIAL_ID);
        query3.addQueryItem("EQUIPMENTID",this->machine_code);
        if(MATERIAL_ID.length() == 0 ){
            ui->LE_ashing1->setText("");
            ui->LE_ashing2->setText("");
            ui->LE_ashing3->setText("");
            ui->LE_ashing_machine->setText("");
            ui->LE_machine_type->setText("");
            ui->LE_wafer_angle->setText("");
            ui->LE_wafer_count->setText("");
            inputitem->ui->LE_Recipe_name1->setText("");
            inputitem->ui->LE_Recipe_name2->setText("");
            inputitem->ui->LE_Recipe_name3->setText("");
            inputitem->ui->LE_Recipe_name4->setText("");
            inputitem->ui->LE_Recipe_name5->setText("");
            inputitem->ui->CB_Recipe_name1->setCurrentText("");
            inputitem->ui->CB_Recipe_name2->setCurrentText("");
            inputitem->ui->CB_Recipe_name3->setCurrentText("");
            inputitem->ui->CB_Recipe_name4->setCurrentText("");
            inputitem->ui->CB_Recipe_name5->setCurrentText("");
            return ;
        }
        url3.setQuery(query3);
        QNetworkRequest request3(url3);
        request3.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        QNetworkReply *netReply3 = manager->post(request3,"");
        connect(netReply3,SIGNAL(finished()),&loop,SLOT(quit()));
        loop.exec();
        QString rev3 = netReply3->readAll();
        QJsonDocument json_doc3 = QJsonDocument::fromJson(rev3.toUtf8());
        size = json_doc3.array().size();
        QString Recipe_NAME ="";
        foreach (QJsonValue var , json_doc3.array()) {
            Recipe_NAME = var.toObject().value("RECIPE_NAME").toString();
            break;
        }
        qDebug()<<Recipe_NAME;

        try{
            inputitem->ui->LE_recipe_number->setText(Recipe_NAME.mid(0,1));
            Recipe_NAME = Recipe_NAME.remove(0,1);

            Recipe_NAME = Recipe_NAME.replace("(","");
            Recipe_NAME = Recipe_NAME.replace(")","");
            Recipe_NAME = Recipe_NAME.replace("PAD","");
            Recipe_NAME = Recipe_NAME.replace(" ","");
            QStringList Recipe_Names = Recipe_NAME.split("/");
            if(Recipe_Names.length() > 0){
                QString Recipeitem  = Recipe_Names.at(0);
                QString Matel = Recipeitem.mid(0,2);
                QString MatelValue = Recipeitem.mid(2,Recipeitem.length());
                inputitem->ui->CB_Recipe_name1->setCurrentText(Matel);
                inputitem->ui->LE_Recipe_name1->setText(MatelValue);
            }else {
                inputitem->ui->CB_Recipe_name1->setCurrentText("");
                inputitem->ui->LE_Recipe_name1->setText("");
            }
            if(Recipe_Names.length() > 1){
                QString Recipeitem  = Recipe_Names.at(1);
                QString Matel = Recipeitem.mid(0,2);
                QString MatelValue = Recipeitem.mid(2,Recipeitem.length());
                inputitem->ui->CB_Recipe_name2->setCurrentText(Matel);
                inputitem->ui->LE_Recipe_name2->setText(MatelValue);
            }else {
                inputitem->ui->CB_Recipe_name2->setCurrentText("");
                inputitem->ui->LE_Recipe_name2->setText("");
            }
            if(Recipe_Names.length() > 2){
                QString Recipeitem  = Recipe_Names.at(2);
                QString Matel = Recipeitem.mid(0,2);
                QString MatelValue = Recipeitem.mid(2,Recipeitem.length());
                inputitem->ui->CB_Recipe_name3->setCurrentText(Matel);
                inputitem->ui->LE_Recipe_name3->setText(MatelValue);
            }else {
                inputitem->ui->CB_Recipe_name3->setCurrentText("");
                inputitem->ui->LE_Recipe_name3->setText("");
            }
            if(Recipe_Names.length() > 3){
                QString Recipeitem  = Recipe_Names.at(3);
                QString Matel = Recipeitem.mid(0,2);
                QString MatelValue = Recipeitem.mid(2,Recipeitem.length());
                inputitem->ui->CB_Recipe_name4->setCurrentText(Matel);
                inputitem->ui->LE_Recipe_name4->setText(MatelValue);
            }else {
                inputitem->ui->CB_Recipe_name4->setCurrentText("");
                inputitem->ui->LE_Recipe_name4->setText("");
            }
            if(Recipe_Names.length() > 4){
                QString Recipeitem  = Recipe_Names.at(3);
                QString Matel = Recipeitem.mid(0,2);
                QString MatelValue = Recipeitem.mid(2,Recipeitem.length());
                inputitem->ui->CB_Recipe_name5->setCurrentText(Matel);
                inputitem->ui->LE_Recipe_name5->setText(MatelValue);
            }else {
                inputitem->ui->CB_Recipe_name5->setCurrentText("");
                inputitem->ui->LE_Recipe_name5->setText("");
            }
        }catch(int exceptionCode){
            return ;
        }


        //171109수정.
//        query.exec(QString("SELECT * FROM V_LOT_HISTORY WHERE CASSETTE_ID = '%1' AND OPERATION_NAME LIKE '% %2' AND TX_CODE = 'TX_END' "
//                           ).arg(LOT_ID).arg(tr("ashing")));
//        qDebug()<<query.lastQuery();

//        if(query.next()){
//            ui->LE_ashing_machine->setText(query.value("EQUIPMENT_NAME").toString());
//        }
        //171109수정.
//        query.exec(QString("SELECT A.CASSETTE_ID,A.MATERIAL_ID,A.LOT_UDF8,B.MATERIAL_NAME "
//                           "FROM [V_NM_LOTS] A,[V_NM_MATERIALS] B  with(NOLOCK) "
//                           "where A.[CASSETTE_ID] = '%1' AND A.LOT_UDF4 = B.MATERIAL_ID AND "
//                           "B.SITE_ID = 'WOSF' AND B.DELETE_FLAG <> 'Y' AND B.MATERIAL_TYPE = 'ROH'").arg(LOT_ID));
//        if(query.next()){
//            ui->LE_wafer_angle->setText(query.value("MATERIAL_NAME").toString());
//        }
//        query.exec(QString("select CARRIER_ID,count(*) as wafer_count  from V_LOT_HISTORY with(NOLOCK) where CASSETTE_ID = '%1' AND TX_CODE = 'TX_START' "
//                           "AND EQUIPMENT_ID = '%2' group by CARRIER_ID ").arg(LOT_ID).arg(machine_code));
//        if(query.next()){

//            ui->LE_wafer_count->setText(query.value("wafer_count").toString());

//            inputitem->ui->CB_domnumber->setEditText(query.value("CARRIER_ID").toString());

//        }


//        thin_film_manager_input_lot_item send_item;
//        query.exec(QString("SELECT TOP 1 LOT_ID,COLLECTION_ID,TX_DTTM,TX_DTTM,TX_USER_NAME,OPERATION_ID,EQUIPMENT_ID,VALUE1 "
//                           "FROM [MESDB].[dbo].[V_NM_EDC_LOTS] A with(NOLOCK) where LOT_ID = '%1' AND EQUIPMENT_ID = '%2' AND CHARACTER_ID = 'PAD_RECIPE' ")
//                   .arg(ui->LE_Lot_id->text()).arg(machine_code));


//        if(query.next()){
//             QString recipe = query.value("VALUE1").toString();
//             QStringList recipe_list  = recipe.split("/");
//             for(int i=0;i<recipe_list.count();i++){
//                 QString list_item = recipe_list.at(i);
//                 send_item.recipe[i]=list_item.toInt();
//             }
//             query_re_count++;
//        }
//        query.exec(QString("SELECT TOP 1 LOT_ID,COLLECTION_ID,TX_DTTM,TX_DTTM,TX_USER_NAME,OPERATION_ID,EQUIPMENT_ID,VALUE1 "
//                           "FROM [MESDB].[dbo].[V_NM_EDC_LOTS] A with(NOLOCK) where LOT_ID = '%1' AND EQUIPMENT_ID = '%2' AND CHARACTER_ID = 'PAD_Au' ")
//                   .arg(ui->LE_Lot_id->text()).arg(machine_code));

//        if(query.next()){
//             send_item.au_f_add = query.value("VALUE1").toDouble();
//             send_item.user_name = query.value("TX_USER_NAME").toString();
//             query_re_count++;
//        }




    }
}

void input_lot_item::on_LE_wafer_count_editingFinished()
{
    emit lot_editing_finished();
}
