#include "thin_combodelegate.h"

Thin_combodelegate::Thin_combodelegate(QStringList &list, QWidget *parent): QItemDelegate(parent)
{
    this->list = list;
}

QWidget *Thin_combodelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    comboxedit = new QComboBox(parent);
    comboxedit->setEditable(true);
    for(int i=0;i<list.count();i++){
        comboxedit->addItem(list.at(i));
    }
    QObject::connect(comboxedit,SIGNAL(currentTextChanged(QString)),this,SLOT(setData(QString)));

    return comboxedit;
}

void Thin_combodelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QVariant data = index.model()->data( index, Qt::DisplayRole );

    (static_cast<QComboBox*>( editor ))->setCurrentText(data.toString());
}

void Thin_combodelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    model->setData( index, static_cast<QComboBox*>( editor )->currentText() );
}

void Thin_combodelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry( option.rect );
}

void Thin_combodelegate::setData(QString val)
{
    emit commitData(comboxedit);
}
