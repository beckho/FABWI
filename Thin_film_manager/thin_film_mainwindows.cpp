#include "thin_film_mainwindows.h"
#include "ui_thin_film_mainwindows.h"
#include "ui_spec_item.h"
#include "ui_input_item.h"
#include "ui_input_lot_item.h"
Thin_film_mainwindows::Thin_film_mainwindows(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Thin_film_mainwindows)
{
    ui->setupUi(this);
    QString msdb_name = QString("MS_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    QString mydb_name = QString("MY_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    ms_mesdb = QSqlDatabase::addDatabase("QODBC",msdb_name);
    QString serverinfo = QString("DRIVER={SQL Server};Server=%1;Database=%2;Uid=%3;Port=1433;Pwd=%4").arg(DBMESSERVERIP).arg(DBMESNAME).arg(DBMESUSERNAME).arg(DBMESPW);
    ms_mesdb.setDatabaseName(serverinfo);
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    QString server_ip = settings.value("serverip").toString();
    QString db_port = settings.value("dbport").toString();
    settings.endGroup();

    batch_item_updatelist.append("vacuum_time");
//    batch_item_updatelist.append("Pad_thin");
//    batch_item_updatelist.append("Wlp_thin");
    batch_item_updatelist.append("N2_Blow");
    batch_item_updatelist.append("Au_f_add");
    batch_item_updatelist.append("Now_r_weight");
    batch_item_updatelist.append("Au_after_weight");
    batch_item_updatelist.append("Au_use");
    batch_item_updatelist.append("Ti_add");
    batch_item_updatelist.append("Al_add");
    batch_item_updatelist.append("X_tal_number");
    batch_item_updatelist.append("X_tal_Hz");
    batch_item_updatelist.append("X_tal_life");
    batch_item_updatelist.append("User_name");
    batch_item_updatelist.append("D_use");
    batch_item_updatelist.append("Ti_total");
    batch_item_updatelist.append("Al_total");
    batch_item_updatelist.append("Au_total");
    batch_item_updatelist.append("Input_time");
    batch_item_updatelist.append("au_pellet_total");
    batch_item_updatelist.append("Input_thin_data");
    batch_item_updatelist.append("Recipe");
    note_combo_list<<tr("x-tal change");

    if(!ms_mesdb.open()){
        qDebug()<<"fasle";
        qDebug()<<ms_mesdb.lastError().text();
    }else {
        qDebug()<<"open";
    }


    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }



    QSqlQuery query(my_mesdb);
    query.exec("select * from `depostion_wet_accumulate_setting` order by machine_name asc");
    while(query.next()){
        wet_machine_list.append(query.value("machine_name").toString());
    }
    wafer_count_index = 0;
    t1_init();
    t2_init();
    t3_init();
    t4_init();
    t5_init();
    t6_init();
    t7_init();
    t13_init();
    t14_init();
    t15_init();
    t16_init();
    t8_init();


    connect(&t11_timer,SIGNAL(timeout()),this,SLOT(all_refresh_graph()));

//    temp_run_timer.setInterval(1500);
//    connect(&temp_run_timer,SIGNAL(timeout()),this,SLOT(temp_run_mode_timeout()));
//    temp_run_timer.start();
    temp_run_mode_thread = new thin_temp_runmode_th(temp_run_widget_map,temp_run_ti_accmulate_widget_map,temp_run_al_accmulate_widget_map);
    connect(temp_run_mode_thread,SIGNAL(change_gui(QString,QLineEdit*)),this,SLOT(temprunmode_gui_slot(QString,QLineEdit*)));
    temp_run_mode_thread->start();
}

//t1_function -- start --
void Thin_film_mainwindows::t1_init()
{
    ui->t1_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t1_search_time_start->setDateTime(ui->t1_search_time_end->dateTime().addDays(-2));

    QSqlTableModel *spec_model = new QSqlTableModel(this,my_mesdb);
    spec_model->setTable("Thin_film_spec_managerment");
    spec_model->setFilter("machine_code = 'MD002'");
    spec_model->select();
    spec_model->setHeaderData(4,Qt::Horizontal,tr("ashing_LCL"));
    spec_model->setHeaderData(5,Qt::Horizontal,tr("ashing_UCL"));
    spec_model->setHeaderData(6,Qt::Horizontal,tr("WLPTiAlashing_LCL"));
    spec_model->setHeaderData(7,Qt::Horizontal,tr("WLPTiAlashing_UCL"));
    spec_model->setHeaderData(8,Qt::Horizontal,tr("WLP_LCL_Ti_Au"));
    spec_model->setHeaderData(9,Qt::Horizontal,tr("WLP_UCL_Ti_Au"));
    spec_model->setHeaderData(10,Qt::Horizontal,tr("WLP_LCL_Ti_Al"));
    spec_model->setHeaderData(11,Qt::Horizontal,tr("WLP_UCL_Ti_Al"));
    spec_model->setEditStrategy(QSqlTableModel::OnManualSubmit);

    t1_data_model = new thin_sqltable_model(this,my_mesdb,"MD002",spec_model);
//    QSqlTableModel *data_model = new QSqlTableModel(this,my_mesdb);
    t1_data_model->setTable("Thin_film_data");

    t1_data_model->setFilter(QString("Input_time Between '%1' AND '%2' AND machine_code = 'MD002'")
                          .arg(ui->t1_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t1_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    ui->t1_data_table->setItemDelegateForColumn(t1_data_model->fieldIndex("vacuum_time"),new DateDelegate(ui->t1_data_table));
    ui->t1_data_table->setItemDelegateForColumn(t1_data_model->fieldIndex("Input_time"),new DateDelegate(ui->t1_data_table));
    ui->t1_data_table->setModel(t1_data_model);
    ui->t1_data_table->setSortingEnabled(true);
    connect(t1_data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>))
            ,this,SLOT(t1_datamodel_dataChanged(QModelIndex,QModelIndex,QVector<int>)));

    ui->t1_data_table->setItemDelegateForColumn(10,new Thin_combodelegate(note_combo_list,ui->t1_data_table));
    t1_data_model->setEditStrategy(QSqlTableModel::OnFieldChange);
    t1_data_model->setSort(2,Qt::AscendingOrder);

    t1_data_model->select();

    wafer_count_index = t1_data_model->fieldIndex("Wafer_count");

    t1_data_model->setHeaderData(t1_data_model->fieldIndex("day_and_night"),Qt::Horizontal,tr("day_and_night"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("vacuum_time"),Qt::Horizontal,tr("vacuum_time"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Run_number"),Qt::Horizontal,tr("thin_Run_number"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Resister"),Qt::Horizontal,tr("thin_Resister"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("machine_type"),Qt::Horizontal,tr("thin_machine_type"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Lot_id"),Qt::Horizontal,tr("thin_Lot_id"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Wafer_count"),Qt::Horizontal,tr("thin_Wafer_count"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Pad_thin"),Qt::Horizontal,tr("thin_Pad_thin"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Wlp_thin"),Qt::Horizontal,tr("thin_Wlp_thin"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Wafer_angle"),Qt::Horizontal,tr("thin_Wafer_angle"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Input_thin_data"),Qt::Horizontal,tr("thin_Input_thin_data"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Recipe"),Qt::Horizontal,tr("thin_Recipe"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("ashing1"),Qt::Horizontal,tr("thin_ashing1"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("ashing2"),Qt::Horizontal,tr("thin_ashing2"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("ashing3"),Qt::Horizontal,tr("thin_ashing3"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("ashing_machine"),Qt::Horizontal,tr("thin_ashing_machine"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("N2_Blow"),Qt::Horizontal,tr("thin_N2_Blow"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Au_f_add"),Qt::Horizontal,tr("thin_thin_Au_f_add"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Now_r_weight"),Qt::Horizontal,tr("thin_Now_r_weight"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Au_after_weight"),Qt::Horizontal,tr("thin_Au_after_weight"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Au_use"),Qt::Horizontal,tr("thin_Au_use"));
//    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Ti_add"),Qt::Horizontal,tr("thin_Ti_add"));
//    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Al_add"),Qt::Horizontal,tr("thin_Al_add"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("X_tal_number"),Qt::Horizontal,tr("thin_X_tal_number"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("X_tal_Hz"),Qt::Horizontal,tr("thin_X_tal_Hz"));
//    t1_data_model->setHeaderData(t1_data_model->fieldIndex("X_tal_life"),Qt::Horizontal,tr("thin_X_tal_life"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("User_name"),Qt::Horizontal,tr("thin_User_name"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("D_use"),Qt::Horizontal,tr("thin_D_use"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("note"),Qt::Horizontal,tr("thin_note"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Ti_total"),Qt::Horizontal,tr("thin_Ti_total"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Al_total"),Qt::Horizontal,tr("thin_Al_total"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Au_total"),Qt::Horizontal,tr("thin_Au_total"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("Input_time"),Qt::Horizontal,tr("thin_Input_time"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("machine_name"),Qt::Horizontal,tr("thin_machine_name"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("machine_code"),Qt::Horizontal,tr("thin_machine_code"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("batch_id"),Qt::Horizontal,tr("thin_batch_id"));
    t1_data_model->setHeaderData(t1_data_model->fieldIndex("cleaning_cal_flag"),Qt::Horizontal,tr("thin_cleaning_cal_flag"));
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("day_and_night"),50);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("vacuum_time"),110);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Run_number"),60);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Resister"),50);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("machine_type"),100);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Lot_id"),80);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Wafer_count"),50);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Pad_thin"),80);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Wlp_thin"),80);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Wafer_angle"),80);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Input_thin_data"),150);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Recipe"),150);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("ashing1"),65);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("ashing2"),65);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("ashing3"),65);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("ashing_machine"),80);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("N2_Blow"),80);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Au_f_add"),80);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Now_r_weight"),120);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Au_after_weight"),140);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Au_use"),50);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Ti_add"),50);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Al_add"),50);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("X_tal_number"),50);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("X_tal_Hz"),50);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("X_tal_life"),80);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("User_name"),80);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("D_use"),80);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("note"),140);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Ti_total"),60);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Al_total"),60);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Au_total"),60);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("Input_time"),120);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("machine_name"),50);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("machine_code"),50);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("batch_id"),50);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("cleaning_cal_flag"),50);
    ui->t1_data_table->horizontalHeader()->resizeSection(t1_data_model->fieldIndex("dom"),50);


    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("X_tal_number"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("X_tal_Hz"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("machine_code"));
//    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("batch_id"));
//    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("cleaning_cal_flag"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("Au_use"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("au_pellet_output"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("au_add"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("check_user"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("au_pellet_total"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("work_start_au"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("au_add_weight"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("au_case_weight"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("check_NG"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("Run_number"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("Pad_thin"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("Wlp_thin"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("N2_Blow"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("D_use"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("Ti_add"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("Al_add"));
    ui->t1_data_table->horizontalHeader()->hideSection(t1_data_model->fieldIndex("X_tal_life"));
//    ui->t1_data_table->horizontalHeader()->resizeSection(0,50);
//    ui->t1_data_table->horizontalHeader()->resizeSection(1,150);
//    ui->t1_data_table->horizontalHeader()->resizeSection(6,120);
//    ui->t1_data_table->horizontalHeader()->resizeSection(12,160);
    ui->t1_data_table->scrollToBottom();



    ui->t1_spec_table->setModel(spec_model);
    ui->t1_spec_table->horizontalHeader()->hideSection(0);
    ui->t1_spec_table->horizontalHeader()->hideSection(1);
    ui->t1_spec_table->horizontalHeader()->hideSection(2);
    ui->t1_spec_table->horizontalHeader()->hideSection(3);
    ui->t1_spec_table->horizontalHeader()->hideSection(4);
    ui->t1_spec_table->horizontalHeader()->hideSection(5);
    ui->t1_spec_table->horizontalHeader()->hideSection(6);
    ui->t1_spec_table->horizontalHeader()->hideSection(7);
    ui->t1_spec_table->horizontalHeader()->hideSection(8);
    ui->t1_spec_table->horizontalHeader()->hideSection(9);
    ui->t1_spec_table->horizontalHeader()->hideSection(10);
    ui->t1_spec_table->horizontalHeader()->hideSection(11);
    ui->t1_spec_table->horizontalHeader()->hideSection(12);
    ui->t1_spec_table->horizontalHeader()->hideSection(13);
    ui->t1_spec_table->horizontalHeader()->hideSection(14);
    ui->t1_spec_table->horizontalHeader()->hideSection(17);
    ui->t1_spec_table->horizontalHeader()->hideSection(18);
    ui->t1_spec_table->horizontalHeader()->hideSection(19);
    ui->t1_spec_table->horizontalHeader()->hideSection(20);
    ui->t1_spec_table->horizontalHeader()->hideSection(21);
    ui->t1_spec_table->horizontalHeader()->hideSection(22);
    ui->t1_spec_table->horizontalHeader()->hideSection(23);
    ui->t1_spec_table->horizontalHeader()->hideSection(24);
    ui->t1_spec_table->horizontalHeader()->hideSection(25);
    ui->t1_spec_table->horizontalHeader()->hideSection(26);
    ui->t1_spec_table->horizontalHeader()->hideSection(27);
    ui->t1_spec_table->horizontalHeader()->hideSection(28);
    ui->t1_spec_table->horizontalHeader()->hideSection(29);
    ui->t1_spec_table->horizontalHeader()->hideSection(30);
    ui->t1_spec_table->horizontalHeader()->hideSection(31);
    ui->t1_spec_table->horizontalHeader()->hideSection(32);
    ui->t1_spec_table->horizontalHeader()->hideSection(33);
    ui->t1_spec_table->horizontalHeader()->hideSection(34);
    ui->t1_spec_table->horizontalHeader()->hideSection(35);
    ui->t1_spec_table->horizontalHeader()->resizeSection(6,150);
    ui->t1_spec_table->horizontalHeader()->resizeSection(7,150);

    for(int i=0;i<8;i++){
        ui->t1_input_table->insertColumn(i);
    }
    ui->t1_input_table->insertRow(0);
    ui->t1_input_table->insertRow(1);
    ui->t1_input_table->insertRow(2);
    ui->t1_input_table->insertRow(3);
    ui->t1_input_table->insertRow(4);
    ui->t1_input_table->insertRow(5);
    ui->t1_input_table->insertRow(6);
    ui->t1_input_table->horizontalHeader()->resizeSection(0,70);
    ui->t1_input_table->horizontalHeader()->resizeSection(1,150);
    ui->t1_input_table->horizontalHeader()->resizeSection(3,70);
    ui->t1_input_table->horizontalHeader()->resizeSection(5,130);
    ui->t1_input_table->horizontalHeader()->resizeSection(6,140);
    ui->t1_input_table->horizontalHeader()->resizeSection(7,80);
    t1_input_item = new input_item(my_mesdb,"MD002");
    ui->t1_input_table->setCellWidget(0,0,t1_input_item->ui->LA_day_and_night);
    ui->t1_input_table->setCellWidget(1,0,t1_input_item->ui->CB_day_and_night);
    ui->t1_input_table->setCellWidget(0,1,t1_input_item->ui->LA_vacuum_time);
    ui->t1_input_table->setCellWidget(1,1,t1_input_item->ui->DT_vacuum_time);
    t1_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
    ui->t1_input_table->setCellWidget(0,2,t1_input_item->ui->LA_resister);
    ui->t1_input_table->setCellWidget(1,2,t1_input_item->ui->LE_resister);
//    ui->t1_input_table->setCellWidget(0,3,t1_input_item->ui->LA_N2_blow);
//    ui->t1_input_table->setCellWidget(1,3,t1_input_item->ui->WCB_N2_blow);
    ui->t1_input_table->setCellWidget(0,4,t1_input_item->ui->LA_au_f_add);
    ui->t1_input_table->setCellWidget(1,4,t1_input_item->ui->LE_au_f_add);
    ui->t1_input_table->setCellWidget(0,5,t1_input_item->ui->LA_now_r_weight);
    ui->t1_input_table->setCellWidget(1,5,t1_input_item->ui->LE_now_r_weight);
    ui->t1_input_table->setCellWidget(0,6,t1_input_item->ui->LA_au_after_r_weight);
    ui->t1_input_table->setCellWidget(1,6,t1_input_item->ui->LE_au_after_r_weight);
    ui->t1_input_table->setCellWidget(0,7,t1_input_item->ui->LA_au_after_weight);
    ui->t1_input_table->setCellWidget(1,7,t1_input_item->ui->LE_au_after_weight);
//    ui->t1_input_table->setCellWidget(0,7,t1_input_item->ui->LA_au_use);
//    ui->t1_input_table->setCellWidget(1,7,t1_input_item->ui->LE_au_use);
//    ui->t1_input_table->setCellWidget(2,0,t1_input_item->ui->LA_ti_add);
//    ui->t1_input_table->setCellWidget(3,0,t1_input_item->ui->LE_ti_add);
//    ui->t1_input_table->setCellWidget(2,1,t1_input_item->ui->LA_al_add);
//    ui->t1_input_table->setCellWidget(3,1,t1_input_item->ui->LE_al_add);
//    ui->t1_input_table->setCellWidget(2,2,t1_input_item->ui->LA_xtal_life);
//    ui->t1_input_table->setCellWidget(3,2,t1_input_item->ui->LE_xtal_life);
    ui->t1_input_table->setCellWidget(2,3,t1_input_item->ui->LA_name);
    ui->t1_input_table->setCellWidget(3,3,t1_input_item->ui->LE_name);
//    ui->t1_input_table->setCellWidget(2,4,t1_input_item->ui->LA_d_run);
//    ui->t1_input_table->setCellWidget(3,4,t1_input_item->ui->WCB_d_run);
    ui->t1_input_table->setCellWidget(2,5,t1_input_item->ui->LA_note);
    ui->t1_input_table->setCellWidget(3,5,t1_input_item->ui->LE_note);
    ui->t1_input_table->setSpan(2,5,1,5);
    ui->t1_input_table->setSpan(3,5,1,5);
    ui->t1_input_table->setCellWidget(4,0,t1_input_item->ui->LE_recipe_number);
    ui->t1_input_table->setCellWidget(4,1,t1_input_item->ui->CB_recipe_choice);
//    ui->t1_input_table->setCellWidget(4,3,t1_input_item->ui->LA_recipe_2);
    ui->t1_input_table->setCellWidget(5,0,t1_input_item->ui->W_recipe);
    ui->t1_input_table->setSpan(4,1,1,5);
//    ui->t1_input_table->setSpan(4,3,1,2);
    ui->t1_input_table->setSpan(5,0,2,5);

    ui->t1_input_table->setCellWidget(4,6,t1_input_item->ui->LA_dom_number);
    ui->t1_input_table->setCellWidget(5,6,t1_input_item->ui->CB_domnumber);

    ui->t1_LE_Lot_count->setValidator(new QIntValidator(0, 1000, this));
//    ui->t1_LE_Run_number->setValidator(new QIntValidator(0, 1000, this));
    QSqlQuery query(my_mesdb);
    query.exec(QString("select `Lot_max_count`,`Run_number_count` from Thin_film_spec_managerment where machine_code = 'MD002'"));
    if(query.next()){
        ui->t1_LE_Lot_count->setText(query.value("Lot_max_count").toString());
//        ui->t1_LE_Run_number->setText(query.value("Run_number_count").toString());
    }
    t1_lot_item_list.clear();
    for(int i=0;i<ui->t1_LE_Lot_count->text().toInt();i++){

        input_lot_item *lot_item = new input_lot_item(ms_mesdb,t1_input_item,"MD002");
        t1_lot_item_list.append(lot_item);
        ui->t1_input_lot_table->insertRow(i);
        ui->t1_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
        ui->t1_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
        ui->t1_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
        ui->t1_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
        ui->t1_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
        ui->t1_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
        ui->t1_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//        connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                t1_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
    }
    query.exec("SELECT * FROM `Thin_film_repcipe` where machine_code = 'MD002' ORDER BY `recipe_number` ASC  ");
    t1_input_item->ui->CB_recipe_choice->addItem(tr("recipe_choice"));
    while(query.next()){
        QString recipe_name = query.value("recipe_number").toString()+"."+ query.value("recipename").toString()
                                + "|"+query.value("metal_name").toString()+"|"+query.value("metal_thin").toString()+"|"
                                +query.value("type").toString();
        QPixmap pixmap(100,100);
        pixmap.fill(QColor(query.value("color").toString()));
        t1_input_item->ui->CB_recipe_choice->addItem(QIcon(pixmap),recipe_name);
    }
    connect(t1_input_item->ui->CB_recipe_choice,SIGNAL(currentIndexChanged(int)),this,SLOT(t1_recipe_choice(int)));

    t1_ti_chart = new Thin_film_chart();
    t1_ti_chartview = new Thin_film_chart_view(t1_ti_chart);
    connect(t1_ti_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t1_ti_y_value_slot(QPointF)));
    ui->t1_Ti_layout->addWidget(t1_ti_chartview);

    t1_al_chart = new Thin_film_chart();
    t1_al_chartview = new Thin_film_chart_view(t1_al_chart);
    connect(t1_al_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t1_al_y_value_slot(QPointF)));
    ui->t1_Al_layout->addWidget(t1_al_chartview);
    t1_ti_axisX = new QDateTimeAxis();
    t1_ti_axisX->setFormat("MM-dd HH:mm:ss");
    t1_ti_axisX->setTitleText("Date");
    t1_ti_axisX->setVisible(true);
    t1_ti_axisX->setTitleVisible(true);
    t1_ti_chart->addAxis(t1_ti_axisX, Qt::AlignBottom);
    t1_ti_axisY = new QValueAxis();
    t1_ti_chart->addAxis(t1_ti_axisY,Qt::AlignLeft);

    t1_al_axisX = new QDateTimeAxis();
    t1_al_axisX->setFormat("MM-dd HH:mm:ss");
    t1_al_axisX->setTitleText("Date");
    t1_al_axisX->setVisible(true);
    t1_al_axisX->setTitleVisible(true);
    t1_al_chart->addAxis(t1_al_axisX, Qt::AlignBottom);
    t1_al_axisY = new QValueAxis();
    t1_al_chart->addAxis(t1_al_axisY,Qt::AlignLeft);

    ti_al_chart_init(t1_ti_chart,t1_al_chart,t1_ti_value_series,t1_al_value_series,t1_ti_UCL_value_series,t1_al_UCL_value_series,
                     t1_al_axisX,t1_al_axisY,t1_ti_axisX,t1_ti_axisY,"MD002");


    query.exec("SELECT machine_name,machine_code FROM Thin_film_spec_managerment");
    while(query.next()){
        ui->t1_CB_move_machine->addItem(query.value("machine_name").toString());
    }
    temp_run_widget_map.insert("MD002",ui->t1_temp_run_mode_time);
    temp_run_ti_accmulate_widget_map.insert("MD002",ui->t1_ti_accmulate);
    temp_run_al_accmulate_widget_map.insert("MD002",ui->t1_al_accmulate);

    sync_now_date("MD002",t1_input_item->ui->LE_au_f_add,t1_input_item->ui->LE_now_r_weight,t1_input_item->ui->LE_au_after_r_weight
                  ,t1_input_item->ui->LE_au_after_weight);

    ui->t1_data_table->selectionModel()->setObjectName("t1_data");
    connect(ui->t1_data_table->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this,SLOT(table_view_selection_change(QModelIndex,QModelIndex)));


}
void Thin_film_mainwindows::t1_recipe_choice(int idx)
{
    if(idx == 0){
        t1_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t1_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t1_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t1_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t1_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t1_input_item->ui->LE_Recipe_name1->setText("");
        t1_input_item->ui->LE_Recipe_name2->setText("");
        t1_input_item->ui->LE_Recipe_name3->setText("");
        t1_input_item->ui->LE_Recipe_name4->setText("");
        t1_input_item->ui->LE_Recipe_name5->setText("");
        return ;
    }
    QSqlQuery query(my_mesdb);
    query.exec(QString("select * from `Thin_film_repcipe` where recipe_number  = %1 AND machine_code = 'MD002'").arg(idx));

    if(query.next()){
        t1_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t1_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t1_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t1_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t1_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t1_input_item->ui->LE_Recipe_name1->setText("");
        t1_input_item->ui->LE_Recipe_name2->setText("");
        t1_input_item->ui->LE_Recipe_name3->setText("");
        t1_input_item->ui->LE_Recipe_name4->setText("");
        t1_input_item->ui->LE_Recipe_name5->setText("");
        if(query.value("metal_name").toString() != ""){
            QString item_data = query.value("metal_name").toString();
            QStringList item_list = item_data.split("/");
            for(int i=0;i<item_list.count();i++){
                if(i==0){
                    t1_input_item->ui->CB_Recipe_name1->setCurrentText(item_list.at(i));
                }else if(i==1){
                    t1_input_item->ui->CB_Recipe_name2->setCurrentText(item_list.at(i));
                }else if(i==2){
                    t1_input_item->ui->CB_Recipe_name3->setCurrentText(item_list.at(i));
                }else if(i==3){
                    t1_input_item->ui->CB_Recipe_name4->setCurrentText(item_list.at(i));
                }else if(i==4){
                    t1_input_item->ui->CB_Recipe_name5->setCurrentText(item_list.at(i));
                }
            }
        }
        if(query.value("metal_thin").toString() != ""){
            QStringList recipe_item = query.value("metal_thin").toString().split("/");
            if(recipe_item.count()>0){
                t1_input_item->ui->LE_Recipe_name1->setText(QString("%1").arg(recipe_item[0]));
            }
            if(recipe_item.count()>1){
                t1_input_item->ui->LE_Recipe_name2->setText(QString("%1").arg(recipe_item[1]));
            }
            if(recipe_item.count()>2){
                t1_input_item->ui->LE_Recipe_name3->setText(QString("%1").arg(recipe_item[2]));
            }
            if(recipe_item.count()>3){
                t1_input_item->ui->LE_Recipe_name4->setText(QString("%1").arg(recipe_item[3]));
            }
            if(recipe_item.count()>4){
                t1_input_item->ui->LE_Recipe_name5->setText(QString("%1").arg(recipe_item[4]));
            }
        }
    }
}
void Thin_film_mainwindows::t1_ti_y_value_slot(QPointF value)
{
    ui->t1_ti_y_value->setText(QString("%1").arg(value.y()));
}
void Thin_film_mainwindows::t1_al_y_value_slot(QPointF value)
{
    ui->t1_al_y_value->setText(QString("%1").arg(value.y()));
}
void Thin_film_mainwindows::t1_datamodel_dataChanged(QModelIndex index1, QModelIndex index2, QVector<int> vector)
{
    v1_data_model_change(index1,t1_data_model,"MD002");
    ui->t1_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t1_LE_Lot_count_editingFinished()
{
//    if(ui->t1_LE_Lot_count->text()!= ""){
//        QSqlQuery query(my_mesdb);
//        query.exec(QString("update `Thin_film_spec_managerment` SET `Lot_max_count`='%1' Where `machine_code`='MD002'").arg(ui->t1_LE_Lot_count->text()));
//        while(ui->t1_input_lot_table->rowCount()>0){
//            ui->t1_input_lot_table->removeRow(0);
//        }
//        t1_lot_item_list.clear();
//        for(int i=0;i<ui->t1_LE_Lot_count->text().toInt();i++){

//            input_lot_item *lot_item = new input_lot_item(ms_mesdb,"MD002");
//            t1_lot_item_list.append(lot_item);
//            ui->t1_input_lot_table->insertRow(i);
//            ui->t1_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//    //        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
//            ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
//            ui->t1_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
//            ui->t1_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
//            ui->t1_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
//            ui->t1_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
//            ui->t1_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
//            ui->t1_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//            connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                    t1_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
//            connect(lot_item,SIGNAL(lot_editing_finished()),this,SLOT(t1_lot_edit_finish()));

//        }
//    }
}
void Thin_film_mainwindows::on_t1_LE_Run_number_editingFinished()
{
//    if(ui->t1_LE_Run_number->text() != ""){
//        QSqlQuery query(my_mesdb);
//        query.exec(QString("update `Thin_film_spec_managerment` SET `Run_number_count`='%1' Where `machine_code`='MD002'").arg(ui->t1_LE_Run_number->text()));
//    }
}
void Thin_film_mainwindows::on_t1_Input_btn_clicked()
{
//    QString Run_number = ui->t1_LE_Run_number->text();
//    if(Run_number.toInt() > 60){
//        ui->t1_LE_Run_number->setText("1");
//        on_t1_LE_Run_number_editingFinished();
//        QMessageBox msg;
//        msg.addButton(QMessageBox::Ok);
//        msg.setText(tr("60 over set 1"));
//        msg.exec();
//    }
    QString Lot_count = ui->t1_LE_Lot_count->text();
    QString resister = t1_input_item->ui->LE_resister->text();
//    QString N2_blow = QString("%1").arg(QVariant(t1_input_item->ui->CB_N2_blow->isChecked()).toInt());
    QString au_f_add = t1_input_item->ui->LE_au_f_add->text();
    QString now_r_weight = t1_input_item->ui->LE_now_r_weight->text();
    QString au_after_r_weight = t1_input_item->ui->LE_au_after_r_weight->text();
    QString au_use =  t1_input_item->ui->LE_au_use->text();
    QString ti_add =  t1_input_item->ui->LE_ti_add->text();
    QString al_add =  t1_input_item->ui->LE_al_add->text();
    QString xtal_life =  t1_input_item->ui->LE_xtal_life->text();
    QString name =  t1_input_item->ui->LE_name->text();
//    QString d_use =  QString("%1").arg(QVariant(t1_input_item->ui->CB_d_run->isChecked()).toInt());

    QString note =  t1_input_item->ui->LE_note->text();

    ui->t1_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t1_search_time_start->setDateTime(ui->t1_search_time_end->dateTime().addDays(-2));

    QString Recipe_total_text;
    QString Recipe_total_name_text;
    QString Recipe_name1 = t1_input_item->ui->LE_Recipe_name1->text();
    QString Recipe_name2 = t1_input_item->ui->LE_Recipe_name2->text();
    QString Recipe_name3 = t1_input_item->ui->LE_Recipe_name3->text();
    QString Recipe_name4 = t1_input_item->ui->LE_Recipe_name4->text();
    QString Recipe_name5 = t1_input_item->ui->LE_Recipe_name5->text();
    QString CB_Recipe_name1 = t1_input_item->ui->CB_Recipe_name1->currentText();
    QString CB_Recipe_name2 = t1_input_item->ui->CB_Recipe_name2->currentText();
    QString CB_Recipe_name3 = t1_input_item->ui->CB_Recipe_name3->currentText();
    QString CB_Recipe_name4 = t1_input_item->ui->CB_Recipe_name4->currentText();
    QString CB_Recipe_name5 = t1_input_item->ui->CB_Recipe_name5->currentText();
    int Recipe_count = 0;
    int ti_count = 0;
    int au_count = 0;
    int al_count = 0;

    if(CB_Recipe_name1 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name1));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name1));
        if(CB_Recipe_name1=="Ti"){
            ti_count += Recipe_name1.toInt();
        }else if(CB_Recipe_name1=="Al"){
            al_count += Recipe_name1.toInt();
        }else if(CB_Recipe_name1=="Au"){
            au_count += Recipe_name1.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name2 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name2));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name2));
        if(CB_Recipe_name2=="Ti"){
            ti_count += Recipe_name2.toInt();
        }else if(CB_Recipe_name2=="Al"){
            al_count += Recipe_name2.toInt();
        }else if(CB_Recipe_name2=="Au"){
            au_count += Recipe_name2.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name3 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name3));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name3));
        if(CB_Recipe_name3=="Ti"){
            ti_count += Recipe_name3.toInt();
        }else if(CB_Recipe_name3=="Al"){
            al_count += Recipe_name3.toInt();
        }else if(CB_Recipe_name3=="Au"){
            au_count += Recipe_name3.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name4 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name4));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name4));
        if(CB_Recipe_name4=="Ti"){
            ti_count += Recipe_name4.toInt();
        }else if(CB_Recipe_name4=="Al"){
            al_count += Recipe_name4.toInt();
        }else if(CB_Recipe_name4=="Au"){
            au_count += Recipe_name4.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name5 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name5));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name5));
        if(CB_Recipe_name5=="Ti"){
            ti_count += Recipe_name5.toInt();
        }else if(CB_Recipe_name5=="Al"){
            al_count += Recipe_name5.toInt();
        }else if(CB_Recipe_name5=="Au"){
            au_count += Recipe_name5.toInt();
        }
        Recipe_count++;
    }
    if(Recipe_count>0){
        Recipe_total_text.remove(Recipe_total_text.length()-1,1);
        Recipe_total_name_text.remove(Recipe_total_name_text.length()-1,1);
    }
    QSqlQuery query(my_mesdb);
    QString batch_id;
    query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_code = 'MD002'"));
    if(query.next()){
        batch_id = query.value("current_batch_id").toString();
        query.exec("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+1  where machine_code = 'MD002'");
    }
    bool query_result = false;
    qDebug()<<ui->t1_input_lot_table->rowCount();

    query.exec("select now() as now_time");
    QDateTime now_time;

    now_time = QDateTime::currentDateTime();
    for(int i=0;i<ui->t1_input_lot_table->rowCount();i++){
        QString Lot_id = t1_lot_item_list.at(i)->ui->LE_Lot_id->text();
        QString wafer_count = t1_lot_item_list.at(i)->ui->LE_wafer_count->text();
        QString ashing1 = t1_lot_item_list.at(i)->ui->LE_ashing1->text();
        QString ashing2 = t1_lot_item_list.at(i)->ui->LE_ashing2->text();
        QString ashing3 = t1_lot_item_list.at(i)->ui->LE_ashing3->text();
        QString ashing_machine = t1_lot_item_list.at(i)->ui->LE_ashing_machine->text();
        QString machine_type = t1_lot_item_list.at(i)->ui->LE_machine_type->text();
        QString wafer_angle = t1_lot_item_list.at(i)->ui->LE_wafer_angle->text();
        if(Lot_id.trimmed() !=""){
            QString query_str = QString("INSERT INTO `Thin_film_data` "
                                        "(`Run_number`, `Resister`, `machine_type`, `Lot_id`, `Wafer_count`, "
                                        "`Wafer_angle`, `Input_thin_data`, `ashing1`, `ashing2`, "
                                        "`ashing3`, `ashing_machine`, `Pad_thin`, `Wlp_thin`, "
                                        "`N2_Blow`, `Au_f_add`, `Now_r_weight`, `Au_after_weight`, "
                                        "`Au_use`, `Recipe`, `Ti_add`, `Al_add`, `X_tal_number`, "
                                        "`X_tal_life`, `User_name`, `D_use`, `note`, `Ti_total`, "
                                        "`Al_total`, `Au_total`, `batch_id`, `cleaning_cal_flag`,`machine_code`,`machine_name`,"
                                        "`Input_time`,`vacuum_time`,`day_and_night`,`au_pellet_output`,`au_add_weight`,`dom`) "
                                        "VALUES "
                                        "('%1', '%2', '%3', '%4', '%5', '%6', "
                                        "'%7', '%8', '%9', '%10', "
                                        "'%11', '%12', '%13', '%14', '%15', '%16', "
                                        "'%17', '%18', '%19', '%20', '%21', '%22', "
                                        "'%23', '%24', '%25', '%26', '%27', "
                                        "'%28', '%29', '%30' ,'%31','%32','%33','%34','%35','%36','%37','%38','%39' );").arg("").arg(resister).arg(machine_type).arg(Lot_id).arg(wafer_count)
                                                                      .arg(wafer_angle).arg(Recipe_total_text).arg(ashing1).arg(ashing2)
                                                                      .arg(ashing3).arg(ashing_machine).arg(QString("0")).arg(QString("0"))
                                                                      .arg("").arg(au_f_add).arg(now_r_weight).arg(au_after_r_weight)
                                                                      .arg(au_use).arg(Recipe_total_name_text).arg(ti_add).arg(al_add).arg(QString("0"))
                                                                      .arg(xtal_life).arg(name).arg("").arg(note).arg(ti_count)
                                                                      .arg(al_count).arg(au_count).arg(batch_id).arg(QString("0")).arg("MD002").arg("CHA#1")
                                                                      .arg(now_time.toString("yyyy-MM-dd hh:mm:ss"))
                                                                      .arg(t1_input_item->ui->DT_vacuum_time->dateTime().toString("yyyy-MM-dd hh:mm:00"))
                                                                      .arg(t1_input_item->ui->CB_day_and_night->currentText())
                                                                      .arg(t1_input_item->ui->DT_vacuum_time->dateTime().addSecs(-300).toString("yyyy-MM-dd hh:mm:00"))
                                                                      .arg(t1_input_item->ui->LE_au_after_weight->text())
                                                                      .arg(t1_input_item->ui->CB_domnumber->currentText());
            query_result = query.exec(query_str);
            if(!query_result){
                break;
            }

        }
    }
    au_calc(now_time);
    if(query_result){
        t1_input_item->clear_data();
        for(int i=0;i<ui->t1_input_lot_table->rowCount();i++){
            t1_lot_item_list.at(i)->clear_data();
        }
        QMessageBox msg;
        msg.addButton(QMessageBox::Ok);
        msg.setText(tr("input complete"));
        msg.exec();
//        int run_count = ui->t1_LE_Run_number->text().toInt();
//        run_count++;
//        ui->t1_LE_Run_number->setText(QString("%1").arg(run_count));
        on_t1_LE_Run_number_editingFinished();
        ui->t1_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
        ui->t1_search_time_start->setDateTime(ui->t1_search_time_end->dateTime().addDays(-2));
        t1_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
        on_t1_search_btn_clicked();
    }else {
        QMessageBox::warning(this, tr("conntion false"),
                                                       "server connection fail\n"
                                                          ""+my_mesdb.lastError().text(),
                                                            QMessageBox::Close);
    }
    query.exec(QString("select * from depostion_au_use_current_view where machine_name = '%1' AND batch_id = %2")
               .arg("CHA#1").arg(batch_id));
    if(query.next()){
        double  OKNG = query.value("OKNG").toDouble();
        if(abs(OKNG)>0.1){
            QMessageBox msg;
            msg.addButton(QMessageBox::Ok);
            msg.setText(tr("au pellet NG"));
            msg.exec();
        }
    }
    query.exec("update `Thin_film_spec_managerment` set au_pullet_add = 0 ,now_r_weight = 0,au_after_r_weight =0,au_after_weight=0 where machine_code = 'MD002'");
    ti_al_chart_init(t1_ti_chart,t1_al_chart,t1_ti_value_series,t1_al_value_series,t1_ti_UCL_value_series,t1_al_UCL_value_series,
                     t1_al_axisX,t1_al_axisY,t1_ti_axisX,t1_ti_axisY,"MD002");
}
void Thin_film_mainwindows::on_t1_search_btn_clicked()
{
    t1_data_model->setFilter(QString("vacuum_time Between '%1' AND '%2' AND machine_code = 'MD002'")
                          .arg(ui->t1_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t1_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    t1_data_model->select();
    t1_data_model->setSort(2,Qt::AscendingOrder);
    QSqlQuery query(my_mesdb);
    query.exec(QString("select sum((case when (`A`.`machine_code` = 'MD002') then `A`.`Wafer_count` end)) AS `wafer` from `Thin_film_data` `A` "
               "where vacuum_time Between '%1' AND '%2' AND machine_code = 'MD002'").arg(ui->t1_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
               .arg(ui->t1_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    if(query.next()){

        ui->t1_total_wafer->setText(query.value("wafer").toString());
    }

    ui->t1_data_table->scrollToBottom();

}
void Thin_film_mainwindows::on_t1_BT_search_lot_id_clicked()
{
    t1_data_model->setFilter(QString("Lot_id = '%1' AND machine_code = 'MD002'")
                          .arg(ui->t1_LE_search_lot_id->text()));
    t1_data_model->select();
    t1_data_model->setSort(1,Qt::AscendingOrder);
}
void Thin_film_mainwindows::on_t1_ti_al_refresh_clicked()
{
    ti_al_chart_init(t1_ti_chart,t1_al_chart,t1_ti_value_series,t1_al_value_series,t1_ti_UCL_value_series,t1_al_UCL_value_series,
                     t1_al_axisX,t1_al_axisY,t1_ti_axisX,t1_ti_axisY,"MD002");
}
void Thin_film_mainwindows::on_t1_DT_del_btn_clicked()
{
    int count = ui->t1_data_table->selectionModel()->selectedIndexes().count();
    QVector<int> index_history;
    for(int i=0;i<count;i++){
        if(!index_history.contains(ui->t1_data_table->selectionModel()->selectedIndexes().at(i).row())){
        QDateTime Input_time2 = t1_data_model->record(ui->t1_data_table->selectionModel()->selectedIndexes().at(i).row()).value("Input_time").toDateTime();
        t1_data_model->removeRow(ui->t1_data_table->selectionModel()->selectedIndexes().at(i).row());
         au_calc(Input_time2);
         index_history.append(ui->t1_data_table->selectionModel()->selectedIndexes().at(i).row());
        }else {

        }
    }
    on_t1_search_btn_clicked();
    ui->t1_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t1_move_machine_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    QString batch_id_temp = "0";
    QString machine_name;
    QString machine_code;

    int count  = ui->t1_data_table->selectionModel()->selectedIndexes().count();
    for(int i=0;i<count;i++){
         QSqlRecord recode = t1_data_model->record(ui->t1_data_table->selectionModel()->selectedIndexes().at(i).row());
        if(batch_id_temp != recode.value("batch_id").toString()){
            query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_name = '%1'").arg(ui->t1_CB_move_machine->currentText()));
            if(query.next()){
                batch_id_temp = query.value("current_batch_id").toString()+5;
                machine_name = query.value("machine_name").toString();
                machine_code = query.value("machine_code").toString();
                query.exec(QString("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+10  where machine_name = '%1'").arg(ui->t1_CB_move_machine->currentText()));
            }
            batch_id_temp = recode.value("batch_id").toString();
        }

        recode.setGenerated("batch_id",true);
        recode.setGenerated("machine_name",true);
        recode.setGenerated("machine_code",true);
        recode.setValue("batch_id",batch_id_temp);
        recode.setValue("machine_name",machine_name);
        recode.setValue("machine_code",machine_code);
        t1_data_model->setRecord(ui->t1_data_table->selectionModel()->selectedIndexes().at(i).row(),recode);
    }
    on_t1_search_btn_clicked();
    ui->t1_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t1_excel_header_copy_flag_toggled(bool checked)
{
    ui->t1_data_table->copyheader_flag = checked;
}
void Thin_film_mainwindows::on_t1_machine_temp_run_mode_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    query.exec("update Thin_film_spec_managerment set nomal_mode_time = ADDTIME(NOW(),'0:5:0') where `machine_code`='MD002'");
}

void Thin_film_mainwindows::t1_lot_edit_finish()
{
    QString update_text;
    for(int i=0;i<t1_lot_item_list.count();i++){
        input_lot_item *lot_item = t1_lot_item_list.at(i);
        QString lot_id = lot_item->ui->LE_Lot_id->text();
        QString wafer_count = lot_item->ui->LE_wafer_count->text();
        QString update_data = QString("%1/%2,").arg(lot_id).arg(wafer_count);
        update_text += update_data;

    }

//    query.exec("")

}
//t1_function -- end --

//t2_function -- start --
void Thin_film_mainwindows::t2_init()
{
    ui->t2_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t2_search_time_start->setDateTime(ui->t2_search_time_end->dateTime().addDays(-2));

    QSqlTableModel *spec_model = new QSqlTableModel(this,my_mesdb);
    spec_model->setTable("Thin_film_spec_managerment");
    spec_model->setFilter("machine_code = 'MD003'");
    spec_model->select();
    spec_model->setHeaderData(4,Qt::Horizontal,tr("ashing_LCL"));
    spec_model->setHeaderData(5,Qt::Horizontal,tr("ashing_UCL"));
    spec_model->setHeaderData(6,Qt::Horizontal,tr("WLPTiAlashing_LCL"));
    spec_model->setHeaderData(7,Qt::Horizontal,tr("WLPTiAlashing_UCL"));
    spec_model->setHeaderData(8,Qt::Horizontal,tr("WLP_LCL_Ti_Au"));
    spec_model->setHeaderData(9,Qt::Horizontal,tr("WLP_UCL_Ti_Au"));
    spec_model->setHeaderData(10,Qt::Horizontal,tr("WLP_LCL_Ti_Al"));
    spec_model->setHeaderData(11,Qt::Horizontal,tr("WLP_UCL_Ti_Al"));
    spec_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    t2_data_model = new thin_sqltable_model(this,my_mesdb,"MD003",spec_model);

    t2_data_model->setTable("Thin_film_data");

    t2_data_model->setFilter(QString("Input_time Between '%1' AND '%2' AND machine_code = 'MD003'")
                          .arg(ui->t2_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t2_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    ui->t2_data_table->setItemDelegateForColumn(t2_data_model->fieldIndex("vacuum_time"),new DateDelegate(ui->t2_data_table));
    ui->t2_data_table->setItemDelegateForColumn(t2_data_model->fieldIndex("Input_time"),new DateDelegate(ui->t2_data_table));
    ui->t2_data_table->setModel(t2_data_model);
    ui->t2_data_table->setSortingEnabled(true);
    connect(t2_data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>))
            ,this,SLOT(t2_datamodel_dataChanged(QModelIndex,QModelIndex,QVector<int>)));
    ui->t2_data_table->setItemDelegateForColumn(10,new Thin_combodelegate(note_combo_list,ui->t2_data_table));
    t2_data_model->setEditStrategy(QSqlTableModel::OnFieldChange);
    t2_data_model->setSort(2,Qt::AscendingOrder);

    t2_data_model->select();


    t2_data_model->setHeaderData(t2_data_model->fieldIndex("day_and_night"),Qt::Horizontal,tr("day_and_night"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("vacuum_time"),Qt::Horizontal,tr("vacuum_time"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Run_number"),Qt::Horizontal,tr("thin_Run_number"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Resister"),Qt::Horizontal,tr("thin_Resister"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("machine_type"),Qt::Horizontal,tr("thin_machine_type"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Lot_id"),Qt::Horizontal,tr("thin_Lot_id"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Wafer_count"),Qt::Horizontal,tr("thin_Wafer_count"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Pad_thin"),Qt::Horizontal,tr("thin_Pad_thin"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Wlp_thin"),Qt::Horizontal,tr("thin_Wlp_thin"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Wafer_angle"),Qt::Horizontal,tr("thin_Wafer_angle"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Input_thin_data"),Qt::Horizontal,tr("thin_Input_thin_data"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Recipe"),Qt::Horizontal,tr("thin_Recipe"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("ashing1"),Qt::Horizontal,tr("thin_ashing1"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("ashing2"),Qt::Horizontal,tr("thin_ashing2"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("ashing3"),Qt::Horizontal,tr("thin_ashing3"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("ashing_machine"),Qt::Horizontal,tr("thin_ashing_machine"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("N2_Blow"),Qt::Horizontal,tr("thin_N2_Blow"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Au_f_add"),Qt::Horizontal,tr("thin_thin_Au_f_add"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Now_r_weight"),Qt::Horizontal,tr("thin_Now_r_weight"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Au_after_weight"),Qt::Horizontal,tr("thin_Au_after_weight"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Au_use"),Qt::Horizontal,tr("thin_Au_use"));
//    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Ti_add"),Qt::Horizontal,tr("thin_Ti_add"));
//    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Al_add"),Qt::Horizontal,tr("thin_Al_add"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("X_tal_number"),Qt::Horizontal,tr("thin_X_tal_number"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("X_tal_Hz"),Qt::Horizontal,tr("thin_X_tal_Hz"));
//    t2_data_model->setHeaderData(t2_data_model->fieldIndex("X_tal_life"),Qt::Horizontal,tr("thin_X_tal_life"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("User_name"),Qt::Horizontal,tr("thin_User_name"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("D_use"),Qt::Horizontal,tr("thin_D_use"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("note"),Qt::Horizontal,tr("thin_note"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Ti_total"),Qt::Horizontal,tr("thin_Ti_total"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Al_total"),Qt::Horizontal,tr("thin_Al_total"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Au_total"),Qt::Horizontal,tr("thin_Au_total"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("Input_time"),Qt::Horizontal,tr("thin_Input_time"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("machine_name"),Qt::Horizontal,tr("thin_machine_name"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("machine_code"),Qt::Horizontal,tr("thin_machine_code"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("batch_id"),Qt::Horizontal,tr("thin_batch_id"));
    t2_data_model->setHeaderData(t2_data_model->fieldIndex("cleaning_cal_flag"),Qt::Horizontal,tr("thin_cleaning_cal_flag"));
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("day_and_night"),50);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("vacuum_time"),110);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Run_number"),60);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Resister"),50);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("machine_type"),100);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Lot_id"),80);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Wafer_count"),50);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Pad_thin"),80);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Wlp_thin"),80);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Wafer_angle"),80);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Input_thin_data"),150);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Recipe"),150);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("ashing1"),65);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("ashing2"),65);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("ashing3"),65);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("ashing_machine"),80);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("N2_Blow"),80);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Au_f_add"),80);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Now_r_weight"),120);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Au_after_weight"),140);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Au_use"),50);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Ti_add"),50);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Al_add"),50);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("X_tal_number"),50);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("X_tal_Hz"),50);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("X_tal_life"),80);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("User_name"),80);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("D_use"),80);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("note"),140);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Ti_total"),60);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Al_total"),60);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Au_total"),60);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("Input_time"),120);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("machine_name"),50);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("machine_code"),50);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("batch_id"),50);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("cleaning_cal_flag"),50);
    ui->t2_data_table->horizontalHeader()->resizeSection(t2_data_model->fieldIndex("dom"),50);
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("X_tal_number"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("X_tal_Hz"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("machine_code"));
//    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("batch_id"));
//    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("cleaning_cal_flag"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("Au_use"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("au_pellet_output"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("au_add"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("check_user"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("au_pellet_total"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("work_start_au"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("au_add_weight"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("au_case_weight"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("check_NG"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("Run_number"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("Pad_thin"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("Wlp_thin"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("N2_Blow"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("D_use"));

    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("Ti_add"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("Al_add"));
    ui->t2_data_table->horizontalHeader()->hideSection(t2_data_model->fieldIndex("X_tal_life"));

    ui->t2_data_table->scrollToBottom();

    ui->t2_spec_table->setModel(spec_model);
    ui->t2_spec_table->horizontalHeader()->hideSection(0);
    ui->t2_spec_table->horizontalHeader()->hideSection(1);
    ui->t2_spec_table->horizontalHeader()->hideSection(2);
    ui->t2_spec_table->horizontalHeader()->hideSection(3);
    ui->t2_spec_table->horizontalHeader()->hideSection(4);
    ui->t2_spec_table->horizontalHeader()->hideSection(5);
    ui->t2_spec_table->horizontalHeader()->hideSection(6);
    ui->t2_spec_table->horizontalHeader()->hideSection(7);
    ui->t2_spec_table->horizontalHeader()->hideSection(8);
    ui->t2_spec_table->horizontalHeader()->hideSection(9);
    ui->t2_spec_table->horizontalHeader()->hideSection(10);
    ui->t2_spec_table->horizontalHeader()->hideSection(11);
    ui->t2_spec_table->horizontalHeader()->hideSection(12);
    ui->t2_spec_table->horizontalHeader()->hideSection(13);
    ui->t2_spec_table->horizontalHeader()->hideSection(14);
    ui->t2_spec_table->horizontalHeader()->hideSection(17);
    ui->t2_spec_table->horizontalHeader()->hideSection(18);
    ui->t2_spec_table->horizontalHeader()->hideSection(19);
    ui->t2_spec_table->horizontalHeader()->hideSection(20);
    ui->t2_spec_table->horizontalHeader()->hideSection(21);
    ui->t2_spec_table->horizontalHeader()->hideSection(22);
    ui->t2_spec_table->horizontalHeader()->hideSection(23);
    ui->t2_spec_table->horizontalHeader()->hideSection(24);
    ui->t2_spec_table->horizontalHeader()->hideSection(25);
    ui->t2_spec_table->horizontalHeader()->hideSection(26);
    ui->t2_spec_table->horizontalHeader()->hideSection(27);
    ui->t2_spec_table->horizontalHeader()->hideSection(28);
    ui->t2_spec_table->horizontalHeader()->hideSection(29);
    ui->t2_spec_table->horizontalHeader()->hideSection(30);
    ui->t2_spec_table->horizontalHeader()->hideSection(31);
    ui->t2_spec_table->horizontalHeader()->hideSection(32);
    ui->t2_spec_table->horizontalHeader()->hideSection(33);
    ui->t2_spec_table->horizontalHeader()->hideSection(34);
    ui->t2_spec_table->horizontalHeader()->hideSection(35);
    ui->t2_spec_table->horizontalHeader()->resizeSection(6,150);
    ui->t2_spec_table->horizontalHeader()->resizeSection(7,150);

    for(int i=0;i<8;i++){
        ui->t2_input_table->insertColumn(i);
    }
    ui->t2_input_table->insertRow(0);
    ui->t2_input_table->insertRow(1);
    ui->t2_input_table->insertRow(2);
    ui->t2_input_table->insertRow(3);
    ui->t2_input_table->insertRow(4);
    ui->t2_input_table->insertRow(5);
    ui->t2_input_table->insertRow(6);
    ui->t2_input_table->horizontalHeader()->resizeSection(0,70);
    ui->t2_input_table->horizontalHeader()->resizeSection(1,150);
    ui->t2_input_table->horizontalHeader()->resizeSection(3,70);
    ui->t2_input_table->horizontalHeader()->resizeSection(5,130);
    ui->t2_input_table->horizontalHeader()->resizeSection(6,140);
    ui->t2_input_table->horizontalHeader()->resizeSection(7,80);
    t2_input_item = new input_item(my_mesdb,"MD003");
    ui->t2_input_table->setCellWidget(0,0,t2_input_item->ui->LA_day_and_night);
    ui->t2_input_table->setCellWidget(1,0,t2_input_item->ui->CB_day_and_night);
    ui->t2_input_table->setCellWidget(0,1,t2_input_item->ui->LA_vacuum_time);
    ui->t2_input_table->setCellWidget(1,1,t2_input_item->ui->DT_vacuum_time);
    t2_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
    ui->t2_input_table->setCellWidget(0,2,t2_input_item->ui->LA_resister);
    ui->t2_input_table->setCellWidget(1,2,t2_input_item->ui->LE_resister);
//    ui->t2_input_table->setCellWidget(0,3,t2_input_item->ui->LA_N2_blow);
//    ui->t2_input_table->setCellWidget(1,3,t2_input_item->ui->WCB_N2_blow);
    ui->t2_input_table->setCellWidget(0,4,t2_input_item->ui->LA_au_f_add);
    ui->t2_input_table->setCellWidget(1,4,t2_input_item->ui->LE_au_f_add);
    ui->t2_input_table->setCellWidget(0,5,t2_input_item->ui->LA_now_r_weight);
    ui->t2_input_table->setCellWidget(1,5,t2_input_item->ui->LE_now_r_weight);
    ui->t2_input_table->setCellWidget(0,6,t2_input_item->ui->LA_au_after_r_weight);
    ui->t2_input_table->setCellWidget(1,6,t2_input_item->ui->LE_au_after_r_weight);
    ui->t2_input_table->setCellWidget(0,7,t2_input_item->ui->LA_au_after_weight);
    ui->t2_input_table->setCellWidget(1,7,t2_input_item->ui->LE_au_after_weight);
//    ui->t2_input_table->setCellWidget(0,7,t2_input_item->ui->LA_au_use);
//    ui->t2_input_table->setCellWidget(1,7,t2_input_item->ui->LE_au_use);
//    ui->t2_input_table->setCellWidget(2,0,t2_input_item->ui->LA_ti_add);
//    ui->t2_input_table->setCellWidget(3,0,t2_input_item->ui->LE_ti_add);
//    ui->t2_input_table->setCellWidget(2,1,t2_input_item->ui->LA_al_add);
//    ui->t2_input_table->setCellWidget(3,1,t2_input_item->ui->LE_al_add);
//    ui->t2_input_table->setCellWidget(2,2,t2_input_item->ui->LA_xtal_life);
//    ui->t2_input_table->setCellWidget(3,2,t2_input_item->ui->LE_xtal_life);
    ui->t2_input_table->setCellWidget(2,3,t2_input_item->ui->LA_name);
    ui->t2_input_table->setCellWidget(3,3,t2_input_item->ui->LE_name);
//    ui->t2_input_table->setCellWidget(2,4,t2_input_item->ui->LA_d_run);
//    ui->t2_input_table->setCellWidget(3,4,t2_input_item->ui->WCB_d_run);
    ui->t2_input_table->setCellWidget(2,5,t2_input_item->ui->LA_note);
    ui->t2_input_table->setCellWidget(3,5,t2_input_item->ui->LE_note);
    ui->t2_input_table->setSpan(2,5,1,5);
    ui->t2_input_table->setSpan(3,5,1,5);
    ui->t2_input_table->setCellWidget(4,0,t2_input_item->ui->LE_recipe_number);
    ui->t2_input_table->setCellWidget(4,1,t2_input_item->ui->CB_recipe_choice);
//    ui->t2_input_table->setCellWidget(4,3,t2_input_item->ui->LA_recipe_2);
    ui->t2_input_table->setCellWidget(5,0,t2_input_item->ui->W_recipe);
    ui->t2_input_table->setSpan(4,1,1,5);
//    ui->t2_input_table->setSpan(4,3,1,2);
    ui->t2_input_table->setSpan(5,0,2,5);

    ui->t2_input_table->setCellWidget(4,6,t2_input_item->ui->LA_dom_number);
    ui->t2_input_table->setCellWidget(5,6,t2_input_item->ui->CB_domnumber);


    ui->t2_LE_Lot_count->setValidator(new QIntValidator(0, 1000, this));
//    ui->t2_LE_Run_number->setValidator(new QIntValidator(0, 1000, this));
    QSqlQuery query(my_mesdb);
    query.exec(QString("select `Lot_max_count`,`Run_number_count` from Thin_film_spec_managerment where machine_code = 'MD003'"));
    if(query.next()){
        ui->t2_LE_Lot_count->setText(query.value("Lot_max_count").toString());
//        ui->t2_LE_Run_number->setText(query.value("Run_number_count").toString());
    }
    t2_lot_item_list.clear();
    for(int i=0;i<ui->t2_LE_Lot_count->text().toInt();i++){

        input_lot_item *lot_item = new input_lot_item(ms_mesdb,t2_input_item,"MD003");
        t2_lot_item_list.append(lot_item);
        ui->t2_input_lot_table->insertRow(i);
        ui->t2_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
        ui->t2_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
        ui->t2_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
        ui->t2_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
        ui->t2_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
        ui->t2_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
        ui->t2_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
        ui->t2_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//        connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                t2_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
    }
    query.exec("SELECT * FROM `Thin_film_repcipe` where machine_code = 'MD003' ORDER BY `recipe_number` ASC  ");
    t2_input_item->ui->CB_recipe_choice->addItem(tr("recipe_choice"));
    while(query.next()){
        QString recipe_name = query.value("recipe_number").toString()+"."+ query.value("recipename").toString()
                                + "|"+query.value("metal_name").toString()+"|"+query.value("metal_thin").toString()+"|"
                                +query.value("type").toString();
        QPixmap pixmap(100,100);
        pixmap.fill(QColor(query.value("color").toString()));
        t2_input_item->ui->CB_recipe_choice->addItem(QIcon(pixmap),recipe_name);
    }
    connect(t2_input_item->ui->CB_recipe_choice,SIGNAL(currentIndexChanged(int)),this,SLOT(t2_recipe_choice(int)));

    t2_ti_chart = new Thin_film_chart();
    t2_ti_chartview = new Thin_film_chart_view(t2_ti_chart);
    connect(t2_ti_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t2_ti_y_value_slot(QPointF)));
    ui->t2_Ti_layout->addWidget(t2_ti_chartview);

    t2_al_chart = new Thin_film_chart();
    t2_al_chartview = new Thin_film_chart_view(t2_al_chart);
    connect(t2_al_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t2_al_y_value_slot(QPointF)));
    ui->t2_Al_layout->addWidget(t2_al_chartview);
    t2_ti_axisX = new QDateTimeAxis();
    t2_ti_axisX->setFormat("MM-dd HH:mm:ss");
    t2_ti_axisX->setTitleText("Date");
    t2_ti_axisX->setVisible(true);
    t2_ti_axisX->setTitleVisible(true);
    t2_ti_chart->addAxis(t2_ti_axisX, Qt::AlignBottom);
    t2_ti_axisY = new QValueAxis();
    t2_ti_chart->addAxis(t2_ti_axisY,Qt::AlignLeft);

    t2_al_axisX = new QDateTimeAxis();
    t2_al_axisX->setFormat("MM-dd HH:mm:ss");
    t2_al_axisX->setTitleText("Date");
    t2_al_axisX->setVisible(true);
    t2_al_axisX->setTitleVisible(true);
    t2_al_chart->addAxis(t2_al_axisX, Qt::AlignBottom);
    t2_al_axisY = new QValueAxis();
    t2_al_chart->addAxis(t2_al_axisY,Qt::AlignLeft);

    ti_al_chart_init(t2_ti_chart,t2_al_chart,t2_ti_value_series,t2_al_value_series,t2_ti_UCL_value_series,t2_al_UCL_value_series,
                     t2_al_axisX,t2_al_axisY,t2_ti_axisX,t2_ti_axisY,"MD003");
    query.exec("SELECT machine_name,machine_code FROM Thin_film_spec_managerment");
    while(query.next()){
        ui->t2_CB_move_machine->addItem(query.value("machine_name").toString());
    }
    temp_run_widget_map.insert("MD003",ui->t2_temp_run_mode_time);
    temp_run_ti_accmulate_widget_map.insert("MD003",ui->t2_ti_accmulate);
    temp_run_al_accmulate_widget_map.insert("MD003",ui->t2_al_accmulate);
    ui->t2_CB_wet_insert->addItems(wet_machine_list);
    sync_now_date("MD003",t2_input_item->ui->LE_au_f_add,t2_input_item->ui->LE_now_r_weight,t2_input_item->ui->LE_au_after_r_weight
                  ,t2_input_item->ui->LE_au_after_weight);
    ui->t2_data_table->selectionModel()->setObjectName("t2_data");
    connect(ui->t2_data_table->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this,SLOT(table_view_selection_change(QModelIndex,QModelIndex)));

}
void Thin_film_mainwindows::t2_ti_y_value_slot(QPointF value)
{
     ui->t2_ti_y_value->setText(QString("%1").arg(value.y()));
}
void Thin_film_mainwindows::t2_al_y_value_slot(QPointF value)
{
    ui->t2_al_y_value->setText(QString("%1").arg(value.y()));
}
void Thin_film_mainwindows::t2_datamodel_dataChanged(QModelIndex index1, QModelIndex index2, QVector<int> vector)
{
    v1_data_model_change(index1,t2_data_model,"MD003");
    ui->t2_data_table->scrollToBottom();
}
void Thin_film_mainwindows::t2_recipe_choice(int idx)
{
    if(idx == 0){
        t2_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t2_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t2_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t2_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t2_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t2_input_item->ui->LE_Recipe_name1->setText("");
        t2_input_item->ui->LE_Recipe_name2->setText("");
        t2_input_item->ui->LE_Recipe_name3->setText("");
        t2_input_item->ui->LE_Recipe_name4->setText("");
        t2_input_item->ui->LE_Recipe_name5->setText("");
        return ;
    }
    QSqlQuery query(my_mesdb);
    query.exec(QString("select * from `Thin_film_repcipe` where recipe_number  = %1 AND machine_code = 'MD003'").arg(idx));

    if(query.next()){
        t2_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t2_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t2_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t2_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t2_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t2_input_item->ui->LE_Recipe_name1->setText("");
        t2_input_item->ui->LE_Recipe_name2->setText("");
        t2_input_item->ui->LE_Recipe_name3->setText("");
        t2_input_item->ui->LE_Recipe_name4->setText("");
        t2_input_item->ui->LE_Recipe_name5->setText("");
        if(query.value("metal_name").toString() != ""){
            QString item_data = query.value("metal_name").toString();
            QStringList item_list = item_data.split("/");
            for(int i=0;i<item_list.count();i++){
                if(i==0){
                    t2_input_item->ui->CB_Recipe_name1->setCurrentText(item_list.at(i));
                }else if(i==1){
                    t2_input_item->ui->CB_Recipe_name2->setCurrentText(item_list.at(i));
                }else if(i==2){
                    t2_input_item->ui->CB_Recipe_name3->setCurrentText(item_list.at(i));
                }else if(i==3){
                    t2_input_item->ui->CB_Recipe_name4->setCurrentText(item_list.at(i));
                }else if(i==4){
                    t2_input_item->ui->CB_Recipe_name5->setCurrentText(item_list.at(i));
                }
            }
        }
        if(query.value("metal_thin").toString() != ""){
            QStringList recipe_item = query.value("metal_thin").toString().split("/");
            if(recipe_item.count()>0){
                t2_input_item->ui->LE_Recipe_name1->setText(QString("%1").arg(recipe_item[0]));
            }
            if(recipe_item.count()>1){
                t2_input_item->ui->LE_Recipe_name2->setText(QString("%1").arg(recipe_item[1]));
            }
            if(recipe_item.count()>2){
                t2_input_item->ui->LE_Recipe_name3->setText(QString("%1").arg(recipe_item[2]));
            }
            if(recipe_item.count()>3){
                t2_input_item->ui->LE_Recipe_name4->setText(QString("%1").arg(recipe_item[3]));
            }
            if(recipe_item.count()>4){
                t2_input_item->ui->LE_Recipe_name5->setText(QString("%1").arg(recipe_item[4]));
            }
        }
    }
}
void Thin_film_mainwindows::on_t2_LE_Lot_count_editingFinished()
{
//    if(ui->t2_LE_Lot_count->text()!= ""){
//        QSqlQuery query(my_mesdb);
//        query.exec(QString("update `Thin_film_spec_managerment` SET `Lot_max_count`='%1' Where `machine_code`='MD003'").arg(ui->t2_LE_Lot_count->text()));
//        while(ui->t2_input_lot_table->rowCount()>0){
//            ui->t2_input_lot_table->removeRow(0);
//        }
//        t2_lot_item_list.clear();
//        for(int i=0;i<ui->t2_LE_Lot_count->text().toInt();i++){

//            input_lot_item *lot_item = new input_lot_item(ms_mesdb,"MD003");
//            t2_lot_item_list.append(lot_item);
//            ui->t2_input_lot_table->insertRow(i);
//            ui->t2_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//    //        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
//            ui->t2_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
//            ui->t2_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
//            ui->t2_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
//            ui->t2_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
//            ui->t2_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
//            ui->t2_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
//            ui->t2_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//            connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                    t2_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
//        }
//    }
}
void Thin_film_mainwindows::on_t2_LE_Run_number_editingFinished()
{
//    if(ui->t2_LE_Run_number->text() != ""){
//        QSqlQuery query(my_mesdb);
//        query.exec(QString("update `Thin_film_spec_managerment` SET `Run_number_count`='%1' Where `machine_code`='MD003'").arg(ui->t2_LE_Run_number->text()));
//    }
}
void Thin_film_mainwindows::on_t2_Input_btn_clicked()
{
//    QString Run_number = ui->t2_LE_Run_number->text();
//    if(Run_number.toInt() > 60){
//        ui->t2_LE_Run_number->setText("1");
//        on_t2_LE_Run_number_editingFinished();
//        QMessageBox msg;
//        msg.addButton(QMessageBox::Ok);
//        msg.setText(tr("60 over set 1"));
//        msg.exec();
//    }
    QString Lot_count = ui->t2_LE_Lot_count->text();
    QString resister = t2_input_item->ui->LE_resister->text();
//    QString N2_blow = QString("%1").arg(QVariant(t2_input_item->ui->CB_N2_blow->isChecked()).toInt());
    QString au_f_add = t2_input_item->ui->LE_au_f_add->text();
    QString now_r_weight = t2_input_item->ui->LE_now_r_weight->text();
    QString au_after_r_weight = t2_input_item->ui->LE_au_after_r_weight->text();
    QString au_use =  t2_input_item->ui->LE_au_use->text();
    QString ti_add =  t2_input_item->ui->LE_ti_add->text();
    QString al_add =  t2_input_item->ui->LE_al_add->text();
    QString xtal_life =  t2_input_item->ui->LE_xtal_life->text();
    QString name =  t2_input_item->ui->LE_name->text();
//    QString d_use =  QString("%1").arg(QVariant(t2_input_item->ui->CB_d_run->isChecked()).toInt());

    QString note =  t2_input_item->ui->LE_note->text();

    ui->t2_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t2_search_time_start->setDateTime(ui->t2_search_time_end->dateTime().addDays(-2));

    QString Recipe_total_text;
    QString Recipe_total_name_text;
    QString Recipe_name1 = t2_input_item->ui->LE_Recipe_name1->text();
    QString Recipe_name2 = t2_input_item->ui->LE_Recipe_name2->text();
    QString Recipe_name3 = t2_input_item->ui->LE_Recipe_name3->text();
    QString Recipe_name4 = t2_input_item->ui->LE_Recipe_name4->text();
    QString Recipe_name5 = t2_input_item->ui->LE_Recipe_name5->text();
    QString CB_Recipe_name1 = t2_input_item->ui->CB_Recipe_name1->currentText();
    QString CB_Recipe_name2 = t2_input_item->ui->CB_Recipe_name2->currentText();
    QString CB_Recipe_name3 = t2_input_item->ui->CB_Recipe_name3->currentText();
    QString CB_Recipe_name4 = t2_input_item->ui->CB_Recipe_name4->currentText();
    QString CB_Recipe_name5 = t2_input_item->ui->CB_Recipe_name5->currentText();
    int Recipe_count = 0;
    int ti_count = 0;
    int au_count = 0;
    int al_count = 0;

    if(CB_Recipe_name1 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name1));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name1));
        if(CB_Recipe_name1=="Ti"){
            ti_count += Recipe_name1.toInt();
        }else if(CB_Recipe_name1=="Al"){
            al_count += Recipe_name1.toInt();
        }else if(CB_Recipe_name1=="Au"){
            au_count += Recipe_name1.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name2 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name2));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name2));
        if(CB_Recipe_name2=="Ti"){
            ti_count += Recipe_name2.toInt();
        }else if(CB_Recipe_name2=="Al"){
            al_count += Recipe_name2.toInt();
        }else if(CB_Recipe_name2=="Au"){
            au_count += Recipe_name2.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name3 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name3));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name3));
        if(CB_Recipe_name3=="Ti"){
            ti_count += Recipe_name3.toInt();
        }else if(CB_Recipe_name3=="Al"){
            al_count += Recipe_name3.toInt();
        }else if(CB_Recipe_name3=="Au"){
            au_count += Recipe_name3.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name4 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name4));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name4));
        if(CB_Recipe_name4=="Ti"){
            ti_count += Recipe_name4.toInt();
        }else if(CB_Recipe_name4=="Al"){
            al_count += Recipe_name4.toInt();
        }else if(CB_Recipe_name4=="Au"){
            au_count += Recipe_name4.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name5 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name5));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name5));
        if(CB_Recipe_name5=="Ti"){
            ti_count += Recipe_name5.toInt();
        }else if(CB_Recipe_name5=="Al"){
            al_count += Recipe_name5.toInt();
        }else if(CB_Recipe_name5=="Au"){
            au_count += Recipe_name5.toInt();
        }
        Recipe_count++;
    }
    if(Recipe_count>0){
        Recipe_total_text.remove(Recipe_total_text.length()-1,1);
        Recipe_total_name_text.remove(Recipe_total_name_text.length()-1,1);
    }
    QSqlQuery query(my_mesdb);
    QString batch_id;
    query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_code = 'MD003'"));
    if(query.next()){
        batch_id = query.value("current_batch_id").toString();
        query.exec("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+1  where machine_code = 'MD003'");
    }
    bool query_result = false;
    qDebug()<<ui->t2_input_lot_table->rowCount();

    query.exec("select now() as now_time");
    QDateTime now_time;

    now_time = QDateTime::currentDateTime();

    for(int i=0;i<ui->t2_input_lot_table->rowCount();i++){
        QString Lot_id = t2_lot_item_list.at(i)->ui->LE_Lot_id->text();
        QString wafer_count = t2_lot_item_list.at(i)->ui->LE_wafer_count->text();
        QString ashing1 = t2_lot_item_list.at(i)->ui->LE_ashing1->text();
        QString ashing2 = t2_lot_item_list.at(i)->ui->LE_ashing2->text();
        QString ashing3 = t2_lot_item_list.at(i)->ui->LE_ashing3->text();
        QString ashing_machine = t2_lot_item_list.at(i)->ui->LE_ashing_machine->text();
        QString machine_type = t2_lot_item_list.at(i)->ui->LE_machine_type->text();
        QString wafer_angle = t2_lot_item_list.at(i)->ui->LE_wafer_angle->text();
        if(Lot_id.trimmed() !=""){
            QString query_str = QString("INSERT INTO `Thin_film_data` "
                                        "(`Run_number`, `Resister`, `machine_type`, `Lot_id`, `Wafer_count`, "
                                        "`Wafer_angle`, `Input_thin_data`, `ashing1`, `ashing2`, "
                                        "`ashing3`, `ashing_machine`, `Pad_thin`, `Wlp_thin`, "
                                        "`N2_Blow`, `Au_f_add`, `Now_r_weight`, `Au_after_weight`, "
                                        "`Au_use`, `Recipe`, `Ti_add`, `Al_add`, `X_tal_number`, "
                                        "`X_tal_life`, `User_name`, `D_use`, `note`, `Ti_total`, "
                                        "`Al_total`, `Au_total`, `batch_id`, `cleaning_cal_flag`,`machine_code`,`machine_name`,"
                                        "`Input_time`,`vacuum_time`,`day_and_night`,`au_pellet_output`,`au_add_weight`,`dom`) "
                                        "VALUES "
                                        "('%1', '%2', '%3', '%4', '%5', '%6', "
                                        "'%7', '%8', '%9', '%10', "
                                        "'%11', '%12', '%13', '%14', '%15', '%16', "
                                        "'%17', '%18', '%19', '%20', '%21', '%22', "
                                        "'%23', '%24', '%25', '%26', '%27', "
                                        "'%28', '%29', '%30' ,'%31','%32','%33','%34','%35','%36','%37','%38','%39' );").arg("").arg(resister).arg(machine_type).arg(Lot_id).arg(wafer_count)
                                                                      .arg(wafer_angle).arg(Recipe_total_text).arg(ashing1).arg(ashing2)
                                                                      .arg(ashing3).arg(ashing_machine).arg(QString("0")).arg(QString("0"))
                                                                      .arg("").arg(au_f_add).arg(now_r_weight).arg(au_after_r_weight)
                                                                      .arg(au_use).arg(Recipe_total_name_text).arg(ti_add).arg(al_add).arg(QString("0"))
                                                                      .arg(xtal_life).arg(name).arg("").arg(note).arg(ti_count)
                                                                      .arg(al_count).arg(au_count).arg(batch_id).arg(QString("0")).arg("MD003").arg("CHA#2")
                                                                      .arg(now_time.toString("yyyy-MM-dd hh:mm:ss"))
                                                                      .arg(t2_input_item->ui->DT_vacuum_time->dateTime().toString("yyyy-MM-dd hh:mm:00"))
                                                                      .arg(t2_input_item->ui->CB_day_and_night->currentText())
                                                                      .arg(t2_input_item->ui->DT_vacuum_time->dateTime().addSecs(-300).toString("yyyy-MM-dd hh:mm:00"))
                                                                      .arg(t2_input_item->ui->LE_au_after_weight->text())
                                                                      .arg(t2_input_item->ui->CB_domnumber->currentText());
            query_result = query.exec(query_str);

            if(!query_result){
                break;
            }

        }
    }
    au_calc(now_time);
    if(query_result){
        t2_input_item->clear_data();
        for(int i=0;i<ui->t2_input_lot_table->rowCount();i++){
            t2_lot_item_list.at(i)->clear_data();
        }
        QMessageBox msg;
        msg.addButton(QMessageBox::Ok);
        msg.setText(tr("input complete"));
        msg.exec();
//        int run_count = ui->t2_LE_Run_number->text().toInt();
//        run_count++;
//        ui->t2_LE_Run_number->setText(QString("%1").arg(run_count));
        on_t2_LE_Run_number_editingFinished();
        ui->t2_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
        ui->t2_search_time_start->setDateTime(ui->t2_search_time_end->dateTime().addDays(-2));
        t2_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
        on_t2_search_btn_clicked();
        ui->t2_data_table->scrollToBottom();
    }else {
        QMessageBox::warning(this, tr("conntion false"),
                                                       "server connection fail\n"
                                                          ""+my_mesdb.lastError().text(),
                                                            QMessageBox::Close);
    }
    query.exec(QString("select * from depostion_au_use_current_view where machine_name = '%1' AND batch_id = %2")
               .arg("CHA#2").arg(batch_id));
    if(query.next()){
        double  OKNG = query.value("OKNG").toDouble();
        if(abs(OKNG)>0.1){
            QMessageBox msg;
            msg.addButton(QMessageBox::Ok);
            msg.setText(tr("au pellet NG"));
            msg.exec();
        }
    }
    query.exec("update `Thin_film_spec_managerment` set au_pullet_add = 0 ,now_r_weight = 0,au_after_r_weight =0,au_after_weight=0 where machine_code = 'MD003'");
    ti_al_chart_init(t2_ti_chart,t2_al_chart,t2_ti_value_series,t2_al_value_series,t2_ti_UCL_value_series,t2_al_UCL_value_series,
                     t2_al_axisX,t2_al_axisY,t2_ti_axisX,t2_ti_axisY,"MD003");
}
void Thin_film_mainwindows::on_t2_search_btn_clicked()
{
    t2_data_model->setFilter(QString("vacuum_time Between '%1' AND '%2' AND machine_code = 'MD003'")
                          .arg(ui->t2_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t2_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    t2_data_model->select();
    t2_data_model->setSort(2,Qt::AscendingOrder);
    QSqlQuery query(my_mesdb);
    query.exec(QString("select sum((case when (`A`.`machine_code` = 'MD003') then `A`.`Wafer_count` end)) AS `wafer` from `Thin_film_data` `A` "
               "where vacuum_time Between '%1' AND '%2' AND machine_code = 'MD003'").arg(ui->t2_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
               .arg(ui->t2_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    if(query.next()){

        ui->t2_total_wafer->setText(query.value("wafer").toString());
    }
    ui->t2_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t2_BT_search_lot_id_clicked()
{
    t2_data_model->setFilter(QString("Lot_id = '%1' AND machine_code = 'MD003'")
                          .arg(ui->t2_LE_search_lot_id->text()));
    t2_data_model->select();
    t2_data_model->setSort(1,Qt::AscendingOrder);
}
void Thin_film_mainwindows::on_t2_ti_al_refresh_clicked()
{
    ti_al_chart_init(t2_ti_chart,t2_al_chart,t2_ti_value_series,t2_al_value_series,t2_ti_UCL_value_series,t2_al_UCL_value_series,
                     t2_al_axisX,t2_al_axisY,t2_ti_axisX,t2_ti_axisY,"MD003");
}
void Thin_film_mainwindows::on_t2_DT_del_btn_clicked()
{
    int count = ui->t2_data_table->selectionModel()->selectedIndexes().count();
    QVector<int> index_history;
    for(int i=0;i<count;i++){
        if(!index_history.contains(ui->t2_data_table->selectionModel()->selectedIndexes().at(i).row())){
        QDateTime Input_time2 = t2_data_model->record(ui->t2_data_table->selectionModel()->selectedIndexes().at(i).row()).value("Input_time").toDateTime();
        t2_data_model->removeRow(ui->t2_data_table->selectionModel()->selectedIndexes().at(i).row());
         au_calc(Input_time2);
         index_history.append(ui->t2_data_table->selectionModel()->selectedIndexes().at(i).row());
        }else {

        }
    }
    on_t2_search_btn_clicked();
    ui->t2_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t2_move_machine_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    QString batch_id_temp = "0";
    QString machine_name;
    QString machine_code;

    int count  = ui->t2_data_table->selectionModel()->selectedIndexes().count();
    for(int i=0;i<count;i++){
         QSqlRecord recode = t2_data_model->record(ui->t2_data_table->selectionModel()->selectedIndexes().at(i).row());
        if(batch_id_temp != recode.value("batch_id").toString()){
            query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_name = '%1'").arg(ui->t2_CB_move_machine->currentText()));
            if(query.next()){
                batch_id_temp = query.value("current_batch_id").toString()+5;
                machine_name = query.value("machine_name").toString();
                machine_code = query.value("machine_code").toString();
                query.exec(QString("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+10  where machine_name = '%1'").arg(ui->t2_CB_move_machine->currentText()));
            }
            batch_id_temp = recode.value("batch_id").toString();
        }

        recode.setGenerated("batch_id",true);
        recode.setGenerated("machine_name",true);
        recode.setGenerated("machine_code",true);
        recode.setValue("batch_id",batch_id_temp);
        recode.setValue("machine_name",machine_name);
        recode.setValue("machine_code",machine_code);
        t2_data_model->setRecord(ui->t2_data_table->selectionModel()->selectedIndexes().at(i).row(),recode);
    }
    on_t2_search_btn_clicked();
    ui->t2_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t2_excel_header_copy_flag_toggled(bool checked)
{
    ui->t2_data_table->copyheader_flag = checked;
}
void Thin_film_mainwindows::on_t2_machine_temp_run_mode_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    query.exec("update Thin_film_spec_managerment set nomal_mode_time = ADDTIME(NOW(),'0:5:0') where `machine_code`='MD003'");
}

//t2_function -- end --

//t3_function -- start --
void Thin_film_mainwindows::t3_init()
{
    ui->t3_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t3_search_time_start->setDateTime(ui->t3_search_time_end->dateTime().addDays(-2));

    QSqlTableModel *spec_model = new QSqlTableModel(this,my_mesdb);
    spec_model->setTable("Thin_film_spec_managerment");
    spec_model->setFilter("machine_code = 'MD008'");
    spec_model->select();
    spec_model->setHeaderData(4,Qt::Horizontal,tr("ashing_LCL"));
    spec_model->setHeaderData(5,Qt::Horizontal,tr("ashing_UCL"));
    spec_model->setHeaderData(6,Qt::Horizontal,tr("WLPTiAlashing_LCL"));
    spec_model->setHeaderData(7,Qt::Horizontal,tr("WLPTiAlashing_UCL"));
    spec_model->setHeaderData(8,Qt::Horizontal,tr("WLP_LCL_Ti_Au"));
    spec_model->setHeaderData(9,Qt::Horizontal,tr("WLP_UCL_Ti_Au"));
    spec_model->setHeaderData(10,Qt::Horizontal,tr("WLP_LCL_Ti_Al"));
    spec_model->setHeaderData(11,Qt::Horizontal,tr("WLP_UCL_Ti_Al"));
    spec_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    t3_data_model = new thin_sqltable_model(this,my_mesdb,"MD008",spec_model);

    t3_data_model->setTable("Thin_film_data");

    t3_data_model->setFilter(QString("Input_time Between '%1' AND '%2' AND machine_code = 'MD008'")
                          .arg(ui->t3_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t3_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    ui->t3_data_table->setItemDelegateForColumn(t3_data_model->fieldIndex("vacuum_time"),new DateDelegate(ui->t3_data_table));
    ui->t3_data_table->setItemDelegateForColumn(t3_data_model->fieldIndex("Input_time"),new DateDelegate(ui->t3_data_table));
    ui->t3_data_table->setModel(t3_data_model);
    ui->t3_data_table->setSortingEnabled(true);
    connect(t3_data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>))
            ,this,SLOT(t3_datamodel_dataChanged(QModelIndex,QModelIndex,QVector<int>)));
    ui->t3_data_table->setItemDelegateForColumn(10,new Thin_combodelegate(note_combo_list,ui->t3_data_table));

    t3_data_model->setEditStrategy(QSqlTableModel::OnFieldChange);
    t3_data_model->setSort(2,Qt::AscendingOrder);

    t3_data_model->select();

    t3_data_model->setHeaderData(t3_data_model->fieldIndex("day_and_night"),Qt::Horizontal,tr("day_and_night"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("vacuum_time"),Qt::Horizontal,tr("vacuum_time"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Run_number"),Qt::Horizontal,tr("thin_Run_number"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Resister"),Qt::Horizontal,tr("thin_Resister"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("machine_type"),Qt::Horizontal,tr("thin_machine_type"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Lot_id"),Qt::Horizontal,tr("thin_Lot_id"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Wafer_count"),Qt::Horizontal,tr("thin_Wafer_count"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Pad_thin"),Qt::Horizontal,tr("thin_Pad_thin"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Wlp_thin"),Qt::Horizontal,tr("thin_Wlp_thin"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Wafer_angle"),Qt::Horizontal,tr("thin_Wafer_angle"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Input_thin_data"),Qt::Horizontal,tr("thin_Input_thin_data"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Recipe"),Qt::Horizontal,tr("thin_Recipe"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("ashing1"),Qt::Horizontal,tr("thin_ashing1"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("ashing2"),Qt::Horizontal,tr("thin_ashing2"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("ashing3"),Qt::Horizontal,tr("thin_ashing3"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("ashing_machine"),Qt::Horizontal,tr("thin_ashing_machine"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("N2_Blow"),Qt::Horizontal,tr("thin_N2_Blow"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Au_f_add"),Qt::Horizontal,tr("thin_thin_Au_f_add"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Now_r_weight"),Qt::Horizontal,tr("thin_Now_r_weight"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Au_after_weight"),Qt::Horizontal,tr("thin_Au_after_weight"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Au_use"),Qt::Horizontal,tr("thin_Au_use"));
//    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Ti_add"),Qt::Horizontal,tr("thin_Ti_add"));
//    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Al_add"),Qt::Horizontal,tr("thin_Al_add"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("X_tal_number"),Qt::Horizontal,tr("thin_X_tal_number"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("X_tal_Hz"),Qt::Horizontal,tr("thin_X_tal_Hz"));
//    t3_data_model->setHeaderData(t3_data_model->fieldIndex("X_tal_life"),Qt::Horizontal,tr("thin_X_tal_life"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("User_name"),Qt::Horizontal,tr("thin_User_name"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("D_use"),Qt::Horizontal,tr("thin_D_use"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("note"),Qt::Horizontal,tr("thin_note"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Ti_total"),Qt::Horizontal,tr("thin_Ti_total"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Al_total"),Qt::Horizontal,tr("thin_Al_total"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Au_total"),Qt::Horizontal,tr("thin_Au_total"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("Input_time"),Qt::Horizontal,tr("thin_Input_time"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("machine_name"),Qt::Horizontal,tr("thin_machine_name"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("machine_code"),Qt::Horizontal,tr("thin_machine_code"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("batch_id"),Qt::Horizontal,tr("thin_batch_id"));
    t3_data_model->setHeaderData(t3_data_model->fieldIndex("cleaning_cal_flag"),Qt::Horizontal,tr("thin_cleaning_cal_flag"));
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("day_and_night"),50);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("vacuum_time"),110);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Run_number"),60);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Resister"),50);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("machine_type"),100);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Lot_id"),80);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Wafer_count"),50);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Pad_thin"),80);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Wlp_thin"),80);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Wafer_angle"),80);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Input_thin_data"),150);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Recipe"),150);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("ashing1"),65);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("ashing2"),65);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("ashing3"),65);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("ashing_machine"),80);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("N2_Blow"),80);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Au_f_add"),80);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Now_r_weight"),120);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Au_after_weight"),140);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Au_use"),50);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Ti_add"),50);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Al_add"),50);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("X_tal_number"),50);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("X_tal_Hz"),50);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("X_tal_life"),80);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("User_name"),80);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("D_use"),80);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("note"),140);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Ti_total"),60);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Al_total"),60);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Au_total"),60);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("Input_time"),120);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("machine_name"),50);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("machine_code"),50);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("batch_id"),50);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("cleaning_cal_flag"),50);
    ui->t3_data_table->horizontalHeader()->resizeSection(t3_data_model->fieldIndex("dom"),50);
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("X_tal_number"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("X_tal_Hz"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("machine_code"));
//    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("batch_id"));
//    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("cleaning_cal_flag"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("Au_use"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("au_pellet_output"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("au_add"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("check_user"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("au_pellet_total"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("work_start_au"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("au_add_weight"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("au_case_weight"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("check_NG"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("Run_number"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("Pad_thin"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("Wlp_thin"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("N2_Blow"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("D_use"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("Ti_add"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("Al_add"));
    ui->t3_data_table->horizontalHeader()->hideSection(t3_data_model->fieldIndex("X_tal_life"));

    ui->t3_data_table->scrollToBottom();


    ui->t3_spec_table->setModel(spec_model);
    ui->t3_spec_table->horizontalHeader()->hideSection(0);
    ui->t3_spec_table->horizontalHeader()->hideSection(1);
    ui->t3_spec_table->horizontalHeader()->hideSection(2);
    ui->t3_spec_table->horizontalHeader()->hideSection(3);
    ui->t3_spec_table->horizontalHeader()->hideSection(4);
    ui->t3_spec_table->horizontalHeader()->hideSection(5);
    ui->t3_spec_table->horizontalHeader()->hideSection(6);
    ui->t3_spec_table->horizontalHeader()->hideSection(7);
    ui->t3_spec_table->horizontalHeader()->hideSection(8);
    ui->t3_spec_table->horizontalHeader()->hideSection(9);
    ui->t3_spec_table->horizontalHeader()->hideSection(10);
    ui->t3_spec_table->horizontalHeader()->hideSection(11);
    ui->t3_spec_table->horizontalHeader()->hideSection(12);
    ui->t3_spec_table->horizontalHeader()->hideSection(13);
    ui->t3_spec_table->horizontalHeader()->hideSection(14);
    ui->t3_spec_table->horizontalHeader()->hideSection(17);
    ui->t3_spec_table->horizontalHeader()->hideSection(18);
    ui->t3_spec_table->horizontalHeader()->hideSection(19);
    ui->t3_spec_table->horizontalHeader()->hideSection(20);
    ui->t3_spec_table->horizontalHeader()->hideSection(21);
    ui->t3_spec_table->horizontalHeader()->hideSection(22);
    ui->t3_spec_table->horizontalHeader()->hideSection(23);
    ui->t3_spec_table->horizontalHeader()->hideSection(24);
    ui->t3_spec_table->horizontalHeader()->hideSection(25);
    ui->t3_spec_table->horizontalHeader()->hideSection(26);
    ui->t3_spec_table->horizontalHeader()->hideSection(27);
    ui->t3_spec_table->horizontalHeader()->hideSection(28);
    ui->t3_spec_table->horizontalHeader()->hideSection(29);
    ui->t3_spec_table->horizontalHeader()->hideSection(30);
    ui->t3_spec_table->horizontalHeader()->hideSection(31);
    ui->t3_spec_table->horizontalHeader()->hideSection(32);
    ui->t3_spec_table->horizontalHeader()->hideSection(33);
    ui->t3_spec_table->horizontalHeader()->hideSection(34);
    ui->t3_spec_table->horizontalHeader()->hideSection(35);
    ui->t3_spec_table->horizontalHeader()->resizeSection(6,150);
    ui->t3_spec_table->horizontalHeader()->resizeSection(7,150);

    for(int i=0;i<8;i++){
        ui->t3_input_table->insertColumn(i);
    }
    ui->t3_input_table->insertRow(0);
    ui->t3_input_table->insertRow(1);
    ui->t3_input_table->insertRow(2);
    ui->t3_input_table->insertRow(3);
    ui->t3_input_table->insertRow(4);
    ui->t3_input_table->insertRow(5);
    ui->t3_input_table->insertRow(6);
    ui->t3_input_table->horizontalHeader()->resizeSection(0,70);
    ui->t3_input_table->horizontalHeader()->resizeSection(1,150);
    ui->t3_input_table->horizontalHeader()->resizeSection(3,70);
    ui->t3_input_table->horizontalHeader()->resizeSection(5,130);
    ui->t3_input_table->horizontalHeader()->resizeSection(6,140);
    ui->t3_input_table->horizontalHeader()->resizeSection(7,80);
    t3_input_item = new input_item(my_mesdb,"MD008");
    ui->t3_input_table->setCellWidget(0,0,t3_input_item->ui->LA_day_and_night);
    ui->t3_input_table->setCellWidget(1,0,t3_input_item->ui->CB_day_and_night);
    ui->t3_input_table->setCellWidget(0,1,t3_input_item->ui->LA_vacuum_time);
    ui->t3_input_table->setCellWidget(1,1,t3_input_item->ui->DT_vacuum_time);
    t3_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
    ui->t3_input_table->setCellWidget(0,2,t3_input_item->ui->LA_resister);
    ui->t3_input_table->setCellWidget(1,2,t3_input_item->ui->LE_resister);
//    ui->t3_input_table->setCellWidget(0,3,t3_input_item->ui->LA_N2_blow);
//    ui->t3_input_table->setCellWidget(1,3,t3_input_item->ui->WCB_N2_blow);
    ui->t3_input_table->setCellWidget(0,4,t3_input_item->ui->LA_au_f_add);
    ui->t3_input_table->setCellWidget(1,4,t3_input_item->ui->LE_au_f_add);
    ui->t3_input_table->setCellWidget(0,5,t3_input_item->ui->LA_now_r_weight);
    ui->t3_input_table->setCellWidget(1,5,t3_input_item->ui->LE_now_r_weight);
    ui->t3_input_table->setCellWidget(0,6,t3_input_item->ui->LA_au_after_r_weight);
    ui->t3_input_table->setCellWidget(1,6,t3_input_item->ui->LE_au_after_r_weight);
    ui->t3_input_table->setCellWidget(0,7,t3_input_item->ui->LA_au_after_weight);
    ui->t3_input_table->setCellWidget(1,7,t3_input_item->ui->LE_au_after_weight);
//    ui->t3_input_table->setCellWidget(0,7,t3_input_item->ui->LA_au_use);
//    ui->t3_input_table->setCellWidget(1,7,t3_input_item->ui->LE_au_use);
//    ui->t3_input_table->setCellWidget(2,0,t3_input_item->ui->LA_ti_add);
//    ui->t3_input_table->setCellWidget(3,0,t3_input_item->ui->LE_ti_add);
//    ui->t3_input_table->setCellWidget(2,1,t3_input_item->ui->LA_al_add);
//    ui->t3_input_table->setCellWidget(3,1,t3_input_item->ui->LE_al_add);
//    ui->t3_input_table->setCellWidget(2,2,t3_input_item->ui->LA_xtal_life);
//    ui->t3_input_table->setCellWidget(3,2,t3_input_item->ui->LE_xtal_life);
    ui->t3_input_table->setCellWidget(2,3,t3_input_item->ui->LA_name);
    ui->t3_input_table->setCellWidget(3,3,t3_input_item->ui->LE_name);
//    ui->t3_input_table->setCellWidget(2,4,t3_input_item->ui->LA_d_run);
//    ui->t3_input_table->setCellWidget(3,4,t3_input_item->ui->WCB_d_run);
    ui->t3_input_table->setCellWidget(2,5,t3_input_item->ui->LA_note);
    ui->t3_input_table->setCellWidget(3,5,t3_input_item->ui->LE_note);
    ui->t3_input_table->setSpan(2,5,1,5);
    ui->t3_input_table->setSpan(3,5,1,5);
    ui->t3_input_table->setCellWidget(4,0,t3_input_item->ui->LE_recipe_number);
    ui->t3_input_table->setCellWidget(4,1,t3_input_item->ui->CB_recipe_choice);
//    ui->t3_input_table->setCellWidget(4,3,t3_input_item->ui->LA_recipe_2);
    ui->t3_input_table->setCellWidget(5,0,t3_input_item->ui->W_recipe);
    ui->t3_input_table->setSpan(4,1,1,5);
//    ui->t3_input_table->setSpan(4,3,1,2);
    ui->t3_input_table->setSpan(5,0,2,5);
    ui->t3_input_table->setCellWidget(4,6,t3_input_item->ui->LA_dom_number);
    ui->t3_input_table->setCellWidget(5,6,t3_input_item->ui->CB_domnumber);

    ui->t3_LE_Lot_count->setValidator(new QIntValidator(0, 1000, this));
//    ui->t3_LE_Run_number->setValidator(new QIntValidator(0, 1000, this));
    QSqlQuery query(my_mesdb);
    query.exec(QString("select `Lot_max_count`,`Run_number_count` from Thin_film_spec_managerment where machine_code = 'MD008'"));
    if(query.next()){
        ui->t3_LE_Lot_count->setText(query.value("Lot_max_count").toString());
//        ui->t3_LE_Run_number->setText(query.value("Run_number_count").toString());
    }
    t3_lot_item_list.clear();
    for(int i=0;i<ui->t3_LE_Lot_count->text().toInt();i++){

        input_lot_item *lot_item = new input_lot_item(ms_mesdb,t3_input_item,"MD008");
        t3_lot_item_list.append(lot_item);
        ui->t3_input_lot_table->insertRow(i);
        ui->t3_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
        ui->t3_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
        ui->t3_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
        ui->t3_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
        ui->t3_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
        ui->t3_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
        ui->t3_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
        ui->t3_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//        connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                t3_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
    }
    query.exec("SELECT * FROM `Thin_film_repcipe` where machine_code = 'MD008' ORDER BY `recipe_number` ASC  ");
    t3_input_item->ui->CB_recipe_choice->addItem(tr("recipe_choice"));
    while(query.next()){
        QString recipe_name = query.value("recipe_number").toString()+"."+ query.value("recipename").toString()
                                + "|"+query.value("metal_name").toString()+"|"+query.value("metal_thin").toString()+"|"
                                +query.value("type").toString();
        QPixmap pixmap(100,100);
        pixmap.fill(QColor(query.value("color").toString()));
        t3_input_item->ui->CB_recipe_choice->addItem(QIcon(pixmap),recipe_name);
    }
    connect(t3_input_item->ui->CB_recipe_choice,SIGNAL(currentIndexChanged(int)),this,SLOT(t3_recipe_choice(int)));

    t3_ti_chart = new Thin_film_chart();
    t3_ti_chartview = new Thin_film_chart_view(t3_ti_chart);
    connect(t3_ti_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t3_ti_y_value_slot(QPointF)));
    ui->t3_Ti_layout->addWidget(t3_ti_chartview);

    t3_al_chart = new Thin_film_chart();
    t3_al_chartview = new Thin_film_chart_view(t3_al_chart);
    connect(t3_al_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t3_al_y_value_slot(QPointF)));
    ui->t3_Al_layout->addWidget(t3_al_chartview);
    t3_ti_axisX = new QDateTimeAxis();
    t3_ti_axisX->setFormat("MM-dd HH:mm:ss");
    t3_ti_axisX->setTitleText("Date");
    t3_ti_axisX->setVisible(true);
    t3_ti_axisX->setTitleVisible(true);
    t3_ti_chart->addAxis(t3_ti_axisX, Qt::AlignBottom);
    t3_ti_axisY = new QValueAxis();
    t3_ti_chart->addAxis(t3_ti_axisY,Qt::AlignLeft);

    t3_al_axisX = new QDateTimeAxis();
    t3_al_axisX->setFormat("MM-dd HH:mm:ss");
    t3_al_axisX->setTitleText("Date");
    t3_al_axisX->setVisible(true);
    t3_al_axisX->setTitleVisible(true);
    t3_al_chart->addAxis(t3_al_axisX, Qt::AlignBottom);
    t3_al_axisY = new QValueAxis();
    t3_al_chart->addAxis(t3_al_axisY,Qt::AlignLeft);

    ti_al_chart_init(t3_ti_chart,t3_al_chart,t3_ti_value_series,t3_al_value_series,t3_ti_UCL_value_series,t3_al_UCL_value_series,
                     t3_al_axisX,t3_al_axisY,t3_ti_axisX,t3_ti_axisY,"MD008");
    query.exec("SELECT machine_name,machine_code FROM Thin_film_spec_managerment");
    while(query.next()){
        ui->t3_CB_move_machine->addItem(query.value("machine_name").toString());
    }
    temp_run_widget_map.insert("MD008",ui->t3_temp_run_mode_time);
    temp_run_ti_accmulate_widget_map.insert("MD008",ui->t3_ti_accmulate);
    temp_run_al_accmulate_widget_map.insert("MD008",ui->t3_al_accmulate);
    ui->t3_CB_wet_insert->addItems(wet_machine_list);

    sync_now_date("MD008",t3_input_item->ui->LE_au_f_add,t3_input_item->ui->LE_now_r_weight,t3_input_item->ui->LE_au_after_r_weight
                  ,t3_input_item->ui->LE_au_after_weight);
    ui->t3_data_table->selectionModel()->setObjectName("t3_data");
    connect(ui->t3_data_table->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this,SLOT(table_view_selection_change(QModelIndex,QModelIndex)));
}
void Thin_film_mainwindows::t3_ti_y_value_slot(QPointF value)
{
    ui->t3_ti_y_value->setText(QString("%1").arg(value.y()));
}
void Thin_film_mainwindows::t3_al_y_value_slot(QPointF value)
{
    ui->t3_al_y_value->setText(QString("%1").arg(value.y()));
}
void Thin_film_mainwindows::t3_datamodel_dataChanged(QModelIndex index1, QModelIndex index2, QVector<int> vector)
{
     v1_data_model_change(index1,t3_data_model,"MD008");
     ui->t3_data_table->scrollToBottom();
}
void Thin_film_mainwindows::t3_recipe_choice(int idx)
{
    if(idx == 0){
        t3_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t3_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t3_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t3_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t3_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t3_input_item->ui->LE_Recipe_name1->setText("");
        t3_input_item->ui->LE_Recipe_name2->setText("");
        t3_input_item->ui->LE_Recipe_name3->setText("");
        t3_input_item->ui->LE_Recipe_name4->setText("");
        t3_input_item->ui->LE_Recipe_name5->setText("");
        return ;
    }
    QSqlQuery query(my_mesdb);
    query.exec(QString("select * from `Thin_film_repcipe` where recipe_number  = %1 AND machine_code = 'MD008'").arg(idx));

    if(query.next()){
        t3_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t3_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t3_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t3_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t3_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t3_input_item->ui->LE_Recipe_name1->setText("");
        t3_input_item->ui->LE_Recipe_name2->setText("");
        t3_input_item->ui->LE_Recipe_name3->setText("");
        t3_input_item->ui->LE_Recipe_name4->setText("");
        t3_input_item->ui->LE_Recipe_name5->setText("");
        if(query.value("metal_name").toString() != ""){
            QString item_data = query.value("metal_name").toString();
            QStringList item_list = item_data.split("/");
            for(int i=0;i<item_list.count();i++){
                if(i==0){
                    t3_input_item->ui->CB_Recipe_name1->setCurrentText(item_list.at(i));
                }else if(i==1){
                    t3_input_item->ui->CB_Recipe_name2->setCurrentText(item_list.at(i));
                }else if(i==2){
                    t3_input_item->ui->CB_Recipe_name3->setCurrentText(item_list.at(i));
                }else if(i==3){
                    t3_input_item->ui->CB_Recipe_name4->setCurrentText(item_list.at(i));
                }else if(i==4){
                    t3_input_item->ui->CB_Recipe_name5->setCurrentText(item_list.at(i));
                }
            }
        }
        if(query.value("metal_thin").toString() != ""){
            QStringList recipe_item = query.value("metal_thin").toString().split("/");
            if(recipe_item.count()>0){
                t3_input_item->ui->LE_Recipe_name1->setText(QString("%1").arg(recipe_item[0]));
            }
            if(recipe_item.count()>1){
                t3_input_item->ui->LE_Recipe_name2->setText(QString("%1").arg(recipe_item[1]));
            }
            if(recipe_item.count()>2){
                t3_input_item->ui->LE_Recipe_name3->setText(QString("%1").arg(recipe_item[2]));
            }
            if(recipe_item.count()>3){
                t3_input_item->ui->LE_Recipe_name4->setText(QString("%1").arg(recipe_item[3]));
            }
            if(recipe_item.count()>4){
                t3_input_item->ui->LE_Recipe_name5->setText(QString("%1").arg(recipe_item[4]));
            }
        }
    }
}
void Thin_film_mainwindows::on_t3_LE_Lot_count_editingFinished()
{
//    if(ui->t3_LE_Lot_count->text()!= ""){
//        QSqlQuery query(my_mesdb);
//        query.exec(QString("update `Thin_film_spec_managerment` SET `Lot_max_count`='%1' Where `machine_code`='MD008'").arg(ui->t3_LE_Lot_count->text()));
//        while(ui->t3_input_lot_table->rowCount()>0){
//            ui->t3_input_lot_table->removeRow(0);
//        }
//        t3_lot_item_list.clear();
//        for(int i=0;i<ui->t3_LE_Lot_count->text().toInt();i++){

//            input_lot_item *lot_item = new input_lot_item(ms_mesdb,"MD008");
//            t3_lot_item_list.append(lot_item);
//            ui->t3_input_lot_table->insertRow(i);
//            ui->t3_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//    //        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
//            ui->t3_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
//            ui->t3_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
//            ui->t3_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
//            ui->t3_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
//            ui->t3_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
//            ui->t3_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
//            ui->t3_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//            connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                    t3_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
//        }
//    }
}
void Thin_film_mainwindows::on_t3_LE_Run_number_editingFinished()
{
//    if(ui->t3_LE_Run_number->text() != ""){
//        QSqlQuery query(my_mesdb);
//        query.exec(QString("update `Thin_film_spec_managerment` SET `Run_number_count`='%1' Where `machine_code`='MD008'").arg(ui->t3_LE_Run_number->text()));
//    }
}
void Thin_film_mainwindows::on_t3_Input_btn_clicked()
{
//    QString Run_number = ui->t3_LE_Run_number->text();
//    if(Run_number.toInt() > 60){
//        ui->t3_LE_Run_number->setText("1");
//        QMessageBox msg;
//        msg.addButton(QMessageBox::Ok);
//        msg.setText(tr("60 over set 1"));
//        msg.exec();
//        on_t3_LE_Run_number_editingFinished();
//    }
    QString Lot_count = ui->t3_LE_Lot_count->text();
    QString resister = t3_input_item->ui->LE_resister->text();
//    QString N2_blow = QString("%1").arg(QVariant(t3_input_item->ui->CB_N2_blow->isChecked()).toInt());
    QString au_f_add = t3_input_item->ui->LE_au_f_add->text();
    QString now_r_weight = t3_input_item->ui->LE_now_r_weight->text();
    QString au_after_r_weight = t3_input_item->ui->LE_au_after_r_weight->text();
    QString au_use =  t3_input_item->ui->LE_au_use->text();
    QString ti_add =  t3_input_item->ui->LE_ti_add->text();
    QString al_add =  t3_input_item->ui->LE_al_add->text();
    QString xtal_life =  t3_input_item->ui->LE_xtal_life->text();
    QString name =  t3_input_item->ui->LE_name->text();
//    QString d_use =  QString("%1").arg(QVariant(t3_input_item->ui->CB_d_run->isChecked()).toInt());

    QString note =  t3_input_item->ui->LE_note->text();

    ui->t3_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t3_search_time_start->setDateTime(ui->t3_search_time_end->dateTime().addDays(-2));

    QString Recipe_total_text;
    QString Recipe_total_name_text;
    QString Recipe_name1 = t3_input_item->ui->LE_Recipe_name1->text();
    QString Recipe_name2 = t3_input_item->ui->LE_Recipe_name2->text();
    QString Recipe_name3 = t3_input_item->ui->LE_Recipe_name3->text();
    QString Recipe_name4 = t3_input_item->ui->LE_Recipe_name4->text();
    QString Recipe_name5 = t3_input_item->ui->LE_Recipe_name5->text();
    QString CB_Recipe_name1 = t3_input_item->ui->CB_Recipe_name1->currentText();
    QString CB_Recipe_name2 = t3_input_item->ui->CB_Recipe_name2->currentText();
    QString CB_Recipe_name3 = t3_input_item->ui->CB_Recipe_name3->currentText();
    QString CB_Recipe_name4 = t3_input_item->ui->CB_Recipe_name4->currentText();
    QString CB_Recipe_name5 = t3_input_item->ui->CB_Recipe_name5->currentText();
    int Recipe_count = 0;
    int ti_count = 0;
    int au_count = 0;
    int al_count = 0;

    if(CB_Recipe_name1 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name1));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name1));
        if(CB_Recipe_name1=="Ti"){
            ti_count += Recipe_name1.toInt();
        }else if(CB_Recipe_name1=="Al"){
            al_count += Recipe_name1.toInt();
        }else if(CB_Recipe_name1=="Au"){
            au_count += Recipe_name1.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name2 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name2));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name2));
        if(CB_Recipe_name2=="Ti"){
            ti_count += Recipe_name2.toInt();
        }else if(CB_Recipe_name2=="Al"){
            al_count += Recipe_name2.toInt();
        }else if(CB_Recipe_name2=="Au"){
            au_count += Recipe_name2.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name3 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name3));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name3));
        if(CB_Recipe_name3=="Ti"){
            ti_count += Recipe_name3.toInt();
        }else if(CB_Recipe_name3=="Al"){
            al_count += Recipe_name3.toInt();
        }else if(CB_Recipe_name3=="Au"){
            au_count += Recipe_name3.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name4 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name4));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name4));
        if(CB_Recipe_name4=="Ti"){
            ti_count += Recipe_name4.toInt();
        }else if(CB_Recipe_name4=="Al"){
            al_count += Recipe_name4.toInt();
        }else if(CB_Recipe_name4=="Au"){
            au_count += Recipe_name4.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name5 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name5));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name5));
        if(CB_Recipe_name5=="Ti"){
            ti_count += Recipe_name5.toInt();
        }else if(CB_Recipe_name5=="Al"){
            al_count += Recipe_name5.toInt();
        }else if(CB_Recipe_name5=="Au"){
            au_count += Recipe_name5.toInt();
        }
        Recipe_count++;
    }
    if(Recipe_count>0){
        Recipe_total_text.remove(Recipe_total_text.length()-1,1);
        Recipe_total_name_text.remove(Recipe_total_name_text.length()-1,1);
    }
    QSqlQuery query(my_mesdb);
    QString batch_id;
    query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_code = 'MD008'"));
    if(query.next()){
        batch_id = query.value("current_batch_id").toString();
        query.exec("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+1  where machine_code = 'MD008'");
    }
    bool query_result = false;
    qDebug()<<ui->t3_input_lot_table->rowCount();
    query.exec("select now() as now_time");
    QDateTime now_time;

    now_time = QDateTime::currentDateTime();
    for(int i=0;i<ui->t3_input_lot_table->rowCount();i++){
        QString Lot_id = t3_lot_item_list.at(i)->ui->LE_Lot_id->text();
        QString wafer_count = t3_lot_item_list.at(i)->ui->LE_wafer_count->text();
        QString ashing1 = t3_lot_item_list.at(i)->ui->LE_ashing1->text();
        QString ashing2 = t3_lot_item_list.at(i)->ui->LE_ashing2->text();
        QString ashing3 = t3_lot_item_list.at(i)->ui->LE_ashing3->text();
        QString ashing_machine = t3_lot_item_list.at(i)->ui->LE_ashing_machine->text();
        QString machine_type = t3_lot_item_list.at(i)->ui->LE_machine_type->text();
        QString wafer_angle = t3_lot_item_list.at(i)->ui->LE_wafer_angle->text();
        if(Lot_id.trimmed() !=""){
            QString query_str = QString("INSERT INTO `Thin_film_data` "
                                        "(`Run_number`, `Resister`, `machine_type`, `Lot_id`, `Wafer_count`, "
                                        "`Wafer_angle`, `Input_thin_data`, `ashing1`, `ashing2`, "
                                        "`ashing3`, `ashing_machine`, `Pad_thin`, `Wlp_thin`, "
                                        "`N2_Blow`, `Au_f_add`, `Now_r_weight`, `Au_after_weight`, "
                                        "`Au_use`, `Recipe`, `Ti_add`, `Al_add`, `X_tal_number`, "
                                        "`X_tal_life`, `User_name`, `D_use`, `note`, `Ti_total`, "
                                        "`Al_total`, `Au_total`, `batch_id`, `cleaning_cal_flag`,`machine_code`,`machine_name`,"
                                        "`Input_time`,`vacuum_time`,`day_and_night`,`au_pellet_output`,`au_add_weight`,`dom`) "
                                        "VALUES "
                                        "('%1', '%2', '%3', '%4', '%5', '%6', "
                                        "'%7', '%8', '%9', '%10', "
                                        "'%11', '%12', '%13', '%14', '%15', '%16', "
                                        "'%17', '%18', '%19', '%20', '%21', '%22', "
                                        "'%23', '%24', '%25', '%26', '%27', "
                                        "'%28', '%29', '%30' ,'%31','%32','%33','%34','%35','%36','%37','%38','%39' );").arg("").arg(resister).arg(machine_type).arg(Lot_id).arg(wafer_count)
                                                                      .arg(wafer_angle).arg(Recipe_total_text).arg(ashing1).arg(ashing2)
                                                                      .arg(ashing3).arg(ashing_machine).arg(QString("0")).arg(QString("0"))
                                                                      .arg("").arg(au_f_add).arg(now_r_weight).arg(au_after_r_weight)
                                                                      .arg(au_use).arg(Recipe_total_name_text).arg(ti_add).arg(al_add).arg(QString("0"))
                                                                      .arg(xtal_life).arg(name).arg("").arg(note).arg(ti_count)
                                                                      .arg(al_count).arg(au_count).arg(batch_id).arg(QString("0")).arg("MD008").arg("CHA#3")
                                                                      .arg(now_time.toString("yyyy-MM-dd hh:mm:ss"))
                                                                      .arg(t3_input_item->ui->DT_vacuum_time->dateTime().toString("yyyy-MM-dd hh:mm:00"))
                                                                      .arg(t3_input_item->ui->CB_day_and_night->currentText())
                                                                      .arg(t3_input_item->ui->DT_vacuum_time->dateTime().addSecs(-300).toString("yyyy-MM-dd hh:mm:00"))
                                                                      .arg(t3_input_item->ui->LE_au_after_weight->text())
                                                                      .arg(t3_input_item->ui->CB_domnumber->currentText());
            query_result = query.exec(query_str);

            if(!query_result){
                break;
            }

        }
    }
    au_calc(now_time);
    if(query_result){
        t3_input_item->clear_data();
        for(int i=0;i<ui->t3_input_lot_table->rowCount();i++){
            t3_lot_item_list.at(i)->clear_data();
        }
        QMessageBox msg;
        msg.addButton(QMessageBox::Ok);
        msg.setText(tr("input complete"));
        msg.exec();
//        int run_count = ui->t3_LE_Run_number->text().toInt();
//        run_count++;
//        ui->t3_LE_Run_number->setText(QString("%1").arg(run_count));
        on_t3_LE_Run_number_editingFinished();
        ui->t3_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
        ui->t3_search_time_start->setDateTime(ui->t3_search_time_end->dateTime().addDays(-2));
        t3_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
        on_t3_search_btn_clicked();
        ui->t3_data_table->scrollToBottom();
    }else {
        QMessageBox::warning(this, tr("conntion false"),
                                                       "server connection fail\n"
                                                          ""+my_mesdb.lastError().text(),
                                                            QMessageBox::Close);
    }
    query.exec(QString("select * from depostion_au_use_current_view where machine_name = '%1' AND batch_id = %2")
               .arg("CHA#3").arg(batch_id));
    if(query.next()){
        double  OKNG = query.value("OKNG").toDouble();
        if(abs(OKNG)>0.1){
            QMessageBox msg;
            msg.addButton(QMessageBox::Ok);
            msg.setText(tr("au pellet NG"));
            msg.exec();
        }
    }
    query.exec("update `Thin_film_spec_managerment` set au_pullet_add = 0 ,now_r_weight = 0,au_after_r_weight =0,au_after_weight=0 where machine_code = 'MD008'");
    ti_al_chart_init(t3_ti_chart,t3_al_chart,t3_ti_value_series,t3_al_value_series,t3_ti_UCL_value_series,t3_al_UCL_value_series,
                     t3_al_axisX,t3_al_axisY,t3_ti_axisX,t3_ti_axisY,"MD008");
}
void Thin_film_mainwindows::on_t3_search_btn_clicked()
{
    t3_data_model->setFilter(QString("vacuum_time Between '%1' AND '%2' AND machine_code = 'MD008'")
                          .arg(ui->t3_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t3_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    t3_data_model->select();
    t3_data_model->setSort(2,Qt::AscendingOrder);
    QSqlQuery query(my_mesdb);
    query.exec(QString("select sum((case when (`A`.`machine_code` = 'MD008') then `A`.`Wafer_count` end)) AS `wafer` from `Thin_film_data` `A` "
               "where vacuum_time Between '%1' AND '%2' AND machine_code = 'MD008'").arg(ui->t3_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
               .arg(ui->t3_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    if(query.next()){

        ui->t3_total_wafer->setText(query.value("wafer").toString());
    }
    ui->t3_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t3_BT_search_lot_id_clicked()
{
    t3_data_model->setFilter(QString("Lot_id = '%1' AND machine_code = 'MD008'")
                          .arg(ui->t3_LE_search_lot_id->text()));
    t3_data_model->select();
    t3_data_model->setSort(1,Qt::AscendingOrder);
}
void Thin_film_mainwindows::on_t3_ti_al_refresh_clicked()
{
    ti_al_chart_init(t3_ti_chart,t3_al_chart,t3_ti_value_series,t3_al_value_series,t3_ti_UCL_value_series,t3_al_UCL_value_series,
                     t3_al_axisX,t3_al_axisY,t3_ti_axisX,t3_ti_axisY,"MD008");
}
void Thin_film_mainwindows::on_t3_DT_del_btn_clicked()
{
    int count = ui->t3_data_table->selectionModel()->selectedIndexes().count();
    QVector<int> index_history;
    for(int i=0;i<count;i++){
        if(!index_history.contains(ui->t3_data_table->selectionModel()->selectedIndexes().at(i).row())){
        QDateTime Input_time2 = t3_data_model->record(ui->t3_data_table->selectionModel()->selectedIndexes().at(i).row()).value("Input_time").toDateTime();
        t3_data_model->removeRow(ui->t3_data_table->selectionModel()->selectedIndexes().at(i).row());
         au_calc(Input_time2);
         index_history.append(ui->t3_data_table->selectionModel()->selectedIndexes().at(i).row());
        }else {

        }
    }
    on_t3_search_btn_clicked();
    ui->t3_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t3_move_machine_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    QString batch_id_temp = "0";
    QString machine_name;
    QString machine_code;

    int count  = ui->t3_data_table->selectionModel()->selectedIndexes().count();
    for(int i=0;i<count;i++){
         QSqlRecord recode = t3_data_model->record(ui->t3_data_table->selectionModel()->selectedIndexes().at(i).row());
        if(batch_id_temp != recode.value("batch_id").toString()){
            query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_name = '%1'").arg(ui->t3_CB_move_machine->currentText()));
            if(query.next()){
                batch_id_temp = query.value("current_batch_id").toString()+5;
                machine_name = query.value("machine_name").toString();
                machine_code = query.value("machine_code").toString();
                query.exec(QString("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+10  where machine_name = '%1'").arg(ui->t3_CB_move_machine->currentText()));
            }
            batch_id_temp = recode.value("batch_id").toString();
        }

        recode.setGenerated("batch_id",true);
        recode.setGenerated("machine_name",true);
        recode.setGenerated("machine_code",true);
        recode.setValue("batch_id",batch_id_temp);
        recode.setValue("machine_name",machine_name);
        recode.setValue("machine_code",machine_code);
        t3_data_model->setRecord(ui->t3_data_table->selectionModel()->selectedIndexes().at(i).row(),recode);
    }
    on_t3_search_btn_clicked();
    ui->t3_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t3_excel_header_copy_flag_toggled(bool checked)
{
    ui->t3_data_table->copyheader_flag = checked;
}
void Thin_film_mainwindows::on_t3_machine_temp_run_mode_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    query.exec("update Thin_film_spec_managerment set nomal_mode_time = ADDTIME(NOW(),'0:5:0') where `machine_code`='MD008'");
}

//t3_function -- end --

//t4_function -- start --
void Thin_film_mainwindows::t4_init()
{
    ui->t4_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t4_search_time_start->setDateTime(ui->t4_search_time_end->dateTime().addDays(-2));

    QSqlTableModel *spec_model = new QSqlTableModel(this,my_mesdb);
    spec_model->setTable("Thin_film_spec_managerment");
    spec_model->setFilter("machine_code = 'MD009'");
    spec_model->select();
    spec_model->setHeaderData(4,Qt::Horizontal,tr("ashing_LCL"));
    spec_model->setHeaderData(5,Qt::Horizontal,tr("ashing_UCL"));
    spec_model->setHeaderData(6,Qt::Horizontal,tr("WLPTiAlashing_LCL"));
    spec_model->setHeaderData(7,Qt::Horizontal,tr("WLPTiAlashing_UCL"));
    spec_model->setHeaderData(8,Qt::Horizontal,tr("WLP_LCL_Ti_Au"));
    spec_model->setHeaderData(9,Qt::Horizontal,tr("WLP_UCL_Ti_Au"));
    spec_model->setHeaderData(10,Qt::Horizontal,tr("WLP_LCL_Ti_Al"));
    spec_model->setHeaderData(11,Qt::Horizontal,tr("WLP_UCL_Ti_Al"));
    spec_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    t4_data_model = new thin_sqltable_model(this,my_mesdb,"MD009",spec_model);

    t4_data_model->setTable("Thin_film_data");

    t4_data_model->setFilter(QString("Input_time Between '%1' AND '%2' AND machine_code = 'MD009'")
                          .arg(ui->t4_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t4_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    ui->t4_data_table->setItemDelegateForColumn(t4_data_model->fieldIndex("vacuum_time"),new DateDelegate(ui->t4_data_table));
    ui->t4_data_table->setItemDelegateForColumn(t4_data_model->fieldIndex("Input_time"),new DateDelegate(ui->t4_data_table));
    ui->t4_data_table->setModel(t4_data_model);
    ui->t4_data_table->setSortingEnabled(true);
    connect(t4_data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>))
            ,this,SLOT(t4_datamodel_dataChanged(QModelIndex,QModelIndex,QVector<int>)));
    ui->t4_data_table->setItemDelegateForColumn(10,new Thin_combodelegate(note_combo_list,ui->t4_data_table));

    t4_data_model->setEditStrategy(QSqlTableModel::OnFieldChange);
    t4_data_model->setSort(2,Qt::AscendingOrder);

    t4_data_model->select();

    t4_data_model->setHeaderData(t4_data_model->fieldIndex("day_and_night"),Qt::Horizontal,tr("day_and_night"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("vacuum_time"),Qt::Horizontal,tr("vacuum_time"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Run_number"),Qt::Horizontal,tr("thin_Run_number"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Resister"),Qt::Horizontal,tr("thin_Resister"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("machine_type"),Qt::Horizontal,tr("thin_machine_type"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Lot_id"),Qt::Horizontal,tr("thin_Lot_id"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Wafer_count"),Qt::Horizontal,tr("thin_Wafer_count"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Pad_thin"),Qt::Horizontal,tr("thin_Pad_thin"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Wlp_thin"),Qt::Horizontal,tr("thin_Wlp_thin"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Wafer_angle"),Qt::Horizontal,tr("thin_Wafer_angle"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Input_thin_data"),Qt::Horizontal,tr("thin_Input_thin_data"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Recipe"),Qt::Horizontal,tr("thin_Recipe"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("ashing1"),Qt::Horizontal,tr("thin_ashing1"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("ashing2"),Qt::Horizontal,tr("thin_ashing2"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("ashing3"),Qt::Horizontal,tr("thin_ashing3"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("ashing_machine"),Qt::Horizontal,tr("thin_ashing_machine"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("N2_Blow"),Qt::Horizontal,tr("thin_N2_Blow"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Au_f_add"),Qt::Horizontal,tr("thin_thin_Au_f_add"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Now_r_weight"),Qt::Horizontal,tr("thin_Now_r_weight"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Au_after_weight"),Qt::Horizontal,tr("thin_Au_after_weight"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Au_use"),Qt::Horizontal,tr("thin_Au_use"));
//    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Ti_add"),Qt::Horizontal,tr("thin_Ti_add"));
//    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Al_add"),Qt::Horizontal,tr("thin_Al_add"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("X_tal_number"),Qt::Horizontal,tr("thin_X_tal_number"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("X_tal_Hz"),Qt::Horizontal,tr("thin_X_tal_Hz"));
//    t4_data_model->setHeaderData(t4_data_model->fieldIndex("X_tal_life"),Qt::Horizontal,tr("thin_X_tal_life"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("User_name"),Qt::Horizontal,tr("thin_User_name"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("D_use"),Qt::Horizontal,tr("thin_D_use"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("note"),Qt::Horizontal,tr("thin_note"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Ti_total"),Qt::Horizontal,tr("thin_Ti_total"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Al_total"),Qt::Horizontal,tr("thin_Al_total"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Au_total"),Qt::Horizontal,tr("thin_Au_total"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("Input_time"),Qt::Horizontal,tr("thin_Input_time"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("machine_name"),Qt::Horizontal,tr("thin_machine_name"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("machine_code"),Qt::Horizontal,tr("thin_machine_code"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("batch_id"),Qt::Horizontal,tr("thin_batch_id"));
    t4_data_model->setHeaderData(t4_data_model->fieldIndex("cleaning_cal_flag"),Qt::Horizontal,tr("thin_cleaning_cal_flag"));
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("day_and_night"),50);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("vacuum_time"),110);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Run_number"),60);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Resister"),50);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("machine_type"),100);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Lot_id"),80);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Wafer_count"),50);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Pad_thin"),80);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Wlp_thin"),80);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Wafer_angle"),80);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Input_thin_data"),150);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Recipe"),150);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("ashing1"),65);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("ashing2"),65);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("ashing3"),65);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("ashing_machine"),80);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("N2_Blow"),80);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Au_f_add"),80);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Now_r_weight"),120);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Au_after_weight"),140);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Au_use"),50);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Ti_add"),50);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Al_add"),50);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("X_tal_number"),50);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("X_tal_Hz"),50);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("X_tal_life"),80);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("User_name"),80);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("D_use"),80);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("note"),140);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Ti_total"),60);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Al_total"),60);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Au_total"),60);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("Input_time"),120);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("machine_name"),50);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("machine_code"),50);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("batch_id"),50);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("cleaning_cal_flag"),50);
    ui->t4_data_table->horizontalHeader()->resizeSection(t4_data_model->fieldIndex("dom"),50);
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("X_tal_number"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("X_tal_Hz"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("machine_code"));
//    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("batch_id"));
//    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("cleaning_cal_flag"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("Au_use"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("au_pellet_output"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("au_add"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("check_user"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("au_pellet_total"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("work_start_au"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("au_add_weight"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("au_case_weight"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("check_NG"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("Run_number"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("Pad_thin"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("Wlp_thin"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("N2_Blow"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("D_use"));

    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("Ti_add"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("Al_add"));
    ui->t4_data_table->horizontalHeader()->hideSection(t4_data_model->fieldIndex("X_tal_life"));

    ui->t4_data_table->scrollToBottom();


    ui->t4_spec_table->setModel(spec_model);
    ui->t4_spec_table->horizontalHeader()->hideSection(0);
    ui->t4_spec_table->horizontalHeader()->hideSection(1);
    ui->t4_spec_table->horizontalHeader()->hideSection(2);
    ui->t4_spec_table->horizontalHeader()->hideSection(3);
    ui->t4_spec_table->horizontalHeader()->hideSection(4);
    ui->t4_spec_table->horizontalHeader()->hideSection(5);
    ui->t4_spec_table->horizontalHeader()->hideSection(6);
    ui->t4_spec_table->horizontalHeader()->hideSection(7);
    ui->t4_spec_table->horizontalHeader()->hideSection(8);
    ui->t4_spec_table->horizontalHeader()->hideSection(9);
    ui->t4_spec_table->horizontalHeader()->hideSection(10);
    ui->t4_spec_table->horizontalHeader()->hideSection(11);
    ui->t4_spec_table->horizontalHeader()->hideSection(12);
    ui->t4_spec_table->horizontalHeader()->hideSection(13);
    ui->t4_spec_table->horizontalHeader()->hideSection(14);
    ui->t4_spec_table->horizontalHeader()->hideSection(17);
    ui->t4_spec_table->horizontalHeader()->hideSection(18);
    ui->t4_spec_table->horizontalHeader()->hideSection(19);
    ui->t4_spec_table->horizontalHeader()->hideSection(20);
    ui->t4_spec_table->horizontalHeader()->hideSection(21);
    ui->t4_spec_table->horizontalHeader()->hideSection(22);
    ui->t4_spec_table->horizontalHeader()->hideSection(23);
    ui->t4_spec_table->horizontalHeader()->hideSection(24);
    ui->t4_spec_table->horizontalHeader()->hideSection(25);
    ui->t4_spec_table->horizontalHeader()->hideSection(26);
    ui->t4_spec_table->horizontalHeader()->hideSection(27);
    ui->t4_spec_table->horizontalHeader()->hideSection(28);
    ui->t4_spec_table->horizontalHeader()->hideSection(29);
    ui->t4_spec_table->horizontalHeader()->hideSection(30);
    ui->t4_spec_table->horizontalHeader()->hideSection(31);
    ui->t4_spec_table->horizontalHeader()->hideSection(32);
    ui->t4_spec_table->horizontalHeader()->hideSection(33);
    ui->t4_spec_table->horizontalHeader()->hideSection(34);
    ui->t4_spec_table->horizontalHeader()->hideSection(35);
    ui->t4_spec_table->horizontalHeader()->resizeSection(6,150);
    ui->t4_spec_table->horizontalHeader()->resizeSection(7,150);

    for(int i=0;i<8;i++){
        ui->t4_input_table->insertColumn(i);
    }
    ui->t4_input_table->insertRow(0);
    ui->t4_input_table->insertRow(1);
    ui->t4_input_table->insertRow(2);
    ui->t4_input_table->insertRow(3);
    ui->t4_input_table->insertRow(4);
    ui->t4_input_table->insertRow(5);
    ui->t4_input_table->insertRow(6);
    ui->t4_input_table->horizontalHeader()->resizeSection(0,70);
    ui->t4_input_table->horizontalHeader()->resizeSection(1,150);
    ui->t4_input_table->horizontalHeader()->resizeSection(3,70);
    ui->t4_input_table->horizontalHeader()->resizeSection(5,130);
    ui->t4_input_table->horizontalHeader()->resizeSection(6,140);
    ui->t4_input_table->horizontalHeader()->resizeSection(7,80);
    t4_input_item = new input_item(my_mesdb,"MD009");
    ui->t4_input_table->setCellWidget(0,0,t4_input_item->ui->LA_day_and_night);
    ui->t4_input_table->setCellWidget(1,0,t4_input_item->ui->CB_day_and_night);
    ui->t4_input_table->setCellWidget(0,1,t4_input_item->ui->LA_vacuum_time);
    ui->t4_input_table->setCellWidget(1,1,t4_input_item->ui->DT_vacuum_time);
    t4_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
    ui->t4_input_table->setCellWidget(0,2,t4_input_item->ui->LA_resister);
    ui->t4_input_table->setCellWidget(1,2,t4_input_item->ui->LE_resister);
//    ui->t4_input_table->setCellWidget(0,3,t4_input_item->ui->LA_N2_blow);
//    ui->t4_input_table->setCellWidget(1,3,t4_input_item->ui->WCB_N2_blow);
    ui->t4_input_table->setCellWidget(0,4,t4_input_item->ui->LA_au_f_add);
    ui->t4_input_table->setCellWidget(1,4,t4_input_item->ui->LE_au_f_add);
    ui->t4_input_table->setCellWidget(0,5,t4_input_item->ui->LA_now_r_weight);
    ui->t4_input_table->setCellWidget(1,5,t4_input_item->ui->LE_now_r_weight);
    ui->t4_input_table->setCellWidget(0,6,t4_input_item->ui->LA_au_after_r_weight);
    ui->t4_input_table->setCellWidget(1,6,t4_input_item->ui->LE_au_after_r_weight);
    ui->t4_input_table->setCellWidget(0,7,t4_input_item->ui->LA_au_after_weight);
    ui->t4_input_table->setCellWidget(1,7,t4_input_item->ui->LE_au_after_weight);
//    ui->t4_input_table->setCellWidget(0,7,t4_input_item->ui->LA_au_use);
//    ui->t4_input_table->setCellWidget(1,7,t4_input_item->ui->LE_au_use);
//    ui->t4_input_table->setCellWidget(2,0,t4_input_item->ui->LA_ti_add);
//    ui->t4_input_table->setCellWidget(3,0,t4_input_item->ui->LE_ti_add);
//    ui->t4_input_table->setCellWidget(2,1,t4_input_item->ui->LA_al_add);
//    ui->t4_input_table->setCellWidget(3,1,t4_input_item->ui->LE_al_add);
//    ui->t4_input_table->setCellWidget(2,2,t4_input_item->ui->LA_xtal_life);
//    ui->t4_input_table->setCellWidget(3,2,t4_input_item->ui->LE_xtal_life);
    ui->t4_input_table->setCellWidget(2,3,t4_input_item->ui->LA_name);
    ui->t4_input_table->setCellWidget(3,3,t4_input_item->ui->LE_name);
//    ui->t4_input_table->setCellWidget(2,4,t4_input_item->ui->LA_d_run);
//    ui->t4_input_table->setCellWidget(3,4,t4_input_item->ui->WCB_d_run);
    ui->t4_input_table->setCellWidget(2,5,t4_input_item->ui->LA_note);
    ui->t4_input_table->setCellWidget(3,5,t4_input_item->ui->LE_note);
    ui->t4_input_table->setSpan(2,5,1,5);
    ui->t4_input_table->setSpan(3,5,1,5);
    ui->t4_input_table->setCellWidget(4,0,t4_input_item->ui->LE_recipe_number);
    ui->t4_input_table->setCellWidget(4,1,t4_input_item->ui->CB_recipe_choice);
//    ui->t4_input_table->setCellWidget(4,3,t4_input_item->ui->LA_recipe_2);
    ui->t4_input_table->setCellWidget(5,0,t4_input_item->ui->W_recipe);
    ui->t4_input_table->setSpan(4,1,1,5);
//    ui->t4_input_table->setSpan(4,3,1,2);
    ui->t4_input_table->setSpan(5,0,2,5);
    ui->t4_input_table->setCellWidget(4,6,t4_input_item->ui->LA_dom_number);
    ui->t4_input_table->setCellWidget(5,6,t4_input_item->ui->CB_domnumber);

    ui->t4_LE_Lot_count->setValidator(new QIntValidator(0, 1000, this));
//    ui->t4_LE_Run_number->setValidator(new QIntValidator(0, 1000, this));
    QSqlQuery query(my_mesdb);
    query.exec(QString("select `Lot_max_count`,`Run_number_count` from Thin_film_spec_managerment where machine_code = 'MD009'"));
    if(query.next()){
        ui->t4_LE_Lot_count->setText(query.value("Lot_max_count").toString());
//        ui->t4_LE_Run_number->setText(query.value("Run_number_count").toString());
    }
    t4_lot_item_list.clear();
    for(int i=0;i<ui->t4_LE_Lot_count->text().toInt();i++){

        input_lot_item *lot_item = new input_lot_item(ms_mesdb,t4_input_item,"MD009");
        t4_lot_item_list.append(lot_item);
        ui->t4_input_lot_table->insertRow(i);
        ui->t4_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
        ui->t4_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
        ui->t4_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
        ui->t4_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
        ui->t4_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
        ui->t4_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
        ui->t4_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
        ui->t4_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//        connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                t4_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
    }
    query.exec("SELECT * FROM `Thin_film_repcipe` where machine_code = 'MD009' ORDER BY `recipe_number` ASC  ");
    t4_input_item->ui->CB_recipe_choice->addItem(tr("recipe_choice"));
    while(query.next()){
        QString recipe_name = query.value("recipe_number").toString()+"."+ query.value("recipename").toString()
                                + "|"+query.value("metal_name").toString()+"|"+query.value("metal_thin").toString()+"|"
                                +query.value("type").toString();
        QPixmap pixmap(100,100);
        pixmap.fill(QColor(query.value("color").toString()));
        t4_input_item->ui->CB_recipe_choice->addItem(QIcon(pixmap),recipe_name);
    }
    connect(t4_input_item->ui->CB_recipe_choice,SIGNAL(currentIndexChanged(int)),this,SLOT(t4_recipe_choice(int)));

    t4_ti_chart = new Thin_film_chart();
    t4_ti_chartview = new Thin_film_chart_view(t4_ti_chart);
    connect(t4_ti_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t4_ti_y_value_slot(QPointF)));
    ui->t4_Ti_layout->addWidget(t4_ti_chartview);

    t4_al_chart = new Thin_film_chart();
    t4_al_chartview = new Thin_film_chart_view(t4_al_chart);
    connect(t4_al_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t4_al_y_value_slot(QPointF)));
    ui->t4_Al_layout->addWidget(t4_al_chartview);
    t4_ti_axisX = new QDateTimeAxis();
    t4_ti_axisX->setFormat("MM-dd HH:mm:ss");
    t4_ti_axisX->setTitleText("Date");
    t4_ti_axisX->setVisible(true);
    t4_ti_axisX->setTitleVisible(true);
    t4_ti_chart->addAxis(t4_ti_axisX, Qt::AlignBottom);
    t4_ti_axisY = new QValueAxis();
    t4_ti_chart->addAxis(t4_ti_axisY,Qt::AlignLeft);

    t4_al_axisX = new QDateTimeAxis();
    t4_al_axisX->setFormat("MM-dd HH:mm:ss");
    t4_al_axisX->setTitleText("Date");
    t4_al_axisX->setVisible(true);
    t4_al_axisX->setTitleVisible(true);
    t4_al_chart->addAxis(t4_al_axisX, Qt::AlignBottom);
    t4_al_axisY = new QValueAxis();
    t4_al_chart->addAxis(t4_al_axisY,Qt::AlignLeft);

    ti_al_chart_init(t4_ti_chart,t4_al_chart,t4_ti_value_series,t4_al_value_series,t4_ti_UCL_value_series,t4_al_UCL_value_series,
                     t4_al_axisX,t4_al_axisY,t4_ti_axisX,t4_ti_axisY,"MD009");
    query.exec("SELECT machine_name,machine_code FROM Thin_film_spec_managerment");
    while(query.next()){
        ui->t4_CB_move_machine->addItem(query.value("machine_name").toString());
    }
    temp_run_widget_map.insert("MD009",ui->t4_temp_run_mode_time);
    temp_run_ti_accmulate_widget_map.insert("MD009",ui->t4_ti_accmulate);
    temp_run_al_accmulate_widget_map.insert("MD009",ui->t4_al_accmulate);
    ui->t4_CB_wet_insert->addItems(wet_machine_list);
    sync_now_date("MD009",t4_input_item->ui->LE_au_f_add,t4_input_item->ui->LE_now_r_weight,t4_input_item->ui->LE_au_after_r_weight
                  ,t4_input_item->ui->LE_au_after_weight);
    ui->t4_data_table->selectionModel()->setObjectName("t4_data");
    connect(ui->t4_data_table->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this,SLOT(table_view_selection_change(QModelIndex,QModelIndex)));


}
void Thin_film_mainwindows::t4_recipe_choice(int idx)
{
    if(idx == 0){
        t4_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t4_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t4_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t4_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t4_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t4_input_item->ui->LE_Recipe_name1->setText("");
        t4_input_item->ui->LE_Recipe_name2->setText("");
        t4_input_item->ui->LE_Recipe_name3->setText("");
        t4_input_item->ui->LE_Recipe_name4->setText("");
        t4_input_item->ui->LE_Recipe_name5->setText("");
        return ;
    }
    QSqlQuery query(my_mesdb);
    query.exec(QString("select * from `Thin_film_repcipe` where recipe_number  = %1 AND machine_code = 'MD009'").arg(idx));

    if(query.next()){
        t4_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t4_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t4_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t4_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t4_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t4_input_item->ui->LE_Recipe_name1->setText("");
        t4_input_item->ui->LE_Recipe_name2->setText("");
        t4_input_item->ui->LE_Recipe_name3->setText("");
        t4_input_item->ui->LE_Recipe_name4->setText("");
        t4_input_item->ui->LE_Recipe_name5->setText("");
        if(query.value("metal_name").toString() != ""){
            QString item_data = query.value("metal_name").toString();
            QStringList item_list = item_data.split("/");
            for(int i=0;i<item_list.count();i++){
                if(i==0){
                    t4_input_item->ui->CB_Recipe_name1->setCurrentText(item_list.at(i));
                }else if(i==1){
                    t4_input_item->ui->CB_Recipe_name2->setCurrentText(item_list.at(i));
                }else if(i==2){
                    t4_input_item->ui->CB_Recipe_name3->setCurrentText(item_list.at(i));
                }else if(i==3){
                    t4_input_item->ui->CB_Recipe_name4->setCurrentText(item_list.at(i));
                }else if(i==4){
                    t4_input_item->ui->CB_Recipe_name5->setCurrentText(item_list.at(i));
                }
            }
        }
        if(query.value("metal_thin").toString() != ""){
            QStringList recipe_item = query.value("metal_thin").toString().split("/");
            if(recipe_item.count()>0){
                t4_input_item->ui->LE_Recipe_name1->setText(QString("%1").arg(recipe_item[0]));
            }
            if(recipe_item.count()>1){
                t4_input_item->ui->LE_Recipe_name2->setText(QString("%1").arg(recipe_item[1]));
            }
            if(recipe_item.count()>2){
                t4_input_item->ui->LE_Recipe_name3->setText(QString("%1").arg(recipe_item[2]));
            }
            if(recipe_item.count()>3){
                t4_input_item->ui->LE_Recipe_name4->setText(QString("%1").arg(recipe_item[3]));
            }
            if(recipe_item.count()>4){
                t4_input_item->ui->LE_Recipe_name5->setText(QString("%1").arg(recipe_item[4]));
            }
        }
    }
}
void Thin_film_mainwindows::t4_ti_y_value_slot(QPointF value)
{
    ui->t4_ti_y_value->setText(QString("%1").arg(value.y()));
}
void Thin_film_mainwindows::t4_al_y_value_slot(QPointF value)
{
    ui->t4_al_y_value->setText(QString("%1").arg(value.y()));
}
void Thin_film_mainwindows::t4_datamodel_dataChanged(QModelIndex index1, QModelIndex index2, QVector<int> vector)
{
     v1_data_model_change(index1,t4_data_model,"MD009");
     ui->t4_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t4_LE_Lot_count_editingFinished()
{
//    if(ui->t4_LE_Lot_count->text()!= ""){
//        QSqlQuery query(my_mesdb);
//        query.exec(QString("update `Thin_film_spec_managerment` SET `Lot_max_count`='%1' Where `machine_code`='MD009'").arg(ui->t4_LE_Lot_count->text()));
//        while(ui->t4_input_lot_table->rowCount()>0){
//            ui->t4_input_lot_table->removeRow(0);
//        }
//        t4_lot_item_list.clear();
//        for(int i=0;i<ui->t4_LE_Lot_count->text().toInt();i++){

//            input_lot_item *lot_item = new input_lot_item(ms_mesdb,"MD009");
//            t4_lot_item_list.append(lot_item);
//            ui->t4_input_lot_table->insertRow(i);
//            ui->t4_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//    //        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
//            ui->t4_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
//            ui->t4_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
//            ui->t4_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
//            ui->t4_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
//            ui->t4_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
//            ui->t4_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
//            ui->t4_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//            connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                    t4_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
//        }
//    }
}
void Thin_film_mainwindows::on_t4_LE_Run_number_editingFinished()
{
//    if(ui->t4_LE_Run_number->text() != ""){
//        QSqlQuery query(my_mesdb);
//        query.exec(QString("update `Thin_film_spec_managerment` SET `Run_number_count`='%1' Where `machine_code`='MD009'").arg(ui->t4_LE_Run_number->text()));
//    }
}
void Thin_film_mainwindows::on_t4_Input_btn_clicked()
{

//    QString Run_number = ui->t4_LE_Run_number->text();
//    if(Run_number.toInt() > 60){
//        ui->t4_LE_Run_number->setText("1");
//        on_t4_LE_Run_number_editingFinished();
//        QMessageBox msg;
//        msg.addButton(QMessageBox::Ok);
//        msg.setText(tr("60 over set 1"));
//        msg.exec();
//    }
    QString Lot_count = ui->t4_LE_Lot_count->text();
    QString resister = t4_input_item->ui->LE_resister->text();
//    QString N2_blow = QString("%1").arg(QVariant(t4_input_item->ui->CB_N2_blow->isChecked()).toInt());
    QString au_f_add = t4_input_item->ui->LE_au_f_add->text();
    QString now_r_weight = t4_input_item->ui->LE_now_r_weight->text();
    QString au_after_r_weight = t4_input_item->ui->LE_au_after_r_weight->text();
    QString au_use =  t4_input_item->ui->LE_au_use->text();
    QString ti_add =  t4_input_item->ui->LE_ti_add->text();
    QString al_add =  t4_input_item->ui->LE_al_add->text();
    QString xtal_life =  t4_input_item->ui->LE_xtal_life->text();
    QString name =  t4_input_item->ui->LE_name->text();
//    QString d_use =  QString("%1").arg(QVariant(t4_input_item->ui->CB_d_run->isChecked()).toInt());

    QString note =  t4_input_item->ui->LE_note->text();

    ui->t4_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t4_search_time_start->setDateTime(ui->t4_search_time_end->dateTime().addDays(-2));

    QString Recipe_total_text;
    QString Recipe_total_name_text;
    QString Recipe_name1 = t4_input_item->ui->LE_Recipe_name1->text();
    QString Recipe_name2 = t4_input_item->ui->LE_Recipe_name2->text();
    QString Recipe_name3 = t4_input_item->ui->LE_Recipe_name3->text();
    QString Recipe_name4 = t4_input_item->ui->LE_Recipe_name4->text();
    QString Recipe_name5 = t4_input_item->ui->LE_Recipe_name5->text();
    QString CB_Recipe_name1 = t4_input_item->ui->CB_Recipe_name1->currentText();
    QString CB_Recipe_name2 = t4_input_item->ui->CB_Recipe_name2->currentText();
    QString CB_Recipe_name3 = t4_input_item->ui->CB_Recipe_name3->currentText();
    QString CB_Recipe_name4 = t4_input_item->ui->CB_Recipe_name4->currentText();
    QString CB_Recipe_name5 = t4_input_item->ui->CB_Recipe_name5->currentText();
    int Recipe_count = 0;
    int ti_count = 0;
    int au_count = 0;
    int al_count = 0;

    if(CB_Recipe_name1 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name1));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name1));
        if(CB_Recipe_name1=="Ti"){
            ti_count += Recipe_name1.toInt();
        }else if(CB_Recipe_name1=="Al"){
            al_count += Recipe_name1.toInt();
        }else if(CB_Recipe_name1=="Au"){
            au_count += Recipe_name1.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name2 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name2));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name2));
        if(CB_Recipe_name2=="Ti"){
            ti_count += Recipe_name2.toInt();
        }else if(CB_Recipe_name2=="Al"){
            al_count += Recipe_name2.toInt();
        }else if(CB_Recipe_name2=="Au"){
            au_count += Recipe_name2.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name3 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name3));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name3));
        if(CB_Recipe_name3=="Ti"){
            ti_count += Recipe_name3.toInt();
        }else if(CB_Recipe_name3=="Al"){
            al_count += Recipe_name3.toInt();
        }else if(CB_Recipe_name3=="Au"){
            au_count += Recipe_name3.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name4 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name4));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name4));
        if(CB_Recipe_name4=="Ti"){
            ti_count += Recipe_name4.toInt();
        }else if(CB_Recipe_name4=="Al"){
            al_count += Recipe_name4.toInt();
        }else if(CB_Recipe_name4=="Au"){
            au_count += Recipe_name4.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name5 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name5));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name5));
        if(CB_Recipe_name5=="Ti"){
            ti_count += Recipe_name5.toInt();
        }else if(CB_Recipe_name5=="Al"){
            al_count += Recipe_name5.toInt();
        }else if(CB_Recipe_name5=="Au"){
            au_count += Recipe_name5.toInt();
        }
        Recipe_count++;
    }
    if(Recipe_count>0){
        Recipe_total_text.remove(Recipe_total_text.length()-1,1);
        Recipe_total_name_text.remove(Recipe_total_name_text.length()-1,1);
    }
    QSqlQuery query(my_mesdb);
    QString batch_id;
    query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_code = 'MD009'"));
    if(query.next()){
        batch_id = query.value("current_batch_id").toString();
        query.exec("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+1  where machine_code = 'MD009'");
    }
    bool query_result = false;
    qDebug()<<ui->t4_input_lot_table->rowCount();
    query.exec("select now() as now_time");
    QDateTime now_time;

    now_time = QDateTime::currentDateTime();
    for(int i=0;i<ui->t4_input_lot_table->rowCount();i++){
        QString Lot_id = t4_lot_item_list.at(i)->ui->LE_Lot_id->text();
        QString wafer_count = t4_lot_item_list.at(i)->ui->LE_wafer_count->text();
        QString ashing1 = t4_lot_item_list.at(i)->ui->LE_ashing1->text();
        QString ashing2 = t4_lot_item_list.at(i)->ui->LE_ashing2->text();
        QString ashing3 = t4_lot_item_list.at(i)->ui->LE_ashing3->text();
        QString ashing_machine = t4_lot_item_list.at(i)->ui->LE_ashing_machine->text();
        QString machine_type = t4_lot_item_list.at(i)->ui->LE_machine_type->text();
        QString wafer_angle = t4_lot_item_list.at(i)->ui->LE_wafer_angle->text();
        if(Lot_id.trimmed() !=""){
            QString query_str = QString("INSERT INTO `Thin_film_data` "
                                        "(`Run_number`, `Resister`, `machine_type`, `Lot_id`, `Wafer_count`, "
                                        "`Wafer_angle`, `Input_thin_data`, `ashing1`, `ashing2`, "
                                        "`ashing3`, `ashing_machine`, `Pad_thin`, `Wlp_thin`, "
                                        "`N2_Blow`, `Au_f_add`, `Now_r_weight`, `Au_after_weight`, "
                                        "`Au_use`, `Recipe`, `Ti_add`, `Al_add`, `X_tal_number`, "
                                        "`X_tal_life`, `User_name`, `D_use`, `note`, `Ti_total`, "
                                        "`Al_total`, `Au_total`, `batch_id`, `cleaning_cal_flag`,`machine_code`,`machine_name`,"
                                        "`Input_time`,`vacuum_time`,`day_and_night`,`au_pellet_output`,`au_add_weight`,`dom`) "
                                        "VALUES "
                                        "('%1', '%2', '%3', '%4', '%5', '%6', "
                                        "'%7', '%8', '%9', '%10', "
                                        "'%11', '%12', '%13', '%14', '%15', '%16', "
                                        "'%17', '%18', '%19', '%20', '%21', '%22', "
                                        "'%23', '%24', '%25', '%26', '%27', "
                                        "'%28', '%29', '%30' ,'%31','%32','%33','%34','%35','%36','%37','%38','%39' );").arg("").arg(resister).arg(machine_type).arg(Lot_id).arg(wafer_count)
                                                                      .arg(wafer_angle).arg(Recipe_total_text).arg(ashing1).arg(ashing2)
                                                                      .arg(ashing3).arg(ashing_machine).arg(QString("0")).arg(QString("0"))
                                                                      .arg("").arg(au_f_add).arg(now_r_weight).arg(au_after_r_weight)
                                                                      .arg(au_use).arg(Recipe_total_name_text).arg(ti_add).arg(al_add).arg(QString("0"))
                                                                      .arg(xtal_life).arg(name).arg("").arg(note).arg(ti_count)
                                                                      .arg(al_count).arg(au_count).arg(batch_id).arg(QString("0")).arg("MD009").arg("CHA#4")
                                                                      .arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"))
                                                                      .arg(t4_input_item->ui->DT_vacuum_time->dateTime().toString("yyyy-MM-dd hh:mm:00"))
                                                                      .arg(t4_input_item->ui->CB_day_and_night->currentText())
                                                                      .arg(t4_input_item->ui->DT_vacuum_time->dateTime().addSecs(-300).toString("yyyy-MM-dd hh:mm:00"))
                                                                      .arg(t4_input_item->ui->LE_au_after_weight->text())
                                                                      .arg(t4_input_item->ui->CB_domnumber->currentText());
            query_result = query.exec(query_str);

            if(!query_result){
                break;
            }

        }
    }
    au_calc(now_time);
    if(query_result){
        t4_input_item->clear_data();
        for(int i=0;i<ui->t4_input_lot_table->rowCount();i++){
            t4_lot_item_list.at(i)->clear_data();
        }
        QMessageBox msg;
        msg.addButton(QMessageBox::Ok);
        msg.setText(tr("input complete"));
        msg.exec();
//        int run_count = ui->t4_LE_Run_number->text().toInt();
//        run_count++;
//        ui->t4_LE_Run_number->setText(QString("%1").arg(run_count));
        on_t4_LE_Run_number_editingFinished();
        ui->t4_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
        ui->t4_search_time_start->setDateTime(ui->t4_search_time_end->dateTime().addDays(-2));
        t4_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
        on_t4_search_btn_clicked();
        ui->t4_data_table->scrollToBottom();
    }else {
        QMessageBox::warning(this, tr("conntion false"),
                                                       "server connection fail\n"
                                                          ""+my_mesdb.lastError().text(),
                                                            QMessageBox::Close);
    }
    query.exec(QString("select * from depostion_au_use_current_view where machine_name = '%1' AND batch_id = %2")
               .arg("CHA#4").arg(batch_id));
    if(query.next()){
        double  OKNG = query.value("OKNG").toDouble();
        if(abs(OKNG)>0.1){
            QMessageBox msg;
            msg.addButton(QMessageBox::Ok);
            msg.setText(tr("au pellet NG"));
            msg.exec();
        }
    }
    query.exec("update `Thin_film_spec_managerment` set au_pullet_add = 0 ,now_r_weight = 0,au_after_r_weight =0,au_after_weight=0 where machine_code = 'MD009'");
    ti_al_chart_init(t4_ti_chart,t4_al_chart,t4_ti_value_series,t4_al_value_series,t4_ti_UCL_value_series,t4_al_UCL_value_series,
                     t4_al_axisX,t4_al_axisY,t4_ti_axisX,t4_ti_axisY,"MD009");


}
void Thin_film_mainwindows::on_t4_search_btn_clicked()
{
    t4_data_model->setFilter(QString("vacuum_time Between '%1' AND '%2' AND machine_code = 'MD009'")
                          .arg(ui->t4_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t4_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    t4_data_model->select();
    t4_data_model->setSort(2,Qt::AscendingOrder);
    QSqlQuery query(my_mesdb);
    query.exec(QString("select sum((case when (`A`.`machine_code` = 'MD009') then `A`.`Wafer_count` end)) AS `wafer` from `Thin_film_data` `A` "
               "where vacuum_time Between '%1' AND '%2' AND machine_code = 'MD009'").arg(ui->t4_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
               .arg(ui->t4_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    if(query.next()){

        ui->t4_total_wafer->setText(query.value("wafer").toString());
    }
    ui->t4_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t4_BT_search_lot_id_clicked()
{
    t4_data_model->setFilter(QString("Lot_id = '%1' AND machine_code = 'MD009'")
                          .arg(ui->t4_LE_search_lot_id->text()));
    t4_data_model->select();
    t4_data_model->setSort(1,Qt::AscendingOrder);
}
void Thin_film_mainwindows::on_t4_ti_al_refresh_clicked()
{
    ti_al_chart_init(t4_ti_chart,t4_al_chart,t4_ti_value_series,t4_al_value_series,t4_ti_UCL_value_series,t4_al_UCL_value_series,
                     t4_al_axisX,t4_al_axisY,t4_ti_axisX,t4_ti_axisY,"MD009");
}
void Thin_film_mainwindows::on_t4_DT_del_btn_clicked()
{
    int count = ui->t4_data_table->selectionModel()->selectedIndexes().count();
    QVector<int> index_history;
    for(int i=0;i<count;i++){
        if(!index_history.contains(ui->t4_data_table->selectionModel()->selectedIndexes().at(i).row())){
        QDateTime Input_time2 = t4_data_model->record(ui->t4_data_table->selectionModel()->selectedIndexes().at(i).row()).value("Input_time").toDateTime();
        t4_data_model->removeRow(ui->t4_data_table->selectionModel()->selectedIndexes().at(i).row());
         au_calc(Input_time2);
         index_history.append(ui->t4_data_table->selectionModel()->selectedIndexes().at(i).row());
        }else {

        }
    }
    on_t4_search_btn_clicked();
    ui->t4_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t4_move_machine_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    QString batch_id_temp = "0";
    QString machine_name;
    QString machine_code;

    int count  = ui->t4_data_table->selectionModel()->selectedIndexes().count();
    for(int i=0;i<count;i++){
         QSqlRecord recode = t4_data_model->record(ui->t4_data_table->selectionModel()->selectedIndexes().at(i).row());
        if(batch_id_temp != recode.value("batch_id").toString()){
            query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_name = '%1'").arg(ui->t4_CB_move_machine->currentText()));
            if(query.next()){
                batch_id_temp = query.value("current_batch_id").toString()+5;
                machine_name = query.value("machine_name").toString();
                machine_code = query.value("machine_code").toString();
                query.exec(QString("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+10  where machine_name = '%1'").arg(ui->t4_CB_move_machine->currentText()));
            }
            batch_id_temp = recode.value("batch_id").toString();
        }

        recode.setGenerated("batch_id",true);
        recode.setGenerated("machine_name",true);
        recode.setGenerated("machine_code",true);
        recode.setValue("batch_id",batch_id_temp);
        recode.setValue("machine_name",machine_name);
        recode.setValue("machine_code",machine_code);
        t4_data_model->setRecord(ui->t4_data_table->selectionModel()->selectedIndexes().at(i).row(),recode);
    }
    on_t4_search_btn_clicked();
    ui->t4_data_table->scrollToBottom();

}
void Thin_film_mainwindows::on_t4_excel_header_copy_flag_toggled(bool checked)
{
     ui->t4_data_table->copyheader_flag = checked;
}
void Thin_film_mainwindows::on_t4_machine_temp_run_mode_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    query.exec("update Thin_film_spec_managerment set nomal_mode_time = ADDTIME(NOW(),'0:5:0') where `machine_code`='MD009'");
}

//t4_function -- end --

//t5_function -- start --
void Thin_film_mainwindows::t5_init()
{
    ui->t5_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t5_search_time_start->setDateTime(ui->t5_search_time_end->dateTime().addDays(-2));

    QSqlTableModel *spec_model = new QSqlTableModel(this,my_mesdb);
    spec_model->setTable("Thin_film_spec_managerment");
    spec_model->setFilter("machine_code = 'MD001'");
    spec_model->select();
    spec_model->setHeaderData(4,Qt::Horizontal,tr("ashing_LCL"));
    spec_model->setHeaderData(5,Qt::Horizontal,tr("ashing_UCL"));
    spec_model->setHeaderData(6,Qt::Horizontal,tr("WLPTiAlashing_LCL"));
    spec_model->setHeaderData(7,Qt::Horizontal,tr("WLPTiAlashing_UCL"));
    spec_model->setHeaderData(8,Qt::Horizontal,tr("WLP_LCL_Ti_Au"));
    spec_model->setHeaderData(9,Qt::Horizontal,tr("WLP_UCL_Ti_Au"));
    spec_model->setHeaderData(10,Qt::Horizontal,tr("WLP_LCL_Ti_Al"));
    spec_model->setHeaderData(11,Qt::Horizontal,tr("WLP_UCL_Ti_Al"));
    spec_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    t5_data_model = new thin_sqltable_model(this,my_mesdb,"MD001",spec_model);

    t5_data_model->setTable("Thin_film_data");

    t5_data_model->setFilter(QString("Input_time Between '%1' AND '%2' AND machine_code = 'MD001'")
                          .arg(ui->t5_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t5_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    ui->t5_data_table->setItemDelegateForColumn(t5_data_model->fieldIndex("vacuum_time"),new DateDelegate(ui->t5_data_table));
    ui->t5_data_table->setItemDelegateForColumn(t5_data_model->fieldIndex("Input_time"),new DateDelegate(ui->t5_data_table));
    ui->t5_data_table->setModel(t5_data_model);
    ui->t5_data_table->setSortingEnabled(true);
    connect(t5_data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>))
            ,this,SLOT(t5_datamodel_dataChanged(QModelIndex,QModelIndex,QVector<int>)));
    ui->t5_data_table->setItemDelegateForColumn(10,new Thin_combodelegate(note_combo_list,ui->t5_data_table));

    t5_data_model->setEditStrategy(QSqlTableModel::OnFieldChange);
    t5_data_model->setSort(2,Qt::AscendingOrder);

    t5_data_model->select();

    t5_data_model->setHeaderData(t5_data_model->fieldIndex("day_and_night"),Qt::Horizontal,tr("day_and_night"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("vacuum_time"),Qt::Horizontal,tr("vacuum_time"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Run_number"),Qt::Horizontal,tr("thin_Run_number"));
//    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Resister"),Qt::Horizontal,tr("thin_Resister"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("machine_type"),Qt::Horizontal,tr("thin_machine_type"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Lot_id"),Qt::Horizontal,tr("thin_Lot_id"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Wafer_count"),Qt::Horizontal,tr("thin_Wafer_count"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Pad_thin"),Qt::Horizontal,tr("thin_Pad_thin"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Wlp_thin"),Qt::Horizontal,tr("thin_Wlp_thin"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Wafer_angle"),Qt::Horizontal,tr("thin_Wafer_angle"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Input_thin_data"),Qt::Horizontal,tr("thin_Input_thin_data"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Recipe"),Qt::Horizontal,tr("thin_Recipe"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("ashing1"),Qt::Horizontal,tr("thin_ashing1"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("ashing2"),Qt::Horizontal,tr("thin_ashing2"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("ashing3"),Qt::Horizontal,tr("thin_ashing3"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("ashing_machine"),Qt::Horizontal,tr("thin_ashing_machine"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("N2_Blow"),Qt::Horizontal,tr("thin_N2_Blow"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Au_f_add"),Qt::Horizontal,tr("thin_thin_Au_f_add"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Now_r_weight"),Qt::Horizontal,tr("thin_Now_r_weight"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Au_after_weight"),Qt::Horizontal,tr("thin_Au_after_weight"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Au_use"),Qt::Horizontal,tr("thin_Au_use"));
//    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Ti_add"),Qt::Horizontal,tr("thin_Ti_add"));
//    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Al_add"),Qt::Horizontal,tr("thin_Al_add"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("X_tal_number"),Qt::Horizontal,tr("thin_X_tal_number"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("X_tal_Hz"),Qt::Horizontal,tr("thin_X_tal_Hz"));
//    t5_data_model->setHeaderData(t5_data_model->fieldIndex("X_tal_life"),Qt::Horizontal,tr("thin_X_tal_life"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("User_name"),Qt::Horizontal,tr("thin_User_name"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("D_use"),Qt::Horizontal,tr("thin_D_use"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("note"),Qt::Horizontal,tr("thin_note"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Ti_total"),Qt::Horizontal,tr("thin_Ti_total"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Al_total"),Qt::Horizontal,tr("thin_Al_total"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Au_total"),Qt::Horizontal,tr("thin_Au_total"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("Input_time"),Qt::Horizontal,tr("thin_Input_time"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("machine_name"),Qt::Horizontal,tr("thin_machine_name"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("machine_code"),Qt::Horizontal,tr("thin_machine_code"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("batch_id"),Qt::Horizontal,tr("thin_batch_id"));
    t5_data_model->setHeaderData(t5_data_model->fieldIndex("cleaning_cal_flag"),Qt::Horizontal,tr("thin_cleaning_cal_flag"));
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("day_and_night"),50);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("vacuum_time"),110);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Run_number"),60);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Resister"),50);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("machine_type"),100);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Lot_id"),80);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Wafer_count"),50);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Pad_thin"),80);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Wlp_thin"),80);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Wafer_angle"),80);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Input_thin_data"),150);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Recipe"),150);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("ashing1"),65);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("ashing2"),65);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("ashing3"),65);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("ashing_machine"),80);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("N2_Blow"),80);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Au_f_add"),80);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Now_r_weight"),120);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Au_after_weight"),140);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Au_use"),50);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Ti_add"),50);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Al_add"),50);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("X_tal_number"),50);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("X_tal_Hz"),50);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("X_tal_life"),80);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("User_name"),80);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("D_use"),80);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("note"),140);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Ti_total"),60);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Al_total"),60);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Au_total"),60);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("Input_time"),120);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("machine_name"),50);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("machine_code"),50);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("batch_id"),50);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("cleaning_cal_flag"),50);
    ui->t5_data_table->horizontalHeader()->resizeSection(t5_data_model->fieldIndex("dom"),50);
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("X_tal_life"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("machine_code"));
//    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("batch_id"));
//    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("cleaning_cal_flag"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("Au_use"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("au_pellet_output"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("au_add"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("check_user"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("au_pellet_total"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("work_start_au"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("au_add_weight"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("au_case_weight"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("check_NG"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("Run_number"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("Pad_thin"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("Wlp_thin"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("N2_Blow"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("D_use"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("Ti_add"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("Al_add"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("X_tal_life"));
    ui->t5_data_table->horizontalHeader()->hideSection(t5_data_model->fieldIndex("Resister"));

    ui->t5_data_table->scrollToBottom();


    ui->t5_spec_table->setModel(spec_model);
    ui->t5_spec_table->horizontalHeader()->hideSection(0);
    ui->t5_spec_table->horizontalHeader()->hideSection(1);
    ui->t5_spec_table->horizontalHeader()->hideSection(2);
    ui->t5_spec_table->horizontalHeader()->hideSection(3);
    ui->t5_spec_table->horizontalHeader()->hideSection(4);
    ui->t5_spec_table->horizontalHeader()->hideSection(5);
    ui->t5_spec_table->horizontalHeader()->hideSection(6);
    ui->t5_spec_table->horizontalHeader()->hideSection(7);
    ui->t5_spec_table->horizontalHeader()->hideSection(8);
    ui->t5_spec_table->horizontalHeader()->hideSection(9);
    ui->t5_spec_table->horizontalHeader()->hideSection(10);
    ui->t5_spec_table->horizontalHeader()->hideSection(11);
    ui->t5_spec_table->horizontalHeader()->hideSection(12);
    ui->t5_spec_table->horizontalHeader()->hideSection(13);
    ui->t5_spec_table->horizontalHeader()->hideSection(14);
    ui->t5_spec_table->horizontalHeader()->hideSection(17);
    ui->t5_spec_table->horizontalHeader()->hideSection(18);
    ui->t5_spec_table->horizontalHeader()->hideSection(19);
    ui->t5_spec_table->horizontalHeader()->hideSection(20);
    ui->t5_spec_table->horizontalHeader()->hideSection(21);
    ui->t5_spec_table->horizontalHeader()->hideSection(22);
    ui->t5_spec_table->horizontalHeader()->hideSection(23);
    ui->t5_spec_table->horizontalHeader()->hideSection(24);
    ui->t5_spec_table->horizontalHeader()->hideSection(25);
    ui->t5_spec_table->horizontalHeader()->hideSection(26);
    ui->t5_spec_table->horizontalHeader()->hideSection(27);
    ui->t5_spec_table->horizontalHeader()->hideSection(28);
    ui->t5_spec_table->horizontalHeader()->hideSection(29);
    ui->t5_spec_table->horizontalHeader()->hideSection(30);
    ui->t5_spec_table->horizontalHeader()->hideSection(31);
    ui->t5_spec_table->horizontalHeader()->hideSection(32);
    ui->t5_spec_table->horizontalHeader()->hideSection(33);
    ui->t5_spec_table->horizontalHeader()->hideSection(34);
    ui->t5_spec_table->horizontalHeader()->hideSection(35);
    ui->t5_spec_table->horizontalHeader()->resizeSection(6,150);
    ui->t5_spec_table->horizontalHeader()->resizeSection(7,150);

    for(int i=0;i<8;i++){
        ui->t5_input_table->insertColumn(i);
    }
    ui->t5_input_table->insertRow(0);
    ui->t5_input_table->insertRow(1);
    ui->t5_input_table->insertRow(2);
    ui->t5_input_table->insertRow(3);
    ui->t5_input_table->insertRow(4);
    ui->t5_input_table->insertRow(5);
    ui->t5_input_table->insertRow(6);
    ui->t5_input_table->horizontalHeader()->resizeSection(0,70);
    ui->t5_input_table->horizontalHeader()->resizeSection(1,150);
    ui->t5_input_table->horizontalHeader()->resizeSection(3,70);
    ui->t5_input_table->horizontalHeader()->resizeSection(5,130);
    ui->t5_input_table->horizontalHeader()->resizeSection(6,140);
    ui->t5_input_table->horizontalHeader()->resizeSection(7,80);
    t5_input_item = new input_item(my_mesdb,"MD001");
    ui->t5_input_table->setCellWidget(0,0,t5_input_item->ui->LA_day_and_night);
    ui->t5_input_table->setCellWidget(1,0,t5_input_item->ui->CB_day_and_night);
    ui->t5_input_table->setCellWidget(0,1,t5_input_item->ui->LA_vacuum_time);
    ui->t5_input_table->setCellWidget(1,1,t5_input_item->ui->DT_vacuum_time);
    t5_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
//    ui->t5_input_table->setCellWidget(0,2,t5_input_item->ui->LA_resister);
//    ui->t5_input_table->setCellWidget(1,2,t5_input_item->ui->LE_resister);
//    ui->t5_input_table->setCellWidget(0,3,t5_input_item->ui->LA_N2_blow);
//    ui->t5_input_table->setCellWidget(1,3,t5_input_item->ui->WCB_N2_blow);
    ui->t5_input_table->setCellWidget(0,4,t5_input_item->ui->LA_au_f_add);
    ui->t5_input_table->setCellWidget(1,4,t5_input_item->ui->LE_au_f_add);
    ui->t5_input_table->setCellWidget(0,5,t5_input_item->ui->LA_now_r_weight);
    ui->t5_input_table->setCellWidget(1,5,t5_input_item->ui->LE_now_r_weight);
    ui->t5_input_table->setCellWidget(0,6,t5_input_item->ui->LA_au_after_r_weight);
    ui->t5_input_table->setCellWidget(1,6,t5_input_item->ui->LE_au_after_r_weight);
    ui->t5_input_table->setCellWidget(0,7,t5_input_item->ui->LA_au_after_weight);
    ui->t5_input_table->setCellWidget(1,7,t5_input_item->ui->LE_au_after_weight);
//    ui->t5_input_table->setCellWidget(0,7,t5_input_item->ui->LA_au_use);
//    ui->t5_input_table->setCellWidget(1,7,t5_input_item->ui->LE_au_use);
//    ui->t5_input_table->setCellWidget(2,0,t5_input_item->ui->LA_ti_add);
//    ui->t5_input_table->setCellWidget(3,0,t5_input_item->ui->LE_ti_add);
//    ui->t5_input_table->setCellWidget(2,1,t5_input_item->ui->LA_al_add);
//    ui->t5_input_table->setCellWidget(3,1,t5_input_item->ui->LE_al_add);
//    ui->t5_input_table->setCellWidget(2,2,t5_input_item->ui->LA_xtal_life);
//    ui->t5_input_table->setCellWidget(3,2,t5_input_item->ui->LE_xtal_life);
    ui->t5_input_table->setCellWidget(2,2,t5_input_item->ui->W_X_TAL);
    ui->t5_input_table->setCellWidget(2,3,t5_input_item->ui->LA_name);
    ui->t5_input_table->setCellWidget(3,3,t5_input_item->ui->LE_name);
//    ui->t5_input_table->setCellWidget(2,4,t5_input_item->ui->LA_d_run);
//    ui->t5_input_table->setCellWidget(3,4,t5_input_item->ui->WCB_d_run);
    ui->t5_input_table->setCellWidget(2,5,t5_input_item->ui->LA_note);
    ui->t5_input_table->setCellWidget(3,5,t5_input_item->ui->LE_note);
    ui->t5_input_table->setSpan(2,2,2,1);
    ui->t5_input_table->setSpan(2,5,1,5);
    ui->t5_input_table->setSpan(3,5,1,5);
    ui->t5_input_table->setCellWidget(4,0,t5_input_item->ui->LE_recipe_number);
    ui->t5_input_table->setCellWidget(4,1,t5_input_item->ui->CB_recipe_choice);
//    ui->t5_input_table->setCellWidget(4,2,t5_input_item->ui->LA_recipe_2);
    ui->t5_input_table->setCellWidget(5,0,t5_input_item->ui->W_recipe);
    ui->t5_input_table->setSpan(4,1,1,5);
//    ui->t5_input_table->setSpan(4,2,1,3);
    ui->t5_input_table->setSpan(5,0,2,5);
    ui->t5_input_table->setCellWidget(4,6,t5_input_item->ui->LA_dom_number);
    ui->t5_input_table->setCellWidget(5,6,t5_input_item->ui->CB_domnumber);


    ui->t5_LE_Lot_count->setValidator(new QIntValidator(0, 1000, this));
//    ui->t5_LE_Run_number->setValidator(new QIntValidator(0, 1000, this));
    QSqlQuery query(my_mesdb);
    query.exec(QString("select `Lot_max_count`,`Run_number_count` from Thin_film_spec_managerment where machine_code = 'MD001'"));
    if(query.next()){
        ui->t5_LE_Lot_count->setText(query.value("Lot_max_count").toString());
//        ui->t5_LE_Run_number->setText(query.value("Run_number_count").toString());
    }
    t5_lot_item_list.clear();
    for(int i=0;i<ui->t5_LE_Lot_count->text().toInt();i++){

        input_lot_item *lot_item = new input_lot_item(ms_mesdb,t5_input_item,"MD001");
        t5_lot_item_list.append(lot_item);
        ui->t5_input_lot_table->insertRow(i);
        ui->t5_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
        ui->t5_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
        ui->t5_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
        ui->t5_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
        ui->t5_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
        ui->t5_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
        ui->t5_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
        ui->t5_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//        connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                t5_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
    }
    query.exec("SELECT * FROM `Thin_film_repcipe` where machine_code = 'MD001' ORDER BY `recipe_number` ASC  ");
    t5_input_item->ui->CB_recipe_choice->addItem(tr("recipe_choice"));
    while(query.next()){
        QString recipe_name = query.value("recipe_number").toString()+"."+ query.value("recipename").toString()
                                + "|"+query.value("metal_name").toString()+"|"+query.value("metal_thin").toString()+"|"
                                +query.value("type").toString();
        QPixmap pixmap(100,100);
        pixmap.fill(QColor(query.value("color").toString()));
        t5_input_item->ui->CB_recipe_choice->addItem(QIcon(pixmap),recipe_name);
    }
    connect(t5_input_item->ui->CB_recipe_choice,SIGNAL(currentIndexChanged(int)),this,SLOT(t5_recipe_choice(int)));

    t5_ti_chart = new Thin_film_chart();
    t5_ti_chartview = new Thin_film_chart_view(t5_ti_chart);
    connect(t5_ti_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t5_ti_y_value_slot(QPointF)));
    ui->t5_Ti_layout->addWidget(t5_ti_chartview);

    t5_al_chart = new Thin_film_chart();
    t5_al_chartview = new Thin_film_chart_view(t5_al_chart);
    connect(t5_al_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t5_al_y_value_slot(QPointF)));
    ui->t5_Al_layout->addWidget(t5_al_chartview);
    t5_ti_axisX = new QDateTimeAxis();
    t5_ti_axisX->setFormat("MM-dd HH:mm:ss");
    t5_ti_axisX->setTitleText("Date");
    t5_ti_axisX->setVisible(true);
    t5_ti_axisX->setTitleVisible(true);
    t5_ti_chart->addAxis(t5_ti_axisX, Qt::AlignBottom);
    t5_ti_axisY = new QValueAxis();
    t5_ti_chart->addAxis(t5_ti_axisY,Qt::AlignLeft);

    t5_al_axisX = new QDateTimeAxis();
    t5_al_axisX->setFormat("MM-dd HH:mm:ss");
    t5_al_axisX->setTitleText("Date");
    t5_al_axisX->setVisible(true);
    t5_al_axisX->setTitleVisible(true);
    t5_al_chart->addAxis(t5_al_axisX, Qt::AlignBottom);
    t5_al_axisY = new QValueAxis();
    t5_al_chart->addAxis(t5_al_axisY,Qt::AlignLeft);

    ti_al_chart_init(t5_ti_chart,t5_al_chart,t5_ti_value_series,t5_al_value_series,t5_ti_UCL_value_series,t5_al_UCL_value_series,
                     t5_al_axisX,t5_al_axisY,t5_ti_axisX,t5_ti_axisY,"MD001");
    query.exec("SELECT machine_name,machine_code FROM Thin_film_spec_managerment");
    while(query.next()){
        ui->t5_CB_move_machine->addItem(query.value("machine_name").toString());
    }
    temp_run_widget_map.insert("MD001",ui->t5_temp_run_mode_time);
    temp_run_ti_accmulate_widget_map.insert("MD001",ui->t5_ti_accmulate);
    temp_run_al_accmulate_widget_map.insert("MD001",ui->t5_al_accmulate);
    ui->t5_CB_wet_insert->addItems(wet_machine_list);
    sync_now_date("MD001",t5_input_item->ui->LE_au_f_add,t5_input_item->ui->LE_now_r_weight,t5_input_item->ui->LE_au_after_r_weight
                  ,t5_input_item->ui->LE_au_after_weight);
    ui->t5_data_table->selectionModel()->setObjectName("t5_data");
    connect(ui->t5_data_table->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this,SLOT(table_view_selection_change(QModelIndex,QModelIndex)));

}
void Thin_film_mainwindows::t5_recipe_choice(int idx)
{
    if(idx == 0){
        t5_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t5_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t5_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t5_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t5_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t5_input_item->ui->LE_Recipe_name1->setText("");
        t5_input_item->ui->LE_Recipe_name2->setText("");
        t5_input_item->ui->LE_Recipe_name3->setText("");
        t5_input_item->ui->LE_Recipe_name4->setText("");
        t5_input_item->ui->LE_Recipe_name5->setText("");
        return ;
    }
    QSqlQuery query(my_mesdb);
    query.exec(QString("select * from `Thin_film_repcipe` where recipe_number  = %1 AND machine_code = 'MD001'").arg(idx));

    if(query.next()){
        t5_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t5_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t5_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t5_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t5_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t5_input_item->ui->LE_Recipe_name1->setText("");
        t5_input_item->ui->LE_Recipe_name2->setText("");
        t5_input_item->ui->LE_Recipe_name3->setText("");
        t5_input_item->ui->LE_Recipe_name4->setText("");
        t5_input_item->ui->LE_Recipe_name5->setText("");
        if(query.value("metal_name").toString() != ""){
            QString item_data = query.value("metal_name").toString();
            QStringList item_list = item_data.split("/");
            for(int i=0;i<item_list.count();i++){
                if(i==0){
                    t5_input_item->ui->CB_Recipe_name1->setCurrentText(item_list.at(i));
                }else if(i==1){
                    t5_input_item->ui->CB_Recipe_name2->setCurrentText(item_list.at(i));
                }else if(i==2){
                    t5_input_item->ui->CB_Recipe_name3->setCurrentText(item_list.at(i));
                }else if(i==3){
                    t5_input_item->ui->CB_Recipe_name4->setCurrentText(item_list.at(i));
                }else if(i==4){
                    t5_input_item->ui->CB_Recipe_name5->setCurrentText(item_list.at(i));
                }
            }
        }
        if(query.value("metal_thin").toString() != ""){
            QStringList recipe_item = query.value("metal_thin").toString().split("/");
            if(recipe_item.count()>0){
                t5_input_item->ui->LE_Recipe_name1->setText(QString("%1").arg(recipe_item[0]));
            }
            if(recipe_item.count()>1){
                t5_input_item->ui->LE_Recipe_name2->setText(QString("%1").arg(recipe_item[1]));
            }
            if(recipe_item.count()>2){
                t5_input_item->ui->LE_Recipe_name3->setText(QString("%1").arg(recipe_item[2]));
            }
            if(recipe_item.count()>3){
                t5_input_item->ui->LE_Recipe_name4->setText(QString("%1").arg(recipe_item[3]));
            }
            if(recipe_item.count()>4){
                t5_input_item->ui->LE_Recipe_name5->setText(QString("%1").arg(recipe_item[4]));
            }
        }
    }
}
void Thin_film_mainwindows::t5_ti_y_value_slot(QPointF value)
{
    ui->t5_ti_y_value->setText(QString("%1").arg(value.y()));
}
void Thin_film_mainwindows::t5_al_y_value_slot(QPointF value)
{
    ui->t5_al_y_value->setText(QString("%1").arg(value.y()));
}
void Thin_film_mainwindows::t5_datamodel_dataChanged(QModelIndex index1, QModelIndex index2, QVector<int> vector)
{
     v1_data_model_change(index1,t5_data_model,"MD001");
     ui->t5_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t5_LE_Lot_count_editingFinished()
{
//    if(ui->t5_LE_Lot_count->text()!= ""){
//        QSqlQuery query(my_mesdb);
//        query.exec(QString("update `Thin_film_spec_managerment` SET `Lot_max_count`='%1' Where `machine_code`='MD001'").arg(ui->t5_LE_Lot_count->text()));
//        while(ui->t5_input_lot_table->rowCount()>0){
//            ui->t5_input_lot_table->removeRow(0);
//        }
//        t5_lot_item_list.clear();
//        for(int i=0;i<ui->t5_LE_Lot_count->text().toInt();i++){

//            input_lot_item *lot_item = new input_lot_item(ms_mesdb,"MD009");
//            t5_lot_item_list.append(lot_item);
//            ui->t5_input_lot_table->insertRow(i);
//            ui->t5_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//    //        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
//            ui->t5_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
//            ui->t5_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
//            ui->t5_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
//            ui->t5_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
//            ui->t5_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
//            ui->t5_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
//            ui->t5_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//            connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                    t5_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
//        }
//    }
}
void Thin_film_mainwindows::on_t5_LE_Run_number_editingFinished()
{
//    if(ui->t5_LE_Run_number->text() != ""){
//        QSqlQuery query(my_mesdb);
//        query.exec(QString("update `Thin_film_spec_managerment` SET `Run_number_count`='%1' Where `machine_code`='MD001'").arg(ui->t5_LE_Run_number->text()));
//    }

}
void Thin_film_mainwindows::on_t5_Input_btn_clicked()
{

//    QString Run_number = ui->t5_LE_Run_number->text();
//    if(Run_number.toInt() > 60){
//        ui->t5_LE_Run_number->setText("1");
//        on_t5_LE_Run_number_editingFinished();
//        QMessageBox msg;
//        msg.addButton(QMessageBox::Ok);
//        msg.setText(tr("60 over set 1"));
//        msg.exec();
//    }
    QString Lot_count = ui->t5_LE_Lot_count->text();
    QString resister = t5_input_item->ui->LE_resister->text();
//    QString N2_blow = QString("%1").arg(QVariant(t5_input_item->ui->CB_N2_blow->isChecked()).toInt());
    QString au_f_add = t5_input_item->ui->LE_au_f_add->text();
    QString now_r_weight = t5_input_item->ui->LE_now_r_weight->text();
    QString au_after_r_weight = t5_input_item->ui->LE_au_after_r_weight->text();
    QString au_use =  t5_input_item->ui->LE_au_use->text();
    QString ti_add =  t5_input_item->ui->LE_ti_add->text();
    QString al_add =  t5_input_item->ui->LE_al_add->text();
    QString xtal_life =  t5_input_item->ui->LE_xtal_life->text();
    QString name =  t5_input_item->ui->LE_name->text();
//    QString d_use =  QString("%1").arg(QVariant(t5_input_item->ui->CB_d_run->isChecked()).toInt());
    QString X_tal_number = t5_input_item->ui->LE_Xtal_number->text();
    QString X_tal_Hz = t5_input_item->ui->LE_Xtal_hz->text();

    QString note =  t5_input_item->ui->LE_note->text();

    ui->t5_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t5_search_time_start->setDateTime(ui->t5_search_time_end->dateTime().addDays(-2));

    QString Recipe_total_text;
    QString Recipe_total_name_text;
    QString Recipe_name1 = t5_input_item->ui->LE_Recipe_name1->text();
    QString Recipe_name2 = t5_input_item->ui->LE_Recipe_name2->text();
    QString Recipe_name3 = t5_input_item->ui->LE_Recipe_name3->text();
    QString Recipe_name4 = t5_input_item->ui->LE_Recipe_name4->text();
    QString Recipe_name5 = t5_input_item->ui->LE_Recipe_name5->text();
    QString CB_Recipe_name1 = t5_input_item->ui->CB_Recipe_name1->currentText();
    QString CB_Recipe_name2 = t5_input_item->ui->CB_Recipe_name2->currentText();
    QString CB_Recipe_name3 = t5_input_item->ui->CB_Recipe_name3->currentText();
    QString CB_Recipe_name4 = t5_input_item->ui->CB_Recipe_name4->currentText();
    QString CB_Recipe_name5 = t5_input_item->ui->CB_Recipe_name5->currentText();
    int Recipe_count = 0;
    int ti_count = 0;
    int au_count = 0;
    int al_count = 0;

    if(CB_Recipe_name1 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name1));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name1));
        if(CB_Recipe_name1=="Ti"){
            ti_count += Recipe_name1.toInt();
        }else if(CB_Recipe_name1=="Al"){
            al_count += Recipe_name1.toInt();
        }else if(CB_Recipe_name1=="Au"){
            au_count += Recipe_name1.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name2 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name2));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name2));
        if(CB_Recipe_name2=="Ti"){
            ti_count += Recipe_name2.toInt();
        }else if(CB_Recipe_name2=="Al"){
            al_count += Recipe_name2.toInt();
        }else if(CB_Recipe_name2=="Au"){
            au_count += Recipe_name2.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name3 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name3));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name3));
        if(CB_Recipe_name3=="Ti"){
            ti_count += Recipe_name3.toInt();
        }else if(CB_Recipe_name3=="Al"){
            al_count += Recipe_name3.toInt();
        }else if(CB_Recipe_name3=="Au"){
            au_count += Recipe_name3.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name4 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name4));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name4));
        if(CB_Recipe_name4=="Ti"){
            ti_count += Recipe_name4.toInt();
        }else if(CB_Recipe_name4=="Al"){
            al_count += Recipe_name4.toInt();
        }else if(CB_Recipe_name4=="Au"){
            au_count += Recipe_name4.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name5 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name5));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name5));
        if(CB_Recipe_name5=="Ti"){
            ti_count += Recipe_name5.toInt();
        }else if(CB_Recipe_name5=="Al"){
            al_count += Recipe_name5.toInt();
        }else if(CB_Recipe_name5=="Au"){
            au_count += Recipe_name5.toInt();
        }
        Recipe_count++;
    }
    if(Recipe_count>0){
        Recipe_total_text.remove(Recipe_total_text.length()-1,1);
        Recipe_total_name_text.remove(Recipe_total_name_text.length()-1,1);
    }
    QSqlQuery query(my_mesdb);
    QString batch_id;
    query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_code = 'MD001'"));
    if(query.next()){
        batch_id = query.value("current_batch_id").toString();
        query.exec("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+1  where machine_code = 'MD001'");
    }
    bool query_result = false;
    qDebug()<<ui->t5_input_lot_table->rowCount();
    query.exec("select now() as now_time");
    QDateTime now_time;

    now_time = QDateTime::currentDateTime();
    for(int i=0;i<ui->t5_input_lot_table->rowCount();i++){
        QString Lot_id = t5_lot_item_list.at(i)->ui->LE_Lot_id->text();
        QString wafer_count = t5_lot_item_list.at(i)->ui->LE_wafer_count->text();
        QString ashing1 = t5_lot_item_list.at(i)->ui->LE_ashing1->text();
        QString ashing2 = t5_lot_item_list.at(i)->ui->LE_ashing2->text();
        QString ashing3 = t5_lot_item_list.at(i)->ui->LE_ashing3->text();
        QString ashing_machine = t5_lot_item_list.at(i)->ui->LE_ashing_machine->text();
        QString machine_type = t5_lot_item_list.at(i)->ui->LE_machine_type->text();
        QString wafer_angle = t5_lot_item_list.at(i)->ui->LE_wafer_angle->text();

        if(Lot_id.trimmed() !=""){
            QString query_str = QString("INSERT INTO `Thin_film_data` "
                                        "(`Run_number`, `Resister`, `machine_type`, `Lot_id`, `Wafer_count`, "
                                        "`Wafer_angle`, `Input_thin_data`, `ashing1`, `ashing2`, "
                                        "`ashing3`, `ashing_machine`, `Pad_thin`, `Wlp_thin`, "
                                        "`N2_Blow`, `Au_f_add`, `Now_r_weight`, `Au_after_weight`, "
                                        "`Au_use`, `Recipe`, `Ti_add`, `Al_add`, `X_tal_number`,`X_tal_Hz`, "
                                        "`X_tal_life`, `User_name`, `D_use`, `note`, `Ti_total`, "
                                        "`Al_total`, `Au_total`, `batch_id`, `cleaning_cal_flag`,`machine_code`,`machine_name`,"
                                        "`Input_time`,`vacuum_time`,`day_and_night`,`au_pellet_output`,`au_add_weight`,`dom`) "
                                        "VALUES "
                                        "('%1', '%2', '%3', '%4', '%5', '%6', "
                                        "'%7', '%8', '%9', '%10', "
                                        "'%11', '%12', '%13', '%14', '%15', '%16', "
                                        "'%17', '%18', '%19', '%20', '%21', '%22','%23', "
                                        "'%24', '%25', '%26', '%27', '%28', "
                                        "'%29', '%30', '%31' ,'%32','%33','%34','%35','%36','%37','%38','%39','%40' );").arg("").arg(resister).arg(machine_type).arg(Lot_id).arg(wafer_count)
                                                                      .arg(wafer_angle).arg(Recipe_total_text).arg(ashing1).arg(ashing2)
                                                                      .arg(ashing3).arg(ashing_machine).arg(QString("0")).arg(QString("0"))
                                                                      .arg("").arg(au_f_add).arg(now_r_weight).arg(au_after_r_weight)
                                                                      .arg(au_use).arg(Recipe_total_name_text).arg(ti_add).arg(al_add).arg(X_tal_number).arg(X_tal_Hz)
                                                                      .arg(xtal_life).arg(name).arg("").arg(note).arg(ti_count)
                                                                      .arg(al_count).arg(au_count).arg(batch_id).arg(QString("0")).arg("MD001").arg("BPS")
                                                                      .arg(now_time.toString("yyyy-MM-dd hh:mm:ss"))
                                                                      .arg(t5_input_item->ui->DT_vacuum_time->dateTime().toString("yyyy-MM-dd hh:mm:00"))
                                                                      .arg(t5_input_item->ui->CB_day_and_night->currentText())
                                                                      .arg(t5_input_item->ui->DT_vacuum_time->dateTime().addSecs(-300).toString("yyyy-MM-dd hh:mm:00"))
                                                                      .arg(t5_input_item->ui->LE_au_after_weight->text())
                                                                      .arg(t5_input_item->ui->CB_domnumber->currentText());
            query_result = query.exec(query_str);

            if(!query_result){
                break;
            }

        }
    }
    au_calc(now_time);
    if(query_result){
        t5_input_item->clear_data();
        for(int i=0;i<ui->t5_input_lot_table->rowCount();i++){
            t5_lot_item_list.at(i)->clear_data();
        }
        QMessageBox msg;
        msg.addButton(QMessageBox::Ok);
        msg.setText(tr("input complete"));
        msg.exec();
//        int run_count = ui->t5_LE_Run_number->text().toInt();
//        run_count++;
//        ui->t5_LE_Run_number->setText(QString("%1").arg(run_count));
        on_t5_LE_Run_number_editingFinished();
        ui->t5_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
        ui->t5_search_time_start->setDateTime(ui->t5_search_time_end->dateTime().addDays(-2));
        t5_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
        on_t5_search_btn_clicked();
        ui->t5_data_table->scrollToBottom();
    }else {
        QMessageBox::warning(this, tr("conntion false"),
                                                       "server connection fail\n"
                                                          ""+my_mesdb.lastError().text(),
                                                            QMessageBox::Close);
    }
    query.exec(QString("select * from depostion_au_use_current_view where machine_name = '%1' AND batch_id = %2")
               .arg("BPS").arg(batch_id));
    if(query.next()){
        double  OKNG = query.value("OKNG").toDouble();
        if(abs(OKNG)>0.1){
            QMessageBox msg;
            msg.addButton(QMessageBox::Ok);
            msg.setText(tr("au pellet NG"));
            msg.exec();
        }
    }
    query.exec("update `Thin_film_spec_managerment` set au_pullet_add = 0 ,now_r_weight = 0,au_after_r_weight =0,au_after_weight=0 where machine_code = 'MD001'");
    ti_al_chart_init(t5_ti_chart,t5_al_chart,t5_ti_value_series,t5_al_value_series,t5_ti_UCL_value_series,t5_al_UCL_value_series,
                     t5_al_axisX,t5_al_axisY,t5_ti_axisX,t5_ti_axisY,"MD001");

}
void Thin_film_mainwindows::on_t5_search_btn_clicked()
{
    t5_data_model->setFilter(QString("vacuum_time Between '%1' AND '%2' AND machine_code = 'MD001'")
                          .arg(ui->t5_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t5_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    t5_data_model->select();
    t5_data_model->setSort(2,Qt::AscendingOrder);
    QSqlQuery query(my_mesdb);
    query.exec(QString("select sum((case when (`A`.`machine_code` = 'MD001') then `A`.`Wafer_count` end)) AS `wafer` from `Thin_film_data` `A` "
               "where vacuum_time Between '%1' AND '%2' AND machine_code = 'MD001'").arg(ui->t5_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
               .arg(ui->t5_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    if(query.next()){

        ui->t5_total_wafer->setText(query.value("wafer").toString());
    }
    ui->t5_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t5_BT_search_lot_id_clicked()
{
    t5_data_model->setFilter(QString("Lot_id = '%1' AND machine_code = 'MD001'")
                          .arg(ui->t4_LE_search_lot_id->text()));
    t5_data_model->select();
    t5_data_model->setSort(1,Qt::AscendingOrder);
}
void Thin_film_mainwindows::on_t5_ti_al_refresh_clicked()
{
    ti_al_chart_init(t5_ti_chart,t5_al_chart,t5_ti_value_series,t5_al_value_series,t5_ti_UCL_value_series,t5_al_UCL_value_series,
                     t5_al_axisX,t5_al_axisY,t5_ti_axisX,t5_ti_axisY,"MD001");
}
void Thin_film_mainwindows::on_t5_DT_del_btn_clicked()
{
    int count = ui->t5_data_table->selectionModel()->selectedIndexes().count();
    QVector<int> index_history;
    for(int i=0;i<count;i++){
        if(!index_history.contains(ui->t5_data_table->selectionModel()->selectedIndexes().at(i).row())){
        QDateTime Input_time2 = t5_data_model->record(ui->t5_data_table->selectionModel()->selectedIndexes().at(i).row()).value("Input_time").toDateTime();
        t5_data_model->removeRow(ui->t5_data_table->selectionModel()->selectedIndexes().at(i).row());
         au_calc(Input_time2);
         index_history.append(ui->t5_data_table->selectionModel()->selectedIndexes().at(i).row());
        }else {

        }
    }
    on_t5_search_btn_clicked();
    ui->t5_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t5_move_machine_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    QString batch_id_temp = "0";
    QString machine_name;
    QString machine_code;

    int count  = ui->t5_data_table->selectionModel()->selectedIndexes().count();
    for(int i=0;i<count;i++){
         QSqlRecord recode = t5_data_model->record(ui->t5_data_table->selectionModel()->selectedIndexes().at(i).row());
        if(batch_id_temp != recode.value("batch_id").toString()){
            query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_name = '%1'").arg(ui->t5_CB_move_machine->currentText()));
            if(query.next()){
                batch_id_temp = query.value("current_batch_id").toString()+5;
                machine_name = query.value("machine_name").toString();
                machine_code = query.value("machine_code").toString();
                query.exec(QString("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+1  where machine_name = '%1'").arg(ui->t5_CB_move_machine->currentText()));
            }
            batch_id_temp = recode.value("batch_id").toString();
        }

        recode.setGenerated("batch_id",true);
        recode.setGenerated("machine_name",true);
        recode.setGenerated("machine_code",true);
        recode.setValue("batch_id",batch_id_temp);
        recode.setValue("machine_name",machine_name);
        recode.setValue("machine_code",machine_code);
        t5_data_model->setRecord(ui->t5_data_table->selectionModel()->selectedIndexes().at(i).row(),recode);
    }
    on_t5_search_btn_clicked();
    ui->t5_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t5_excel_header_copy_flag_toggled(bool checked)
{
    ui->t5_data_table->copyheader_flag = checked;
}
void Thin_film_mainwindows::on_t5_machine_temp_run_mode_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    query.exec("update Thin_film_spec_managerment set nomal_mode_time = ADDTIME(NOW(),'0:5:0') where `machine_code`='MD001'");
}


//t5_function -- end --

//t6_function -- start --
void Thin_film_mainwindows::t6_init()
{
    ui->t6_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t6_search_time_start->setDateTime(ui->t6_search_time_end->dateTime().addDays(-2));

    QSqlTableModel *spec_model = new QSqlTableModel(this,my_mesdb);
    spec_model->setTable("Thin_film_spec_managerment");
    spec_model->setFilter("machine_code = 'MD005'");
    spec_model->select();
    spec_model->setHeaderData(4,Qt::Horizontal,tr("ashing_LCL"));
    spec_model->setHeaderData(5,Qt::Horizontal,tr("ashing_UCL"));
    spec_model->setHeaderData(6,Qt::Horizontal,tr("WLPTiAlashing_LCL"));
    spec_model->setHeaderData(7,Qt::Horizontal,tr("WLPTiAlashing_UCL"));
    spec_model->setHeaderData(8,Qt::Horizontal,tr("WLP_LCL_Ti_Au"));
    spec_model->setHeaderData(9,Qt::Horizontal,tr("WLP_UCL_Ti_Au"));
    spec_model->setHeaderData(10,Qt::Horizontal,tr("WLP_LCL_Ti_Al"));
    spec_model->setHeaderData(11,Qt::Horizontal,tr("WLP_UCL_Ti_Al"));
    spec_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    t6_data_model = new thin_sqltable_model(this,my_mesdb,"MD005",spec_model);

    t6_data_model->setTable("Thin_film_data");

    t6_data_model->setFilter(QString("Input_time Between '%1' AND '%2' AND machine_code = 'MD005'")
                          .arg(ui->t6_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t6_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    ui->t6_data_table->setItemDelegateForColumn(t6_data_model->fieldIndex("vacuum_time"),new DateDelegate(ui->t6_data_table));
    ui->t6_data_table->setItemDelegateForColumn(t6_data_model->fieldIndex("Input_time"),new DateDelegate(ui->t6_data_table));
    ui->t6_data_table->setModel(t6_data_model);
    ui->t6_data_table->setSortingEnabled(true);
    connect(t6_data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>))
            ,this,SLOT(t6_datamodel_dataChanged(QModelIndex,QModelIndex,QVector<int>)));
    ui->t6_data_table->setItemDelegateForColumn(10,new Thin_combodelegate(note_combo_list,ui->t6_data_table));

    t6_data_model->setEditStrategy(QSqlTableModel::OnFieldChange);
    t6_data_model->setSort(2,Qt::AscendingOrder);

    t6_data_model->select();

    t6_data_model->setHeaderData(t6_data_model->fieldIndex("day_and_night"),Qt::Horizontal,tr("day_and_night"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("vacuum_time"),Qt::Horizontal,tr("vacuum_time"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Run_number"),Qt::Horizontal,tr("thin_Run_number"));
//    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Resister"),Qt::Horizontal,tr("thin_Resister"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("machine_type"),Qt::Horizontal,tr("thin_machine_type"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Lot_id"),Qt::Horizontal,tr("thin_Lot_id"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Wafer_count"),Qt::Horizontal,tr("thin_Wafer_count"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Pad_thin"),Qt::Horizontal,tr("thin_Pad_thin"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Wlp_thin"),Qt::Horizontal,tr("thin_Wlp_thin"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Wafer_angle"),Qt::Horizontal,tr("thin_Wafer_angle"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Input_thin_data"),Qt::Horizontal,tr("thin_Input_thin_data"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Recipe"),Qt::Horizontal,tr("thin_Recipe"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("ashing1"),Qt::Horizontal,tr("thin_ashing1"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("ashing2"),Qt::Horizontal,tr("thin_ashing2"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("ashing3"),Qt::Horizontal,tr("thin_ashing3"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("ashing_machine"),Qt::Horizontal,tr("thin_ashing_machine"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("N2_Blow"),Qt::Horizontal,tr("thin_N2_Blow"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Au_f_add"),Qt::Horizontal,tr("thin_thin_Au_f_add"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Now_r_weight"),Qt::Horizontal,tr("thin_Now_r_weight"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Au_after_weight"),Qt::Horizontal,tr("thin_Au_after_weight"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Au_use"),Qt::Horizontal,tr("thin_Au_use"));
//    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Ti_add"),Qt::Horizontal,tr("thin_Ti_add"));
//    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Al_add"),Qt::Horizontal,tr("thin_Al_add"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("X_tal_number"),Qt::Horizontal,tr("thin_X_tal_number"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("X_tal_Hz"),Qt::Horizontal,tr("thin_X_tal_Hz"));
//    t6_data_model->setHeaderData(t6_data_model->fieldIndex("X_tal_life"),Qt::Horizontal,tr("thin_X_tal_life"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("User_name"),Qt::Horizontal,tr("thin_User_name"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("D_use"),Qt::Horizontal,tr("thin_D_use"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("note"),Qt::Horizontal,tr("thin_note"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Ti_total"),Qt::Horizontal,tr("thin_Ti_total"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Al_total"),Qt::Horizontal,tr("thin_Al_total"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Au_total"),Qt::Horizontal,tr("thin_Au_total"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("Input_time"),Qt::Horizontal,tr("thin_Input_time"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("machine_name"),Qt::Horizontal,tr("thin_machine_name"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("machine_code"),Qt::Horizontal,tr("thin_machine_code"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("batch_id"),Qt::Horizontal,tr("thin_batch_id"));
    t6_data_model->setHeaderData(t6_data_model->fieldIndex("cleaning_cal_flag"),Qt::Horizontal,tr("thin_cleaning_cal_flag"));
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("day_and_night"),50);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("vacuum_time"),110);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Run_number"),60);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Resister"),50);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("machine_type"),100);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Lot_id"),80);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Wafer_count"),50);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Pad_thin"),80);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Wlp_thin"),80);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Wafer_angle"),80);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Input_thin_data"),150);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Recipe"),150);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("ashing1"),65);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("ashing2"),65);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("ashing3"),65);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("ashing_machine"),80);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("N2_Blow"),80);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Au_f_add"),80);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Now_r_weight"),120);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Au_after_weight"),140);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Au_use"),50);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Ti_add"),50);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Al_add"),50);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("X_tal_number"),50);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("X_tal_Hz"),50);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("X_tal_life"),80);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("User_name"),80);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("D_use"),80);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("note"),140);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Ti_total"),60);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Al_total"),60);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Au_total"),60);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("Input_time"),120);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("machine_name"),50);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("machine_code"),50);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("batch_id"),50);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("cleaning_cal_flag"),50);
    ui->t6_data_table->horizontalHeader()->resizeSection(t6_data_model->fieldIndex("dom"),50);
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("X_tal_life"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("machine_code"));
//    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("batch_id"));
//    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("cleaning_cal_flag"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("Au_use"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("au_pellet_output"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("au_add"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("check_user"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("au_pellet_total"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("work_start_au"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("au_add_weight"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("au_case_weight"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("check_NG"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("Run_number"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("Pad_thin"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("Wlp_thin"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("N2_Blow"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("D_use"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("Ti_add"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("Al_add"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("X_tal_life"));
    ui->t6_data_table->horizontalHeader()->hideSection(t6_data_model->fieldIndex("Resister"));

    ui->t6_data_table->scrollToBottom();


    ui->t6_spec_table->setModel(spec_model);
    ui->t6_spec_table->horizontalHeader()->hideSection(0);
    ui->t6_spec_table->horizontalHeader()->hideSection(1);
    ui->t6_spec_table->horizontalHeader()->hideSection(2);
    ui->t6_spec_table->horizontalHeader()->hideSection(3);
    ui->t6_spec_table->horizontalHeader()->hideSection(4);
    ui->t6_spec_table->horizontalHeader()->hideSection(5);
    ui->t6_spec_table->horizontalHeader()->hideSection(6);
    ui->t6_spec_table->horizontalHeader()->hideSection(7);
    ui->t6_spec_table->horizontalHeader()->hideSection(8);
    ui->t6_spec_table->horizontalHeader()->hideSection(9);
    ui->t6_spec_table->horizontalHeader()->hideSection(10);
    ui->t6_spec_table->horizontalHeader()->hideSection(11);
    ui->t6_spec_table->horizontalHeader()->hideSection(12);
    ui->t6_spec_table->horizontalHeader()->hideSection(13);
    ui->t6_spec_table->horizontalHeader()->hideSection(14);
    ui->t6_spec_table->horizontalHeader()->hideSection(17);
    ui->t6_spec_table->horizontalHeader()->hideSection(18);
    ui->t6_spec_table->horizontalHeader()->hideSection(19);
    ui->t6_spec_table->horizontalHeader()->hideSection(20);
    ui->t6_spec_table->horizontalHeader()->hideSection(21);
    ui->t6_spec_table->horizontalHeader()->hideSection(22);
    ui->t6_spec_table->horizontalHeader()->hideSection(23);
    ui->t6_spec_table->horizontalHeader()->hideSection(24);
    ui->t6_spec_table->horizontalHeader()->hideSection(25);
    ui->t6_spec_table->horizontalHeader()->hideSection(26);
    ui->t6_spec_table->horizontalHeader()->hideSection(27);
    ui->t6_spec_table->horizontalHeader()->hideSection(28);
    ui->t6_spec_table->horizontalHeader()->hideSection(29);
    ui->t6_spec_table->horizontalHeader()->hideSection(30);
    ui->t6_spec_table->horizontalHeader()->hideSection(31);
    ui->t6_spec_table->horizontalHeader()->hideSection(32);
    ui->t6_spec_table->horizontalHeader()->hideSection(33);
    ui->t6_spec_table->horizontalHeader()->hideSection(34);
    ui->t6_spec_table->horizontalHeader()->hideSection(35);
    ui->t6_spec_table->horizontalHeader()->resizeSection(6,150);
    ui->t6_spec_table->horizontalHeader()->resizeSection(7,150);

    for(int i=0;i<8;i++){
        ui->t6_input_table->insertColumn(i);
    }
    ui->t6_input_table->insertRow(0);
    ui->t6_input_table->insertRow(1);
    ui->t6_input_table->insertRow(2);
    ui->t6_input_table->insertRow(3);
    ui->t6_input_table->insertRow(4);
    ui->t6_input_table->insertRow(5);
    ui->t6_input_table->insertRow(6);
    ui->t6_input_table->horizontalHeader()->resizeSection(0,70);
    ui->t6_input_table->horizontalHeader()->resizeSection(1,150);
    ui->t6_input_table->horizontalHeader()->resizeSection(3,70);
    ui->t6_input_table->horizontalHeader()->resizeSection(5,130);
    ui->t6_input_table->horizontalHeader()->resizeSection(6,140);
    ui->t6_input_table->horizontalHeader()->resizeSection(7,80);
    t6_input_item = new input_item(my_mesdb,"MD005");
    ui->t6_input_table->setCellWidget(0,0,t6_input_item->ui->LA_day_and_night);
    ui->t6_input_table->setCellWidget(1,0,t6_input_item->ui->CB_day_and_night);
    ui->t6_input_table->setCellWidget(0,1,t6_input_item->ui->LA_vacuum_time);
    ui->t6_input_table->setCellWidget(1,1,t6_input_item->ui->DT_vacuum_time);
    t6_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
//    ui->t6_input_table->setCellWidget(0,2,t6_input_item->ui->LA_resister);
//    ui->t6_input_table->setCellWidget(1,2,t6_input_item->ui->LE_resister);
//    ui->t6_input_table->setCellWidget(0,3,t6_input_item->ui->LA_N2_blow);
//    ui->t6_input_table->setCellWidget(1,3,t6_input_item->ui->WCB_N2_blow);
    ui->t6_input_table->setCellWidget(0,4,t6_input_item->ui->LA_au_f_add);
    ui->t6_input_table->setCellWidget(1,4,t6_input_item->ui->LE_au_f_add);
    ui->t6_input_table->setCellWidget(0,5,t6_input_item->ui->LA_now_r_weight);
    ui->t6_input_table->setCellWidget(1,5,t6_input_item->ui->LE_now_r_weight);
    ui->t6_input_table->setCellWidget(0,6,t6_input_item->ui->LA_au_after_r_weight);
    ui->t6_input_table->setCellWidget(1,6,t6_input_item->ui->LE_au_after_r_weight);
    ui->t6_input_table->setCellWidget(0,7,t6_input_item->ui->LA_au_after_weight);
    ui->t6_input_table->setCellWidget(1,7,t6_input_item->ui->LE_au_after_weight);
//    ui->t6_input_table->setCellWidget(0,7,t6_input_item->ui->LA_au_use);
//    ui->t6_input_table->setCellWidget(1,7,t6_input_item->ui->LE_au_use);
//    ui->t6_input_table->setCellWidget(2,0,t6_input_item->ui->LA_ti_add);
//    ui->t6_input_table->setCellWidget(3,0,t6_input_item->ui->LE_ti_add);
//    ui->t6_input_table->setCellWidget(2,1,t6_input_item->ui->LA_al_add);
//    ui->t6_input_table->setCellWidget(3,1,t6_input_item->ui->LE_al_add);
//    ui->t6_input_table->setCellWidget(2,2,t6_input_item->ui->LA_xtal_life);
//    ui->t6_input_table->setCellWidget(3,2,t6_input_item->ui->LE_xtal_life);
    ui->t6_input_table->setCellWidget(2,2,t6_input_item->ui->W_X_TAL);
    ui->t6_input_table->setCellWidget(2,3,t6_input_item->ui->LA_name);
    ui->t6_input_table->setCellWidget(3,3,t6_input_item->ui->LE_name);
//    ui->t6_input_table->setCellWidget(2,4,t6_input_item->ui->LA_d_run);
//    ui->t6_input_table->setCellWidget(3,4,t6_input_item->ui->WCB_d_run);
    ui->t6_input_table->setCellWidget(2,5,t6_input_item->ui->LA_note);
    ui->t6_input_table->setCellWidget(3,5,t6_input_item->ui->LE_note);
    ui->t6_input_table->setSpan(2,2,2,1);
    ui->t6_input_table->setSpan(2,5,1,5);
    ui->t6_input_table->setSpan(3,5,1,5);
    ui->t6_input_table->setCellWidget(4,0,t6_input_item->ui->LE_recipe_number);
    ui->t6_input_table->setCellWidget(4,1,t6_input_item->ui->CB_recipe_choice);
//    ui->t6_input_table->setCellWidget(4,2,t6_input_item->ui->LA_recipe_2);
    ui->t6_input_table->setCellWidget(5,0,t6_input_item->ui->W_recipe);
    ui->t6_input_table->setSpan(4,1,1,5);
//    ui->t6_input_table->setSpan(4,2,1,3);
    ui->t6_input_table->setSpan(5,0,2,5);
    ui->t6_input_table->setCellWidget(4,6,t6_input_item->ui->LA_dom_number);
    ui->t6_input_table->setCellWidget(5,6,t6_input_item->ui->CB_domnumber);

    ui->t6_LE_Lot_count->setValidator(new QIntValidator(0, 1000, this));
//    ui->t6_LE_Run_number->setValidator(new QIntValidator(0, 1000, this));
    QSqlQuery query(my_mesdb);
    query.exec(QString("select `Lot_max_count`,`Run_number_count` from Thin_film_spec_managerment where machine_code = 'MD005'"));
    if(query.next()){
        ui->t6_LE_Lot_count->setText(query.value("Lot_max_count").toString());
//        ui->t6_LE_Run_number->setText(query.value("Run_number_count").toString());
    }
    t6_lot_item_list.clear();
    for(int i=0;i<ui->t6_LE_Lot_count->text().toInt();i++){

        input_lot_item *lot_item = new input_lot_item(ms_mesdb,t6_input_item,"MD005");
        t6_lot_item_list.append(lot_item);
        ui->t6_input_lot_table->insertRow(i);
        ui->t6_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
        ui->t6_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
        ui->t6_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
        ui->t6_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
        ui->t6_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
        ui->t6_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
        ui->t6_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
        ui->t6_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//        connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                t6_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
    }
    query.exec("SELECT * FROM `Thin_film_repcipe` where machine_code = 'MD005' ORDER BY `recipe_number` ASC  ");
    t6_input_item->ui->CB_recipe_choice->addItem(tr("recipe_choice"));
    while(query.next()){
        QString recipe_name = query.value("recipe_number").toString()+"."+ query.value("recipename").toString()
                                + "|"+query.value("metal_name").toString()+"|"+query.value("metal_thin").toString()+"|"
                                +query.value("type").toString();
        QPixmap pixmap(100,100);
        pixmap.fill(QColor(query.value("color").toString()));
        t6_input_item->ui->CB_recipe_choice->addItem(QIcon(pixmap),recipe_name);
    }
    connect(t6_input_item->ui->CB_recipe_choice,SIGNAL(currentIndexChanged(int)),this,SLOT(t6_recipe_choice(int)));

    t6_ti_chart = new Thin_film_chart();
    t6_ti_chartview = new Thin_film_chart_view(t6_ti_chart);
    connect(t6_ti_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t6_ti_y_value_slot(QPointF)));
    ui->t6_Ti_layout->addWidget(t6_ti_chartview);

    t6_al_chart = new Thin_film_chart();
    t6_al_chartview = new Thin_film_chart_view(t6_al_chart);
    connect(t6_al_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t6_al_y_value_slot(QPointF)));
    ui->t6_Al_layout->addWidget(t6_al_chartview);
    t6_ti_axisX = new QDateTimeAxis();
    t6_ti_axisX->setFormat("MM-dd HH:mm:ss");
    t6_ti_axisX->setTitleText("Date");
    t6_ti_axisX->setVisible(true);
    t6_ti_axisX->setTitleVisible(true);
    t6_ti_chart->addAxis(t6_ti_axisX, Qt::AlignBottom);
    t6_ti_axisY = new QValueAxis();
    t6_ti_chart->addAxis(t6_ti_axisY,Qt::AlignLeft);

    t6_al_axisX = new QDateTimeAxis();
    t6_al_axisX->setFormat("MM-dd HH:mm:ss");
    t6_al_axisX->setTitleText("Date");
    t6_al_axisX->setVisible(true);
    t6_al_axisX->setTitleVisible(true);
    t6_al_chart->addAxis(t6_al_axisX, Qt::AlignBottom);
    t6_al_axisY = new QValueAxis();
    t6_al_chart->addAxis(t6_al_axisY,Qt::AlignLeft);

    ti_al_chart_init(t6_ti_chart,t6_al_chart,t6_ti_value_series,t6_al_value_series,t6_ti_UCL_value_series,t6_al_UCL_value_series,
                     t6_al_axisX,t6_al_axisY,t6_ti_axisX,t6_ti_axisY,"MD005");
    query.exec("SELECT machine_name,machine_code FROM Thin_film_spec_managerment");
    while(query.next()){
        ui->t6_CB_move_machine->addItem(query.value("machine_name").toString());
    }
    temp_run_widget_map.insert("MD005",ui->t6_temp_run_mode_time);
    temp_run_ti_accmulate_widget_map.insert("MD005",ui->t6_ti_accmulate);
    temp_run_al_accmulate_widget_map.insert("MD005",ui->t6_al_accmulate);
    ui->t6_CB_wet_insert->addItems(wet_machine_list);
    sync_now_date("MD005",t6_input_item->ui->LE_au_f_add,t6_input_item->ui->LE_now_r_weight,t6_input_item->ui->LE_au_after_r_weight
                  ,t6_input_item->ui->LE_au_after_weight);

    ui->t6_data_table->selectionModel()->setObjectName("t6_data");
    connect(ui->t6_data_table->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this,SLOT(table_view_selection_change(QModelIndex,QModelIndex)));
}
void Thin_film_mainwindows::t6_recipe_choice(int idx)
{
    if(idx == 0){
        t6_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t6_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t6_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t6_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t6_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t6_input_item->ui->LE_Recipe_name1->setText("");
        t6_input_item->ui->LE_Recipe_name2->setText("");
        t6_input_item->ui->LE_Recipe_name3->setText("");
        t6_input_item->ui->LE_Recipe_name4->setText("");
        t6_input_item->ui->LE_Recipe_name5->setText("");
        return ;
    }
    QSqlQuery query(my_mesdb);
    query.exec(QString("select * from `Thin_film_repcipe` where recipe_number  = %1 AND machine_code = 'MD005'").arg(idx));

    if(query.next()){
        t6_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t6_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t6_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t6_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t6_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t6_input_item->ui->LE_Recipe_name1->setText("");
        t6_input_item->ui->LE_Recipe_name2->setText("");
        t6_input_item->ui->LE_Recipe_name3->setText("");
        t6_input_item->ui->LE_Recipe_name4->setText("");
        t6_input_item->ui->LE_Recipe_name5->setText("");
        if(query.value("metal_name").toString() != ""){
            QString item_data = query.value("metal_name").toString();
            QStringList item_list = item_data.split("/");
            for(int i=0;i<item_list.count();i++){
                if(i==0){
                    t6_input_item->ui->CB_Recipe_name1->setCurrentText(item_list.at(i));
                }else if(i==1){
                    t6_input_item->ui->CB_Recipe_name2->setCurrentText(item_list.at(i));
                }else if(i==2){
                    t6_input_item->ui->CB_Recipe_name3->setCurrentText(item_list.at(i));
                }else if(i==3){
                    t6_input_item->ui->CB_Recipe_name4->setCurrentText(item_list.at(i));
                }else if(i==4){
                    t6_input_item->ui->CB_Recipe_name5->setCurrentText(item_list.at(i));
                }
            }
        }
        if(query.value("metal_thin").toString() != ""){
            QStringList recipe_item = query.value("metal_thin").toString().split("/");
            if(recipe_item.count()>0){
                t6_input_item->ui->LE_Recipe_name1->setText(QString("%1").arg(recipe_item[0]));
            }
            if(recipe_item.count()>1){
                t6_input_item->ui->LE_Recipe_name2->setText(QString("%1").arg(recipe_item[1]));
            }
            if(recipe_item.count()>2){
                t6_input_item->ui->LE_Recipe_name3->setText(QString("%1").arg(recipe_item[2]));
            }
            if(recipe_item.count()>3){
                t6_input_item->ui->LE_Recipe_name4->setText(QString("%1").arg(recipe_item[3]));
            }
            if(recipe_item.count()>4){
                t6_input_item->ui->LE_Recipe_name5->setText(QString("%1").arg(recipe_item[4]));
            }
        }
    }}
void Thin_film_mainwindows::t6_ti_y_value_slot(QPointF value)
{
     ui->t6_ti_y_value->setText(QString("%1").arg(value.y()));
}
void Thin_film_mainwindows::t6_al_y_value_slot(QPointF value)
{
    ui->t6_al_y_value->setText(QString("%1").arg(value.y()));
}
void Thin_film_mainwindows::t6_datamodel_dataChanged(QModelIndex index1, QModelIndex index2, QVector<int> vector)
{
    v1_data_model_change(index1,t6_data_model,"MD005");
    ui->t6_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t6_LE_Lot_count_editingFinished()
{
//    if(ui->t6_LE_Lot_count->text()!= ""){
//        QSqlQuery query(my_mesdb);
//        query.exec(QString("update `Thin_film_spec_managerment` SET `Lot_max_count`='%1' Where `machine_code`='MD005'").arg(ui->t6_LE_Lot_count->text()));
//        while(ui->t6_input_lot_table->rowCount()>0){
//            ui->t6_input_lot_table->removeRow(0);
//        }
//        t6_lot_item_list.clear();
//        for(int i=0;i<ui->t6_LE_Lot_count->text().toInt();i++){

//            input_lot_item *lot_item = new input_lot_item(ms_mesdb,"MD009");
//            t6_lot_item_list.append(lot_item);
//            ui->t6_input_lot_table->insertRow(i);
//            ui->t6_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//    //        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
//            ui->t6_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
//            ui->t6_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
//            ui->t6_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
//            ui->t6_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
//            ui->t6_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
//            ui->t6_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
//            ui->t6_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//            connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                    t6_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
//        }
//    }
}
void Thin_film_mainwindows::on_t6_LE_Run_number_editingFinished()
{
//    if(ui->t6_LE_Run_number->text() != ""){
//        QSqlQuery query(my_mesdb);
//        query.exec(QString("update `Thin_film_spec_managerment` SET `Run_number_count`='%1' Where `machine_code`='MD005'").arg(ui->t6_LE_Run_number->text()));
//    }

}
void Thin_film_mainwindows::on_t6_Input_btn_clicked()
{
//    QString Run_number = ui->t6_LE_Run_number->text();
//    if(Run_number.toInt() > 60){
//        ui->t6_LE_Run_number->setText("1");
//        on_t6_LE_Run_number_editingFinished();
//        QMessageBox msg;
//        msg.addButton(QMessageBox::Ok);
//        msg.setText(tr("60 over set 1"));
//        msg.exec();
//    }
     QString Lot_count = ui->t6_LE_Lot_count->text();
     QString resister = t6_input_item->ui->LE_resister->text();
//     QString N2_blow = QString("%1").arg(QVariant(t6_input_item->ui->CB_N2_blow->isChecked()).toInt());
     QString au_f_add = t6_input_item->ui->LE_au_f_add->text();
     QString now_r_weight = t6_input_item->ui->LE_now_r_weight->text();
     QString au_after_r_weight = t6_input_item->ui->LE_au_after_r_weight->text();
     QString au_use =  t6_input_item->ui->LE_au_use->text();
     QString ti_add =  t6_input_item->ui->LE_ti_add->text();
     QString al_add =  t6_input_item->ui->LE_al_add->text();
     QString xtal_life =  t6_input_item->ui->LE_xtal_life->text();
     QString name =  t6_input_item->ui->LE_name->text();
//     QString d_use =  QString("%1").arg(QVariant(t6_input_item->ui->CB_d_run->isChecked()).toInt());
     QString X_tal_number = t6_input_item->ui->LE_Xtal_number->text();
     QString X_tal_Hz = t6_input_item->ui->LE_Xtal_hz->text();

     QString note =  t6_input_item->ui->LE_note->text();

     ui->t6_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
     ui->t6_search_time_start->setDateTime(ui->t6_search_time_end->dateTime().addDays(-2));

     QString Recipe_total_text;
     QString Recipe_total_name_text;
     QString Recipe_name1 = t6_input_item->ui->LE_Recipe_name1->text();
     QString Recipe_name2 = t6_input_item->ui->LE_Recipe_name2->text();
     QString Recipe_name3 = t6_input_item->ui->LE_Recipe_name3->text();
     QString Recipe_name4 = t6_input_item->ui->LE_Recipe_name4->text();
     QString Recipe_name5 = t6_input_item->ui->LE_Recipe_name5->text();
     QString CB_Recipe_name1 = t6_input_item->ui->CB_Recipe_name1->currentText();
     QString CB_Recipe_name2 = t6_input_item->ui->CB_Recipe_name2->currentText();
     QString CB_Recipe_name3 = t6_input_item->ui->CB_Recipe_name3->currentText();
     QString CB_Recipe_name4 = t6_input_item->ui->CB_Recipe_name4->currentText();
     QString CB_Recipe_name5 = t6_input_item->ui->CB_Recipe_name5->currentText();
     int Recipe_count = 0;
     int ti_count = 0;
     int au_count = 0;
     int al_count = 0;

     if(CB_Recipe_name1 != ""){
         Recipe_total_text.append(QString("%1/").arg(Recipe_name1));
         Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name1));
         if(CB_Recipe_name1=="Ti"){
             ti_count += Recipe_name1.toInt();
         }else if(CB_Recipe_name1=="Al"){
             al_count += Recipe_name1.toInt();
         }else if(CB_Recipe_name1=="Au"){
             au_count += Recipe_name1.toInt();
         }
         Recipe_count++;
     }
     if(CB_Recipe_name2 != ""){
         Recipe_total_text.append(QString("%1/").arg(Recipe_name2));
         Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name2));
         if(CB_Recipe_name2=="Ti"){
             ti_count += Recipe_name2.toInt();
         }else if(CB_Recipe_name2=="Al"){
             al_count += Recipe_name2.toInt();
         }else if(CB_Recipe_name2=="Au"){
             au_count += Recipe_name2.toInt();
         }
         Recipe_count++;
     }
     if(CB_Recipe_name3 != ""){
         Recipe_total_text.append(QString("%1/").arg(Recipe_name3));
         Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name3));
         if(CB_Recipe_name3=="Ti"){
             ti_count += Recipe_name3.toInt();
         }else if(CB_Recipe_name3=="Al"){
             al_count += Recipe_name3.toInt();
         }else if(CB_Recipe_name3=="Au"){
             au_count += Recipe_name3.toInt();
         }
         Recipe_count++;
     }
     if(CB_Recipe_name4 != ""){
         Recipe_total_text.append(QString("%1/").arg(Recipe_name4));
         Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name4));
         if(CB_Recipe_name4=="Ti"){
             ti_count += Recipe_name4.toInt();
         }else if(CB_Recipe_name4=="Al"){
             al_count += Recipe_name4.toInt();
         }else if(CB_Recipe_name4=="Au"){
             au_count += Recipe_name4.toInt();
         }
         Recipe_count++;
     }
     if(CB_Recipe_name5 != ""){
         Recipe_total_text.append(QString("%1/").arg(Recipe_name5));
         Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name5));
         if(CB_Recipe_name5=="Ti"){
             ti_count += Recipe_name5.toInt();
         }else if(CB_Recipe_name5=="Al"){
             al_count += Recipe_name5.toInt();
         }else if(CB_Recipe_name5=="Au"){
             au_count += Recipe_name5.toInt();
         }
         Recipe_count++;
     }
     if(Recipe_count>0){
         Recipe_total_text.remove(Recipe_total_text.length()-1,1);
         Recipe_total_name_text.remove(Recipe_total_name_text.length()-1,1);
     }
     QSqlQuery query(my_mesdb);
     QString batch_id;
     query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_code = 'MD005'"));
     if(query.next()){
         batch_id = query.value("current_batch_id").toString();
         query.exec("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+1  where machine_code = 'MD005'");
     }
     bool query_result = false;
     qDebug()<<ui->t6_input_lot_table->rowCount();
     query.exec("select now() as now_time");
     QDateTime now_time;

     now_time = QDateTime::currentDateTime();
     for(int i=0;i<ui->t6_input_lot_table->rowCount();i++){
         QString Lot_id = t6_lot_item_list.at(i)->ui->LE_Lot_id->text();
         QString wafer_count = t6_lot_item_list.at(i)->ui->LE_wafer_count->text();
         QString ashing1 = t6_lot_item_list.at(i)->ui->LE_ashing1->text();
         QString ashing2 = t6_lot_item_list.at(i)->ui->LE_ashing2->text();
         QString ashing3 = t6_lot_item_list.at(i)->ui->LE_ashing3->text();
         QString ashing_machine = t6_lot_item_list.at(i)->ui->LE_ashing_machine->text();
         QString machine_type = t6_lot_item_list.at(i)->ui->LE_machine_type->text();
         QString wafer_angle = t6_lot_item_list.at(i)->ui->LE_wafer_angle->text();

         if(Lot_id.trimmed() !=""){
             QString query_str = QString("INSERT INTO `Thin_film_data` "
                                         "(`Run_number`, `Resister`, `machine_type`, `Lot_id`, `Wafer_count`, "
                                         "`Wafer_angle`, `Input_thin_data`, `ashing1`, `ashing2`, "
                                         "`ashing3`, `ashing_machine`, `Pad_thin`, `Wlp_thin`, "
                                         "`N2_Blow`, `Au_f_add`, `Now_r_weight`, `Au_after_weight`, "
                                         "`Au_use`, `Recipe`, `Ti_add`, `Al_add`, `X_tal_number`,`X_tal_Hz`, "
                                         "`X_tal_life`, `User_name`, `D_use`, `note`, `Ti_total`, "
                                         "`Al_total`, `Au_total`, `batch_id`, `cleaning_cal_flag`,`machine_code`,`machine_name`,"
                                         "`Input_time`,`vacuum_time`,`day_and_night`,`au_pellet_output`,`au_add_weight`,`dom`) "
                                         "VALUES "
                                         "('%1', '%2', '%3', '%4', '%5', '%6', "
                                         "'%7', '%8', '%9', '%10', "
                                         "'%11', '%12', '%13', '%14', '%15', '%16', "
                                         "'%17', '%18', '%19', '%20', '%21', '%22','%23', "
                                         "'%24', '%25', '%26', '%27', '%28', "
                                         "'%29', '%30', '%31' ,'%32','%33','%34','%35','%36','%37','%38','%39','%40' );").arg("").arg(resister).arg(machine_type).arg(Lot_id).arg(wafer_count)
                                                                       .arg(wafer_angle).arg(Recipe_total_text).arg(ashing1).arg(ashing2)
                                                                       .arg(ashing3).arg(ashing_machine).arg(QString("0")).arg(QString("0"))
                                                                       .arg("").arg(au_f_add).arg(now_r_weight).arg(au_after_r_weight)
                                                                       .arg(au_use).arg(Recipe_total_name_text).arg(ti_add).arg(al_add).arg(X_tal_number).arg(X_tal_Hz)
                                                                       .arg(xtal_life).arg(name).arg("").arg(note).arg(ti_count)
                                                                       .arg(al_count).arg(au_count).arg(batch_id).arg(QString("0")).arg("MD005").arg("EVATEC")
                                                                       .arg(now_time.toString("yyyy-MM-dd hh:mm:ss"))
                                                                       .arg(t6_input_item->ui->DT_vacuum_time->dateTime().toString("yyyy-MM-dd hh:mm:00"))
                                                                       .arg(t6_input_item->ui->CB_day_and_night->currentText())
                                                                       .arg(t6_input_item->ui->DT_vacuum_time->dateTime().addSecs(-300).toString("yyyy-MM-dd hh:mm:00"))
                                                                       .arg(t6_input_item->ui->LE_au_after_weight->text())
                                                                       .arg(t6_input_item->ui->CB_domnumber->currentText());
             query_result = query.exec(query_str);


             if(!query_result){
                 break;
             }

         }
     }
     au_calc(now_time);
     if(query_result){
         t6_input_item->clear_data();
         for(int i=0;i<ui->t6_input_lot_table->rowCount();i++){
             t6_lot_item_list.at(i)->clear_data();
         }
         QMessageBox msg;
         msg.addButton(QMessageBox::Ok);
         msg.setText(tr("input complete"));
         msg.exec();
//         int run_count = ui->t6_LE_Run_number->text().toInt();
//         run_count++;
//         ui->t6_LE_Run_number->setText(QString("%1").arg(run_count));
         on_t6_LE_Run_number_editingFinished();
         ui->t6_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
         ui->t6_search_time_start->setDateTime(ui->t6_search_time_end->dateTime().addDays(-2));
         t6_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
         on_t6_search_btn_clicked();
         ui->t6_data_table->scrollToBottom();
     }else {
         QMessageBox::warning(this, tr("conntion false"),
                                                        "server connection fail\n"
                                                           ""+my_mesdb.lastError().text(),
                                                             QMessageBox::Close);
     }
     query.exec(QString("select * from depostion_au_use_current_view where machine_name = '%1' AND batch_id = %2")
                .arg("EVATEC").arg(batch_id));
     if(query.next()){
         double  OKNG = query.value("OKNG").toDouble();
         if(abs(OKNG)>0.1){
             QMessageBox msg;
             msg.addButton(QMessageBox::Ok);
             msg.setText(tr("au pellet NG"));
             msg.exec();
         }
     }
     query.exec("update `Thin_film_spec_managerment` set au_pullet_add = 0 ,now_r_weight = 0,au_after_r_weight =0,au_after_weight=0 where machine_code = 'MD005'");
     ti_al_chart_init(t6_ti_chart,t6_al_chart,t6_ti_value_series,t6_al_value_series,t6_ti_UCL_value_series,t6_al_UCL_value_series,
                      t6_al_axisX,t6_al_axisY,t6_ti_axisX,t6_ti_axisY,"MD005");
}
void Thin_film_mainwindows::on_t6_search_btn_clicked()
{
    t6_data_model->setFilter(QString("vacuum_time Between '%1' AND '%2' AND machine_code = 'MD005'")
                          .arg(ui->t6_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t6_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    t6_data_model->select();
    t6_data_model->setSort(2,Qt::AscendingOrder);
    QSqlQuery query(my_mesdb);
    query.exec(QString("select sum((case when (`A`.`machine_code` = 'MD005') then `A`.`Wafer_count` end)) AS `wafer` from `Thin_film_data` `A` "
               "where vacuum_time Between '%1' AND '%2' AND machine_code = 'MD005'").arg(ui->t6_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
               .arg(ui->t6_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    if(query.next()){

        ui->t6_total_wafer->setText(query.value("wafer").toString());
    }
    ui->t6_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t6_BT_search_lot_id_clicked()
{
    t6_data_model->setFilter(QString("Lot_id = '%1' AND machine_code = 'MD005'")
                          .arg(ui->t4_LE_search_lot_id->text()));
    t6_data_model->select();
    t6_data_model->setSort(1,Qt::AscendingOrder);
}
void Thin_film_mainwindows::on_t6_ti_al_refresh_clicked()
{
    ti_al_chart_init(t6_ti_chart,t6_al_chart,t6_ti_value_series,t6_al_value_series,t6_ti_UCL_value_series,t6_al_UCL_value_series,
                     t6_al_axisX,t6_al_axisY,t6_ti_axisX,t6_ti_axisY,"MD005");
}
void Thin_film_mainwindows::on_t6_DT_del_btn_clicked()
{
    int count = ui->t6_data_table->selectionModel()->selectedIndexes().count();
    QVector<int> index_history;
    for(int i=0;i<count;i++){
        if(!index_history.contains(ui->t6_data_table->selectionModel()->selectedIndexes().at(i).row())){
        QDateTime Input_time2 = t6_data_model->record(ui->t6_data_table->selectionModel()->selectedIndexes().at(i).row()).value("Input_time").toDateTime();
        t6_data_model->removeRow(ui->t6_data_table->selectionModel()->selectedIndexes().at(i).row());
         au_calc(Input_time2);
         index_history.append(ui->t6_data_table->selectionModel()->selectedIndexes().at(i).row());
        }else {

        }
    }
    on_t6_search_btn_clicked();
    ui->t6_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t6_move_machine_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    QString batch_id_temp = "0";
    QString machine_name;
    QString machine_code;

    int count  = ui->t6_data_table->selectionModel()->selectedIndexes().count();
    for(int i=0;i<count;i++){
         QSqlRecord recode = t6_data_model->record(ui->t6_data_table->selectionModel()->selectedIndexes().at(i).row());
        if(batch_id_temp != recode.value("batch_id").toString()){
            query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_name = '%1'").arg(ui->t6_CB_move_machine->currentText()));
            if(query.next()){
                batch_id_temp = query.value("current_batch_id").toString()+5;
                machine_name = query.value("machine_name").toString();
                machine_code = query.value("machine_code").toString();
                query.exec(QString("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+10  where machine_name = '%1'").arg(ui->t6_CB_move_machine->currentText()));
            }
            batch_id_temp = recode.value("batch_id").toString();
        }

        recode.setGenerated("batch_id",true);
        recode.setGenerated("machine_name",true);
        recode.setGenerated("machine_code",true);
        recode.setValue("batch_id",batch_id_temp);
        recode.setValue("machine_name",machine_name);
        recode.setValue("machine_code",machine_code);
        t6_data_model->setRecord(ui->t6_data_table->selectionModel()->selectedIndexes().at(i).row(),recode);
    }
    on_t6_search_btn_clicked();
    ui->t6_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t6_excel_header_copy_flag_toggled(bool checked)
{
    ui->t6_data_table->copyheader_flag = checked;
}
void Thin_film_mainwindows::on_t6_machine_temp_run_mode_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    query.exec("update Thin_film_spec_managerment set nomal_mode_time = ADDTIME(NOW(),'0:5:0') where `machine_code`='MD005'");
}


//t6_function -- end --

//t7_function -- start --
void Thin_film_mainwindows::t7_init()
{
    ui->t7_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t7_search_time_start->setDateTime(ui->t7_search_time_end->dateTime().addDays(-2));

    QSqlTableModel *spec_model = new QSqlTableModel(this,my_mesdb);
    spec_model->setTable("Thin_film_spec_managerment");
    spec_model->setFilter("machine_code = 'MD014'");
    spec_model->select();
    spec_model->setHeaderData(4,Qt::Horizontal,tr("ashing_LCL"));
    spec_model->setHeaderData(5,Qt::Horizontal,tr("ashing_UCL"));
    spec_model->setHeaderData(6,Qt::Horizontal,tr("WLPTiAlashing_LCL"));
    spec_model->setHeaderData(7,Qt::Horizontal,tr("WLPTiAlashing_UCL"));
    spec_model->setHeaderData(8,Qt::Horizontal,tr("WLP_LCL_Ti_Au"));
    spec_model->setHeaderData(9,Qt::Horizontal,tr("WLP_UCL_Ti_Au"));
    spec_model->setHeaderData(10,Qt::Horizontal,tr("WLP_LCL_Ti_Al"));
    spec_model->setHeaderData(11,Qt::Horizontal,tr("WLP_UCL_Ti_Al"));
    spec_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    t7_data_model = new thin_sqltable_model(this,my_mesdb,"MD014",spec_model);

    t7_data_model->setTable("Thin_film_data");

    t7_data_model->setFilter(QString("Input_time Between '%1' AND '%2' AND machine_code = 'MD014'")
                          .arg(ui->t7_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t7_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    ui->t7_data_table->setItemDelegateForColumn(t7_data_model->fieldIndex("vacuum_time"),new DateDelegate(ui->t7_data_table));
    ui->t7_data_table->setItemDelegateForColumn(t7_data_model->fieldIndex("Input_time"),new DateDelegate(ui->t7_data_table));
    ui->t7_data_table->setModel(t7_data_model);
    ui->t7_data_table->setSortingEnabled(true);
    connect(t7_data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>))
            ,this,SLOT(t7_datamodel_dataChanged(QModelIndex,QModelIndex,QVector<int>)));
    ui->t7_data_table->setItemDelegateForColumn(10,new Thin_combodelegate(note_combo_list,ui->t7_data_table));

    t7_data_model->setEditStrategy(QSqlTableModel::OnFieldChange);
    t7_data_model->setSort(2,Qt::AscendingOrder);

    t7_data_model->select();

    t7_data_model->setHeaderData(t7_data_model->fieldIndex("day_and_night"),Qt::Horizontal,tr("day_and_night"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("vacuum_time"),Qt::Horizontal,tr("vacuum_time"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Run_number"),Qt::Horizontal,tr("thin_Run_number"));
//    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Resister"),Qt::Horizontal,tr("thin_Resister"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("machine_type"),Qt::Horizontal,tr("thin_machine_type"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Lot_id"),Qt::Horizontal,tr("thin_Lot_id"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Wafer_count"),Qt::Horizontal,tr("thin_Wafer_count"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Pad_thin"),Qt::Horizontal,tr("thin_Pad_thin"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Wlp_thin"),Qt::Horizontal,tr("thin_Wlp_thin"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Wafer_angle"),Qt::Horizontal,tr("thin_Wafer_angle"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Input_thin_data"),Qt::Horizontal,tr("thin_Input_thin_data"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Recipe"),Qt::Horizontal,tr("thin_Recipe"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("ashing1"),Qt::Horizontal,tr("thin_ashing1"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("ashing2"),Qt::Horizontal,tr("thin_ashing2"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("ashing3"),Qt::Horizontal,tr("thin_ashing3"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("ashing_machine"),Qt::Horizontal,tr("thin_ashing_machine"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("N2_Blow"),Qt::Horizontal,tr("thin_N2_Blow"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Au_f_add"),Qt::Horizontal,tr("thin_thin_Au_f_add"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Now_r_weight"),Qt::Horizontal,tr("thin_Now_r_weight"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Au_after_weight"),Qt::Horizontal,tr("thin_Au_after_weight"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Au_use"),Qt::Horizontal,tr("thin_Au_use"));
//    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Ti_add"),Qt::Horizontal,tr("thin_Ti_add"));
//    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Al_add"),Qt::Horizontal,tr("thin_Al_add"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("X_tal_number"),Qt::Horizontal,tr("thin_X_tal_number"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("X_tal_Hz"),Qt::Horizontal,tr("thin_X_tal_Hz"));
//    t7_data_model->setHeaderData(t7_data_model->fieldIndex("X_tal_life"),Qt::Horizontal,tr("thin_X_tal_life"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("User_name"),Qt::Horizontal,tr("thin_User_name"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("D_use"),Qt::Horizontal,tr("thin_D_use"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("note"),Qt::Horizontal,tr("thin_note"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Ti_total"),Qt::Horizontal,tr("thin_Ti_total"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Al_total"),Qt::Horizontal,tr("thin_Al_total"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Au_total"),Qt::Horizontal,tr("thin_Au_total"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("Input_time"),Qt::Horizontal,tr("thin_Input_time"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("machine_name"),Qt::Horizontal,tr("thin_machine_name"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("machine_code"),Qt::Horizontal,tr("thin_machine_code"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("batch_id"),Qt::Horizontal,tr("thin_batch_id"));
    t7_data_model->setHeaderData(t7_data_model->fieldIndex("cleaning_cal_flag"),Qt::Horizontal,tr("thin_cleaning_cal_flag"));
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("day_and_night"),50);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("vacuum_time"),110);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Run_number"),60);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Resister"),50);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("machine_type"),100);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Lot_id"),80);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Wafer_count"),50);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Pad_thin"),80);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Wlp_thin"),80);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Wafer_angle"),80);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Input_thin_data"),150);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Recipe"),150);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("ashing1"),65);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("ashing2"),65);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("ashing3"),65);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("ashing_machine"),80);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("N2_Blow"),80);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Au_f_add"),80);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Now_r_weight"),120);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Au_after_weight"),140);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Au_use"),50);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Ti_add"),50);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Al_add"),50);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("X_tal_number"),50);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("X_tal_Hz"),50);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("X_tal_life"),80);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("User_name"),80);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("D_use"),80);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("note"),140);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Ti_total"),60);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Al_total"),60);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Au_total"),60);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("Input_time"),120);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("machine_name"),50);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("machine_code"),50);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("batch_id"),50);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("cleaning_cal_flag"),50);
    ui->t7_data_table->horizontalHeader()->resizeSection(t7_data_model->fieldIndex("dom"),50);
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("X_tal_life"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("machine_code"));
//    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("batch_id"));
//    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("cleaning_cal_flag"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("Au_use"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("au_pellet_output"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("au_add"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("check_user"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("au_pellet_total"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("work_start_au"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("au_add_weight"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("au_case_weight"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("check_NG"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("Run_number"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("Pad_thin"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("Wlp_thin"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("N2_Blow"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("D_use"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("Ti_add"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("Al_add"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("X_tal_life"));
    ui->t7_data_table->horizontalHeader()->hideSection(t7_data_model->fieldIndex("Resister"));


    ui->t7_data_table->scrollToBottom();


    ui->t7_spec_table->setModel(spec_model);
    ui->t7_spec_table->horizontalHeader()->hideSection(0);
    ui->t7_spec_table->horizontalHeader()->hideSection(1);
    ui->t7_spec_table->horizontalHeader()->hideSection(2);
    ui->t7_spec_table->horizontalHeader()->hideSection(3);
    ui->t7_spec_table->horizontalHeader()->hideSection(4);
    ui->t7_spec_table->horizontalHeader()->hideSection(5);
    ui->t7_spec_table->horizontalHeader()->hideSection(6);
    ui->t7_spec_table->horizontalHeader()->hideSection(7);
    ui->t7_spec_table->horizontalHeader()->hideSection(8);
    ui->t7_spec_table->horizontalHeader()->hideSection(9);
    ui->t7_spec_table->horizontalHeader()->hideSection(10);
    ui->t7_spec_table->horizontalHeader()->hideSection(11);
    ui->t7_spec_table->horizontalHeader()->hideSection(12);
    ui->t7_spec_table->horizontalHeader()->hideSection(13);
    ui->t7_spec_table->horizontalHeader()->hideSection(14);
    ui->t7_spec_table->horizontalHeader()->hideSection(17);
    ui->t7_spec_table->horizontalHeader()->hideSection(18);
    ui->t7_spec_table->horizontalHeader()->hideSection(19);
    ui->t7_spec_table->horizontalHeader()->hideSection(20);
    ui->t7_spec_table->horizontalHeader()->hideSection(21);
    ui->t7_spec_table->horizontalHeader()->hideSection(22);
    ui->t7_spec_table->horizontalHeader()->hideSection(23);
    ui->t7_spec_table->horizontalHeader()->hideSection(24);
    ui->t7_spec_table->horizontalHeader()->hideSection(25);
    ui->t7_spec_table->horizontalHeader()->hideSection(26);
    ui->t7_spec_table->horizontalHeader()->hideSection(27);
    ui->t7_spec_table->horizontalHeader()->hideSection(28);
    ui->t7_spec_table->horizontalHeader()->hideSection(29);
    ui->t7_spec_table->horizontalHeader()->hideSection(30);
    ui->t7_spec_table->horizontalHeader()->hideSection(31);
    ui->t7_spec_table->horizontalHeader()->hideSection(32);
    ui->t7_spec_table->horizontalHeader()->hideSection(33);
    ui->t7_spec_table->horizontalHeader()->hideSection(34);
    ui->t7_spec_table->horizontalHeader()->hideSection(35);
    ui->t7_spec_table->horizontalHeader()->resizeSection(6,150);
    ui->t7_spec_table->horizontalHeader()->resizeSection(7,150);

    for(int i=0;i<8;i++){
        ui->t7_input_table->insertColumn(i);
    }
    ui->t7_input_table->insertRow(0);
    ui->t7_input_table->insertRow(1);
    ui->t7_input_table->insertRow(2);
    ui->t7_input_table->insertRow(3);
    ui->t7_input_table->insertRow(4);
    ui->t7_input_table->insertRow(5);
    ui->t7_input_table->insertRow(6);
    ui->t7_input_table->horizontalHeader()->resizeSection(0,70);
    ui->t7_input_table->horizontalHeader()->resizeSection(1,150);
    ui->t7_input_table->horizontalHeader()->resizeSection(3,70);
    ui->t7_input_table->horizontalHeader()->resizeSection(5,130);
    ui->t7_input_table->horizontalHeader()->resizeSection(6,140);
    ui->t7_input_table->horizontalHeader()->resizeSection(7,80);
    t7_input_item = new input_item(my_mesdb,"MD014");
    ui->t7_input_table->setCellWidget(0,0,t7_input_item->ui->LA_day_and_night);
    ui->t7_input_table->setCellWidget(1,0,t7_input_item->ui->CB_day_and_night);
    ui->t7_input_table->setCellWidget(0,1,t7_input_item->ui->LA_vacuum_time);
    ui->t7_input_table->setCellWidget(1,1,t7_input_item->ui->DT_vacuum_time);
    t7_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
//    ui->t7_input_table->setCellWidget(0,2,t7_input_item->ui->LA_resister);
//    ui->t7_input_table->setCellWidget(1,2,t7_input_item->ui->LE_resister);
//    ui->t7_input_table->setCellWidget(0,3,t7_input_item->ui->LA_N2_blow);
//    ui->t7_input_table->setCellWidget(1,3,t7_input_item->ui->WCB_N2_blow);
    ui->t7_input_table->setCellWidget(0,4,t7_input_item->ui->LA_au_f_add);
    ui->t7_input_table->setCellWidget(1,4,t7_input_item->ui->LE_au_f_add);
    ui->t7_input_table->setCellWidget(0,5,t7_input_item->ui->LA_now_r_weight);
    ui->t7_input_table->setCellWidget(1,5,t7_input_item->ui->LE_now_r_weight);
    ui->t7_input_table->setCellWidget(0,6,t7_input_item->ui->LA_au_after_r_weight);
    ui->t7_input_table->setCellWidget(1,6,t7_input_item->ui->LE_au_after_r_weight);
    ui->t7_input_table->setCellWidget(0,7,t7_input_item->ui->LA_au_after_weight);
    ui->t7_input_table->setCellWidget(1,7,t7_input_item->ui->LE_au_after_weight);
//    ui->t7_input_table->setCellWidget(0,7,t7_input_item->ui->LA_au_use);
//    ui->t7_input_table->setCellWidget(1,7,t7_input_item->ui->LE_au_use);
//    ui->t7_input_table->setCellWidget(2,0,t7_input_item->ui->LA_ti_add);
//    ui->t7_input_table->setCellWidget(3,0,t7_input_item->ui->LE_ti_add);
//    ui->t7_input_table->setCellWidget(2,1,t7_input_item->ui->LA_al_add);
//    ui->t7_input_table->setCellWidget(3,1,t7_input_item->ui->LE_al_add);
//    ui->t7_input_table->setCellWidget(2,2,t7_input_item->ui->LA_xtal_life);
//    ui->t7_input_table->setCellWidget(3,2,t7_input_item->ui->LE_xtal_life);
    ui->t7_input_table->setCellWidget(2,2,t7_input_item->ui->W_X_TAL);
    ui->t7_input_table->setCellWidget(2,3,t7_input_item->ui->LA_name);
    ui->t7_input_table->setCellWidget(3,3,t7_input_item->ui->LE_name);
//    ui->t7_input_table->setCellWidget(2,4,t7_input_item->ui->LA_d_run);
//    ui->t7_input_table->setCellWidget(3,4,t7_input_item->ui->WCB_d_run);
    ui->t7_input_table->setCellWidget(2,5,t7_input_item->ui->LA_note);
    ui->t7_input_table->setCellWidget(3,5,t7_input_item->ui->LE_note);
    ui->t7_input_table->setSpan(2,2,2,1);
    ui->t7_input_table->setSpan(2,5,1,5);
    ui->t7_input_table->setSpan(3,5,1,5);
    ui->t7_input_table->setCellWidget(4,0,t7_input_item->ui->LE_recipe_number);
    ui->t7_input_table->setCellWidget(4,1,t7_input_item->ui->CB_recipe_choice);
//    ui->t7_input_table->setCellWidget(4,2,t7_input_item->ui->LA_recipe_2);
    ui->t7_input_table->setCellWidget(5,0,t7_input_item->ui->W_recipe);
    ui->t7_input_table->setSpan(4,1,1,5);
//    ui->t7_input_table->setSpan(4,2,1,3);
    ui->t7_input_table->setSpan(5,0,2,5);
    ui->t7_input_table->setCellWidget(4,6,t7_input_item->ui->LA_dom_number);
    ui->t7_input_table->setCellWidget(5,6,t7_input_item->ui->CB_domnumber);

    ui->t7_LE_Lot_count->setValidator(new QIntValidator(0, 1000, this));
//    ui->t7_LE_Run_number->setValidator(new QIntValidator(0, 1000, this));
    QSqlQuery query(my_mesdb);
    query.exec(QString("select `Lot_max_count`,`Run_number_count` from Thin_film_spec_managerment where machine_code = 'MD014'"));
    if(query.next()){
        ui->t7_LE_Lot_count->setText(query.value("Lot_max_count").toString());
//        ui->t7_LE_Run_number->setText(query.value("Run_number_count").toString());
    }
    t7_lot_item_list.clear();
    for(int i=0;i<ui->t7_LE_Lot_count->text().toInt();i++){

        input_lot_item *lot_item = new input_lot_item(ms_mesdb,t7_input_item,"MD014");
        t7_lot_item_list.append(lot_item);
        ui->t7_input_lot_table->insertRow(i);
        ui->t7_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
        ui->t7_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
        ui->t7_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
        ui->t7_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
        ui->t7_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
        ui->t7_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
        ui->t7_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
        ui->t7_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//        connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                t7_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
    }
    query.exec("SELECT * FROM `Thin_film_repcipe` where machine_code = 'MD014' ORDER BY `recipe_number` ASC  ");
    t7_input_item->ui->CB_recipe_choice->addItem(tr("recipe_choice"));
    while(query.next()){
        QString recipe_name = query.value("recipe_number").toString()+"."+ query.value("recipename").toString()
                                + "|"+query.value("metal_name").toString()+"|"+query.value("metal_thin").toString()+"|"
                                +query.value("type").toString();
        QPixmap pixmap(100,100);
        pixmap.fill(QColor(query.value("color").toString()));
        t7_input_item->ui->CB_recipe_choice->addItem(QIcon(pixmap),recipe_name);
    }
    connect(t7_input_item->ui->CB_recipe_choice,SIGNAL(currentIndexChanged(int)),this,SLOT(t7_recipe_choice(int)));

    t7_ti_chart = new Thin_film_chart();
    t7_ti_chartview = new Thin_film_chart_view(t7_ti_chart);
    connect(t7_ti_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t7_ti_y_value_slot(QPointF)));
    ui->t7_Ti_layout->addWidget(t7_ti_chartview);

    t7_al_chart = new Thin_film_chart();
    t7_al_chartview = new Thin_film_chart_view(t7_al_chart);
    connect(t7_al_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t7_al_y_value_slot(QPointF)));
    ui->t7_Al_layout->addWidget(t7_al_chartview);
    t7_ti_axisX = new QDateTimeAxis();
    t7_ti_axisX->setFormat("MM-dd HH:mm:ss");
    t7_ti_axisX->setTitleText("Date");
    t7_ti_axisX->setVisible(true);
    t7_ti_axisX->setTitleVisible(true);
    t7_ti_chart->addAxis(t7_ti_axisX, Qt::AlignBottom);
    t7_ti_axisY = new QValueAxis();
    t7_ti_chart->addAxis(t7_ti_axisY,Qt::AlignLeft);

    t7_al_axisX = new QDateTimeAxis();
    t7_al_axisX->setFormat("MM-dd HH:mm:ss");
    t7_al_axisX->setTitleText("Date");
    t7_al_axisX->setVisible(true);
    t7_al_axisX->setTitleVisible(true);
    t7_al_chart->addAxis(t7_al_axisX, Qt::AlignBottom);
    t7_al_axisY = new QValueAxis();
    t7_al_chart->addAxis(t7_al_axisY,Qt::AlignLeft);

    ti_al_chart_init(t7_ti_chart,t7_al_chart,t7_ti_value_series,t7_al_value_series,t7_ti_UCL_value_series,t7_al_UCL_value_series,
                     t7_al_axisX,t7_al_axisY,t7_ti_axisX,t7_ti_axisY,"MD014");
    query.exec("SELECT machine_name,machine_code FROM Thin_film_spec_managerment");
    while(query.next()){
        ui->t7_CB_move_machine->addItem(query.value("machine_name").toString());
    }
    temp_run_widget_map.insert("MD014",ui->t7_temp_run_mode_time);
    temp_run_ti_accmulate_widget_map.insert("MD014",ui->t7_ti_accmulate);
    temp_run_al_accmulate_widget_map.insert("MD014",ui->t7_al_accmulate);
    ui->t7_CB_wet_insert->addItems(wet_machine_list);
    sync_now_date("MD014",t7_input_item->ui->LE_au_f_add,t7_input_item->ui->LE_now_r_weight,t7_input_item->ui->LE_au_after_r_weight
                  ,t7_input_item->ui->LE_au_after_weight);
    ui->t7_data_table->selectionModel()->setObjectName("t7_data");
    connect(ui->t7_data_table->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this,SLOT(table_view_selection_change(QModelIndex,QModelIndex)));
}
void Thin_film_mainwindows::t7_recipe_choice(int idx)
{
    if(idx == 0){
        t7_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t7_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t7_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t7_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t7_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t7_input_item->ui->LE_Recipe_name1->setText("");
        t7_input_item->ui->LE_Recipe_name2->setText("");
        t7_input_item->ui->LE_Recipe_name3->setText("");
        t7_input_item->ui->LE_Recipe_name4->setText("");
        t7_input_item->ui->LE_Recipe_name5->setText("");
        return ;
    }
    QSqlQuery query(my_mesdb);
    query.exec(QString("select * from `Thin_film_repcipe` where recipe_number  = %1 AND machine_code = 'MD014'").arg(idx));

    if(query.next()){
        t7_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t7_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t7_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t7_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t7_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t7_input_item->ui->LE_Recipe_name1->setText("");
        t7_input_item->ui->LE_Recipe_name2->setText("");
        t7_input_item->ui->LE_Recipe_name3->setText("");
        t7_input_item->ui->LE_Recipe_name4->setText("");
        t7_input_item->ui->LE_Recipe_name5->setText("");
        if(query.value("metal_name").toString() != ""){
            QString item_data = query.value("metal_name").toString();
            QStringList item_list = item_data.split("/");
            for(int i=0;i<item_list.count();i++){
                if(i==0){
                    t7_input_item->ui->CB_Recipe_name1->setCurrentText(item_list.at(i));
                }else if(i==1){
                    t7_input_item->ui->CB_Recipe_name2->setCurrentText(item_list.at(i));
                }else if(i==2){
                    t7_input_item->ui->CB_Recipe_name3->setCurrentText(item_list.at(i));
                }else if(i==3){
                    t7_input_item->ui->CB_Recipe_name4->setCurrentText(item_list.at(i));
                }else if(i==4){
                    t7_input_item->ui->CB_Recipe_name5->setCurrentText(item_list.at(i));
                }
            }
        }
        if(query.value("metal_thin").toString() != ""){
            QStringList recipe_item = query.value("metal_thin").toString().split("/");
            if(recipe_item.count()>0){
                t7_input_item->ui->LE_Recipe_name1->setText(QString("%1").arg(recipe_item[0]));
            }
            if(recipe_item.count()>1){
                t7_input_item->ui->LE_Recipe_name2->setText(QString("%1").arg(recipe_item[1]));
            }
            if(recipe_item.count()>2){
                t7_input_item->ui->LE_Recipe_name3->setText(QString("%1").arg(recipe_item[2]));
            }
            if(recipe_item.count()>3){
                t7_input_item->ui->LE_Recipe_name4->setText(QString("%1").arg(recipe_item[3]));
            }
            if(recipe_item.count()>4){
                t7_input_item->ui->LE_Recipe_name5->setText(QString("%1").arg(recipe_item[4]));
            }
        }
    }
}

void Thin_film_mainwindows::t13_recipe_choice(int idx)
{
    if(idx == 0){
           t13_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
           t13_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
           t13_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
           t13_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
           t13_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
           t13_input_item->ui->LE_Recipe_name1->setText("");
           t13_input_item->ui->LE_Recipe_name2->setText("");
           t13_input_item->ui->LE_Recipe_name3->setText("");
           t13_input_item->ui->LE_Recipe_name4->setText("");
           t13_input_item->ui->LE_Recipe_name5->setText("");
           return ;
       }
       QSqlQuery query(my_mesdb);
       query.exec(QString("select * from `Thin_film_repcipe` where recipe_number  = %1 AND machine_code = 'MD042'").arg(idx));

       if(query.next()){
           t13_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
           t13_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
           t13_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
           t13_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
           t13_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
           t13_input_item->ui->LE_Recipe_name1->setText("");
           t13_input_item->ui->LE_Recipe_name2->setText("");
           t13_input_item->ui->LE_Recipe_name3->setText("");
           t13_input_item->ui->LE_Recipe_name4->setText("");
           t13_input_item->ui->LE_Recipe_name5->setText("");
           if(query.value("metal_name").toString() != ""){
               QString item_data = query.value("metal_name").toString();
               QStringList item_list = item_data.split("/");
               for(int i=0;i<item_list.count();i++){
                   if(i==0){
                       t13_input_item->ui->CB_Recipe_name1->setCurrentText(item_list.at(i));
                   }else if(i==1){
                       t13_input_item->ui->CB_Recipe_name2->setCurrentText(item_list.at(i));
                   }else if(i==2){
                       t13_input_item->ui->CB_Recipe_name3->setCurrentText(item_list.at(i));
                   }else if(i==3){
                       t13_input_item->ui->CB_Recipe_name4->setCurrentText(item_list.at(i));
                   }else if(i==4){
                       t13_input_item->ui->CB_Recipe_name5->setCurrentText(item_list.at(i));
                   }
               }
           }
           if(query.value("metal_thin").toString() != ""){
               QStringList recipe_item = query.value("metal_thin").toString().split("/");
               if(recipe_item.count()>0){
                   t13_input_item->ui->LE_Recipe_name1->setText(QString("%1").arg(recipe_item[0]));
               }
               if(recipe_item.count()>1){
                   t13_input_item->ui->LE_Recipe_name2->setText(QString("%1").arg(recipe_item[1]));
               }
               if(recipe_item.count()>2){
                   t13_input_item->ui->LE_Recipe_name3->setText(QString("%1").arg(recipe_item[2]));
               }
               if(recipe_item.count()>3){
                   t13_input_item->ui->LE_Recipe_name4->setText(QString("%1").arg(recipe_item[3]));
               }
               if(recipe_item.count()>4){
                   t13_input_item->ui->LE_Recipe_name5->setText(QString("%1").arg(recipe_item[4]));
               }
           }
       }
}
void Thin_film_mainwindows::t7_ti_y_value_slot(QPointF value)
{
    ui->t7_ti_y_value->setText(QString("%1").arg(value.y()));
}
void Thin_film_mainwindows::t7_al_y_value_slot(QPointF value)
{
    ui->t7_al_y_value->setText(QString("%1").arg(value.y()));
}

void Thin_film_mainwindows::t13_ti_y_value_slot(QPointF value)
{
    ui->t13_ti_y_value->setText(QString("%1").arg(value.y()));
}

void Thin_film_mainwindows::t13_al_y_value_slot(QPointF value)
{
  ui->t13_al_y_value->setText(QString("%1").arg(value.y()));
}
void Thin_film_mainwindows::t7_datamodel_dataChanged(QModelIndex index1, QModelIndex index2, QVector<int> vector)
{
    v1_data_model_change(index1,t7_data_model,"MD014");
    ui->t7_data_table->scrollToBottom();
}

void Thin_film_mainwindows::t13_datamodel_dataChanged(QModelIndex index1, QModelIndex index2, QVector<int> vector)
{
    v1_data_model_change(index1,t13_data_model,"MD042");
    ui->t13_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t7_LE_Lot_count_editingFinished()
{
//    if(ui->t7_LE_Lot_count->text()!= ""){
//        QSqlQuery query(my_mesdb);
//        query.exec(QString("update `Thin_film_spec_managerment` SET `Lot_max_count`='%1' Where `machine_code`='MD014'").arg(ui->t7_LE_Lot_count->text()));
//        while(ui->t7_input_lot_table->rowCount()>0){
//            ui->t7_input_lot_table->removeRow(0);
//        }
//        t7_lot_item_list.clear();
//        for(int i=0;i<ui->t7_LE_Lot_count->text().toInt();i++){

//            input_lot_item *lot_item = new input_lot_item(ms_mesdb,"MD009");
//            t7_lot_item_list.append(lot_item);
//            ui->t7_input_lot_table->insertRow(i);
//            ui->t7_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//    //        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
//            ui->t7_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
//            ui->t7_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
//            ui->t7_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
//            ui->t7_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
//            ui->t7_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
//            ui->t7_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
//            ui->t7_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//            connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                    t7_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
//        }
//    }
}
void Thin_film_mainwindows::on_t7_LE_Run_number_editingFinished()
{
//    if(ui->t7_LE_Run_number->text() != ""){
//        QSqlQuery query(my_mesdb);
//        query.exec(QString("update `Thin_film_spec_managerment` SET `Run_number_count`='%1' Where `machine_code`='MD014'").arg(ui->t7_LE_Run_number->text()));
//    }
}
void Thin_film_mainwindows::on_t7_Input_btn_clicked()
{
//      QString Run_number = ui->t7_LE_Run_number->text();
//      if(Run_number.toInt() > 60){
//          ui->t7_LE_Run_number->setText("1");
//          on_t7_LE_Run_number_editingFinished();
//          QMessageBox msg;
//          msg.addButton(QMessageBox::Ok);
//          msg.setText(tr("60 over set 1"));
//          msg.exec();
//      }
      QString Lot_count = ui->t7_LE_Lot_count->text();
      QString resister = t7_input_item->ui->LE_resister->text();
//      QString N2_blow = QString("%1").arg(QVariant(t7_input_item->ui->CB_N2_blow->isChecked()).toInt());
      QString au_f_add = t7_input_item->ui->LE_au_f_add->text();
      QString now_r_weight = t7_input_item->ui->LE_now_r_weight->text();
      QString au_after_r_weight = t7_input_item->ui->LE_au_after_r_weight->text();
      QString au_use =  t7_input_item->ui->LE_au_use->text();
      QString ti_add =  t7_input_item->ui->LE_ti_add->text();
      QString al_add =  t7_input_item->ui->LE_al_add->text();
      QString xtal_life =  t7_input_item->ui->LE_xtal_life->text();
      QString name =  t7_input_item->ui->LE_name->text();
//      QString d_use =  QString("%1").arg(QVariant(t7_input_item->ui->CB_d_run->isChecked()).toInt());
      QString X_tal_number = t7_input_item->ui->LE_Xtal_number->text();
      QString X_tal_Hz = t7_input_item->ui->LE_Xtal_hz->text();

      QString note =  t7_input_item->ui->LE_note->text();

      ui->t7_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
      ui->t7_search_time_start->setDateTime(ui->t7_search_time_end->dateTime().addDays(-2));

      QString Recipe_total_text;
      QString Recipe_total_name_text;
      QString Recipe_name1 = t7_input_item->ui->LE_Recipe_name1->text();
      QString Recipe_name2 = t7_input_item->ui->LE_Recipe_name2->text();
      QString Recipe_name3 = t7_input_item->ui->LE_Recipe_name3->text();
      QString Recipe_name4 = t7_input_item->ui->LE_Recipe_name4->text();
      QString Recipe_name5 = t7_input_item->ui->LE_Recipe_name5->text();
      QString CB_Recipe_name1 = t7_input_item->ui->CB_Recipe_name1->currentText();
      QString CB_Recipe_name2 = t7_input_item->ui->CB_Recipe_name2->currentText();
      QString CB_Recipe_name3 = t7_input_item->ui->CB_Recipe_name3->currentText();
      QString CB_Recipe_name4 = t7_input_item->ui->CB_Recipe_name4->currentText();
      QString CB_Recipe_name5 = t7_input_item->ui->CB_Recipe_name5->currentText();
      int Recipe_count = 0;
      int ti_count = 0;
      int au_count = 0;
      int al_count = 0;

      if(CB_Recipe_name1 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name1));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name1));
          if(CB_Recipe_name1=="Ti"){
              ti_count += Recipe_name1.toInt();
          }else if(CB_Recipe_name1=="Al"){
              al_count += Recipe_name1.toInt();
          }else if(CB_Recipe_name1=="Au"){
              au_count += Recipe_name1.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name2 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name2));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name2));
          if(CB_Recipe_name2=="Ti"){
              ti_count += Recipe_name2.toInt();
          }else if(CB_Recipe_name2=="Al"){
              al_count += Recipe_name2.toInt();
          }else if(CB_Recipe_name2=="Au"){
              au_count += Recipe_name2.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name3 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name3));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name3));
          if(CB_Recipe_name3=="Ti"){
              ti_count += Recipe_name3.toInt();
          }else if(CB_Recipe_name3=="Al"){
              al_count += Recipe_name3.toInt();
          }else if(CB_Recipe_name3=="Au"){
              au_count += Recipe_name3.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name4 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name4));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name4));
          if(CB_Recipe_name4=="Ti"){
              ti_count += Recipe_name4.toInt();
          }else if(CB_Recipe_name4=="Al"){
              al_count += Recipe_name4.toInt();
          }else if(CB_Recipe_name4=="Au"){
              au_count += Recipe_name4.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name5 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name5));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name5));
          if(CB_Recipe_name5=="Ti"){
              ti_count += Recipe_name5.toInt();
          }else if(CB_Recipe_name5=="Al"){
              al_count += Recipe_name5.toInt();
          }else if(CB_Recipe_name5=="Au"){
              au_count += Recipe_name5.toInt();
          }
          Recipe_count++;
      }
      if(Recipe_count>0){
          Recipe_total_text.remove(Recipe_total_text.length()-1,1);
          Recipe_total_name_text.remove(Recipe_total_name_text.length()-1,1);
      }
      QSqlQuery query(my_mesdb);
      QString batch_id;
      query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_code = 'MD014'"));
      if(query.next()){
          batch_id = query.value("current_batch_id").toString();
          query.exec("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+1  where machine_code = 'MD014'");
      }
      bool query_result = false;
      qDebug()<<ui->t7_input_lot_table->rowCount();
      query.exec("select now() as now_time");
      QDateTime now_time;

      now_time = QDateTime::currentDateTime();
      for(int i=0;i<ui->t7_input_lot_table->rowCount();i++){
          QString Lot_id = t7_lot_item_list.at(i)->ui->LE_Lot_id->text();
          QString wafer_count = t7_lot_item_list.at(i)->ui->LE_wafer_count->text();
          QString ashing1 = t7_lot_item_list.at(i)->ui->LE_ashing1->text();
          QString ashing2 = t7_lot_item_list.at(i)->ui->LE_ashing2->text();
          QString ashing3 = t7_lot_item_list.at(i)->ui->LE_ashing3->text();
          QString ashing_machine = t7_lot_item_list.at(i)->ui->LE_ashing_machine->text();
          QString machine_type = t7_lot_item_list.at(i)->ui->LE_machine_type->text();
          QString wafer_angle = t7_lot_item_list.at(i)->ui->LE_wafer_angle->text();

          if(Lot_id.trimmed() !=""){
              QString query_str = QString("INSERT INTO `Thin_film_data` "
                                          "(`Run_number`, `Resister`, `machine_type`, `Lot_id`, `Wafer_count`, "
                                          "`Wafer_angle`, `Input_thin_data`, `ashing1`, `ashing2`, "
                                          "`ashing3`, `ashing_machine`, `Pad_thin`, `Wlp_thin`, "
                                          "`N2_Blow`, `Au_f_add`, `Now_r_weight`, `Au_after_weight`, "
                                          "`Au_use`, `Recipe`, `Ti_add`, `Al_add`, `X_tal_number`,`X_tal_Hz`, "
                                          "`X_tal_life`, `User_name`, `D_use`, `note`, `Ti_total`, "
                                          "`Al_total`, `Au_total`, `batch_id`, `cleaning_cal_flag`,`machine_code`,`machine_name`,"
                                          "`Input_time`,`vacuum_time`,`day_and_night`,`au_pellet_output`,`au_add_weight`,`dom`) "
                                          "VALUES "
                                          "('%1', '%2', '%3', '%4', '%5', '%6', "
                                          "'%7', '%8', '%9', '%10', "
                                          "'%11', '%12', '%13', '%14', '%15', '%16', "
                                          "'%17', '%18', '%19', '%20', '%21', '%22','%23', "
                                          "'%24', '%25', '%26', '%27', '%28', "
                                          "'%29', '%30', '%31' ,'%32','%33','%34','%35','%36','%37','%38','%39','%40');").arg("").arg(resister).arg(machine_type).arg(Lot_id).arg(wafer_count)
                                                                        .arg(wafer_angle).arg(Recipe_total_text).arg(ashing1).arg(ashing2)
                                                                        .arg(ashing3).arg(ashing_machine).arg(QString("0")).arg(QString("0"))
                                                                        .arg("").arg(au_f_add).arg(now_r_weight).arg(au_after_r_weight)
                                                                        .arg(au_use).arg(Recipe_total_name_text).arg(ti_add).arg(al_add).arg(X_tal_number).arg(X_tal_Hz)
                                                                        .arg(xtal_life).arg(name).arg("").arg(note).arg(ti_count)
                                                                        .arg(al_count).arg(au_count).arg(batch_id).arg(QString("0")).arg("MD014").arg("hanil")
                                                                        .arg(now_time.toString("yyyy-MM-dd hh:mm:ss"))
                                                                        .arg(t7_input_item->ui->DT_vacuum_time->dateTime().toString("yyyy-MM-dd hh:mm:00"))
                                                                        .arg(t7_input_item->ui->CB_day_and_night->currentText())
                                                                        .arg(t7_input_item->ui->DT_vacuum_time->dateTime().addSecs(-300).toString("yyyy-MM-dd hh:mm:00"))
                                                                        .arg(t7_input_item->ui->LE_au_after_weight->text())
                                                                        .arg(t7_input_item->ui->CB_domnumber->currentText());
              query_result = query.exec(query_str);

              if(!query_result){
                  break;
              }

          }
      }
      au_calc(now_time);
      if(query_result){
          t7_input_item->clear_data();
          for(int i=0;i<ui->t7_input_lot_table->rowCount();i++){
              t7_lot_item_list.at(i)->clear_data();
          }
          QMessageBox msg;
          msg.addButton(QMessageBox::Ok);
          msg.setText(tr("input complete"));
          msg.exec();
//          int run_count = ui->t7_LE_Run_number->text().toInt();
//          run_count++;
//          ui->t7_LE_Run_number->setText(QString("%1").arg(run_count));
          on_t7_LE_Run_number_editingFinished();
          ui->t7_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
          ui->t7_search_time_start->setDateTime(ui->t7_search_time_end->dateTime().addDays(-2));
          t7_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
          on_t7_search_btn_clicked();
          ui->t7_data_table->scrollToBottom();
      }else {
          QMessageBox::warning(this, tr("conntion false"),
                                                         "server connection fail\n"
                                                            ""+my_mesdb.lastError().text(),
                                                              QMessageBox::Close);
      }
      query.exec(QString("select * from depostion_au_use_current_view where machine_name = '%1' AND batch_id = %2")
                 .arg("hanil").arg(batch_id));
      if(query.next()){
          double  OKNG = query.value("OKNG").toDouble();
          if(abs(OKNG)>0.1){
              QMessageBox msg;
              msg.addButton(QMessageBox::Ok);
              msg.setText(tr("au pellet NG"));
              msg.exec();
          }
      }
      query.exec("update `Thin_film_spec_managerment` set au_pullet_add = 0 ,now_r_weight = 0,au_after_r_weight =0,au_after_weight=0 where machine_code = 'MD014'");
      ti_al_chart_init(t7_ti_chart,t7_al_chart,t7_ti_value_series,t7_al_value_series,t7_ti_UCL_value_series,t7_al_UCL_value_series,
                       t7_al_axisX,t7_al_axisY,t7_ti_axisX,t7_ti_axisY,"MD014");
}
void Thin_film_mainwindows::on_t7_search_btn_clicked()
{
    t7_data_model->setFilter(QString("vacuum_time Between '%1' AND '%2' AND machine_code = 'MD014'")
                          .arg(ui->t7_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t7_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    t7_data_model->select();
    t7_data_model->setSort(2,Qt::AscendingOrder);
    QSqlQuery query(my_mesdb);
    query.exec(QString("select sum((case when (`A`.`machine_code` = 'MD014') then `A`.`Wafer_count` end)) AS `wafer` from `Thin_film_data` `A` "
               "where vacuum_time Between '%1' AND '%2' AND machine_code = 'MD014'").arg(ui->t7_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
               .arg(ui->t7_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    if(query.next()){

        ui->t7_total_wafer->setText(query.value("wafer").toString());
    }
    ui->t7_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t7_BT_search_lot_id_clicked()
{
    t7_data_model->setFilter(QString("Lot_id = '%1' AND machine_code = 'MD014'")
                          .arg(ui->t4_LE_search_lot_id->text()));
    t7_data_model->select();
    t7_data_model->setSort(1,Qt::AscendingOrder);
}
void Thin_film_mainwindows::on_t7_ti_al_refresh_clicked()
{
    ti_al_chart_init(t7_ti_chart,t7_al_chart,t7_ti_value_series,t7_al_value_series,t7_ti_UCL_value_series,t7_al_UCL_value_series,
                     t7_al_axisX,t7_al_axisY,t7_ti_axisX,t7_ti_axisY,"MD014");
}

void Thin_film_mainwindows::on_t13_LE_Lot_count_editingFinished()
{

}

void Thin_film_mainwindows::on_t13_LE_Run_number_editingFinished()
{
//    if(ui->t13_LE_Run_number->text() != ""){
//        QSqlQuery query(my_mesdb);
//        query.exec(QString("update `Thin_film_spec_managerment` SET `Run_number_count`='%1' Where `machine_code`='MD042'").arg(ui->t13_LE_Run_number->text()));
//    }
}

void Thin_film_mainwindows::on_t13_Input_btn_clicked()
{
//    QString Run_number = ui->t13_LE_Run_number->text();
//    if(Run_number.toInt() > 60){
//        ui->t13_LE_Run_number->setText("1");
//        on_t13_LE_Run_number_editingFinished();
//        QMessageBox msg;
//        msg.addButton(QMessageBox::Ok);
//        msg.setText(tr("60 over set 1"));
//        msg.exec();
//    }
    QString Lot_count = ui->t13_LE_Lot_count->text();
    QString resister = t13_input_item->ui->LE_resister->text();
//    QString N2_blow = QString("%1").arg(QVariant(t13_input_item->ui->CB_N2_blow->isChecked()).toInt());
    QString au_f_add = t13_input_item->ui->LE_au_f_add->text();
    QString now_r_weight = t13_input_item->ui->LE_now_r_weight->text();
    QString au_after_r_weight = t13_input_item->ui->LE_au_after_r_weight->text();
    QString au_use =  t13_input_item->ui->LE_au_use->text();
    QString ti_add =  t13_input_item->ui->LE_ti_add->text();
    QString al_add =  t13_input_item->ui->LE_al_add->text();
    QString xtal_life =  t13_input_item->ui->LE_xtal_life->text();
    QString name =  t13_input_item->ui->LE_name->text();
//    QString d_use =  QString("%1").arg(QVariant(t13_input_item->ui->CB_d_run->isChecked()).toInt());

    QString note =  t13_input_item->ui->LE_note->text();

    ui->t13_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t13_search_time_start->setDateTime(ui->t13_search_time_end->dateTime().addDays(-2));

    QString Recipe_total_text;
    QString Recipe_total_name_text;
    QString Recipe_name1 = t13_input_item->ui->LE_Recipe_name1->text();
    QString Recipe_name2 = t13_input_item->ui->LE_Recipe_name2->text();
    QString Recipe_name3 = t13_input_item->ui->LE_Recipe_name3->text();
    QString Recipe_name4 = t13_input_item->ui->LE_Recipe_name4->text();
    QString Recipe_name5 = t13_input_item->ui->LE_Recipe_name5->text();
    QString CB_Recipe_name1 = t13_input_item->ui->CB_Recipe_name1->currentText();
    QString CB_Recipe_name2 = t13_input_item->ui->CB_Recipe_name2->currentText();
    QString CB_Recipe_name3 = t13_input_item->ui->CB_Recipe_name3->currentText();
    QString CB_Recipe_name4 = t13_input_item->ui->CB_Recipe_name4->currentText();
    QString CB_Recipe_name5 = t13_input_item->ui->CB_Recipe_name5->currentText();
    int Recipe_count = 0;
    int ti_count = 0;
    int au_count = 0;
    int al_count = 0;

    if(CB_Recipe_name1 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name1));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name1));
        if(CB_Recipe_name1=="Ti"){
            ti_count += Recipe_name1.toInt();
        }else if(CB_Recipe_name1=="Al"){
            al_count += Recipe_name1.toInt();
        }else if(CB_Recipe_name1=="Au"){
            au_count += Recipe_name1.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name2 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name2));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name2));
        if(CB_Recipe_name2=="Ti"){
            ti_count += Recipe_name2.toInt();
        }else if(CB_Recipe_name2=="Al"){
            al_count += Recipe_name2.toInt();
        }else if(CB_Recipe_name2=="Au"){
            au_count += Recipe_name2.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name3 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name3));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name3));
        if(CB_Recipe_name3=="Ti"){
            ti_count += Recipe_name3.toInt();
        }else if(CB_Recipe_name3=="Al"){
            al_count += Recipe_name3.toInt();
        }else if(CB_Recipe_name3=="Au"){
            au_count += Recipe_name3.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name4 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name4));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name4));
        if(CB_Recipe_name4=="Ti"){
            ti_count += Recipe_name4.toInt();
        }else if(CB_Recipe_name4=="Al"){
            al_count += Recipe_name4.toInt();
        }else if(CB_Recipe_name4=="Au"){
            au_count += Recipe_name4.toInt();
        }
        Recipe_count++;
    }
    if(CB_Recipe_name5 != ""){
        Recipe_total_text.append(QString("%1/").arg(Recipe_name5));
        Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name5));
        if(CB_Recipe_name5=="Ti"){
            ti_count += Recipe_name5.toInt();
        }else if(CB_Recipe_name5=="Al"){
            al_count += Recipe_name5.toInt();
        }else if(CB_Recipe_name5=="Au"){
            au_count += Recipe_name5.toInt();
        }
        Recipe_count++;
    }
    if(Recipe_count>0){
        Recipe_total_text.remove(Recipe_total_text.length()-1,1);
        Recipe_total_name_text.remove(Recipe_total_name_text.length()-1,1);
    }
    QSqlQuery query(my_mesdb);
    QString batch_id;
    query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_code = 'MD042'"));
    if(query.next()){
        batch_id = query.value("current_batch_id").toString();
        query.exec("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+1  where machine_code = 'MD042'");
    }
    bool query_result = false;
    qDebug()<<ui->t13_input_lot_table->rowCount();

    query.exec("select now() as now_time");
    QDateTime now_time;

    now_time = QDateTime::currentDateTime();
    for(int i=0;i<ui->t13_input_lot_table->rowCount();i++){
        QString Lot_id = t13_lot_item_list.at(i)->ui->LE_Lot_id->text();
        QString wafer_count = t13_lot_item_list.at(i)->ui->LE_wafer_count->text();
        QString ashing1 = t13_lot_item_list.at(i)->ui->LE_ashing1->text();
        QString ashing2 = t13_lot_item_list.at(i)->ui->LE_ashing2->text();
        QString ashing3 = t13_lot_item_list.at(i)->ui->LE_ashing3->text();
        QString ashing_machine = t13_lot_item_list.at(i)->ui->LE_ashing_machine->text();
        QString machine_type = t13_lot_item_list.at(i)->ui->LE_machine_type->text();
        QString wafer_angle = t13_lot_item_list.at(i)->ui->LE_wafer_angle->text();
        if(Lot_id.trimmed() !=""){
            QString query_str = QString("INSERT INTO `Thin_film_data` "
                                        "(`Run_number`, `Resister`, `machine_type`, `Lot_id`, `Wafer_count`, "
                                        "`Wafer_angle`, `Input_thin_data`, `ashing1`, `ashing2`, "
                                        "`ashing3`, `ashing_machine`, `Pad_thin`, `Wlp_thin`, "
                                        "`N2_Blow`, `Au_f_add`, `Now_r_weight`, `Au_after_weight`, "
                                        "`Au_use`, `Recipe`, `Ti_add`, `Al_add`, `X_tal_number`, "
                                        "`X_tal_life`, `User_name`, `D_use`, `note`, `Ti_total`, "
                                        "`Al_total`, `Au_total`, `batch_id`, `cleaning_cal_flag`,`machine_code`,`machine_name`,"
                                        "`Input_time`,`vacuum_time`,`day_and_night`,`au_pellet_output`,`au_add_weight`,`dom`) "
                                        "VALUES "
                                        "('%1', '%2', '%3', '%4', '%5', '%6', "
                                        "'%7', '%8', '%9', '%10', "
                                        "'%11', '%12', '%13', '%14', '%15', '%16', "
                                        "'%17', '%18', '%19', '%20', '%21', '%22', "
                                        "'%23', '%24', '%25', '%26', '%27', "
                                        "'%28', '%29', '%30' ,'%31','%32','%33','%34','%35','%36','%37','%38','%39' );").arg("").arg(resister).arg(machine_type).arg(Lot_id).arg(wafer_count)
                                                                      .arg(wafer_angle).arg(Recipe_total_text).arg(ashing1).arg(ashing2)
                                                                      .arg(ashing3).arg(ashing_machine).arg(QString("0")).arg(QString("0"))
                                                                      .arg("").arg(au_f_add).arg(now_r_weight).arg(au_after_r_weight)
                                                                      .arg(au_use).arg(Recipe_total_name_text).arg(ti_add).arg(al_add).arg(QString("0"))
                                                                      .arg(xtal_life).arg(name).arg("").arg(note).arg(ti_count)
                                                                      .arg(al_count).arg(au_count).arg(batch_id).arg(QString("0")).arg("MD042").arg("SHINCRON ACE#1")
                                                                      .arg(now_time.toString("yyyy-MM-dd hh:mm:ss"))
                                                                      .arg(t13_input_item->ui->DT_vacuum_time->dateTime().toString("yyyy-MM-dd hh:mm:00"))
                                                                      .arg(t13_input_item->ui->CB_day_and_night->currentText())
                                                                      .arg(t13_input_item->ui->DT_vacuum_time->dateTime().addSecs(-300).toString("yyyy-MM-dd hh:mm:00"))
                                                                      .arg(t13_input_item->ui->LE_au_after_weight->text())
                                                                      .arg(t13_input_item->ui->CB_domnumber->currentText());
            query_result = query.exec(query_str);
            if(!query_result){
                break;
            }

        }
    }
    au_calc(now_time);
    if(query_result){
        t13_input_item->clear_data();
        for(int i=0;i<ui->t13_input_lot_table->rowCount();i++){
            t13_lot_item_list.at(i)->clear_data();
        }
        QMessageBox msg;
        msg.addButton(QMessageBox::Ok);
        msg.setText(tr("input complete"));
        msg.exec();
//        int run_count = ui->t13_LE_Run_number->text().toInt();
//        run_count++;
//        ui->t13_LE_Run_number->setText(QString("%1").arg(run_count));
        on_t13_LE_Run_number_editingFinished();
        ui->t13_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
        ui->t13_search_time_start->setDateTime(ui->t13_search_time_end->dateTime().addDays(-2));
        t13_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
        on_t13_search_btn_clicked();
    }else {
        QMessageBox::warning(this, tr("conntion false"),
                                                       "server connection fail\n"
                                                          ""+my_mesdb.lastError().text(),
                                                            QMessageBox::Close);
    }
    query.exec(QString("select * from depostion_au_use_current_view where machine_name = '%1' AND batch_id = %2")
               .arg("SHINCRON ACE#1").arg(batch_id));
    if(query.next()){
        double  OKNG = query.value("OKNG").toDouble();
        if(abs(OKNG)>0.1){
            QMessageBox msg;
            msg.addButton(QMessageBox::Ok);
            msg.setText(tr("au pellet NG"));
            msg.exec();
        }
    }
    query.exec("update `Thin_film_spec_managerment` set au_pullet_add = 0 ,now_r_weight = 0,au_after_r_weight =0,au_after_weight=0 where machine_code = 'MD042'");
    ti_al_chart_init(t13_ti_chart,t13_al_chart,t13_ti_value_series,t13_al_value_series,t13_ti_UCL_value_series,t13_al_UCL_value_series,
                     t13_al_axisX,t13_al_axisY,t13_ti_axisX,t13_ti_axisY,"MD042");
}

void Thin_film_mainwindows::on_t13_search_btn_clicked()
{
    t13_data_model->setFilter(QString("vacuum_time Between '%1' AND '%2' AND machine_code = 'MD042'")
                          .arg(ui->t13_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t13_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    t13_data_model->select();
    t13_data_model->setSort(2,Qt::AscendingOrder);
    QSqlQuery query(my_mesdb);
    query.exec(QString("select sum((case when (`A`.`machine_code` = 'MD042') then `A`.`Wafer_count` end)) AS `wafer` from `Thin_film_data` `A` "
               "where vacuum_time Between '%1' AND '%2' AND machine_code = 'MD042'").arg(ui->t13_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
               .arg(ui->t13_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    if(query.next()){

        ui->t13_total_wafer->setText(query.value("wafer").toString());
    }

    ui->t13_data_table->scrollToBottom();
}

void Thin_film_mainwindows::on_t13_BT_search_lot_id_clicked()
{
    t13_data_model->setFilter(QString("Lot_id = '%1' AND machine_code = 'MD042'")
                          .arg(ui->t13_LE_search_lot_id->text()));
    t13_data_model->select();
    t13_data_model->setSort(1,Qt::AscendingOrder);
}

void Thin_film_mainwindows::on_t13_ti_al_refresh_clicked()
{
    ti_al_chart_init(t13_ti_chart,t13_al_chart,t13_ti_value_series,t13_al_value_series,t13_ti_UCL_value_series,t13_al_UCL_value_series,
                     t13_al_axisX,t13_al_axisY,t13_ti_axisX,t13_ti_axisY,"MD042");
}

void Thin_film_mainwindows::on_t14_ti_al_refresh_clicked()
{
    ti_al_chart_init(t14_ti_chart,t14_al_chart,t14_ti_value_series,t14_al_value_series,t14_ti_UCL_value_series,t14_al_UCL_value_series,
                     t14_al_axisX,t14_al_axisY,t14_ti_axisX,t14_ti_axisY,"MD045");
}
void Thin_film_mainwindows::on_t15_ti_al_refresh_clicked()
{
    ti_al_chart_init(t15_ti_chart,t15_al_chart,t15_ti_value_series,t15_al_value_series,t15_ti_UCL_value_series,t15_al_UCL_value_series,
                     t15_al_axisX,t15_al_axisY,t15_ti_axisX,t15_ti_axisY,"MD049");
}
void Thin_film_mainwindows::on_t16_ti_al_refresh_clicked()
{
    ti_al_chart_init(t16_ti_chart,t16_al_chart,t16_ti_value_series,t16_al_value_series,t16_ti_UCL_value_series,t16_al_UCL_value_series,
                     t16_al_axisX,t16_al_axisY,t16_ti_axisX,t16_ti_axisY,"MD048");
}


void Thin_film_mainwindows::on_t13_DT_del_btn_clicked()
{
    int count = ui->t13_data_table->selectionModel()->selectedIndexes().count();
    QVector<int> index_history;
    for(int i=0;i<count;i++){
        if(!index_history.contains(ui->t13_data_table->selectionModel()->selectedIndexes().at(i).row())){
        QDateTime Input_time2 = t13_data_model->record(ui->t13_data_table->selectionModel()->selectedIndexes().at(i).row()).value("Input_time").toDateTime();
        t13_data_model->removeRow(ui->t13_data_table->selectionModel()->selectedIndexes().at(i).row());
         au_calc(Input_time2);
         index_history.append(ui->t13_data_table->selectionModel()->selectedIndexes().at(i).row());
        }else {

        }
    }
    on_t13_search_btn_clicked();
    ui->t13_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t7_DT_del_btn_clicked()
{
    int count = ui->t7_data_table->selectionModel()->selectedIndexes().count();
    QVector<int> index_history;
    for(int i=0;i<count;i++){
        if(!index_history.contains(ui->t7_data_table->selectionModel()->selectedIndexes().at(i).row())){
        QDateTime Input_time2 = t7_data_model->record(ui->t7_data_table->selectionModel()->selectedIndexes().at(i).row()).value("Input_time").toDateTime();
        t7_data_model->removeRow(ui->t7_data_table->selectionModel()->selectedIndexes().at(i).row());
         au_calc(Input_time2);
         index_history.append(ui->t7_data_table->selectionModel()->selectedIndexes().at(i).row());
        }else {

        }
    }
    on_t7_search_btn_clicked();
    ui->t7_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t7_move_machine_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    QString batch_id_temp = "0";
    QString machine_name;
    QString machine_code;

    int count  = ui->t7_data_table->selectionModel()->selectedIndexes().count();
    for(int i=0;i<count;i++){
         QSqlRecord recode = t7_data_model->record(ui->t7_data_table->selectionModel()->selectedIndexes().at(i).row());
        if(batch_id_temp != recode.value("batch_id").toString()){
            query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_name = '%1'").arg(ui->t7_CB_move_machine->currentText()));
            if(query.next()){
                batch_id_temp = query.value("current_batch_id").toString()+5;
                machine_name = query.value("machine_name").toString();
                machine_code = query.value("machine_code").toString();
                query.exec(QString("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+10  where machine_name = '%1'").arg(ui->t7_CB_move_machine->currentText()));
            }
            batch_id_temp = recode.value("batch_id").toString();
        }

        recode.setGenerated("batch_id",true);
        recode.setGenerated("machine_name",true);
        recode.setGenerated("machine_code",true);
        recode.setValue("batch_id",batch_id_temp);
        recode.setValue("machine_name",machine_name);
        recode.setValue("machine_code",machine_code);
        t7_data_model->setRecord(ui->t7_data_table->selectionModel()->selectedIndexes().at(i).row(),recode);
    }
    on_t7_search_btn_clicked();
    ui->t7_data_table->scrollToBottom();
}

void Thin_film_mainwindows::on_t13_move_machine_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    QString batch_id_temp = "0";
    QString machine_name;
    QString machine_code;

    int count  = ui->t13_data_table->selectionModel()->selectedIndexes().count();
    for(int i=0;i<count;i++){
         QSqlRecord recode = t13_data_model->record(ui->t13_data_table->selectionModel()->selectedIndexes().at(i).row());
        if(batch_id_temp != recode.value("batch_id").toString()){
            query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_name = '%1'").arg(ui->t13_CB_move_machine->currentText()));
            if(query.next()){
                batch_id_temp = query.value("current_batch_id").toString()+5;
                machine_name = query.value("machine_name").toString();
                machine_code = query.value("machine_code").toString();
                query.exec(QString("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+10  where machine_name = '%1'").arg(ui->t13_CB_move_machine->currentText()));
            }
            batch_id_temp = recode.value("batch_id").toString();
        }

        recode.setGenerated("batch_id",true);
        recode.setGenerated("machine_name",true);
        recode.setGenerated("machine_code",true);
        recode.setValue("batch_id",batch_id_temp);
        recode.setValue("machine_name",machine_name);
        recode.setValue("machine_code",machine_code);
        t13_data_model->setRecord(ui->t13_data_table->selectionModel()->selectedIndexes().at(i).row(),recode);
    }
    on_t13_search_btn_clicked();
    ui->t13_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t7_excel_header_copy_flag_toggled(bool checked)
{
    ui->t7_data_table->copyheader_flag = checked;
}

void Thin_film_mainwindows::on_t13_excel_header_copy_flag_toggled(bool checked)
{
    ui->t13_data_table->copyheader_flag = checked;
}
void Thin_film_mainwindows::on_t7_machine_temp_run_mode_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    query.exec("update Thin_film_spec_managerment set nomal_mode_time = ADDTIME(NOW(),'0:5:0') where `machine_code`='MD014'");
}

void Thin_film_mainwindows::on_t13_machine_temp_run_mode_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    query.exec("update Thin_film_spec_managerment set nomal_mode_time = ADDTIME(NOW(),'0:5:0') where `machine_code`='MD042'");
}


//t7_function -- end --

//t14_function -- start --
void Thin_film_mainwindows::t14_init()
{
    ui->t14_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t14_search_time_start->setDateTime(ui->t14_search_time_end->dateTime().addDays(-2));

    QSqlTableModel *spec_model = new QSqlTableModel(this,my_mesdb);
    spec_model->setTable("Thin_film_spec_managerment");
    spec_model->setFilter("machine_code = 'MD045'");
    spec_model->select();
    spec_model->setHeaderData(4,Qt::Horizontal,tr("ashing_LCL"));
    spec_model->setHeaderData(5,Qt::Horizontal,tr("ashing_UCL"));
    spec_model->setHeaderData(6,Qt::Horizontal,tr("WLPTiAlashing_LCL"));
    spec_model->setHeaderData(7,Qt::Horizontal,tr("WLPTiAlashing_UCL"));
    spec_model->setHeaderData(8,Qt::Horizontal,tr("WLP_LCL_Ti_Au"));
    spec_model->setHeaderData(9,Qt::Horizontal,tr("WLP_UCL_Ti_Au"));
    spec_model->setHeaderData(10,Qt::Horizontal,tr("WLP_LCL_Ti_Al"));
    spec_model->setHeaderData(11,Qt::Horizontal,tr("WLP_UCL_Ti_Al"));
    spec_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    t14_data_model = new thin_sqltable_model(this,my_mesdb,"MD045",spec_model);

    t14_data_model->setTable("Thin_film_data");

    t14_data_model->setFilter(QString("Input_time Between '%1' AND '%2' AND machine_code = 'MD045'")
                          .arg(ui->t14_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t14_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    ui->t14_data_table->setItemDelegateForColumn(t14_data_model->fieldIndex("vacuum_time"),new DateDelegate(ui->t14_data_table));
    ui->t14_data_table->setItemDelegateForColumn(t14_data_model->fieldIndex("Input_time"),new DateDelegate(ui->t14_data_table));
    ui->t14_data_table->setModel(t14_data_model);
    ui->t14_data_table->setSortingEnabled(true);
    connect(t14_data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>))
            ,this,SLOT(t14_datamodel_dataChanged(QModelIndex,QModelIndex,QVector<int>)));
    ui->t14_data_table->setItemDelegateForColumn(10,new Thin_combodelegate(note_combo_list,ui->t14_data_table));

    t14_data_model->setEditStrategy(QSqlTableModel::OnFieldChange);
    t14_data_model->setSort(2,Qt::AscendingOrder);

    t14_data_model->select();

    t14_data_model->setHeaderData(t14_data_model->fieldIndex("day_and_night"),Qt::Horizontal,tr("day_and_night"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("vacuum_time"),Qt::Horizontal,tr("vacuum_time"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Run_number"),Qt::Horizontal,tr("thin_Run_number"));
//    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Resister"),Qt::Horizontal,tr("thin_Resister"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("machine_type"),Qt::Horizontal,tr("thin_machine_type"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Lot_id"),Qt::Horizontal,tr("thin_Lot_id"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Wafer_count"),Qt::Horizontal,tr("thin_Wafer_count"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Pad_thin"),Qt::Horizontal,tr("thin_Pad_thin"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Wlp_thin"),Qt::Horizontal,tr("thin_Wlp_thin"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Wafer_angle"),Qt::Horizontal,tr("thin_Wafer_angle"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Input_thin_data"),Qt::Horizontal,tr("thin_Input_thin_data"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Recipe"),Qt::Horizontal,tr("thin_Recipe"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("ashing1"),Qt::Horizontal,tr("thin_ashing1"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("ashing2"),Qt::Horizontal,tr("thin_ashing2"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("ashing3"),Qt::Horizontal,tr("thin_ashing3"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("ashing_machine"),Qt::Horizontal,tr("thin_ashing_machine"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("N2_Blow"),Qt::Horizontal,tr("thin_N2_Blow"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Au_f_add"),Qt::Horizontal,tr("thin_thin_Au_f_add"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Now_r_weight"),Qt::Horizontal,tr("thin_Now_r_weight"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Au_after_weight"),Qt::Horizontal,tr("thin_Au_after_weight"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Au_use"),Qt::Horizontal,tr("thin_Au_use"));
//    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Ti_add"),Qt::Horizontal,tr("thin_Ti_add"));
//    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Al_add"),Qt::Horizontal,tr("thin_Al_add"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("X_tal_number"),Qt::Horizontal,tr("thin_X_tal_number"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("X_tal_Hz"),Qt::Horizontal,tr("thin_X_tal_Hz"));
//    t14_data_model->setHeaderData(t14_data_model->fieldIndex("X_tal_life"),Qt::Horizontal,tr("thin_X_tal_life"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("User_name"),Qt::Horizontal,tr("thin_User_name"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("D_use"),Qt::Horizontal,tr("thin_D_use"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("note"),Qt::Horizontal,tr("thin_note"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Ti_total"),Qt::Horizontal,tr("thin_Ti_total"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Al_total"),Qt::Horizontal,tr("thin_Al_total"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Au_total"),Qt::Horizontal,tr("thin_Au_total"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("Input_time"),Qt::Horizontal,tr("thin_Input_time"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("machine_name"),Qt::Horizontal,tr("thin_machine_name"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("machine_code"),Qt::Horizontal,tr("thin_machine_code"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("batch_id"),Qt::Horizontal,tr("thin_batch_id"));
    t14_data_model->setHeaderData(t14_data_model->fieldIndex("cleaning_cal_flag"),Qt::Horizontal,tr("thin_cleaning_cal_flag"));
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("day_and_night"),50);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("vacuum_time"),110);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Run_number"),60);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Resister"),50);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("machine_type"),100);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Lot_id"),80);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Wafer_count"),50);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Pad_thin"),80);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Wlp_thin"),80);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Wafer_angle"),80);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Input_thin_data"),150);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Recipe"),150);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("ashing1"),65);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("ashing2"),65);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("ashing3"),65);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("ashing_machine"),80);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("N2_Blow"),80);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Au_f_add"),80);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Now_r_weight"),120);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Au_after_weight"),140);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Au_use"),50);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Ti_add"),50);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Al_add"),50);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("X_tal_number"),50);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("X_tal_Hz"),50);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("X_tal_life"),80);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("User_name"),80);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("D_use"),80);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("note"),140);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Ti_total"),60);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Al_total"),60);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Au_total"),60);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("Input_time"),120);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("machine_name"),50);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("machine_code"),50);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("batch_id"),50);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("cleaning_cal_flag"),50);
    ui->t14_data_table->horizontalHeader()->resizeSection(t14_data_model->fieldIndex("dom"),50);
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("X_tal_life"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("machine_code"));
//    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("batch_id"));
//    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("cleaning_cal_flag"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("Au_use"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("au_pellet_output"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("au_add"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("check_user"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("au_pellet_total"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("work_start_au"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("au_add_weight"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("au_case_weight"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("check_NG"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("Run_number"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("Pad_thin"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("Wlp_thin"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("N2_Blow"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("D_use"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("Ti_add"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("Al_add"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("X_tal_life"));
    ui->t14_data_table->horizontalHeader()->hideSection(t14_data_model->fieldIndex("Resister"));

    ui->t14_data_table->scrollToBottom();


    ui->t14_spec_table->setModel(spec_model);
    ui->t14_spec_table->horizontalHeader()->hideSection(0);
    ui->t14_spec_table->horizontalHeader()->hideSection(1);
    ui->t14_spec_table->horizontalHeader()->hideSection(2);
    ui->t14_spec_table->horizontalHeader()->hideSection(3);
    ui->t14_spec_table->horizontalHeader()->hideSection(4);
    ui->t14_spec_table->horizontalHeader()->hideSection(5);
    ui->t14_spec_table->horizontalHeader()->hideSection(6);
    ui->t14_spec_table->horizontalHeader()->hideSection(7);
    ui->t14_spec_table->horizontalHeader()->hideSection(8);
    ui->t14_spec_table->horizontalHeader()->hideSection(9);
    ui->t14_spec_table->horizontalHeader()->hideSection(10);
    ui->t14_spec_table->horizontalHeader()->hideSection(11);
    ui->t14_spec_table->horizontalHeader()->hideSection(12);
    ui->t14_spec_table->horizontalHeader()->hideSection(13);
    ui->t14_spec_table->horizontalHeader()->hideSection(14);
    ui->t14_spec_table->horizontalHeader()->hideSection(17);
    ui->t14_spec_table->horizontalHeader()->hideSection(18);
    ui->t14_spec_table->horizontalHeader()->hideSection(19);
    ui->t14_spec_table->horizontalHeader()->hideSection(20);
    ui->t14_spec_table->horizontalHeader()->hideSection(21);
    ui->t14_spec_table->horizontalHeader()->hideSection(22);
    ui->t14_spec_table->horizontalHeader()->hideSection(23);
    ui->t14_spec_table->horizontalHeader()->hideSection(24);
    ui->t14_spec_table->horizontalHeader()->hideSection(25);
    ui->t14_spec_table->horizontalHeader()->hideSection(26);
    ui->t14_spec_table->horizontalHeader()->hideSection(27);
    ui->t14_spec_table->horizontalHeader()->hideSection(28);
    ui->t14_spec_table->horizontalHeader()->hideSection(29);
    ui->t14_spec_table->horizontalHeader()->hideSection(30);
    ui->t14_spec_table->horizontalHeader()->hideSection(31);
    ui->t14_spec_table->horizontalHeader()->hideSection(32);
    ui->t14_spec_table->horizontalHeader()->hideSection(33);
    ui->t14_spec_table->horizontalHeader()->hideSection(34);
    ui->t14_spec_table->horizontalHeader()->hideSection(35);
    ui->t14_spec_table->horizontalHeader()->resizeSection(6,150);
    ui->t14_spec_table->horizontalHeader()->resizeSection(7,150);

    for(int i=0;i<8;i++){
        ui->t14_input_table->insertColumn(i);
    }
    ui->t14_input_table->insertRow(0);
    ui->t14_input_table->insertRow(1);
    ui->t14_input_table->insertRow(2);
    ui->t14_input_table->insertRow(3);
    ui->t14_input_table->insertRow(4);
    ui->t14_input_table->insertRow(5);
    ui->t14_input_table->insertRow(6);
    ui->t14_input_table->horizontalHeader()->resizeSection(0,70);
    ui->t14_input_table->horizontalHeader()->resizeSection(1,150);
    ui->t14_input_table->horizontalHeader()->resizeSection(3,70);
    ui->t14_input_table->horizontalHeader()->resizeSection(5,130);
    ui->t14_input_table->horizontalHeader()->resizeSection(6,140);
    ui->t14_input_table->horizontalHeader()->resizeSection(7,80);
    t14_input_item = new input_item(my_mesdb,"MD045");
    ui->t14_input_table->setCellWidget(0,0,t14_input_item->ui->LA_day_and_night);
    ui->t14_input_table->setCellWidget(1,0,t14_input_item->ui->CB_day_and_night);
    ui->t14_input_table->setCellWidget(0,1,t14_input_item->ui->LA_vacuum_time);
    ui->t14_input_table->setCellWidget(1,1,t14_input_item->ui->DT_vacuum_time);
    t14_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
//    ui->t14_input_table->setCellWidget(0,2,t14_input_item->ui->LA_resister);
//    ui->t14_input_table->setCellWidget(1,2,t14_input_item->ui->LE_resister);
//    ui->t14_input_table->setCellWidget(0,3,t14_input_item->ui->LA_N2_blow);
//    ui->t14_input_table->setCellWidget(1,3,t14_input_item->ui->WCB_N2_blow);
    ui->t14_input_table->setCellWidget(0,4,t14_input_item->ui->LA_au_f_add);
    ui->t14_input_table->setCellWidget(1,4,t14_input_item->ui->LE_au_f_add);
    ui->t14_input_table->setCellWidget(0,5,t14_input_item->ui->LA_now_r_weight);
    ui->t14_input_table->setCellWidget(1,5,t14_input_item->ui->LE_now_r_weight);
    ui->t14_input_table->setCellWidget(0,6,t14_input_item->ui->LA_au_after_r_weight);
    ui->t14_input_table->setCellWidget(1,6,t14_input_item->ui->LE_au_after_r_weight);
    ui->t14_input_table->setCellWidget(0,7,t14_input_item->ui->LA_au_after_weight);
    ui->t14_input_table->setCellWidget(1,7,t14_input_item->ui->LE_au_after_weight);
//    ui->t14_input_table->setCellWidget(0,7,t14_input_item->ui->LA_au_use);
//    ui->t14_input_table->setCellWidget(1,7,t14_input_item->ui->LE_au_use);
//    ui->t14_input_table->setCellWidget(2,0,t14_input_item->ui->LA_ti_add);
//    ui->t14_input_table->setCellWidget(3,0,t14_input_item->ui->LE_ti_add);
//    ui->t14_input_table->setCellWidget(2,1,t14_input_item->ui->LA_al_add);
//    ui->t14_input_table->setCellWidget(3,1,t14_input_item->ui->LE_al_add);
//    ui->t14_input_table->setCellWidget(2,2,t14_input_item->ui->LA_xtal_life);
//    ui->t14_input_table->setCellWidget(3,2,t14_input_item->ui->LE_xtal_life);
    ui->t14_input_table->setCellWidget(2,2,t14_input_item->ui->W_X_TAL);
    ui->t14_input_table->setCellWidget(2,3,t14_input_item->ui->LA_name);
    ui->t14_input_table->setCellWidget(3,3,t14_input_item->ui->LE_name);
//    ui->t14_input_table->setCellWidget(2,4,t14_input_item->ui->LA_d_run);
//    ui->t14_input_table->setCellWidget(3,4,t14_input_item->ui->WCB_d_run);
    ui->t14_input_table->setCellWidget(2,5,t14_input_item->ui->LA_note);
    ui->t14_input_table->setCellWidget(3,5,t14_input_item->ui->LE_note);
    ui->t14_input_table->setSpan(2,2,2,1);
    ui->t14_input_table->setSpan(2,5,1,5);
    ui->t14_input_table->setSpan(3,5,1,5);
    ui->t14_input_table->setCellWidget(4,0,t14_input_item->ui->LE_recipe_number);
    ui->t14_input_table->setCellWidget(4,1,t14_input_item->ui->CB_recipe_choice);
//    ui->t14_input_table->setCellWidget(4,2,t14_input_item->ui->LA_recipe_2);
    ui->t14_input_table->setCellWidget(5,0,t14_input_item->ui->W_recipe);
    ui->t14_input_table->setSpan(4,1,1,5);
//    ui->t14_input_table->setSpan(4,2,1,3);
    ui->t14_input_table->setSpan(5,0,2,5);
    ui->t14_input_table->setCellWidget(4,6,t14_input_item->ui->LA_dom_number);
    ui->t14_input_table->setCellWidget(5,6,t14_input_item->ui->CB_domnumber);

    ui->t14_LE_Lot_count->setValidator(new QIntValidator(0, 1000, this));
//    ui->t14_LE_Run_number->setValidator(new QIntValidator(0, 1000, this));
    QSqlQuery query(my_mesdb);
    query.exec(QString("select `Lot_max_count`,`Run_number_count` from Thin_film_spec_managerment where machine_code = 'MD045'"));
    if(query.next()){
        ui->t14_LE_Lot_count->setText(query.value("Lot_max_count").toString());
//        ui->t14_LE_Run_number->setText(query.value("Run_number_count").toString());
    }
    t14_lot_item_list.clear();
    for(int i=0;i<ui->t14_LE_Lot_count->text().toInt();i++){

        input_lot_item *lot_item = new input_lot_item(ms_mesdb,t14_input_item,"MD045");
        t14_lot_item_list.append(lot_item);
        ui->t14_input_lot_table->insertRow(i);
        ui->t14_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
        ui->t14_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
        ui->t14_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
        ui->t14_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
        ui->t14_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
        ui->t14_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
        ui->t14_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
        ui->t14_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//        connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                t14_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
    }
    query.exec("SELECT * FROM `Thin_film_repcipe` where machine_code = 'MD045' ORDER BY `recipe_number` ASC  ");
    t14_input_item->ui->CB_recipe_choice->addItem(tr("recipe_choice"));
    while(query.next()){
        QString recipe_name = query.value("recipe_number").toString()+"."+ query.value("recipename").toString()
                                + "|"+query.value("metal_name").toString()+"|"+query.value("metal_thin").toString()+"|"
                                +query.value("type").toString();
        QPixmap pixmap(100,100);
        pixmap.fill(QColor(query.value("color").toString()));
        t14_input_item->ui->CB_recipe_choice->addItem(QIcon(pixmap),recipe_name);
    }
    connect(t14_input_item->ui->CB_recipe_choice,SIGNAL(currentIndexChanged(int)),this,SLOT(t14_recipe_choice(int)));

    t14_ti_chart = new Thin_film_chart();
    t14_ti_chartview = new Thin_film_chart_view(t14_ti_chart);
    connect(t14_ti_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t14_ti_y_value_slot(QPointF)));
    ui->t14_Ti_layout->addWidget(t14_ti_chartview);

    t14_al_chart = new Thin_film_chart();
    t14_al_chartview = new Thin_film_chart_view(t14_al_chart);
    connect(t14_al_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t14_al_y_value_slot(QPointF)));
    ui->t14_Al_layout->addWidget(t14_al_chartview);
    t14_ti_axisX = new QDateTimeAxis();
    t14_ti_axisX->setFormat("MM-dd HH:mm:ss");
    t14_ti_axisX->setTitleText("Date");
    t14_ti_axisX->setVisible(true);
    t14_ti_axisX->setTitleVisible(true);
    t14_ti_chart->addAxis(t14_ti_axisX, Qt::AlignBottom);
    t14_ti_axisY = new QValueAxis();
    t14_ti_chart->addAxis(t14_ti_axisY,Qt::AlignLeft);

    t14_al_axisX = new QDateTimeAxis();
    t14_al_axisX->setFormat("MM-dd HH:mm:ss");
    t14_al_axisX->setTitleText("Date");
    t14_al_axisX->setVisible(true);
    t14_al_axisX->setTitleVisible(true);
    t14_al_chart->addAxis(t14_al_axisX, Qt::AlignBottom);
    t14_al_axisY = new QValueAxis();
    t14_al_chart->addAxis(t14_al_axisY,Qt::AlignLeft);

    ti_al_chart_init(t14_ti_chart,t14_al_chart,t14_ti_value_series,t14_al_value_series,t14_ti_UCL_value_series,t14_al_UCL_value_series,
                     t14_al_axisX,t14_al_axisY,t14_ti_axisX,t14_ti_axisY,"MD045");
    query.exec("SELECT machine_name,machine_code FROM Thin_film_spec_managerment");
    while(query.next()){
        ui->t14_CB_move_machine->addItem(query.value("machine_name").toString());
    }
    temp_run_widget_map.insert("MD045",ui->t14_temp_run_mode_time);
    temp_run_ti_accmulate_widget_map.insert("MD045",ui->t14_ti_accmulate);
    temp_run_al_accmulate_widget_map.insert("MD045",ui->t14_al_accmulate);
    ui->t14_CB_wet_insert->addItems(wet_machine_list);
    sync_now_date("MD045",t14_input_item->ui->LE_au_f_add,t14_input_item->ui->LE_now_r_weight,t14_input_item->ui->LE_au_after_r_weight
                  ,t14_input_item->ui->LE_au_after_weight);
    ui->t14_data_table->selectionModel()->setObjectName("t14_data");
    connect(ui->t14_data_table->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this,SLOT(table_view_selection_change(QModelIndex,QModelIndex)));
}
void Thin_film_mainwindows::t14_recipe_choice(int idx)
{
    if(idx == 0){
        t14_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t14_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t14_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t14_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t14_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t14_input_item->ui->LE_Recipe_name1->setText("");
        t14_input_item->ui->LE_Recipe_name2->setText("");
        t14_input_item->ui->LE_Recipe_name3->setText("");
        t14_input_item->ui->LE_Recipe_name4->setText("");
        t14_input_item->ui->LE_Recipe_name5->setText("");
        return ;
    }
    QSqlQuery query(my_mesdb);
    query.exec(QString("select * from `Thin_film_repcipe` where recipe_number  = %1 AND machine_code = 'MD045'").arg(idx));

    if(query.next()){
        t14_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t14_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t14_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t14_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t14_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t14_input_item->ui->LE_Recipe_name1->setText("");
        t14_input_item->ui->LE_Recipe_name2->setText("");
        t14_input_item->ui->LE_Recipe_name3->setText("");
        t14_input_item->ui->LE_Recipe_name4->setText("");
        t14_input_item->ui->LE_Recipe_name5->setText("");
        if(query.value("metal_name").toString() != ""){
            QString item_data = query.value("metal_name").toString();
            QStringList item_list = item_data.split("/");
            for(int i=0;i<item_list.count();i++){
                if(i==0){
                    t14_input_item->ui->CB_Recipe_name1->setCurrentText(item_list.at(i));
                }else if(i==1){
                    t14_input_item->ui->CB_Recipe_name2->setCurrentText(item_list.at(i));
                }else if(i==2){
                    t14_input_item->ui->CB_Recipe_name3->setCurrentText(item_list.at(i));
                }else if(i==3){
                    t14_input_item->ui->CB_Recipe_name4->setCurrentText(item_list.at(i));
                }else if(i==4){
                    t14_input_item->ui->CB_Recipe_name5->setCurrentText(item_list.at(i));
                }
            }
        }
        if(query.value("metal_thin").toString() != ""){
            QStringList recipe_item = query.value("metal_thin").toString().split("/");
            if(recipe_item.count()>0){
                t14_input_item->ui->LE_Recipe_name1->setText(QString("%1").arg(recipe_item[0]));
            }
            if(recipe_item.count()>1){
                t14_input_item->ui->LE_Recipe_name2->setText(QString("%1").arg(recipe_item[1]));
            }
            if(recipe_item.count()>2){
                t14_input_item->ui->LE_Recipe_name3->setText(QString("%1").arg(recipe_item[2]));
            }
            if(recipe_item.count()>3){
                t14_input_item->ui->LE_Recipe_name4->setText(QString("%1").arg(recipe_item[3]));
            }
            if(recipe_item.count()>4){
                t14_input_item->ui->LE_Recipe_name5->setText(QString("%1").arg(recipe_item[4]));
            }
        }
    }
}

void Thin_film_mainwindows::t14_ti_y_value_slot(QPointF value)
{
    ui->t14_ti_y_value->setText(QString("%1").arg(value.y()));
}
void Thin_film_mainwindows::t14_al_y_value_slot(QPointF value)
{
    ui->t14_al_y_value->setText(QString("%1").arg(value.y()));
}


void Thin_film_mainwindows::t14_datamodel_dataChanged(QModelIndex index1, QModelIndex index2, QVector<int> vector)
{
    v1_data_model_change(index1,t14_data_model,"MD045");
    ui->t14_data_table->scrollToBottom();
}


void Thin_film_mainwindows::on_t14_LE_Lot_count_editingFinished()
{

}
void Thin_film_mainwindows::on_t14_LE_Run_number_editingFinished()
{

}
void Thin_film_mainwindows::on_t14_Input_btn_clicked()
{

      QString Lot_count = ui->t14_LE_Lot_count->text();
      QString resister = t14_input_item->ui->LE_resister->text();
      QString au_f_add = t14_input_item->ui->LE_au_f_add->text();
      QString now_r_weight = t14_input_item->ui->LE_now_r_weight->text();
      QString au_after_r_weight = t14_input_item->ui->LE_au_after_r_weight->text();
      QString au_use =  t14_input_item->ui->LE_au_use->text();
      QString ti_add =  t14_input_item->ui->LE_ti_add->text();
      QString al_add =  t14_input_item->ui->LE_al_add->text();
      QString xtal_life =  t14_input_item->ui->LE_xtal_life->text();
      QString name =  t14_input_item->ui->LE_name->text();
      QString X_tal_number = t14_input_item->ui->LE_Xtal_number->text();
      QString X_tal_Hz = t14_input_item->ui->LE_Xtal_hz->text();

      QString note =  t14_input_item->ui->LE_note->text();

      ui->t14_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
      ui->t14_search_time_start->setDateTime(ui->t14_search_time_end->dateTime().addDays(-2));

      QString Recipe_total_text;
      QString Recipe_total_name_text;
      QString Recipe_name1 = t14_input_item->ui->LE_Recipe_name1->text();
      QString Recipe_name2 = t14_input_item->ui->LE_Recipe_name2->text();
      QString Recipe_name3 = t14_input_item->ui->LE_Recipe_name3->text();
      QString Recipe_name4 = t14_input_item->ui->LE_Recipe_name4->text();
      QString Recipe_name5 = t14_input_item->ui->LE_Recipe_name5->text();
      QString CB_Recipe_name1 = t14_input_item->ui->CB_Recipe_name1->currentText();
      QString CB_Recipe_name2 = t14_input_item->ui->CB_Recipe_name2->currentText();
      QString CB_Recipe_name3 = t14_input_item->ui->CB_Recipe_name3->currentText();
      QString CB_Recipe_name4 = t14_input_item->ui->CB_Recipe_name4->currentText();
      QString CB_Recipe_name5 = t14_input_item->ui->CB_Recipe_name5->currentText();
      int Recipe_count = 0;
      int ti_count = 0;
      int au_count = 0;
      int al_count = 0;

      if(CB_Recipe_name1 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name1));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name1));
          if(CB_Recipe_name1=="Ti"){
              ti_count += Recipe_name1.toInt();
          }else if(CB_Recipe_name1=="Al"){
              al_count += Recipe_name1.toInt();
          }else if(CB_Recipe_name1=="Au"){
              au_count += Recipe_name1.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name2 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name2));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name2));
          if(CB_Recipe_name2=="Ti"){
              ti_count += Recipe_name2.toInt();
          }else if(CB_Recipe_name2=="Al"){
              al_count += Recipe_name2.toInt();
          }else if(CB_Recipe_name2=="Au"){
              au_count += Recipe_name2.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name3 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name3));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name3));
          if(CB_Recipe_name3=="Ti"){
              ti_count += Recipe_name3.toInt();
          }else if(CB_Recipe_name3=="Al"){
              al_count += Recipe_name3.toInt();
          }else if(CB_Recipe_name3=="Au"){
              au_count += Recipe_name3.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name4 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name4));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name4));
          if(CB_Recipe_name4=="Ti"){
              ti_count += Recipe_name4.toInt();
          }else if(CB_Recipe_name4=="Al"){
              al_count += Recipe_name4.toInt();
          }else if(CB_Recipe_name4=="Au"){
              au_count += Recipe_name4.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name5 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name5));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name5));
          if(CB_Recipe_name5=="Ti"){
              ti_count += Recipe_name5.toInt();
          }else if(CB_Recipe_name5=="Al"){
              al_count += Recipe_name5.toInt();
          }else if(CB_Recipe_name5=="Au"){
              au_count += Recipe_name5.toInt();
          }
          Recipe_count++;
      }
      if(Recipe_count>0){
          Recipe_total_text.remove(Recipe_total_text.length()-1,1);
          Recipe_total_name_text.remove(Recipe_total_name_text.length()-1,1);
      }
      QSqlQuery query(my_mesdb);
      QString batch_id;
      query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_code = 'MD045'"));
      if(query.next()){
          batch_id = query.value("current_batch_id").toString();
          query.exec("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+1  where machine_code = 'MD045'");
      }
      bool query_result = false;
      qDebug()<<ui->t14_input_lot_table->rowCount();
      query.exec("select now() as now_time");
      QDateTime now_time;

      now_time = QDateTime::currentDateTime();
      for(int i=0;i<ui->t14_input_lot_table->rowCount();i++){
          QString Lot_id = t14_lot_item_list.at(i)->ui->LE_Lot_id->text();
          QString wafer_count = t14_lot_item_list.at(i)->ui->LE_wafer_count->text();
          QString ashing1 = t14_lot_item_list.at(i)->ui->LE_ashing1->text();
          QString ashing2 = t14_lot_item_list.at(i)->ui->LE_ashing2->text();
          QString ashing3 = t14_lot_item_list.at(i)->ui->LE_ashing3->text();
          QString ashing_machine = t14_lot_item_list.at(i)->ui->LE_ashing_machine->text();
          QString machine_type = t14_lot_item_list.at(i)->ui->LE_machine_type->text();
          QString wafer_angle = t14_lot_item_list.at(i)->ui->LE_wafer_angle->text();

          if(Lot_id.trimmed() !=""){
              QString query_str = QString("INSERT INTO `Thin_film_data` "
                                          "(`Run_number`, `Resister`, `machine_type`, `Lot_id`, `Wafer_count`, "
                                          "`Wafer_angle`, `Input_thin_data`, `ashing1`, `ashing2`, "
                                          "`ashing3`, `ashing_machine`, `Pad_thin`, `Wlp_thin`, "
                                          "`N2_Blow`, `Au_f_add`, `Now_r_weight`, `Au_after_weight`, "
                                          "`Au_use`, `Recipe`, `Ti_add`, `Al_add`, `X_tal_number`,`X_tal_Hz`, "
                                          "`X_tal_life`, `User_name`, `D_use`, `note`, `Ti_total`, "
                                          "`Al_total`, `Au_total`, `batch_id`, `cleaning_cal_flag`,`machine_code`,`machine_name`,"
                                          "`Input_time`,`vacuum_time`,`day_and_night`,`au_pellet_output`,`au_add_weight`,`dom`) "
                                          "VALUES "
                                          "('%1', '%2', '%3', '%4', '%5', '%6', "
                                          "'%7', '%8', '%9', '%10', "
                                          "'%11', '%12', '%13', '%14', '%15', '%16', "
                                          "'%17', '%18', '%19', '%20', '%21', '%22','%23', "
                                          "'%24', '%25', '%26', '%27', '%28', "
                                          "'%29', '%30', '%31' ,'%32','%33','%34','%35','%36','%37','%38','%39','%40');").arg("").arg(resister).arg(machine_type).arg(Lot_id).arg(wafer_count)
                                                                        .arg(wafer_angle).arg(Recipe_total_text).arg(ashing1).arg(ashing2)
                                                                        .arg(ashing3).arg(ashing_machine).arg(QString("0")).arg(QString("0"))
                                                                        .arg("").arg(au_f_add).arg(now_r_weight).arg(au_after_r_weight)
                                                                        .arg(au_use).arg(Recipe_total_name_text).arg(ti_add).arg(al_add).arg(X_tal_number).arg(X_tal_Hz)
                                                                        .arg(xtal_life).arg(name).arg("").arg(note).arg(ti_count)
                                                                        .arg(al_count).arg(au_count).arg(batch_id).arg(QString("0")).arg("MD045").arg("hanil#2")
                                                                        .arg(now_time.toString("yyyy-MM-dd hh:mm:ss"))
                                                                        .arg(t14_input_item->ui->DT_vacuum_time->dateTime().toString("yyyy-MM-dd hh:mm:00"))
                                                                        .arg(t14_input_item->ui->CB_day_and_night->currentText())
                                                                        .arg(t14_input_item->ui->DT_vacuum_time->dateTime().addSecs(-300).toString("yyyy-MM-dd hh:mm:00"))
                                                                        .arg(t14_input_item->ui->LE_au_after_weight->text())
                                                                        .arg(t14_input_item->ui->CB_domnumber->currentText());
              query_result = query.exec(query_str);

              if(!query_result){
                  break;
              }

          }
      }
      au_calc(now_time);
      if(query_result){
          t14_input_item->clear_data();
          for(int i=0;i<ui->t14_input_lot_table->rowCount();i++){
              t14_lot_item_list.at(i)->clear_data();
          }
          QMessageBox msg;
          msg.addButton(QMessageBox::Ok);
          msg.setText(tr("input complete"));
          msg.exec();
          on_t14_LE_Run_number_editingFinished();
          ui->t14_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
          ui->t14_search_time_start->setDateTime(ui->t14_search_time_end->dateTime().addDays(-2));
          t14_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
          on_t14_search_btn_clicked();
          ui->t14_data_table->scrollToBottom();
      }else {
          QMessageBox::warning(this, tr("conntion false"),
                                                         "server connection fail\n"
                                                            ""+my_mesdb.lastError().text(),
                                                              QMessageBox::Close);
      }
      query.exec(QString("select * from depostion_au_use_current_view where machine_name = '%1' AND batch_id = %2")
                 .arg("hanil#2").arg(batch_id));
      if(query.next()){
          double  OKNG = query.value("OKNG").toDouble();
          if(abs(OKNG)>0.1){
              QMessageBox msg;
              msg.addButton(QMessageBox::Ok);
              msg.setText(tr("au pellet NG"));
              msg.exec();
          }
      }
      query.exec("update `Thin_film_spec_managerment` set au_pullet_add = 0 ,now_r_weight = 0,au_after_r_weight =0,au_after_weight=0 where machine_code = 'MD045'");
      ti_al_chart_init(t14_ti_chart,t14_al_chart,t14_ti_value_series,t14_al_value_series,t14_ti_UCL_value_series,t14_al_UCL_value_series,
                       t14_al_axisX,t14_al_axisY,t14_ti_axisX,t14_ti_axisY,"MD045");
}
void Thin_film_mainwindows::on_t14_search_btn_clicked()
{
    t14_data_model->setFilter(QString("vacuum_time Between '%1' AND '%2' AND machine_code = 'MD045'")
                          .arg(ui->t14_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t14_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    t14_data_model->select();
    t14_data_model->setSort(2,Qt::AscendingOrder);
    QSqlQuery query(my_mesdb);
    query.exec(QString("select sum((case when (`A`.`machine_code` = 'MD045') then `A`.`Wafer_count` end)) AS `wafer` from `Thin_film_data` `A` "
               "where vacuum_time Between '%1' AND '%2' AND machine_code = 'MD045'").arg(ui->t14_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
               .arg(ui->t14_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    if(query.next()){

        ui->t14_total_wafer->setText(query.value("wafer").toString());
    }
    ui->t14_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t14_BT_search_lot_id_clicked()
{
    t14_data_model->setFilter(QString("Lot_id = '%1' AND machine_code = 'MD045'")
                          .arg(ui->t4_LE_search_lot_id->text()));
    t14_data_model->select();
    t14_data_model->setSort(1,Qt::AscendingOrder);
}
void Thin_film_mainwindows::on_t14_DT_del_btn_clicked()
{
    int count = ui->t14_data_table->selectionModel()->selectedIndexes().count();
    QVector<int> index_history;
    for(int i=0;i<count;i++){
        if(!index_history.contains(ui->t14_data_table->selectionModel()->selectedIndexes().at(i).row())){
        QDateTime Input_time2 = t14_data_model->record(ui->t14_data_table->selectionModel()->selectedIndexes().at(i).row()).value("Input_time").toDateTime();
        t14_data_model->removeRow(ui->t14_data_table->selectionModel()->selectedIndexes().at(i).row());
         au_calc(Input_time2);
         index_history.append(ui->t14_data_table->selectionModel()->selectedIndexes().at(i).row());
        }else {

        }
    }
    on_t14_search_btn_clicked();
    ui->t14_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t14_move_machine_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    QString batch_id_temp = "0";
    QString machine_name;
    QString machine_code;

    int count  = ui->t14_data_table->selectionModel()->selectedIndexes().count();
    for(int i=0;i<count;i++){
         QSqlRecord recode = t14_data_model->record(ui->t14_data_table->selectionModel()->selectedIndexes().at(i).row());
        if(batch_id_temp != recode.value("batch_id").toString()){
            query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_name = '%1'").arg(ui->t14_CB_move_machine->currentText()));
            if(query.next()){
                batch_id_temp = query.value("current_batch_id").toString()+5;
                machine_name = query.value("machine_name").toString();
                machine_code = query.value("machine_code").toString();
                query.exec(QString("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+10  where machine_name = '%1'").arg(ui->t14_CB_move_machine->currentText()));
            }
            batch_id_temp = recode.value("batch_id").toString();
        }

        recode.setGenerated("batch_id",true);
        recode.setGenerated("machine_name",true);
        recode.setGenerated("machine_code",true);
        recode.setValue("batch_id",batch_id_temp);
        recode.setValue("machine_name",machine_name);
        recode.setValue("machine_code",machine_code);
        t14_data_model->setRecord(ui->t14_data_table->selectionModel()->selectedIndexes().at(i).row(),recode);
    }
    on_t14_search_btn_clicked();
    ui->t14_data_table->scrollToBottom();
}


void Thin_film_mainwindows::on_t14_excel_header_copy_flag_toggled(bool checked)
{
    ui->t14_data_table->copyheader_flag = checked;
}


void Thin_film_mainwindows::on_t14_machine_temp_run_mode_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    query.exec("update Thin_film_spec_managerment set nomal_mode_time = ADDTIME(NOW(),'0:5:0') where `machine_code`='MD045'");
}



//t14_function -- end --

//t15_function -- start --
void Thin_film_mainwindows::t15_init()
{
    ui->t15_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t15_search_time_start->setDateTime(ui->t15_search_time_end->dateTime().addDays(-2));

    QSqlTableModel *spec_model = new QSqlTableModel(this,my_mesdb);
    spec_model->setTable("Thin_film_spec_managerment");
    spec_model->setFilter("machine_code = 'MD049'");
    spec_model->select();
    spec_model->setHeaderData(4,Qt::Horizontal,tr("ashing_LCL"));
    spec_model->setHeaderData(5,Qt::Horizontal,tr("ashing_UCL"));
    spec_model->setHeaderData(6,Qt::Horizontal,tr("WLPTiAlashing_LCL"));
    spec_model->setHeaderData(7,Qt::Horizontal,tr("WLPTiAlashing_UCL"));
    spec_model->setHeaderData(8,Qt::Horizontal,tr("WLP_LCL_Ti_Au"));
    spec_model->setHeaderData(9,Qt::Horizontal,tr("WLP_UCL_Ti_Au"));
    spec_model->setHeaderData(10,Qt::Horizontal,tr("WLP_LCL_Ti_Al"));
    spec_model->setHeaderData(11,Qt::Horizontal,tr("WLP_UCL_Ti_Al"));
    spec_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    t15_data_model = new thin_sqltable_model(this,my_mesdb,"MD049",spec_model);

    t15_data_model->setTable("Thin_film_data");

    t15_data_model->setFilter(QString("Input_time Between '%1' AND '%2' AND machine_code = 'MD049'")
                          .arg(ui->t15_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t15_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    ui->t15_data_table->setItemDelegateForColumn(t15_data_model->fieldIndex("vacuum_time"),new DateDelegate(ui->t15_data_table));
    ui->t15_data_table->setItemDelegateForColumn(t15_data_model->fieldIndex("Input_time"),new DateDelegate(ui->t15_data_table));
    ui->t15_data_table->setModel(t15_data_model);
    ui->t15_data_table->setSortingEnabled(true);
    connect(t15_data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>))
            ,this,SLOT(t15_datamodel_dataChanged(QModelIndex,QModelIndex,QVector<int>)));
    ui->t15_data_table->setItemDelegateForColumn(10,new Thin_combodelegate(note_combo_list,ui->t15_data_table));

    t15_data_model->setEditStrategy(QSqlTableModel::OnFieldChange);
    t15_data_model->setSort(2,Qt::AscendingOrder);

    t15_data_model->select();

    t15_data_model->setHeaderData(t15_data_model->fieldIndex("day_and_night"),Qt::Horizontal,tr("day_and_night"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("vacuum_time"),Qt::Horizontal,tr("vacuum_time"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Run_number"),Qt::Horizontal,tr("thin_Run_number"));
//    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Resister"),Qt::Horizontal,tr("thin_Resister"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("machine_type"),Qt::Horizontal,tr("thin_machine_type"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Lot_id"),Qt::Horizontal,tr("thin_Lot_id"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Wafer_count"),Qt::Horizontal,tr("thin_Wafer_count"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Pad_thin"),Qt::Horizontal,tr("thin_Pad_thin"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Wlp_thin"),Qt::Horizontal,tr("thin_Wlp_thin"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Wafer_angle"),Qt::Horizontal,tr("thin_Wafer_angle"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Input_thin_data"),Qt::Horizontal,tr("thin_Input_thin_data"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Recipe"),Qt::Horizontal,tr("thin_Recipe"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("ashing1"),Qt::Horizontal,tr("thin_ashing1"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("ashing2"),Qt::Horizontal,tr("thin_ashing2"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("ashing3"),Qt::Horizontal,tr("thin_ashing3"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("ashing_machine"),Qt::Horizontal,tr("thin_ashing_machine"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("N2_Blow"),Qt::Horizontal,tr("thin_N2_Blow"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Au_f_add"),Qt::Horizontal,tr("thin_thin_Au_f_add"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Now_r_weight"),Qt::Horizontal,tr("thin_Now_r_weight"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Au_after_weight"),Qt::Horizontal,tr("thin_Au_after_weight"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Au_use"),Qt::Horizontal,tr("thin_Au_use"));
//    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Ti_add"),Qt::Horizontal,tr("thin_Ti_add"));
//    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Al_add"),Qt::Horizontal,tr("thin_Al_add"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("X_tal_number"),Qt::Horizontal,tr("thin_X_tal_number"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("X_tal_Hz"),Qt::Horizontal,tr("thin_X_tal_Hz"));
//    t15_data_model->setHeaderData(t15_data_model->fieldIndex("X_tal_life"),Qt::Horizontal,tr("thin_X_tal_life"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("User_name"),Qt::Horizontal,tr("thin_User_name"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("D_use"),Qt::Horizontal,tr("thin_D_use"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("note"),Qt::Horizontal,tr("thin_note"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Ti_total"),Qt::Horizontal,tr("thin_Ti_total"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Al_total"),Qt::Horizontal,tr("thin_Al_total"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Au_total"),Qt::Horizontal,tr("thin_Au_total"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("Input_time"),Qt::Horizontal,tr("thin_Input_time"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("machine_name"),Qt::Horizontal,tr("thin_machine_name"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("machine_code"),Qt::Horizontal,tr("thin_machine_code"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("batch_id"),Qt::Horizontal,tr("thin_batch_id"));
    t15_data_model->setHeaderData(t15_data_model->fieldIndex("cleaning_cal_flag"),Qt::Horizontal,tr("thin_cleaning_cal_flag"));
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("day_and_night"),50);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("vacuum_time"),110);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Run_number"),60);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Resister"),50);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("machine_type"),100);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Lot_id"),80);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Wafer_count"),50);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Pad_thin"),80);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Wlp_thin"),80);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Wafer_angle"),80);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Input_thin_data"),150);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Recipe"),150);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("ashing1"),65);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("ashing2"),65);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("ashing3"),65);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("ashing_machine"),80);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("N2_Blow"),80);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Au_f_add"),80);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Now_r_weight"),120);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Au_after_weight"),140);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Au_use"),50);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Ti_add"),50);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Al_add"),50);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("X_tal_number"),50);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("X_tal_Hz"),50);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("X_tal_life"),80);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("User_name"),80);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("D_use"),80);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("note"),140);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Ti_total"),60);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Al_total"),60);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Au_total"),60);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("Input_time"),120);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("machine_name"),50);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("machine_code"),50);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("batch_id"),50);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("cleaning_cal_flag"),50);
    ui->t15_data_table->horizontalHeader()->resizeSection(t15_data_model->fieldIndex("dom"),50);
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("X_tal_life"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("machine_code"));
//    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("batch_id"));
//    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("cleaning_cal_flag"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("Au_use"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("au_pellet_output"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("au_add"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("check_user"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("au_pellet_total"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("work_start_au"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("au_add_weight"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("au_case_weight"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("check_NG"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("Run_number"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("Pad_thin"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("Wlp_thin"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("N2_Blow"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("D_use"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("Ti_add"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("Al_add"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("X_tal_life"));
    ui->t15_data_table->horizontalHeader()->hideSection(t15_data_model->fieldIndex("Resister"));


    ui->t15_data_table->scrollToBottom();


    ui->t15_spec_table->setModel(spec_model);
    ui->t15_spec_table->horizontalHeader()->hideSection(0);
    ui->t15_spec_table->horizontalHeader()->hideSection(1);
    ui->t15_spec_table->horizontalHeader()->hideSection(2);
    ui->t15_spec_table->horizontalHeader()->hideSection(3);
    ui->t15_spec_table->horizontalHeader()->hideSection(4);
    ui->t15_spec_table->horizontalHeader()->hideSection(5);
    ui->t15_spec_table->horizontalHeader()->hideSection(6);
    ui->t15_spec_table->horizontalHeader()->hideSection(7);
    ui->t15_spec_table->horizontalHeader()->hideSection(8);
    ui->t15_spec_table->horizontalHeader()->hideSection(9);
    ui->t15_spec_table->horizontalHeader()->hideSection(10);
    ui->t15_spec_table->horizontalHeader()->hideSection(11);
    ui->t15_spec_table->horizontalHeader()->hideSection(12);
    ui->t15_spec_table->horizontalHeader()->hideSection(13);
    ui->t15_spec_table->horizontalHeader()->hideSection(14);
    ui->t15_spec_table->horizontalHeader()->hideSection(17);
    ui->t15_spec_table->horizontalHeader()->hideSection(18);
    ui->t15_spec_table->horizontalHeader()->hideSection(19);
    ui->t15_spec_table->horizontalHeader()->hideSection(20);
    ui->t15_spec_table->horizontalHeader()->hideSection(21);
    ui->t15_spec_table->horizontalHeader()->hideSection(22);
    ui->t15_spec_table->horizontalHeader()->hideSection(23);
    ui->t15_spec_table->horizontalHeader()->hideSection(24);
    ui->t15_spec_table->horizontalHeader()->hideSection(25);
    ui->t15_spec_table->horizontalHeader()->hideSection(26);
    ui->t15_spec_table->horizontalHeader()->hideSection(27);
    ui->t15_spec_table->horizontalHeader()->hideSection(28);
    ui->t15_spec_table->horizontalHeader()->hideSection(29);
    ui->t15_spec_table->horizontalHeader()->hideSection(30);
    ui->t15_spec_table->horizontalHeader()->hideSection(31);
    ui->t15_spec_table->horizontalHeader()->hideSection(32);
    ui->t15_spec_table->horizontalHeader()->hideSection(33);
    ui->t15_spec_table->horizontalHeader()->hideSection(34);
    ui->t15_spec_table->horizontalHeader()->hideSection(35);
    ui->t15_spec_table->horizontalHeader()->resizeSection(6,150);
    ui->t15_spec_table->horizontalHeader()->resizeSection(7,150);

    for(int i=0;i<8;i++){
        ui->t15_input_table->insertColumn(i);
    }
    ui->t15_input_table->insertRow(0);
    ui->t15_input_table->insertRow(1);
    ui->t15_input_table->insertRow(2);
    ui->t15_input_table->insertRow(3);
    ui->t15_input_table->insertRow(4);
    ui->t15_input_table->insertRow(5);
    ui->t15_input_table->insertRow(6);
    ui->t15_input_table->horizontalHeader()->resizeSection(0,70);
    ui->t15_input_table->horizontalHeader()->resizeSection(1,150);
    ui->t15_input_table->horizontalHeader()->resizeSection(3,70);
    ui->t15_input_table->horizontalHeader()->resizeSection(5,130);
    ui->t15_input_table->horizontalHeader()->resizeSection(6,140);
    ui->t15_input_table->horizontalHeader()->resizeSection(7,80);
    t15_input_item = new input_item(my_mesdb,"MD049");
    ui->t15_input_table->setCellWidget(0,0,t15_input_item->ui->LA_day_and_night);
    ui->t15_input_table->setCellWidget(1,0,t15_input_item->ui->CB_day_and_night);
    ui->t15_input_table->setCellWidget(0,1,t15_input_item->ui->LA_vacuum_time);
    ui->t15_input_table->setCellWidget(1,1,t15_input_item->ui->DT_vacuum_time);
    t15_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
//    ui->t15_input_table->setCellWidget(0,2,t15_input_item->ui->LA_resister);
//    ui->t15_input_table->setCellWidget(1,2,t15_input_item->ui->LE_resister);
//    ui->t15_input_table->setCellWidget(0,3,t15_input_item->ui->LA_N2_blow);
//    ui->t15_input_table->setCellWidget(1,3,t15_input_item->ui->WCB_N2_blow);
    ui->t15_input_table->setCellWidget(0,4,t15_input_item->ui->LA_au_f_add);
    ui->t15_input_table->setCellWidget(1,4,t15_input_item->ui->LE_au_f_add);
    ui->t15_input_table->setCellWidget(0,5,t15_input_item->ui->LA_now_r_weight);
    ui->t15_input_table->setCellWidget(1,5,t15_input_item->ui->LE_now_r_weight);
    ui->t15_input_table->setCellWidget(0,6,t15_input_item->ui->LA_au_after_r_weight);
    ui->t15_input_table->setCellWidget(1,6,t15_input_item->ui->LE_au_after_r_weight);
    ui->t15_input_table->setCellWidget(0,7,t15_input_item->ui->LA_au_after_weight);
    ui->t15_input_table->setCellWidget(1,7,t15_input_item->ui->LE_au_after_weight);
//    ui->t15_input_table->setCellWidget(0,7,t15_input_item->ui->LA_au_use);
//    ui->t15_input_table->setCellWidget(1,7,t15_input_item->ui->LE_au_use);
//    ui->t15_input_table->setCellWidget(2,0,t15_input_item->ui->LA_ti_add);
//    ui->t15_input_table->setCellWidget(3,0,t15_input_item->ui->LE_ti_add);
//    ui->t15_input_table->setCellWidget(2,1,t15_input_item->ui->LA_al_add);
//    ui->t15_input_table->setCellWidget(3,1,t15_input_item->ui->LE_al_add);
//    ui->t15_input_table->setCellWidget(2,2,t15_input_item->ui->LA_xtal_life);
//    ui->t15_input_table->setCellWidget(3,2,t15_input_item->ui->LE_xtal_life);
    ui->t15_input_table->setCellWidget(2,2,t15_input_item->ui->W_X_TAL);
    ui->t15_input_table->setCellWidget(2,3,t15_input_item->ui->LA_name);
    ui->t15_input_table->setCellWidget(3,3,t15_input_item->ui->LE_name);
//    ui->t15_input_table->setCellWidget(2,4,t15_input_item->ui->LA_d_run);
//    ui->t15_input_table->setCellWidget(3,4,t15_input_item->ui->WCB_d_run);
    ui->t15_input_table->setCellWidget(2,5,t15_input_item->ui->LA_note);
    ui->t15_input_table->setCellWidget(3,5,t15_input_item->ui->LE_note);
    ui->t15_input_table->setSpan(2,2,2,1);
    ui->t15_input_table->setSpan(2,5,1,5);
    ui->t15_input_table->setSpan(3,5,1,5);
    ui->t15_input_table->setCellWidget(4,0,t15_input_item->ui->LE_recipe_number);
    ui->t15_input_table->setCellWidget(4,1,t15_input_item->ui->CB_recipe_choice);
//    ui->t15_input_table->setCellWidget(4,2,t15_input_item->ui->LA_recipe_2);
    ui->t15_input_table->setCellWidget(5,0,t15_input_item->ui->W_recipe);
    ui->t15_input_table->setSpan(4,1,1,5);
//    ui->t15_input_table->setSpan(4,2,1,3);
    ui->t15_input_table->setSpan(5,0,2,5);
    ui->t15_input_table->setCellWidget(4,6,t15_input_item->ui->LA_dom_number);
    ui->t15_input_table->setCellWidget(5,6,t15_input_item->ui->CB_domnumber);

    ui->t15_LE_Lot_count->setValidator(new QIntValidator(0, 1000, this));
//    ui->t15_LE_Run_number->setValidator(new QIntValidator(0, 1000, this));
    QSqlQuery query(my_mesdb);
    query.exec(QString("select `Lot_max_count`,`Run_number_count` from Thin_film_spec_managerment where machine_code = 'MD049'"));
    if(query.next()){
        ui->t15_LE_Lot_count->setText(query.value("Lot_max_count").toString());
//        ui->t15_LE_Run_number->setText(query.value("Run_number_count").toString());
    }
    t15_lot_item_list.clear();
    for(int i=0;i<ui->t15_LE_Lot_count->text().toInt();i++){

        input_lot_item *lot_item = new input_lot_item(ms_mesdb,t15_input_item,"MD049");
        t15_lot_item_list.append(lot_item);
        ui->t15_input_lot_table->insertRow(i);
        ui->t15_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
        ui->t15_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
        ui->t15_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
        ui->t15_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
        ui->t15_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
        ui->t15_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
        ui->t15_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
        ui->t15_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//        connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                t15_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
    }
    query.exec("SELECT * FROM `Thin_film_repcipe` where machine_code = 'MD049' ORDER BY `recipe_number` ASC  ");
    t15_input_item->ui->CB_recipe_choice->addItem(tr("recipe_choice"));
    while(query.next()){
        QString recipe_name = query.value("recipe_number").toString()+"."+ query.value("recipename").toString()
                                + "|"+query.value("metal_name").toString()+"|"+query.value("metal_thin").toString()+"|"
                                +query.value("type").toString();
        QPixmap pixmap(100,100);
        pixmap.fill(QColor(query.value("color").toString()));
        t15_input_item->ui->CB_recipe_choice->addItem(QIcon(pixmap),recipe_name);
    }
    connect(t15_input_item->ui->CB_recipe_choice,SIGNAL(currentIndexChanged(int)),this,SLOT(t15_recipe_choice(int)));

    t15_ti_chart = new Thin_film_chart();
    t15_ti_chartview = new Thin_film_chart_view(t15_ti_chart);
    connect(t15_ti_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t15_ti_y_value_slot(QPointF)));
    ui->t15_Ti_layout->addWidget(t15_ti_chartview);

    t15_al_chart = new Thin_film_chart();
    t15_al_chartview = new Thin_film_chart_view(t15_al_chart);
    connect(t15_al_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t15_al_y_value_slot(QPointF)));
    ui->t15_Al_layout->addWidget(t15_al_chartview);
    t15_ti_axisX = new QDateTimeAxis();
    t15_ti_axisX->setFormat("MM-dd HH:mm:ss");
    t15_ti_axisX->setTitleText("Date");
    t15_ti_axisX->setVisible(true);
    t15_ti_axisX->setTitleVisible(true);
    t15_ti_chart->addAxis(t15_ti_axisX, Qt::AlignBottom);
    t15_ti_axisY = new QValueAxis();
    t15_ti_chart->addAxis(t15_ti_axisY,Qt::AlignLeft);

    t15_al_axisX = new QDateTimeAxis();
    t15_al_axisX->setFormat("MM-dd HH:mm:ss");
    t15_al_axisX->setTitleText("Date");
    t15_al_axisX->setVisible(true);
    t15_al_axisX->setTitleVisible(true);
    t15_al_chart->addAxis(t15_al_axisX, Qt::AlignBottom);
    t15_al_axisY = new QValueAxis();
    t15_al_chart->addAxis(t15_al_axisY,Qt::AlignLeft);

    ti_al_chart_init(t15_ti_chart,t15_al_chart,t15_ti_value_series,t15_al_value_series,t15_ti_UCL_value_series,t15_al_UCL_value_series,
                     t15_al_axisX,t15_al_axisY,t15_ti_axisX,t15_ti_axisY,"MD049");
    query.exec("SELECT machine_name,machine_code FROM Thin_film_spec_managerment");
    while(query.next()){
        ui->t15_CB_move_machine->addItem(query.value("machine_name").toString());
    }
    temp_run_widget_map.insert("MD049",ui->t15_temp_run_mode_time);
    temp_run_ti_accmulate_widget_map.insert("MD049",ui->t15_ti_accmulate);
    temp_run_al_accmulate_widget_map.insert("MD049",ui->t15_al_accmulate);
    ui->t15_CB_wet_insert->addItems(wet_machine_list);
    sync_now_date("MD049",t15_input_item->ui->LE_au_f_add,t15_input_item->ui->LE_now_r_weight,t15_input_item->ui->LE_au_after_r_weight
                  ,t15_input_item->ui->LE_au_after_weight);
    ui->t15_data_table->selectionModel()->setObjectName("t15_data");
    connect(ui->t15_data_table->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this,SLOT(table_view_selection_change(QModelIndex,QModelIndex)));
}
void Thin_film_mainwindows::t15_recipe_choice(int idx)
{
    if(idx == 0){
        t15_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t15_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t15_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t15_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t15_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t15_input_item->ui->LE_Recipe_name1->setText("");
        t15_input_item->ui->LE_Recipe_name2->setText("");
        t15_input_item->ui->LE_Recipe_name3->setText("");
        t15_input_item->ui->LE_Recipe_name4->setText("");
        t15_input_item->ui->LE_Recipe_name5->setText("");
        return ;
    }
    QSqlQuery query(my_mesdb);
    query.exec(QString("select * from `Thin_film_repcipe` where recipe_number  = %1 AND machine_code = 'MD049'").arg(idx));

    if(query.next()){
        t15_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t15_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t15_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t15_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t15_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t15_input_item->ui->LE_Recipe_name1->setText("");
        t15_input_item->ui->LE_Recipe_name2->setText("");
        t15_input_item->ui->LE_Recipe_name3->setText("");
        t15_input_item->ui->LE_Recipe_name4->setText("");
        t15_input_item->ui->LE_Recipe_name5->setText("");
        if(query.value("metal_name").toString() != ""){
            QString item_data = query.value("metal_name").toString();
            QStringList item_list = item_data.split("/");
            for(int i=0;i<item_list.count();i++){
                if(i==0){
                    t15_input_item->ui->CB_Recipe_name1->setCurrentText(item_list.at(i));
                }else if(i==1){
                    t15_input_item->ui->CB_Recipe_name2->setCurrentText(item_list.at(i));
                }else if(i==2){
                    t15_input_item->ui->CB_Recipe_name3->setCurrentText(item_list.at(i));
                }else if(i==3){
                    t15_input_item->ui->CB_Recipe_name4->setCurrentText(item_list.at(i));
                }else if(i==4){
                    t15_input_item->ui->CB_Recipe_name5->setCurrentText(item_list.at(i));
                }
            }
        }
        if(query.value("metal_thin").toString() != ""){
            QStringList recipe_item = query.value("metal_thin").toString().split("/");
            if(recipe_item.count()>0){
                t15_input_item->ui->LE_Recipe_name1->setText(QString("%1").arg(recipe_item[0]));
            }
            if(recipe_item.count()>1){
                t15_input_item->ui->LE_Recipe_name2->setText(QString("%1").arg(recipe_item[1]));
            }
            if(recipe_item.count()>2){
                t15_input_item->ui->LE_Recipe_name3->setText(QString("%1").arg(recipe_item[2]));
            }
            if(recipe_item.count()>3){
                t15_input_item->ui->LE_Recipe_name4->setText(QString("%1").arg(recipe_item[3]));
            }
            if(recipe_item.count()>4){
                t15_input_item->ui->LE_Recipe_name5->setText(QString("%1").arg(recipe_item[4]));
            }
        }
    }
}

void Thin_film_mainwindows::t15_ti_y_value_slot(QPointF value)
{
    ui->t15_ti_y_value->setText(QString("%1").arg(value.y()));
}

void Thin_film_mainwindows::t15_al_y_value_slot(QPointF value)
{
    ui->t15_al_y_value->setText(QString("%1").arg(value.y()));
}

void Thin_film_mainwindows::t15_datamodel_dataChanged(QModelIndex index1, QModelIndex index2, QVector<int> vector)
{
    v1_data_model_change(index1,t15_data_model,"MD049");
    ui->t15_data_table->scrollToBottom();
}


void Thin_film_mainwindows::on_t15_LE_Lot_count_editingFinished()
{

}
void Thin_film_mainwindows::on_t15_LE_Run_number_editingFinished()
{

}
void Thin_film_mainwindows::on_t15_Input_btn_clicked()
{

      QString Lot_count = ui->t15_LE_Lot_count->text();
      QString resister = t15_input_item->ui->LE_resister->text();
      QString au_f_add = t15_input_item->ui->LE_au_f_add->text();
      QString now_r_weight = t15_input_item->ui->LE_now_r_weight->text();
      QString au_after_r_weight = t15_input_item->ui->LE_au_after_r_weight->text();
      QString au_use =  t15_input_item->ui->LE_au_use->text();
      QString ti_add =  t15_input_item->ui->LE_ti_add->text();
      QString al_add =  t15_input_item->ui->LE_al_add->text();
      QString xtal_life =  t15_input_item->ui->LE_xtal_life->text();
      QString name =  t15_input_item->ui->LE_name->text();
      QString X_tal_number = t15_input_item->ui->LE_Xtal_number->text();
      QString X_tal_Hz = t15_input_item->ui->LE_Xtal_hz->text();

      QString note =  t15_input_item->ui->LE_note->text();

      ui->t15_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
      ui->t15_search_time_start->setDateTime(ui->t15_search_time_end->dateTime().addDays(-2));

      QString Recipe_total_text;
      QString Recipe_total_name_text;
      QString Recipe_name1 = t15_input_item->ui->LE_Recipe_name1->text();
      QString Recipe_name2 = t15_input_item->ui->LE_Recipe_name2->text();
      QString Recipe_name3 = t15_input_item->ui->LE_Recipe_name3->text();
      QString Recipe_name4 = t15_input_item->ui->LE_Recipe_name4->text();
      QString Recipe_name5 = t15_input_item->ui->LE_Recipe_name5->text();
      QString CB_Recipe_name1 = t15_input_item->ui->CB_Recipe_name1->currentText();
      QString CB_Recipe_name2 = t15_input_item->ui->CB_Recipe_name2->currentText();
      QString CB_Recipe_name3 = t15_input_item->ui->CB_Recipe_name3->currentText();
      QString CB_Recipe_name4 = t15_input_item->ui->CB_Recipe_name4->currentText();
      QString CB_Recipe_name5 = t15_input_item->ui->CB_Recipe_name5->currentText();
      int Recipe_count = 0;
      int ti_count = 0;
      int au_count = 0;
      int al_count = 0;

      if(CB_Recipe_name1 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name1));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name1));
          if(CB_Recipe_name1=="Ti"){
              ti_count += Recipe_name1.toInt();
          }else if(CB_Recipe_name1=="Al"){
              al_count += Recipe_name1.toInt();
          }else if(CB_Recipe_name1=="Au"){
              au_count += Recipe_name1.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name2 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name2));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name2));
          if(CB_Recipe_name2=="Ti"){
              ti_count += Recipe_name2.toInt();
          }else if(CB_Recipe_name2=="Al"){
              al_count += Recipe_name2.toInt();
          }else if(CB_Recipe_name2=="Au"){
              au_count += Recipe_name2.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name3 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name3));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name3));
          if(CB_Recipe_name3=="Ti"){
              ti_count += Recipe_name3.toInt();
          }else if(CB_Recipe_name3=="Al"){
              al_count += Recipe_name3.toInt();
          }else if(CB_Recipe_name3=="Au"){
              au_count += Recipe_name3.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name4 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name4));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name4));
          if(CB_Recipe_name4=="Ti"){
              ti_count += Recipe_name4.toInt();
          }else if(CB_Recipe_name4=="Al"){
              al_count += Recipe_name4.toInt();
          }else if(CB_Recipe_name4=="Au"){
              au_count += Recipe_name4.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name5 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name5));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name5));
          if(CB_Recipe_name5=="Ti"){
              ti_count += Recipe_name5.toInt();
          }else if(CB_Recipe_name5=="Al"){
              al_count += Recipe_name5.toInt();
          }else if(CB_Recipe_name5=="Au"){
              au_count += Recipe_name5.toInt();
          }
          Recipe_count++;
      }
      if(Recipe_count>0){
          Recipe_total_text.remove(Recipe_total_text.length()-1,1);
          Recipe_total_name_text.remove(Recipe_total_name_text.length()-1,1);
      }
      QSqlQuery query(my_mesdb);
      QString batch_id;
      query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_code = 'MD049'"));
      if(query.next()){
          batch_id = query.value("current_batch_id").toString();
          query.exec("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+1  where machine_code = 'MD049'");
      }
      bool query_result = false;
      qDebug()<<ui->t15_input_lot_table->rowCount();
      query.exec("select now() as now_time");
      QDateTime now_time;

      now_time = QDateTime::currentDateTime();
      for(int i=0;i<ui->t15_input_lot_table->rowCount();i++){
          QString Lot_id = t15_lot_item_list.at(i)->ui->LE_Lot_id->text();
          QString wafer_count = t15_lot_item_list.at(i)->ui->LE_wafer_count->text();
          QString ashing1 = t15_lot_item_list.at(i)->ui->LE_ashing1->text();
          QString ashing2 = t15_lot_item_list.at(i)->ui->LE_ashing2->text();
          QString ashing3 = t15_lot_item_list.at(i)->ui->LE_ashing3->text();
          QString ashing_machine = t15_lot_item_list.at(i)->ui->LE_ashing_machine->text();
          QString machine_type = t15_lot_item_list.at(i)->ui->LE_machine_type->text();
          QString wafer_angle = t15_lot_item_list.at(i)->ui->LE_wafer_angle->text();

          if(Lot_id.trimmed() !=""){
              QString query_str = QString("INSERT INTO `Thin_film_data` "
                                          "(`Run_number`, `Resister`, `machine_type`, `Lot_id`, `Wafer_count`, "
                                          "`Wafer_angle`, `Input_thin_data`, `ashing1`, `ashing2`, "
                                          "`ashing3`, `ashing_machine`, `Pad_thin`, `Wlp_thin`, "
                                          "`N2_Blow`, `Au_f_add`, `Now_r_weight`, `Au_after_weight`, "
                                          "`Au_use`, `Recipe`, `Ti_add`, `Al_add`, `X_tal_number`,`X_tal_Hz`, "
                                          "`X_tal_life`, `User_name`, `D_use`, `note`, `Ti_total`, "
                                          "`Al_total`, `Au_total`, `batch_id`, `cleaning_cal_flag`,`machine_code`,`machine_name`,"
                                          "`Input_time`,`vacuum_time`,`day_and_night`,`au_pellet_output`,`au_add_weight`,`dom`) "
                                          "VALUES "
                                          "('%1', '%2', '%3', '%4', '%5', '%6', "
                                          "'%7', '%8', '%9', '%10', "
                                          "'%11', '%12', '%13', '%14', '%15', '%16', "
                                          "'%17', '%18', '%19', '%20', '%21', '%22','%23', "
                                          "'%24', '%25', '%26', '%27', '%28', "
                                          "'%29', '%30', '%31' ,'%32','%33','%34','%35','%36','%37','%38','%39','%40');").arg("").arg(resister).arg(machine_type).arg(Lot_id).arg(wafer_count)
                                                                        .arg(wafer_angle).arg(Recipe_total_text).arg(ashing1).arg(ashing2)
                                                                        .arg(ashing3).arg(ashing_machine).arg(QString("0")).arg(QString("0"))
                                                                        .arg("").arg(au_f_add).arg(now_r_weight).arg(au_after_r_weight)
                                                                        .arg(au_use).arg(Recipe_total_name_text).arg(ti_add).arg(al_add).arg(X_tal_number).arg(X_tal_Hz)
                                                                        .arg(xtal_life).arg(name).arg("").arg(note).arg(ti_count)
                                                                        .arg(al_count).arg(au_count).arg(batch_id).arg(QString("0")).arg("MD049").arg("hanil#3")
                                                                        .arg(now_time.toString("yyyy-MM-dd hh:mm:ss"))
                                                                        .arg(t15_input_item->ui->DT_vacuum_time->dateTime().toString("yyyy-MM-dd hh:mm:00"))
                                                                        .arg(t15_input_item->ui->CB_day_and_night->currentText())
                                                                        .arg(t15_input_item->ui->DT_vacuum_time->dateTime().addSecs(-300).toString("yyyy-MM-dd hh:mm:00"))
                                                                        .arg(t15_input_item->ui->LE_au_after_weight->text())
                                                                        .arg(t15_input_item->ui->CB_domnumber->currentText());
              query_result = query.exec(query_str);

              if(!query_result){
                  break;
              }

          }
      }
      au_calc(now_time);
      if(query_result){
          t15_input_item->clear_data();
          for(int i=0;i<ui->t15_input_lot_table->rowCount();i++){
              t15_lot_item_list.at(i)->clear_data();
          }
          QMessageBox msg;
          msg.addButton(QMessageBox::Ok);
          msg.setText(tr("input complete"));
          msg.exec();
          on_t15_LE_Run_number_editingFinished();
          ui->t15_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
          ui->t15_search_time_start->setDateTime(ui->t15_search_time_end->dateTime().addDays(-2));
          t15_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
          on_t15_search_btn_clicked();
          ui->t15_data_table->scrollToBottom();
      }else {
          QMessageBox::warning(this, tr("conntion false"),
                                                         "server connection fail\n"
                                                            ""+my_mesdb.lastError().text(),
                                                              QMessageBox::Close);
      }
      query.exec(QString("select * from depostion_au_use_current_view where machine_name = '%1' AND batch_id = %2")
                 .arg("hanil#3").arg(batch_id));
      if(query.next()){
          double  OKNG = query.value("OKNG").toDouble();
          if(abs(OKNG)>0.1){
              QMessageBox msg;
              msg.addButton(QMessageBox::Ok);
              msg.setText(tr("au pellet NG"));
              msg.exec();
          }
      }
      query.exec("update `Thin_film_spec_managerment` set au_pullet_add = 0 ,now_r_weight = 0,au_after_r_weight =0,au_after_weight=0 where machine_code = 'MD049'");
      ti_al_chart_init(t15_ti_chart,t15_al_chart,t15_ti_value_series,t15_al_value_series,t15_ti_UCL_value_series,t15_al_UCL_value_series,
                       t15_al_axisX,t15_al_axisY,t15_ti_axisX,t15_ti_axisY,"MD049");
}
void Thin_film_mainwindows::on_t15_search_btn_clicked()
{
    t15_data_model->setFilter(QString("vacuum_time Between '%1' AND '%2' AND machine_code = 'MD049'")
                          .arg(ui->t15_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t15_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    t15_data_model->select();
    t15_data_model->setSort(2,Qt::AscendingOrder);
    QSqlQuery query(my_mesdb);
    query.exec(QString("select sum((case when (`A`.`machine_code` = 'MD049') then `A`.`Wafer_count` end)) AS `wafer` from `Thin_film_data` `A` "
               "where vacuum_time Between '%1' AND '%2' AND machine_code = 'MD049'").arg(ui->t15_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
               .arg(ui->t15_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    if(query.next()){

        ui->t15_total_wafer->setText(query.value("wafer").toString());
    }
    ui->t15_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t15_BT_search_lot_id_clicked()
{
    t15_data_model->setFilter(QString("Lot_id = '%1' AND machine_code = 'MD049'")
                          .arg(ui->t4_LE_search_lot_id->text()));
    t15_data_model->select();
    t15_data_model->setSort(1,Qt::AscendingOrder);
}
void Thin_film_mainwindows::on_t15_DT_del_btn_clicked()
{
    int count = ui->t15_data_table->selectionModel()->selectedIndexes().count();
    QVector<int> index_history;
    for(int i=0;i<count;i++){
        if(!index_history.contains(ui->t15_data_table->selectionModel()->selectedIndexes().at(i).row())){
        QDateTime Input_time2 = t15_data_model->record(ui->t15_data_table->selectionModel()->selectedIndexes().at(i).row()).value("Input_time").toDateTime();
        t15_data_model->removeRow(ui->t15_data_table->selectionModel()->selectedIndexes().at(i).row());
         au_calc(Input_time2);
         index_history.append(ui->t15_data_table->selectionModel()->selectedIndexes().at(i).row());
        }else {

        }
    }
    on_t15_search_btn_clicked();
    ui->t15_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t15_move_machine_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    QString batch_id_temp = "0";
    QString machine_name;
    QString machine_code;

    int count  = ui->t15_data_table->selectionModel()->selectedIndexes().count();
    for(int i=0;i<count;i++){
         QSqlRecord recode = t15_data_model->record(ui->t15_data_table->selectionModel()->selectedIndexes().at(i).row());
        if(batch_id_temp != recode.value("batch_id").toString()){
            query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_name = '%1'").arg(ui->t15_CB_move_machine->currentText()));
            if(query.next()){
                batch_id_temp = query.value("current_batch_id").toString()+5;
                machine_name = query.value("machine_name").toString();
                machine_code = query.value("machine_code").toString();
                query.exec(QString("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+10  where machine_name = '%1'").arg(ui->t15_CB_move_machine->currentText()));
            }
            batch_id_temp = recode.value("batch_id").toString();
        }

        recode.setGenerated("batch_id",true);
        recode.setGenerated("machine_name",true);
        recode.setGenerated("machine_code",true);
        recode.setValue("batch_id",batch_id_temp);
        recode.setValue("machine_name",machine_name);
        recode.setValue("machine_code",machine_code);
        t15_data_model->setRecord(ui->t15_data_table->selectionModel()->selectedIndexes().at(i).row(),recode);
    }
    on_t15_search_btn_clicked();
    ui->t15_data_table->scrollToBottom();
}


void Thin_film_mainwindows::on_t15_excel_header_copy_flag_toggled(bool checked)
{
    ui->t15_data_table->copyheader_flag = checked;
}


void Thin_film_mainwindows::on_t15_machine_temp_run_mode_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    query.exec("update Thin_film_spec_managerment set nomal_mode_time = ADDTIME(NOW(),'0:5:0') where `machine_code`='MD049'");
}



//t15_function -- end --

//t16_function -- start --
void Thin_film_mainwindows::t16_init()
{
    ui->t16_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t16_search_time_start->setDateTime(ui->t16_search_time_end->dateTime().addDays(-2));

    QSqlTableModel *spec_model = new QSqlTableModel(this,my_mesdb);
    spec_model->setTable("Thin_film_spec_managerment");
    spec_model->setFilter("machine_code = 'MD048'");
    spec_model->select();
    spec_model->setHeaderData(4,Qt::Horizontal,tr("ashing_LCL"));
    spec_model->setHeaderData(5,Qt::Horizontal,tr("ashing_UCL"));
    spec_model->setHeaderData(6,Qt::Horizontal,tr("WLPTiAlashing_LCL"));
    spec_model->setHeaderData(7,Qt::Horizontal,tr("WLPTiAlashing_UCL"));
    spec_model->setHeaderData(8,Qt::Horizontal,tr("WLP_LCL_Ti_Au"));
    spec_model->setHeaderData(9,Qt::Horizontal,tr("WLP_UCL_Ti_Au"));
    spec_model->setHeaderData(10,Qt::Horizontal,tr("WLP_LCL_Ti_Al"));
    spec_model->setHeaderData(11,Qt::Horizontal,tr("WLP_UCL_Ti_Al"));
    spec_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    t16_data_model = new thin_sqltable_model(this,my_mesdb,"MD048",spec_model);

    t16_data_model->setTable("Thin_film_data");

    t16_data_model->setFilter(QString("Input_time Between '%1' AND '%2' AND machine_code = 'MD048'")
                          .arg(ui->t16_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t16_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    ui->t16_data_table->setItemDelegateForColumn(t16_data_model->fieldIndex("vacuum_time"),new DateDelegate(ui->t16_data_table));
    ui->t16_data_table->setItemDelegateForColumn(t16_data_model->fieldIndex("Input_time"),new DateDelegate(ui->t16_data_table));
    ui->t16_data_table->setModel(t16_data_model);
    ui->t16_data_table->setSortingEnabled(true);
    connect(t16_data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>))
            ,this,SLOT(t16_datamodel_dataChanged(QModelIndex,QModelIndex,QVector<int>)));
    ui->t16_data_table->setItemDelegateForColumn(10,new Thin_combodelegate(note_combo_list,ui->t16_data_table));

    t16_data_model->setEditStrategy(QSqlTableModel::OnFieldChange);
    t16_data_model->setSort(2,Qt::AscendingOrder);

    t16_data_model->select();

    t16_data_model->setHeaderData(t16_data_model->fieldIndex("day_and_night"),Qt::Horizontal,tr("day_and_night"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("vacuum_time"),Qt::Horizontal,tr("vacuum_time"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Run_number"),Qt::Horizontal,tr("thin_Run_number"));
//    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Resister"),Qt::Horizontal,tr("thin_Resister"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("machine_type"),Qt::Horizontal,tr("thin_machine_type"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Lot_id"),Qt::Horizontal,tr("thin_Lot_id"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Wafer_count"),Qt::Horizontal,tr("thin_Wafer_count"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Pad_thin"),Qt::Horizontal,tr("thin_Pad_thin"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Wlp_thin"),Qt::Horizontal,tr("thin_Wlp_thin"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Wafer_angle"),Qt::Horizontal,tr("thin_Wafer_angle"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Input_thin_data"),Qt::Horizontal,tr("thin_Input_thin_data"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Recipe"),Qt::Horizontal,tr("thin_Recipe"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("ashing1"),Qt::Horizontal,tr("thin_ashing1"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("ashing2"),Qt::Horizontal,tr("thin_ashing2"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("ashing3"),Qt::Horizontal,tr("thin_ashing3"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("ashing_machine"),Qt::Horizontal,tr("thin_ashing_machine"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("N2_Blow"),Qt::Horizontal,tr("thin_N2_Blow"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Au_f_add"),Qt::Horizontal,tr("thin_thin_Au_f_add"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Now_r_weight"),Qt::Horizontal,tr("thin_Now_r_weight"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Au_after_weight"),Qt::Horizontal,tr("thin_Au_after_weight"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Au_use"),Qt::Horizontal,tr("thin_Au_use"));
//    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Ti_add"),Qt::Horizontal,tr("thin_Ti_add"));
//    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Al_add"),Qt::Horizontal,tr("thin_Al_add"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("X_tal_number"),Qt::Horizontal,tr("thin_X_tal_number"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("X_tal_Hz"),Qt::Horizontal,tr("thin_X_tal_Hz"));
//    t16_data_model->setHeaderData(t16_data_model->fieldIndex("X_tal_life"),Qt::Horizontal,tr("thin_X_tal_life"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("User_name"),Qt::Horizontal,tr("thin_User_name"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("D_use"),Qt::Horizontal,tr("thin_D_use"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("note"),Qt::Horizontal,tr("thin_note"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Ti_total"),Qt::Horizontal,tr("thin_Ti_total"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Al_total"),Qt::Horizontal,tr("thin_Al_total"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Au_total"),Qt::Horizontal,tr("thin_Au_total"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("Input_time"),Qt::Horizontal,tr("thin_Input_time"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("machine_name"),Qt::Horizontal,tr("thin_machine_name"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("machine_code"),Qt::Horizontal,tr("thin_machine_code"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("batch_id"),Qt::Horizontal,tr("thin_batch_id"));
    t16_data_model->setHeaderData(t16_data_model->fieldIndex("cleaning_cal_flag"),Qt::Horizontal,tr("thin_cleaning_cal_flag"));
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("day_and_night"),50);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("vacuum_time"),110);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Run_number"),60);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Resister"),50);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("machine_type"),100);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Lot_id"),80);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Wafer_count"),50);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Pad_thin"),80);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Wlp_thin"),80);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Wafer_angle"),80);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Input_thin_data"),150);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Recipe"),150);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("ashing1"),65);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("ashing2"),65);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("ashing3"),65);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("ashing_machine"),80);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("N2_Blow"),80);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Au_f_add"),80);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Now_r_weight"),120);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Au_after_weight"),140);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Au_use"),50);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Ti_add"),50);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Al_add"),50);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("X_tal_number"),50);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("X_tal_Hz"),50);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("X_tal_life"),80);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("User_name"),80);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("D_use"),80);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("note"),140);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Ti_total"),60);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Al_total"),60);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Au_total"),60);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("Input_time"),120);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("machine_name"),50);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("machine_code"),50);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("batch_id"),50);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("cleaning_cal_flag"),50);
    ui->t16_data_table->horizontalHeader()->resizeSection(t16_data_model->fieldIndex("dom"),50);
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("X_tal_life"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("machine_code"));
//    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("batch_id"));
//    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("cleaning_cal_flag"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("Au_use"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("au_pellet_output"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("au_add"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("check_user"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("au_pellet_total"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("work_start_au"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("au_add_weight"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("au_case_weight"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("check_NG"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("Run_number"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("Pad_thin"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("Wlp_thin"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("N2_Blow"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("D_use"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("Ti_add"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("Al_add"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("X_tal_life"));
    ui->t16_data_table->horizontalHeader()->hideSection(t16_data_model->fieldIndex("Resister"));

    ui->t16_data_table->scrollToBottom();


    ui->t16_spec_table->setModel(spec_model);
    ui->t16_spec_table->horizontalHeader()->hideSection(0);
    ui->t16_spec_table->horizontalHeader()->hideSection(1);
    ui->t16_spec_table->horizontalHeader()->hideSection(2);
    ui->t16_spec_table->horizontalHeader()->hideSection(3);
    ui->t16_spec_table->horizontalHeader()->hideSection(4);
    ui->t16_spec_table->horizontalHeader()->hideSection(5);
    ui->t16_spec_table->horizontalHeader()->hideSection(6);
    ui->t16_spec_table->horizontalHeader()->hideSection(7);
    ui->t16_spec_table->horizontalHeader()->hideSection(8);
    ui->t16_spec_table->horizontalHeader()->hideSection(9);
    ui->t16_spec_table->horizontalHeader()->hideSection(10);
    ui->t16_spec_table->horizontalHeader()->hideSection(11);
    ui->t16_spec_table->horizontalHeader()->hideSection(12);
    ui->t16_spec_table->horizontalHeader()->hideSection(13);
    ui->t16_spec_table->horizontalHeader()->hideSection(14);
    ui->t16_spec_table->horizontalHeader()->hideSection(17);
    ui->t16_spec_table->horizontalHeader()->hideSection(18);
    ui->t16_spec_table->horizontalHeader()->hideSection(19);
    ui->t16_spec_table->horizontalHeader()->hideSection(20);
    ui->t16_spec_table->horizontalHeader()->hideSection(21);
    ui->t16_spec_table->horizontalHeader()->hideSection(22);
    ui->t16_spec_table->horizontalHeader()->hideSection(23);
    ui->t16_spec_table->horizontalHeader()->hideSection(24);
    ui->t16_spec_table->horizontalHeader()->hideSection(25);
    ui->t16_spec_table->horizontalHeader()->hideSection(26);
    ui->t16_spec_table->horizontalHeader()->hideSection(27);
    ui->t16_spec_table->horizontalHeader()->hideSection(28);
    ui->t16_spec_table->horizontalHeader()->hideSection(29);
    ui->t16_spec_table->horizontalHeader()->hideSection(30);
    ui->t16_spec_table->horizontalHeader()->hideSection(31);
    ui->t16_spec_table->horizontalHeader()->hideSection(32);
    ui->t16_spec_table->horizontalHeader()->hideSection(33);
    ui->t16_spec_table->horizontalHeader()->hideSection(34);
    ui->t16_spec_table->horizontalHeader()->hideSection(35);
    ui->t16_spec_table->horizontalHeader()->resizeSection(6,150);
    ui->t16_spec_table->horizontalHeader()->resizeSection(7,150);

    for(int i=0;i<8;i++){
        ui->t16_input_table->insertColumn(i);
    }
    ui->t16_input_table->insertRow(0);
    ui->t16_input_table->insertRow(1);
    ui->t16_input_table->insertRow(2);
    ui->t16_input_table->insertRow(3);
    ui->t16_input_table->insertRow(4);
    ui->t16_input_table->insertRow(5);
    ui->t16_input_table->insertRow(6);
    ui->t16_input_table->horizontalHeader()->resizeSection(0,70);
    ui->t16_input_table->horizontalHeader()->resizeSection(1,150);
    ui->t16_input_table->horizontalHeader()->resizeSection(3,70);
    ui->t16_input_table->horizontalHeader()->resizeSection(5,130);
    ui->t16_input_table->horizontalHeader()->resizeSection(6,140);
    ui->t16_input_table->horizontalHeader()->resizeSection(7,80);
    t16_input_item = new input_item(my_mesdb,"MD048");
    ui->t16_input_table->setCellWidget(0,0,t16_input_item->ui->LA_day_and_night);
    ui->t16_input_table->setCellWidget(1,0,t16_input_item->ui->CB_day_and_night);
    ui->t16_input_table->setCellWidget(0,1,t16_input_item->ui->LA_vacuum_time);
    ui->t16_input_table->setCellWidget(1,1,t16_input_item->ui->DT_vacuum_time);
    t16_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
//    ui->t16_input_table->setCellWidget(0,2,t16_input_item->ui->LA_resister);
//    ui->t16_input_table->setCellWidget(1,2,t16_input_item->ui->LE_resister);
//    ui->t16_input_table->setCellWidget(0,3,t16_input_item->ui->LA_N2_blow);
//    ui->t16_input_table->setCellWidget(1,3,t16_input_item->ui->WCB_N2_blow);
    ui->t16_input_table->setCellWidget(0,4,t16_input_item->ui->LA_au_f_add);
    ui->t16_input_table->setCellWidget(1,4,t16_input_item->ui->LE_au_f_add);
    ui->t16_input_table->setCellWidget(0,5,t16_input_item->ui->LA_now_r_weight);
    ui->t16_input_table->setCellWidget(1,5,t16_input_item->ui->LE_now_r_weight);
    ui->t16_input_table->setCellWidget(0,6,t16_input_item->ui->LA_au_after_r_weight);
    ui->t16_input_table->setCellWidget(1,6,t16_input_item->ui->LE_au_after_r_weight);
    ui->t16_input_table->setCellWidget(0,7,t16_input_item->ui->LA_au_after_weight);
    ui->t16_input_table->setCellWidget(1,7,t16_input_item->ui->LE_au_after_weight);
//    ui->t16_input_table->setCellWidget(0,7,t16_input_item->ui->LA_au_use);
//    ui->t16_input_table->setCellWidget(1,7,t16_input_item->ui->LE_au_use);
//    ui->t16_input_table->setCellWidget(2,0,t16_input_item->ui->LA_ti_add);
//    ui->t16_input_table->setCellWidget(3,0,t16_input_item->ui->LE_ti_add);
//    ui->t16_input_table->setCellWidget(2,1,t16_input_item->ui->LA_al_add);
//    ui->t16_input_table->setCellWidget(3,1,t16_input_item->ui->LE_al_add);
//    ui->t16_input_table->setCellWidget(2,2,t16_input_item->ui->LA_xtal_life);
//    ui->t16_input_table->setCellWidget(3,2,t16_input_item->ui->LE_xtal_life);
    ui->t16_input_table->setCellWidget(2,2,t16_input_item->ui->W_X_TAL);
    ui->t16_input_table->setCellWidget(2,3,t16_input_item->ui->LA_name);
    ui->t16_input_table->setCellWidget(3,3,t16_input_item->ui->LE_name);
//    ui->t16_input_table->setCellWidget(2,4,t16_input_item->ui->LA_d_run);
//    ui->t16_input_table->setCellWidget(3,4,t16_input_item->ui->WCB_d_run);
    ui->t16_input_table->setCellWidget(2,5,t16_input_item->ui->LA_note);
    ui->t16_input_table->setCellWidget(3,5,t16_input_item->ui->LE_note);
    ui->t16_input_table->setSpan(2,2,2,1);
    ui->t16_input_table->setSpan(2,5,1,5);
    ui->t16_input_table->setSpan(3,5,1,5);
    ui->t16_input_table->setCellWidget(4,0,t16_input_item->ui->LE_recipe_number);
    ui->t16_input_table->setCellWidget(4,1,t16_input_item->ui->CB_recipe_choice);
//    ui->t16_input_table->setCellWidget(4,2,t16_input_item->ui->LA_recipe_2);
    ui->t16_input_table->setCellWidget(5,0,t16_input_item->ui->W_recipe);
    ui->t16_input_table->setSpan(4,1,1,5);
//    ui->t16_input_table->setSpan(4,2,1,3);
    ui->t16_input_table->setSpan(5,0,2,5);
    ui->t16_input_table->setCellWidget(4,6,t16_input_item->ui->LA_dom_number);
    ui->t16_input_table->setCellWidget(5,6,t16_input_item->ui->CB_domnumber);

    ui->t16_LE_Lot_count->setValidator(new QIntValidator(0, 1000, this));
//    ui->t16_LE_Run_number->setValidator(new QIntValidator(0, 1000, this));
    QSqlQuery query(my_mesdb);
    query.exec(QString("select `Lot_max_count`,`Run_number_count` from Thin_film_spec_managerment where machine_code = 'MD048'"));
    if(query.next()){
        ui->t16_LE_Lot_count->setText(query.value("Lot_max_count").toString());
//        ui->t16_LE_Run_number->setText(query.value("Run_number_count").toString());
    }
    t16_lot_item_list.clear();
    for(int i=0;i<ui->t16_LE_Lot_count->text().toInt();i++){

        input_lot_item *lot_item = new input_lot_item(ms_mesdb,t16_input_item,"MD048");
        t16_lot_item_list.append(lot_item);
        ui->t16_input_lot_table->insertRow(i);
        ui->t16_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
//        ui->t1_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
        ui->t16_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
        ui->t16_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
        ui->t16_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
        ui->t16_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
        ui->t16_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
        ui->t16_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
        ui->t16_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
//        connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
//                t16_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
    }
    query.exec("SELECT * FROM `Thin_film_repcipe` where machine_code = 'MD048' ORDER BY `recipe_number` ASC  ");
    t16_input_item->ui->CB_recipe_choice->addItem(tr("recipe_choice"));
    while(query.next()){
        QString recipe_name = query.value("recipe_number").toString()+"."+ query.value("recipename").toString()
                                + "|"+query.value("metal_name").toString()+"|"+query.value("metal_thin").toString()+"|"
                                +query.value("type").toString();
        QPixmap pixmap(100,100);
        pixmap.fill(QColor(query.value("color").toString()));
        t16_input_item->ui->CB_recipe_choice->addItem(QIcon(pixmap),recipe_name);
    }
    connect(t16_input_item->ui->CB_recipe_choice,SIGNAL(currentIndexChanged(int)),this,SLOT(t16_recipe_choice(int)));

    t16_ti_chart = new Thin_film_chart();
    t16_ti_chartview = new Thin_film_chart_view(t16_ti_chart);
    connect(t16_ti_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t16_ti_y_value_slot(QPointF)));
    ui->t16_Ti_layout->addWidget(t16_ti_chartview);

    t16_al_chart = new Thin_film_chart();
    t16_al_chartview = new Thin_film_chart_view(t16_al_chart);
    connect(t16_al_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t16_al_y_value_slot(QPointF)));
    ui->t16_Al_layout->addWidget(t16_al_chartview);
    t16_ti_axisX = new QDateTimeAxis();
    t16_ti_axisX->setFormat("MM-dd HH:mm:ss");
    t16_ti_axisX->setTitleText("Date");
    t16_ti_axisX->setVisible(true);
    t16_ti_axisX->setTitleVisible(true);
    t16_ti_chart->addAxis(t16_ti_axisX, Qt::AlignBottom);
    t16_ti_axisY = new QValueAxis();
    t16_ti_chart->addAxis(t16_ti_axisY,Qt::AlignLeft);

    t16_al_axisX = new QDateTimeAxis();
    t16_al_axisX->setFormat("MM-dd HH:mm:ss");
    t16_al_axisX->setTitleText("Date");
    t16_al_axisX->setVisible(true);
    t16_al_axisX->setTitleVisible(true);
    t16_al_chart->addAxis(t16_al_axisX, Qt::AlignBottom);
    t16_al_axisY = new QValueAxis();
    t16_al_chart->addAxis(t16_al_axisY,Qt::AlignLeft);

    ti_al_chart_init(t16_ti_chart,t16_al_chart,t16_ti_value_series,t16_al_value_series,t16_ti_UCL_value_series,t16_al_UCL_value_series,
                     t16_al_axisX,t16_al_axisY,t16_ti_axisX,t16_ti_axisY,"MD048");
    query.exec("SELECT machine_name,machine_code FROM Thin_film_spec_managerment");
    while(query.next()){
        ui->t16_CB_move_machine->addItem(query.value("machine_name").toString());
    }
    temp_run_widget_map.insert("MD048",ui->t16_temp_run_mode_time);
    temp_run_ti_accmulate_widget_map.insert("MD048",ui->t16_ti_accmulate);
    temp_run_al_accmulate_widget_map.insert("MD048",ui->t16_al_accmulate);
    ui->t16_CB_wet_insert->addItems(wet_machine_list);
    sync_now_date("MD048",t16_input_item->ui->LE_au_f_add,t16_input_item->ui->LE_now_r_weight,t16_input_item->ui->LE_au_after_r_weight
                  ,t16_input_item->ui->LE_au_after_weight);
    ui->t16_data_table->selectionModel()->setObjectName("t16_data");
    connect(ui->t16_data_table->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this,SLOT(table_view_selection_change(QModelIndex,QModelIndex)));
}
void Thin_film_mainwindows::t16_recipe_choice(int idx)
{
    if(idx == 0){
        t16_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t16_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t16_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t16_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t16_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t16_input_item->ui->LE_Recipe_name1->setText("");
        t16_input_item->ui->LE_Recipe_name2->setText("");
        t16_input_item->ui->LE_Recipe_name3->setText("");
        t16_input_item->ui->LE_Recipe_name4->setText("");
        t16_input_item->ui->LE_Recipe_name5->setText("");
        return ;
    }
    QSqlQuery query(my_mesdb);
    query.exec(QString("select * from `Thin_film_repcipe` where recipe_number  = %1 AND machine_code = 'MD048'").arg(idx));

    if(query.next()){
        t16_input_item->ui->CB_Recipe_name1->setCurrentIndex(0);
        t16_input_item->ui->CB_Recipe_name2->setCurrentIndex(0);
        t16_input_item->ui->CB_Recipe_name3->setCurrentIndex(0);
        t16_input_item->ui->CB_Recipe_name4->setCurrentIndex(0);
        t16_input_item->ui->CB_Recipe_name5->setCurrentIndex(0);
        t16_input_item->ui->LE_Recipe_name1->setText("");
        t16_input_item->ui->LE_Recipe_name2->setText("");
        t16_input_item->ui->LE_Recipe_name3->setText("");
        t16_input_item->ui->LE_Recipe_name4->setText("");
        t16_input_item->ui->LE_Recipe_name5->setText("");
        if(query.value("metal_name").toString() != ""){
            QString item_data = query.value("metal_name").toString();
            QStringList item_list = item_data.split("/");
            for(int i=0;i<item_list.count();i++){
                if(i==0){
                    t16_input_item->ui->CB_Recipe_name1->setCurrentText(item_list.at(i));
                }else if(i==1){
                    t16_input_item->ui->CB_Recipe_name2->setCurrentText(item_list.at(i));
                }else if(i==2){
                    t16_input_item->ui->CB_Recipe_name3->setCurrentText(item_list.at(i));
                }else if(i==3){
                    t16_input_item->ui->CB_Recipe_name4->setCurrentText(item_list.at(i));
                }else if(i==4){
                    t16_input_item->ui->CB_Recipe_name5->setCurrentText(item_list.at(i));
                }
            }
        }
        if(query.value("metal_thin").toString() != ""){
            QStringList recipe_item = query.value("metal_thin").toString().split("/");
            if(recipe_item.count()>0){
                t16_input_item->ui->LE_Recipe_name1->setText(QString("%1").arg(recipe_item[0]));
            }
            if(recipe_item.count()>1){
                t16_input_item->ui->LE_Recipe_name2->setText(QString("%1").arg(recipe_item[1]));
            }
            if(recipe_item.count()>2){
                t16_input_item->ui->LE_Recipe_name3->setText(QString("%1").arg(recipe_item[2]));
            }
            if(recipe_item.count()>3){
                t16_input_item->ui->LE_Recipe_name4->setText(QString("%1").arg(recipe_item[3]));
            }
            if(recipe_item.count()>4){
                t16_input_item->ui->LE_Recipe_name5->setText(QString("%1").arg(recipe_item[4]));
            }
        }
    }
}

void Thin_film_mainwindows::t16_ti_y_value_slot(QPointF value)
{
    ui->t16_ti_y_value->setText(QString("%1").arg(value.y()));
}

void Thin_film_mainwindows::t16_al_y_value_slot(QPointF value)
{
    ui->t16_al_y_value->setText(QString("%1").arg(value.y()));
}

void Thin_film_mainwindows::t16_datamodel_dataChanged(QModelIndex index1, QModelIndex index2, QVector<int> vector)
{
    v1_data_model_change(index1,t16_data_model,"MD048");
    ui->t16_data_table->scrollToBottom();
}


void Thin_film_mainwindows::on_t16_LE_Lot_count_editingFinished()
{

}
void Thin_film_mainwindows::on_t16_LE_Run_number_editingFinished()
{

}
void Thin_film_mainwindows::on_t16_Input_btn_clicked()
{

      QString Lot_count = ui->t16_LE_Lot_count->text();
      QString resister = t16_input_item->ui->LE_resister->text();
      QString au_f_add = t16_input_item->ui->LE_au_f_add->text();
      QString now_r_weight = t16_input_item->ui->LE_now_r_weight->text();
      QString au_after_r_weight = t16_input_item->ui->LE_au_after_r_weight->text();
      QString au_use =  t16_input_item->ui->LE_au_use->text();
      QString ti_add =  t16_input_item->ui->LE_ti_add->text();
      QString al_add =  t16_input_item->ui->LE_al_add->text();
      QString xtal_life =  t16_input_item->ui->LE_xtal_life->text();
      QString name =  t16_input_item->ui->LE_name->text();
      QString X_tal_number = t16_input_item->ui->LE_Xtal_number->text();
      QString X_tal_Hz = t16_input_item->ui->LE_Xtal_hz->text();

      QString note =  t16_input_item->ui->LE_note->text();

      ui->t16_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
      ui->t16_search_time_start->setDateTime(ui->t16_search_time_end->dateTime().addDays(-2));

      QString Recipe_total_text;
      QString Recipe_total_name_text;
      QString Recipe_name1 = t16_input_item->ui->LE_Recipe_name1->text();
      QString Recipe_name2 = t16_input_item->ui->LE_Recipe_name2->text();
      QString Recipe_name3 = t16_input_item->ui->LE_Recipe_name3->text();
      QString Recipe_name4 = t16_input_item->ui->LE_Recipe_name4->text();
      QString Recipe_name5 = t16_input_item->ui->LE_Recipe_name5->text();
      QString CB_Recipe_name1 = t16_input_item->ui->CB_Recipe_name1->currentText();
      QString CB_Recipe_name2 = t16_input_item->ui->CB_Recipe_name2->currentText();
      QString CB_Recipe_name3 = t16_input_item->ui->CB_Recipe_name3->currentText();
      QString CB_Recipe_name4 = t16_input_item->ui->CB_Recipe_name4->currentText();
      QString CB_Recipe_name5 = t16_input_item->ui->CB_Recipe_name5->currentText();
      int Recipe_count = 0;
      int ti_count = 0;
      int au_count = 0;
      int al_count = 0;

      if(CB_Recipe_name1 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name1));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name1));
          if(CB_Recipe_name1=="Ti"){
              ti_count += Recipe_name1.toInt();
          }else if(CB_Recipe_name1=="Al"){
              al_count += Recipe_name1.toInt();
          }else if(CB_Recipe_name1=="Au"){
              au_count += Recipe_name1.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name2 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name2));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name2));
          if(CB_Recipe_name2=="Ti"){
              ti_count += Recipe_name2.toInt();
          }else if(CB_Recipe_name2=="Al"){
              al_count += Recipe_name2.toInt();
          }else if(CB_Recipe_name2=="Au"){
              au_count += Recipe_name2.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name3 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name3));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name3));
          if(CB_Recipe_name3=="Ti"){
              ti_count += Recipe_name3.toInt();
          }else if(CB_Recipe_name3=="Al"){
              al_count += Recipe_name3.toInt();
          }else if(CB_Recipe_name3=="Au"){
              au_count += Recipe_name3.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name4 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name4));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name4));
          if(CB_Recipe_name4=="Ti"){
              ti_count += Recipe_name4.toInt();
          }else if(CB_Recipe_name4=="Al"){
              al_count += Recipe_name4.toInt();
          }else if(CB_Recipe_name4=="Au"){
              au_count += Recipe_name4.toInt();
          }
          Recipe_count++;
      }
      if(CB_Recipe_name5 != ""){
          Recipe_total_text.append(QString("%1/").arg(Recipe_name5));
          Recipe_total_name_text.append(QString("%1/").arg(CB_Recipe_name5));
          if(CB_Recipe_name5=="Ti"){
              ti_count += Recipe_name5.toInt();
          }else if(CB_Recipe_name5=="Al"){
              al_count += Recipe_name5.toInt();
          }else if(CB_Recipe_name5=="Au"){
              au_count += Recipe_name5.toInt();
          }
          Recipe_count++;
      }
      if(Recipe_count>0){
          Recipe_total_text.remove(Recipe_total_text.length()-1,1);
          Recipe_total_name_text.remove(Recipe_total_name_text.length()-1,1);
      }
      QSqlQuery query(my_mesdb);
      QString batch_id;
      query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_code = 'MD048'"));
      if(query.next()){
          batch_id = query.value("current_batch_id").toString();
          query.exec("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+1  where machine_code = 'MD048'");
      }
      bool query_result = false;
      qDebug()<<ui->t16_input_lot_table->rowCount();
      query.exec("select now() as now_time");
      QDateTime now_time;

      now_time = QDateTime::currentDateTime();
      for(int i=0;i<ui->t16_input_lot_table->rowCount();i++){
          QString Lot_id = t16_lot_item_list.at(i)->ui->LE_Lot_id->text();
          QString wafer_count = t16_lot_item_list.at(i)->ui->LE_wafer_count->text();
          QString ashing1 = t16_lot_item_list.at(i)->ui->LE_ashing1->text();
          QString ashing2 = t16_lot_item_list.at(i)->ui->LE_ashing2->text();
          QString ashing3 = t16_lot_item_list.at(i)->ui->LE_ashing3->text();
          QString ashing_machine = t16_lot_item_list.at(i)->ui->LE_ashing_machine->text();
          QString machine_type = t16_lot_item_list.at(i)->ui->LE_machine_type->text();
          QString wafer_angle = t16_lot_item_list.at(i)->ui->LE_wafer_angle->text();

          if(Lot_id.trimmed() !=""){
              QString query_str = QString("INSERT INTO `Thin_film_data` "
                                          "(`Run_number`, `Resister`, `machine_type`, `Lot_id`, `Wafer_count`, "
                                          "`Wafer_angle`, `Input_thin_data`, `ashing1`, `ashing2`, "
                                          "`ashing3`, `ashing_machine`, `Pad_thin`, `Wlp_thin`, "
                                          "`N2_Blow`, `Au_f_add`, `Now_r_weight`, `Au_after_weight`, "
                                          "`Au_use`, `Recipe`, `Ti_add`, `Al_add`, `X_tal_number`,`X_tal_Hz`, "
                                          "`X_tal_life`, `User_name`, `D_use`, `note`, `Ti_total`, "
                                          "`Al_total`, `Au_total`, `batch_id`, `cleaning_cal_flag`,`machine_code`,`machine_name`,"
                                          "`Input_time`,`vacuum_time`,`day_and_night`,`au_pellet_output`,`au_add_weight`,`dom`) "
                                          "VALUES "
                                          "('%1', '%2', '%3', '%4', '%5', '%6', "
                                          "'%7', '%8', '%9', '%10', "
                                          "'%11', '%12', '%13', '%14', '%15', '%16', "
                                          "'%17', '%18', '%19', '%20', '%21', '%22','%23', "
                                          "'%24', '%25', '%26', '%27', '%28', "
                                          "'%29', '%30', '%31' ,'%32','%33','%34','%35','%36','%37','%38','%39','%40');").arg("").arg(resister).arg(machine_type).arg(Lot_id).arg(wafer_count)
                                                                        .arg(wafer_angle).arg(Recipe_total_text).arg(ashing1).arg(ashing2)
                                                                        .arg(ashing3).arg(ashing_machine).arg(QString("0")).arg(QString("0"))
                                                                        .arg("").arg(au_f_add).arg(now_r_weight).arg(au_after_r_weight)
                                                                        .arg(au_use).arg(Recipe_total_name_text).arg(ti_add).arg(al_add).arg(X_tal_number).arg(X_tal_Hz)
                                                                        .arg(xtal_life).arg(name).arg("").arg(note).arg(ti_count)
                                                                        .arg(al_count).arg(au_count).arg(batch_id).arg(QString("0")).arg("MD048").arg("hanil#4")
                                                                        .arg(now_time.toString("yyyy-MM-dd hh:mm:ss"))
                                                                        .arg(t16_input_item->ui->DT_vacuum_time->dateTime().toString("yyyy-MM-dd hh:mm:00"))
                                                                        .arg(t16_input_item->ui->CB_day_and_night->currentText())
                                                                        .arg(t16_input_item->ui->DT_vacuum_time->dateTime().addSecs(-300).toString("yyyy-MM-dd hh:mm:00"))
                                                                        .arg(t16_input_item->ui->LE_au_after_weight->text())
                                                                        .arg(t16_input_item->ui->CB_domnumber->currentText());
              query_result = query.exec(query_str);

              if(!query_result){
                  break;
              }

          }
      }
      au_calc(now_time);
      if(query_result){
          t16_input_item->clear_data();
          for(int i=0;i<ui->t16_input_lot_table->rowCount();i++){
              t16_lot_item_list.at(i)->clear_data();
          }
          QMessageBox msg;
          msg.addButton(QMessageBox::Ok);
          msg.setText(tr("input complete"));
          msg.exec();
          on_t16_LE_Run_number_editingFinished();
          ui->t16_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
          ui->t16_search_time_start->setDateTime(ui->t16_search_time_end->dateTime().addDays(-2));
          t16_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
          on_t16_search_btn_clicked();
          ui->t16_data_table->scrollToBottom();
      }else {
          QMessageBox::warning(this, tr("conntion false"),
                                                         "server connection fail\n"
                                                            ""+my_mesdb.lastError().text(),
                                                              QMessageBox::Close);
      }
      query.exec(QString("select * from depostion_au_use_current_view where machine_name = '%1' AND batch_id = %2")
                 .arg("hanil#4").arg(batch_id));
      if(query.next()){
          double  OKNG = query.value("OKNG").toDouble();
          if(abs(OKNG)>0.1){
              QMessageBox msg;
              msg.addButton(QMessageBox::Ok);
              msg.setText(tr("au pellet NG"));
              msg.exec();
          }
      }
      query.exec("update `Thin_film_spec_managerment` set au_pullet_add = 0 ,now_r_weight = 0,au_after_r_weight =0,au_after_weight=0 where machine_code = 'MD048'");
      ti_al_chart_init(t16_ti_chart,t16_al_chart,t16_ti_value_series,t16_al_value_series,t16_ti_UCL_value_series,t16_al_UCL_value_series,
                       t16_al_axisX,t16_al_axisY,t16_ti_axisX,t16_ti_axisY,"MD048");
}
void Thin_film_mainwindows::on_t16_search_btn_clicked()
{
    t16_data_model->setFilter(QString("vacuum_time Between '%1' AND '%2' AND machine_code = 'MD048'")
                          .arg(ui->t16_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                          .arg(ui->t16_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    t16_data_model->select();
    t16_data_model->setSort(2,Qt::AscendingOrder);
    QSqlQuery query(my_mesdb);
    query.exec(QString("select sum((case when (`A`.`machine_code` = 'MD048') then `A`.`Wafer_count` end)) AS `wafer` from `Thin_film_data` `A` "
               "where vacuum_time Between '%1' AND '%2' AND machine_code = 'MD048'").arg(ui->t16_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
               .arg(ui->t16_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

    if(query.next()){

        ui->t16_total_wafer->setText(query.value("wafer").toString());
    }
    ui->t16_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t16_BT_search_lot_id_clicked()
{
    t16_data_model->setFilter(QString("Lot_id = '%1' AND machine_code = 'MD048'")
                          .arg(ui->t4_LE_search_lot_id->text()));
    t16_data_model->select();
    t16_data_model->setSort(1,Qt::AscendingOrder);
}
void Thin_film_mainwindows::on_t16_DT_del_btn_clicked()
{
    int count = ui->t16_data_table->selectionModel()->selectedIndexes().count();
    QVector<int> index_history;
    for(int i=0;i<count;i++){
        if(!index_history.contains(ui->t16_data_table->selectionModel()->selectedIndexes().at(i).row())){
        QDateTime Input_time2 = t16_data_model->record(ui->t16_data_table->selectionModel()->selectedIndexes().at(i).row()).value("Input_time").toDateTime();
        t16_data_model->removeRow(ui->t16_data_table->selectionModel()->selectedIndexes().at(i).row());
         au_calc(Input_time2);
         index_history.append(ui->t16_data_table->selectionModel()->selectedIndexes().at(i).row());
        }else {

        }
    }
    on_t16_search_btn_clicked();
    ui->t16_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t16_move_machine_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    QString batch_id_temp = "0";
    QString machine_name;
    QString machine_code;

    int count  = ui->t16_data_table->selectionModel()->selectedIndexes().count();
    for(int i=0;i<count;i++){
         QSqlRecord recode = t16_data_model->record(ui->t16_data_table->selectionModel()->selectedIndexes().at(i).row());
        if(batch_id_temp != recode.value("batch_id").toString()){
            query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_name = '%1'").arg(ui->t16_CB_move_machine->currentText()));
            if(query.next()){
                batch_id_temp = query.value("current_batch_id").toString()+5;
                machine_name = query.value("machine_name").toString();
                machine_code = query.value("machine_code").toString();
                query.exec(QString("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+10  where machine_name = '%1'").arg(ui->t16_CB_move_machine->currentText()));
            }
            batch_id_temp = recode.value("batch_id").toString();
        }

        recode.setGenerated("batch_id",true);
        recode.setGenerated("machine_name",true);
        recode.setGenerated("machine_code",true);
        recode.setValue("batch_id",batch_id_temp);
        recode.setValue("machine_name",machine_name);
        recode.setValue("machine_code",machine_code);
        t16_data_model->setRecord(ui->t16_data_table->selectionModel()->selectedIndexes().at(i).row(),recode);
    }
    on_t16_search_btn_clicked();
    ui->t16_data_table->scrollToBottom();
}


void Thin_film_mainwindows::on_t16_excel_header_copy_flag_toggled(bool checked)
{
    ui->t16_data_table->copyheader_flag = checked;
}


void Thin_film_mainwindows::on_t16_machine_temp_run_mode_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    query.exec("update Thin_film_spec_managerment set nomal_mode_time = ADDTIME(NOW(),'0:5:0') where `machine_code`='MD048'");
}



//t16_function -- end --


//t8_function -- start --
void Thin_film_mainwindows::t8_init()
{
    t8_data_model = new thin_pellet_sqltable_model(this,my_mesdb);
    t8_data_model->setTable("depostion_au_use_current_view");
    ui->t8_start_search_time->setDateTime(QDateTime::currentDateTime().addDays(-2));
    ui->t8_end_search_time->setDateTime(QDateTime::currentDateTime().addSecs(30));
    ui->t8_data_table->setModel(t8_data_model);
    t8_data_model->setSort(t8_data_model->fieldIndex("Input_time"),Qt::AscendingOrder);
    au_chart = new Thin_film_au_chart();
    au_chart_view  = new Thin_film_au_chart_view(au_chart);
    ui->t8_au_chart_layout->addWidget(au_chart_view);
    connect(t8_data_model,SIGNAL(search()),this,SLOT(t8_fast_search()));


    on_t8_search_btn_clicked();

    t8_data_model->setHeaderData(t8_data_model->fieldIndex("day_and_night"),Qt::Horizontal,tr("day_and_night"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("au_pellet_output"),Qt::Horizontal,tr("au_pellet_output"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("vacuum_time"),Qt::Horizontal,tr("au_pellet_input"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("au_add"),Qt::Horizontal,tr("au_add"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("CHA#1"),Qt::Horizontal,tr("CHA#1"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("CHA#2"),Qt::Horizontal,tr("CHA#2"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("CHA#3"),Qt::Horizontal,tr("CHA#3"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("CHA#4"),Qt::Horizontal,tr("CHA#4"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("BPS"),Qt::Horizontal,tr("BPS"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("EVATEC"),Qt::Horizontal,tr("EVATEC"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("hanil"),Qt::Horizontal,tr("hanil"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("au_pellet_total"),Qt::Horizontal,tr("au_pellet_total"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("work_start_au"),Qt::Horizontal,tr("work_start_au"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("au_add_weight"),Qt::Horizontal,tr("au_add_weight"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("work_finish_au"),Qt::Horizontal,tr("work_finish_au"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("check_user"),Qt::Horizontal,tr("check_user"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("diff"),Qt::Horizontal,tr("diff"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("Input_time"),Qt::Horizontal,tr("thin_Input_time"));
    t8_data_model->setHeaderData(t8_data_model->fieldIndex("machine_name"),Qt::Horizontal,tr("machine_name"));

    ui->t8_data_table->copyheader_flag = true;
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("day_and_night"),30);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("au_pellet_output"),130);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("vacuum_time"),130);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("au_add"),60);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("CHA#1"),60);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("CHA#2"),60);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("CHA#3"),60);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("CHA#4"),60);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("BPS"),60);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("EVATEC"),60);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("hanil"),60);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("au_pellet_total"),100);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("work_start_au"),170);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("au_add_weight"),100);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("work_finish_au"),170);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("check_user"),60);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("diff"),60);
    ui->t8_data_table->horizontalHeader()->resizeSection(t8_data_model->fieldIndex("Input_time"),140);



    ui->t8_data_table->setItemDelegateForColumn(t8_data_model->fieldIndex("au_pellet_output"),new DateDelegate(ui->t8_data_table));
    ui->t8_data_table->setItemDelegateForColumn(t8_data_model->fieldIndex("vacuum_time"),new DateDelegate(ui->t8_data_table));
    ui->t8_data_table->setItemDelegateForColumn(t8_data_model->fieldIndex("Input_time"),new DateDelegate("yyyy-MM-dd hh:mm:ss",ui->t8_data_table));
    ui->t8_data_table->horizontalHeader()->hideSection(t8_data_model->fieldIndex("batch_id"));
    t8_data_model->setProperty("progressbar",QVariant::fromValue(ui->t8_progressbar));




}
void Thin_film_mainwindows::on_t8_search_btn_clicked()
{
    t8_data_model->setFilter(QString("Input_time >= '%1' AND Input_time <= '%2'")
                             .arg(ui->t8_start_search_time->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                             .arg(ui->t8_end_search_time->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    t8_data_model->setProperty("parent_table",QVariant::fromValue(ui->t8_data_table));
    QTime start_time = QTime::currentTime();
    t8_data_model->select();
    QTime end_time = QTime::currentTime();
    qDebug()<<"fast search = "<<end_time.msecsTo(start_time);
    ui->t8_data_table->scrollToBottom();

    au_chart->removeAllSeries();
    if(au_chart->axisX() != NULL){
        au_chart->removeAxis(au_chart->axisX());
    }
    if(au_chart->axisY() != NULL){
        au_chart->removeAxis(au_chart->axisY());
    }
    au_value_series = new QLineSeries();
    for(int i=0;i<t8_data_model->rowCount();i++){
        au_value_series->append(i,t8_data_model->record(i).value("diff").toDouble());
    }
    au_chart->addSeries(au_value_series);
    au_axisX = new QValueAxis();
    au_chart->createDefaultAxes();
//    au_value_series->attachAxis(au_chart->axisX());
//    au_value_series->attachAxis(au_chart->axisY());

}
void Thin_film_mainwindows::t8_fast_search()
{
    ui->t8_start_search_time->setDateTime(QDateTime::currentDateTime().addDays(-1));
    ui->t8_end_search_time->setDateTime(QDateTime::currentDateTime().addSecs(600));
    t8_data_model->setFilter(QString("Input_time >= '%1' AND Input_time <= '%2'")
                             .arg(ui->t8_start_search_time->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                             .arg(ui->t8_end_search_time->dateTime().addDays(1).toString("yyyy-MM-dd hh:mm:ss")));
    QTime start_time = QTime::currentTime();
    t8_data_model->select();
    QTime end_time = QTime::currentTime();
    qDebug()<<"fast search = "<<end_time.msecsTo(start_time);
    ui->t8_data_table->scrollToBottom();
}
void Thin_film_mainwindows::on_t8_au_add_del_row_btn_clicked()
{
    int count = ui->t8_data_table->selectionModel()->selectedIndexes().count();
    for(int i=0;i<count;i++){
        QString machine_name = t8_data_model->record(ui->t8_data_table->selectionModel()->selectedIndexes().at(i).row())
                .value("machine_name").toString();
        int batch_id = t8_data_model->record(ui->t8_data_table->selectionModel()->selectedIndexes().at(i).row())
                .value("batch_id").toInt();
        if(machine_name == "AU"){
            QDateTime Input_Time = t8_data_model->record(ui->t8_data_table->selectionModel()->selectedIndexes().at(i).row())
                    .value("Input_time").toDateTime();
            QSqlQuery query(my_mesdb);
            query.exec(QString("DELETE FROM `FAB`.`Thin_film_data` WHERE machine_name = '%1' AND batch_id = %2")
                       .arg(machine_name).arg(batch_id));
            au_calc(Input_Time);
        }else {
            return ;
        }
    }
}
void Thin_film_mainwindows::on_t8_add_row_btn_clicked()
{

    QSqlQuery query(my_mesdb);
    QDateTime now_time;
    int batch_id = 0;
    query.exec("select now() as now_time");
    if(query.next()){
        now_time = query.value("now_time").toDateTime();
    }
    query.exec(QString("select machine_name,machine_code,current_batch_id from Thin_film_spec_managerment where machine_code = 'AU001'"));
    if(query.next()){
        batch_id = query.value("current_batch_id").toInt();
        query.exec("update Thin_film_spec_managerment set current_batch_id =  current_batch_id+1  where machine_code = 'AU001'");
    }

    query.exec(QString("insert into `Thin_film_data` (`machine_name`,`machine_code`,`batch_id`,`Input_time`,`vacuum_time`,`au_pellet_output`) "
                       " VALUES ('%1','%2',%3,'%4','%5','%6')")
               .arg("AU").arg("AU001").arg(batch_id).arg(now_time.toString("yyyy-MM-dd hh:mm:ss"))
               .arg(now_time.toString("yyyy-MM-dd hh:mm:00"))
               .arg(now_time.addSecs(-300).toString("yyyy-MM-dd hh:mm:00")));
    ui->t8_end_search_time->setDateTime(now_time.addSecs(30));
    au_calc(now_time);
    on_t8_search_btn_clicked();

}
void Thin_film_mainwindows::on_t8_up_btn_clicked()
{
    QModelIndexList index_list =  ui->t8_data_table->selectionModel()->selectedIndexes();
    QVector<int> index_history;
    QDateTime first_work_time;
    QString first_wet_machine;
    for(int i=0;i<index_list.count();i++){
        if(!index_history.contains(index_list.at(i).row())){
            if(i==0){
                first_work_time= t8_data_model->record(index_list.at(i).row()).value("work_time").toDateTime();
                first_wet_machine = t8_data_model->record(index_list.at(i).row()).value("wet_machine").toString();
            }
            QSqlQuery query(my_mesdb);

            QDateTime dst_time = t8_data_model->record(index_list.at(i).row()-1).value("Input_time").toDateTime()
                    .addSecs(-1);
            QSqlRecord row_recode = t8_data_model->record(index_list.at(i).row());
            int batch_id = row_recode.value("batch_id").toInt();
            query.exec(QString("update `Thin_film_data` set `Input_time` = '%1' where batch_id = %2 AND machine_name = '%3'")
                       .arg(dst_time.toString("yyyy-MM-dd hh:mm:ss")).arg(batch_id).arg(row_recode.value("machine_name").toString()));
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError().text();
            au_calc(dst_time);
            t8_fast_search();
        }else {

        }
    }
}
void Thin_film_mainwindows::on_t8_down_btn_clicked()
{
    QModelIndexList index_list =  ui->t8_data_table->selectionModel()->selectedIndexes();
    QVector<int> index_history;
    QDateTime first_work_time;
    QString first_wet_machine;
    for(int i=0;i<index_list.count();i++){
        if(!index_history.contains(index_list.at(i).row())){
            if(i==0){
                first_work_time= t8_data_model->record(index_list.at(i).row()).value("work_time").toDateTime();
                first_wet_machine = t8_data_model->record(index_list.at(i).row()).value("wet_machine").toString();
            }
            QSqlQuery query(my_mesdb);

            QDateTime dst_time = t8_data_model->record(index_list.at(i).row()+1).value("Input_time").toDateTime()
                    .addSecs(1);
            QSqlRecord row_recode = t8_data_model->record(index_list.at(i).row());
            int batch_id = row_recode.value("batch_id").toInt();
            query.exec(QString("update `Thin_film_data` set `Input_time` = '%1' where batch_id = %2 AND machine_name = '%3'")
                       .arg(dst_time.toString("yyyy-MM-dd hh:mm:ss")).arg(batch_id).arg(row_recode.value("machine_name").toString()));
            au_calc(row_recode.value("Input_time").toDateTime());
            t8_fast_search();
        }else {

        }
    }
}

void Thin_film_mainwindows::temprunmode_gui_slot(QString value, QLineEdit *view)
{
    view->setText(value);
}
//t8_function -- end --


void Thin_film_mainwindows::t13_init()
{
    ui->t13_search_time_end->setDateTime(QDateTime::currentDateTime().addSecs(30));
       ui->t13_search_time_start->setDateTime(ui->t13_search_time_end->dateTime().addDays(-2));

       QSqlTableModel *spec_model = new QSqlTableModel(this,my_mesdb);
       spec_model->setTable("Thin_film_spec_managerment");
       spec_model->setFilter("machine_code = 'MD042'");
       spec_model->select();
       spec_model->setHeaderData(4,Qt::Horizontal,tr("ashing_LCL"));
       spec_model->setHeaderData(5,Qt::Horizontal,tr("ashing_UCL"));
       spec_model->setHeaderData(6,Qt::Horizontal,tr("WLPTiAlashing_LCL"));
       spec_model->setHeaderData(7,Qt::Horizontal,tr("WLPTiAlashing_UCL"));
       spec_model->setHeaderData(8,Qt::Horizontal,tr("WLP_LCL_Ti_Au"));
       spec_model->setHeaderData(9,Qt::Horizontal,tr("WLP_UCL_Ti_Au"));
       spec_model->setHeaderData(10,Qt::Horizontal,tr("WLP_LCL_Ti_Al"));
       spec_model->setHeaderData(11,Qt::Horizontal,tr("WLP_UCL_Ti_Al"));
       spec_model->setEditStrategy(QSqlTableModel::OnManualSubmit);

       t13_data_model = new thin_sqltable_model(this,my_mesdb,"MD042",spec_model);
   //    QSqlTableModel *data_model = new QSqlTableModel(this,my_mesdb);
       t13_data_model->setTable("Thin_film_data");

       t13_data_model->setFilter(QString("Input_time Between '%1' AND '%2' AND machine_code = 'MD042'")
                             .arg(ui->t13_search_time_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                             .arg(ui->t13_search_time_end->dateTime().toString("yyyy-MM-dd hh:mm:ss")));

       ui->t13_data_table->setItemDelegateForColumn(t13_data_model->fieldIndex("vacuum_time"),new DateDelegate(ui->t13_data_table));
       ui->t13_data_table->setItemDelegateForColumn(t13_data_model->fieldIndex("Input_time"),new DateDelegate(ui->t13_data_table));
       ui->t13_data_table->setModel(t13_data_model);
       ui->t13_data_table->setSortingEnabled(true);
       connect(t13_data_model,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>))
               ,this,SLOT(t13_datamodel_dataChanged(QModelIndex,QModelIndex,QVector<int>)));

       ui->t13_data_table->setItemDelegateForColumn(10,new Thin_combodelegate(note_combo_list,ui->t13_data_table));
       t13_data_model->setEditStrategy(QSqlTableModel::OnFieldChange);
       t13_data_model->setSort(2,Qt::AscendingOrder);

       t13_data_model->select();

       wafer_count_index = t13_data_model->fieldIndex("Wafer_count");

       t13_data_model->setHeaderData(t13_data_model->fieldIndex("day_and_night"),Qt::Horizontal,tr("day_and_night"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("vacuum_time"),Qt::Horizontal,tr("vacuum_time"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Run_number"),Qt::Horizontal,tr("thin_Run_number"));
//       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Resister"),Qt::Horizontal,tr("thin_Resister"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("machine_type"),Qt::Horizontal,tr("thin_machine_type"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Lot_id"),Qt::Horizontal,tr("thin_Lot_id"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Wafer_count"),Qt::Horizontal,tr("thin_Wafer_count"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Pad_thin"),Qt::Horizontal,tr("thin_Pad_thin"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Wlp_thin"),Qt::Horizontal,tr("thin_Wlp_thin"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Wafer_angle"),Qt::Horizontal,tr("thin_Wafer_angle"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Input_thin_data"),Qt::Horizontal,tr("thin_Input_thin_data"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Recipe"),Qt::Horizontal,tr("thin_Recipe"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("ashing1"),Qt::Horizontal,tr("thin_ashing1"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("ashing2"),Qt::Horizontal,tr("thin_ashing2"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("ashing3"),Qt::Horizontal,tr("thin_ashing3"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("ashing_machine"),Qt::Horizontal,tr("thin_ashing_machine"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("N2_Blow"),Qt::Horizontal,tr("thin_N2_Blow"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Au_f_add"),Qt::Horizontal,tr("thin_thin_Au_f_add"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Now_r_weight"),Qt::Horizontal,tr("thin_Now_r_weight"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Au_after_weight"),Qt::Horizontal,tr("thin_Au_after_weight"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Au_use"),Qt::Horizontal,tr("thin_Au_use"));
//       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Ti_add"),Qt::Horizontal,tr("thin_Ti_add"));
//       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Al_add"),Qt::Horizontal,tr("thin_Al_add"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("X_tal_number"),Qt::Horizontal,tr("thin_X_tal_number"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("X_tal_Hz"),Qt::Horizontal,tr("thin_X_tal_Hz"));
//       t13_data_model->setHeaderData(t13_data_model->fieldIndex("X_tal_life"),Qt::Horizontal,tr("thin_X_tal_life"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("User_name"),Qt::Horizontal,tr("thin_User_name"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("D_use"),Qt::Horizontal,tr("thin_D_use"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("note"),Qt::Horizontal,tr("thin_note"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Ti_total"),Qt::Horizontal,tr("thin_Ti_total"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Al_total"),Qt::Horizontal,tr("thin_Al_total"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Au_total"),Qt::Horizontal,tr("thin_Au_total"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("Input_time"),Qt::Horizontal,tr("thin_Input_time"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("machine_name"),Qt::Horizontal,tr("thin_machine_name"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("machine_code"),Qt::Horizontal,tr("thin_machine_code"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("batch_id"),Qt::Horizontal,tr("thin_batch_id"));
       t13_data_model->setHeaderData(t13_data_model->fieldIndex("cleaning_cal_flag"),Qt::Horizontal,tr("thin_cleaning_cal_flag"));
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("day_and_night"),50);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("vacuum_time"),110);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Run_number"),60);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Resister"),50);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("machine_type"),100);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Lot_id"),80);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Wafer_count"),50);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Pad_thin"),80);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Wlp_thin"),80);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Wafer_angle"),80);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Input_thin_data"),150);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Recipe"),150);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("ashing1"),65);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("ashing2"),65);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("ashing3"),65);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("ashing_machine"),80);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("N2_Blow"),80);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Au_f_add"),80);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Now_r_weight"),120);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Au_after_weight"),140);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Au_use"),50);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Ti_add"),50);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Al_add"),50);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("X_tal_number"),50);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("X_tal_Hz"),50);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("X_tal_life"),80);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("User_name"),80);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("D_use"),80);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("note"),140);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Ti_total"),60);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Al_total"),60);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Au_total"),60);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("Input_time"),120);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("machine_name"),50);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("machine_code"),50);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("batch_id"),50);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("cleaning_cal_flag"),50);
       ui->t13_data_table->horizontalHeader()->resizeSection(t13_data_model->fieldIndex("dom"),50);

       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("X_tal_number"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("X_tal_Hz"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("machine_code"));
   //    ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("batch_id"));
   //    ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("cleaning_cal_flag"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("Au_use"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("au_pellet_output"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("au_add"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("check_user"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("au_pellet_total"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("work_start_au"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("au_add_weight"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("au_case_weight"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("check_NG"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("Run_number"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("Pad_thin"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("Wlp_thin"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("N2_Blow"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("D_use"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("Ti_add"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("Al_add"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("X_tal_life"));
       ui->t13_data_table->horizontalHeader()->hideSection(t13_data_model->fieldIndex("Resister"));

   //    ui->t13_data_table->horizontalHeader()->resizeSection(0,50);
   //    ui->t13_data_table->horizontalHeader()->resizeSection(1,150);
   //    ui->t13_data_table->horizontalHeader()->resizeSection(6,120);
   //    ui->t13_data_table->horizontalHeader()->resizeSection(12,160);
       ui->t13_data_table->scrollToBottom();



       ui->t13_spec_table->setModel(spec_model);
       ui->t13_spec_table->horizontalHeader()->hideSection(0);
       ui->t13_spec_table->horizontalHeader()->hideSection(1);
       ui->t13_spec_table->horizontalHeader()->hideSection(2);
       ui->t13_spec_table->horizontalHeader()->hideSection(3);
       ui->t13_spec_table->horizontalHeader()->hideSection(4);
       ui->t13_spec_table->horizontalHeader()->hideSection(5);
       ui->t13_spec_table->horizontalHeader()->hideSection(6);
       ui->t13_spec_table->horizontalHeader()->hideSection(7);
       ui->t13_spec_table->horizontalHeader()->hideSection(8);
       ui->t13_spec_table->horizontalHeader()->hideSection(9);
       ui->t13_spec_table->horizontalHeader()->hideSection(10);
       ui->t13_spec_table->horizontalHeader()->hideSection(11);
       ui->t13_spec_table->horizontalHeader()->hideSection(12);
       ui->t13_spec_table->horizontalHeader()->hideSection(13);
       ui->t13_spec_table->horizontalHeader()->hideSection(14);
       ui->t13_spec_table->horizontalHeader()->hideSection(17);
       ui->t13_spec_table->horizontalHeader()->hideSection(18);
       ui->t13_spec_table->horizontalHeader()->hideSection(19);
       ui->t13_spec_table->horizontalHeader()->hideSection(20);
       ui->t13_spec_table->horizontalHeader()->hideSection(21);
       ui->t13_spec_table->horizontalHeader()->hideSection(22);
       ui->t13_spec_table->horizontalHeader()->hideSection(23);
       ui->t13_spec_table->horizontalHeader()->hideSection(24);
       ui->t13_spec_table->horizontalHeader()->hideSection(25);
       ui->t13_spec_table->horizontalHeader()->hideSection(26);
       ui->t13_spec_table->horizontalHeader()->hideSection(27);
       ui->t13_spec_table->horizontalHeader()->hideSection(28);
       ui->t13_spec_table->horizontalHeader()->hideSection(29);
       ui->t13_spec_table->horizontalHeader()->hideSection(30);
       ui->t13_spec_table->horizontalHeader()->hideSection(31);
       ui->t13_spec_table->horizontalHeader()->hideSection(32);
       ui->t13_spec_table->horizontalHeader()->hideSection(33);
       ui->t13_spec_table->horizontalHeader()->hideSection(34);
       ui->t13_spec_table->horizontalHeader()->hideSection(35);
       ui->t13_spec_table->horizontalHeader()->resizeSection(6,150);
       ui->t13_spec_table->horizontalHeader()->resizeSection(7,150);

       for(int i=0;i<8;i++){
           ui->t13_input_table->insertColumn(i);
       }
       ui->t13_input_table->insertRow(0);
       ui->t13_input_table->insertRow(1);
       ui->t13_input_table->insertRow(2);
       ui->t13_input_table->insertRow(3);
       ui->t13_input_table->insertRow(4);
       ui->t13_input_table->insertRow(5);
       ui->t13_input_table->insertRow(6);
       ui->t13_input_table->horizontalHeader()->resizeSection(0,70);
       ui->t13_input_table->horizontalHeader()->resizeSection(1,150);
       ui->t13_input_table->horizontalHeader()->resizeSection(3,70);
       ui->t13_input_table->horizontalHeader()->resizeSection(5,130);
       ui->t13_input_table->horizontalHeader()->resizeSection(6,140);
       ui->t13_input_table->horizontalHeader()->resizeSection(7,80);
       t13_input_item = new input_item(my_mesdb,"MD042");
       ui->t13_input_table->setCellWidget(0,0,t13_input_item->ui->LA_day_and_night);
       ui->t13_input_table->setCellWidget(1,0,t13_input_item->ui->CB_day_and_night);
       ui->t13_input_table->setCellWidget(0,1,t13_input_item->ui->LA_vacuum_time);
       ui->t13_input_table->setCellWidget(1,1,t13_input_item->ui->DT_vacuum_time);
       t13_input_item->ui->DT_vacuum_time->setDateTime(QDateTime::currentDateTime());
//       ui->t13_input_table->setCellWidget(0,2,t13_input_item->ui->LA_resister);
//       ui->t13_input_table->setCellWidget(1,2,t13_input_item->ui->LE_resister);
//       ui->t13_input_table->setCellWidget(0,3,t13_input_item->ui->LA_N2_blow);
//       ui->t13_input_table->setCellWidget(1,3,t13_input_item->ui->WCB_N2_blow);
       ui->t13_input_table->setCellWidget(0,4,t13_input_item->ui->LA_au_f_add);
       ui->t13_input_table->setCellWidget(1,4,t13_input_item->ui->LE_au_f_add);
       ui->t13_input_table->setCellWidget(0,5,t13_input_item->ui->LA_now_r_weight);
       ui->t13_input_table->setCellWidget(1,5,t13_input_item->ui->LE_now_r_weight);
       ui->t13_input_table->setCellWidget(0,6,t13_input_item->ui->LA_au_after_r_weight);
       ui->t13_input_table->setCellWidget(1,6,t13_input_item->ui->LE_au_after_r_weight);
       ui->t13_input_table->setCellWidget(0,7,t13_input_item->ui->LA_au_after_weight);
       ui->t13_input_table->setCellWidget(1,7,t13_input_item->ui->LE_au_after_weight);
   //    ui->t13_input_table->setCellWidget(0,7,t13_input_item->ui->LA_au_use);
   //    ui->t13_input_table->setCellWidget(1,7,t13_input_item->ui->LE_au_use);
//       ui->t13_input_table->setCellWidget(2,0,t13_input_item->ui->LA_ti_add);
//       ui->t13_input_table->setCellWidget(3,0,t13_input_item->ui->LE_ti_add);
//       ui->t13_input_table->setCellWidget(2,1,t13_input_item->ui->LA_al_add);
//       ui->t13_input_table->setCellWidget(3,1,t13_input_item->ui->LE_al_add);
//       ui->t13_input_table->setCellWidget(2,2,t13_input_item->ui->LA_xtal_life);
//       ui->t13_input_table->setCellWidget(3,2,t13_input_item->ui->LE_xtal_life);
       ui->t13_input_table->setCellWidget(2,3,t13_input_item->ui->LA_name);
       ui->t13_input_table->setCellWidget(3,3,t13_input_item->ui->LE_name);
//       ui->t13_input_table->setCellWidget(2,4,t13_input_item->ui->LA_d_run);
//       ui->t13_input_table->setCellWidget(3,4,t13_input_item->ui->WCB_d_run);
       ui->t13_input_table->setCellWidget(2,5,t13_input_item->ui->LA_note);
       ui->t13_input_table->setCellWidget(3,5,t13_input_item->ui->LE_note);
       ui->t13_input_table->setSpan(2,5,1,5);
       ui->t13_input_table->setSpan(3,5,1,5);
       ui->t13_input_table->setCellWidget(4,0,t13_input_item->ui->LE_recipe_number);
       ui->t13_input_table->setCellWidget(4,1,t13_input_item->ui->CB_recipe_choice);
   //    ui->t13_input_table->setCellWidget(4,3,t13_input_item->ui->LA_recipe_2);
       ui->t13_input_table->setCellWidget(5,0,t13_input_item->ui->W_recipe);
       ui->t13_input_table->setSpan(4,1,1,5);
   //    ui->t13_input_table->setSpan(4,3,1,2);
       ui->t13_input_table->setSpan(5,0,2,5);

       ui->t13_input_table->setCellWidget(4,6,t13_input_item->ui->LA_dom_number);
       ui->t13_input_table->setCellWidget(5,6,t13_input_item->ui->CB_domnumber);

       ui->t13_LE_Lot_count->setValidator(new QIntValidator(0, 1000, this));
//       ui->t13_LE_Run_number->setValidator(new QIntValidator(0, 1000, this));
       QSqlQuery query(my_mesdb);
       query.exec(QString("select `Lot_max_count`,`Run_number_count` from Thin_film_spec_managerment where machine_code = 'MD042'"));
       if(query.next()){
           ui->t13_LE_Lot_count->setText(query.value("Lot_max_count").toString());
//           ui->t13_LE_Run_number->setText(query.value("Run_number_count").toString());
       }
       t13_lot_item_list.clear();
       for(int i=0;i<ui->t13_LE_Lot_count->text().toInt();i++){

           input_lot_item *lot_item = new input_lot_item(ms_mesdb,t13_input_item,"MD042");
           t13_lot_item_list.append(lot_item);
           ui->t13_input_lot_table->insertRow(i);
           ui->t13_input_lot_table->setCellWidget(i,0,lot_item->ui->LE_Lot_id);
   //        ui->t13_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_Run_number);
           ui->t13_input_lot_table->setCellWidget(i,1,lot_item->ui->LE_wafer_count);
           ui->t13_input_lot_table->setCellWidget(i,2,lot_item->ui->LE_ashing1);
           ui->t13_input_lot_table->setCellWidget(i,3,lot_item->ui->LE_ashing2);
           ui->t13_input_lot_table->setCellWidget(i,4,lot_item->ui->LE_ashing3);
           ui->t13_input_lot_table->setCellWidget(i,5,lot_item->ui->LE_ashing_machine);
           ui->t13_input_lot_table->setCellWidget(i,6,lot_item->ui->LE_machine_type);
           ui->t13_input_lot_table->setCellWidget(i,7,lot_item->ui->LE_wafer_angle);
   //        connect(lot_item,SIGNAL(lot_find_item(thin_film_manager_input_lot_item)),
   //                t13_input_item,SLOT(slot_lot_item_data(thin_film_manager_input_lot_item)));
       }
       query.exec("SELECT * FROM `Thin_film_repcipe` where machine_code = 'MD042' ORDER BY `recipe_number` ASC  ");
       t13_input_item->ui->CB_recipe_choice->addItem(tr("recipe_choice"));
       while(query.next()){
           QString recipe_name = query.value("recipe_number").toString()+"."+ query.value("recipename").toString()
                                   + "|"+query.value("metal_name").toString()+"|"+query.value("metal_thin").toString()+"|"
                                   +query.value("type").toString();
           QPixmap pixmap(100,100);
           pixmap.fill(QColor(query.value("color").toString()));
           t13_input_item->ui->CB_recipe_choice->addItem(QIcon(pixmap),recipe_name);
       }
       connect(t13_input_item->ui->CB_recipe_choice,SIGNAL(currentIndexChanged(int)),this,SLOT(t13_recipe_choice(int)));

       t13_ti_chart = new Thin_film_chart();
       t13_ti_chartview = new Thin_film_chart_view(t13_ti_chart);
       connect(t13_ti_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t13_ti_y_value_slot(QPointF)));
       ui->t13_Ti_layout->addWidget(t13_ti_chartview);

       t13_al_chart = new Thin_film_chart();
       t13_al_chartview = new Thin_film_chart_view(t13_al_chart);
       connect(t13_al_chartview,SIGNAL(move_value(QPointF)),this,SLOT(t13_al_y_value_slot(QPointF)));
       ui->t13_Al_layout->addWidget(t13_al_chartview);
       t13_ti_axisX = new QDateTimeAxis();
       t13_ti_axisX->setFormat("MM-dd HH:mm:ss");
       t13_ti_axisX->setTitleText("Date");
       t13_ti_axisX->setVisible(true);
       t13_ti_axisX->setTitleVisible(true);
       t13_ti_chart->addAxis(t13_ti_axisX, Qt::AlignBottom);
       t13_ti_axisY = new QValueAxis();
       t13_ti_chart->addAxis(t13_ti_axisY,Qt::AlignLeft);

       t13_al_axisX = new QDateTimeAxis();
       t13_al_axisX->setFormat("MM-dd HH:mm:ss");
       t13_al_axisX->setTitleText("Date");
       t13_al_axisX->setVisible(true);
       t13_al_axisX->setTitleVisible(true);
       t13_al_chart->addAxis(t13_al_axisX, Qt::AlignBottom);
       t13_al_axisY = new QValueAxis();
       t13_al_chart->addAxis(t13_al_axisY,Qt::AlignLeft);

       ti_al_chart_init(t13_ti_chart,t13_al_chart,t13_ti_value_series,t13_al_value_series,t13_ti_UCL_value_series,t13_al_UCL_value_series,
                        t13_al_axisX,t13_al_axisY,t13_ti_axisX,t13_ti_axisY,"MD042");


       query.exec("SELECT machine_name,machine_code FROM Thin_film_spec_managerment");
       while(query.next()){
           ui->t13_CB_move_machine->addItem(query.value("machine_name").toString());
       }
       temp_run_widget_map.insert("MD042",ui->t13_temp_run_mode_time);
       temp_run_ti_accmulate_widget_map.insert("MD042",ui->t13_ti_accmulate);
       temp_run_al_accmulate_widget_map.insert("MD042",ui->t13_al_accmulate);

       ui->t13_CB_wet_insert->addItems(wet_machine_list);

       sync_now_date("MD042",t13_input_item->ui->LE_au_f_add,t13_input_item->ui->LE_now_r_weight,t13_input_item->ui->LE_au_after_r_weight
                     ,t13_input_item->ui->LE_au_after_weight);

       ui->t13_data_table->selectionModel()->setObjectName("t13_data");
       connect(ui->t13_data_table->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),
               this,SLOT(table_view_selection_change(QModelIndex,QModelIndex)));
}


void Thin_film_mainwindows::closeEvent(QCloseEvent *event)
{
    temp_run_mode_thread->requestInterruption();
    temp_run_mode_thread->wait();
    this->deleteLater();
}
void Thin_film_mainwindows::v1_data_model_change(QModelIndex index1, thin_sqltable_model *data_model, QString macihne_code)
{
    for(int i=0;i<batch_item_updatelist.count();i++){
        if(index1.column() == data_model->fieldIndex(batch_item_updatelist.at(i))){
            int batch_id = data_model->index(index1.row(),data_model->fieldIndex("batch_id")).data().toInt();
            QSqlQuery query(my_mesdb);
            QVariant index_data = index1.data();
            if(index1.data().toString()=="O"){
                index_data = "2";
            }else if(index1.data().toString()=="X"){
                index_data = "0";
            }
            if(batch_item_updatelist.at(i) == "vacuum_time"){
                query.exec(QString("update `Thin_film_data` set `%1`='%2' where machine_code = '%3' AND  batch_id = '%4' ")
                           .arg(batch_item_updatelist.at(i)).arg(index_data.toDateTime().toString("yyyy-MM-dd hh:mm:00"))
                           .arg(macihne_code).arg(batch_id));
            }else if(batch_item_updatelist.at(i) == "User_name"){
                query.exec(QString("update `Thin_film_data` set `%1`='%2' where machine_code = '%3' AND  batch_id = '%4' ")
                           .arg(batch_item_updatelist.at(i)).arg(index_data.toString())
                           .arg(macihne_code).arg(batch_id));
            }else if(batch_item_updatelist.at(i) == "Input_time"){
                query.exec(QString("update `Thin_film_data` set `%1`='%2' where machine_code = '%3' AND  batch_id = '%4' ")
                           .arg(batch_item_updatelist.at(i)).arg(index_data.toDateTime().toString("yyyy-MM-dd hh:mm:ss"))
                           .arg(macihne_code).arg(batch_id));
            }else if(batch_item_updatelist.at(i) == "Input_thin_data"){
                QString Recipe = data_model->index(index1.row(),data_model->fieldIndex("Recipe")).data().toString();
                QString Input_thin_data = data_model->index(index1.row(),data_model->fieldIndex("Input_thin_data")).data().toString();
                Input_thin_data = Input_thin_data.replace("\r\n","");
                QStringList input_thin_data_list = Input_thin_data.split("/");
                Recipe = Recipe.toUpper();
                QStringList recipelist = Recipe.split("/");
                int Ti_total = 0;
                int Al_total = 0;
                int Au_total = 0;
                for(int i=0;i<recipelist.count();i++){
                    if(i>=input_thin_data_list.count()){
                        break ;
                    }
                    if(recipelist.at(i) == "AL"){
                        Al_total += QString(input_thin_data_list.at(i)).toInt();
                    }else if(recipelist.at(i) == "TI"){
                        Ti_total += QString(input_thin_data_list.at(i)).toInt();
                    }else if(recipelist.at(i) == "AU"){
                        Au_total += QString(input_thin_data_list.at(i)).toInt();
                    }
                }
                query.exec(QString("update `Thin_film_data` set `Ti_total` = %1,`Al_total` = %2, `Au_total` = %3,"
                                   "`Input_thin_data` = '%4' where `machine_code` = '%5' AND batch_id = '%6'")
                           .arg(Ti_total).arg(Al_total).arg(Au_total).arg(data_model->index(index1.row(),data_model->fieldIndex("Input_thin_data")).data().toString())
                           .arg(macihne_code).arg(batch_id));
            }else if(batch_item_updatelist.at(i) == "Recipe"){
                QString Recipe = data_model->index(index1.row(),data_model->fieldIndex("Recipe")).data().toString();
                QString Input_thin_data = data_model->index(index1.row(),data_model->fieldIndex("Input_thin_data")).data().toString();
                Input_thin_data = Input_thin_data.replace("\r\n","");
                QStringList input_thin_data_list = Input_thin_data.split("/");
                Recipe = Recipe.toUpper();
                QStringList recipelist = Recipe.split("/");
                int Ti_total = 0;
                int Al_total = 0;
                int Au_total = 0;
                for(int i=0;i<recipelist.count();i++){
                    if(i>=input_thin_data_list.count()){
                        break ;
                    }
                    if(recipelist.at(i) == "AL"){
                        Al_total += QString(input_thin_data_list.at(i)).toInt();
                    }else if(recipelist.at(i) == "TI"){
                        Ti_total += QString(input_thin_data_list.at(i)).toInt();
                    }else if(recipelist.at(i) == "AU"){
                        Au_total += QString(input_thin_data_list.at(i)).toInt();
                    }
                }
                query.exec(QString("update `Thin_film_data` set `Ti_total` = %1,`Al_total` = %2, `Au_total` = %3,"
                                   "`Recipe` = '%4' where `machine_code` = '%5' AND batch_id = '%6'")
                           .arg(Ti_total).arg(Al_total).arg(Au_total).arg(data_model->index(index1.row(),data_model->fieldIndex("Recipe")).data().toString())
                           .arg(macihne_code).arg(batch_id));
            }else {
                query.exec(QString("update `Thin_film_data` set `%1`='%2' where machine_code = '%3' AND  batch_id = '%4' ")
                           .arg(batch_item_updatelist.at(i)).arg(index_data.toDouble()).arg(macihne_code).arg(batch_id));
            }

            if(batch_item_updatelist.at(i)== "Now_r_weight"||batch_item_updatelist.at(i)== "Au_after_weight"){
                double au_after = data_model->index(index1.row(),data_model->fieldIndex("Au_after_weight")).data().toDouble();
                double au_now = data_model->index(index1.row(),data_model->fieldIndex("Now_r_weight")).data().toDouble();
                double result = au_after-au_now;
                query.exec(QString("update `Thin_film_data` set `Au_f_add`='%1' where machine_code = '%2' AND  batch_id = '%3' ")
                           .arg(result).arg(macihne_code).arg(batch_id));

                QDateTime input_time = data_model->index(index1.row(),data_model->fieldIndex("Input_time")).data().toDateTime();
                au_calc(input_time);

            }
            data_model->select();

            return ;
        }
    }
}
void Thin_film_mainwindows::ti_al_chart_init(Thin_film_chart *ti_chart,Thin_film_chart *al_chart,QLineSeries *ti_value_series,
                                             QLineSeries *al_value_series,QLineSeries *ti_UCL_value_series,
                                             QLineSeries *al_UCL_value_series,QDateTimeAxis *al_axisX,QValueAxis *al_axisY,QDateTimeAxis *ti_axisX,QValueAxis *ti_axisY,QString machine_code){
    QSqlQuery query(my_mesdb);

        ti_chart->removeAllSeries();
        al_chart->removeAllSeries();

        if(ti_chart->axisX() != NULL){
            ti_chart->removeAxis(ti_chart->axisX());
        }
        if(ti_chart->axisY() != NULL){
            ti_chart->removeAxis(ti_chart->axisY());
        }
        if(al_chart->axisX() != NULL){
            al_chart->removeAxis(al_chart->axisX());
        }
        if(al_chart->axisY() != NULL){
            al_chart->removeAxis(al_chart->axisY());
        }

        ti_value_series = new QLineSeries();
        al_value_series = new QLineSeries();
        ti_UCL_value_series = new QLineSeries();
        ti_UCL_value_series->setName("UCL");
        al_UCL_value_series = new QLineSeries();
        al_UCL_value_series->setName("UCL");
        double ti_spec = 0;
        double al_spec = 0;
        query.exec(QString("select `machine_name`, `machine_code`,`cleaning_ti_UCL`,`cleaning_al_UCL` from `Thin_film_spec_managerment` "
                           "where `machine_code` = '%1' ").arg(machine_code));
        if(query.next()){
            ti_spec = query.value("cleaning_ti_UCL").toDouble();
            al_spec = query.value("cleaning_al_UCL").toDouble();
        }

        query.exec(QString("select * from Thin_film_data"
                           " where cleaning_cal_flag = '0' AND machine_code = '%1' group by batch_id order by Input_time asc  ").arg(machine_code));
        double ti_acc_total = 0;
        double al_acc_total = 0;

        while(query.next()){
            ti_acc_total += query.value("Ti_total").toDouble();
            ti_value_series->append(query.value("Input_time").toDateTime().toMSecsSinceEpoch(),ti_acc_total);
            al_acc_total += query.value("Al_total").toDouble();
            al_value_series->append(query.value("Input_time").toDateTime().toMSecsSinceEpoch(),al_acc_total);
            ti_UCL_value_series->append(query.value("Input_time").toDateTime().toMSecsSinceEpoch(),ti_spec);
            al_UCL_value_series->append(query.value("Input_time").toDateTime().toMSecsSinceEpoch(),al_spec);
        }



        ti_chart->addSeries(ti_value_series);
        ti_chart->addSeries(ti_UCL_value_series);

        ti_axisX = new QDateTimeAxis();
        ti_axisX->setFormat("MM-dd HH:mm:ss");
        ti_axisX->setTitleText("Date");
        ti_axisX->setVisible(true);
        ti_axisX->setTitleVisible(true);

        if(ti_value_series->points().count()>0){
            double cal = ti_value_series->points().last().x() - ti_value_series->points().first().x();
            ti_axisX->setRange(QDateTime::fromMSecsSinceEpoch(ti_value_series->points().first().x()).addMSecs(-(cal/10)),
                                  QDateTime::fromMSecsSinceEpoch(ti_value_series->points().last().x()).addMSecs(cal/10));
        }
        ti_chart->addAxis(ti_axisX, Qt::AlignBottom);
        ti_axisY = new QValueAxis();
        ti_chart->addAxis(ti_axisY,Qt::AlignLeft);
        if(ti_value_series->points().count()>0){
            ti_axisY->setRange(0,ti_spec+(ti_spec/10));
        }
        ti_value_series->attachAxis(ti_axisX);
        ti_value_series->attachAxis(ti_axisY);
        ti_value_series->setPointsVisible(true);
        ti_UCL_value_series->setColor(QColor("red"));
        ti_UCL_value_series->attachAxis(ti_axisX);
        ti_UCL_value_series->attachAxis(ti_axisY);


        al_chart->addSeries(al_value_series);
        al_chart->addSeries(al_UCL_value_series);
        al_axisX = new QDateTimeAxis();
        al_axisX->setFormat("MM-dd HH:mm:ss");
        al_axisX->setTitleText("Date");
        al_axisX->setVisible(true);
        al_axisX->setTitleVisible(true);
        if(al_value_series->points().count()>0){
            double cal = al_value_series->points().last().x() - al_value_series->points().first().x();
            al_axisX->setRange(QDateTime::fromMSecsSinceEpoch(al_value_series->points().first().x()).addMSecs(-(cal/10)),
                                  QDateTime::fromMSecsSinceEpoch(al_value_series->points().last().x()).addMSecs(cal/10));
        }
        al_chart->addAxis(al_axisX, Qt::AlignBottom);
        al_axisY = new QValueAxis();
        if(al_value_series->points().count()>0){
            al_axisY->setRange(0,al_spec+(al_spec/10));
        }
        al_chart->addAxis(al_axisY,Qt::AlignLeft);
        al_value_series->attachAxis(al_axisX);
        al_value_series->attachAxis(al_axisY);

        al_UCL_value_series->setColor(QColor("red"));
        al_UCL_value_series->attachAxis(al_axisX);
        al_UCL_value_series->attachAxis(al_axisY);
        al_value_series->setPointsVisible(true);

        ti_value_series->setName("Ti value");
        ti_value_series->setPointLabelsVisible(true);
        ti_value_series->setPointLabelsFormat("(@yPoint)");

        al_value_series->setName("Al value");
        al_value_series->setPointLabelsVisible(true);
        al_value_series->setPointLabelsFormat("(@yPoint)");

}


void Thin_film_mainwindows::temp_run_mode_timeout()
{
    QSqlQuery query(my_mesdb);
    query.exec("select machine_code,difftime,Ti_total,Al_total from Thin_film_cleaning_view");
    while(query.next()){
        if(temp_run_widget_map.contains(query.value("machine_code").toString())){
            QTime difftime = query.value("difftime").toTime();
            int secs = QTime(0,0,0).secsTo(difftime);
            temp_run_widget_map.value(query.value("machine_code").toString())->setText(QString("%1").arg(secs));
            temp_run_ti_accmulate_widget_map.value(query.value("machine_code").toString())->setText(QString("%1").arg(query.value("Ti_total").toString()));
            temp_run_al_accmulate_widget_map.value(query.value("machine_code").toString())->setText(QString("%1").arg(query.value("Al_total").toString()));
        }
    }
}
Thin_film_mainwindows::~Thin_film_mainwindows()
{
    delete ui;
}
void Thin_film_mainwindows::au_calc(QDateTime input_time)
{
    QSqlQuery query1(my_mesdb);
    query1.exec(QString("select * from Thin_film_cleaning_view_temp where Input_time<'%1' order by Input_time desc LIMIT 1")
                 .arg(input_time.toString("yyyy-MM-dd hh:mm:ss")));
    double before_au_pellet_total = 0;
    if(query1.next()){
        before_au_pellet_total =  query1.value("au_pellet_total").toDouble();
        query1.exec(QString("select * from Thin_film_cleaning_view_temp where Input_time>='%1' order by Input_time asc")
                    .arg(input_time.toString("yyyy-MM-dd hh:mm:ss")));
        while(query1.next()){
            int batch_id = query1.value("batch_id").toInt();
            QString machine_name = query1.value("machine_name").toString();
            double au_pellet_total  = before_au_pellet_total - query1.value("Au_f_add").toDouble() + query1.value("au_add").toDouble();

            QSqlQuery query2(my_mesdb);
            query2.exec(QString("update `Thin_film_data` set `au_pellet_total` = %1 where machine_name = '%2' AND batch_id = %3")
                        .arg(au_pellet_total).arg(machine_name).arg(batch_id));
            qDebug()<<query2.lastError().text();
            qDebug()<<query2.lastQuery();
            before_au_pellet_total = au_pellet_total;
        }
    }
    t8_fast_search();
    return;
}


void Thin_film_mainwindows::sync_now_date(QString machine_code,QLineEdit *au_add,QLineEdit *now_r_weight,QLineEdit *au_after_r_weight,QLineEdit *au_after_weight)
{
    QSqlQuery query(my_mesdb);
    query.exec(QString("select * from `Thin_film_spec_managerment` where `machine_code` = '%1'").arg(machine_code));
    if(query.next()){
        au_add->setText(query.value("au_pullet_add").toString());
        now_r_weight->setText(query.value("now_r_weight").toString());
        au_after_r_weight->setText(query.value("au_after_r_weight").toString());
        au_after_weight->setText(query.value("au_after_weight").toString());
    }
}


void Thin_film_mainwindows::table_view_selection_change(QModelIndex current, QModelIndex previous)
{
    QItemSelectionModel *select_model = (QItemSelectionModel *)sender();
    QModelIndexList index_list = select_model->selectedIndexes();
    int wafer_total = 0;
    for(int i=0;i<index_list.count();i++){
        if(index_list.at(i).column() == wafer_count_index){
            wafer_total += index_list.at(i).data().toInt();
        }
    }
    if(select_model->objectName() == "t1_data"){
         ui->t1_total_wafer->setText(QString("%1").arg(wafer_total));
    }else if(select_model->objectName() == "t2_data"){
        ui->t2_total_wafer->setText(QString("%1").arg(wafer_total));
    }else if(select_model->objectName() == "t3_data"){
        ui->t3_total_wafer->setText(QString("%1").arg(wafer_total));
    }else if(select_model->objectName() == "t4_data"){
        ui->t4_total_wafer->setText(QString("%1").arg(wafer_total));
    }else if(select_model->objectName() == "t5_data"){
        ui->t5_total_wafer->setText(QString("%1").arg(wafer_total));
    }else if(select_model->objectName() == "t6_data"){
        ui->t6_total_wafer->setText(QString("%1").arg(wafer_total));
    }else if(select_model->objectName() == "t7_data"){
        ui->t7_total_wafer->setText(QString("%1").arg(wafer_total));
    }else if(select_model->objectName() == "t13_data"){
        ui->t13_total_wafer->setText(QString("%1").arg(wafer_total));
    }
}




void Thin_film_mainwindows::on_tabWidget_currentChanged(int index)
{
    if(index==12){
        ui->t11_cha1_ti->addWidget(t1_ti_chartview,0,0);
        ui->t11_cha1_al->addWidget(t1_al_chartview,0,0);
        ui->t11_cha2_ti->addWidget(t2_ti_chartview,0,0);
        ui->t11_cha2_al->addWidget(t2_al_chartview,0,0);
        ui->t11_cha3_ti->addWidget(t3_ti_chartview,0,0);
        ui->t11_cha3_al->addWidget(t3_al_chartview,0,0);
        ui->t11_cha4_ti->addWidget(t4_ti_chartview,0,0);
        ui->t11_cha4_al->addWidget(t4_al_chartview,0,0);
        ui->t11_bps_ti->addWidget(t5_ti_chartview,0,0);
        ui->t11_bps_al->addWidget(t5_al_chartview,0,0);
        ui->t11_evatec1_ti->addWidget(t6_ti_chartview,0,0);
        ui->t11_evatec1_al->addWidget(t6_al_chartview,0,0);
        ui->t11_hanil1_ti->addWidget(t7_ti_chartview,0,0);
        ui->t11_hanil1_al->addWidget(t7_al_chartview,0,0);
        ui->t11_sin1_ti->addWidget(t13_ti_chartview,0,0);
        ui->t11_sin1_al->addWidget(t13_al_chartview,0,0);
        ui->t11_hanil2_ti->addWidget(t14_ti_chartview,0,0);
        ui->t11_hanil2_al->addWidget(t14_al_chartview,0,0);
        ui->t11_hanil3_ti->addWidget(t15_ti_chartview,0,0);
        ui->t11_hanil3_al->addWidget(t15_al_chartview,0,0);
        ui->t11_hanil4_ti->addWidget(t16_ti_chartview,0,0);
        ui->t11_hanil4_al->addWidget(t16_al_chartview,0,0);
        t11_timer.setInterval(20000);
        t11_timer.start();

    }else {
        ui->t1_Ti_layout->addWidget(t1_ti_chartview,0,0);
        ui->t1_Al_layout->addWidget(t1_al_chartview,0,0);
        ui->t2_Ti_layout->addWidget(t2_ti_chartview,0,0);
        ui->t2_Al_layout->addWidget(t2_al_chartview,0,0);
        ui->t3_Ti_layout->addWidget(t3_ti_chartview,0,0);
        ui->t3_Al_layout->addWidget(t3_al_chartview,0,0);
        ui->t4_Ti_layout->addWidget(t4_ti_chartview,0,0);
        ui->t4_Al_layout->addWidget(t4_al_chartview,0,0);
        ui->t5_Ti_layout->addWidget(t5_ti_chartview,0,0);
        ui->t5_Al_layout->addWidget(t5_al_chartview,0,0);
        ui->t6_Ti_layout->addWidget(t6_ti_chartview,0,0);
        ui->t6_Al_layout->addWidget(t6_al_chartview,0,0);
        ui->t7_Ti_layout->addWidget(t7_ti_chartview,0,0);
        ui->t7_Al_layout->addWidget(t7_al_chartview,0,0);
        ui->t13_Ti_layout->addWidget(t13_ti_chartview,0,0);
        ui->t13_Al_layout->addWidget(t13_al_chartview,0,0);
        ui->t14_Ti_layout->addWidget(t14_ti_chartview,0,0);
        ui->t14_Al_layout->addWidget(t14_al_chartview,0,0);
        ui->t15_Ti_layout->addWidget(t15_ti_chartview,0,0);
        ui->t15_Al_layout->addWidget(t15_al_chartview,0,0);
        ui->t16_Ti_layout->addWidget(t16_ti_chartview,0,0);
        ui->t16_Al_layout->addWidget(t16_al_chartview,0,0);
        t11_timer.stop();
    }
}

void Thin_film_mainwindows::all_refresh_graph()
{
    on_t1_ti_al_refresh_clicked();
    on_t2_ti_al_refresh_clicked();
    on_t3_ti_al_refresh_clicked();
    on_t4_ti_al_refresh_clicked();
    on_t5_ti_al_refresh_clicked();
    on_t6_ti_al_refresh_clicked();
    on_t7_ti_al_refresh_clicked();
    on_t13_ti_al_refresh_clicked();
    on_t14_ti_al_refresh_clicked();
    on_t15_ti_al_refresh_clicked();
    on_t16_ti_al_refresh_clicked();
}

