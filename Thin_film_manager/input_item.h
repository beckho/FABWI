#ifndef INPUT_ITEM_H
#define INPUT_ITEM_H

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <Thin_film_manager/thin_film_manager_input_lot_item.h>
#include <QMessageBox>
namespace Ui {
class input_item;
}

class input_item : public QWidget
{
    Q_OBJECT

public:
    explicit input_item(QSqlDatabase db,QString machine_code,QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    QString machine_code;
    ~input_item();
     Ui::input_item *ui;
     void clear_data();
public slots:
     void slot_lot_item_data(thin_film_manager_input_lot_item item);
private slots:


     void on_LE_now_r_weight_editingFinished();

     void on_LE_recipe_number_textChanged(const QString &arg1);

     void on_LE_au_after_r_weight_editingFinished();

     void on_LE_au_after_weight_editingFinished();


private:

};

#endif // INPUT_ITEM_H
