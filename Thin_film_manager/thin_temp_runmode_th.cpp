#include "thin_temp_runmode_th.h"

thin_temp_runmode_th::thin_temp_runmode_th(QHash<QString,QLineEdit *> temp_run_widget_map
                                           ,QHash<QString,QLineEdit *> temp_run_ti_accmulate_widget_map
                                           ,QHash<QString,QLineEdit *> temp_run_al_accmulate_widget_map)
{
    QString mydb_name = QString("MY_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss_temp"));
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    QString server_ip = settings.value("serverip").toString();
    QString db_port = settings.value("dbport").toString();
    settings.endGroup();
    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }
    this->temp_run_widget_map =temp_run_widget_map;
    this->temp_run_ti_accmulate_widget_map =temp_run_ti_accmulate_widget_map;
    this->temp_run_al_accmulate_widget_map =temp_run_al_accmulate_widget_map;

}

thin_temp_runmode_th::~thin_temp_runmode_th()
{
    my_mesdb.close();
    QThread::~QThread();
}

void thin_temp_runmode_th::run()
{
    forever{
        if(this->isInterruptionRequested()){
            return ;
        }
        QSqlQuery query(my_mesdb);
        query.exec("select machine_code,difftime,Ti_total,Al_total from Thin_film_cleaning_view");
        while(query.next()){
            if(temp_run_widget_map.contains(query.value("machine_code").toString())){
                QTime difftime = query.value("difftime").toTime();
                int secs = QTime(0,0,0).secsTo(difftime);
                emit change_gui(QString("%1").arg(secs),temp_run_widget_map.value(query.value("machine_code").toString()));
                emit change_gui(query.value("Ti_total").toString(),temp_run_ti_accmulate_widget_map.value(query.value("machine_code").toString()));
                emit change_gui(query.value("Al_total").toString(),temp_run_al_accmulate_widget_map.value(query.value("machine_code").toString()));
            }
        }
        sleep(2);
    }
}
