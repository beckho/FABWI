#ifndef SPEC_ITEM_H
#define SPEC_ITEM_H

#include <QWidget>

namespace Ui {
class spec_item;
}

class spec_item : public QWidget
{
    Q_OBJECT

public:
    explicit spec_item(QWidget *parent = 0);
    ~spec_item();
    Ui::spec_item *ui;

private:

};

#endif // SPEC_ITEM_H
