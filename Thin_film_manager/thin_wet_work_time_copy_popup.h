#ifndef THIN_WET_WORK_TIME_COPY_POPUP_H
#define THIN_WET_WORK_TIME_COPY_POPUP_H

#include <QDialog>

namespace Ui {
class Thin_wet_work_time_copy_popup;
}

class Thin_wet_work_time_copy_popup : public QDialog
{
    Q_OBJECT

public:
    explicit Thin_wet_work_time_copy_popup(QWidget *parent = 0);
    QString now_value;
    ~Thin_wet_work_time_copy_popup();

private slots:
    void on_LE_work_time_textEdited(const QString &arg1);

    void on_LE_work_time_textChanged(const QString &arg1);

    void on_buttonBox_accepted();

private:
    Ui::Thin_wet_work_time_copy_popup *ui;
};

#endif // THIN_WET_WORK_TIME_COPY_POPUP_H
