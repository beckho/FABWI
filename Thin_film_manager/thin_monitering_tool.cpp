#include "thin_monitering_tool.h"
#include "ui_thin_monitering_tool.h"

Thin_monitering_tool::Thin_monitering_tool(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Thin_monitering_tool)
{
    ui->setupUi(this);
    QString mydb_name = QString("MY_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    QString server_ip = settings.value("serverip").toString();
    QString db_port = settings.value("dbport").toString();
    settings.endGroup();
    loop_count = 0;
    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }
    moniter_timer.setInterval(5000);
    connect(&moniter_timer,SIGNAL(timeout()),this,SLOT(moniter_timer_timeout()));
}

Thin_monitering_tool::~Thin_monitering_tool()
{
    delete ui;
}

void Thin_monitering_tool::closeEvent(QCloseEvent *event)
{
    this->deleteLater();
}

void Thin_monitering_tool::moniter_timer_timeout()
{
    QSqlQuery query(my_mesdb);
    loop_count++;
    ui->LA_Count_number->setText(QString("%1").arg(loop_count));
    query.exec("select * from Thin_film_cleaning_view where AL_UCL_flag = '1' OR Ti_UCL_flag = '1'");
    if(query.next()){
       Thin_alarm_popup *popup = new Thin_alarm_popup(my_mesdb);
       popup->setWindowFlags(Qt::WindowStaysOnTopHint);
       popup->exec();
    }
}

void Thin_monitering_tool::on_monitering_start_clicked()
{
    moniter_timer.start();
}
