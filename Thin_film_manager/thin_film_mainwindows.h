#ifndef THIN_FILM_MAINWINDOWS_H
#define THIN_FILM_MAINWINDOWS_H

#include <QMainWindow>
#include <QStandardItemModel>
#include <Thin_film_manager/spec_item.h>
#include <Thin_film_manager/input_item.h>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <global_define.h>
#include <QDateTime>
#include <QSettings>
#include <QDebug>
#include <Thin_film_manager/input_lot_item.h>
#include <QSqlTableModel>
#include <QMessageBox>
#include <Thin_film_manager/thin_sqltable_model.h>
#include <Thin_film_manager/datedelegate.h>
#include <Thin_film_manager/thin_data_table_view.h>
#include <Thin_film_manager/thin_film_chart.h>
#include <Thin_film_manager/thin_film_chart_view.h>
#include <QDateTimeAxis>
#include <Thin_film_manager/thin_pellet_sqltable_model.h>
#include <Thin_film_manager/thin_wet_sqltable_model.h>
#include <Thin_film_manager/thin_lot_find_popup.h>
#include <Thin_film_manager/thin_film_au_chart_view.h>
#include <Thin_film_manager/thin_film_au_chart.h>
#include <Thin_film_manager/thin_wet_work_time_copy_popup.h>
#include <QScatterSeries>
#include <Thin_film_manager/thin_combodelegate.h>
#include <Thin_film_manager/thin_temp_runmode_th.h>
#include <QTime>
namespace Ui {
class Thin_film_mainwindows;
}

class Thin_film_mainwindows : public QMainWindow
{
    Q_OBJECT

public:
    explicit Thin_film_mainwindows(QWidget *parent = 0);
    QSqlDatabase ms_mesdb;
    QSqlDatabase my_mesdb;
    QSqlDatabase my_mesdb_2;
    QHash<QString,QLineEdit *> temp_run_widget_map;
    QHash<QString,QLineEdit *> temp_run_ti_accmulate_widget_map;
    QHash<QString,QLineEdit *> temp_run_al_accmulate_widget_map;
    QTimer temp_run_timer;
    QStringList batch_item_updatelist;
    QStringList wet_machine_list;
    int wafer_count_index;
    QStringList note_combo_list;
    thin_temp_runmode_th *temp_run_mode_thread;

    //t1 -- start
    QVector<input_lot_item *> t1_lot_item_list;
    thin_sqltable_model *t1_data_model;
    input_item *t1_input_item;
    Thin_film_chart *t1_ti_chart;
    QLineSeries *t1_ti_value_series;
    QLineSeries *t1_ti_UCL_value_series;
    QLineSeries *t1_al_UCL_value_series;
    QDateTimeAxis *t1_ti_axisX ;
    QValueAxis *t1_ti_axisY;
    Thin_film_chart_view *t1_ti_chartview;
    Thin_film_chart *t1_al_chart;
    QLineSeries *t1_al_value_series;
    QDateTimeAxis *t1_al_axisX ;
    QValueAxis *t1_al_axisY;
    Thin_film_chart_view *t1_al_chartview;


    //t1 -- end

    //t2 -- start
    QVector<input_lot_item *> t2_lot_item_list;
    thin_sqltable_model *t2_data_model;
    input_item *t2_input_item;
    Thin_film_chart *t2_ti_chart;
    QLineSeries *t2_ti_value_series;
    QLineSeries *t2_ti_UCL_value_series;
    QLineSeries *t2_al_UCL_value_series;
    QDateTimeAxis *t2_ti_axisX ;
    QValueAxis *t2_ti_axisY;
    Thin_film_chart_view *t2_ti_chartview;
    Thin_film_chart *t2_al_chart;
    QLineSeries *t2_al_value_series;
    QDateTimeAxis *t2_al_axisX ;
    QValueAxis *t2_al_axisY;
    Thin_film_chart_view *t2_al_chartview;
    //t2 -- end

    //t3 -- start
    QVector<input_lot_item *> t3_lot_item_list;
    thin_sqltable_model *t3_data_model;
    input_item *t3_input_item;
    Thin_film_chart *t3_ti_chart;
    QLineSeries *t3_ti_value_series;
    QLineSeries *t3_ti_UCL_value_series;
    QLineSeries *t3_al_UCL_value_series;
    QDateTimeAxis *t3_ti_axisX ;
    QValueAxis *t3_ti_axisY;
    Thin_film_chart_view *t3_ti_chartview;
    Thin_film_chart *t3_al_chart;
    QLineSeries *t3_al_value_series;
    QDateTimeAxis *t3_al_axisX ;
    QValueAxis *t3_al_axisY;
    Thin_film_chart_view *t3_al_chartview;
    //t3 -- end

    //t4 -- start
    QVector<input_lot_item *> t4_lot_item_list;
    thin_sqltable_model *t4_data_model;
    input_item *t4_input_item;
    Thin_film_chart *t4_ti_chart;
    QLineSeries *t4_ti_value_series;
    QLineSeries *t4_ti_UCL_value_series;
    QLineSeries *t4_al_UCL_value_series;
    QDateTimeAxis *t4_ti_axisX ;
    QValueAxis *t4_ti_axisY;
    Thin_film_chart_view *t4_ti_chartview;
    Thin_film_chart *t4_al_chart;
    QLineSeries *t4_al_value_series;
    QDateTimeAxis *t4_al_axisX ;
    QValueAxis *t4_al_axisY;
    Thin_film_chart_view *t4_al_chartview;
    //t4 -- end

    //t5 -- start
    QVector<input_lot_item *> t5_lot_item_list;
    thin_sqltable_model *t5_data_model;
    input_item *t5_input_item;
    Thin_film_chart *t5_ti_chart;
    QLineSeries *t5_ti_value_series;
    QLineSeries *t5_ti_UCL_value_series;
    QLineSeries *t5_al_UCL_value_series;
    QDateTimeAxis *t5_ti_axisX ;
    QValueAxis *t5_ti_axisY;
    Thin_film_chart_view *t5_ti_chartview;
    Thin_film_chart *t5_al_chart;
    QLineSeries *t5_al_value_series;
    QDateTimeAxis *t5_al_axisX ;
    QValueAxis *t5_al_axisY;
    Thin_film_chart_view *t5_al_chartview;
    //t5 -- end

    //t6 -- start
    QVector<input_lot_item *> t6_lot_item_list;
    thin_sqltable_model *t6_data_model;
    input_item *t6_input_item;
    Thin_film_chart *t6_ti_chart;
    QLineSeries *t6_ti_value_series;
    QLineSeries *t6_ti_UCL_value_series;
    QLineSeries *t6_al_UCL_value_series;
    QDateTimeAxis *t6_ti_axisX ;
    QValueAxis *t6_ti_axisY;
    Thin_film_chart_view *t6_ti_chartview;
    Thin_film_chart *t6_al_chart;
    QLineSeries *t6_al_value_series;
    QDateTimeAxis *t6_al_axisX ;
    QValueAxis *t6_al_axisY;
    Thin_film_chart_view *t6_al_chartview;
    //t6 -- end

    //t7 -- start
    QVector<input_lot_item *> t7_lot_item_list;
    thin_sqltable_model *t7_data_model;
    input_item *t7_input_item;
    Thin_film_chart *t7_ti_chart;
    QLineSeries *t7_ti_value_series;
    QLineSeries *t7_ti_UCL_value_series;
    QLineSeries *t7_al_UCL_value_series;
    QDateTimeAxis *t7_ti_axisX ;
    QValueAxis *t7_ti_axisY;
    Thin_film_chart_view *t7_ti_chartview;
    Thin_film_chart *t7_al_chart;
    QLineSeries *t7_al_value_series;
    QDateTimeAxis *t7_al_axisX ;
    QValueAxis *t7_al_axisY;
    Thin_film_chart_view *t7_al_chartview;
    //t7 -- end

    //t14 -- start
    QVector<input_lot_item *> t14_lot_item_list;
    thin_sqltable_model *t14_data_model;
    input_item *t14_input_item;
    Thin_film_chart *t14_ti_chart;
    QLineSeries *t14_ti_value_series;
    QLineSeries *t14_ti_UCL_value_series;
    QLineSeries *t14_al_UCL_value_series;
    QDateTimeAxis *t14_ti_axisX ;
    QValueAxis *t14_ti_axisY;
    Thin_film_chart_view *t14_ti_chartview;
    Thin_film_chart *t14_al_chart;
    QLineSeries *t14_al_value_series;
    QDateTimeAxis *t14_al_axisX ;
    QValueAxis *t14_al_axisY;
    Thin_film_chart_view *t14_al_chartview;
    //t14 -- end

    //t15 -- start
    QVector<input_lot_item *> t15_lot_item_list;
    thin_sqltable_model *t15_data_model;
    input_item *t15_input_item;
    Thin_film_chart *t15_ti_chart;
    QLineSeries *t15_ti_value_series;
    QLineSeries *t15_ti_UCL_value_series;
    QLineSeries *t15_al_UCL_value_series;
    QDateTimeAxis *t15_ti_axisX ;
    QValueAxis *t15_ti_axisY;
    Thin_film_chart_view *t15_ti_chartview;
    Thin_film_chart *t15_al_chart;
    QLineSeries *t15_al_value_series;
    QDateTimeAxis *t15_al_axisX ;
    QValueAxis *t15_al_axisY;
    Thin_film_chart_view *t15_al_chartview;
    //t15 -- end

    //t16 -- start
    QVector<input_lot_item *> t16_lot_item_list;
    thin_sqltable_model *t16_data_model;
    input_item *t16_input_item;
    Thin_film_chart *t16_ti_chart;
    QLineSeries *t16_ti_value_series;
    QLineSeries *t16_ti_UCL_value_series;
    QLineSeries *t16_al_UCL_value_series;
    QDateTimeAxis *t16_ti_axisX ;
    QValueAxis *t16_ti_axisY;
    Thin_film_chart_view *t16_ti_chartview;
    Thin_film_chart *t16_al_chart;
    QLineSeries *t16_al_value_series;
    QDateTimeAxis *t16_al_axisX ;
    QValueAxis *t16_al_axisY;
    Thin_film_chart_view *t16_al_chartview;
    //t16 -- end

    //t8 -- start
    thin_pellet_sqltable_model *t8_data_model;
    Thin_film_au_chart_view * au_chart_view;
    Thin_film_au_chart * au_chart;
    QLineSeries * au_value_series;
    QValueAxis *au_axisX;
    QValueAxis *au_axisY;

    //t8 -- end

    //t11 --start
    QTimer t11_timer;

    //t11 --end

    //t13 -- start
    QVector<input_lot_item *> t13_lot_item_list;
    thin_sqltable_model *t13_data_model;
    input_item *t13_input_item;
    Thin_film_chart *t13_ti_chart;
    QLineSeries *t13_ti_value_series;
    QLineSeries *t13_ti_UCL_value_series;
    QLineSeries *t13_al_UCL_value_series;
    QDateTimeAxis *t13_ti_axisX ;
    QValueAxis *t13_ti_axisY;
    Thin_film_chart_view *t13_ti_chartview;
    Thin_film_chart *t13_al_chart;
    QLineSeries *t13_al_value_series;
    QDateTimeAxis *t13_al_axisX ;
    QValueAxis *t13_al_axisY;
    Thin_film_chart_view *t13_al_chartview;
    //t13 -- end

    void t1_init();
    void t2_init();
    void t3_init();
    void t4_init();
    void t5_init();
    void t6_init();
    void t7_init();
    void t8_init();
    void t9_init();
    void t10_init();
    void t12_init();
    void t13_init();
    void t14_init();
    void t15_init();
    void t16_init();

    void closeEvent(QCloseEvent *event);
    void v1_data_model_change(QModelIndex index1,thin_sqltable_model *data_model,QString macihne_code);
    void au_calc(QDateTime input_time);

    ~Thin_film_mainwindows();

private slots:
    void t1_recipe_choice(int idx);
    void t2_recipe_choice(int idx);
    void t3_recipe_choice(int idx);
    void t4_recipe_choice(int idx);
    void t5_recipe_choice(int idx);
    void t6_recipe_choice(int idx);
    void t7_recipe_choice(int idx);
    void t14_recipe_choice(int idx);
    void t15_recipe_choice(int idx);
    void t16_recipe_choice(int idx);
    void t13_recipe_choice(int idx);

    void t1_ti_y_value_slot(QPointF value);
    void t1_al_y_value_slot(QPointF value);
    void t2_ti_y_value_slot(QPointF value);
    void t2_al_y_value_slot(QPointF value);
    void t3_ti_y_value_slot(QPointF value);
    void t3_al_y_value_slot(QPointF value);
    void t4_ti_y_value_slot(QPointF value);
    void t4_al_y_value_slot(QPointF value);
    void t5_ti_y_value_slot(QPointF value);
    void t5_al_y_value_slot(QPointF value);
    void t6_ti_y_value_slot(QPointF value);
    void t6_al_y_value_slot(QPointF value);
    void t7_ti_y_value_slot(QPointF value);
    void t7_al_y_value_slot(QPointF value);
    void t14_ti_y_value_slot(QPointF value);
    void t14_al_y_value_slot(QPointF value);
    void t15_ti_y_value_slot(QPointF value);
    void t15_al_y_value_slot(QPointF value);
    void t16_ti_y_value_slot(QPointF value);
    void t16_al_y_value_slot(QPointF value);
    void t13_ti_y_value_slot(QPointF value);
    void t13_al_y_value_slot(QPointF value);

    void ti_al_chart_init(Thin_film_chart *ti_chart, Thin_film_chart *al_chart, QLineSeries *ti_value_series,
                          QLineSeries *al_value_series, QLineSeries *ti_UCL_value_series,
                          QLineSeries *al_UCL_value_series, QDateTimeAxis *al_axisX, QValueAxis *al_axisY, QDateTimeAxis *ti_axisX, QValueAxis *ti_axisY, QString machine_code);

    void t1_datamodel_dataChanged(QModelIndex index1,QModelIndex index2,QVector<int> vector);
    void t2_datamodel_dataChanged(QModelIndex index1,QModelIndex index2,QVector<int> vector);
    void t3_datamodel_dataChanged(QModelIndex index1,QModelIndex index2,QVector<int> vector);
    void t4_datamodel_dataChanged(QModelIndex index1,QModelIndex index2,QVector<int> vector);
    void t5_datamodel_dataChanged(QModelIndex index1,QModelIndex index2,QVector<int> vector);
    void t6_datamodel_dataChanged(QModelIndex index1,QModelIndex index2,QVector<int> vector);
    void t7_datamodel_dataChanged(QModelIndex index1,QModelIndex index2,QVector<int> vector);
    void t14_datamodel_dataChanged(QModelIndex index1,QModelIndex index2,QVector<int> vector);
    void t15_datamodel_dataChanged(QModelIndex index1,QModelIndex index2,QVector<int> vector);
    void t16_datamodel_dataChanged(QModelIndex index1,QModelIndex index2,QVector<int> vector);
    void t13_datamodel_dataChanged(QModelIndex index1,QModelIndex index2,QVector<int> vector);

    void table_view_selection_change(QModelIndex current, QModelIndex previous);

    void on_t1_LE_Lot_count_editingFinished();

    void on_t1_LE_Run_number_editingFinished();

    void on_t1_Input_btn_clicked();

    void on_t1_search_btn_clicked();

    void on_t1_BT_search_lot_id_clicked();

    void on_t1_ti_al_refresh_clicked();

    void on_t1_DT_del_btn_clicked();

    void on_t2_LE_Lot_count_editingFinished();

    void on_t2_LE_Run_number_editingFinished();

    void on_t2_Input_btn_clicked();

    void on_t2_search_btn_clicked();

    void on_t2_BT_search_lot_id_clicked();

    void on_t2_ti_al_refresh_clicked();

    void on_t3_LE_Lot_count_editingFinished();

    void on_t3_LE_Run_number_editingFinished();

    void on_t3_Input_btn_clicked();

    void on_t3_search_btn_clicked();

    void on_t3_BT_search_lot_id_clicked();

    void on_t3_ti_al_refresh_clicked();

    void on_t4_LE_Lot_count_editingFinished();

    void on_t4_LE_Run_number_editingFinished();

    void on_t4_Input_btn_clicked();

    void on_t4_search_btn_clicked();

    void on_t4_BT_search_lot_id_clicked();

    void on_t4_ti_al_refresh_clicked();

    void on_t5_LE_Lot_count_editingFinished();

    void on_t5_LE_Run_number_editingFinished();

    void on_t5_Input_btn_clicked();

    void on_t5_search_btn_clicked();

    void on_t5_BT_search_lot_id_clicked();

    void on_t5_ti_al_refresh_clicked();

    void on_t6_LE_Lot_count_editingFinished();

    void on_t6_LE_Run_number_editingFinished();

    void on_t6_Input_btn_clicked();

    void on_t6_search_btn_clicked();

    void on_t6_BT_search_lot_id_clicked();

    void on_t6_ti_al_refresh_clicked();

    void on_t7_LE_Lot_count_editingFinished();

    void on_t7_LE_Run_number_editingFinished();

    void on_t7_Input_btn_clicked();

    void on_t7_search_btn_clicked();

    void on_t7_BT_search_lot_id_clicked();

    void on_t7_ti_al_refresh_clicked();

    void on_t14_LE_Lot_count_editingFinished();

    void on_t14_LE_Run_number_editingFinished();

    void on_t14_Input_btn_clicked();

    void on_t14_search_btn_clicked();

    void on_t14_BT_search_lot_id_clicked();

    void on_t14_ti_al_refresh_clicked();

    void on_t15_LE_Lot_count_editingFinished();

    void on_t15_LE_Run_number_editingFinished();

    void on_t15_Input_btn_clicked();

    void on_t15_search_btn_clicked();

    void on_t15_BT_search_lot_id_clicked();

    void on_t15_ti_al_refresh_clicked();

    void on_t16_LE_Lot_count_editingFinished();

    void on_t16_LE_Run_number_editingFinished();

    void on_t16_Input_btn_clicked();

    void on_t16_search_btn_clicked();

    void on_t16_BT_search_lot_id_clicked();

    void on_t16_ti_al_refresh_clicked();

    void on_t13_LE_Lot_count_editingFinished();

    void on_t13_LE_Run_number_editingFinished();

    void on_t13_Input_btn_clicked();

    void on_t13_search_btn_clicked();

    void on_t13_BT_search_lot_id_clicked();

    void on_t13_ti_al_refresh_clicked();

    void on_t13_DT_del_btn_clicked();

    void on_t2_DT_del_btn_clicked();

    void on_t3_DT_del_btn_clicked();

    void on_t4_DT_del_btn_clicked();

    void on_t5_DT_del_btn_clicked();

    void on_t6_DT_del_btn_clicked();

    void on_t7_DT_del_btn_clicked();

    void on_t14_DT_del_btn_clicked();

    void on_t15_DT_del_btn_clicked();

    void on_t16_DT_del_btn_clicked();

    void on_t1_move_machine_btn_clicked();

    void on_t2_move_machine_btn_clicked();

    void on_t3_move_machine_btn_clicked();

    void on_t4_move_machine_btn_clicked();

    void on_t5_move_machine_btn_clicked();

    void on_t6_move_machine_btn_clicked();

    void on_t7_move_machine_btn_clicked();

    void on_t14_move_machine_btn_clicked();

    void on_t15_move_machine_btn_clicked();

    void on_t16_move_machine_btn_clicked();

    void on_t13_move_machine_btn_clicked();

    void on_t1_excel_header_copy_flag_toggled(bool checked);

    void on_t2_excel_header_copy_flag_toggled(bool checked);

    void on_t3_excel_header_copy_flag_toggled(bool checked);

    void on_t4_excel_header_copy_flag_toggled(bool checked);

    void on_t5_excel_header_copy_flag_toggled(bool checked);

    void on_t6_excel_header_copy_flag_toggled(bool checked);

    void on_t7_excel_header_copy_flag_toggled(bool checked);

    void on_t14_excel_header_copy_flag_toggled(bool checked);

    void on_t15_excel_header_copy_flag_toggled(bool checked);

    void on_t16_excel_header_copy_flag_toggled(bool checked);

    void on_t13_excel_header_copy_flag_toggled(bool checked);

    void on_t1_machine_temp_run_mode_btn_clicked();

    void on_t2_machine_temp_run_mode_btn_clicked();

    void on_t3_machine_temp_run_mode_btn_clicked();

    void on_t4_machine_temp_run_mode_btn_clicked();

    void on_t5_machine_temp_run_mode_btn_clicked();

    void on_t6_machine_temp_run_mode_btn_clicked();

    void on_t7_machine_temp_run_mode_btn_clicked();

    void on_t14_machine_temp_run_mode_btn_clicked();

    void on_t15_machine_temp_run_mode_btn_clicked();

    void on_t16_machine_temp_run_mode_btn_clicked();

    void on_t13_machine_temp_run_mode_btn_clicked();

    void temp_run_mode_timeout();

    void on_t8_search_btn_clicked();

    void t8_fast_search();

    void on_t8_add_row_btn_clicked();

    void on_t8_au_add_del_row_btn_clicked();

    void sync_now_date(QString machine_code,QLineEdit *au_add,QLineEdit *now_r_weight,QLineEdit *au_after_r_weight,QLineEdit *au_after_weight);

    void t1_lot_edit_finish();

    void on_t8_up_btn_clicked();

    void on_t8_down_btn_clicked();

    void temprunmode_gui_slot(QString value,QLineEdit *view);

    void all_refresh_graph();

    void on_tabWidget_currentChanged(int index);


private:
    Ui::Thin_film_mainwindows *ui;
};

#endif // THIN_FILM_MAINWINDOWS_H
