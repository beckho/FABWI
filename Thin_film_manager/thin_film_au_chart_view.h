#ifndef THIN_FILM_AU_CHART_VIEW_H
#define THIN_FILM_AU_CHART_VIEW_H

#include <QObject>
#include <QWidget>
#include <QtCharts/QChartView>
#include <QtWidgets/QRubberBand>
#include <QDebug>
#include <QPointF>
#include <QTimer>
QT_CHARTS_USE_NAMESPACE
class Thin_film_au_chart_view : public QChartView
{
    Q_OBJECT
public:
    Thin_film_au_chart_view(QChart *chart, QWidget *parent = 0);
    void mouseMoveEvent(QMouseEvent *event);
    QChart *mchart;
signals :
    void move_value(QPointF value);
private:
     void keyPressEvent(QKeyEvent *event);
};

#endif // THIN_FILM_AU_CHART_VIEW_H
