#ifndef THIN_MODIFY_PASSWORD_POPUP_H
#define THIN_MODIFY_PASSWORD_POPUP_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
namespace Ui {
class Thin_modify_password_popup;
}

class Thin_modify_password_popup : public QDialog
{
    Q_OBJECT

public:
    explicit Thin_modify_password_popup(QSqlDatabase my_mesdb,QWidget *parent = 0);
    bool password_alright;
    QSqlDatabase my_mesdb;
    ~Thin_modify_password_popup();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::Thin_modify_password_popup *ui;
};

#endif // THIN_MODIFY_PASSWORD_POPUP_H
