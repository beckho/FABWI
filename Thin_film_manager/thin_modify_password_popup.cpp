#include "thin_modify_password_popup.h"
#include "ui_thin_modify_password_popup.h"

Thin_modify_password_popup::Thin_modify_password_popup(QSqlDatabase my_mesdb, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Thin_modify_password_popup)
{
    ui->setupUi(this);
    password_alright=false;
    this->my_mesdb = my_mesdb;
}
Thin_modify_password_popup::~Thin_modify_password_popup()
{
    delete ui;
}

void Thin_modify_password_popup::on_buttonBox_accepted()
{
    QSqlQuery query(my_mesdb);
    query.exec("select * from Thin_flim_manager");
    if(query.next()){
        QString value = query.value("password").toString();
        if(value == ui->LE_pw->text()){
            password_alright = true;
        }
    }
}
