#ifndef THIN_WET_SQLTABLE_MODEL_H
#define THIN_WET_SQLTABLE_MODEL_H
#include <QObject>
#include <QWidget>
#include <QSqlTableModel>
#include <QDebug>
#include <QModelIndex>
#include <QSqlRecord>
#include <QSqlField>
#include <QDateTime>
#include <QSqlQuery>
#include <QSqlError>
#include <QProgressBar>
class Thin_wet_sqltable_model:public QSqlTableModel
{
     Q_OBJECT
public:
    Thin_wet_sqltable_model(QObject *parent,QSqlDatabase db);
    Qt::ItemFlags flags(const QModelIndex & index) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const ;
    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
    void calc_wet_accumulate(QDateTime input_time, QString wet_machine);
signals:
    void scroll_bottom();
};

#endif // THIN_WET_SQLTABLE_MODEL_H
