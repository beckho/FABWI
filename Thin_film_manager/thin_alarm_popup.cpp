#include "thin_alarm_popup.h"
#include "ui_thin_alarm_popup.h"

Thin_alarm_popup::Thin_alarm_popup(QSqlDatabase my_mes_db,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Thin_alarm_popup)
{
    ui->setupUi(this);
    this->my_mes_db = my_mes_db;
    this->showFullScreen();
    main_timer.setInterval(10000);
    connect(&main_timer,SIGNAL(timeout()),this,SLOT(main_time_out()));
    connect(&screen_timer,SIGNAL(timeout()),this,SLOT(screen_timer_time_out()));
    connect(&main_screen_timer,SIGNAL(timeout()),this,SLOT(main_screen_timer_time_out()));
    main_timer.start();
    main_time_out();
    main_screen_timer.start();
    main_screen_timer.setInterval(2000);
    main_screen_count = 0;

}

void Thin_alarm_popup::closeEvent(QCloseEvent *)
{
    this->deleteLater();
}

Thin_alarm_popup::~Thin_alarm_popup()
{
    delete ui;
}

void Thin_alarm_popup::main_time_out()
{
    QSqlQuery query(my_mes_db);
    query.exec(" select * from Thin_film_cleaning_view where AL_UCL_flag = '1' OR Ti_UCL_flag = '1'");
    int count = 0;
    while(query.next()){
        QStringList item;
        item.append(query.value("machine_name").toString());
        item.append(query.value("machine_code").toString());
        screen_quque.append(item);
        count ++;
    }
    if(count == 0){
        this->close();
    }
    if(count > 0){
        main_timer.setInterval(5000*count+5000);
        screen_timer.setInterval(5000);
        screen_timer.start();
        screen_timer_time_out();
    }
}

void Thin_alarm_popup::screen_timer_time_out()
{
    if(!screen_quque.isEmpty()){
        QStringList item = screen_quque.takeFirst();
        ui->LA_Machine_name->setText(item.at(0));
        if(!ui->CB_sound_flag->isChecked()){
            QSound::play(QString(":/pm_sound/pm_sound/%1.wav").arg(item.at(1)));
        }
    }else {
        screen_timer.stop();
    }
}

void Thin_alarm_popup::main_screen_timer_time_out()
{
    main_screen_count++;
    if(main_screen_count%2 == 0){
        this->setStyleSheet("background-color: rgb(255, 0, 0);");
    }else {
        this->setStyleSheet("background-color: rgb(255, 144, 144);");
    }
}
