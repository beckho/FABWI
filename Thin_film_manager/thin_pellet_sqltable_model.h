#ifndef THIN_PELLET_SQLTABLE_MODEL_H
#define THIN_PELLET_SQLTABLE_MODEL_H
#include <QObject>
#include <QWidget>
#include <QSqlTableModel>
#include <QDebug>
#include <QModelIndex>
#include <QSqlRecord>
#include <QSqlField>
#include <QDateTime>
#include <QSqlQuery>
#include <QSqlError>
#include <QMessageBox>
#include <QProgressBar>
class thin_pellet_sqltable_model : public QSqlTableModel
{
    Q_OBJECT
public:
    thin_pellet_sqltable_model(QObject *parent,QSqlDatabase db);
    Qt::ItemFlags flags(const QModelIndex & index) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const ;
    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
    void au_calc(QDateTime input_time);
    QTime query_start_time;
    QTime query_end_time;
signals:
    void search();

};

#endif // THIN_PELLET_SQLTABLE_MODEL_H
