#ifndef INPUT_LOT_ITEM_H
#define INPUT_LOT_ITEM_H

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <Thin_film_manager/thin_film_manager_input_lot_item.h>
#include <Thin_film_manager/input_item.h>
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QUrlQuery>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

namespace Ui {
class input_lot_item;
}

class input_lot_item : public QWidget
{
    Q_OBJECT

public:
    explicit input_lot_item(QSqlDatabase db,input_item *inputitem,QString machine_code,QWidget *parent = 0);
    QSqlDatabase ms_mesdb;
    QString machine_code;
    input_item *inputitem;
    QEventLoop loop;
    Ui::input_lot_item *ui;
    QNetworkAccessManager *manager;

    void clear_data();
    ~input_lot_item();
signals:
    void lot_find_item(thin_film_manager_input_lot_item item);
    void lot_editing_finished();

private slots:
    void on_LE_Lot_id_editingFinished();
    void on_LE_wafer_count_editingFinished();

private:

};

#endif // INPUT_LOT_ITEM_H
