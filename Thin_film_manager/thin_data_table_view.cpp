#include "thin_data_table_view.h"

thin_data_table_view::thin_data_table_view(QWidget *parent):QTableView(parent)
{
    copyheader_flag = false;

}

void thin_data_table_view::keyPressEvent(QKeyEvent *event)
{
    // If Ctrl-C typed
     if (event->key() == Qt::Key_C && (event->modifiers() & Qt::ControlModifier))
     {
         QModelIndexList cells = selectedIndexes();

         qSort(cells); // Necessary, otherwise they are in column order

         QString text;

         int currentRow = 0; // To determine when to insert newlines
         int first_row=0;
         foreach (const QModelIndex& cell, cells) {
             if (text.length() == 0) {
                 // First item
                 first_row = cell.row();
             } else if (cell.row() != currentRow) {
                 // New row
                 text += '\n';
             } else {
                 // Next cell
                 text += '\t';
             }
             currentRow = cell.row();

             text += cell.data().toString().replace("\n","").replace("\r","");
         }
         QString header_data;
         foreach (const QModelIndex& cell, cells) {
             if(header_data.length() == 0){

             }else if (first_row != cell.row()){
                 header_data += '\n';
                 break;
             }else {
                 header_data += '\t';
             }
             header_data += model()->headerData(cell.column(),Qt::Horizontal).toString();

         }
         if(header_data.indexOf('\n',0)== -1){
             header_data += '\n';
         }
         if(copyheader_flag){
             text = header_data + text;
         }else {

         }
         QApplication::clipboard()->setText(text);
         return ;
     }
//     qDebug()<<event->key();
//     if(event->key() == Qt::Key_Right){
//        QModelIndex current = currentIndex();
//        setCurrentIndex(model()->index(current.row(),current.column()+1));
//     }
//     if(event->key() == Qt::Key_Left){
//         QModelIndex current = currentIndex();
//         setCurrentIndex(model()->index(current.row(),current.column()-1));
//     }
//     if(event->key() == Qt::Key_Up){
//         QModelIndex current = currentIndex();
//         setCurrentIndex(model()->index(current.row()-1,current.column()));
//     }
//     if(event->key() == Qt::Key_Down){
//         QModelIndex current = currentIndex();
//         setCurrentIndex(model()->index(current.row()+1,current.column()));
//     }
     QTableView::keyPressEvent(event);

}
