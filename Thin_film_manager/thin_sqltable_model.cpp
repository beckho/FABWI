#include "thin_sqltable_model.h"

thin_sqltable_model::thin_sqltable_model(QObject *parent, QSqlDatabase db,QString tab_mode,QSqlTableModel *spec_motel):QSqlTableModel(parent,db)
{
    this->specmodel = spec_motel;
    this->tabmode = tab_mode;
    this->db = db;
}
Qt::ItemFlags thin_sqltable_model::flags(const QModelIndex &index) const
{
    if(index.column() == fieldIndex("N2_Blow") || index.column() == fieldIndex("D_use")){
        return QSqlTableModel::flags(index)|Qt::ItemIsUserCheckable;
    }
    if(index.column() == fieldIndex("Au_f_add")){
        return Qt::ItemIsSelectable|Qt::ItemIsEnabled;
    }
    return QSqlTableModel::flags(index);
}
QVariant thin_sqltable_model::data(const QModelIndex &index, int role) const
{
    if(index.column() == fieldIndex("vacuum_time") && role == Qt::DisplayRole){
           QDateTime datetime =  record(index.row()).value(fieldIndex("vacuum_time")).toDateTime();
           return datetime.toString("yyyy-MM-dd hh:mm");
    }
    if(index.column() == fieldIndex("Input_time") && role == Qt::DisplayRole){
           QDateTime datetime =  record(index.row()).value(fieldIndex("Input_time")).toDateTime();
           return datetime.toString("yyyy-MM-dd hh:mm:ss");
    }

    if(index.column() == fieldIndex("N2_Blow") && role == Qt::DisplayRole){
        if(record(index.row()).value(fieldIndex("N2_Blow")).toBool()){
            return "O";
        }else {
            return "X";
        }
    }
    if(index.column() == fieldIndex("N2_Blow") && role == Qt::CheckStateRole)
   {
        if(record(index.row()).value(fieldIndex("N2_Blow")).toBool()){
            return Qt::Checked;
        }else {
            return Qt::Unchecked;
        }
    }
    if(index.column() == fieldIndex("D_use") && role == Qt::DisplayRole){
        if(record(index.row()).value(fieldIndex("D_use")).toBool()){
            return "O";
        }else {
            return "X";
        }
    }
    if(index.column() == fieldIndex("D_use") && role == Qt::CheckStateRole)
   {
        if(record(index.row()).value("D_use").toBool()){
            return Qt::Checked;
        }else {
            return Qt::Unchecked;
        }
    }
    if(index.column() == fieldIndex("Pad_thin") &&  role == Qt::BackgroundRole){
        //LCL
        if(specmodel->index(0,specmodel->fieldIndex("LCL")).data().toDouble()>record(index.row()).value("Pad_thin").toDouble()){
            return QBrush(QColor("#8cbaff"));
        }
        //UCL
        if(specmodel->index(0,specmodel->fieldIndex("UCL")).data().toDouble()<record(index.row()).value("Pad_thin").toDouble()){
            return QBrush(QColor("#ffb5bd"));
        }
    }else if (index.column() == fieldIndex("Recipe") && role == Qt::BackgroundRole) {
        QSqlQuery query(database());
        query.exec(QString("select * from `Thin_film_repcipe` where `machine_code` = '%1' AND `metal_name` = '%2' AND `metal_thin` = '%3'")
                   .arg(record(index.row()).value(fieldIndex("machine_code")).toString())
                   .arg(record(index.row()).value(fieldIndex("Recipe")).toString())
                   .arg(record(index.row()).value(fieldIndex("Input_thin_data")).toString())
                   );

        if(query.next()){
            return QBrush(QColor(query.value("color").toString()));
        }else {
            return QBrush(QColor("white"));
        }
    }else if(index.column() == fieldIndex("note") && role == Qt::BackgroundRole){
        if(record(index.row()).value("note").toString() == tr("x-tal change")){
            return QBrush(QColor("#d7fffe"));
        }
    }

//    if(index.column() == fieldIndex("Pad_thin") && role == Qt::ForegroundRole){
//        //LCL
//        if(specmodel->index(0,specmodel->fieldIndex("LCL")).data().toDouble()>=record(index.row()).value("Pad_thin").toDouble()){
//            return QBrush(QColor("white"));
//        }
//        //UCL
//        if(specmodel->index(0,specmodel->fieldIndex("UCL")).data().toDouble()<=record(index.row()).value("Pad_thin").toDouble()){
//            return QBrush(QColor("white"));
//        }
//    }

    return QSqlTableModel::data(index, role);
}


bool thin_sqltable_model::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.column() == fieldIndex("N2_Blow") && role == Qt::CheckStateRole)
    {
        bool result = false;
        if ( value == Qt::Checked ){
            QSqlRecord recode = record(index.row());
            recode.setGenerated("N2_Blow",true);
            recode.setGenerated("Input_time",true);
            recode.setGenerated("batch_id",true);
            recode.setValue("N2_Blow",Qt::Checked);
            result = setRecord(index.row(),recode);
        }
        else if(value == Qt::Unchecked) {
            QSqlRecord recode = record(index.row());
            recode.setGenerated("N2_Blow",true);
            recode.setGenerated("Input_time",true);
            recode.setGenerated("batch_id",true);
            recode.setValue("N2_Blow",Qt::Unchecked);
            result = setRecord(index.row(),recode);
        }
        emit dataChanged(index, index );
        qDebug()<<result;
        return result;
    }


    if(index.column() == fieldIndex("D_use")  && role == Qt::CheckStateRole)
    {
        bool result = false;
        if ( value == Qt::Checked ){
            QSqlRecord recode = record(index.row());
            recode.setGenerated("D_use",true);
            recode.setGenerated("Input_time",true);
            recode.setGenerated("batch_id",true);
            recode.setValue("D_use",Qt::Checked);
            setRecord(index.row(),recode);
        }
        else if(value == Qt::Unchecked){
            QSqlRecord recode = record(index.row());
            recode.setGenerated("D_use",true);
            recode.setGenerated("Input_time",true);
            recode.setGenerated("batch_id",true);
            recode.setValue("D_use",Qt::Unchecked);
            setRecord(index.row(),recode);
        }
        emit dataChanged(index, index );

        qDebug()<<result;
        return result;
    }
    if(index.column() == fieldIndex("Recipe")  && role == Qt::EditRole)
    {
        Thin_modify_password_popup popup(db);
        if((popup.exec()== QDialog::Accepted) && popup.password_alright){
           emit dataChanged(index, index );
           return QSqlTableModel::setData(index, value, role);
        }else {
            return false;
        }
    }
    if(index.column() == fieldIndex("Input_thin_data")  && role == Qt::EditRole)
    {
        Thin_modify_password_popup popup(db);
        if((popup.exec()== QDialog::Accepted) && popup.password_alright){
           emit dataChanged(index, index );
           return QSqlTableModel::setData(index, value, role);
        }else {
            return false;
        }
    }

    return QSqlTableModel::setData(index, value, role);

}
