#ifndef DATEDELEGATE_H
#define DATEDELEGATE_H

#include <QItemDelegate>
#include <QDateTimeEdit>
#include <QPainter>
class DateDelegate : public QItemDelegate
{
public:
    DateDelegate(QWidget *parent = 0);
    DateDelegate(QString timeformat,QWidget *parent = 0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex& index) const;
    void updateEditorGeometry( QWidget *editor,
                            const QStyleOptionViewItem &option,
                            const QModelIndex &index ) const;
    mutable QDateTimeEdit *dataTimeEdit;
    mutable QString timeformat;
    mutable bool format_use;
private slots:

    void setData(QDateTime val);

};

#endif // DATEDELEGATE_H
