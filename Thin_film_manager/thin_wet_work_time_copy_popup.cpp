#include "thin_wet_work_time_copy_popup.h"
#include "ui_thin_wet_work_time_copy_popup.h"

Thin_wet_work_time_copy_popup::Thin_wet_work_time_copy_popup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Thin_wet_work_time_copy_popup)
{
    ui->setupUi(this);
    ui->LE_work_time->setValidator(new QIntValidator(-999,999));
}

Thin_wet_work_time_copy_popup::~Thin_wet_work_time_copy_popup()
{
    delete ui;
}

void Thin_wet_work_time_copy_popup::on_LE_work_time_textEdited(const QString &arg1)
{
    now_value = arg1;
}

void Thin_wet_work_time_copy_popup::on_LE_work_time_textChanged(const QString &arg1)
{
    now_value = arg1;
}

void Thin_wet_work_time_copy_popup::on_buttonBox_accepted()
{
    now_value = ui->LE_work_time->text();
}
