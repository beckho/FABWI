#ifndef THIN_SQLTABLE_MODEL_H
#define THIN_SQLTABLE_MODEL_H
#include <QObject>
#include <QWidget>
#include <QSqlTableModel>
#include <QDebug>
#include <QModelIndex>
#include <QSqlRecord>
#include <QSqlField>
#include <QDateTime>
#include <QSqlQuery>
#include <QSqlError>
#include <Thin_film_manager/thin_modify_password_popup.h>

class thin_sqltable_model :public QSqlTableModel
{
    Q_OBJECT
public:
    thin_sqltable_model(QObject *parent,QSqlDatabase db,QString tab_mode,QSqlTableModel *spec_motel);
    Qt::ItemFlags flags(const QModelIndex & index) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const ;
    QSqlTableModel *specmodel;
    QSqlDatabase db;
    QString tabmode;
    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

    QSet<int> m_idList;
private:

};

#endif // THIN_SQLTABLE_MODEL_H
