#include "thin_lot_find_popup.h"
#include "ui_thin_lot_find_popup.h"

Thin_lot_find_popup::Thin_lot_find_popup(QSqlDatabase db, QString lot_name, QString wet_machine,
                                         Thin_wet_sqltable_model * parent_model, QString worker, QDateTimeEdit *start_date_time,QDateTimeEdit *end_date_time, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Thin_lot_find_popup)
{
    ui->setupUi(this);
    this->my_mesdb = db;
    this->lot_name = lot_name;
    this->wet_machine =wet_machine;
    this->parent_model= parent_model;
    this->worker = worker;
    this->start_date_time = start_date_time;
    this->end_date_time = end_date_time;
    data_model = new QSqlTableModel(this,my_mesdb);
    ui->lot_find_table->setModel(data_model);
    data_model->setTable("Thin_wet_lot_find_view");
    QSqlQuery query(my_mesdb);
    query.exec(QString("select * from Thin_wet_lot_find_view where Lot_id = '%1'").arg(lot_name));

    QStringList Filter_list;
    QString Filter_txt;
    while(query.next()){
         Filter_list.append(QString(" (machine_name = '%1' AND batch_id = '%2') ").arg(query.value("machine_name").toString()).arg(query.value("batch_id").toString()));
    }
    if(Filter_list.count() == 0){

    }else {
        for(int i=0;i<Filter_list.count();i++){
            if(i == Filter_list.count()-1 ){
                Filter_txt.append(Filter_list.at(i));
            }else {
                Filter_txt.append(Filter_list.at(i) + " OR " );
            }
        }
        data_model->setFilter(Filter_txt);
        data_model->select();
    }

    data_model->setHeaderData(data_model->fieldIndex("day_and_night"),Qt::Horizontal,tr("day_and_night"));
    data_model->setHeaderData(data_model->fieldIndex("vacuum_time"),Qt::Horizontal,tr("vacuum_time"));
    data_model->setHeaderData(data_model->fieldIndex("Run_number"),Qt::Horizontal,tr("Run_number"));
    data_model->setHeaderData(data_model->fieldIndex("machine_type"),Qt::Horizontal,tr("machine_type"));
    data_model->setHeaderData(data_model->fieldIndex("Lot_id"),Qt::Horizontal,tr("Lot_id"));
    data_model->setHeaderData(data_model->fieldIndex("Wafer_count"),Qt::Horizontal,tr("Wafer_count"));
    data_model->setHeaderData(data_model->fieldIndex("Wafer_angle"),Qt::Horizontal,tr("Wafer_angle"));
    data_model->setHeaderData(data_model->fieldIndex("Recipe"),Qt::Horizontal,tr("Recipe"));
    data_model->setHeaderData(data_model->fieldIndex("User_name"),Qt::Horizontal,tr("User_name"));
    data_model->setHeaderData(data_model->fieldIndex("Input_time"),Qt::Horizontal,tr("Input_time"));
    data_model->setHeaderData(data_model->fieldIndex("machine_name"),Qt::Horizontal,tr("machine_name"));
    ui->lot_find_table->horizontalHeader()->resizeSection(data_model->fieldIndex("day_and_night"),50);
    ui->lot_find_table->horizontalHeader()->resizeSection(data_model->fieldIndex("vacuum_time"),150);
    ui->lot_find_table->horizontalHeader()->resizeSection(data_model->fieldIndex("Run_number"),60);
    ui->lot_find_table->horizontalHeader()->resizeSection(data_model->fieldIndex("machine_type"),150);
    ui->lot_find_table->horizontalHeader()->resizeSection(data_model->fieldIndex("Lot_id"),150);
    ui->lot_find_table->horizontalHeader()->resizeSection(data_model->fieldIndex("Wafer_count"),60);
    ui->lot_find_table->horizontalHeader()->resizeSection(data_model->fieldIndex("Recipe"),200);
    ui->lot_find_table->horizontalHeader()->resizeSection(data_model->fieldIndex("Input_time"),150);
    qDebug()<<data_model->query().lastQuery();
    qDebug()<<data_model->query().lastError().text();

}

void Thin_lot_find_popup::calc_wet_accumulate(QDateTime input_time, QString wet_machine)
{
    QSqlQuery query(my_mesdb);
    query.exec(QString("select work_time,max(`chemical_accumulate`) AS `chemical_accumulate`,wet_machine "
                       "from depostion_wet_accumulate_manager "
                       "where work_time < '%1' AND wet_machine = '%2' group by work_time order by work_time desc LIMIT 1")
               .arg(input_time.toString("yyyy-MM-dd hh:mm:00")).arg(wet_machine));

    int before_accumulate = 0;
    if(query.next()){
        before_accumulate = query.value("chemical_accumulate").toInt();
    }
    query.exec(QString("select * from depostion_wet_accumulate_manager where work_time >= '%1' AND wet_machine = '%2' order by work_time asc")
               .arg(input_time.toString("yyyy-MM-dd hh:mm:00")).arg(wet_machine));

    while(query.next()){
        int chemical_change = query.value("chemical_change").toInt();
        int now_accumulate =0;
        if(chemical_change){
            now_accumulate = query.value("wafer_count").toInt();
        }else {
            now_accumulate = before_accumulate+query.value("wafer_count").toInt();
        }
        QSqlQuery query1(my_mesdb);
        query1.exec(QString("update `depostion_wet_accumulate_manager` set chemical_accumulate = %1 where work_time = '%2' AND "
                            "wet_machine = '%3' AND model_type = '%4' AND lot_number = '%5' ")
                    .arg(now_accumulate).arg(query.value("work_time").toDateTime().toString("yyyy-MM-dd hh:mm:00"))
                    .arg(query.value("wet_machine").toString()).arg(query.value("model_type").toString())
                    .arg(query.value("lot_number").toString()));

        before_accumulate = now_accumulate;
    }
    end_date_time->setDateTime(QDateTime::currentDateTime().addSecs(300));
    parent_model->setFilter(QString("wet_machine = '%1' AND work_time between '%2' AND '%3'")
                             .arg(wet_machine)
                             .arg(start_date_time->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                             .arg(end_date_time->dateTime().toString("yyyy-MM-dd hh:mm:ss")));
    parent_model->select();
}

Thin_lot_find_popup::~Thin_lot_find_popup()
{
    delete ui;
}

void Thin_lot_find_popup::closeEvent(QCloseEvent *)
{
    data_model->deleteLater();
    this->deleteLater();
}

void Thin_lot_find_popup::on_select_insert_btn_clicked()
{
    QSqlQuery query(my_mesdb);
    query.exec("select now() as now_time");
    QDateTime now_time;
    if(query.next()){
       now_time = query.value("now_time").toDateTime();
    }
    QModelIndexList index_list = ui->lot_find_table->selectionModel()->selectedIndexes();
    QVector<int> row_history;
    for(int i=0;i<index_list.count();i++){
        int row = index_list.at(i).row();
        if(!row_history.contains(row)){
            QString model_type =  data_model->record(row).value("machine_type").toString();
            QString lot_number =  data_model->record(row).value("Lot_id").toString();
            QString Wafer_angle =  data_model->record(row).value("Wafer_angle").toString();
            QString Wafer_count =  data_model->record(row).value("Wafer_count").toString();
            query.exec(QString("insert into `depostion_wet_accumulate_manager` (`work_time`,`model_type`,`lot_number`,`wafer_angle`,`wafer_count`,"
                               "`chemical_accumulate`,`chemical_change`,`worker`,`wet_machine`) VALUES ('%1','%2','%3','%4','%5','%6','%7','%8','%9')")
                       .arg(now_time.toString("yyyy-MM-dd hh:mm:00")).arg(model_type).arg(lot_number).arg(Wafer_angle)  //4
                       .arg(Wafer_count).arg("0").arg("0").arg(worker).arg(wet_machine));
            row_history.append(row);
        }
    }
    calc_wet_accumulate(now_time,wet_machine);
    QMessageBox msg;
    msg.addButton(QMessageBox::Ok);
    msg.setText(tr("insert completet"));
    msg.exec();
}
