#ifndef THIN_TEMP_RUNMODE_TH_H
#define THIN_TEMP_RUNMODE_TH_H

#include <QObject>
#include <QWidget>
#include <Qthread>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <global_define.h>
#include <QSettings>
#include <QLineEdit>
#include <QDateTime>
#include <QTime>
#include <QDebug>
#include <QApplication>
class thin_temp_runmode_th : public QThread
{
    Q_OBJECT
public:
    thin_temp_runmode_th(QHash<QString,QLineEdit *> temp_run_widget_map
                         ,QHash<QString,QLineEdit *> temp_run_ti_accmulate_widget_map
                         ,QHash<QString,QLineEdit *> temp_run_al_accmulate_widget_map);
    QSqlDatabase my_mesdb;
    QHash<QString,QLineEdit *> temp_run_widget_map;
    QHash<QString,QLineEdit *> temp_run_ti_accmulate_widget_map;
    QHash<QString,QLineEdit *> temp_run_al_accmulate_widget_map;

    ~thin_temp_runmode_th();


private :
    void run();
signals:
    void change_gui(QString value,QLineEdit *view);

};

#endif // THIN_TEMP_RUNMODE_TH_H
