#include "thin_pellet_sqltable_model.h"
#include <Thin_film_manager/thin_data_table_view.h>

thin_pellet_sqltable_model::thin_pellet_sqltable_model(QObject *parent, QSqlDatabase db):QSqlTableModel(parent,db)
{

}

Qt::ItemFlags thin_pellet_sqltable_model::flags(const QModelIndex &index) const
{
    if(index.column() == fieldIndex("CHA#1")){
        return Qt::ItemIsSelectable|Qt::ItemIsEnabled;
    }else if(index.column() == fieldIndex("CHA#2")){
        return Qt::ItemIsSelectable|Qt::ItemIsEnabled;
    }else if(index.column() == fieldIndex("CHA#3")){
        return Qt::ItemIsSelectable|Qt::ItemIsEnabled;
    }else if(index.column() == fieldIndex("CHA#4")){
        return Qt::ItemIsSelectable|Qt::ItemIsEnabled;
    }else if(index.column() == fieldIndex("BPS")){
        return Qt::ItemIsSelectable|Qt::ItemIsEnabled;
    }else if(index.column() == fieldIndex("EVATEC")){
        return Qt::ItemIsSelectable|Qt::ItemIsEnabled;
    }else if(index.column() == fieldIndex("hanil")){
        return Qt::ItemIsSelectable|Qt::ItemIsEnabled;
    }else if(index.column() == fieldIndex("au_pellet_total")){
        return Qt::ItemIsSelectable|Qt::ItemIsEnabled;
    }else if(index.column() == fieldIndex("diff")){
        return Qt::ItemIsSelectable|Qt::ItemIsEnabled;
    }else if(index.column() == fieldIndex("check_user")){
        return Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable;
    }else if(index.column() == fieldIndex("au_pellet_output")){
        return Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable;
    }else if(index.column() == fieldIndex("day_and_night")){
        return Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable;
    }else if(index.column() == fieldIndex("vacuum_time")){
        return Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable;
    }else if(index.column() == fieldIndex("work_start_au")){
        return Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable;
    }
    return QSqlTableModel::flags(index);
}

QVariant thin_pellet_sqltable_model::data(const QModelIndex &index, int role) const
{
    if(index.column() == fieldIndex("vacuum_time") && role == Qt::DisplayRole){
           QDateTime datetime =  record(index.row()).value(fieldIndex("vacuum_time")).toDateTime();
           return datetime.toString("yyyy-MM-dd hh:mm");
    }
    if(index.column() == fieldIndex("au_pellet_output") && role == Qt::DisplayRole){
           QDateTime datetime =  record(index.row()).value(fieldIndex("au_pellet_output")).toDateTime();
           return datetime.toString("yyyy-MM-dd hh:mm");
    }
    if(index.column() == fieldIndex("Input_time") && role == Qt::DisplayRole){
           QDateTime datetime =  record(index.row()).value(fieldIndex("Input_time")).toDateTime();
           return datetime.toString("yyyy-MM-dd hh:mm:ss");
    }
    if(index.column() == fieldIndex("OKNG") && role == Qt::BackgroundRole){
        double diff = record(index.row()).value("OKNG").toDouble();
        double abs_diff = abs(diff);
        if(abs_diff>0.1){
            return QBrush(QColor("red"));
        }else {
            return QBrush(QColor("white"));
        }
    }
    if(index.column() == fieldIndex("OKNG") && role == Qt::DisplayRole){
        double diff = record(index.row()).value("OKNG").toDouble();
        return QString("%1").arg(diff,0,'f',2);
    }
    return QSqlTableModel::data(index, role);
}

bool thin_pellet_sqltable_model::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.column() == fieldIndex("au_add_weight")){
            QSqlQuery query(database());
            QSqlRecord row_recode = record(index.row());
            int batch_id = row_recode.value("batch_id").toInt();
            QString machine_name = row_recode.value("machine_name").toString();
            query.exec(QString("update `Thin_film_data` set `au_add_weight` = %1 where batch_id = %2 AND machine_name = '%3'")
                       .arg(value.toDouble()).arg(batch_id).arg(row_recode.value("machine_name").toString()));
            query.exec(QString("select * from depostion_au_use_current_view where machine_name = '%1' AND batch_id = %2")
                       .arg(machine_name).arg(batch_id));
            if(query.next()){
                double  OKNG = query.value("OKNG").toDouble();
                if(abs(OKNG)>0.1){
                    QMessageBox msg;
                    msg.addButton(QMessageBox::Ok);
                    msg.setText(tr("au pellet NG"));
                    msg.exec();
                }
            }
            emit search();
    }else if(index.column() == fieldIndex("check_user")){
        QSqlQuery query(database());
        QSqlRecord row_recode = record(index.row());
        int batch_id = row_recode.value("batch_id").toInt();
        query_start_time = QTime::currentTime();
        query.exec(QString("update `Thin_film_data` set `check_user` = '%1' where batch_id = %2 AND machine_name = '%3'")
                   .arg(value.toString()).arg(batch_id).arg(row_recode.value("machine_name").toString()));
        query_end_time = QTime::currentTime();
        QSqlTableModel::setData(index,value.toString(),Qt::EditRole);

        qDebug()<<"check_user 1 = "<<query_end_time.msecsTo(query_start_time);

    }else if(index.column() == fieldIndex("au_add")){
        QSqlQuery query(database());
        QSqlRecord row_recode = record(index.row());
        int batch_id = row_recode.value("batch_id").toInt();
        query.exec(QString("update `Thin_film_data` set `au_add` = %1 where batch_id = %2 AND machine_name = '%3'")
                   .arg(value.toDouble()).arg(batch_id).arg(row_recode.value("machine_name").toString()));
        au_calc(row_recode.value("Input_time").toDateTime());
        emit search();
    }else if(index.column() == fieldIndex("Input_time")){
        QSqlQuery query(database());
        QSqlRecord row_recode = record(index.row());
        int batch_id = row_recode.value("batch_id").toInt();
        query.exec(QString("update `Thin_film_data` set `Input_time` = '%1' where batch_id = %2 AND machine_name = '%3'")
                   .arg(value.toDateTime().toString("yyyy-MM-dd hh:mm:ss")).arg(batch_id).arg(row_recode.value("machine_name").toString()));
        au_calc(value.toDateTime());
        emit search();
    }else if(index.column() == fieldIndex("au_pellet_output")){
        QSqlQuery query(database());
        QSqlRecord row_recode = record(index.row());
        int batch_id = row_recode.value("batch_id").toInt();
        query.exec(QString("update `Thin_film_data` set `au_pellet_output` = '%1' where batch_id = %2 AND machine_name = '%3'")
                   .arg(value.toDateTime().toString("yyyy-MM-dd hh:mm:ss")).arg(batch_id).arg(row_recode.value("machine_name").toString()));
         QSqlTableModel::setData(index,value.toString(),Qt::EditRole);
    }else if(index.column() == fieldIndex("day_and_night")){
        QSqlQuery query(database());
        QSqlRecord row_recode = record(index.row());
        int batch_id = row_recode.value("batch_id").toInt();
        query.exec(QString("update `Thin_film_data` set `day_and_night` = '%1' where batch_id = %2 AND machine_name = '%3'")
                   .arg(value.toString()).arg(batch_id).arg(row_recode.value("machine_name").toString()));
         QSqlTableModel::setData(index,value.toString(),Qt::EditRole);
    }else if(index.column() == fieldIndex("vacuum_time")){
        QSqlQuery query(database());
        QSqlRecord row_recode = record(index.row());
        int batch_id = row_recode.value("batch_id").toInt();
        query.exec(QString("update `Thin_film_data` set `vacuum_time` = '%1' where batch_id = %2 AND machine_name = '%3'")
                   .arg(value.toDateTime().toString("yyyy-MM-dd hh:mm:ss")).arg(batch_id).arg(row_recode.value("machine_name").toString()));
         QSqlTableModel::setData(index,value.toString(),Qt::EditRole);
    }else if(index.column() == fieldIndex("work_start_au")){
        QSqlQuery query(database());
        QSqlRecord row_recode = record(index.row());
        int batch_id = row_recode.value("batch_id").toInt();
        query.exec(QString("update `Thin_film_data` set `work_start_au` = '%1' where batch_id = %2 AND machine_name = '%3'")
                   .arg(value.toDouble()).arg(batch_id).arg(row_recode.value("machine_name").toString()));
         QSqlTableModel::setData(index,value.toString(),Qt::EditRole);
    }


//    thin_data_table_view *prenttble = this->property("parent_table").value<thin_data_table_view *>();
//    prenttble->scrollToBottom();
    return true;
}

void thin_pellet_sqltable_model::au_calc(QDateTime input_time)
{
    QSqlQuery query1(database());
    query_start_time = QTime::currentTime();
    query1.exec(QString("select * from Thin_film_cleaning_view_temp where Input_time<'%1' order by Input_time desc LIMIT 1")
                 .arg(input_time.toString("yyyy-MM-dd hh:mm:ss")));
    query_end_time = QTime::currentTime();
    qDebug()<<"au_calc 1 = "<<query_end_time.msecsTo(query_start_time);
    double before_au_pellet_total = 0;
    QProgressBar *progress_bar = property("progressbar").value<QProgressBar *>();
    if(query1.next()){
        before_au_pellet_total =  query1.value("au_pellet_total").toDouble();
        query_start_time = QTime::currentTime();

        query1.exec(QString("select count(*) as count from Thin_film_cleaning_view_temp where Input_time>='%1' ")
                    .arg(input_time.toString("yyyy-MM-dd hh:mm:ss")));
        int progress_max = 0;
        if(query1.next()){
            progress_max = query1.value("count").toInt();
            progress_bar->setMaximum(progress_max);
        }
        int progress_value = 0;
        query1.exec(QString("select * from Thin_film_cleaning_view_temp where Input_time>='%1' order by Input_time asc")
                    .arg(input_time.toString("yyyy-MM-dd hh:mm:ss")));
        query_end_time = QTime::currentTime();
        qDebug()<<"au_calc 2 = "<<query_end_time.msecsTo(query_start_time);
        while(query1.next()){
            int batch_id = query1.value("batch_id").toInt();
            QString machine_name = query1.value("machine_name").toString();
            double au_pellet_total  = before_au_pellet_total - query1.value("Au_f_add").toDouble() + query1.value("au_add").toDouble();

            QSqlQuery query2(database());
            query_start_time = QTime::currentTime();
            query2.exec(QString("update `Thin_film_data` set `au_pellet_total` = %1 where machine_name = '%2' AND batch_id = %3")
                        .arg(au_pellet_total).arg(machine_name).arg(batch_id));
            query_end_time = QTime::currentTime();
            qDebug()<<"au_calc 3 = "<<query_end_time.msecsTo(query_start_time);
            qDebug()<<query2.lastError().text();
            qDebug()<<query2.lastQuery();
            before_au_pellet_total = au_pellet_total;
            progress_value++;
            progress_bar->setValue(progress_value);
        }
    }
    return;
}
