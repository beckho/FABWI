#ifndef THIN_DATA_TABLE_VIEW_H
#define THIN_DATA_TABLE_VIEW_H

#include <QObject>
#include <QWidget>
#include <QTableView>
#include <QKeyEvent>
#include <QApplication>
#include <QClipboard>
#include <QDebug>
class thin_data_table_view : public QTableView
{
    Q_OBJECT
public:
    explicit thin_data_table_view(QWidget *parent = 0);
    bool copyheader_flag;
private:
    void keyPressEvent(QKeyEvent *event);
};

#endif // THIN_DATA_TABLE_VIEW_H
