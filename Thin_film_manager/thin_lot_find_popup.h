#ifndef THIN_LOT_FIND_POPUP_H
#define THIN_LOT_FIND_POPUP_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlTableModel>
#include <Thin_film_manager/thin_data_table_view.h>
#include <Thin_film_manager/thin_wet_sqltable_model.h>
#include <QLineEdit>
#include <QDateTimeEdit>
#include <QMessageBox>
namespace Ui {
class Thin_lot_find_popup;
}

class Thin_lot_find_popup : public QDialog
{
    Q_OBJECT

public:
    explicit Thin_lot_find_popup(QSqlDatabase db,QString lot_name,QString wet_machine,
                                 Thin_wet_sqltable_model * parent_model,QString worker,QDateTimeEdit *start_date_time,QDateTimeEdit *end_date_time,QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    QString lot_name;
    QSqlTableModel *data_model;
    QString wet_machine;
    QString worker;
    Thin_wet_sqltable_model * parent_model;
    QDateTimeEdit *start_date_time;
    QDateTimeEdit *end_date_time;
    void calc_wet_accumulate(QDateTime input_time, QString wet_machine);

    ~Thin_lot_find_popup();
    void closeEvent(QCloseEvent *);

private slots:
    void on_select_insert_btn_clicked();

private:
    Ui::Thin_lot_find_popup *ui;
};

#endif // THIN_LOT_FIND_POPUP_H
