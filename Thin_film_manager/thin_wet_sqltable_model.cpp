#include "thin_wet_sqltable_model.h"

Thin_wet_sqltable_model::Thin_wet_sqltable_model(QObject *parent, QSqlDatabase db):QSqlTableModel(parent,db)
{

}

Qt::ItemFlags Thin_wet_sqltable_model::flags(const QModelIndex &index) const
{
    if(index.column() == fieldIndex("chemical_change")){
        return QSqlTableModel::flags(index)|Qt::ItemIsUserCheckable;
    }
    return QSqlTableModel::flags(index);
}

QVariant Thin_wet_sqltable_model::data(const QModelIndex &index, int role) const
{
    if(index.column() == fieldIndex("work_time") && role == Qt::DisplayRole){
           QDateTime datetime =  record(index.row()).value(fieldIndex("work_time")).toDateTime();
           return datetime.toString("yyyy-MM-dd hh:mm");
    }
    if(index.column() == fieldIndex("DPS7300_work_time") && role == Qt::DisplayRole){
           QTime datetime =  record(index.row()).value(fieldIndex("DPS7300_work_time")).toTime();
           return datetime.toString("hh:mm");
    }
    if(index.column() == fieldIndex("IPA_work_time") && role == Qt::DisplayRole){
           QTime datetime =  record(index.row()).value(fieldIndex("IPA_work_time")).toTime();
           return datetime.toString("hh:mm");
    }
    if(index.column() == fieldIndex("chemical_change") && role == Qt::DisplayRole){
        if(record(index.row()).value(fieldIndex("chemical_change")).toBool()){
            return "O";
        }else {
            return "X";
        }
    }
    if(index.column() == fieldIndex("chemical_change") && role == Qt::CheckStateRole)
   {
        if(record(index.row()).value(fieldIndex("chemical_change")).toBool()){
            return Qt::Checked;
        }else {
            return Qt::Unchecked;
        }
    }
    return QSqlTableModel::data(index, role);
}

bool Thin_wet_sqltable_model::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.column() == fieldIndex("chemical_change") && role == Qt::CheckStateRole)
    {
        bool result = false;
        if ( value == Qt::Checked ){
            QSqlRecord recode = record(index.row());
            recode.setGenerated("chemical_change",true);
            recode.setGenerated("idx",true);
            recode.setValue("chemical_change",Qt::Checked);
            result = setRecord(index.row(),recode);
        }
        else if(value == Qt::Unchecked) {
            QSqlRecord recode = record(index.row());
            recode.setGenerated("chemical_change",true);
            recode.setGenerated("idx",true);
            recode.setValue("chemical_change",Qt::Unchecked);
            result = setRecord(index.row(),recode);
        }
        QDateTime work_time = record(index.row()).value("work_time").toDateTime();
        QString wet_machine  = record(index.row()).value("wet_machine").toString();
        calc_wet_accumulate(work_time,wet_machine);

        emit dataChanged(index, index );
        return result;
    }else if(index.column() == fieldIndex("work_time") && role == Qt::EditRole){
        bool result = false;
//        QString old_work_time = record(index.row()).value("work_time").toDateTime().toString("yyyy-MM-dd hh:mm:ss");
//        QString model_type = record(index.row()).value("model_type").toString();
//        QString lot_number = record(index.row()).value("lot_number").toString();
//        int wafer_count = record(index.row()).value("wafer_count").toInt();
        QString wet_machine = record(index.row()).value("wet_machine").toString();
        int idx = record(index.row()).value("idx").toInt();

        QSqlQuery query(database());
        result = query.exec(QString("UPDATE `FAB`.`depostion_wet_accumulate_manager` SET `work_time`='%1' "
                           "where `idx` = %2 ")
                   .arg(value.toDateTime().toString("yyyy-MM-dd hh:mm:00")).arg(idx));

        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError().text();

        calc_wet_accumulate(value.toDateTime(),wet_machine);
        emit scroll_bottom();
        return result;
    }

    return QSqlTableModel::setData(index, value, role);
}

void Thin_wet_sqltable_model::calc_wet_accumulate(QDateTime input_time, QString wet_machine)
{
    QSqlQuery query(database());
    query.exec(QString("select work_time,max(`chemical_accumulate`) AS `chemical_accumulate`,wet_machine "
                       "from depostion_wet_accumulate_manager "
                       "where work_time < '%1' AND wet_machine = '%2' group by work_time order by work_time desc LIMIT 1")
               .arg(input_time.toString("yyyy-MM-dd hh:mm:00")).arg(wet_machine));

    int before_accumulate = 0;
    if(query.next()){
        before_accumulate = query.value("chemical_accumulate").toInt();
    }

    query.exec(QString("select count(*) as count from depostion_wet_accumulate_manager where work_time >= '%1' AND wet_machine = '%2' order by work_time asc")
               .arg(input_time.toString("yyyy-MM-dd hh:mm:00")).arg(wet_machine));
    int progress_max =0 ;
    if(query.next()){
        progress_max = query.value("count").toInt();
    }
    QProgressBar * progress_bar = property("progressbar").value<QProgressBar *>();
    progress_bar->setMaximum(progress_max);

    query.exec(QString("select * from depostion_wet_accumulate_manager where work_time >= '%1' AND wet_machine = '%2' order by work_time asc")
               .arg(input_time.toString("yyyy-MM-dd hh:mm:00")).arg(wet_machine));
    int progress = 0;
    while(query.next()){
        int chemical_change = query.value("chemical_change").toInt();
        int now_accumulate =0;
        if(chemical_change){
            now_accumulate = query.value("wafer_count").toInt();
        }else {
            now_accumulate = before_accumulate+query.value("wafer_count").toInt();
        }
        QSqlQuery query1(database());
        query1.exec(QString("update `depostion_wet_accumulate_manager` set chemical_accumulate = %1 where idx = %2 ")
                    .arg(now_accumulate)
                    .arg(query.value("idx").toString()));
        before_accumulate = now_accumulate;
        progress++;
        progress_bar->setValue(progress);
    }
    select();
}
