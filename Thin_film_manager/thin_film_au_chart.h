#ifndef THIN_FILM_AU_CHART_H
#define THIN_FILM_AU_CHART_H


#include <QObject>
#include <QWidget>

#include <QtCharts/QChart>
#include <QLineSeries>
#include <QValueAxis>
QT_CHARTS_USE_NAMESPACE
class Thin_film_au_chart: public QChart
{
    Q_OBJECT
public:
    Thin_film_au_chart(QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0);
};

#endif // THIN_FILM_AU_CHART_H
