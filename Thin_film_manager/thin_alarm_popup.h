#ifndef THIN_ALARM_POPUP_H
#define THIN_ALARM_POPUP_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QTimer>
#include <QSound>
namespace Ui {
class Thin_alarm_popup;
}

class Thin_alarm_popup : public QDialog
{
    Q_OBJECT

public:
    explicit Thin_alarm_popup(QSqlDatabase my_mes_db,QWidget *parent = 0);
    QSqlDatabase my_mes_db;
    QTimer main_timer;
    QTimer main_screen_timer;
    QTimer screen_timer;
    QVector <QStringList> screen_quque;
    void closeEvent(QCloseEvent *);
    int main_screen_count;
    ~Thin_alarm_popup();

private:
    Ui::Thin_alarm_popup *ui;
private slots:
    void main_time_out();
    void screen_timer_time_out();
    void main_screen_timer_time_out();
};

#endif // THIN_ALARM_POPUP_H
