#include "datedelegate.h"


DateDelegate::DateDelegate(QWidget *parent): QItemDelegate(parent)
{
    format_use = false;
}
DateDelegate::DateDelegate(QString timeformat,QWidget *parent): QItemDelegate(parent)
{
    this->timeformat = timeformat;
    format_use= true;
}

QWidget *DateDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    dataTimeEdit = new QDateTimeEdit( parent );
    dataTimeEdit->setCalendarPopup(true);
    if(format_use){
        dataTimeEdit->setDisplayFormat(timeformat);
    }else {
        dataTimeEdit->setDisplayFormat("yyyy-MM-dd hh:mm");
    }

//    connect(dataTimeEdit,SIGNAL(dateTimeChanged(QDateTime)),this,SLOT(setData(QDateTime)));
    return dataTimeEdit;
}

void DateDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QVariant dateTime = index.model()->data( index, Qt::DisplayRole );

    (static_cast<QDateTimeEdit*>( editor ))->setDateTime(dateTime.toDateTime());
}

void DateDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    model->setData( index, static_cast<QDateTimeEdit*>( editor )->dateTime() );
}

void DateDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry( option.rect );
}

void DateDelegate::setData(QDateTime val)
{
    emit commitData(dataTimeEdit);
}
