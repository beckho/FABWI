#include "thin_film_chart_view.h"

Thin_film_chart_view::Thin_film_chart_view(QChart *chart, QWidget *parent):
    QChartView(chart, parent)
{
    setRubberBand(QChartView::RectangleRubberBand);
    this->mchart = chart;
}

void Thin_film_chart_view::mouseMoveEvent(QMouseEvent *event)
{
    QPointF value(chart()->mapToValue(event->pos()).x(),chart()->mapToValue(event->pos()).y());
    emit move_value(value);
    QChartView::mouseMoveEvent(event);
}

void Thin_film_chart_view::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
     case Qt::Key_Plus:
         chart()->zoomIn();
         break;
     case Qt::Key_Minus:
         chart()->zoomOut();
         break;
     case Qt::Key_Left:
         chart()->scroll(-10, 0);
         break;
     case Qt::Key_Right:
         chart()->scroll(10, 0);
         break;
     case Qt::Key_Up:
         chart()->scroll(0, 10);
         break;
     case Qt::Key_Down:
         chart()->scroll(0, -10);
         break;
     default:
         QGraphicsView::keyPressEvent(event);
         break;
     }

    this->update();
    this->updateGeometry();
    mchart->update(mchart->geometry());

}
