#ifndef THIN_MONITERING_TOOL_H
#define THIN_MONITERING_TOOL_H

#include <QWidget>
#include <QTimer>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <Thin_film_manager/thin_alarm_popup.h>
#include <global_define.h>
#include <QSettings>
#include <QDateTime>
#include <QDebug>
#include <QSqlError>
namespace Ui {
class Thin_monitering_tool;
}

class Thin_monitering_tool : public QWidget
{
    Q_OBJECT

public:
    explicit Thin_monitering_tool(QWidget *parent = 0);
    QTimer now_time_timer;
    QTimer moniter_timer;
    QSqlDatabase my_mesdb;
    int loop_count;
    ~Thin_monitering_tool();
    void closeEvent(QCloseEvent *event);

private slots:
    void moniter_timer_timeout();
    void on_monitering_start_clicked();

private:
    Ui::Thin_monitering_tool *ui;
};

#endif // THIN_MONITERING_TOOL_H
