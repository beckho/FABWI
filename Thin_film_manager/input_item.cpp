#include "input_item.h"
#include "ui_input_item.h"

input_item::input_item(QSqlDatabase db,QString machine_code,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::input_item)
{
    ui->setupUi(this);
    this->my_mesdb = db;

//    if(!my_mesdb.open()){
//        qDebug()<<"fasle";
//        qDebug()<<my_mesdb.lastError().text();
//    }else {
//        qDebug()<<"open";
//    }
    ui->LE_resister->setValidator(new QDoubleValidator(0, 10000, 2, this));
    ui->LE_au_f_add->setValidator(new QDoubleValidator(0, 10000, 2, this));
    ui->LE_now_r_weight->setValidator(new QDoubleValidator(0, 10000, 2, this));
    ui->LE_au_after_r_weight->setValidator(new QDoubleValidator(0, 10000, 2, this));
    ui->LE_au_after_weight->setValidator(new QDoubleValidator(-10000, 10000, 2, this));
    ui->LE_au_use->setValidator(new QIntValidator(0, 10000, this));
    ui->LE_ti_add->setValidator(new QIntValidator(0, 10000, this));
    ui->LE_al_add->setValidator(new QIntValidator(0, 10000, this));
    ui->LE_xtal_life->setValidator(new QIntValidator(0, 100000, this));
    ui->LE_Recipe_name1->setValidator(new QIntValidator(-1000000, 1000000, this));
    ui->LE_Recipe_name2->setValidator(new QIntValidator(-1000000, 1000000, this));
    ui->LE_Recipe_name3->setValidator(new QIntValidator(-1000000, 1000000, this));
    ui->LE_Recipe_name4->setValidator(new QIntValidator(-1000000, 1000000, this));
    ui->LE_Recipe_name5->setValidator(new QIntValidator(-1000000, 1000000, this));
    ui->LE_recipe_number->setValidator(new QIntValidator(0, 1000, this));
    this->machine_code = machine_code;


}

input_item::~input_item()
{
    delete ui;
}

void input_item::clear_data()
{
    ui->LE_al_add->clear();
    ui->LE_au_after_r_weight->clear();
    ui->LE_au_f_add->clear();
    ui->LE_au_use->clear();
//    ui->LE_name->clear();
    ui->LE_note->clear();
    ui->LE_now_r_weight->clear();
//    ui->LE_Recipe_name1->clear();
//    ui->LE_Recipe_name2->clear();
//    ui->LE_Recipe_name3->clear();
//    ui->LE_Recipe_name4->clear();
//    ui->LE_Recipe_name5->clear();
    ui->LE_resister->clear();
    ui->LE_ti_add->clear();
    ui->LE_xtal_life->clear();
//    ui->CB_N2_blow->setChecked(true);
//    ui->CB_d_run->setChecked(false);
    ui->LE_au_after_weight->clear();
//    ui->CB_Recipe_name1->setCurrentIndex(0);
//    ui->CB_Recipe_name2->setCurrentIndex(0);
//    ui->CB_Recipe_name3->setCurrentIndex(0);
//    ui->CB_Recipe_name4->setCurrentIndex(0);
//    ui->CB_Recipe_name5->setCurrentIndex(0);


}

void input_item::slot_lot_item_data(thin_film_manager_input_lot_item item)
{
//    ui->LE_au_f_add->setText(QString("%1").arg(item.au_f_add));

//    ui->LE_Recipe_name1->setText(QString("%1").arg(item.recipe[0]));
//    ui->LE_Recipe_name2->setText(QString("%1").arg(item.recipe[1]));
//    ui->LE_Recipe_name3->setText(QString("%1").arg(item.recipe[2]));
//    ui->LE_Recipe_name4->setText(QString("%1").arg(item.recipe[3]));
//    ui->LE_Recipe_name5->setText(QString("%1").arg(item.recipe[4]));
//    ui->LE_name->setText(item.user_name);


}


void input_item::on_LE_now_r_weight_editingFinished()
{
//    double a_f_add = ui->LE_au_f_add->text().toDouble();
//    double now_r_weight = ui->LE_now_r_weight->text().toDouble();
//    double result  = a_f_add+now_r_weight;
//    double au_use = 0;
//    double before_au_after_weight = 0;
//    ui->LE_au_after_r_weight->setText(QString("%1").arg(result));
//    QSqlQuery query(my_mesdb);
//    query.exec(QString("select  Au_after_weight from Thin_film_data where machine_code = '%1' order by Input_time desc LIMIT 1").arg(machine_code));
//    if(query.next()){
//        before_au_after_weight = query.value("Au_after_weight").toDouble();
//    }
//    au_use = before_au_after_weight - now_r_weight;
//    ui->LE_au_use->setText(QString("%1").arg(au_use));
    QSqlQuery query(my_mesdb);
    query.exec(QString("update `Thin_film_spec_managerment` set `now_r_weight` = '%1' where `machine_code` = '%2'")
               .arg(ui->LE_now_r_weight->text()).arg(machine_code));
}

void input_item::on_LE_recipe_number_textChanged(const QString &arg1)
{
    ui->CB_recipe_choice->setCurrentIndex(arg1.toInt());
}

void input_item::on_LE_au_after_r_weight_editingFinished()
{
    double now =  ui->LE_now_r_weight->text().toDouble();
    double after = ui->LE_au_after_r_weight->text().toDouble();
    double result = after - now;
    ui->LE_au_f_add->setText(QString("%1").arg(result,0,'f',2));
    QSqlQuery query(my_mesdb);
    query.exec(QString("update `Thin_film_spec_managerment` set `au_after_r_weight` = '%1' where `machine_code` = '%2'")
               .arg(ui->LE_au_after_r_weight->text()).arg(machine_code));
    query.exec(QString("update `Thin_film_spec_managerment` set `au_pullet_add` = '%1' where `machine_code` = '%2'")
               .arg(result).arg(machine_code));
}

void input_item::on_LE_au_after_weight_editingFinished()
{
    QSqlQuery query(my_mesdb);
    double au_case_weight = 0;
    query.exec("select * from depostion_au_maneger_setting");
    if(query.next()){
        au_case_weight = query.value("au_pellet_case_weight").toDouble();
    }
    query.exec("select * from `depostion_au_use_current_view` order by `Input_time` desc LIMIT 1");
    if(query.next()){
        double au_after_weight = ui->LE_au_after_weight->text().toDouble();
        double au_exit_weight = au_after_weight-au_case_weight;

        double now_au_pellet = query.value("au_pellet_total").toDouble() - ui->LE_au_f_add->text().toDouble();
        double diff = au_exit_weight-now_au_pellet;

        double OKNG = query.value("diff").toDouble() - diff;
        if(abs(OKNG)>=0.1){
            QMessageBox msg;
            msg.addButton(QMessageBox::Ok);
            msg.setText(tr("au pellet NG"));
            msg.exec();
        }
    }
    query.exec(QString("update `Thin_film_spec_managerment` set `au_after_weight` = '%1' where `machine_code` = '%2'")
               .arg(ui->LE_au_after_weight->text()).arg(machine_code));
}
