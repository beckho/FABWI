#include "spec_item.h"
#include "ui_spec_item.h"

spec_item::spec_item(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::spec_item)
{
    ui->setupUi(this);
    ui->spec1_1->setValidator(new QDoubleValidator(0, 100000, 2, this));
    ui->spec1_2->setValidator(new QDoubleValidator(0, 100000, 2, this));
    ui->LE_LCL->setValidator(new QDoubleValidator(0, 100000, 2, this));
    ui->LE_UCL->setValidator(new QDoubleValidator(0, 100000, 2, this));
    ui->LE_E_LCL->setValidator(new QDoubleValidator(0, 100000, 2, this));
    ui->LE_E_UCL->setValidator(new QDoubleValidator(0, 100000, 2, this));
    ui->LE_WE_LCL->setValidator(new QDoubleValidator(0, 100000, 2, this));
    ui->LE_WE_UCL->setValidator(new QDoubleValidator(0, 100000, 2, this));
    ui->LE_W_LCL->setValidator(new QDoubleValidator(0, 100000, 2, this));
    ui->LE_W_UCL->setValidator(new QDoubleValidator(0, 100000, 2, this));
    ui->LE_W_LCL_2->setValidator(new QDoubleValidator(0, 100000, 2, this));
    ui->LE_W_UCL_2->setValidator(new QDoubleValidator(0, 100000, 2, this));
}

spec_item::~spec_item()
{
    delete ui;
}
