#ifndef BRIDGE_DATA_TABLE_H
#define BRIDGE_DATA_TABLE_H

#include <QObject>
#include <QWidget>
#include <QTableView>
#include <QKeyEvent>
#include <QApplication>
#include <QClipboard>
class bridge_data_table : public QTableView
{
public:
    bridge_data_table(QWidget *parent = Q_NULLPTR);
private:
    void keyPressEvent(QKeyEvent *event);
};

#endif // BRIDGE_DATA_TABLE_H
