#include "escp_chart_widget.h"
#include "ui_escp_chart_widget.h"

escp_chart_widget::escp_chart_widget(QSqlDatabase my_mesdb, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::escp_chart_widget)
{
    ui->setupUi(this);
    this->my_mesdb = my_mesdb;
    this->chart1_layout = ui->chart_layout;
    this->chart2_layout = ui->chart_layout_2;
    this->chart3_layout = ui->chart_layout_3;
    chart1 = 0;
    chart2 = 0;
    chart3 = 0;
    LA_PR_metal = ui->LA_PR_metal;
    LA_M_metal = ui->LA_M_metal;
    LA_CD_B_metal = ui->LA_CD_B_metal;
    QSqlQuery query(my_mesdb);
    query.exec(QString("select * from escp_spec_data  WHERE  spec_name='ADI_SPEC' "));
    if(query.next()){
        ui->LE_ADI_SPEC->setText(query.value("spec_USL").toString());
    }

    query.exec(QString("select * from escp_spec_data  WHERE  spec_name='ACI_SPEC' "));
    if(query.next()){
        ui->LE_ACI_SPEC->setText(query.value("spec_USL").toString());
    }

    query.exec(QString("select * from escp_spec_data  WHERE  spec_name='CD_B_SPEC' "));
    if(query.next()){
        ui->LE_CD_B_SPEC->setText(query.value("spec_USL").toString());
    }


}

escp_chart_widget::~escp_chart_widget()
{
    delete ui;
}

void escp_chart_widget::chart1_hovered(QPointF point, bool result)
{
    QLineSeries *templine = (QLineSeries *)sender();
    if(result){
        ui->LA_PR_metal->setText(templine->property("hover_name").toString());
    }
}

void escp_chart_widget::chart2_hovered(QPointF point, bool result)
{
     QLineSeries *templine = (QLineSeries *)sender();
     if(result){
        ui->LA_M_metal->setText(templine->property("hover_name").toString());
     }
}

void escp_chart_widget::chart3_hovered(QPointF point, bool result)
{
    QLineSeries *templine = (QLineSeries *)sender();
    if(result){
       ui->LA_CD_B_metal->setText(templine->property("hover_name").toString());
    }
}

void escp_chart_widget::on_label1_view_btn_clicked()
{
    if(chart1 !=0){
        QList<QAbstractSeries *> series =  chart1->series();
        qDebug()<<series.count();
        for(int i=0; i<series.count();i++){
            QBarSeries *templine =   qobject_cast<QBarSeries *>(series.at(i));

            if(templine != 0){
                templine->setLabelsVisible(!templine->isLabelsVisible());
            }
        }
    }
}

void escp_chart_widget::on_label2_view_btn_clicked()
{
    if(chart2 !=0){
        QList<QAbstractSeries *> series =  chart2->series();
        for(int i=0; i<series.count();i++){
            QBarSeries *templine =   qobject_cast<QBarSeries *>(series.at(i));
            if(templine != 0){
                templine->setLabelsVisible(!templine->isLabelsVisible());
            }
        }
    }
}

void escp_chart_widget::on_CD_B_view_btn_clicked()
{
    if(chart3 !=0){
        QList<QAbstractSeries *> series =  chart3->series();
        for(int i=0; i<series.count();i++){
            QBarSeries *templine =   qobject_cast<QBarSeries *>(series.at(i));
            if(templine != 0){
                templine->setLabelsVisible(!templine->isLabelsVisible());
            }
        }
    }
}

void escp_chart_widget::on_z_clicked()
{
    if(chart1 !=0){
        chart1->zoomReset();
    }
}

void escp_chart_widget::on_pushButton_2_clicked()
{
    if(chart2 !=0){
        chart2->zoomReset();
    }
}

void escp_chart_widget::on_CB_PR_CP_clicked(bool checked)
{
    remake_chart1();
}
void escp_chart_widget::remake_chart1(){
    chart1_bar_series->clear();
    qreal rangemin = 0;
    qreal rangemax = 0;
    if(ui->CB_PR_CP->isChecked()){
        chart1_cp_bar_set = new QBarSet("CP");
        chart1_cp_bar_set->setLabelColor("black");

        for(int i=0;i<chart1_cp_points.count();i++){
            chart1_cp_bar_set->append(chart1_cp_points.at(i));
            rangemin = qMin(rangemin,chart1_cp_points.at(i));
            rangemax = qMax(rangemax,chart1_cp_points.at(i));
        }
        chart1_bar_series->append(chart1_cp_bar_set);
    }
    if(ui->CB_PR_CPK->isChecked()){
        chart1_cpk_bar_set = new QBarSet("CPK");
        chart1_cpk_bar_set->setLabelColor("black");
        for(int i=0;i<chart1_cpk_points.count();i++){
            chart1_cpk_bar_set->append(chart1_cpk_points.at(i));
            rangemin = qMin(rangemin,chart1_cpk_points.at(i));
            rangemax = qMax(rangemax,chart1_cpk_points.at(i));
        }
        chart1_bar_series->append(chart1_cpk_bar_set);
    }
    if(ui->CB_PR_SIGMA->isChecked()){
        chart1_sigma_bar_set = new QBarSet("SIGMA");
        chart1_sigma_bar_set->setLabelColor("black");
        for(int i=0;i<chart1_sigma_points.count();i++){
            chart1_sigma_bar_set->append(chart1_sigma_points.at(i));
            rangemin = qMin(rangemin,chart1_sigma_points.at(i));
            rangemax = qMax(rangemax,chart1_sigma_points.at(i));
        }
        chart1_bar_series->append(chart1_sigma_bar_set);
    }
    chart1->axisY()->setRange(rangemin,rangemax);
    chart1_bar_series->attachAxis(chart1_x);
    chart1_bar_series->attachAxis(chart1_y);
}
void escp_chart_widget::remake_chart2(){
    chart2_bar_series->clear();
    qreal rangemin = 0;
    qreal rangemax = 0;
    if(ui->CB_M_CP->isChecked()){
        chart2_cp_bar_set = new QBarSet("CP");
        chart2_cp_bar_set->setLabelColor("black");
        for(int i=0;i<chart2_cp_points.count();i++){
            chart2_cp_bar_set->append(chart2_cp_points.at(i));
            rangemin = qMin(rangemin,chart2_cp_points.at(i));
            rangemax = qMax(rangemax,chart2_cp_points.at(i));
        }
        chart2_bar_series->append(chart2_cp_bar_set);
    }
    if(ui->CB_M_CPK->isChecked()){
        chart2_cpk_bar_set = new QBarSet("CPK");
        chart2_cpk_bar_set->setLabelColor("black");
        for(int i=0;i<chart2_cpk_points.count();i++){
            chart2_cpk_bar_set->append(chart2_cpk_points.at(i));
            rangemin = qMin(rangemin,chart2_cpk_points.at(i));
            rangemax = qMax(rangemax,chart2_cpk_points.at(i));
        }
        chart2_bar_series->append(chart2_cpk_bar_set);
    }
    if(ui->CB_M_SIGMA->isChecked()){
        chart2_sigma_bar_set = new QBarSet("SIGMA");
        chart2_sigma_bar_set->setLabelColor("black");
        for(int i=0;i<chart2_sigma_points.count();i++){
            chart2_sigma_bar_set->append(chart2_sigma_points.at(i));
            rangemin = qMin(rangemin,chart2_sigma_points.at(i));
            rangemax = qMax(rangemax,chart2_sigma_points.at(i));
        }
        chart2_bar_series->append(chart2_sigma_bar_set);
    }
    chart2->axisY()->setRange(rangemin,rangemax);
    chart2_bar_series->attachAxis(chart2_x);
    chart2_bar_series->attachAxis(chart2_y);
}

void escp_chart_widget::remake_chart3()
{
    chart3_bar_series->clear();
    qreal rangemin = 0;
    qreal rangemax = 0;
    if(ui->CD_B_CP->isChecked()){
        chart3_cp_bar_set = new QBarSet("CP");
        chart3_cp_bar_set->setLabelColor("black");
        for(int i=0;i<chart3_cp_points.count();i++){
            chart3_cp_bar_set->append(chart3_cp_points.at(i));
            rangemin = qMin(rangemin,chart3_cp_points.at(i));
            rangemax = qMax(rangemax,chart3_cp_points.at(i));
        }
        chart3_bar_series->append(chart3_cp_bar_set);
    }
    if(ui->CD_B_CPK->isChecked()){
        chart3_cpk_bar_set = new QBarSet("CPK");
        chart3_cpk_bar_set->setLabelColor("black");
        for(int i=0;i<chart3_cpk_points.count();i++){
            chart3_cpk_bar_set->append(chart3_cpk_points.at(i));
            rangemin = qMin(rangemin,chart3_cpk_points.at(i));
            rangemax = qMax(rangemax,chart3_cpk_points.at(i));
        }
        chart3_bar_series->append(chart3_cpk_bar_set);
    }
    if(ui->CD_B_SIGMA->isChecked()){
        chart3_sigma_bar_set = new QBarSet("SIGMA");
        chart3_sigma_bar_set->setLabelColor("black");
        for(int i=0;i<chart3_sigma_points.count();i++){
            chart3_sigma_bar_set->append(chart3_sigma_points.at(i));
            rangemin = qMin(rangemin,chart3_sigma_points.at(i));
            rangemax = qMax(rangemax,chart3_sigma_points.at(i));
        }
        chart3_bar_series->append(chart3_sigma_bar_set);
    }
    chart3->axisY()->setRange(rangemin,rangemax);
    chart3_bar_series->attachAxis(chart3_x);
    chart3_bar_series->attachAxis(chart3_y);
}

void escp_chart_widget::on_CB_PR_CPK_clicked(bool checked)
{
    remake_chart1();
}

void escp_chart_widget::on_CB_PR_SIGMA_clicked(bool checked)
{
    remake_chart1();
}

void escp_chart_widget::on_CB_M_CP_clicked(bool checked)
{
    remake_chart2();
}

void escp_chart_widget::on_CB_M_CPK_clicked(bool checked)
{
    remake_chart2();
}

void escp_chart_widget::on_CB_M_SIGMA_clicked(bool checked)
{
    remake_chart2();
}

void escp_chart_widget::on_btn_ADI_SPEC_clicked()
{
    QSqlQuery query(my_mesdb);
    query.exec(QString("update  `escp_spec_data` SET `spec_USL`='%1' WHERE  `spec_name`='ADI_SPEC' ")
               .arg(ui->LE_ADI_SPEC->text()));
}

void escp_chart_widget::on_btn_ACI_SPEC_clicked()
{
    QSqlQuery query(my_mesdb);
    query.exec(QString("update  `escp_spec_data` SET `spec_USL`='%1' WHERE  `spec_name`='ACI_SPEC' ")
               .arg(ui->LE_ACI_SPEC->text()));
}



void escp_chart_widget::on_CD_B_CP_clicked(bool checked)
{
    remake_chart3();
}

void escp_chart_widget::on_CD_B_CPK_clicked(bool checked)
{
    remake_chart3();
}

void escp_chart_widget::on_CD_B_SIGMA_clicked(bool checked)
{
    remake_chart3();
}
