#ifndef ESCP_CHART_H
#define ESCP_CHART_H

#include <QObject>
#include <QWidget>
#include <QtCharts/QChart>
#include <QLineSeries>
QT_CHARTS_USE_NAMESPACE

class escp_chart : public QChart
{
public:
    escp_chart(QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0);
};

#endif // ESCP_CHART_H
