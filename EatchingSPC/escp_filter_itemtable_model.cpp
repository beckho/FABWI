#include "escp_filter_itemtable_model.h"

escp_filter_itemtable_model::escp_filter_itemtable_model(int rows, int columns, QObject *parent)
    :QStandardItemModel(rows,columns,parent)
{

}

Qt::ItemFlags escp_filter_itemtable_model::flags(const QModelIndex &index) const
{
    if(index.column() == 0){
        return QStandardItemModel::flags(index)|Qt::ItemIsUserCheckable;
    }
    return QStandardItemModel::flags(index);
}

QVariant escp_filter_itemtable_model::data(const QModelIndex &index, int role) const
{
   if( (index.column() == 0) && (role == Qt::CheckStateRole))
   {
        if(index.data(Qt::UserRole).toInt() ==  Qt::Checked){
            return Qt::Checked;
        }else {
            return Qt::Unchecked;
        }
    }
    return QStandardItemModel::data(index, role);
}

bool escp_filter_itemtable_model::setData(const QModelIndex &index1, const QVariant &value, int role)
{
    if((index1.column() == 0) && (role == Qt::CheckStateRole))
    {
        if ( value.toInt() == Qt::Checked ){
            return QStandardItemModel::setData(index1,Qt::Checked,Qt::UserRole);
        }else if(value == Qt::Unchecked) {
            return QStandardItemModel::setData(index1,Qt::Unchecked,Qt::UserRole);
        }
        emit dataChanged(index1, index1 );
    }
    return QStandardItemModel::setData(index1, value, role);

}
