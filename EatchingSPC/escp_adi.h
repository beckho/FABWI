#ifndef ESCP_ADI_H
#define ESCP_ADI_H

#include <QWidget>
#include <QSizePolicy>
#include <QStandardItemModel>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <global_define.h>
#include <QSqlError>
#include <QDateTime>
#include <qtconcurrentrun.h>
#include <QThread>
#include <QFutureWatcher>
#include <QSqlRecord>
#include <QSqlDriver>
#include <EatchingSPC/checkboxlist.h>
#include <EatchingSPC/escp_adi_machine_setpopup.h>
#include <QSqlTableModel>
#include <EatchingSPC/escp_chart.h>
#include <EatchingSPC/escp_chart_view.h>
#include <EatchingSPC/escp_chart_widget.h>
#include <QDateTimeAxis>
#include <QBarCategoryAxis>
#include <QValueAxis>
#include <EatchingSPC/escp_table_view.h>
#include <ns_core_com/ns_core_com.h>
#include <QBarSeries>
#include <QBarset>
#include <QMenu>
#include <EatchingSPC/escp_filter_popup.h>
#include <EatchingSPC/escp_item_table_model.h>
#include <QMap>
#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
namespace Ui {
class escp_ADI;
}
using namespace QtConcurrent;
class escp_ADI : public QWidget
{
    Q_OBJECT

public:
    explicit escp_ADI(NS_Core_Com::NS_Main *ns_main,QWidget *parent = 0);
    QStandardItemModel *CB_Matelname_model;
    QStandardItemModel *CB_process_model;
    QStandardItemModel *CB_Machine_model;
    QSqlDatabase ms_mesdb;
    QSqlDatabase my_mesdb;
    QSqlDatabase light_db;
    QFutureWatcher<void> *CB_watcher;
    QFutureWatcher<void> *search_watcher;
    QStringList item_list;
    escp_item_table_model *main_table_model;
    QSqlTableModel *result_table_model;
    NS_Core_Com::NS_Main *ns_main;
    QString search_table_filter;
    qreal calculateSD(QVector<qreal> source);
    qreal cpk_calc(qreal USL, qreal LSL, qreal cp, qreal mean);
    QMap<QString,escp_filter_popup *> filter_map;

    QString filter_string;
    QString filter_string2;

    int calc_point;
    void MATEL_NAME_INPUT();
    qreal average(QVector<qreal> source);
    void search_scp();

    QString return_item(QString data);
    ~escp_ADI();
signals:
    void progressbar_setrange_signal(int value);
    void progressbar_setvalue_signal(int value);
    void signal_search_progressbar_setmax(int arg1);
    void signal_search_progressbar_setvalue(int arg1);


private slots:
    void CB_model_append();
    void progressbar_setrange_slot(int value);
    void progressbar_setvalue_slot(int value);
    void on_search_btn_clicked();
    void search_finish();

    void slot_search_progressbar_setmax(int arg1);
    void slot_search_progressbar_setvalue(int arg1);

    void slot_TOTAL_CP_CPK_make();

    void slot_calc_point_change();

    void slot_main_model_check(int checked);

//    void on_machine_add_btn_clicked();

//    void CB_machine_name_update();

    void chart_maker_ver1(QString column_name1, QString column_name2, QString column_name3, QString split_line);



//    void on_CB_D_view_toggled(bool checked);

//    void on_CB_C_view_toggled(bool checked);

//    void on_CB_L_view_toggled(bool checked);

//    void on_CB_E_view_toggled(bool checked);

//    void on_CB_P_view_toggled(bool checked);


    void on_chart_viewer_btn_clicked();


    void on_RB_Point5_toggled(bool checked);

    void on_RB_Point7_toggled(bool checked);

public slots:
    void main_table_double_click();
    void main_table_menu(QPoint ex);


private:
    Ui::escp_ADI *ui;
};

#endif // ESCP_ADI_H
