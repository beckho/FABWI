#ifndef ESCP_ITEM_TABLE_MODEL_H
#define ESCP_ITEM_TABLE_MODEL_H

#include <QObject>
#include <QWidget>
#include <QSqlTableModel>
#include <QSqlRecord>
#include <QSqlDatabase>
#include <QDebug>
class escp_item_table_model : public QSqlTableModel
{
    Q_OBJECT
public:
    escp_item_table_model(QObject *parent = Q_NULLPTR, QSqlDatabase db = QSqlDatabase());
    Qt::ItemFlags flags(const QModelIndex & index) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const ;
    QSqlTableModel *specmodel;
    QSqlDatabase db;
    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
signals:
    void use_check_signal(int check);
};

#endif // ESCP_ITEM_TABLE_MODEL_H
