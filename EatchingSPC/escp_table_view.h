#ifndef ESCP_TABLE_VIEW_H
#define ESCP_TABLE_VIEW_H

#include <QObject>
#include <QWidget>
#include <QTableView>
#include <QKeyEvent>
#include <QApplication>
#include <QClipboard>

class escp_table_view : public QTableView
{
    Q_OBJECT
public:
    escp_table_view(QWidget *parent = 0);

private:
      void clicked(const QModelIndex &index);
      void keyPressEvent(QKeyEvent *event);
};

#endif // ESCP_TABLE_VIEW_H
