#ifndef ESCP_FILTER_POPUP_H
#define ESCP_FILTER_POPUP_H

#include <QDialog>
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>
#include <EatchingSPC/escp_item_table_model.h>
#include <QSqlError>
#include <EatchingSPC/escp_filter_itemtable_model.h>
#include <EatchingSPC/escp_table_view.h>
namespace Ui {
class escp_filter_popup;
}

class escp_filter_popup : public QDialog
{
    Q_OBJECT

public:
    explicit escp_filter_popup(QSqlDatabase light_db,int logic,escp_item_table_model *source_model,QString &filter,QWidget *parent = 0);
    QSqlDatabase light_db;
    int logic;
    QString filter;
    QString fieldname;

    escp_item_table_model *source_model;
    escp_filter_itemtable_model *filter_model;


    ~escp_filter_popup();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::escp_filter_popup *ui;
};

#endif // ESCP_FILTER_POPUP_H
