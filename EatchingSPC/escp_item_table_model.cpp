#include "escp_item_table_model.h"

escp_item_table_model::escp_item_table_model(QObject *parent, QSqlDatabase db) : QSqlTableModel(parent,db)
{

}

Qt::ItemFlags escp_item_table_model::flags(const QModelIndex &index) const
{
    if(index.column() == fieldIndex("USE")){
        return QSqlTableModel::flags(index)|Qt::ItemIsUserCheckable;
    }
    return QSqlTableModel::flags(index);
}

QVariant escp_item_table_model::data(const QModelIndex &index, int role) const
{
    if(index.column() == fieldIndex("USE") && role == Qt::DisplayRole){
        if(record(index.row()).value(fieldIndex("USE")).toBool()){
            return "";
        }else {
            return "";
        }
    }
    if(index.column() == fieldIndex("USE") && role == Qt::CheckStateRole)
   {
        if(record(index.row()).value(fieldIndex("USE")).toBool()){
            return Qt::Checked;
        }else {
            return Qt::Unchecked;
        }
    }
    return QSqlTableModel::data(index,role);
}

bool escp_item_table_model::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.column() == fieldIndex("USE") && role == Qt::CheckStateRole)
    {
        bool result = false;
        qDebug()<<index.data(Qt::CheckStateRole);

        if ( index.data(Qt::CheckStateRole).toBool()){
            QSqlRecord recode = record(index.row());
            recode.setGenerated("USE",true);
            recode.setValue("USE",Qt::Unchecked);
            result = setRecord(index.row(),recode);
            emit use_check_signal(Qt::Unchecked);
        }
        else if(!index.data(Qt::CheckStateRole).toBool()) {
            QSqlRecord recode = record(index.row());
            recode.setGenerated("USE",true);
            recode.setValue("USE",Qt::Checked);
            result = setRecord(index.row(),recode);
            emit use_check_signal(Qt::Checked);
        }
        emit dataChanged(index, index );
        return result;
    }
    return QSqlTableModel::setData(index,value,role);
}
