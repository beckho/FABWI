#ifndef ESCP_MAIN_H
#define ESCP_MAIN_H

#include <QMainWindow>
#include <QSplitter>
#include <EatchingSPC/escp_adi.h>
#include <QMdiSubWindow>
#include <ns_core_com/ns_core_com.h>

namespace Ui {
class ESCP_main;
}

class ESCP_main : public QMainWindow
{
    Q_OBJECT

public:
    explicit ESCP_main(QWidget *parent = 0);
    NS_Core_Com::NS_Main *NS_core;

    ~ESCP_main();

private:
    Ui::ESCP_main *ui;
};

#endif // ESCP_MAIN_H
