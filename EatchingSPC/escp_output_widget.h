#ifndef ESCP_OUTPUT_WIDGET_H
#define ESCP_OUTPUT_WIDGET_H

#include <QWidget>

namespace Ui {
class escp_output_widget;
}

class escp_output_widget : public QWidget
{
    Q_OBJECT

public:
    explicit escp_output_widget(QWidget *parent = 0);
    ~escp_output_widget();

private:
    Ui::escp_output_widget *ui;
};

#endif // ESCP_OUTPUT_WIDGET_H
