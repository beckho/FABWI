#include "escp_adi.h"
#include "ui_escp_adi.h"

escp_ADI::escp_ADI(NS_Core_Com::NS_Main *ns_main,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::escp_ADI)
{
    ui->setupUi(this);
    this->ns_main =ns_main;
    ui->splitter_2->setStretchFactor(1,QSizePolicy::Minimum);
    ui->splitter_3->setStretchFactor(0,QSizePolicy::Minimum);
    CB_Matelname_model = new QStandardItemModel();

    QString msdb_name = QString("MS_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    ms_mesdb = QSqlDatabase::addDatabase("QODBC",msdb_name);
    QString serverinfo = QString("DRIVER={SQL Server};Server=%1;Database=%2;Uid=%3;Port=1433;Pwd=%4").arg(DBMESSERVERIP).arg(DBMESNAME).arg(DBMESUSERNAME).arg(DBMESPW);
    ms_mesdb.setDatabaseName(serverinfo);
    if(!ms_mesdb.open()){
        qDebug()<<"fasle";
        qDebug()<<ms_mesdb.lastError().text();
    }else {
        qDebug()<<"open";
    }
    calc_point= 5;


    QString mydb_name = QString("MY_MESDB_%1_escpadi").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    QString server_ip = settings.value("serverip").toString();
    QString db_port = settings.value("dbport").toString();
    settings.endGroup();

    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }

    QString db_name = QString("local_DB_OI_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss_scp"));
    light_db = QSqlDatabase::addDatabase("QSQLITE",db_name);
    light_db.setDatabaseName(":memory:");
    light_db.open();


    main_table_model = new escp_item_table_model(this,light_db);
    connect(main_table_model,SIGNAL(use_check_signal(int)),this,SLOT(slot_main_model_check(int)));

    ui->main_table_view->setModel(main_table_model);
//    ui->main_table_view->setSortingEnabled(true);
    CB_watcher = new QFutureWatcher<void> ();
    search_watcher =  new QFutureWatcher<void> ();

    ui->main_table_view->setSortingEnabled(true);
    result_table_model  = new QSqlTableModel(this,light_db);
    ui->result_table_view->setModel(result_table_model);
    ui->result_table_view->setSortingEnabled(true);
    ui->main_table_view->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->main_table_view->horizontalHeader(),SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(main_table_menu(QPoint)));
    connect(CB_watcher,SIGNAL(finished()),this,SLOT(CB_model_append()));
    connect(search_watcher,SIGNAL(finished()),this,SLOT(search_finish()));
    connect(this,SIGNAL(progressbar_setrange_signal(int)),this,SLOT(progressbar_setrange_slot(int)));
    connect(this,SIGNAL(progressbar_setvalue_signal(int)),this,SLOT(progressbar_setvalue_slot(int)));

    QFuture<void> f1 = run(this,&escp_ADI::MATEL_NAME_INPUT);
    CB_watcher->setFuture(f1);
    ui->search_start_date->setDate(QDate::currentDate().addDays(-2));
    ui->search_start_date->setTime(QTime(8,0,0));
    ui->search_end_date->setDate(QDate::currentDate().addDays(-1));
    ui->search_end_date->setTime(QTime(7,59,59));
    CB_process_model = new QStandardItemModel();
    CB_process_model->appendRow(new QStandardItem(""));
    CB_process_model->appendRow(new QStandardItem(tr("depostion")));
    CB_process_model->appendRow(new QStandardItem(tr("1stcothing")));
    CB_process_model->appendRow(new QStandardItem(tr("1stlight")));
    CB_process_model->appendRow(new QStandardItem(tr("1stdev")));
    CB_process_model->appendRow(new QStandardItem(tr("eatching")));

    QSqlQuery light_query(light_db);
    QString light_query_txt = QString("CREATE TABLE [SPCDATA]( \n"
                                      "[USE] INT DEFAULT 2, "
                                      "[CASSETTE_ID] NVARCHAR(50), \n"
                                      "[MATERIAL_ID] VARCHAR(50), \n"
                                      "[D_TX_DTTM] VARCHAR(14), \n"
                                      "[D_EQ] VARCHAR(20), \n"
                                      "[D_VALUE1] VARCHAR(30), \n"
                                      "[D_VALUE2] VARCHAR(30), \n"
                                      "[PR_TX_DTTM] VARCHAR(14), \n"
                                      "[PR_EQ] VARCHAR(20), \n"
                                      "[PR_VALUE1] DOUBLE, \n"
                                      "[PR_VALUE2] DOUBLE, \n"
                                      "[PR_VALUE3] DOUBLE, \n"
                                      "[PR_VALUE4] DOUBLE, \n"
                                      "[PR_VALUE5] DOUBLE, \n"
                                      "[PR_VALUE6] DOUBLE, \n"
                                      "[PR_VALUE7] DOUBLE, \n"
                                      "[PR_TARGET_VALUE] DOUBLE, \n"
                                      "[PR_UPPER_SPEC_LIMIT] DOUBLE, \n"
                                      "[PR_LOWER_SPEC_LIMIT] DOUBLE, \n"
                                      "[PR_CP] DOUBLE, \n"
                                      "[PR_CPK] DOUBLE, \n"
                                      "[PR_AVG] DOUBLE, \n"
                                      "[PR_SIGMA] DOUBLE, \n"
                                      "[M_TX_DTTM] VARCHAR(14), \n"
                                      "[M_EQ] VARCHAR(20), \n"
                                      "[M_VALUE1] DOUBLE, \n"
                                      "[M_VALUE2] DOUBLE, \n"
                                      "[M_VALUE3] DOUBLE, \n"
                                      "[M_VALUE4] DOUBLE, \n"
                                      "[M_VALUE5] DOUBLE, \n"
                                      "[M_VALUE6] DOUBLE, \n"
                                      "[M_VALUE7] DOUBLE, \n"
                                      "[M_TARGET_VALUE] DOUBLE, \n"
                                      "[M_UPPER_SPEC_LIMIT] DOUBLE, \n"
                                      "[M_LOWER_SPEC_LIMIT] DOUBLE, \n"
                                      "[M_CP] DOUBLE, \n"
                                      "[M_CPK] DOUBLE, \n"
                                      "[M_AVG] DOUBLE, \n"
                                      "[M_SIGMA] DOUBLE, \n"
                                      "[C_TX_DTTM] VARCHAR(14), \n"
                                      "[C_EQ] VARCHAR(20), \n"
                                      "[L_TX_DTTM] VARCHAR(14), \n"
                                      "[L_EQ] VARCHAR(20), \n"
                                      "[L_VALUE1] DOUBLE, \n"
                                      "[P_TX_DTTM] VARCHAR(14), \n"
                                      "[P_EQ] VARCHAR(20), \n"
                                      "[E_TX_DTTM] VARCHAR(14), \n"
                                      "[E_EQ] VARCHAR(20), \n"
                                      "[E_VALUE1] DOUBLE, \n"
                                      "[E_VALUE2] DOUBLE, \n"
                                      "[E_VALUE3] DOUBLE, \n"
                                      "[E_VALUE4] VARCHAR(20), \n"
                                      "[CD_BIAS_VALUE1] DOUBLE, \n"
                                      "[CD_BIAS_VALUE2] DOUBLE, \n"
                                      "[CD_BIAS_VALUE3] DOUBLE, \n"
                                      "[CD_BIAS_VALUE4] DOUBLE, \n"
                                      "[CD_BIAS_VALUE5] DOUBLE, \n"
                                      "[CD_BIAS_VALUE6] DOUBLE, \n"
                                      "[CD_BIAS_VALUE7] DOUBLE, \n"
                                      "[CD_BIAS_TARGET] DOUBLE, \n"
                                      "[CD_BIAS_USL] DOUBLE, \n"
                                      "[CD_BIAS_LSL] DOUBLE, \n"
                                      "[CD_BIAS_CP] DOUBLE, \n"
                                      "[CD_BIAS_CPK] DOUBLE, \n"
                                      "[CD_BIAS_AVG] DOUBLE, \n"
                                      "[CD_BIAS_SIGMA] DOUBLE )");
    light_query.exec(light_query_txt);
    qDebug()<<light_query.lastError().text();


    light_query_txt = QString("CREATE TABLE [TEMP_SPCDATA]( \n"
                              "[CASSETTE_ID] VARCHAR(20), \n"
                              "[MATERIAL_ID] VARCHAR(20), \n"
                              "[TX_DTTM] VARCHAR(14), \n"
                              "[OPERATION_ID] VARCHAR(20), \n"
                              "[OPERATION_NAME] VARCHAR(50), \n"
                              "[EQUIPMENT_NAME] VARCHAR(50), \n"
                              "[CHARACTER_ID] VARCHAR(20), \n"
                              "[VALUE_COUNT] INT, \n"
                              "[VALUE1] VARCHAR(50), \n"
                              "[VALUE2] VARCHAR(50), \n"
                              "[VALUE3] VARCHAR(50), \n"
                              "[VALUE4] VARCHAR(50), \n"
                              "[VALUE5] VARCHAR(50), \n"
                              "[VALUE6] VARCHAR(50), \n"
                              "[VALUE7] VARCHAR(50), \n"
                              "[TARGET_VALUE] VARCHAR(100), \n"
                              "[UPPER_SPEC_LIMIT] VARCHAR(100), \n"
                              "[LOWER_SPEC_LIMIT] VARCHAR(100), \n"
                              "[UPPER_WARN_LIMIT] VARCHAR(100), \n"
                              "[LOWER_WARN_LIMIT] VARCHAR(100) \n"
                              "); \n");
    light_query.exec(light_query_txt);

    light_query_txt = QString("CREATE TABLE [TOTAL_CP_CPK]( \n"
                              "[MATERIAL_ID] VARCHAR(20), \n"
                              "[PR_CP] DOUBLE, \n"
                              "[PR_CPK] DOUBLE, \n"
                              "[PR_AVG] DOUBLE, \n"
                              "[PR_SIGMA] DOUBLE, \n"
                              "[PR_SAMPLECOUNT] INT, \n"
                              "[PR_TARGET_VALUE] VARCHAR(100), \n"
                              "[PR_UPPER_SPEC_LIMIT] VARCHAR(100), \n"
                              "[PR_LOWER_SPEC_LIMIT] VARCHAR(100), \n"
                              "[M_CP] DOUBLE, \n"
                              "[M_CPK] DOUBLE, \n"
                              "[M_AVG] DOUBLE, \n"
                              "[M_SIGMA] DOUBLE, \n"
                              "[M_SAMPLECOUNT] INT, \n"
                              "[M_TARGET_VALUE] VARCHAR(100), \n"
                              "[M_UPPER_SPEC_LIMIT] VARCHAR(100), \n"
                              "[M_LOWER_SPEC_LIMIT] VARCHAR(100), \n"
                              "[CD_BIAS_CP] DOUBLE, \n"
                              "[CD_BIAS_CPK] DOUBLE, \n"
                              "[CD_BIAS_AVG] DOUBLE, \n"
                              "[CD_BIAS_SIGMA] DOUBLE, \n"
                              "[CD_BIAS_SAMPLECOUNT] INT, \n"
                              "[CD_BIAS_TARGET] VARCHAR(100), \n"
                              "[CD_BIAS_USL] VARCHAR(100), \n"
                              "[CD_BIAS_LSL] VARCHAR(100) "
                              "); \n");
    light_query.exec(light_query_txt);
    filter_string = " 1 = 1 ";
    filter_string2 = " AND 1 = 1 ";

    connect(this,SIGNAL(signal_search_progressbar_setmax(int)),this,SLOT(slot_search_progressbar_setmax(int)));
    connect(this,SIGNAL(signal_search_progressbar_setvalue(int)),this,SLOT(slot_search_progressbar_setvalue(int)));

}



void escp_ADI::MATEL_NAME_INPUT()
{
    QSqlQuery query(ms_mesdb);
    query.exec("select count(*) as count from V_NM_MATERIALS where SITE_ID = 'WOSF' AND MATERIAL_TYPE = 'HALB' AND DELETE_FLAG <> 'Y' ");
    if(query.next()){
        emit progressbar_setrange_signal(query.value("count").toInt());
    }
    query.exec("select MATERIAL_NAME from V_NM_MATERIALS where SITE_ID = 'WOSF' AND MATERIAL_TYPE = 'HALB' AND DELETE_FLAG <> 'Y' order by MATERIAL_NAME asc");

    int i=0;
    QList<QStandardItem*> item_list;
    CB_Matelname_model->appendRow(new QStandardItem(""));
    while(query.next()){
        i++;
        QString MATERIAL_NAME = query.value("MATERIAL_NAME").toString();
        QStandardItem* item = new QStandardItem(MATERIAL_NAME);
        item_list.append(item);
        CB_Matelname_model->appendRow(item);
        emit progressbar_setvalue_signal(i);
    }
}



void escp_ADI::search_scp()
{
    QSqlQuery light_query(light_db);
    light_query.exec("delete from TEMP_SPCDATA");
    light_query.exec("delete from SPCDATA");
    light_query.exec("delete from TOTAL_CP_CPK");

    QStringList MATEL_NAMELIST =  ui->CB_MATELNAME->currentText().split(",");
    QString MATEL_NAMELIST_TXT= "";
    for(int i=0;i<MATEL_NAMELIST.count();i++){
        MATEL_NAMELIST_TXT = MATEL_NAMELIST_TXT.append("'"+MATEL_NAMELIST.at(i)+"',");
    }
    MATEL_NAMELIST_TXT = MATEL_NAMELIST_TXT.remove(MATEL_NAMELIST_TXT.length()-1,1);
    MATEL_NAMELIST_TXT = QString("AND A.MATERIAL_ID IN (%1)").arg(MATEL_NAMELIST_TXT);
    if(ui->CB_MATELNAME->currentText().trimmed().length()==0){
        MATEL_NAMELIST_TXT = "";
    }


    QStringList LOT_TYPE;
    if(ui->CB_typeA->isChecked()){
        LOT_TYPE.append("A");
    }
    if(ui->CB_typeB->isChecked()){
        LOT_TYPE.append("B");
    }
    if(ui->CB_typeC->isChecked()){
        LOT_TYPE.append("C");
    }
    if(ui->CB_typeD->isChecked()){
        LOT_TYPE.append("D");
    }
    if(ui->CB_typeM->isChecked()){
        LOT_TYPE.append("M");
    }
    if(ui->CB_typeP->isChecked()){
        LOT_TYPE.append("P");
    }
    if(ui->CB_typeS->isChecked()){
        LOT_TYPE.append("S");
    }
    if(ui->CB_typeO->isChecked()){
        LOT_TYPE.append("O");
    }
    QStringList bteCondMaterialType;
    bteCondMaterialType.append("HALB");
    QStringList bteCondCharacter;
    bteCondCharacter.append("LINE");
    QStringList bteCondAddOperation;
    bteCondAddOperation.append("OP02001010");
    bteCondAddOperation.append("OP02003020");
    bteCondAddOperation.append("OP02003030");
    bteCondAddOperation.append("OP02003040");
    bteCondAddOperation.append("OP02003050");
    bteCondAddOperation.append("OP02003060");
    bteCondAddOperation.append("OP02004010");
    QStringList bteCondMaterialID;
    if(ui->CB_MATELNAME->currentText().trimmed().length()!=0){
        bteCondMaterialID = ui->CB_MATELNAME->currentText().split(",");
    }
    QStringList nulldata;
    QStringList scpdata = ns_main->get_ESPC_data(ui->search_start_date->dateTime().toString("yyyyMMddhhmmss"), ui->search_end_date->dateTime().toString("yyyyMMddhhmmss"),
                                                 LOT_TYPE,bteCondMaterialType,bteCondCharacter,nulldata,nulldata,bteCondMaterialID,bteCondAddOperation);
    qDebug()<<scpdata.count();
    QSqlQuery light_query2(light_db);
    QString values_txt;
     int progress = 0;
     emit signal_search_progressbar_setmax(scpdata.count());
    foreach (QString items, scpdata) {
        items = items.remove(0,2);
        items = items.remove(items.length()-2,2);
        qDebug()<<items;
        QStringList itemslist = items.split(",");
        QString CASSETTE_ID  = return_item(itemslist.at(1));
        QString MATERIAL_ID = return_item(itemslist.at(2));
        QString D_TX_DTTM;
        QString D_EQ = return_item(itemslist.at(47));
        QString D_VALUE1 = return_item(itemslist.at(48));
        QString D_VALUE2 = return_item(itemslist.at(49));
        QString PR_TX_DTTM = return_item(itemslist.at(0));
        QString PR_EQ ;
        qreal PR_VALUE1 = return_item(itemslist.at(7)).toDouble();
        qreal PR_VALUE2 = return_item(itemslist.at(8)).toDouble();
        qreal PR_VALUE3 = return_item(itemslist.at(9)).toDouble();
        qreal PR_VALUE4 = return_item(itemslist.at(10)).toDouble();
        qreal PR_VALUE5 = return_item(itemslist.at(11)).toDouble();
        qreal PR_VALUE6 = return_item(itemslist.at(12)).toDouble();
        qreal PR_VALUE7 = return_item(itemslist.at(13)).toDouble();
        qreal PR_TARGET_VALUE = return_item(itemslist.at(5)).toDouble();
        qreal PR_UPPER_SPEC_LIMIT = return_item(itemslist.at(3)).toDouble();
        qreal PR_LOWER_SPEC_LIMIT = return_item(itemslist.at(4)).toDouble();
        QVector<qreal> standard_source;
        if(calc_point == 7){
            standard_source.append(PR_VALUE1);
        }
        standard_source.append(PR_VALUE2);
        standard_source.append(PR_VALUE3);
        standard_source.append(PR_VALUE4);
        standard_source.append(PR_VALUE5);
        standard_source.append(PR_VALUE6);
        if(calc_point == 7){
            standard_source.append(PR_VALUE7);
        }
        qreal standard_result = calculateSD(standard_source);
        qreal PR_mean = average(standard_source);
        qreal cp = (PR_UPPER_SPEC_LIMIT - PR_LOWER_SPEC_LIMIT)/(6*standard_result);
        qreal cpk = cpk_calc(PR_UPPER_SPEC_LIMIT,PR_LOWER_SPEC_LIMIT,cp,PR_mean);
        qreal PR_CP = 0;
        qreal PR_CPK = 0;
        qreal PR_SIGMA = 0;
        if(standard_result == 0){
            PR_CP = 0;
            PR_CPK = 0;
            PR_SIGMA  = 0;
        }else {
            PR_CP = cp;
            PR_CPK = cpk;
            PR_SIGMA  = standard_result;
        }
        QString M_TX_DTTM = return_item(itemslist.at(14));
        QString M_EQ;
        qreal M_VALUE1 = return_item(itemslist.at(19)).toDouble();
        qreal M_VALUE2 = return_item(itemslist.at(20)).toDouble();
        qreal M_VALUE3 = return_item(itemslist.at(21)).toDouble();
        qreal M_VALUE4 = return_item(itemslist.at(22)).toDouble();
        qreal M_VALUE5 = return_item(itemslist.at(23)).toDouble();
        qreal M_VALUE6 = return_item(itemslist.at(24)).toDouble();
        qreal M_VALUE7 = return_item(itemslist.at(25)).toDouble();
        qreal M_TARGET_VALUE = return_item(itemslist.at(17)).toDouble();
        qreal M_UPPER_SPEC_LIMIT = return_item(itemslist.at(15)).toDouble();
        qreal M_LOWER_SPEC_LIMIT = return_item(itemslist.at(16)).toDouble();
        standard_source.clear();
        if(calc_point == 7){
            standard_source.append(M_VALUE1);
        }
        standard_source.append(M_VALUE2);
        standard_source.append(M_VALUE3);
        standard_source.append(M_VALUE4);
        standard_source.append(M_VALUE5);
        standard_source.append(M_VALUE6);
        if(calc_point == 7){
            standard_source.append(M_VALUE7);
        }
        qreal M_CP = 0;
        qreal M_CPK = 0;
        qreal M_SIGMA = 0;
        standard_result = calculateSD(standard_source);
        qreal M_mean = average(standard_source);
        cp = (M_UPPER_SPEC_LIMIT - M_LOWER_SPEC_LIMIT)/(6*standard_result);

        cpk = cpk_calc(M_UPPER_SPEC_LIMIT,M_LOWER_SPEC_LIMIT,cp,M_mean);

        if(standard_result == 0){
            M_CP = 0;
            M_CPK = 0;
            M_SIGMA  = 0;
        }else {
            M_CP = cp;
            M_CPK = cpk;
            M_SIGMA  = standard_result;
        }
        QString C_TX_DTTM;
        QString C_EQ;
        QString L_TX_DTTM;
        QString L_EQ;
        qreal L_VALUE1 = 0;
        if(return_item(itemslist.at(26)) == "MVL002"){
            C_EQ = "MARK7#8";
            L_EQ = "ASML#2" ;
            L_VALUE1 = return_item(itemslist.at(28)).toDouble();
        }else if (return_item(itemslist.at(29)) == "MVL001"){
            C_EQ = "ACT#1";
            L_EQ = "ASML#3";
            L_VALUE1 = return_item(itemslist.at(31)).toDouble();
        }else {
            C_EQ = return_item(itemslist.at(33));
            L_EQ = return_item(itemslist.at(35));
            L_VALUE1 = return_item(itemslist.at(36)).toDouble();
        }
        QString P_TX_DTTM;
        QString P_EQ = return_item(itemslist.at(38));
        QString E_TX_DTTM;
        QString E_EQ = return_item(itemslist.at(40));
        qreal E_VALUE1 = return_item(itemslist.at(41)).toDouble();
        qreal E_VALUE2 = return_item(itemslist.at(42)).toDouble();
        qreal E_VALUE3 = return_item(itemslist.at(45)).toDouble();
        QString E_VALUE4 = return_item(itemslist.at(44));

        qreal CD_BIAS_VALUE[8] = {0,0,0,0,0,0,0,0};
        CD_BIAS_VALUE[1] = abs(PR_VALUE1 - M_VALUE1);
        CD_BIAS_VALUE[2] = abs(PR_VALUE2 - M_VALUE2);
        CD_BIAS_VALUE[3] = abs(PR_VALUE3 - M_VALUE3);
        CD_BIAS_VALUE[4] = abs(PR_VALUE4 - M_VALUE4);
        CD_BIAS_VALUE[5] = abs(PR_VALUE5 - M_VALUE5);
        CD_BIAS_VALUE[6] = abs(PR_VALUE6 - M_VALUE6);
        CD_BIAS_VALUE[7] = abs(PR_VALUE7 - M_VALUE7);
        qreal CD_BIAS_TARGET = M_TARGET_VALUE - PR_TARGET_VALUE;
        qreal CD_TEMP_SPC = M_UPPER_SPEC_LIMIT - M_LOWER_SPEC_LIMIT;
        CD_TEMP_SPC = CD_TEMP_SPC/2;
        qreal CD_BIAS_USL = CD_BIAS_TARGET + CD_TEMP_SPC;
        qreal CD_BIAS_LSL = CD_BIAS_TARGET - CD_TEMP_SPC;

        standard_source.clear();
        if(calc_point == 7){
            standard_source.append(CD_BIAS_VALUE[1]);
        }
        standard_source.append(CD_BIAS_VALUE[2]);
        standard_source.append(CD_BIAS_VALUE[3]);
        standard_source.append(CD_BIAS_VALUE[4]);
        standard_source.append(CD_BIAS_VALUE[5]);
        standard_source.append(CD_BIAS_VALUE[6]);
        if(calc_point == 7){
            standard_source.append(CD_BIAS_VALUE[7]);
        }
        qreal CD_BIAS_CP = 0;
        qreal CD_BIAS_CPK = 0;
        qreal CD_BIAS_SIGMA = 0;
        standard_result = calculateSD(standard_source);
        qreal CD_BIAS_mean = average(standard_source);
        cp = (CD_BIAS_USL - CD_BIAS_LSL)/(6*standard_result);

        cpk = cpk_calc(CD_BIAS_USL,CD_BIAS_LSL,cp,CD_BIAS_mean);
        if(standard_result == 0){
            CD_BIAS_CP = 0;
            CD_BIAS_CPK = 0;
            CD_BIAS_SIGMA  = 0;
        }else {
            CD_BIAS_CP = cp;
            CD_BIAS_CPK = cpk;
            CD_BIAS_SIGMA  = standard_result;
        }



        QString query_txt = QString("insert into [SPCDATA] values('2','%1','%2','%3','%4','%5','%6','%7','%8',%9,%10,%11,%12,"
                                "%13,%14,%15,%16,%17,%18,%19,%20,%21,%22,'%23','%24',%25,%26,%27,%28,%29,%30,%31,%32,"
                                "%33,%34,%35,%36,%37,%38,'%39','%40','%41','%42',%43,'%44','%45','%46','%47',%48,%49,%50,'%51',"
                                    "%52,%53,%54,%55,%56,%57,%58,%59,%60,%61,%62,%63,%64,%65)")
                    .arg(CASSETTE_ID)//CASSETTE_ID
                    .arg(MATERIAL_ID)//MATERIAL_ID
                    .arg(D_TX_DTTM)//D_TX_DTTM
                    .arg(D_EQ)//D_EQ
                    .arg(D_VALUE1)//D_VALUE
                    .arg(D_VALUE2)//D_VALUE
                    .arg(PR_TX_DTTM)//PR_TX_DTTM
                    .arg(PR_EQ)//PR_EQ
                    .arg(PR_VALUE1)//PR_VALUE1
                    .arg(PR_VALUE2)//PR_VALUE2
                    .arg(PR_VALUE3)//PR_VALUE3
                    .arg(PR_VALUE4)//PR_VALUE4
                    .arg(PR_VALUE5)//PR_VALUE5
                    .arg(PR_VALUE6)//PR_VALUE6
                    .arg(PR_VALUE7)//PR_VALUE7
                    .arg(PR_TARGET_VALUE)//PR_TARGET_VALUE
                    .arg(PR_UPPER_SPEC_LIMIT)//PR_UPPER_SPEC_LIMIT
                    .arg(PR_LOWER_SPEC_LIMIT)//PR_LOWER_SPEC_LIMIT
                    .arg(PR_CP)//PR_CP
                    .arg(PR_CPK)//PR_CPK
                    .arg(PR_mean)//PR_AVG 21
                    .arg(PR_SIGMA)//PR_SIGMA 22
                    .arg(M_TX_DTTM)//M_TX_DTTM 23
                    .arg(M_EQ)//M_TX_DTTM 24
                    .arg(M_VALUE1)//M_VALUE1 25
                    .arg(M_VALUE2)//M_VALUE2 26
                    .arg(M_VALUE3)//M_VALUE3 27
                    .arg(M_VALUE4)//M_VALUE4 28
                    .arg(M_VALUE5)//M_VALUE5 29
                    .arg(M_VALUE6)//M_VALUE6 30
                    .arg(M_VALUE7)//M_VALUE7 31
                    .arg(M_TARGET_VALUE)//M_TARGET_VALUE 32
                    .arg(M_UPPER_SPEC_LIMIT)//M_UPPER_SPEC_LIMIT 33
                    .arg(M_LOWER_SPEC_LIMIT)//M_LOWER_SPEC_LIMIT 34
                    .arg(M_CP)//M_CP 35
                    .arg(M_CPK)//M_CPK 36
                    .arg(M_mean)//M_AVG 37
                    .arg(M_SIGMA)//M_SIGMA 38
                    .arg(C_TX_DTTM)//M_SIGMA 39
                    .arg(C_EQ)//M_SIGMA
                    .arg(L_TX_DTTM)//M_SIGMA
                    .arg(L_EQ)//M_SIGMA
                    .arg(L_VALUE1)//M_SIGMA
                    .arg(P_TX_DTTM)//M_SIGMA
                    .arg(P_EQ)//M_SIGMA
                    .arg(E_TX_DTTM)//M_SIGMA
                    .arg(E_EQ)//M_SIGMA
                    .arg(E_VALUE1)//M_SIGMA
                    .arg(E_VALUE2)//M_SIGMA
                    .arg(E_VALUE3)//M_SIGMA
                    .arg(E_VALUE4)//M_SIGMA
                    .arg(CD_BIAS_VALUE[1])
                    .arg(CD_BIAS_VALUE[2])
                    .arg(CD_BIAS_VALUE[3])
                    .arg(CD_BIAS_VALUE[4])
                    .arg(CD_BIAS_VALUE[5])
                    .arg(CD_BIAS_VALUE[6])
                    .arg(CD_BIAS_VALUE[7])
                    .arg(CD_BIAS_TARGET)
                    .arg(CD_BIAS_USL)
                    .arg(CD_BIAS_LSL)
                    .arg(CD_BIAS_CP)
                    .arg(CD_BIAS_CPK)
                    .arg(CD_BIAS_mean)
                    .arg(CD_BIAS_SIGMA)
                    ;

            light_query2.exec(query_txt);
            qDebug()<<light_query2.lastQuery();
            qDebug()<<light_query2.lastError().text();
            ++progress;
            emit signal_search_progressbar_setvalue(progress);

    }
    slot_TOTAL_CP_CPK_make();

}

QString escp_ADI::return_item(QString data)
{
    QStringList list =  data.split("=");
    QString data1 =list.at(1);
    data1 = data1.remove(0,1);
    return data1;
}
qreal escp_ADI::calculateSD(QVector<qreal> source)
{
    float sum = 0.0, mean, standardDeviation = 0.0;
     int i;
     for(i = 0; i < source.count(); i++)
     {
         sum += source.at(i);
     }
     mean = sum/source.count();

     for(i = 0; i < source.count(); i++){
         standardDeviation += pow(source.at(i) - mean, 2);
     }

     return sqrt(standardDeviation / (source.count()-1));

}

qreal escp_ADI::cpk_calc(qreal USL, qreal LSL, qreal cp,qreal mean)
{
    qreal p1 = ((USL+LSL)/2) - mean;
    qreal p2 = ((USL-LSL)/2);
    qreal cpk = 0;
    if(p2 == 0){
       cpk = 0;
    }else {
        qreal k = abs(p1)/p2;

        cpk = (1-k)*cp;
    }
    return cpk;

}

qreal escp_ADI::average(QVector<qreal> source)
{
    float sum = 0.0, mean = 0.0;
     int i;
     for(i = 0; i < source.count(); i++)
     {
         sum += source.at(i);
     }
     mean = sum/source.count();
     return mean;
}
escp_ADI::~escp_ADI()
{
    delete ui;
}

void escp_ADI::CB_model_append()
{

    ui->CB_MATELNAME->setModel(CB_Matelname_model);
}

void escp_ADI::progressbar_setrange_slot(int value)
{
    ui->progressBar->setRange(0,value);
}

void escp_ADI::progressbar_setvalue_slot(int value)
{
     ui->progressBar->setValue(value);
}

void escp_ADI::on_search_btn_clicked()
{
    filter_map.clear();
    QSqlQuery light_query(light_db);
    light_query.exec("delete from TEMP_SPCDATA");
    light_query.exec("delete from SPCDATA");
    light_query.exec("delete from TOTAL_CP_CPK");
    main_table_model->select();
    result_table_model->select();
    QFuture<void> f1 = run(this,&escp_ADI::search_scp);
    search_watcher->setFuture(f1);
    ui->search_btn->setEnabled(false);

}

void escp_ADI::search_finish()
{
    main_table_model->setTable("SPCDATA");


    main_table_model->setSort(6,Qt::AscendingOrder);
    main_table_model->select();

    //main_table_model->setHeaderData(main_table_model->record().indexOf("D_TX_DTTM"),Qt::Horizontal,tr("D_TX_DTTM"));
    ui->main_table_view->hideColumn(main_table_model->record().indexOf("D_TX_DTTM"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("D_EQ"),Qt::Horizontal,tr("D_EQ"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("D_VALUE"),Qt::Horizontal,tr("D_VALUE"));
    ui->main_table_view->hideColumn(main_table_model->record().indexOf("PR_EQ"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("PR_TX_DTTM"),Qt::Horizontal,tr("PR_TX_DTTM"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("PR_TARGET_VALUE"),Qt::Horizontal,tr("PR_TARGET_VALUE"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("PR_UPPER_SPEC_LIMIT"),Qt::Horizontal,tr("PR_UPPER_SPEC_LIMIT"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("PR_LOWER_SPEC_LIMIT"),Qt::Horizontal,tr("PR_LOWER_SPEC_LIMIT"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("M_TX_DTTM"),Qt::Horizontal,tr("M_TX_DTTM"));
    //main_table_model->setHeaderData(main_table_model->record().indexOf("M_EQ"),Qt::Horizontal,tr("M_EQ"));
    ui->main_table_view->hideColumn(main_table_model->record().indexOf("M_EQ"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("M_TARGET_VALUE"),Qt::Horizontal,tr("M_TARGET_VALUE"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("M_UPPER_SPEC_LIMIT"),Qt::Horizontal,tr("M_UPPER_SPEC_LIMIT"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("M_LOWER_SPEC_LIMIT"),Qt::Horizontal,tr("M_LOWER_SPEC_LIMIT"));
    //main_table_model->setHeaderData(main_table_model->record().indexOf("C_TX_DTTM"),Qt::Horizontal,tr("C_TX_DTTM"));
    ui->main_table_view->hideColumn(main_table_model->record().indexOf("C_TX_DTTM"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("C_EQ"),Qt::Horizontal,tr("C_EQ"));
    //main_table_model->setHeaderData(main_table_model->record().indexOf("L_TX_DTTM"),Qt::Horizontal,tr("L_TX_DTTM"));
    ui->main_table_view->hideColumn(main_table_model->record().indexOf("L_TX_DTTM"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("L_EQ"),Qt::Horizontal,tr("L_EQ"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("L_VALUE1"),Qt::Horizontal,tr("L_VALUE1"));
    //main_table_model->setHeaderData(main_table_model->record().indexOf("P_TX_DTTM"),Qt::Horizontal,tr("P_TX_DTTM"));
    ui->main_table_view->hideColumn(main_table_model->record().indexOf("P_TX_DTTM"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("P_EQ"),Qt::Horizontal,tr("P_EQ"));
    //main_table_model->setHeaderData(main_table_model->record().indexOf("E_TX_DTTM"),Qt::Horizontal,tr("E_TX_DTTM"));
    ui->main_table_view->hideColumn(main_table_model->record().indexOf("E_TX_DTTM"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("E_EQ"),Qt::Horizontal,tr("E_EQ"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("E_VALUE1"),Qt::Horizontal,tr("E_VALUE1"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("E_VALUE2"),Qt::Horizontal,tr("E_VALUE2"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("E_VALUE3"),Qt::Horizontal,tr("E_VALUE3"));
    main_table_model->setHeaderData(main_table_model->record().indexOf("E_VALUE4"),Qt::Horizontal,tr("E_VALUE4"));


    result_table_model->setTable("TOTAL_CP_CPK");
    result_table_model->select();

    result_table_model->setHeaderData(result_table_model->record().indexOf("PR_UPPER_SPEC_LIMIT"),Qt::Horizontal,tr("PR_UPPER_SPEC_LIMIT"));
    result_table_model->setHeaderData(result_table_model->record().indexOf("PR_LOWER_SPEC_LIMIT"),Qt::Horizontal,tr("PR_LOWER_SPEC_LIMIT"));
    result_table_model->setHeaderData(result_table_model->record().indexOf("M_UPPER_SPEC_LIMIT"),Qt::Horizontal,tr("M_UPPER_SPEC_LIMIT"));
    result_table_model->setHeaderData(result_table_model->record().indexOf("M_LOWER_SPEC_LIMIT"),Qt::Horizontal,tr("M_LOWER_SPEC_LIMIT"));

    ui->main_table_view->horizontalHeader()->resizeSection(0,25);

    QString split;


    split = "MATERIAL";

//    QString column_name1 ;
//    QString column_name2 ;
//    if(ui->CB_CP->isChecked()){
//        column_name1 = "PR";
//        column_name2 = "M";
//        chart_maker_ver1(column_name1,column_name2,split);
//    }

//    if(ui->CB_CPK->isChecked()){
//        column_name1 = "PR_CPK";
//        column_name2 = "M_CPK";
//        chart_maker_ver1(column_name1,column_name2,split);
//    }
//    if(ui->CB_SIGMA->isChecked()){
//        column_name1 = "PR_SIGMA";
//        column_name2 = "M_SIGMA";
//        chart_maker_ver1(column_name1,column_name2,split);
//    }

    ui->search_btn->setEnabled(true);






}

void escp_ADI::slot_search_progressbar_setmax(int arg1)
{
    ui->search_bar->setMaximum(arg1);
}

void escp_ADI::slot_search_progressbar_setvalue(int arg1)
{
    ui->search_bar->setValue(arg1);
}

void escp_ADI::slot_TOTAL_CP_CPK_make()
{
    QSqlQuery light_query(light_db);
    QSqlQuery light_query2(light_db);
    QSqlQuery light_query3(light_db);

    light_query.exec("delete from TOTAL_CP_CPK");


    light_query3.exec(QString("select * from SPCDATA where USE = '0'"));
    filter_string2 = " AND 1 = 1 ";
    while(light_query3.next()){
        filter_string2 = filter_string2.append(QString(" AND CASSETTE_ID <> '%2' ").arg(light_query3.value("CASSETTE_ID").toString()));
    }

    light_query.exec(QString("select count(*) rowcount from SPCDATA wehre %1 %2 group by MATERIAL_ID ").arg(filter_string).arg(filter_string2));
    if(light_query.next()){
        emit signal_search_progressbar_setmax(light_query.value("rowcount").toInt());
    }
    light_query.exec(QString("select MATERIAL_ID from SPCDATA where %1 %2 group by MATERIAL_ID ").arg(filter_string).arg(filter_string2));


    while(light_query.next()){
        light_query2.exec(QString("select * from SPCDATA where MATERIAL_ID = '%1' AND %2 ").arg(light_query.value("MATERIAL_ID").toString()).arg(filter_string));
        QVector<qreal> PR_standard_source;
        QVector<qreal> M_standard_source;
        QVector<qreal> CD_standard_source;

        while(light_query2.next()){
            if(light_query2.value("PR_VALUE1").toReal() != 0){
                if(calc_point == 7 ){
                    PR_standard_source.append(light_query2.value("PR_VALUE1").toReal());
                }
                PR_standard_source.append(light_query2.value("PR_VALUE2").toReal());
                PR_standard_source.append(light_query2.value("PR_VALUE3").toReal());
                PR_standard_source.append(light_query2.value("PR_VALUE4").toReal());
                PR_standard_source.append(light_query2.value("PR_VALUE5").toReal());
                PR_standard_source.append(light_query2.value("PR_VALUE6").toReal());
                if(calc_point == 7 ){
                    PR_standard_source.append(light_query2.value("PR_VALUE7").toReal());
                }
            }
            if(light_query2.value("M_VALUE1").toReal() != 0){
                if(calc_point == 7){
                    M_standard_source.append(light_query2.value("M_VALUE1").toReal());
                }
                M_standard_source.append(light_query2.value("M_VALUE2").toReal());
                M_standard_source.append(light_query2.value("M_VALUE3").toReal());
                M_standard_source.append(light_query2.value("M_VALUE4").toReal());
                M_standard_source.append(light_query2.value("M_VALUE5").toReal());
                M_standard_source.append(light_query2.value("M_VALUE6").toReal());
                if(calc_point == 7){
                    M_standard_source.append(light_query2.value("M_VALUE7").toReal());
                }
            }
            if(light_query2.value("CD_BIAS_VALUE1").toReal() != 0){
                if(calc_point == 7){
                    CD_standard_source.append(light_query2.value("CD_BIAS_VALUE1").toReal());
                }
                CD_standard_source.append(light_query2.value("CD_BIAS_VALUE2").toReal());
                CD_standard_source.append(light_query2.value("CD_BIAS_VALUE3").toReal());
                CD_standard_source.append(light_query2.value("CD_BIAS_VALUE4").toReal());
                CD_standard_source.append(light_query2.value("CD_BIAS_VALUE5").toReal());
                CD_standard_source.append(light_query2.value("CD_BIAS_VALUE6").toReal());
                if(calc_point == 7){
                    CD_standard_source.append(light_query2.value("CD_BIAS_VALUE7").toReal());
                }
            }
        }
        light_query2.first();
        qreal PR_UPPER_SPEC_LIMIT = light_query2.value("PR_UPPER_SPEC_LIMIT").toReal();
        qreal PR_LOWER_SPEC_LIMIT = light_query2.value("PR_LOWER_SPEC_LIMIT").toReal();
        qreal PR_standard_result = calculateSD(PR_standard_source);
        qreal PR_mean = average(PR_standard_source);
        qreal PR_cp = (PR_UPPER_SPEC_LIMIT - PR_LOWER_SPEC_LIMIT)/(6*PR_standard_result);
        qreal PR_cpk = cpk_calc(PR_UPPER_SPEC_LIMIT,PR_LOWER_SPEC_LIMIT,PR_cp,PR_mean);
        if(PR_standard_result == 0){
            PR_cp = 0;
            PR_cpk = 0;
            PR_standard_result  = 0;
        }
        qreal M_UPPER_SPEC_LIMIT = light_query2.value("M_UPPER_SPEC_LIMIT").toReal();
        qreal M_LOWER_SPEC_LIMIT = light_query2.value("M_LOWER_SPEC_LIMIT").toReal();
        qreal M_standard_result = calculateSD(M_standard_source);
        qreal M_mean = average(M_standard_source);
        qreal M_cp = (M_UPPER_SPEC_LIMIT - M_LOWER_SPEC_LIMIT)/(6*M_standard_result);
        qreal M_cpk = cpk_calc(M_UPPER_SPEC_LIMIT,M_LOWER_SPEC_LIMIT,M_cp,M_mean);
        if(M_standard_result == 0){
            M_cp = 0;
            M_cpk = 0;
            M_standard_result  = 0;
        }

        qreal CD_BIAS_UPPER_SPEC_LIMIT = light_query2.value("CD_BIAS_USL").toReal();
        qreal CD_BIAS_LOWER_SPEC_LIMIT = light_query2.value("CD_BIAS_LSL").toReal();
        qreal CD_BIAS_standard_result = calculateSD(CD_standard_source);
        qreal CD_BIAS_mean = average(CD_standard_source);
        qreal CD_BIAS_cp = (CD_BIAS_UPPER_SPEC_LIMIT - CD_BIAS_LOWER_SPEC_LIMIT)/(6*CD_BIAS_standard_result);
        qreal CD_BIAS_cpk = cpk_calc(CD_BIAS_UPPER_SPEC_LIMIT,CD_BIAS_LOWER_SPEC_LIMIT,CD_BIAS_cp,CD_BIAS_mean);
        if(CD_BIAS_standard_result == 0){
            CD_BIAS_cp = 0;
            CD_BIAS_cpk = 0;
            CD_BIAS_standard_result  = 0;
        }



        light_query2.exec(QString("insert into [TOTAL_CP_CPK] values ('%1',%2,%3,%4,%5,%6,'%7','%8','%9',%10,%11,%12,%13,%14,'%15','%16','%17',%18,%19,%20,%21,%22,'%23','%24','%25')")
                          .arg(light_query.value("MATERIAL_ID").toString())
                          .arg(PR_cp).arg(PR_cpk).arg(PR_mean).arg(PR_standard_result).arg(PR_standard_source.count())
                          .arg(light_query2.value("PR_TARGET_VALUE").toString())
                          .arg(light_query2.value("PR_UPPER_SPEC_LIMIT").toString())
                          .arg(light_query2.value("PR_LOWER_SPEC_LIMIT").toString())
                          .arg(M_cp).arg(M_cpk).arg(M_mean).arg(M_standard_result).arg(M_standard_source.count())
                          .arg(light_query2.value("M_TARGET_VALUE").toString())
                          .arg(light_query2.value("M_UPPER_SPEC_LIMIT").toString())
                          .arg(light_query2.value("M_LOWER_SPEC_LIMIT").toString())
                          .arg(CD_BIAS_cp).arg(CD_BIAS_cpk).arg(CD_BIAS_mean).arg(CD_BIAS_standard_result).arg(CD_standard_source.count())
                          .arg(light_query2.value("CD_BIAS_TARGET").toString())
                          .arg(light_query2.value("CD_BIAS_USL").toString())
                          .arg(light_query2.value("CD_BIAS_LSL").toString())
                );
        qDebug()<<light_query2.lastQuery();
    }
    result_table_model->select();
}

void escp_ADI::slot_calc_point_change()
{
    QSqlQuery light_query(light_db);
    QSqlQuery light_query2(light_db);
    light_query.exec("select * from SPCDATA");
    while(light_query.next()){
        QString CASSETTE_ID = light_query.value("CASSETTE_ID").toString();
        QString PR_TX_DTTM = light_query.value("PR_TX_DTTM").toString();
        QString M_TX_DTTM = light_query.value("M_TX_DTTM").toString();
        qreal PR_VALUE1 = light_query.value("PR_VALUE1").toDouble();
        qreal PR_VALUE2 = light_query.value("PR_VALUE2").toDouble();
        qreal PR_VALUE3 = light_query.value("PR_VALUE3").toDouble();
        qreal PR_VALUE4 = light_query.value("PR_VALUE4").toDouble();
        qreal PR_VALUE5 = light_query.value("PR_VALUE5").toDouble();
        qreal PR_VALUE6 = light_query.value("PR_VALUE6").toDouble();
        qreal PR_VALUE7 = light_query.value("PR_VALUE7").toDouble();
        qreal PR_UPPER_SPEC_LIMIT = light_query.value("PR_UPPER_SPEC_LIMIT").toDouble();
        qreal PR_LOWER_SPEC_LIMIT = light_query.value("PR_LOWER_SPEC_LIMIT").toDouble();
        qreal M_VALUE1 = light_query.value("M_VALUE1").toDouble();
        qreal M_VALUE2 = light_query.value("M_VALUE2").toDouble();
        qreal M_VALUE3 = light_query.value("M_VALUE3").toDouble();
        qreal M_VALUE4 = light_query.value("M_VALUE4").toDouble();
        qreal M_VALUE5 = light_query.value("M_VALUE5").toDouble();
        qreal M_VALUE6 = light_query.value("M_VALUE6").toDouble();
        qreal M_VALUE7 = light_query.value("M_VALUE7").toDouble();
        qreal M_UPPER_SPEC_LIMIT = light_query.value("M_UPPER_SPEC_LIMIT").toDouble();
        qreal M_LOWER_SPEC_LIMIT = light_query.value("M_LOWER_SPEC_LIMIT").toDouble();

        QVector<qreal> standard_source;
        if(calc_point == 7){
            standard_source.append(PR_VALUE1);
        }
        standard_source.append(PR_VALUE2);
        standard_source.append(PR_VALUE3);
        standard_source.append(PR_VALUE4);
        standard_source.append(PR_VALUE5);
        standard_source.append(PR_VALUE6);
        if(calc_point == 7){
            standard_source.append(PR_VALUE7);
        }
        qreal standard_result = calculateSD(standard_source);
        qreal PR_mean = average(standard_source);
        qreal cp = (PR_UPPER_SPEC_LIMIT - PR_LOWER_SPEC_LIMIT)/(6*standard_result);
        qreal cpk = cpk_calc(PR_UPPER_SPEC_LIMIT,PR_LOWER_SPEC_LIMIT,cp,PR_mean);
        qreal PR_CP = 0;
        qreal PR_CPK = 0;
        qreal PR_SIGMA = 0;
        if(standard_result == 0){
            PR_CP = 0;
            PR_CPK = 0;
            PR_SIGMA  = 0;
        }else {
            PR_CP = cp;
            PR_CPK = cpk;
            PR_SIGMA  = standard_result;
        }
        standard_source.clear();
        if(calc_point == 7){
            standard_source.append(M_VALUE1);
        }
        standard_source.append(M_VALUE2);
        standard_source.append(M_VALUE3);
        standard_source.append(M_VALUE4);
        standard_source.append(M_VALUE5);
        standard_source.append(M_VALUE6);
        if(calc_point == 7){
            standard_source.append(M_VALUE7);
        }
        qreal M_CP = 0;
        qreal M_CPK = 0;
        qreal M_SIGMA = 0;
        standard_result = calculateSD(standard_source);
        qreal M_mean = average(standard_source);
        cp = (M_UPPER_SPEC_LIMIT - M_LOWER_SPEC_LIMIT)/(6*standard_result);

        cpk = cpk_calc(M_UPPER_SPEC_LIMIT,M_LOWER_SPEC_LIMIT,cp,M_mean);

        if(standard_result == 0){
            M_CP = 0;
            M_CPK = 0;
            M_SIGMA  = 0;
        }else {
            M_CP = cp;
            M_CPK = cpk;
            M_SIGMA  = standard_result;
        }

        qreal CD_BIAS_VALUE[8] = {0,0,0,0,0,0,0,0};
        CD_BIAS_VALUE[1] = light_query.value("CD_BIAS_VALUE1").toReal();
        CD_BIAS_VALUE[2] = light_query.value("CD_BIAS_VALUE2").toReal();
        CD_BIAS_VALUE[3] = light_query.value("CD_BIAS_VALUE3").toReal();
        CD_BIAS_VALUE[4] = light_query.value("CD_BIAS_VALUE4").toReal();
        CD_BIAS_VALUE[5] = light_query.value("CD_BIAS_VALUE5").toReal();
        CD_BIAS_VALUE[6] = light_query.value("CD_BIAS_VALUE6").toReal();
        CD_BIAS_VALUE[7] = light_query.value("CD_BIAS_VALUE7").toReal();
        qreal CD_BIAS_USL = light_query.value("CD_BIAS_USL").toReal();
        qreal CD_BIAS_LSL = light_query.value("CD_BIAS_LSL").toReal();

        standard_source.clear();
        if(calc_point == 7){
            standard_source.append(CD_BIAS_VALUE[1]);
        }
        standard_source.append(CD_BIAS_VALUE[2]);
        standard_source.append(CD_BIAS_VALUE[3]);
        standard_source.append(CD_BIAS_VALUE[4]);
        standard_source.append(CD_BIAS_VALUE[5]);
        standard_source.append(CD_BIAS_VALUE[6]);
        if(calc_point == 7){
            standard_source.append(CD_BIAS_VALUE[7]);
        }
        qreal CD_BIAS_CP = 0;
        qreal CD_BIAS_CPK = 0;
        qreal CD_BIAS_SIGMA = 0;
        standard_result = calculateSD(standard_source);
        qreal CD_BIAS_mean = average(standard_source);
        cp = (CD_BIAS_USL - CD_BIAS_LSL)/(6*standard_result);

        cpk = cpk_calc(CD_BIAS_USL,CD_BIAS_LSL,cp,CD_BIAS_mean);
        if(standard_result == 0){
            CD_BIAS_CP = 0;
            CD_BIAS_CPK = 0;
            CD_BIAS_SIGMA  = 0;
        }else {
            CD_BIAS_CP = cp;
            CD_BIAS_CPK = cpk;
            CD_BIAS_SIGMA  = standard_result;
        }

        QString update_sql = QString("update SPCDATA SET PR_CP = %1,PR_CPK = %2,PR_SIGMA = %3,"
                                     "M_CP = %4,M_CPK = %5,M_SIGMA = %6,PR_AVG=%7,M_AVG=%8, "
                                     "CD_BIAS_CP = %9,CD_BIAS_CPK = %10,CD_BIAS_AVG = %11,CD_BIAS_SIGMA=%12 "
                                     "WHERE "
                                     "CASSETTE_ID = '%13' AND PR_TX_DTTM = '%14' AND M_TX_DTTM = '%15' ")
                                    .arg(PR_CP).arg(PR_CPK).arg(PR_SIGMA).arg(M_CP).arg(M_CPK).arg(M_SIGMA)
                                    .arg(PR_mean).arg(M_mean).arg(CD_BIAS_CP).arg(CD_BIAS_CPK).arg(CD_BIAS_mean).arg(CD_BIAS_SIGMA)
                                    .arg(CASSETTE_ID).arg(PR_TX_DTTM).arg(M_TX_DTTM);
        light_query2.exec(update_sql);

    }
    main_table_model->select();


}

void escp_ADI::slot_main_model_check(int checked)
{
    slot_TOTAL_CP_CPK_make();
}

//void escp_ADI::on_machine_add_btn_clicked()
//{
//    escp_adi_machine_setpopup popup;
//    popup.exec();
//    CB_machine_name_update();
//}

//void escp_ADI::CB_machine_name_update()
//{
//    CB_Machine_model->clear();
//    QSqlQuery query(my_mesdb);
//    query.exec("select machine_name,machine_code from escp_search_machine_table where program_part = 'ADI' order by  machine_name asc ");
//    CB_Machine_model->appendRow(new QStandardItem(""));
//    while(query.next()){
//        CB_Machine_model->appendRow(new QStandardItem(query.value("machine_name").toString()));
//    }
//}

void escp_ADI::chart_maker_ver1(QString column_name1,QString column_name2,QString column_name3, QString split_line)
{
    escp_chart_widget *chart_widget = new escp_chart_widget(my_mesdb);

    escp_chart *chart1 = new escp_chart();
    escp_chart_view *chart_view1 = new escp_chart_view(chart1);

    escp_chart *chart2 = new escp_chart();
    escp_chart_view *chart_view2 = new escp_chart_view(chart2);

    escp_chart *chart3 = new escp_chart();
    escp_chart_view *chart_view3 = new escp_chart_view(chart3);


    chart1->setTitle(column_name1);

    chart2->setTitle(column_name2);

    chart3->setTitle(column_name3);

    QStringList chart1_x_categories;
    QBarCategoryAxis *chart1_x = new QBarCategoryAxis();
    chart_widget->chart1_x = chart1_x;
    QValueAxis *chart1_y = new QValueAxis();
    chart_widget->chart1_y = chart1_y;
    chart1_x->setLabelsAngle(90);

    QStringList chart2_x_categories;
    QBarCategoryAxis *chart2_x = new QBarCategoryAxis();
    chart_widget->chart2_x = chart2_x;
    QValueAxis *chart2_y = new QValueAxis();
    chart_widget->chart2_y = chart2_y;
    chart2_x->setLabelsAngle(90);

    QStringList chart3_x_categories;
    QBarCategoryAxis *chart3_x = new QBarCategoryAxis();
    chart_widget->chart3_x = chart3_x;
    QValueAxis *chart3_y = new QValueAxis();
    chart_widget->chart3_y = chart3_y;
    chart3_x->setLabelsAngle(90);

    chart1->legend()->setAlignment(Qt::AlignLeft);
    chart2->legend()->setAlignment(Qt::AlignLeft);
    chart3->legend()->setAlignment(Qt::AlignLeft);


    chart1->setAxisX(chart1_x);
    chart1->setAxisY(chart1_y);

    chart2->setAxisX(chart2_x);
    chart2->setAxisY(chart2_y);

    chart3->setAxisX(chart3_x);
    chart3->setAxisY(chart3_y);

    QSqlQuery query_spec(my_mesdb);
    query_spec.exec("select * from escp_spec_data where spec_name = 'ADI_SPEC' ");
    query_spec.next();
    qreal PR_SPEC = query_spec.value("spec_USL").toReal();

    query_spec.exec("select * from escp_spec_data where spec_name = 'ACI_SPEC' ");
    query_spec.next();
    qreal M_SPEC = query_spec.value("spec_USL").toReal();

    query_spec.exec("select * from escp_spec_data where spec_name = 'CD_B_SPEC' ");
    query_spec.next();
    qreal CD_B_SPEC = query_spec.value("spec_USL").toReal();

    QSqlQuery light_query(light_db);
    QSqlQuery light_query2(light_db);
    QSqlQuery light_query3(light_db);
    QBarSeries *chart1_bar_series = new QBarSeries();
    QLineSeries *chart1_spec_series = new QLineSeries();
    chart1_spec_series->setColor("red");

    chart1_bar_series->setLabelsAngle(90);
    chart1_bar_series->setLabelsPosition(QAbstractBarSeries::LabelsInsideEnd);
    chart_widget->chart1_bar_series = chart1_bar_series;
    QBarSet *chart1_cp_bar_set = new QBarSet("cp");
    chart1_cp_bar_set->setLabelColor("black");

    chart_widget->chart1_cp_bar_set = chart1_cp_bar_set;
    QBarSet *chart1_cpk_bar_set = new QBarSet("cpk");
    chart1_cpk_bar_set->setLabelColor("black");
    chart_widget->chart1_cpk_bar_set = chart1_cpk_bar_set;
    QBarSet *chart1_sigma_bar_set = new QBarSet("sigma");
    chart1_sigma_bar_set->setLabelColor("black");
    chart_widget->chart1_sigma_bar_set = chart1_sigma_bar_set;

    QBarSeries *chart2_bar_series = new QBarSeries();
    QLineSeries *chart2_spec_series = new QLineSeries();
    chart2_spec_series->setColor("red");
    chart2_bar_series->setLabelsAngle(90);
    chart2_bar_series->setLabelsPosition(QAbstractBarSeries::LabelsInsideEnd);
    chart_widget->chart2_bar_series = chart2_bar_series;
    QBarSet *chart2_cp_bar_set = new QBarSet("cp");
    chart2_cp_bar_set->setLabelColor("black");
    chart_widget->chart2_cp_bar_set = chart2_cp_bar_set;
    QBarSet *chart2_cpk_bar_set = new QBarSet("cpk");
    chart2_cpk_bar_set->setLabelColor("black");
    chart_widget->chart2_cpk_bar_set = chart2_cpk_bar_set;
    QBarSet *chart2_sigma_bar_set = new QBarSet("sigma");
    chart2_sigma_bar_set->setLabelColor("black");
    chart_widget->chart2_sigma_bar_set = chart2_sigma_bar_set;

    QBarSeries *chart3_bar_series = new QBarSeries();
    QLineSeries *chart3_spec_series = new QLineSeries();
    chart3_spec_series->setColor("red");
    chart3_bar_series->setLabelsAngle(90);
    chart3_bar_series->setLabelsPosition(QAbstractBarSeries::LabelsInsideEnd);
    chart_widget->chart3_bar_series = chart3_bar_series;
    QBarSet *chart3_cp_bar_set = new QBarSet("cp");
    chart3_cp_bar_set->setLabelColor("black");
    chart_widget->chart3_cp_bar_set = chart3_cp_bar_set;
    QBarSet *chart3_cpk_bar_set = new QBarSet("cpk");
    chart3_cpk_bar_set->setLabelColor("black");
    chart_widget->chart3_cpk_bar_set = chart3_cpk_bar_set;
    QBarSet *chart3_sigma_bar_set = new QBarSet("sigma");
    chart3_sigma_bar_set->setLabelColor("black");
    chart_widget->chart3_sigma_bar_set = chart3_sigma_bar_set;


    QVector<qreal> chart1_cp_points;
    QVector<qreal> chart1_cpk_points;
    QVector<qreal> chart1_sigma_points;
    QVector<qreal> chart2_cp_points;
    QVector<qreal> chart2_cpk_points;
    QVector<qreal> chart2_sigma_points;
    QVector<qreal> chart3_cp_points;
    QVector<qreal> chart3_cpk_points;
    QVector<qreal> chart3_sigma_points;
    QVector<qreal> chart1_spec_points;
    QVector<qreal> chart2_spec_points;
    QVector<qreal> chart3_spec_points;

    if(split_line == "MATERIAL"){
        light_query.exec(QString("select * from TOTAL_CP_CPK group by MATERIAL_ID order by MATERIAL_ID asc"));
        int MATERIAL_ID_count = 0;
        while(light_query.next()){
            chart1_x_categories<<light_query.value("MATERIAL_ID").toString();
            chart1_cp_bar_set->append(light_query.value("PR_CP").toReal());
            chart1_cp_points.append(light_query.value("PR_CP").toReal());
            chart1_cpk_bar_set->append(light_query.value("PR_CPK").toReal());
            chart1_cpk_points.append(light_query.value("PR_CPK").toReal());
            chart1_sigma_bar_set->append(light_query.value("PR_SIGMA").toReal());
            chart1_sigma_points.append(light_query.value("PR_SIGMA").toReal());
            chart2_x_categories<<light_query.value("MATERIAL_ID").toString();
            chart2_cp_bar_set->append(light_query.value("M_CP").toReal());
            chart2_cp_points.append(light_query.value("M_CP").toReal());
            chart2_cpk_bar_set->append(light_query.value("M_CPK").toReal());
            chart2_cpk_points.append(light_query.value("M_CPK").toReal());
            chart2_sigma_bar_set->append(light_query.value("M_SIGMA").toReal());
            chart2_sigma_points.append(light_query.value("M_SIGMA").toReal());
            chart3_x_categories<<light_query.value("MATERIAL_ID").toString();
            chart3_cp_bar_set->append(light_query.value("CD_BIAS_CP").toReal());
            chart3_cp_points.append(light_query.value("CD_BIAS_CP").toReal());
            chart3_cpk_bar_set->append(light_query.value("CD_BIAS_CPK").toReal());
            chart3_cpk_points.append(light_query.value("CD_BIAS_CPK").toReal());
            chart3_sigma_bar_set->append(light_query.value("CD_BIAS_SIGMA").toReal());
            chart3_sigma_points.append(light_query.value("CD_BIAS_SIGMA").toReal());
            chart1_spec_series->append(MATERIAL_ID_count,PR_SPEC);
            chart2_spec_series->append(MATERIAL_ID_count,M_SPEC);
            chart3_spec_series->append(MATERIAL_ID_count,CD_B_SPEC);
            chart1_spec_points.append(PR_SPEC);
            chart2_spec_points.append(M_SPEC);
            chart3_spec_points.append(CD_B_SPEC);
            MATERIAL_ID_count++;
        }
        chart1_x->append(chart1_x_categories);
        chart1_bar_series->append(chart1_cp_bar_set);
        chart1_bar_series->append(chart1_cpk_bar_set);
        chart1_bar_series->append(chart1_sigma_bar_set);
        chart2_x->append(chart2_x_categories);
        chart2_bar_series->append(chart2_cp_bar_set);
        chart2_bar_series->append(chart2_cpk_bar_set);
        chart2_bar_series->append(chart2_sigma_bar_set);
        chart3_x->append(chart3_x_categories);
        chart3_bar_series->append(chart3_cp_bar_set);
        chart3_bar_series->append(chart3_cpk_bar_set);
        chart3_bar_series->append(chart3_sigma_bar_set);



        chart1->addSeries(chart1_bar_series);
        chart1->addSeries(chart1_spec_series);
        chart1_bar_series->attachAxis(chart1_x);
        chart1_bar_series->attachAxis(chart1_y);
        chart1_spec_series->attachAxis(chart1_x);
        chart1_spec_series->attachAxis(chart1_y);
        chart2->addSeries(chart2_bar_series);
        chart2->addSeries(chart2_spec_series);
        chart2_bar_series->attachAxis(chart2_x);
        chart2_bar_series->attachAxis(chart2_y);
        chart2_spec_series->attachAxis(chart2_y);
        chart2_spec_series->attachAxis(chart2_y);
        chart3->addSeries(chart3_bar_series);
        chart3->addSeries(chart3_spec_series);
        chart3_bar_series->attachAxis(chart3_x);
        chart3_bar_series->attachAxis(chart3_y);
        chart3_spec_series->attachAxis(chart3_y);
        chart3_spec_series->attachAxis(chart3_y);
    }
    chart_widget->chart1_cp_points = chart1_cp_points;
    chart_widget->chart1_cpk_points = chart1_cpk_points;
    chart_widget->chart1_sigma_points = chart1_sigma_points;
    chart_widget->chart1_spec_points = chart1_spec_points;


    chart_widget->chart2_cp_points = chart2_cp_points;
    chart_widget->chart2_cpk_points = chart2_cpk_points;
    chart_widget->chart2_sigma_points = chart2_sigma_points;
    chart_widget->chart2_spec_points = chart2_spec_points;

    chart_widget->chart3_cp_points = chart3_cp_points;
    chart_widget->chart3_cpk_points = chart3_cpk_points;
    chart_widget->chart3_sigma_points = chart3_sigma_points;
    chart_widget->chart3_spec_points = chart3_spec_points;

    light_query.exec(QString("select MIN(%1) MIN_VALUE1,MAX(%1) MAX_VALUE1,"
                     "MIN(%2) MIN_VALUE2,MAX(%2) MAX_VALUE2 from SPCDATA ")
                     .arg(column_name1).arg(column_name2).arg(column_name3));
    if(light_query.next()){
        qreal MIN_VALUE1  =  light_query.value("MIN_VALUE1").toReal();
        qreal MAX_VALUE1  =  light_query.value("MAX_VALUE1").toReal();
        qreal MIN_VALUE2  =  light_query.value("MIN_VALUE2").toReal();
        qreal MAX_VALUE2  =  light_query.value("MAX_VALUE2").toReal();
//        qreal MIN_VALUE3  =  light_query.value("MIN_VALUE3").toReal();
//        qreal MAX_VALUE3  =  light_query.value("MAX_VALUE3").toReal();
        chart1_y->setRange(MIN_VALUE1,MAX_VALUE1);
        chart2_y->setRange(MIN_VALUE2,MAX_VALUE2);
//        chart3_y->setRange(MIN_VALUE3,MAX_VALUE3);
    }

    chart_widget->chart1_layout->addWidget(chart_view1);
    chart_widget->chart1 = chart1;
    chart_widget->chart2_layout->addWidget(chart_view2);
    chart_widget->chart2 = chart2;
    chart_widget->chart3_layout->addWidget(chart_view3);
    chart_widget->chart3 = chart3;
    chart_widget->show();


}

void escp_ADI::main_table_menu(QPoint ex)
{
    int logic = ui->main_table_view->horizontalHeader()->logicalIndexAt(ex);
    escp_filter_popup *popup;
    if(!filter_map.contains(QString("%1").arg(logic))){
        popup = new escp_filter_popup(light_db,logic,main_table_model,search_table_filter);
        filter_map.insert(QString("%1").arg(logic),popup);
    }else {
        popup = filter_map.value(QString("%1").arg(logic));
    }
    QPoint point = ui->main_table_view->horizontalHeader()->viewport()->mapToGlobal(ex);
    popup->setGeometry(point.x(),point.y(),200,200);
    popup->exec();


    filter_string = " 1=1 ";
    foreach (escp_filter_popup * items, filter_map) {
        qDebug()<<items->fieldname;
        for(int i=0;i<items->filter_model->rowCount();i++){
            QModelIndex item_index1 = items->filter_model->index(i,0);
            QModelIndex item_index2 = items->filter_model->index(i,1);
            int item_data = item_index1.data(Qt::UserRole).toInt();
            if(item_data == Qt::Unchecked){
                filter_string = filter_string.append(QString(" AND %1 <> '%2' ").arg(items->fieldname).arg(item_index2.data().toString()));
            }
        }
    }
    qDebug()<<filter_string;
    main_table_model->setFilter(filter_string);
    main_table_model->select();
    slot_TOTAL_CP_CPK_make();


}

void escp_ADI::main_table_double_click()
{

}
/*
void escp_ADI::on_CB_D_view_toggled(bool checked)
{

    if(main_table_model->columnCount() >0 ){
        if(checked){
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("D_TX_DTTM"));
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("D_EQ"));
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("D_VALUE"));
         }else {
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("D_TX_DTTM"));
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("D_EQ"));
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("D_VALUE"));
         }

    }
}

void escp_ADI::on_CB_C_view_toggled(bool checked)
{
    if(main_table_model->columnCount() >0 ){
        if(checked){
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("C_TX_DTTM"));
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("C_EQ"));
         }else {
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("C_TX_DTTM"));
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("C_EQ"));
         }
    }
}

void escp_ADI::on_CB_L_view_toggled(bool checked)
{
    if(main_table_model->columnCount() >0 ){
        if(checked){
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("L_TX_DTTM"));
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("L_EQ"));
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("L_VALUE1"));
         }else {
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("L_TX_DTTM"));
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("L_EQ"));
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("L_VALUE1"));
         }
    }
}

void escp_ADI::on_CB_P_view_toggled(bool checked)
{
    if(main_table_model->columnCount() >0 ){
        if(checked){
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("P_TX_DTTM"));
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("P_EQ"));
         }else {
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("P_TX_DTTM"));
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("P_EQ"));
         }
    }
}


void escp_ADI::on_CB_E_view_toggled(bool checked)
{
    if(main_table_model->columnCount() >0 ){
        if(checked){
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("E_TX_DTTM"));
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("E_EQ"));
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("E_VALUE1"));
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("E_VALUE2"));
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("E_VALUE3"));
             ui->main_table_view->horizontalHeader()->showSection(main_table_model->record().indexOf("E_VALUE4"));
         }else {
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("E_TX_DTTM"));
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("E_EQ"));
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("E_VALUE1"));
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("E_VALUE2"));
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("E_VALUE3"));
             ui->main_table_view->horizontalHeader()->hideSection(main_table_model->record().indexOf("E_VALUE4"));
         }
    }
}
*/




void escp_ADI::on_chart_viewer_btn_clicked()
{
    QString split;
    split = "MATERIAL";
    QString column_name1 ;
    QString column_name2 ;
    QString column_name3 ;
    if(ui->CB_CP->isChecked()){
        column_name1 = "PR";
        column_name2 = "M";
        column_name3 = "CD_B";
        chart_maker_ver1(column_name1,column_name2,column_name3,split);
    }
}

void escp_ADI::on_RB_Point5_toggled(bool checked)
{
    calc_point= 5;
    slot_calc_point_change();
    slot_TOTAL_CP_CPK_make();
}

void escp_ADI::on_RB_Point7_toggled(bool checked)
{
    calc_point= 7;
    slot_calc_point_change();
    slot_TOTAL_CP_CPK_make();
}

