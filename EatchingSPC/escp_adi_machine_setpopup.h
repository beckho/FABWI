#ifndef ESCP_ADI_MACHINE_SETPOPUP_H
#define ESCP_ADI_MACHINE_SETPOPUP_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQueryModel>

#include <QSettings>
#include <global_define.h>
#include <QSqlQuery>
#include <QDateTime>
#include <QDebug>
#include <QSqlError>
namespace Ui {
class escp_adi_machine_setpopup;
}

class escp_adi_machine_setpopup : public QDialog
{
    Q_OBJECT

public:
    explicit escp_adi_machine_setpopup(QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    QSqlDatabase ms_mesdb;
    QSqlQueryModel *ms_table_model;
    QSqlQueryModel *my_table_model;
    ~escp_adi_machine_setpopup();

private slots:
    void on_machine_add_btn_clicked();

    void on_machine_del_btn_clicked();

    void on_LE_search_machine_editingFinished();

    void on_search_btn_clicked();

private:
    Ui::escp_adi_machine_setpopup *ui;
};

#endif // ESCP_ADI_MACHINE_SETPOPUP_H
