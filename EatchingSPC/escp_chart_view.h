#ifndef ESCP_CHART_VIEW_H
#define ESCP_CHART_VIEW_H

#include <QObject>
#include <QWidget>

#include <QtCharts/QChartView>
#include <QtWidgets/QRubberBand>
#include <QDebug>
#include <QPointF>
#include <QKeyEvent>
QT_CHARTS_USE_NAMESPACE

class escp_chart_view : public QChartView
{
public:
    escp_chart_view(QChart *chart, QWidget *parent = 0);
    QChart *mchart;
protected:
    void keyPressEvent(QKeyEvent *event);
};

#endif // ESCP_CHART_VIEW_H
