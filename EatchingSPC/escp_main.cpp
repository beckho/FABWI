#include "escp_main.h"
#include "ui_escp_main.h"

ESCP_main::ESCP_main(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ESCP_main)
{
    ui->setupUi(this);
    QProcess process;
    process.setWorkingDirectory(qApp->applicationDirPath());
    process.start("RegAsm.exe NS_Core_Com.dll");
    process.waitForFinished();
    QString output(process.readAllStandardOutput());
    qDebug()<<output;


    NS_core = new NS_Core_Com::NS_Main();
    qDebug()<<NS_core->init_NS_Core();
    escp_ADI *ADI = new escp_ADI(NS_core);
    QMdiSubWindow *subwindows = new QMdiSubWindow();
    subwindows->setWidget(ADI);
    ui->mdiArea->addSubWindow(subwindows);
    subwindows->setWindowState(Qt::WindowMaximized);



}

ESCP_main::~ESCP_main()
{
    delete ui;
}
