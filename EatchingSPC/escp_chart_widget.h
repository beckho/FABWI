#ifndef ESCP_CHART_WIDGET_H
#define ESCP_CHART_WIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <EatchingSPC/escp_chart.h>
#include <QLabel>
#include <QBarSeries>
#include <QBarset>
#include <QBarCategoryAxis>
#include <QValueAxis>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
namespace Ui {
class escp_chart_widget;
}

class escp_chart_widget : public QWidget
{
    Q_OBJECT

public:
    explicit escp_chart_widget(QSqlDatabase my_mesdb,QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    QGridLayout *chart1_layout;
    escp_chart *chart1;
    QGridLayout *chart2_layout;
    escp_chart *chart2;
    QGridLayout *chart3_layout;
    escp_chart *chart3;
    QLabel *LA_PR_metal;
    QLabel *LA_M_metal;
    QLabel *LA_CD_B_metal;

    QBarSeries *chart1_bar_series;
    QLineSeries *chart1_spec_series;
    QBarSet *chart1_cp_bar_set;
    QBarSet *chart1_cpk_bar_set;
    QBarSet *chart1_sigma_bar_set;

    QBarSeries *chart2_bar_series;
    QLineSeries *chart2_spec_series;
    QBarSet *chart2_cp_bar_set;
    QBarSet *chart2_cpk_bar_set;
    QBarSet *chart2_sigma_bar_set;

    QBarSeries *chart3_bar_series;
    QLineSeries *chart3_spec_series;
    QBarSet *chart3_cp_bar_set;
    QBarSet *chart3_cpk_bar_set;
    QBarSet *chart3_sigma_bar_set;

    QBarCategoryAxis *chart1_x ;
    QValueAxis *chart1_y ;

    QBarCategoryAxis *chart2_x ;
    QValueAxis *chart2_y ;

    QBarCategoryAxis *chart3_x ;
    QValueAxis *chart3_y ;


    QVector<qreal> chart1_cp_points;
    QVector<qreal> chart1_cpk_points;
    QVector<qreal> chart1_sigma_points;

    QVector<qreal> chart2_cp_points;
    QVector<qreal> chart2_cpk_points;
    QVector<qreal> chart2_sigma_points;

    QVector<qreal> chart3_cp_points;
    QVector<qreal> chart3_cpk_points;
    QVector<qreal> chart3_sigma_points;

    QVector<qreal> chart1_spec_points;
    QVector<qreal> chart2_spec_points;
    QVector<qreal> chart3_spec_points;


    void remake_chart1();
    void remake_chart2();
    void remake_chart3();
    ~escp_chart_widget();
public slots:
    void chart1_hovered(QPointF point,bool result);
    void chart2_hovered(QPointF point,bool result);
    void chart3_hovered(QPointF point,bool result);

private slots:
    void on_label1_view_btn_clicked();

    void on_label2_view_btn_clicked();

    void on_z_clicked();

    void on_pushButton_2_clicked();

    void on_CB_PR_CP_clicked(bool checked);

    void on_CB_PR_CPK_clicked(bool checked);

    void on_CB_PR_SIGMA_clicked(bool checked);

    void on_CB_M_CP_clicked(bool checked);

    void on_CB_M_CPK_clicked(bool checked);

    void on_CB_M_SIGMA_clicked(bool checked);

    void on_btn_ADI_SPEC_clicked();

    void on_btn_ACI_SPEC_clicked();

    void on_CD_B_view_btn_clicked();

    void on_CD_B_CP_clicked(bool checked);

    void on_CD_B_CPK_clicked(bool checked);

    void on_CD_B_SIGMA_clicked(bool checked);

private:
    Ui::escp_chart_widget *ui;
};

#endif // ESCP_CHART_WIDGET_H
