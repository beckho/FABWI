#ifndef ESCP_FILTER_ITEMTABLE_MODEL_H
#define ESCP_FILTER_ITEMTABLE_MODEL_H
#include <QObject>
#include <QWidget>
#include <QStandardItemModel>
#include <QDebug>
#include <QFile>

class escp_filter_itemtable_model : public QStandardItemModel
{
     Q_OBJECT
public:
    escp_filter_itemtable_model(int rows, int columns, QObject *parent = Q_NULLPTR);
    Qt::ItemFlags flags(const QModelIndex & index) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const ;
    bool setData(const QModelIndex & index1, const QVariant & value, int role = Qt::EditRole);
};

#endif // ESCP_FILTER_ITEMTABLE_MODEL_H
