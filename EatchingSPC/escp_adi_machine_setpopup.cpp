#include "escp_adi_machine_setpopup.h"
#include "ui_escp_adi_machine_setpopup.h"

escp_adi_machine_setpopup::escp_adi_machine_setpopup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::escp_adi_machine_setpopup)
{
    ui->setupUi(this);

    QString mydb_name = QString("MY_MESDB_%1_escp").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    QString server_ip = settings.value("serverip").toString();
    QString db_port = settings.value("dbport").toString();
    settings.endGroup();

    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName(server_ip);
       my_mesdb.setPort(db_port.toInt());
       my_mesdb.setUserName(DBID);
       my_mesdb.setPassword(DBPW);
       my_mesdb.setDatabaseName(DBFABNAME);
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }
    QString msdb_name = QString("MS_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    ms_mesdb = QSqlDatabase::addDatabase("QODBC",msdb_name);
    QString serverinfo = QString("DRIVER={SQL Server};Server=%1;Database=%2;Uid=%3;Port=1433;Pwd=%4").arg(DBMESSERVERIP).arg(DBMESNAME).arg(DBMESUSERNAME).arg(DBMESPW);
    ms_mesdb.setDatabaseName(serverinfo);
    if(!ms_mesdb.open()){
        qDebug()<<"fasle";
        qDebug()<<ms_mesdb.lastError().text();
    }else {
        qDebug()<<"open";
    }
    ms_table_model = new QSqlQueryModel(this);
    ms_table_model->setQuery("select EQUIPMENT_NAME,EQUIPMENT_ID from V_NM_EQUIPMENT with(NOLOCK) where SITE_ID = 'WOSF'AND EQUIPMENT_STATUS = 'USE' AND EQUIPMENT_TYPE = 'EQUIPMENT' order by EQUIPMENT_NAME",ms_mesdb);
    ui->ms_table_view->setModel(ms_table_model);
    my_table_model = new QSqlQueryModel(this);
    my_table_model->setQuery("select machine_name,machine_code from escp_search_machine_table where program_part = 'ADI' order by machine_name",my_mesdb);
    ui->my_table_view->setModel(my_table_model);
}

escp_adi_machine_setpopup::~escp_adi_machine_setpopup()
{
    delete ui;
}

void escp_adi_machine_setpopup::on_machine_add_btn_clicked()
{
    QModelIndexList select_list =  ui->ms_table_view->selectionModel()->selectedIndexes();
    QSqlQuery my_query(my_mesdb);
    for(int i=0;i<select_list.count();i++){
        QString machine_name =  ms_table_model->index(select_list.at(i).row(),0).data().toString();
        QString machine_code =  ms_table_model->index(select_list.at(i).row(),1).data().toString();
        QString query_txt = QString("insert  into escp_search_machine_table  values ('ADI','%1','%2')").arg(machine_name).arg(machine_code);
        my_query.exec(query_txt);
    }
    my_table_model->setQuery("select machine_name,machine_code from escp_search_machine_table where program_part = 'ADI' order by machine_name",my_mesdb);
}

void escp_adi_machine_setpopup::on_machine_del_btn_clicked()
{
    QModelIndexList select_list =  ui->my_table_view->selectionModel()->selectedIndexes();
    QSqlQuery my_query(my_mesdb);
    for(int i=0;i<select_list.count();i++){
        QString machine_name =  my_table_model->index(select_list.at(i).row(),0).data().toString();
        QString machine_code =  my_table_model->index(select_list.at(i).row(),1).data().toString();
        QString query_txt = QString("delete from escp_search_machine_table where program_part = 'ADI' AND machine_name = '%1' AND machine_code = '%2' ").arg(machine_name).arg(machine_code);
        my_query.exec(query_txt);
    }
    my_table_model->setQuery("select machine_name,machine_code from escp_search_machine_table where program_part = 'ADI' order by machine_name",my_mesdb);
}

void escp_adi_machine_setpopup::on_LE_search_machine_editingFinished()
{
    if(ui->LE_search_machine->text()==""){
        ms_table_model->setQuery("select EQUIPMENT_NAME,EQUIPMENT_ID from V_NM_EQUIPMENT with(NOLOCK) where SITE_ID = 'WOSF'AND EQUIPMENT_STATUS = 'USE' AND EQUIPMENT_TYPE = 'EQUIPMENT'  order by EQUIPMENT_NAME",ms_mesdb);
    }else {
        ms_table_model->setQuery(QString("select EQUIPMENT_NAME,EQUIPMENT_ID from V_NM_EQUIPMENT with(NOLOCK) where SITE_ID = 'WOSF'AND EQUIPMENT_STATUS = 'USE' AND EQUIPMENT_TYPE = 'EQUIPMENT' AND EQUIPMENT_NAME LIKE '%%1%'  order by EQUIPMENT_NAME")
                .arg(ui->LE_search_machine->text()),ms_mesdb);
    }

}

void escp_adi_machine_setpopup::on_search_btn_clicked()
{
    if(ui->LE_search_machine->text()==""){
        ms_table_model->setQuery("select EQUIPMENT_NAME,EQUIPMENT_ID from V_NM_EQUIPMENT with(NOLOCK) where SITE_ID = 'WOSF'AND EQUIPMENT_STATUS = 'USE' AND EQUIPMENT_TYPE = 'EQUIPMENT'  order by EQUIPMENT_NAME",ms_mesdb);
    }else {
        ms_table_model->setQuery(QString("select EQUIPMENT_NAME,EQUIPMENT_ID from V_NM_EQUIPMENT with(NOLOCK) where SITE_ID = 'WOSF'AND EQUIPMENT_STATUS = 'MS01' AND EQUIPMENT_TYPE = 'EQUIPMENT' AND EQUIPMENT_NAME LIKE '%%1%'  order by EQUIPMENT_NAME")
                .arg(ui->LE_search_machine->text()),ms_mesdb);
    }
}
