#include "escp_output_widget.h"
#include "ui_escp_output_widget.h"

escp_output_widget::escp_output_widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::escp_output_widget)
{
    ui->setupUi(this);
}

escp_output_widget::~escp_output_widget()
{
    delete ui;
}
