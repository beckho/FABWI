#include "escp_filter_popup.h"
#include "ui_escp_filter_popup.h"

escp_filter_popup::escp_filter_popup(QSqlDatabase light_db,int logic,escp_item_table_model *source_model,QString &filter,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::escp_filter_popup)
{
    ui->setupUi(this);
    this->light_db= light_db;
    this->logic = logic;
    this->filter = filter;
    this->source_model=source_model;
    fieldname =source_model->record().fieldName(logic);
    filter_model = new escp_filter_itemtable_model(0,2,ui->main_filter_table);
    QSqlQuery query(light_db);
    query.exec(QString("select * from %1 group by %2").arg(source_model->tableName()).arg(fieldname));
    qDebug()<<query.lastQuery();
    qDebug()<<query.lastError().text();
    ui->main_filter_table->setModel(filter_model);

    while(query.next()){
        QList<QStandardItem *> items;
        items.append(new QStandardItem(""));
        items.append(new QStandardItem(query.value(fieldname).toString()));
        filter_model->appendRow(items);
        QModelIndex check_index  = filter_model->index(filter_model->rowCount()-1,0);
        filter_model->setData(check_index,Qt::Checked,Qt::CheckStateRole);
    }
    ui->main_filter_table->horizontalHeader()->resizeSection(0,25);

}

escp_filter_popup::~escp_filter_popup()
{
    delete ui;
}

void escp_filter_popup::on_buttonBox_accepted()
{
    this->close();
}
