#ifndef BPROJECTBOARD_H
#define BPROJECTBOARD_H

#include <QWidget>
#include <NIS/nismainwindow.h>
namespace Ui {
class BprojectBoard;
}

class BprojectBoard : public QWidget
{
    Q_OBJECT

public:
    explicit BprojectBoard(QWidget *parent = 0);
    ~BprojectBoard();

private slots:
    void on_infrom_entet_btn_clicked();

    void on_NIS_btn_clicked();

private:
    Ui::BprojectBoard *ui;
};

#endif // BPROJECTBOARD_H
