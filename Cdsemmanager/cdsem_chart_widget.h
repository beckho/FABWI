#ifndef CDSEM_CHART_WIDGET_H
#define CDSEM_CHART_WIDGET_H

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QStandardItemModel>
#include <QDebug>
#include <QSqlRecord>
#include <QSqlError>
#include <Cdsemmanager/cdsem_chartview.h>
#include <Cdsemmanager/cdsem_chart.h>
#include <QLineSeries>
#include <QDateTime>
#include <QDateTimeAxis>
#include <QValueAxis>
namespace Ui {
class cdsem_chart_widget;
}

class cdsem_chart_widget : public QWidget
{
    Q_OBJECT

public:
    explicit cdsem_chart_widget(QString select_time,QString select_collectionid,QString select_A1,QString select_GM1,QString history_count
                                ,QSqlDatabase ms_mesdb, QWidget *parent = 0);
    QString select_time;
    QString select_collectionid;
    QString select_A1;
    QString select_GM1;
    QSqlDatabase ms_mesdb;
    QVector<int> value_colum_list;
    QStandardItemModel *main_table_model;
    cdsem_chart *ADI_chart;
    QLineSeries *ADI_avg_line;
    QLineSeries *ADI_usl_line;
    QLineSeries *ADI_lsl_line;

    cdsem_chart *ACI_chart;
    QLineSeries *ACI_avg_line;
    QLineSeries *ACI_usl_line;
    QLineSeries *ACI_lsl_line;
    ~cdsem_chart_widget();

private slots:
    void on_VALUE_view_toggled(bool checked);

    void on_PR_zoom_reset_clicked();

    void on_PR_point_label_clicked();

    void on_Metal_zoom_reset_clicked();

    void on_Metal_point_label_clicked();

private:
    Ui::cdsem_chart_widget *ui;
};

#endif // CDSEM_CHART_WIDGET_H
