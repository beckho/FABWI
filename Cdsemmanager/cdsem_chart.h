#ifndef CDSEM_CHART_H
#define CDSEM_CHART_H
#include <QObject>
#include <QWidget>
#include <QtCharts/QChart>
#include <QLineSeries>
QT_CHARTS_USE_NAMESPACE

class cdsem_chart : public QChart
{
    Q_OBJECT
public:
    cdsem_chart(QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0);
};

#endif // CDSEM_CHART_H
