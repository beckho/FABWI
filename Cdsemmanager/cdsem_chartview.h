#ifndef CDSEM_CHARTVIEW_H
#define CDSEM_CHARTVIEW_H
#include <QObject>
#include <QWidget>

#include <QtCharts/QChartView>
#include <QtWidgets/QRubberBand>
#include <QDebug>
#include <QPointF>
#include <QKeyEvent>
QT_CHARTS_USE_NAMESPACE

class cdsem_chartview : public QChartView
{
public:
    cdsem_chartview(QChart *chart, QWidget *parent = 0);
    QChart *mchart;
protected:
    void keyPressEvent(QKeyEvent *event);
};

#endif // CDSEM_CHARTVIEW_H
