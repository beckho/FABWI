#include "cdsem_chart_widget.h"
#include "ui_cdsem_chart_widget.h"

cdsem_chart_widget::cdsem_chart_widget(QString select_time, QString select_collectionid, QString select_A1, QString select_GM1, QString history_count, QSqlDatabase ms_mesdb, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::cdsem_chart_widget)
{
    ui->setupUi(this);
    this->select_time = select_time;
    this->select_collectionid = select_collectionid;
    this->select_A1 = select_A1;
    this->select_GM1 = select_GM1;
    this->ms_mesdb = ms_mesdb;
    main_table_model = new QStandardItemModel();
    ui->main_table_view->setModel(main_table_model);
    QList<int> sizes;
    sizes.append(700);
    sizes.append(300);
    ui->splitter_2->setSizes(sizes);

//    QSqlQuery first_query(ms_mesdb);
    QString CASETTE_ID_LIST_txt = "Declare @CASSETTE_ID_LIST Table("
            "LOT_ID varchar(50), "
            "CASSETTE_ID Varchar(50),"
            "HISTORY_SEQ numeric(10,0),"
            "TX_DTTM Varchar(14),"
            "COLLECTION_ID Varchar(50)) \n";

    QString first_str ="";
    if(select_A1 == "ADI"){
        QString select_GM;
        if(select_GM1=="G"){
            select_GM =QString(" A.COLLECTION_ID LIKE '_G%'");
        }else if(select_GM1=="M"){
            select_GM=QString(" A.COLLECTION_ID NOT LIKE '_G%'");
        }
        first_str = QString("insert into @CASSETTE_ID_LIST select TOP %1 A.LOT_ID,B.CASSETTE_ID AS CASSETTE_ID,MAX(A.HISTORY_SEQ) as HISTORY_SEQ,MAX(A.TX_DTTM) AS TX_DTTM,MAX(A.COLLECTION_ID) as COLLECTION_ID from  V_NM_EDC_LOTS A,V_NM_LOT_EDC B  with (NOLOCK) "
                                    "where A.TX_DTTM <= '%2' AND "
                                          "A.OPERATION_ID = 'OP0801' AND "
                                          "A.CHARACTER_ID = 'LINE' AND "
                                          "A.COLLECTION_ID = '%3' "
                                          "AND A.LOT_ID = B.LOT_ID "
                                          "AND B.LOT_TYPE IN ('A','B') "
                                          "AND A.SITE_ID = 'WOSF' "
                                          "AND %4 "
                                          "group by B.CASSETTE_ID,A.LOT_ID,A.TX_DTTM "
                                          "order by A.TX_DTTM desc \n ").arg(history_count.toInt()*24).arg(select_time).arg(select_collectionid).arg(select_GM);

    }else if(select_A1 == "ACI"){
        QString select_GM;
        if(select_GM1=="G"){
            select_GM =QString(" A.COLLECTION_ID LIKE '_G%'");
        }else if(select_GM1=="M"){
            select_GM=QString(" A.COLLECTION_ID NOT LIKE '_G%'");
        }
        first_str = QString("insert into @CASSETTE_ID_LIST select TOP %1 A.LOT_ID,B.CASSETTE_ID AS CASSETTE_ID,MAX(A.HISTORY_SEQ) as HISTORY_SEQ,MAX(A.TX_DTTM) AS TX_DTTM,MAX(A.COLLECTION_ID) as COLLECTION_ID from  V_NM_EDC_LOTS A,V_NM_LOT_EDC B  with (NOLOCK) "
                            "where A.TX_DTTM <= '%2' AND "
                                  "A.OPERATION_ID = 'OP0901' AND "
                                  "A.CHARACTER_ID = 'LINE' AND "
                                  "A.COLLECTION_ID = '%3' "
                                  "AND A.LOT_ID = B.LOT_ID "
                                  "AND B.LOT_TYPE IN ('A','B') "
                                  "AND A.SITE_ID = 'WOSF' "
                                  "AND %4 "
                                  "group by A.LOT_ID,B.CASSETTE_ID,A.TX_DTTM "
                                  "order by A.TX_DTTM desc \n ").arg(history_count.toInt()*24).arg(select_time).arg(select_collectionid).arg(select_GM);;
    }
//    first_query.prepare(first_str);
//    first_query.exec();


//    QVector<QString> LOT_ID_LIST;
//    QString LOT_ID_LIST_str;
//    while(first_query.next()){
//        LOT_ID_LIST.append(first_query.value("LOT_ID").toString());
//        LOT_ID_LIST_str.append(QString("'%1',").arg(first_query.value("LOT_ID").toString()));
//    }
//    LOT_ID_LIST_str = LOT_ID_LIST_str.remove(LOT_ID_LIST_str.length()-1,1);



    QString var_prcd = QString("Declare @PRCD Table ( \n"
                                   "CASSETTE_ID Varchar(50) NOT NULL,\n"
                                   "HISTORY_SEQ numeric(10,0) NOT NULL,\n"
                                   "COLLECTION_ID Varchar(50) NOT NULL,\n"
                                   "COLLECTION_VERSION numeric(5,0) DEFAULT NULL,\n"
                                   "CHARACTER_ID Varchar(50) NOT NULL,\n"
                                   "TX_DTTM Varchar(14) DEFAULT NULL,\n"
                                   "VALUE1 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE2 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE3 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE4 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE5 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE6 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE7 Varchar(100) DEFAULT NULL,\n"
                                   "AVGS real DEFAULT(0.0),\n"
                                   "TARGET_VALUE Varchar(200) DEFAULT NULL,\n"
                                   "UPPER_SPEC_LIMIT Varchar(100) DEFAULT NULL,\n"
                                   "LOWER_SPEC_LIMIT Varchar(100) DEFAULT NULL) \n "
                               " \n "
                              );


    QString into_prcd = QString("insert into @PRCD select C.CASSETTE_ID,MAX(D.HISTORY_SEQ) AS HISTORY_SEQ ,MAX(D.COLLECTION_ID) AS COLLECTION_ID,MAX(D.COLLECTION_VERSION) AS COLLECTION_VERSION ,MAX(D.CHARACTER_ID) AS CHARACTER_ID, "
                                "MAX(D.TX_DTTM) AS TX_DTTM,MAX(D.VALUE1) AS VALUE1,MAX(D.VALUE2) AS VALUE2,MAX(D.VALUE3) AS VALUE3,MAX(D.VALUE4) AS VALUE4,MAX(D.VALUE5) AS VALUE5,MAX(D.VALUE6) AS VALUE6,MAX(D.VALUE7) AS VALUE7, "
                                "MAX((CAST(D.VALUE1 AS float)+CAST(D.VALUE2 AS float)+CAST(D.VALUE3 AS float)+ "
                                "CAST(D.VALUE4 AS float)+CAST(D.VALUE5 AS float)+CAST(D.VALUE6 AS float)+CAST(D.VALUE7 AS float))/D.VALUE_COUNT) as AVGS , "
                                "MAX(E.TARGET_VALUE) AS TARGET_VALUE,MAX(E.UPPER_SPEC_LIMIT) AS UPPER_SPEC_LIMIT, MAX(E.LOWER_SPEC_LIMIT)  LOWER_SPEC_LIMIT "
                                "from  @CASSETTE_ID_LIST C , V_NM_EDC_LOTS D,V_NM_COLLECTION_CHARACTERS E with (NOLOCK) "
                                      "where C.LOT_ID = D.LOT_ID AND D.OPERATION_ID = 'OP0801' AND "
                                      "D.COLLECTION_ID = E.COLLECTION_ID AND "
                                      "D.COLLECTION_VERSION = E.COLLECTION_VERSION AND D.CHARACTER_ID = E.CHARACTER_ID "
                                      "AND D.CHARACTER_ID = 'LINE' group by C.CASSETTE_ID order by C.CASSETTE_ID asc \n ");


    QString var_METAL_cd = QString("Declare @METAL_CD Table ( \n"
                                   "CASSETTE_ID Varchar(50) NOT NULL, \n"
                                   "HISTORY_SEQ numeric(10,0) NOT NULL,\n"
                                   "COLLECTION_ID Varchar(50) NOT NULL,\n"
                                   "COLLECTION_VERSION numeric(5,0) DEFAULT NULL,\n"
                                   "CHARACTER_ID Varchar(50) NOT NULL,\n"
                                   "TX_DTTM Varchar(14) DEFAULT NULL,\n"
                                   "VALUE1 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE2 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE3 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE4 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE5 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE6 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE7 Varchar(100) DEFAULT NULL,\n"
                                   "AVGS real DEFAULT(0.0),\n"
                                   "TARGET_VALUE Varchar(200) DEFAULT NULL,\n"
                                   "UPPER_SPEC_LIMIT Varchar(100) DEFAULT NULL,\n"
                                   "LOWER_SPEC_LIMIT Varchar(100) DEFAULT NULL)\n "
                                   " \n "
                                   );

    QString into_METAL_cd = QString("insert into @METAL_CD select C.CASSETTE_ID,MAX(D.HISTORY_SEQ) AS HISTORY_SEQ ,MAX(D.COLLECTION_ID) AS COLLECTION_ID,MAX(D.COLLECTION_VERSION) AS COLLECTION_VERSION ,MAX(D.CHARACTER_ID) AS CHARACTER_ID, "
                                    "MAX(D.TX_DTTM) AS TX_DTTM,MAX(D.VALUE1) AS VALUE1,MAX(D.VALUE2) AS VALUE2,MAX(D.VALUE3) AS VALUE3,MAX(D.VALUE4) AS VALUE4,MAX(D.VALUE5) AS VALUE5,MAX(D.VALUE6) AS VALUE6,MAX(D.VALUE7) AS VALUE7, "
                                    "MAX((CAST(D.VALUE1 AS float)+CAST(D.VALUE2 AS float)+CAST(D.VALUE3 AS float)+ "
                                    "CAST(D.VALUE4 AS float)+CAST(D.VALUE5 AS float)+CAST(D.VALUE6 AS float)+CAST(D.VALUE7 AS float))/D.VALUE_COUNT) as AVGS , "
                                    "MAX(E.TARGET_VALUE) AS TARGET_VALUE,MAX(E.UPPER_SPEC_LIMIT) AS UPPER_SPEC_LIMIT, MAX(E.LOWER_SPEC_LIMIT)  LOWER_SPEC_LIMIT "
                                    "from  @CASSETTE_ID_LIST C , V_NM_EDC_LOTS D,V_NM_COLLECTION_CHARACTERS E with (NOLOCK) "
                                          "where C.LOT_ID = D.LOT_ID AND D.OPERATION_ID = 'OP0901' AND "
                                          "D.COLLECTION_ID = E.COLLECTION_ID AND "
                                          "D.COLLECTION_VERSION = E.COLLECTION_VERSION AND D.CHARACTER_ID = E.CHARACTER_ID "
                                          "AND D.CHARACTER_ID = 'LINE' group by C.CASSETTE_ID order by C.CASSETTE_ID asc \n ");

    QString select_A;

    select_A = "@PRCD A  LEFT JOIN @METAL_CD B  ON A.CASSETTE_ID=B.CASSETTE_ID order by A.CASSETTE_ID asc";
    if(select_A1 == "ADI"){
        QString select_GM;
        if(select_GM1=="G"){
            select_GM =QString(" A.COLLECTION_ID LIKE '_G%'");
        }else if(select_GM1=="M"){
            select_GM=QString(" A.COLLECTION_ID NOT LIKE '_G%'");
        }
        select_A = QString("@PRCD A  LEFT JOIN @METAL_CD B  ON A.CASSETTE_ID=B.CASSETTE_ID where %1 order by A.TX_DTTM asc")
                .arg(select_GM);
    }else if(select_A1 == "ACI"){
        QString select_GM;
        if(select_GM1=="G"){
            select_GM =QString(" B.COLLECTION_ID LIKE '_G%'");
        }else if(select_GM1=="M"){
            select_GM=QString(" B.COLLECTION_ID NOT LIKE '_G%'");
        }
        select_A = QString("@PRCD A  RIGHT JOIN @METAL_CD B  ON B.CASSETTE_ID=A.CASSETTE_ID where %1 order by B.TX_DTTM asc")
                .arg(select_GM);
    }

    QString select_query = QString("select A.CASSETTE_ID as PR_LOT_ID,A.HISTORY_SEQ as PR_HISTORY_SEQ,A.COLLECTION_ID as PR_COLLECTION_ID, \n"
                                   "A.COLLECTION_VERSION as PR_COLLECTION_VERSION,A.CHARACTER_ID as PR_CHARACTER_ID,A.TX_DTTM as PR_TM_DTTM, \n"
                                   "A.VALUE1 AS PR_VALUE1,A.VALUE2 AS PR_VALUE2,A.VALUE3 AS PR_VALUE3,A.VALUE4 AS PR_VALUE4, \n"
                                   "A.VALUE5 AS PR_VALUE5,A.VALUE6 AS PR_VALUE6,A.VALUE7 AS PR_VALUE7,ROUND(A.AVGS,2) AS PR_AVGS, \n"
                                   "A.TARGET_VALUE AS PR_TARGET,A.UPPER_SPEC_LIMIT AS PR_USL,A.LOWER_SPEC_LIMIT AS PR_LSL, \n"
                                   "ROUND(A.AVGS - (0+A.TARGET_VALUE),2) as PR_diff, \n"
                                   "B.CASSETTE_ID as METAL_LOT_ID,B.HISTORY_SEQ as METAL_HISTORY_SEQ,B.COLLECTION_ID as METAL_COLLECTION_ID, \n"
                                   "B.COLLECTION_VERSION as METAL_COLLECTION_VERSION,B.CHARACTER_ID as METAL_CHARACTER_ID,B.TX_DTTM as METAL_TM_DTTM, \n"
                                   "B.VALUE1 AS METAL_VALUE1,B.VALUE2 AS METAL_VALUE2,B.VALUE3 AS METAL_VALUE3,B.VALUE4 AS METAL_VALUE4, \n"
                                   "B.VALUE5 AS METAL_VALUE5,B.VALUE6 AS METAL_VALUE6,B.VALUE7 AS METAL_VALUE7,ROUND(B.AVGS,2) AS METAL_AVGS, \n"
                                   "B.TARGET_VALUE AS METAL_TARGET,B.UPPER_SPEC_LIMIT AS METAL_USL,B.LOWER_SPEC_LIMIT AS METAL_LSL,\n"
                                   "ROUND(B.AVGS - (0+B.TARGET_VALUE),2) as METAL_diff \n"
                            "from %1 \n ").arg(select_A);

    QSqlQuery sqlquery(ms_mesdb);
    sqlquery.prepare(CASETTE_ID_LIST_txt+first_str+var_prcd+ into_prcd +var_METAL_cd+into_METAL_cd+select_query);

    sqlquery.exec();
    qDebug()<<sqlquery.lastQuery();
    int row_count = 0;
    while(sqlquery.next()){
        QList<QStandardItem *> Row_data;

        for(int i=0;i<sqlquery.record().count();i++){
            if(row_count == 0){
                main_table_model->setHorizontalHeaderItem(i,new QStandardItem(sqlquery.record().fieldName(i)));
            }
            Row_data.append(new QStandardItem(sqlquery.record().value(i).toString()));
        }
        main_table_model->appendRow(Row_data);
        row_count++;
    }
    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("PR_HISTORY_SEQ"));
//    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("PR_COLLECTION_ID"));
    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("PR_COLLECTION_VERSION"));
    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("PR_CHARACTER_ID"));
    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("METAL_HISTORY_SEQ"));
//    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("METAL_COLLECTION_ID"));
    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("METAL_COLLECTION_VERSION"));
    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("METAL_CHARACTER_ID"));
    ui->main_table_view->horizontalHeader()->resizeSection(sqlquery.record().indexOf("PR_TM_DTTM"),150);
    ui->main_table_view->horizontalHeader()->resizeSection(sqlquery.record().indexOf("PR_COLLECTION_ID"),150);
    ui->main_table_view->horizontalHeader()->resizeSection(sqlquery.record().indexOf("METAL_TM_DTTM"),150);
    ui->main_table_view->horizontalHeader()->resizeSection(sqlquery.record().indexOf("METAL_COLLECTION_ID"),150);

    main_table_model->horizontalHeaderItem(sqlquery.record().indexOf("PR_AVGS"))->setText(tr("PR_AVGS"));
    main_table_model->horizontalHeaderItem(sqlquery.record().indexOf("PR_TM_DTTM"))->setText(tr("PR_TM_DTTM"));
    main_table_model->horizontalHeaderItem(sqlquery.record().indexOf("PR_diff"))->setText(tr("PR_diff"));
    main_table_model->horizontalHeaderItem(sqlquery.record().indexOf("METAL_AVGS"))->setText(tr("METAL_AVGS"));
    main_table_model->horizontalHeaderItem(sqlquery.record().indexOf("METAL_TM_DTTM"))->setText(tr("METAL_TM_DTTM"));
    main_table_model->horizontalHeaderItem(sqlquery.record().indexOf("METAL_diff"))->setText(tr("METAL_diff"));

    value_colum_list.append(sqlquery.record().indexOf("PR_VALUE1"));
    value_colum_list.append(sqlquery.record().indexOf("PR_VALUE2"));
    value_colum_list.append(sqlquery.record().indexOf("PR_VALUE3"));
    value_colum_list.append(sqlquery.record().indexOf("PR_VALUE4"));
    value_colum_list.append(sqlquery.record().indexOf("PR_VALUE5"));
    value_colum_list.append(sqlquery.record().indexOf("PR_VALUE6"));
    value_colum_list.append(sqlquery.record().indexOf("PR_VALUE7"));

    value_colum_list.append(sqlquery.record().indexOf("METAL_VALUE1"));
    value_colum_list.append(sqlquery.record().indexOf("METAL_VALUE2"));
    value_colum_list.append(sqlquery.record().indexOf("METAL_VALUE3"));
    value_colum_list.append(sqlquery.record().indexOf("METAL_VALUE4"));
    value_colum_list.append(sqlquery.record().indexOf("METAL_VALUE5"));
    value_colum_list.append(sqlquery.record().indexOf("METAL_VALUE6"));
    value_colum_list.append(sqlquery.record().indexOf("METAL_VALUE7"));

    ui->VALUE_view->setChecked(true);
    for(int i=0;i<value_colum_list.count();i++){
        ui->main_table_view->horizontalHeader()->setSectionHidden(value_colum_list.at(i),true);
    }

    ADI_chart = new cdsem_chart();
    ADI_chart->setTitle("PR ADI");


    QFont ADI_font = ADI_chart->font();
    ADI_font.setBold(true);
    ADI_font.setPointSize(15);
    ADI_chart->setTitleFont(ADI_font);


    ADI_avg_line = new QLineSeries();
    ADI_usl_line = new QLineSeries();
    ADI_lsl_line = new QLineSeries();

    QList<QVariant> taglist_ADI;
    double yrange_min=0;
    double yrange_max=0;


    for(int i=0; i<main_table_model->rowCount();i++){

        QString PR_TM_DTTM = main_table_model->index(i,sqlquery.record().indexOf("PR_TM_DTTM")).data().toString();
        double avg_value = main_table_model->index(i,sqlquery.record().indexOf("PR_AVGS")).data().toDouble();
        double usl_value = main_table_model->index(i,sqlquery.record().indexOf("PR_USL")).data().toDouble();
        double lsl_value = main_table_model->index(i,sqlquery.record().indexOf("PR_LSL")).data().toDouble();
        if(i==0){
            yrange_min = lsl_value;
            yrange_max = usl_value;
        }
        if(PR_TM_DTTM!=""){
            QDateTime PR_TM_Datetime = QDateTime::fromString(PR_TM_DTTM,"yyyyMMddhhmmss");
            ADI_avg_line->append(PR_TM_Datetime.toMSecsSinceEpoch(),avg_value);
            ADI_usl_line->append(PR_TM_Datetime.toMSecsSinceEpoch(),usl_value);
            ADI_lsl_line->append(PR_TM_Datetime.toMSecsSinceEpoch(),lsl_value);
            if(yrange_max <=avg_value){
                yrange_max = avg_value;
            }
            if(yrange_max <=usl_value){
                yrange_max = usl_value;
            }
            if(yrange_max <=lsl_value){
                yrange_max = lsl_value;
            }
            if(yrange_min >=avg_value){
                yrange_min = avg_value;
            }
            if(yrange_min >=usl_value){
                yrange_min = usl_value;
            }
            if(yrange_min >=lsl_value){
                yrange_min = lsl_value;
            }
            taglist_ADI.append(main_table_model->index(i,sqlquery.record().indexOf("PR_LOT_ID")).data());
        }
    }
    ADI_avg_line->setProperty("tag",taglist_ADI);


    ADI_chart->addSeries(ADI_usl_line);
    ADI_chart->addSeries(ADI_lsl_line);
    ADI_chart->addSeries(ADI_avg_line);

    QDateTimeAxis *axisX_ADI = new QDateTimeAxis;
//    axisX->setFormat("yyyy/MM/dd hh:mm:ss");
    axisX_ADI->setFormat("MM-dd HH:mm");
    axisX_ADI->setTitleText("Date");
    ADI_chart->addAxis(axisX_ADI, Qt::AlignBottom);


    QValueAxis *axisY_ADI = new QValueAxis;
    ADI_chart->addAxis(axisY_ADI, Qt::AlignLeft);

    ADI_usl_line->setPointsVisible(true);
    ADI_usl_line->setColor(QColor("red"));
    ADI_usl_line->attachAxis(axisX_ADI);
    ADI_usl_line->attachAxis(axisY_ADI);

    ADI_lsl_line->setPointsVisible(true);
    ADI_lsl_line->setColor(QColor("green"));
    ADI_lsl_line->attachAxis(axisX_ADI);
    ADI_lsl_line->attachAxis(axisY_ADI);

    ADI_avg_line->setPointsVisible(true);
    ADI_avg_line->setPointLabelsVisible(true);
    ADI_avg_line->setPointLabelsFormat("(@tag,@yPoint)");

    ADI_avg_line->attachAxis(axisX_ADI);
    ADI_avg_line->attachAxis(axisY_ADI);

    axisY_ADI->setRange(yrange_min,yrange_max);


    cdsem_chartview *ADI_chart_view = new cdsem_chartview(ADI_chart);
    ui->PR_chart_layout->addWidget(ADI_chart_view);
    //ACI
    ACI_chart = new cdsem_chart();
    ACI_chart->setTitle("METAL ACI");

    QFont ACI_font = ACI_chart->font();
    ACI_font.setBold(true);
    ACI_font.setPointSize(15);
    ACI_chart->setTitleFont(ACI_font);


    ACI_avg_line = new QLineSeries();
    ACI_usl_line = new QLineSeries();
    ACI_lsl_line = new QLineSeries();
    QList<QVariant> taglist_ACI;
    yrange_min=0;
    yrange_max=0;

    for(int i=0; i<main_table_model->rowCount();i++){

        QString METAL_TM_DTTM = main_table_model->index(i,sqlquery.record().indexOf("METAL_TM_DTTM")).data().toString();
        double avg_value = main_table_model->index(i,sqlquery.record().indexOf("METAL_AVGS")).data().toDouble();
        double usl_value = main_table_model->index(i,sqlquery.record().indexOf("METAL_USL")).data().toDouble();
        double lsl_value = main_table_model->index(i,sqlquery.record().indexOf("METAL_LSL")).data().toDouble();
        if(i==0){
            yrange_min = lsl_value;
            yrange_max = usl_value;
        }
        if(METAL_TM_DTTM!=""){
            QDateTime METAL_TM_Datetime = QDateTime::fromString(METAL_TM_DTTM,"yyyyMMddhhmmss");
            ACI_avg_line->append(METAL_TM_Datetime.toMSecsSinceEpoch(),avg_value);
            ACI_usl_line->append(METAL_TM_Datetime.toMSecsSinceEpoch(),usl_value);
            ACI_lsl_line->append(METAL_TM_Datetime.toMSecsSinceEpoch(),lsl_value);
            if(yrange_max <=avg_value){
                yrange_max = avg_value;
            }
            if(yrange_max <=usl_value){
                yrange_max = usl_value;
            }
            if(yrange_max <=lsl_value){
                yrange_max = lsl_value;
            }
            if(yrange_min >=avg_value){
                yrange_min = avg_value;
            }
            if(yrange_min >=usl_value){
                yrange_min = usl_value;
            }
            if(yrange_min >=lsl_value){
                yrange_min = lsl_value;
            }
            taglist_ACI.append(main_table_model->index(i,sqlquery.record().indexOf("METAL_LOT_ID")).data());
        }
    }
    ACI_avg_line->setProperty("tag",taglist_ACI);

    ACI_chart->addSeries(ACI_usl_line);
    ACI_chart->addSeries(ACI_lsl_line);
    ACI_chart->addSeries(ACI_avg_line);


    QDateTimeAxis *axisX_ACI = new QDateTimeAxis;
//    axisX->setFormat("yyyy/MM/dd hh:mm:ss");
    axisX_ACI->setFormat("MM-dd HH:mm");
    axisX_ACI->setTitleText("Date");
    ACI_chart->addAxis(axisX_ACI, Qt::AlignBottom);


    QValueAxis *axisY_ACI = new QValueAxis;
    ACI_chart->addAxis(axisY_ACI, Qt::AlignLeft);

    ACI_usl_line->setPointsVisible(true);
    ACI_usl_line->setColor(QColor("red"));
    ACI_usl_line->attachAxis(axisX_ACI);
    ACI_usl_line->attachAxis(axisY_ACI);

    ACI_lsl_line->setPointsVisible(true);
    ACI_lsl_line->setColor(QColor("green"));
    ACI_lsl_line->attachAxis(axisX_ACI);
    ACI_lsl_line->attachAxis(axisY_ACI);

    ACI_avg_line->setPointsVisible(true);
    ACI_avg_line->setPointLabelsVisible(true);
    ACI_avg_line->setPointLabelsFormat("(@tag,@yPoint)");

    ACI_avg_line->attachAxis(axisX_ACI);
    ACI_avg_line->attachAxis(axisY_ACI);

    axisY_ACI->setRange(yrange_min,yrange_max);

    cdsem_chartview *ACI_chart_view = new cdsem_chartview(ACI_chart);
    ui->Metal_chart_layout->addWidget(ACI_chart_view);


}

cdsem_chart_widget::~cdsem_chart_widget()
{
    delete ui;
}

void cdsem_chart_widget::on_VALUE_view_toggled(bool checked)
{
    if(checked){
        for(int i=0;i<value_colum_list.count();i++){
            ui->main_table_view->horizontalHeader()->setSectionHidden(value_colum_list.at(i),true);
        }
    }else {
        for(int i=0;i<value_colum_list.count();i++){
            ui->main_table_view->horizontalHeader()->setSectionHidden(value_colum_list.at(i),false);
        }
    }
}

void cdsem_chart_widget::on_PR_zoom_reset_clicked()
{
    ADI_chart->zoomReset();

}

void cdsem_chart_widget::on_PR_point_label_clicked()
{
    if(ADI_avg_line->pointLabelsVisible()){
        ADI_avg_line->setPointLabelsVisible(false);
    }else {
        ADI_avg_line->setPointLabelsVisible(true);
    }
}

void cdsem_chart_widget::on_Metal_zoom_reset_clicked()
{
      ACI_chart->zoomReset();
}

void cdsem_chart_widget::on_Metal_point_label_clicked()
{
    if(ACI_avg_line->pointLabelsVisible()){
        ACI_avg_line->setPointLabelsVisible(false);
    }else {
        ACI_avg_line->setPointLabelsVisible(true);
    }
}
