#ifndef CD_SEM_MTABLE_VIEW_H
#define CD_SEM_MTABLE_VIEW_H

#include <QObject>
#include <QWidget>
#include <QTableView>
#include <QKeyEvent>
#include <QApplication>
#include <QClipboard>
class cd_sem_mtable_view : public QTableView
{
    Q_OBJECT
public:
    cd_sem_mtable_view(QWidget *parent = Q_NULLPTR);
private:
    void keyPressEvent(QKeyEvent *event);
};

#endif // CD_SEM_MTABLE_VIEW_H
