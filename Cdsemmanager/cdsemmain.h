#ifndef CDSEMMAIN_H
#define CDSEMMAIN_H

#include <QWidget>
#include <QSqlQuery>
#include <QSqlDatabase>
#include <QDateTime>
#include <QSettings>
#include <QSqlQueryModel>
#include <Cdsemmanager/cd_sem_mtable_view.h>
#include <QSqlError>
#include <QDebug>
#include <QStandardItemModel>
#include <QSqlRecord>
#include <QMessageBox>
#include <Cdsemmanager/cdsem_chart_widget.h>
#include <global_define.h>
namespace Ui {
class Cdsemmain;
}

class Cdsemmain : public QWidget
{
    Q_OBJECT

public:
    explicit Cdsemmain(QWidget *parent = 0);
    QSqlDatabase ms_mesdb;
    QStandardItemModel *main_table_model;
    QVector<int> value_colum_list;
    int PR_TX_DTTM_index;
    int Metal_TX_DTTM_index;
    int PR_COLLECTION_ID;
    int METAL_COLLECTION_ID;
    ~Cdsemmain();

private slots:
    void closeEvent(QCloseEvent *event);
    void on_search_btn_clicked();

    void on_VALUE_view_clicked(bool checked);

    void on_main_table_view_doubleClicked(const QModelIndex &index);

private:
    Ui::Cdsemmain *ui;
};

#endif // CDSEMMAIN_H
