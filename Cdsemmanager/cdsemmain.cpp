#include "cdsemmain.h"
#include "ui_cdsemmain.h"

Cdsemmain::Cdsemmain(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Cdsemmain)
{
    ui->setupUi(this);
    QString msdb_name = QString("MS_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    ms_mesdb = QSqlDatabase::addDatabase("QODBC",msdb_name);
    QString serverinfo = QString("DRIVER={SQL Server};Server=%1;Database=%2;Uid=%3;Port=1433;Pwd=%4").arg(DBMESSERVERIP).arg(DBMESNAME).arg(DBMESUSERNAME).arg(DBMESPW);

    ms_mesdb.setDatabaseName(serverinfo);
    if(!ms_mesdb.open()){
        qDebug()<<"fasle";
        qDebug()<<ms_mesdb.lastError().text();
    }else {
        qDebug()<<"open";
    }
    ui->LE_USL->setValidator(new QDoubleValidator(-99999.9,99999.9,2));
    ui->LE_LSL->setValidator(new QDoubleValidator(-99999.9,99999.9,2));
    main_table_model = new QStandardItemModel();
    ui->main_table_view->setModel(main_table_model);
    ui->Startdate->setDateTime(QDateTime::currentDateTime().addDays(-7));
    ui->Startdate->setTime(QTime(7,59,59));
    ui->Enddate->setDateTime(QDateTime::currentDateTime());
    ui->Enddate->setTime(QTime(8,00,00));
    ui->LE_USL->setText("80");
    ui->LE_LSL->setText("80");
    PR_TX_DTTM_index = 0;
    Metal_TX_DTTM_index = 0;
    ui->History_count->setValidator(new QIntValidator(0,9999));
    ui->History_count->setText("10");
}

Cdsemmain::~Cdsemmain()
{
    delete ui;
}

void Cdsemmain::closeEvent(QCloseEvent *event)
{

}

void Cdsemmain::on_search_btn_clicked()
{
    value_colum_list.clear();
    main_table_model->clear();
    QString startdate = ui->Startdate->dateTime().toString("yyyyMMddhhmmss");
    QString enddate = ui->Enddate->dateTime().toString("yyyyMMddhhmmss");

//    QSqlQuery first_query(ms_mesdb);
    QString CASSETTE_ID_LIST_str = "Declare @CASSETTE_ID_LIST Table ("
            "LOT_ID varchar(50), "
            "CASSETTE_ID varchar(50), "
            "HISTORY_SEQ numeric(10,0)) \n" ;
    QString first_str ="";
    if(ui->RB_ADI->isChecked()){
        first_str = QString("insert into @CASSETTE_ID_LIST select A.LOT_ID ,C.CASSETTE_ID,MAX(A.HISTORY_SEQ) as HISTORY_SEQ from V_NM_EDC_LOTS A ,V_NM_LOTS B,V_NM_LOT_EDC C   with (NOLOCK) "
                            " where A.TX_DTTM between '%1' AND '%2'  AND "
                                   "A.OPERATION_ID = 'OP0801' AND "
                                   "A.CHARACTER_ID = 'LINE' "
                                   "AND A.LOT_ID = B.LOT_ID "
                                   "AND B.LOT_TYPE IN ('A','B') "
                                   "AND A.SITE_ID = 'WOSF' "
                                   "AND A.LOT_ID = B.LOT_ID AND A.HISTORY_SEQ = C.HISTORY_SEQ "
                                   "AND A.COLLECTION_ID = C.COLLECTION_ID "
                                   "AND A.COLLECTION_VERSION = C.COLLECTION_VERSION "
                                   "group by A.LOT_ID,C.CASSETTE_ID "
                                   "order by A.LOT_ID asc \n").arg(startdate).arg(enddate);

    }else if(ui->RB_ACI->isChecked()){
        first_str = QString("insert into @CASSETTE_ID_LIST select A.LOT_ID ,C.CASSETTE_ID,MAX(A.HISTORY_SEQ) as HISTORY_SEQ from V_NM_EDC_LOTS A ,V_NM_LOTS B,V_NM_LOT_EDC C   with (NOLOCK) "
                             "where A.TX_DTTM between '%1' AND '%2'  AND "
                                   "A.OPERATION_ID = 'OP0901' AND "
                                   "A.CHARACTER_ID = 'LINE' "
                                   "AND A.LOT_ID = B.LOT_ID "
                                   "AND B.LOT_TYPE IN ('A','B') "
                                   "AND A.SITE_ID = 'WOSF' "
                                   "AND A.LOT_ID = B.LOT_ID AND A.HISTORY_SEQ = C.HISTORY_SEQ "
                                   "AND A.COLLECTION_ID = C.COLLECTION_ID "
                                   "AND A.COLLECTION_VERSION = C.COLLECTION_VERSION "
                                   "group by A.LOT_ID,C.CASSETTE_ID "
                                   "order by A.LOT_ID asc \n").arg(startdate).arg(enddate);
    }
//    first_query.prepare(first_str);
//    first_query.exec();

//    QVector<QString> LOT_ID_LIST;
//    QString LOT_ID_LIST_str;
//    while(first_query.next()){
//        LOT_ID_LIST.append(first_query.value("LOT_ID").toString());
//        LOT_ID_LIST_str.append(QString("'%1',").arg(first_query.value("LOT_ID").toString()));
//    }
//    LOT_ID_LIST_str = LOT_ID_LIST_str.remove(LOT_ID_LIST_str.length()-1,1);



    QString var_prcd = QString("Declare @PRCD Table ( \n"
                                   "CASSETTE_ID Varchar(50) NOT NULL,\n"
                                   "HISTORY_SEQ numeric(10,0) NOT NULL,\n"
                                   "COLLECTION_ID Varchar(50) NOT NULL,\n"
                                   "COLLECTION_VERSION numeric(5,0) DEFAULT NULL,\n"
                                   "CHARACTER_ID Varchar(50) NOT NULL,\n"
                                   "TX_DTTM Varchar(14) DEFAULT NULL,\n"
                                   "VALUE1 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE2 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE3 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE4 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE5 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE6 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE7 Varchar(100) DEFAULT NULL,\n"
                                   "AVGS real DEFAULT(0.0),\n"
                                   "TARGET_VALUE Varchar(200) DEFAULT NULL,\n"
                                   "UPPER_SPEC_LIMIT Varchar(100) DEFAULT NULL,\n"
                                   "LOWER_SPEC_LIMIT Varchar(100) DEFAULT NULL) \n"
                               " \n"
                              );


    QString into_prcd = QString("insert into @PRCD select C.CASSETTE_ID,MAX(D.HISTORY_SEQ) AS HISTORY_SEQ ,MAX(D.COLLECTION_ID) AS COLLECTION_ID,MAX(D.COLLECTION_VERSION) AS COLLECTION_VERSION ,MAX(D.CHARACTER_ID) AS CHARACTER_ID, "
                                "MAX(D.TX_DTTM) AS TX_DTTM,MAX(D.VALUE1) AS VALUE1,MAX(D.VALUE2) AS VALUE2,MAX(D.VALUE3) AS VALUE3,MAX(D.VALUE4) AS VALUE4,MAX(D.VALUE5) AS VALUE5,MAX(D.VALUE6) AS VALUE6,MAX(D.VALUE7) AS VALUE7, "
                                "MAX((CAST(D.VALUE1 AS float)+CAST(D.VALUE2 AS float)+CAST(D.VALUE3 AS float)+ "
                                "CAST(D.VALUE4 AS float)+CAST(D.VALUE5 AS float)+CAST(D.VALUE6 AS float)+CAST(D.VALUE7 AS float))/D.VALUE_COUNT) as AVGS , "
                                "MAX(E.TARGET_VALUE) AS TARGET_VALUE,MAX(E.UPPER_SPEC_LIMIT) AS UPPER_SPEC_LIMIT, MAX(E.LOWER_SPEC_LIMIT)  LOWER_SPEC_LIMIT "
                                "from  @CASSETTE_ID_LIST C , V_NM_EDC_LOTS D,V_NM_COLLECTION_CHARACTERS E with (NOLOCK) "
                                      "where C.LOT_ID = D.LOT_ID AND D.OPERATION_ID = 'OP0801' AND "
                                      "D.COLLECTION_ID = E.COLLECTION_ID AND "
                                      "D.COLLECTION_VERSION = E.COLLECTION_VERSION AND D.CHARACTER_ID = E.CHARACTER_ID "
                                      "AND D.CHARACTER_ID = 'LINE' group by C.CASSETTE_ID order by C.CASSETTE_ID asc \n");


    QString var_METAL_cd = QString("Declare @METAL_CD Table ( \n"
                                   "CASSETTE_ID Varchar(50) NOT NULL, \n"
                                   "HISTORY_SEQ numeric(10,0) NOT NULL,\n"
                                   "COLLECTION_ID Varchar(50) NOT NULL,\n"
                                   "COLLECTION_VERSION numeric(5,0) DEFAULT NULL,\n"
                                   "CHARACTER_ID Varchar(50) NOT NULL,\n"
                                   "TX_DTTM Varchar(14) DEFAULT NULL,\n"
                                   "VALUE1 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE2 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE3 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE4 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE5 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE6 Varchar(100) DEFAULT NULL,\n"
                                   "VALUE7 Varchar(100) DEFAULT NULL,\n"
                                   "AVGS real DEFAULT(0.0),\n"
                                   "TARGET_VALUE Varchar(200) DEFAULT NULL,\n"
                                   "UPPER_SPEC_LIMIT Varchar(100) DEFAULT NULL,\n"
                                   "LOWER_SPEC_LIMIT Varchar(100) DEFAULT NULL)\n"
                                   " \n"
                                   );

    QString into_METAL_cd = QString("insert into @METAL_CD select C.CASSETTE_ID,MAX(D.HISTORY_SEQ) AS HISTORY_SEQ ,MAX(D.COLLECTION_ID) AS COLLECTION_ID,MAX(D.COLLECTION_VERSION) AS COLLECTION_VERSION ,MAX(D.CHARACTER_ID) AS CHARACTER_ID, "
                                    "MAX(D.TX_DTTM) AS TX_DTTM,MAX(D.VALUE1) AS VALUE1,MAX(D.VALUE2) AS VALUE2,MAX(D.VALUE3) AS VALUE3,MAX(D.VALUE4) AS VALUE4,MAX(D.VALUE5) AS VALUE5,MAX(D.VALUE6) AS VALUE6,MAX(D.VALUE7) AS VALUE7, "
                                    "MAX((CAST(D.VALUE1 AS float)+CAST(D.VALUE2 AS float)+CAST(D.VALUE3 AS float)+ "
                                    "CAST(D.VALUE4 AS float)+CAST(D.VALUE5 AS float)+CAST(D.VALUE6 AS float)+CAST(D.VALUE7 AS float))/D.VALUE_COUNT) as AVGS , "
                                    "MAX(E.TARGET_VALUE) AS TARGET_VALUE,MAX(E.UPPER_SPEC_LIMIT) AS UPPER_SPEC_LIMIT, MAX(E.LOWER_SPEC_LIMIT)  LOWER_SPEC_LIMIT "
                                    "from  @CASSETTE_ID_LIST C , V_NM_EDC_LOTS D,V_NM_COLLECTION_CHARACTERS E with (NOLOCK) "
                                          "where C.LOT_ID = D.LOT_ID AND D.OPERATION_ID = 'OP0901' AND "
                                          "D.COLLECTION_ID = E.COLLECTION_ID AND "
                                          "D.COLLECTION_VERSION = E.COLLECTION_VERSION AND D.CHARACTER_ID = E.CHARACTER_ID "
                                          "AND D.CHARACTER_ID = 'LINE' group by C.CASSETTE_ID order by C.CASSETTE_ID asc \n");

    QString select_A;

    select_A = "@PRCD A  LEFT JOIN @METAL_CD B  ON A.CASSETTE_ID=B.CASSETTE_ID order by A.CASSETTE_ID asc";
    if(ui->RB_ADI->isChecked()){
        QString select_GM;
        if(ui->RB_G->isChecked()){
            select_GM =QString("AND A.COLLECTION_ID LIKE '_G%'");
        }else if(ui->RB_M->isChecked()){
            select_GM=QString("AND A.COLLECTION_ID NOT LIKE '_G%'");
        }
        select_A = QString("@PRCD A  LEFT JOIN @METAL_CD B  ON A.CASSETTE_ID=B.CASSETTE_ID where ((A.AVGS - (0+A.TARGET_VALUE)) >= %1 OR (A.AVGS - (0+A.TARGET_VALUE)) <= %2) %3 order by A.COLLECTION_ID asc")
                .arg(ui->LE_USL->text()).arg("-"+ui->LE_LSL->text()).arg(select_GM);
    }else if(ui->RB_ACI->isChecked()){
        QString select_GM;
        if(ui->RB_G->isChecked()){
            select_GM =QString("AND B.COLLECTION_ID LIKE '_G%'");
        }else if(ui->RB_M->isChecked()){
            select_GM=QString("AND B.COLLECTION_ID NOT LIKE '_G%'");
        }
        select_A = QString("@PRCD A  LEFT JOIN @METAL_CD B  ON A.CASSETTE_ID=B.CASSETTE_ID where ((B.AVGS - (0+B.TARGET_VALUE)) >= %1 OR (B.AVGS - (0+B.TARGET_VALUE)) <= %2) %3 order by A.COLLECTION_ID asc")
                .arg(ui->LE_USL->text()).arg("-"+ui->LE_LSL->text()).arg(select_GM);
    }
    QString select_query = QString("select A.CASSETTE_ID as PR_LOT_ID,A.HISTORY_SEQ as PR_HISTORY_SEQ,A.COLLECTION_ID as PR_COLLECTION_ID, \n"
                                   "A.COLLECTION_VERSION as PR_COLLECTION_VERSION,A.CHARACTER_ID as PR_CHARACTER_ID,A.TX_DTTM as PR_TM_DTTM, \n"
                                   "A.VALUE1 AS PR_VALUE1,A.VALUE2 AS PR_VALUE2,A.VALUE3 AS PR_VALUE3,A.VALUE4 AS PR_VALUE4, \n"
                                   "A.VALUE5 AS PR_VALUE5,A.VALUE6 AS PR_VALUE6,A.VALUE7 AS PR_VALUE7,ROUND(A.AVGS,2) AS PR_AVGS, \n"
                                   "A.TARGET_VALUE AS PR_TARGET,A.UPPER_SPEC_LIMIT AS PR_USL,A.LOWER_SPEC_LIMIT AS PR_LSL, \n"
                                   "ROUND(A.AVGS - (0+A.TARGET_VALUE),2) as PR_diff, \n"
                                   "B.CASSETTE_ID as METAL_LOT_ID,B.HISTORY_SEQ as METAL_HISTORY_SEQ,B.COLLECTION_ID as METAL_COLLECTION_ID, \n"
                                   "B.COLLECTION_VERSION as METAL_COLLECTION_VERSION,B.CHARACTER_ID as METAL_CHARACTER_ID,B.TX_DTTM as METAL_TM_DTTM, \n"
                                   "B.VALUE1 AS METAL_VALUE1,B.VALUE2 AS METAL_VALUE2,B.VALUE3 AS METAL_VALUE3,B.VALUE4 AS METAL_VALUE4, \n"
                                   "B.VALUE5 AS METAL_VALUE5,B.VALUE6 AS METAL_VALUE6,B.VALUE7 AS METAL_VALUE7,ROUND(B.AVGS,2) AS METAL_AVGS, \n"
                                   "B.TARGET_VALUE AS METAL_TARGET,B.UPPER_SPEC_LIMIT AS METAL_USL,B.LOWER_SPEC_LIMIT AS METAL_LSL,\n"
                                   "ROUND(B.AVGS - (0+B.TARGET_VALUE),2) as METAL_diff \n"
                            "from %1 \n").arg(select_A);

    QSqlQuery sqlquery(ms_mesdb);
    sqlquery.prepare(CASSETTE_ID_LIST_str+first_str+var_prcd+ into_prcd +var_METAL_cd+into_METAL_cd+select_query);
    QFile file("query.sql");
    file.open(QIODevice::WriteOnly);
    QTextStream stream(&file);
    stream<<CASSETTE_ID_LIST_str+first_str+var_prcd+ into_prcd +var_METAL_cd+into_METAL_cd+select_query;
    stream.flush();
    file.close();
    sqlquery.exec();
    int row_count = 0;
    while(sqlquery.next()){
        QList<QStandardItem *> Row_data;

        for(int i=0;i<sqlquery.record().count();i++){
            if(row_count == 0){
                main_table_model->setHorizontalHeaderItem(i,new QStandardItem(sqlquery.record().fieldName(i)));
            }
            Row_data.append(new QStandardItem(sqlquery.record().value(i).toString()));
        }
        main_table_model->appendRow(Row_data);
        row_count++;
    }
    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("PR_HISTORY_SEQ"));
//    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("PR_COLLECTION_ID"));
    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("PR_COLLECTION_VERSION"));
    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("PR_CHARACTER_ID"));
    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("METAL_HISTORY_SEQ"));
//    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("METAL_COLLECTION_ID"));
    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("METAL_COLLECTION_VERSION"));
    ui->main_table_view->horizontalHeader()->hideSection(sqlquery.record().indexOf("METAL_CHARACTER_ID"));
    ui->main_table_view->horizontalHeader()->resizeSection(sqlquery.record().indexOf("PR_TM_DTTM"),150);
    ui->main_table_view->horizontalHeader()->resizeSection(sqlquery.record().indexOf("PR_COLLECTION_ID"),150);
    ui->main_table_view->horizontalHeader()->resizeSection(sqlquery.record().indexOf("METAL_TM_DTTM"),150);
    ui->main_table_view->horizontalHeader()->resizeSection(sqlquery.record().indexOf("METAL_COLLECTION_ID"),150);

    main_table_model->horizontalHeaderItem(sqlquery.record().indexOf("PR_AVGS"))->setText(tr("PR_AVGS"));
    main_table_model->horizontalHeaderItem(sqlquery.record().indexOf("PR_TM_DTTM"))->setText(tr("PR_TM_DTTM"));
    main_table_model->horizontalHeaderItem(sqlquery.record().indexOf("PR_diff"))->setText(tr("PR_diff"));
    main_table_model->horizontalHeaderItem(sqlquery.record().indexOf("METAL_AVGS"))->setText(tr("METAL_AVGS"));
    main_table_model->horizontalHeaderItem(sqlquery.record().indexOf("METAL_TM_DTTM"))->setText(tr("METAL_TM_DTTM"));
    main_table_model->horizontalHeaderItem(sqlquery.record().indexOf("METAL_diff"))->setText(tr("METAL_diff"));

    value_colum_list.append(sqlquery.record().indexOf("PR_VALUE1"));
    value_colum_list.append(sqlquery.record().indexOf("PR_VALUE2"));
    value_colum_list.append(sqlquery.record().indexOf("PR_VALUE3"));
    value_colum_list.append(sqlquery.record().indexOf("PR_VALUE4"));
    value_colum_list.append(sqlquery.record().indexOf("PR_VALUE5"));
    value_colum_list.append(sqlquery.record().indexOf("PR_VALUE6"));
    value_colum_list.append(sqlquery.record().indexOf("PR_VALUE7"));

    value_colum_list.append(sqlquery.record().indexOf("METAL_VALUE1"));
    value_colum_list.append(sqlquery.record().indexOf("METAL_VALUE2"));
    value_colum_list.append(sqlquery.record().indexOf("METAL_VALUE3"));
    value_colum_list.append(sqlquery.record().indexOf("METAL_VALUE4"));
    value_colum_list.append(sqlquery.record().indexOf("METAL_VALUE5"));
    value_colum_list.append(sqlquery.record().indexOf("METAL_VALUE6"));
    value_colum_list.append(sqlquery.record().indexOf("METAL_VALUE7"));

    PR_TX_DTTM_index = sqlquery.record().indexOf("PR_TM_DTTM");
    Metal_TX_DTTM_index = sqlquery.record().indexOf("METAL_TM_DTTM");
    PR_COLLECTION_ID = sqlquery.record().indexOf("PR_COLLECTION_ID");
    METAL_COLLECTION_ID = sqlquery.record().indexOf("METAL_COLLECTION_ID");
    ui->VALUE_view->setChecked(true);
    for(int i=0;i<value_colum_list.count();i++){
        ui->main_table_view->horizontalHeader()->setSectionHidden(value_colum_list.at(i),true);
    }
    QMessageBox msgbox;
    msgbox.setText("complete");
    msgbox.addButton(QMessageBox::Ok);
    msgbox.exec();

}

void Cdsemmain::on_VALUE_view_clicked(bool checked)
{
    if(checked){
        for(int i=0;i<value_colum_list.count();i++){
            ui->main_table_view->horizontalHeader()->setSectionHidden(value_colum_list.at(i),true);
        }
    }else {
        for(int i=0;i<value_colum_list.count();i++){
            ui->main_table_view->horizontalHeader()->setSectionHidden(value_colum_list.at(i),false);
        }
    }
}

void Cdsemmain::on_main_table_view_doubleClicked(const QModelIndex &index)
{

    QString select_time;
    QString select_collectionid;
    QString select_A;
    QString select_GM;
    QString history_count = ui->History_count->text();
    if(ui->RB_ADI->isChecked()){
        select_time =  main_table_model->index(index.row(),PR_TX_DTTM_index).data().toString();
        select_collectionid = main_table_model->index(index.row(),PR_COLLECTION_ID).data().toString();
        select_A = "ADI";
    }else if(ui->RB_ACI->isChecked()){
        select_time =  main_table_model->index(index.row(),Metal_TX_DTTM_index).data().toString();
        select_collectionid = main_table_model->index(index.row(),METAL_COLLECTION_ID).data().toString();
        select_A = "ACI";
    }
    if(ui->RB_G->isChecked()){
        select_GM =QString("G");
    }else if(ui->RB_M->isChecked()){
        select_GM=QString("M");
    }

    cdsem_chart_widget *widget = new cdsem_chart_widget(select_time,select_collectionid,select_A,select_GM,history_count,ms_mesdb);
    widget->show();

}
