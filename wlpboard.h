#ifndef WLPBOARD_H
#define WLPBOARD_H

#include <QWidget>
#include <NIS/nismainwindow.h>
namespace Ui {
class WLPboard;
}

class WLPboard : public QWidget
{
    Q_OBJECT

public:
    explicit WLPboard(QWidget *parent = 0);
    ~WLPboard();

private slots:
    void on_infrom_entet_btn_clicked();

    void on_NIS_btn_clicked();

    void on_Dashboard_btn_clicked();

private:
    Ui::WLPboard *ui;
};

#endif // WLPBOARD_H
