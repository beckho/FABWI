#ifndef EIS_SERARCH_ITEM_H
#define EIS_SERARCH_ITEM_H

#include <QWidget>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <searchlist_table_widget.h>
namespace Ui {
class EIS_serarch_item;
}

class EIS_serarch_item : public QWidget
{
    Q_OBJECT

public:
    explicit EIS_serarch_item(QWidget *parent = 0);
    Ui::EIS_serarch_item *ui;
    QSqlDatabase my_mesdb;
    searchlist_table_widget *table;
    ~EIS_serarch_item();
signals:
    void search_signal();




private:

private slots:


};

#endif // EIS_SERARCH_ITEM_H
