#include "production_out_viewer.h"
#include "ui_production_out_viewer.h"

production_out_viewer::production_out_viewer(QPushButton *pushbtn,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::production_out_viewer)
{
    ui->setupUi(this);
    this->pushbtn = pushbtn;
    Viewer_widget = pushbtn->property("child").value<QWidget *>();

    Viwer_splitter = pushbtn->property("parent_splitter").value<QSplitter *>();

    ui->main_layout->addWidget(Viewer_widget);

}

production_out_viewer::~production_out_viewer()
{
    delete ui;
}

void production_out_viewer::closeEvent(QCloseEvent *event)
{
    Viwer_splitter->addWidget(Viewer_widget);
    QWidget::closeEvent(event);
}
