#ifndef PRODUCTION_MACHINE_LEVEL1_VIEW_H
#define PRODUCTION_MACHINE_LEVEL1_VIEW_H

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <QDateTime>
#include <production_app/production_main_chart1.h>
#include <production_app/production_main_chart_view1.h>
#include <QVBoxLayout>
#include <QBarSeries>
#include <QBarset>
#include <production_app/production_machine_level1_widget.h>
#include <production_app/chart_maker_level1_source.h>
#include <QSplitter>
#include <QSqlQueryModel>
#include <production_app/production_machine_table_view.h>
#include <production_app/production_out_viewer.h>

namespace Ui {
class production_machine_level1_view;
}

class production_machine_level1_view : public QWidget
{
    Q_OBJECT

public:
    explicit production_machine_level1_view(QSqlDatabase my_mesdb,QSqlDatabase ms_mesdb,QSqlDatabase my_mesdb2,QString machine_name,
                                            QDateTime start_datetime,QDateTime end_datetime,bool part_process,QString part,QString process,QString serach_mode,QString operation_name,QString env_name,QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    QSqlDatabase my_mesdb2;
    QSqlDatabase ms_mesdb;
    QString machine_name;
    QDateTime start_datetime;
    QDateTime end_datetime;
    QString part;
    QString process;
    QString operation_name;
    QString search_mode;
    QVector<chart_maker_level1_source *> operation_chart_source;
    QSplitter *splitter;
    bool part_process;
    QString env_name;


    ~production_machine_level1_view();
public slots:
    void output_viwer(bool result);

private:
    void chart_maker1(chart_maker_level1_source soruce);
    void defect_view_search();
    Ui::production_machine_level1_view *ui;
};

#endif // PRODUCTION_MACHINE_LEVEL1_VIEW_H
