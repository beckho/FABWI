#include "production_machine_level1_widget.h"
#include "ui_production_machine_level1_widget.h"
#include "chart_maker_level1_source.h"

production_machine_level1_widget::production_machine_level1_widget(QSqlDatabase my_mesdb, QSqlDatabase ms_mesdb,QSqlDatabase my_mesdb2, QObject *source, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::production_machine_level1_widget)
{
    ui->setupUi(this);
    this->my_mesdb =my_mesdb ;
    this->ms_mesdb= ms_mesdb;
    this->my_mesdb2 = my_mesdb2;
    this->source = source;
    chart_maker_level1_source *chart_source = qobject_cast<chart_maker_level1_source *>(source);
    ui->start_date_time->setDate(chart_source->search_start_datetime.date());
    ui->end_date_time->setDate(chart_source->search_end_datetime.date());
    out_viwer_btn = ui->outview_btn;
    on_search_btn_clicked();

}

production_machine_level1_widget::~production_machine_level1_widget()
{
    delete ui;
}

void production_machine_level1_widget::on_search_btn_clicked()
{

    chart_maker_level1_source *chart_source = qobject_cast<chart_maker_level1_source *>(source);
    QDate start_date = ui->start_date_time->date();
    QDate end_date = ui->end_date_time->date();
    int days_count = start_date.daysTo(end_date);
    QSqlQuery query(ms_mesdb);
    QSqlQuery query2(ms_mesdb);
    QSqlQuery query3(my_mesdb2);
    if(chart_source->barseries->count()>0){
        chart_source->main_chart->removeAllSeries();
        chart_source->main_chart->removeAxis(chart_source->catacorybar_axis);
        chart_source->main_chart->removeAxis(chart_source->value_axis);
        chart_source->barseries = new QBarSeries();

        chart_source->catacorybar_axis = new QBarCategoryAxis();
        if(chart_source->Operation_name == "ALL"){
            chart_source->goal_count_series = new QScatterSeries();
            chart_source->goal_count_series->setMarkerSize(10);
            chart_source->goal_count_series->setPointLabelsVisible(true);
            chart_source->goal_count_series->setPointLabelsFormat("@yPoint");
            chart_source->goal_count_series->setName("goal_count");

        }
        chart_source->value_axis = new QValueAxis();
        chart_source->input_qty_barset = new QBarSet("input_qty");
        chart_source->output_qty_barset = new QBarSet("output_qty");
        chart_source->output_qty_barset->setLabelColor("black");
//        chart_source->barseries->append(chart_source->input_qty_barset);
        chart_source->barseries->append(chart_source->output_qty_barset);
        chart_source->barseries->setLabelsVisible(true);
    }


    QStringList catagorylist;
    QList<QVariant> redbar_list;
    int goal_count = 0;
    if(chart_source->Operation_name == "ALL"){
        if(!chart_source->part_process){
            QSqlQuery query_1(my_mesdb);
            if(chart_source->search_mode=="SiO2,trimmer"){
                query_1.exec(QString("select goal_count from Production_app_machine_view_table where machine_name = '%1' AND operation_name = '%2' AND env_name = '%3'  ")
                             .arg(chart_source->machinename).arg(chart_source->Operation_name2).arg(chart_source->env_name));
            }else {
                query_1.exec(QString("select goal_count from Production_app_machine_view_table where machine_name = '%1' AND env_name = '%2' ")
                             .arg(chart_source->machinename).arg(chart_source->env_name));
            }
            if(query_1.next()){
                goal_count = query_1.value("goal_count").toInt();
            }
        }else {
            QSqlQuery query_1(my_mesdb);

            query_1.exec(QString("select goal_count from Production_app_goal_count_list where part = '%1' AND process = '%2' AND env_name = '%3' ")
                             .arg(chart_source->part).arg(chart_source->process).arg(chart_source->env_name));


            if(query_1.next()){
                goal_count = query_1.value("goal_count").toInt();
            }
        }
    }
    QString part_process_machine_list;
    if(chart_source->part_process){
        QSqlQuery query2(my_mesdb);
        query2.exec(QString("select machine_name from Production_app_machine_view_table where part = '%1' AND machine_part = '%2' AND env_name = '%3'")
                    .arg(chart_source->part).arg(chart_source->process).arg(chart_source->env_name));
        while(query2.next()){
            part_process_machine_list = part_process_machine_list.append(QString("'%1',").arg(query2.value("machine_name").toString()));
        }
        part_process_machine_list = part_process_machine_list.remove(part_process_machine_list.length()-1,1);
    }
    for(int i=0;i<=days_count;i++){
        QDate select_date = start_date.addDays(i);
        QString select_date_start_txt = select_date.toString("yyyyMMdd");
        QString select_date_end_txt = select_date.toString("yyyyMMdd");
        QString query_txt;
        if(chart_source->Operation_name == "ALL"){
            if(!chart_source->part_process){
                if(chart_source->search_mode=="SiO2,trimmer"){
                    query_txt = QString("select SUM(qty) AS `qty`,machine_name,MAX(machine_code) as `machine_code` from machine_output_summary "
                                            "where summary_date between '%1' AND '%2' AND "
                                            "machine_name = '%3' AND operation_name IN ('%4')  group by machine_name")
                            .arg(select_date_start_txt).arg(select_date_end_txt).arg(chart_source->machinename).arg(chart_source->Operation_name2);
                }else {
                    query_txt = QString("select SUM(qty) AS `qty`,machine_name,MAX(machine_code) as `machine_code` from machine_output_summary "
                                            "where summary_date between '%1' AND '%2' AND "
                                            "machine_name = '%3'  group by machine_name")
                            .arg(select_date_start_txt).arg(select_date_end_txt).arg(chart_source->machinename);
                }

            }else {
                if(chart_source->search_mode=="SiO2,trimmer"){
                    query_txt = QString("select SUM(qty) AS `qty`,machine_name,MAX(machine_code) as `machine_code` from machine_output_summary "
                                        "where summary_date between '%1' AND '%2' AND "
                                        "machine_name IN (%3) AND operation_name IN ('%4','%5') ")
                            .arg(select_date_start_txt).arg(select_date_end_txt).arg(part_process_machine_list).arg(tr("SiO2 trimmer")).arg(tr("trimmer"));
                }else {
                    query_txt = QString("select SUM(qty) AS `qty`,machine_name,MAX(machine_code) as `machine_code` from machine_output_summary "
                                        "where summary_date between '%1' AND '%2' AND "
                                        "machine_name IN (%3)  ")
                            .arg(select_date_start_txt).arg(select_date_end_txt).arg(part_process_machine_list);
                }

                qDebug()<<part_process_machine_list;
            }
        }else {
            query_txt = QString("select SUM(qty) AS `qty`,machine_name,MAX(machine_code) as `machine_code` from machine_output_summary "
                                "where summary_date between '%1' AND '%2' AND "
                                "machine_name = '%3'  group by machine_name")
                    .arg(select_date_start_txt).arg(select_date_end_txt).arg(chart_source->machinename);
//            query_txt = QString("select SUM(WAFER_INPUT_QTY)-SUM(WAFER_EXCLUDE_LOSS_QTY)-SUM(WAFER_EXCLUDE_FAIL_QTY) as WAFER_INPUT_QTY,SUM(WAFER_OUTPUT_QTY) as WAFER_OUTPUT_QTY "
//                                "from V_FAB_OUTPUT_LOTS (NOLOCK) where EQUIPMENT_NAME = '%1' AND WORK_DATE between '%2' AND '%3' AND OPERATION_SHORT_NAME = '%4'")
//                    .arg(chart_source->machinename).arg(select_date_start_txt).arg(select_date_end_txt).arg(chart_source->Operation_name);
        }
        query3.exec(query_txt);
        qDebug()<<query3.lastQuery();
        qDebug()<<query3.lastError().text();

        if(query3.next()){
//            chart_source->input_qty_barset->append(query.value("WAFER_INPUT_QTY").toReal());
            chart_source->output_qty_barset->append(query3.value("qty").toReal());

//            if(chart_source->Operation_name == "ALL"){

//            }
//            if(query.value("WAFER_INPUT_QTY").toReal()!=query.value("WAFER_OUTPUT_QTY").toReal()){
//                redbar_list.append(chart_source->output_qty_barset->count()-1);
//            }
        }else {
            chart_source->output_qty_barset->append(0);
        }
        chart_source->goal_count_series->append(i,goal_count);
        catagorylist<<select_date.toString("MM/dd");
    }
    chart_source->catacorybar_axis->append(catagorylist);
//    chart_source->output_qty_barset->setProperty("redbar",redbar_list);

    chart_source->main_chart->setAxisX(chart_source->catacorybar_axis);
    chart_source->main_chart->setAxisY(chart_source->value_axis);

    chart_source->main_chart->addSeries(chart_source->barseries);
    if(chart_source->Operation_name == "ALL"){
        chart_source->main_chart->addSeries(chart_source->goal_count_series);
    }
    chart_source->barseries->attachAxis(chart_source->catacorybar_axis);
    chart_source->barseries->attachAxis(chart_source->value_axis);
    if(chart_source->Operation_name == "ALL"){
        chart_source->goal_count_series->attachAxis(chart_source->catacorybar_axis);
        chart_source->goal_count_series->attachAxis(chart_source->value_axis);
        if(goal_count != 0){
            chart_source->value_axis->setRange(0,goal_count+(goal_count*0.3));
        }
    }
    ui->main_chart_layout->addWidget(chart_source->chart_view);

    if(chart_source->Operation_name == "ALL"){
//        QString start_date_txt = ui->start_date_time->date().toString("yyyyMMdd");
//        QString end_date_txt = ui->end_date_time->date().toString("yyyyMMdd");
//        QString query_txt;
//        if(!chart_source->part_process){
//            query_txt = QString("select SUM(qty) AS `qty`,machine_code,MAX(machine_name) as `machine_name` from machine_output_summary "
//                                "where summary_date between '%1' AND '%2' AND "
//                                "machine_code = '%3'  group by machine_code")
//                    .arg(start_date_txt).arg(end_date_txt).arg(chart_source->machinename);
//        }else {
//            query_txt = QString("select SUM(qty) AS `qty`,machine_code,MAX(machine_name) as `machine_name` from machine_output_summary "
//                                "where summary_date between '%1' AND '%2' AND "
//                                "machine_code IN (%4)  group by machine_code")
//                    .arg(start_date_txt).arg(end_date_txt).arg(part_process_machine_list);
//            qDebug()<<query_txt;
//        }


//        chart_source->output_loss_lot_model->setProperty("query",query_txt);
//        chart_source->output_loss_lot_model->setProperty("order",QString(" order by MOVEOUT_DTTM "));
//        chart_source->output_loss_lot_model->setProperty("sort",QString(" asc "));
//        chart_source->output_loss_lot_model->setProperty("database",QVariant::fromValue(ms_mesdb));
//        chart_source->output_loss_lot_model->setQuery(chart_source->output_loss_lot_model->property("query").toString()
//                                                 +chart_source->output_loss_lot_model->property("order").toString()
//                                                 +chart_source->output_loss_lot_model->property("sort").toString(),ms_mesdb);


//        qDebug()<< chart_source->output_loss_lot_model->query().lastQuery();
//        QString query_lots = "";
//        qDebug()<<chart_source->output_loss_lot_model->rowCount();
//        QString old_CASSTTEID = "";
//        for(int i=0;i<chart_source->output_loss_lot_model->rowCount();i++) {

//            QString CASSETTE_ID = chart_source->output_loss_lot_model->record(i).value("CASSETTE_ID").toString();
//            if(old_CASSTTEID == CASSETTE_ID){
//                continue;
//            }else {
//                old_CASSTTEID= CASSETTE_ID;
//            }
//            QString OPERATION_SHORT_NAME = chart_source->output_loss_lot_model->record(i).value("OPERATION_SHORT_NAME").toString();
//            if(i==0){
//                query_lots = query_lots.append(QString(" (CASSETTE_ID = '%1' AND OPERATION_SHORT_NAME = '%2') ")
//                                                .arg(CASSETTE_ID).arg(OPERATION_SHORT_NAME));
//            }else {
//                query_lots = query_lots.append(QString(" OR (CASSETTE_ID = '%1' AND OPERATION_SHORT_NAME = '%2') ")
//                                                .arg(CASSETTE_ID).arg(OPERATION_SHORT_NAME));
//            }
//        }


//        query_txt = QString("select MOVEOUT_DTTM,CASSETTE_ID,LOT_ID,LOT_TYPE,OPERATION_SHORT_NAME,MATERIAL_GROUP,MATERIAL_ID,"
//                            "WORST_NAME,DEFECT_QTY,TX_COMMENT,REWORK_FLAG from V_FAB_DEFECT_LOTS (NOLOCK)"
//                            "where %1 ").arg(query_lots);


//        chart_source->defect_model->setProperty("query",query_txt);
//        chart_source->defect_model->setProperty("order",QString(" order by MOVEOUT_DTTM "));
//        chart_source->defect_model->setProperty("sort",QString(" asc "));
//        chart_source->defect_model->setProperty("database",QVariant::fromValue(ms_mesdb));
//        chart_source->defect_model->setQuery(chart_source->defect_model->property("query").toString()
//                                                 +chart_source->defect_model->property("order").toString()
//                                                 +chart_source->defect_model->property("sort").toString(),ms_mesdb);

//        qDebug()<< chart_source->defect_model->query().lastError().text();
//        query_txt = QString("select MOVEOUT_DTTM,CASSETTE_ID,LOT_ID,LOT_TYPE,OPERATION_SHORT_NAME,MATERIAL_GROUP,MATERIAL_ID,"
//                            "WORST_NAME,WORST_WAFER_QTY,WORST_CHIP_QTY,TX_COMMENT from V_FAB_REWORK_LOTS (NOLOCK) "
//                            "where %1 ").arg(query_lots);
//        chart_source->rework_model->setProperty("query",query_txt);
//        chart_source->rework_model->setProperty("order",QString(" order by MOVEOUT_DTTM "));
//        chart_source->rework_model->setProperty("sort",QString(" asc "));
//        chart_source->rework_model->setProperty("database",QVariant::fromValue(ms_mesdb));
//        chart_source->rework_model->setQuery(chart_source->rework_model->property("query").toString()
//                                                 +chart_source->rework_model->property("order").toString()
//                                                 +chart_source->rework_model->property("sort").toString(),ms_mesdb);
//        qDebug()<< chart_source->rework_model->query().lastQuery();
//        query_txt = QString("select MOVEOUT_DTTM,CASSETTE_ID,LOT_ID,LOT_TYPE,OPERATION_SHORT_NAME,MATERIAL_GROUP,MATERIAL_ID,"
//                            "PROBE_INSP_NAME,PROBE_INSP_QTY from V_FAB_PROBE_INSP_LOTS (NOLOCK) "
//                            "where %1 ").arg(query_lots);
//        chart_source->probe_model->setProperty("query",query_txt);
//        chart_source->probe_model->setProperty("order",QString(" order by MOVEOUT_DTTM "));
//        chart_source->probe_model->setProperty("sort",QString(" asc "));
//        chart_source->probe_model->setProperty("database",QVariant::fromValue(ms_mesdb));
//        chart_source->probe_model->setQuery(chart_source->probe_model->property("query").toString()
//                                                 +chart_source->probe_model->property("order").toString()
//                                                 +chart_source->probe_model->property("sort").toString(),ms_mesdb);
//        if(!chart_source->part_process){
//            query_txt = QString("select * from V_FAB_OUTPUT_LOTS (NOLOCK) where EQUIPMENT_NAME = '%1' AND WORK_DATE between '%2' AND '%3' ")
//                    .arg(chart_source->machinename).arg(start_date_txt).arg(end_date_txt);
//        }else {
//            query_txt = QString("select * from V_FAB_OUTPUT_LOTS (NOLOCK) where EQUIPMENT_NAME IN (%1) AND WORK_DATE between '%2' AND '%3' ")
//                    .arg(part_process_machine_list).arg(start_date_txt).arg(end_date_txt);
//        }
//        chart_source->output_lot_model->setProperty("query",query_txt);
//        chart_source->output_lot_model->setProperty("order",QString(" order by MOVEOUT_DTTM "));
//        chart_source->output_lot_model->setProperty("sort",QString(" asc "));
//        chart_source->output_lot_model->setProperty("database",QVariant::fromValue(ms_mesdb));
//        chart_source->output_lot_model->setQuery(chart_source->output_lot_model->property("query").toString()
//                                                 +chart_source->output_lot_model->property("order").toString()
//                                                 +chart_source->output_lot_model->property("sort").toString(),ms_mesdb);
//        qDebug()<< chart_source->output_lot_model->query().lastQuery();


    }



}

Q_DECLARE_METATYPE(QSqlDatabase)
