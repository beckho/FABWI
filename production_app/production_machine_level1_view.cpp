#include "production_machine_level1_view.h"
#include "ui_production_machine_level1_view.h"

production_machine_level1_view::production_machine_level1_view(QSqlDatabase my_mesdb,QSqlDatabase ms_mesdb,QSqlDatabase my_mesdb2, QString machine_name,
                                                               QDateTime start_datetime, QDateTime end_datetime, bool part_process,QString part,QString process,QString search_mode,QString operation_name,QString env_name, QWidget *parent):
    QWidget(parent),
    ui(new Ui::production_machine_level1_view)
{
     ui->setupUi(this);
     this->my_mesdb = my_mesdb;
     this->my_mesdb2 = my_mesdb2;
     this->ms_mesdb = ms_mesdb;
     this->search_mode = search_mode;
     this->operation_name = operation_name;
     this->machine_name = machine_name;
     this->start_datetime = start_datetime;
     this->end_datetime = end_datetime;
     this->part_process = part_process;
     this->part = part;
     this->process = process;
     this->env_name = env_name;
     splitter = new QSplitter(Qt::Vertical);



    chart_maker_level1_source *all_operation_source = new chart_maker_level1_source();
    all_operation_source->machinename = machine_name;
    all_operation_source->search_start_datetime = start_datetime;
    all_operation_source->search_end_datetime = end_datetime;
    all_operation_source->search_mode = search_mode;
    all_operation_source->Operation_name = "ALL";
    all_operation_source->Operation_name2 = operation_name;
    all_operation_source->barseries = new QBarSeries();
    all_operation_source->input_qty_barset = new QBarSet("input_qty");
    all_operation_source->output_qty_barset = new QBarSet(tr("output_qty"));
    all_operation_source->main_chart = new production_main_chart1();
    all_operation_source->main_chart->setTitle(machine_name);
    all_operation_source->barseries->append(all_operation_source->input_qty_barset);
    all_operation_source->barseries->append(all_operation_source->output_qty_barset);
    all_operation_source->chart_view = new production_main_chart_view1(all_operation_source->main_chart);
    all_operation_source->catacorybar_axis = new QBarCategoryAxis();
    all_operation_source->value_axis = new QValueAxis();
    all_operation_source->main_chart->setAxisX(all_operation_source->catacorybar_axis);
    all_operation_source->main_chart->setAxisY(all_operation_source->value_axis);
    all_operation_source->main_chart->addSeries(all_operation_source->barseries);
    all_operation_source->part = part;
    all_operation_source->process = process;
    all_operation_source->part_process = part_process;
    all_operation_source->env_name = env_name;
    QFont title_font = all_operation_source->main_chart->font();
    title_font.setBold(true);
    title_font.setPointSize(15);
    all_operation_source->main_chart->setTitleFont(title_font);

    all_operation_source->output_lot_model = new QSqlQueryModel();
    ui->lot_data_table->setModel(all_operation_source->output_lot_model);

    all_operation_source->output_loss_lot_model = new QSqlQueryModel();
    ui->output_table_view->setModel(all_operation_source->output_loss_lot_model);

    all_operation_source->defect_model = new QSqlQueryModel();
    ui->defect_table_view->setModel(all_operation_source->defect_model);

    all_operation_source->rework_model = new QSqlQueryModel();
    ui->rework_table_view->setModel(all_operation_source->rework_model);

    all_operation_source->probe_model = new QSqlQueryModel();
    ui->probe_table_view->setModel(all_operation_source->probe_model);


    all_operation_source->main_widget = new production_machine_level1_widget(my_mesdb,ms_mesdb,my_mesdb2,(QObject *)all_operation_source);
    all_operation_source->main_widget->out_viwer_btn->setProperty("child",QVariant::fromValue(all_operation_source->main_widget));
    all_operation_source->main_widget->out_viwer_btn->setProperty("parent_splitter",QVariant::fromValue(splitter));
    connect(all_operation_source->main_widget->out_viwer_btn,SIGNAL(clicked(bool)),this,SLOT(output_viwer(bool)));

    splitter->insertWidget(splitter->count(),all_operation_source->main_widget);
    operation_chart_source.append(all_operation_source);

    if(!part_process){
        QSqlQuery query(ms_mesdb);
        query.exec(QString("select * from V_FAB_OUTPUT_LOTS (NOLOCK) where WORK_DATE between '%2' AND '%3' AND EQUIPMENT_NAME = '%1' group by OPERATION_SHORT_NAME")
                   .arg(machine_name).arg(start_datetime.toString("yyyyMMdd")).arg(end_datetime.toString("yyyyMMdd")));
        qDebug()<<query.lastQuery();
         while(query.next()){
            chart_maker_level1_source *operation_source = new chart_maker_level1_source();
            operation_source->machinename = machine_name;
            operation_source->search_start_datetime = start_datetime;
            operation_source->search_end_datetime = end_datetime;
            operation_source->Operation_name = query.value("OPERATION_SHORT_NAME").toString();
            operation_source->barseries = new QBarSeries();
            operation_source->input_qty_barset = new QBarSet("input_qty");
            operation_source->output_qty_barset = new QBarSet(tr("output_qty"));
            operation_source->main_chart = new production_main_chart1();
            operation_source->main_chart->setTitle(operation_source->Operation_name);
            operation_source->barseries->append(operation_source->input_qty_barset);
            operation_source->barseries->append(operation_source->output_qty_barset);
            operation_source->chart_view = new production_main_chart_view1(operation_source->main_chart);
            operation_source->catacorybar_axis = new QBarCategoryAxis();
            operation_source->goal_count_series = new QScatterSeries();
            operation_source->value_axis = new QValueAxis();
            operation_source->main_chart->setAxisX(operation_source->catacorybar_axis);
            operation_source->main_chart->setAxisY(operation_source->value_axis);
            operation_source->main_chart->addSeries(operation_source->barseries);
            operation_source->main_chart->addSeries(operation_source->goal_count_series);
            operation_source->env_name=env_name;
            operation_source->main_widget = new production_machine_level1_widget(my_mesdb,ms_mesdb,my_mesdb2,(QObject *)operation_source);
            operation_source->main_widget->out_viwer_btn->setProperty("child",QVariant::fromValue(operation_source->main_widget));
            operation_source->main_widget->out_viwer_btn->setProperty("parent_splitter",QVariant::fromValue(splitter));

            connect(operation_source->main_widget->out_viwer_btn,SIGNAL(clicked(bool)),this,SLOT(output_viwer(bool)));
            splitter->insertWidget(splitter->count(),operation_source->main_widget);
            operation_chart_source.append(operation_source);
         }
    }

    ui->main_widget_layout->addWidget(splitter);

}

production_machine_level1_view::~production_machine_level1_view()
{
    delete ui;
}

void production_machine_level1_view::output_viwer(bool result)
{
    QPushButton *sender_obj =  qobject_cast<QPushButton *>(this->sender());
    production_out_viewer *popup = new production_out_viewer(sender_obj);
    popup->show();
}

void production_machine_level1_view::chart_maker1(chart_maker_level1_source soruce)
{

}

