#ifndef PRODCUTION_MAIN_CHART1_H
#define PRODCUTION_MAIN_CHART1_H

#include <QObject>
#include <QWidget>
#include <QtCharts/QChart>
#include <QSqlDatabase>

QT_CHARTS_USE_NAMESPACE

class production_main_chart1 : public QChart
{
    Q_OBJECT
public:
    production_main_chart1(QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0);
    QSqlDatabase my_mesdb;
    QSqlDatabase getMy_mesdb() const;
    void setMy_mesdb(const QSqlDatabase &value);
};

#endif // PRODCUTION_MAIN_CHART1_H
