#include "production_machine_add_popup.h"
#include "ui_production_machine_add_popup.h"

production_machine_add_popup::production_machine_add_popup(QString part, QString part_machine, QSqlDatabase my_mesdb, QString search_mode, QString env_name, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::production_machine_add_popup)
{
    ui->setupUi(this);
    this->search_mode= search_mode;

    this->env_name = env_name;
    main_model=new QSqlQueryModel();
    ui->main_table->setModel(main_model);
    ui->main_table->setSortingEnabled(true);
    connect(ui->main_table->horizontalHeader(),SIGNAL(sortIndicatorChanged(int,Qt::SortOrder)),this,SLOT(SortOrder(int,Qt::SortOrder)));

    QString mydb_name2 = QString("MY_MESDB2_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    if(!my_mesdb2.contains(mydb_name2)){
       my_mesdb2 = QSqlDatabase::addDatabase("QMYSQL",mydb_name2);
       my_mesdb2.setHostName("10.20.10.101");
       my_mesdb2.setPort(3306);
       my_mesdb2.setUserName("EIS");
       my_mesdb2.setPassword("wisolfab!");
       my_mesdb2.setDatabaseName("fab");
    }else {
       my_mesdb2 = QSqlDatabase::database(mydb_name2);
    }
    if(!my_mesdb2.isOpen()){
        if(!my_mesdb2.open()){
             qDebug()<<my_mesdb2.lastError().text();
        }
    }


    this->part = part;
    this->part_machine = part_machine;
    this->my_mesdb= my_mesdb;

    query_txt = QString("select machine_code,machine_name from oi_machine_list where chamber_code = 1 ");
    order_query_text = QString("order by machine_name asc");
    QString query_total_txt = query_txt+order_query_text;
    main_model->setQuery(query_total_txt,my_mesdb2);


}

production_machine_add_popup::~production_machine_add_popup()
{
    delete ui;
}

void production_machine_add_popup::on_search_btn_clicked()
{
    query_txt = QString("select machine_code,machine_name from oi_machine_list where machine_name like '%1%' AND chamber_code = 1 ")
            .arg(ui->LE_search->text());
    QString query_total_txt = query_txt+order_query_text;
    main_model->setQuery(query_total_txt,my_mesdb2);
}

void production_machine_add_popup::SortOrder(int index, Qt::SortOrder order)
{
    QString field_name = main_model->record().fieldName(index);

    if(order == Qt::AscendingOrder){
        order_query_text = QString("order by %1 asc").arg(field_name);
    }else if(order == Qt::DescendingOrder){
        order_query_text = QString("order by %1 desc").arg(field_name);
    }
    QString query_total_txt = query_txt+order_query_text;
    qDebug()<<query_total_txt;
    main_model->setQuery(query_total_txt,my_mesdb2);
}

void production_machine_add_popup::on_buttonBox_accepted()
{
    QModelIndexList index_list= ui->main_table->selectionModel()->selectedIndexes();
    int current_row = -1;
    for(int i=0;i<index_list.count();i++){
        if(current_row != index_list.at(i).row()){
            QSqlQuery query(my_mesdb);
            QString machine_name = main_model->index(index_list.at(i).row(),1).data().toString();
            QString machine_id = main_model->index(index_list.at(i).row(),0).data().toString();
            if(search_mode=="SiO2,trimmer"){
                QString query_txt1 = QString("INSERT INTO `FAB`.`Production_app_machine_view_table` "
                                             "(`part`, `machine_part`, `use`, `machine_name`, `machine_code` , `env_name`,`operation_name`) "
                                             "VALUES ('%1', '%2', '%3', '%4', '%5','%6','%7');").arg(part).arg(part_machine).arg("2").arg(machine_name).arg(machine_id).arg(env_name)
                                              .arg(tr("SiO2 trimmer"));
                query.exec(query_txt1);
                current_row = index_list.at(i).row();
                query_txt1 = QString("INSERT INTO `FAB`.`Production_app_machine_view_table` "
                                             "(`part`, `machine_part`, `use`, `machine_name`, `machine_code` , `env_name`,`operation_name`) "
                                             "VALUES ('%1', '%2', '%3', '%4', '%5','%6','%7');").arg(part).arg(part_machine).arg("2").arg(machine_name).arg(machine_id).arg(env_name)
                                             .arg(tr("trimmer"));
                query.exec(query_txt1);
                current_row = index_list.at(i).row();

            }else {
                QString query_txt1 = QString("INSERT INTO `FAB`.`Production_app_machine_view_table` "
                                             "(`part`, `machine_part`, `use`, `machine_name`, `machine_code` , `env_name`) "
                                             "VALUES ('%1', '%2', '%3', '%4', '%5','%6');").arg(part).arg(part_machine).arg("2").arg(machine_name).arg(machine_id).arg(env_name);
                query.exec(query_txt1);
                current_row = index_list.at(i).row();

            }


        }
    }

}
