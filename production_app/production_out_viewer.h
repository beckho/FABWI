#ifndef PRODUCTION_OUT_VIEWER_H
#define PRODUCTION_OUT_VIEWER_H

#include <QWidget>
#include <QPushButton>
#include <QCloseEvent>
#include <QSplitter>
#include <QDebug>
namespace Ui {
class production_out_viewer;
}

class production_out_viewer : public QWidget
{
    Q_OBJECT

public:
    explicit production_out_viewer(QPushButton *pushbtn, QWidget *parent = 0);
    QPushButton *pushbtn;
    QWidget *Viewer_widget;
    QSplitter *Viwer_splitter;

    ~production_out_viewer();

private:
    Ui::production_out_viewer *ui;
    void closeEvent(QCloseEvent *event);
};

#endif // PRODUCTION_OUT_VIEWER_H
