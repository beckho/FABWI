#ifndef PRODUCTION_MAIN_H
#define PRODUCTION_MAIN_H

#include <QWidget>
#include <QSqlTableModel>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSettings>
#include <production_app/production_machine_table_popup.h>
#include <global_define.h>
#include <Qdebug>
#include <QSqlError>
#include <QDateTime>
#include <QGridLayout>
#include <production_app/production_main_chart1.h>
#include <production_app/production_main_chart_view1.h>
#include <QBarSeries>
#include <QBarset>
#include <QBarCategoryAxis>
#include <QValueAxis>
#include <production_app/production_machine_level1_view.h>
#include <QCheckBox>
#include <QPushButton>
#include <production_app/production_out_viewer.h>
#include <production_app/chart_maker_level2_source.h>
#include <production_app/production_machine_level2_widget.h>
#include <QSettings>
#include <ns_core_com/ns_core_com.h>
#include <QProcess>

class chart_maker_source{
public:
    QDateTime search_start_datetime;
    QDateTime search_end_datetime;
    QStringList machinenamelist;
    QStringList machinecodelist;
    QString search_mode;
    QString part;
    QString process;
    QGridLayout *chart_layout;
    production_main_chart1 *main_chart;
    production_main_chart_view1 *chart_view;



    chart_maker_source();


};

namespace Ui {
class production_main;
}

class production_main : public QWidget
{
    Q_OBJECT

public:
    explicit production_main(QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    QSqlDatabase my_mesdb2;
    QSqlDatabase ms_mesdb;
    chart_maker_source sputtering_source;
    chart_maker_source deposition_source;
    chart_maker_source lift_off_source;

    chart_maker_source photo_krf_source;
    chart_maker_source photo_ILINE_source;
    chart_maker_source photo_coating_source;
    chart_maker_source photo_dev_source;

    chart_maker_source eatching_etcher_source;
    chart_maker_source protective_D_source;
    chart_maker_source protective_E_source;

    chart_maker_source probe_last_source;
    chart_maker_source probe_total_check_source;

    chart_maker_source trimmer1_source;
    chart_maker_source trimmer2_source;

    bool depostion_tab_init_bool;
    bool photo_tab_init_bool;
    bool eatching_tab_init_bool;
    bool probe_tab_init_bool;
    bool trimmer_tab_init_bool;

    QSplitter *total_viwer_spliter;
    QSplitter *output_list_spliter;

    QString last_env;
    int last_index;
     NS_Core_Com::NS_Main *NS_core;
    ~production_main();
    void chart_maker1(chart_maker_source source);

private:
    void deposition_init_tab();

    void photo_init_tab();

    void eatching_list_init_tab();

    void probe_list_init_tab();

    void total_check_init_tab();

    void output_list_init_tab();

    void trimmer_list_init_tab();

    void init_env();

    void goal_count_edit_finish(chart_maker_source source,int value);




private slots:
    void on_sputtering_machine_table_btn_clicked();

    void on_sputtering_search_btn_clicked();

    void on_deposition_search_btn_clicked();

    void on_deposition_machine_table_btn_clicked();

    void on_lift_off_search_btn_clicked();

    void on_lift_off_machine_table_btn_clicked();

    void on_sputtering_zoom_resetbtn_clicked();

    void on_deposition_zoom_resetbtn_clicked();

    void level1_barset_click(int index);

    void on_lift_off_zoom_resetbtn_clicked();

    void out_view_chart(bool result);

    void del_btn_click(bool result);

    void on_total_check_search_btn_clicked();



    void on_total_check_search_day_clicked(bool checked);

    void on_total_check_search_month_clicked(bool checked);

    void on_photo_krf_search_btn_clicked();

    void on_photo_krf_machine_table_btn_clicked();

    void on_photo_krf_zoom_resetbtn_clicked();


    void on_sputtering_goal_count_editingFinished();

    void on_deposition_goal_count_editingFinished();

    void on_photo_krf_goal_count_editingFinished();

    void on_photo_ILINE_search_btn_clicked();

    void on_photo_ILINE_machine_table_btn_clicked();

    void on_photo_ILINE_zoom_resetbtn_clicked();

    void on_photo_ILINE_goal_count_editingFinished();

    void on_photo_coating_search_btn_clicked();

    void on_photo_coating_machine_table_btn_clicked();

    void on_photo_coating_zoom_resetbtn_clicked();

    void on_photo_coating_goal_count_editingFinished();

    void on_photo_dev_search_btn_clicked();

    void on_photo_dev_machine_table_btn_clicked();

    void on_photo_dev_zoom_resetbtn_clicked();

    void on_photo_dev_goal_count_editingFinished();

    void on_tabWidget_currentChanged(int index);

    void on_output_list_search_btn_clicked();

    void on_output_list_search_day_clicked(bool checked);

    void on_output_list_search_month_clicked(bool checked);

    void on_eatching_etcher_search_btn_clicked();

    void on_eatching_etcher_machine_table_btn_clicked();

    void on_eatching_etcher_zoom_resetbtn_clicked();

    void on_eatching_etcher_goal_count_editingFinished();


    void on_protective_D_search_btn_clicked();

    void on_protective_E_search_btn_clicked();

    void on_protective_D_machine_table_btn_clicked();

    void on_protective_D_zoom_resetbtn_clicked();

    void on_protective_D_goal_count_editingFinished();

    void on_protective_E_machine_table_btn_clicked();

    void on_protective_E_zoom_resetbtn_clicked();

    void on_protective_E_goal_count_editingFinished();

    void on_probe_total_check_search_btn_clicked();

    void on_probe_last_search_btn_clicked();

    void on_probe_last_machine_table_btn_clicked();

    void on_probe_last_zoom_resetbtn_clicked();

    void on_probe_last_goal_count_editingFinished();

    void on_probe_total_check_machine_table_btn_clicked();

    void on_probe_total_check_zoom_resetbtn_clicked();

    void on_probe_total_check_goal_count_editingFinished();


    void on_CB_env_currentIndexChanged(const QString &arg1);

    void on_lift_off_goal_count_editingFinished();

    void on_trimmer1_search_btn_clicked();

    void on_trimmer2_search_btn_clicked();

    void on_trimmer1_machine_table_btn_clicked();

    void on_trimmer2_machine_table_btn_clicked();

    void on_trimmer1_goal_count_editingFinished();



    void on_trimmer1_zoom_resetbtn_clicked();

    void on_trimmer2_goal_count_editingFinished();

private:
    Ui::production_main *ui;
};


#endif // PRODUCTION_MAIN_H
