#ifndef PRODUCTION_MACHINE_TABLE_MODEL_H
#define PRODUCTION_MACHINE_TABLE_MODEL_H

#include <QObject>
#include <QWidget>
#include <QSqlTableModel>
#include <QDebug>
#include <QModelIndex>
#include <QSqlRecord>
#include <QSqlField>
#include <QDateTime>
#include <QSqlQuery>
#include <QSqlError>

class production_machine_table_model : public QSqlTableModel
{
public:
    production_machine_table_model(QObject *parent,QSqlDatabase db);
    Qt::ItemFlags flags(const QModelIndex & index) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const ;
    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

};

#endif // PRODUCTION_MACHINE_TABLE_MODEL_H
