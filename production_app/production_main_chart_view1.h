#ifndef PRODUCTION_MAIN_CHART_VIEW1_H
#define PRODUCTION_MAIN_CHART_VIEW1_H

#include <QObject>
#include <QWidget>
#include <QChartView>

QT_CHARTS_USE_NAMESPACE

class production_main_chart_view1 : public QChartView
{
public:
    production_main_chart_view1(QChart *chart, QWidget *parent = 0);
    QChart *mchart;
protected:
    void keyPressEvent(QKeyEvent *event);
};

#endif // PRODUCTION_MAIN_CHART_VIEW1_H
