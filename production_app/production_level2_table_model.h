#ifndef PRODUCTION_LEVEL2_TABLE_MODEL_H
#define PRODUCTION_LEVEL2_TABLE_MODEL_H

#include <QSqlQueryModel>
#include <QSqlRecord>
#include <QSqlQuery>
class production_level2_table_model :  public QSqlQueryModel
{
    Q_OBJECT
public:
    production_level2_table_model(QObject *parent = Q_NULLPTR);
private:
    QVariant data(const QModelIndex &index, int role) const;
};

#endif // PRODUCTION_LEVEL2_TABLE_MODEL_H
