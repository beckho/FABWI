#include "production_machine_level2_popup.h"
#include "ui_production_machine_level2_popup.h"

production_machine_level2_popup::production_machine_level2_popup(QDate start_date, QDate end_date, QString Lot_type,QSqlDatabase my_mesdb, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::production_machine_level2_popup)
{
    ui->setupUi(this);
    production_init_flag=false;
    lot_id_tab_init_flag=false;
    this->start_date = start_date;
    this->end_date = end_date;
    this->Lot_type = Lot_type;


    this->my_mesdb = my_mesdb;

    emit ui->tabWidget->currentChanged(0);
}

production_machine_level2_popup::~production_machine_level2_popup()
{
    delete ui;
    this->deleteLater();
}

void production_machine_level2_popup::prodcution_tab_init()
{
    if(!production_init_flag){
        production_model = new production_level2_table_model(this);
        ui->main_table->setModel(production_model);
        production_model->setProperty("database",QVariant::fromValue(my_mesdb));

        QString query_txt = QString("select MATERIAL_CATEGORY,SERIES,MATERIAL_NAME,BOM_ID,SUM(WAFER_QTY) as WAFER_QTY,SUM(CHIP_QTY) as CHIP_QTY,SITE from V_FAB_SHIP where %1 AND WORK_DATE >=%2  AND WORK_DATE <= %3 "
                                    "group by MATERIAL_CATEGORY,SERIES,MATERIAL_NAME,BOM_ID,SITE")
                                    .arg(Lot_type).arg(start_date.toString("yyyyMMdd")).arg(end_date.toString("yyyyMMdd"));
        production_model->setProperty("WAFER_QTY_index",4);
        production_model->setProperty("CHIP_QTY_index",5);
        production_model->setProperty("query",query_txt);
        production_model->setProperty("order"," order by MATERIAL_CATEGORY " );
        production_model->setProperty("sort"," asc ");
        qDebug()<<query_txt;
        production_model->setQuery(query_txt + production_model->property("order").toString() + production_model->property("sort").toString(),my_mesdb);

        QSqlQuery table_query = production_model->query();
        table_query.first();
        table_query.previous();
        procution_cb_model = new QStandardItemModel();
        lot_cb_model = new QStandardItemModel();
        ui->CB_prodcution->setModel(procution_cb_model);
        int i=1;
        procution_cb_model->insertRow(0,new QStandardItem(" "));
        lot_cb_model->insertRow(0,new QStandardItem(" "));
        while(table_query.next()){
             procution_cb_model->insertRow(i,new QStandardItem(table_query.value("MATERIAL_NAME").toString()));
             lot_cb_model->insertRow(i,new QStandardItem(table_query.value("MATERIAL_NAME").toString()));
             i++;
        }

    }
}

void production_machine_level2_popup::lot_id_tab_init()
{
    if(!lot_id_tab_init_flag){
        lot_model = new production_level2_table_model(this);
        ui->main_table2->setModel(lot_model);
        lot_model->setProperty("database",QVariant::fromValue(my_mesdb));
        QString query_txt = QString("select TX_DTTM,MATERIAL_NAME,RAW_MATERIAL_NAME,LOT_ID,TX_USER_NAME,BOM_ID,WAFER_QTY,CHIP_QTY,SITE,LOT_TYPE,TX_COMMENT from V_FAB_SHIP where %1 AND WORK_DATE >=%2  AND WORK_DATE <= %3 ")
                                    .arg(Lot_type).arg(start_date.toString("yyyyMMdd")).arg(end_date.toString("yyyyMMdd"));
        lot_model->setProperty("WAFER_QTY_index",6);
        lot_model->setProperty("CHIP_QTY_index",7);
        lot_model->setProperty("query",query_txt);
        lot_model->setProperty("order"," order by TX_DTTM " );
        lot_model->setProperty("sort"," asc ");
        qDebug()<<query_txt;
        lot_model->setQuery(query_txt + lot_model->property("order").toString() + lot_model->property("sort").toString(),my_mesdb);
        ui->CB_lot_id_production->setModel(lot_cb_model);

    }
}
void production_machine_level2_popup::on_tabWidget_currentChanged(int index)
{
    if(index == 0){
        prodcution_tab_init();
        production_init_flag = true;
    }else if(index == 1) {
        lot_id_tab_init();
        lot_id_tab_init_flag = true;
    }
}
void production_machine_level2_popup::on_production_search_btn_clicked()
{
    QString add_query ;
    if(ui->CB_prodcution->currentText().trimmed().length()!=0){
        add_query = QString(" AND MATERIAL_NAME LIKE '%%1%' ").arg(ui->CB_prodcution->currentText());
    }else {
        add_query = QString(" ");
    }
    QString query_txt = QString("select MATERIAL_CATEGORY,SERIES,MATERIAL_NAME,BOM_ID,SUM(WAFER_QTY) as WAFER_QTY,SUM(CHIP_QTY) as CHIP_QTY,SITE from V_FAB_SHIP where %1 AND WORK_DATE >=%2  AND WORK_DATE <= %3 %4"
                                "group by MATERIAL_CATEGORY,SERIES,MATERIAL_NAME,BOM_ID,SITE")
                                .arg(Lot_type).arg(start_date.toString("yyyyMMdd")).arg(end_date.toString("yyyyMMdd")).arg(add_query);
    production_model->setProperty("query",query_txt);
    production_model->setQuery(query_txt,my_mesdb);
}
void production_machine_level2_popup::on_lot_id_search_btn_clicked()
{
    QString add_query;
    if(ui->LE_lot_id->text().trimmed().length()!=0){
        add_query = QString(" AND LOT_ID LIKE '%1%' ").arg(ui->LE_lot_id->text());
    }else {
        add_query = "";
    }
    QString query_txt = QString("select TX_DTTM,MATERIAL_NAME,RAW_MATERIAL_NAME,LOT_ID,TX_USER_NAME,BOM_ID,WAFER_QTY,CHIP_QTY,SITE,LOT_TYPE,TX_COMMENT from V_FAB_SHIP where %1 AND WORK_DATE >=%2  AND WORK_DATE <= %3 %4 ")
                                .arg(Lot_type).arg(start_date.toString("yyyyMMdd")).arg(end_date.toString("yyyyMMdd")).arg(add_query);
    lot_model->setProperty("query",query_txt);
    lot_model->setQuery(query_txt,my_mesdb);

}


void production_machine_level2_popup::on_lot_prodcution_search_btn_clicked()
{
    QString add_query ;
    if(ui->CB_lot_id_production->currentText().trimmed().length()!=0){
        add_query = QString(" AND MATERIAL_NAME LIKE '%%1%' ").arg(ui->CB_lot_id_production->currentText());
    }else {
        add_query = QString(" ");
    }
    QString query_txt = QString("select TX_DTTM,MATERIAL_NAME,RAW_MATERIAL_NAME,LOT_ID,TX_USER_NAME,BOM_ID,WAFER_QTY,CHIP_QTY,SITE,LOT_TYPE,TX_COMMENT from V_FAB_SHIP where %1 AND WORK_DATE >=%2  AND WORK_DATE <= %3 %4 ")
                                .arg(Lot_type).arg(start_date.toString("yyyyMMdd")).arg(end_date.toString("yyyyMMdd")).arg(add_query);
    lot_model->setProperty("query",query_txt);
    lot_model->setQuery(query_txt,my_mesdb);
}

Q_DECLARE_METATYPE(QSqlDatabase)




