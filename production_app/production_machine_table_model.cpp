#include "production_machine_table_model.h"

production_machine_table_model::production_machine_table_model(QObject *parent,QSqlDatabase db):QSqlTableModel(parent,db)
{

}

Qt::ItemFlags production_machine_table_model::flags(const QModelIndex &index) const
{
    if(index.column() == fieldIndex("use")){
        return QSqlTableModel::flags(index)|Qt::ItemIsUserCheckable;
    }
    return QSqlTableModel::flags(index);
}

QVariant production_machine_table_model::data(const QModelIndex &index, int role) const
{
    if(index.column() == fieldIndex("use") && role == Qt::DisplayRole){
        if(record(index.row()).value(fieldIndex("use")).toBool()){
            return "O";
        }else {
            return "X";
        }
    }
    if(index.column() == fieldIndex("use") && role == Qt::CheckStateRole)
   {
        if(record(index.row()).value(fieldIndex("use")).toBool()){
            return Qt::Checked;
        }else {
            return Qt::Unchecked;
        }
    }
    return QSqlTableModel::data(index, role);

}

bool production_machine_table_model::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.column() == fieldIndex("use") && role == Qt::CheckStateRole)
    {
        bool result = false;
        if ( value == Qt::Checked ){
            QSqlRecord recode = record(index.row());
            recode.setGenerated("part",true);
            recode.setGenerated("machine_part",true);
            recode.setGenerated("machine_name",true);
            recode.setGenerated("machine_code",true);
            recode.setValue("use",Qt::Checked);
            result = setRecord(index.row(),recode);
        }
        else if(value == Qt::Unchecked) {
            QSqlRecord recode = record(index.row());
            recode.setGenerated("part",true);
            recode.setGenerated("machine_part",true);
            recode.setGenerated("machine_name",true);
            recode.setGenerated("machine_code",true);
            recode.setValue("use",Qt::Unchecked);
            result = setRecord(index.row(),recode);
        }
        emit dataChanged(index, index );
        qDebug()<<result;
        return result;
    }
    return QSqlTableModel::setData(index, value, role);
}
