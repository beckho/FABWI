#include "production_machine_level2_widget.h"
#include "ui_production_machine_level2_widget.h"

production_machine_level2_widget::production_machine_level2_widget(chart_maker_level2_source *source, NS_Core_Com::NS_Main *NS_Core, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::production_machine_level2_widget)
{
    ui->setupUi(this);
    this->source = source;
    this->my_mesdb = source->my_mesdb;
    QString dbname = QDateTime::currentDateTime().toString("yyyy-MM-dd.zzz")+"localdb";
    this->my_localdb = QSqlDatabase::addDatabase("QSQLITE",dbname);

    my_localdb.setDatabaseName(":memory:");
//    my_localdb.setDatabaseName("testdb.db");

    my_localdb.open();

    this->NS_Core = NS_Core;


    output_viwer_btn = ui->output_viwer_btn;
    del_btn = ui->del_btn;


    source->chart = new production_main_chart1();
    source->chart_view = new production_main_chart_view1(source->chart);
    source->stackerbarseries = new QStackedBarSeries();
    source->scatterSeries = new QScatterSeries();
    source->scatterSeries->setMarkerSize(5);
    source->scatterSeries->setPointLabelsVisible(true);
    source->scatterSeries->setPointLabelsFormat("@comma_long_value");

    source->CSP_set = new QBarSet("CSP");
    source->CSP_set->setProperty("tag","CSP");
    source->CSP_set->setLabelColor("balck");
    source->WLP_set = new QBarSet("WLP");
    source->WLP_set->setProperty("tag","WLP");
    source->WLP_set->setLabelColor("balck");
    source->NJRC_set = new QBarSet("NJRC");
    source->NJRC_set->setProperty("tag","NJRC");
    source->NJRC_set->setLabelColor("balck");
    source->TC_set = new QBarSet("TC");
    source->TC_set->setProperty("tag","TC");
    source->TC_set->setLabelColor("balck");
    QStringList date_query_list;
    QStringList Category_txt_list;
    Category_txt_list.append("total");

    QString start_date_str="eee";
    QString end_date_str = "eee";
    if(source->select_date_type == "day"){
        start_date_str = source->start_date.toString("yyyyMMdd") + "080000";
        end_date_str = source->end_date.addDays(+1).toString("yyyyMMdd") + "075959";
    }if(source->select_date_type == "month"){
        start_date_str = source->start_date.toString("yyyyMM01") + "080000";
        end_date_str = source->end_date.addMonths(1).toString("yyyyMM01")+ + "075959";
    }

    if(source->data_name == tr("outputlist")){
        QString create_sql = QString("CREATE TABLE [ship_output_view]( "
                                     "[Category] VARCHAR(50), "
                                     "[Series] VARCHAR, "
                                     "[Material] VARCHAR(50), "
                                     "[BOMCode] VARCHAR(50), "
                                     "[TianjinBOMCode] VARCHAR(50), "
                                     "[WaferQty] BIGINT, "
                                     "[ChipQty] BIGINT, "
                                     "[DefaultChipQty] BIGINT, "
                                     "[SHIP_DATE] DATE, "
                                     "[Site] VARCHAR(50));");


        QSqlQuery my_local_query(my_localdb);
        my_local_query.exec(create_sql);

        QStringList sites;
        sites.append("TIANJIN");
        sites.append("S3WLP");
        //if outputview
        QStringList result_data= NS_Core->get_SHIP_data(start_date_str,end_date_str,source->types,sites);
        QString insert_value = "";
        foreach (QString items, result_data) {
            items = items.remove(0,2);
            items = items.remove(items.length()-2,2);
            qDebug()<<items;
            QStringList itemslist = items.split(",");
            QString Category  = return_item(itemslist.at(0));
            QString Series  = return_item(itemslist.at(1));
            QString Material  = return_item(itemslist.at(2));
            QString BOMCode  = return_item(itemslist.at(3));
            QString TianjinBOMCode  = return_item(itemslist.at(4));
            QString WaferQty  = return_item(itemslist.at(5));
            QString ChipQty  = return_item(itemslist.at(6));
            QString DefaultChipQty  = return_item(itemslist.at(7));
            QString SHIP_DATE  = QDate::fromString(return_item(itemslist.at(8)),"yyyyMMdd").toString("yyyy-MM-dd");
            QString Site  = return_item(itemslist.at(9));
            insert_value += QString("('%1','%2','%3','%4','%5',%6,%7,%8,'%9','%10'),")
                    .arg(Category).arg(Series).arg(Material).arg(BOMCode).arg(TianjinBOMCode).arg(WaferQty)
                    .arg(ChipQty).arg(DefaultChipQty).arg(SHIP_DATE).arg(Site);

        }
        insert_value = insert_value.remove(insert_value.length()-1,1);
        QString insert_sql = QString("insert into ship_output_view (Category,Series,Material,"
                                     "BOMCode,TianjinBOMCode,WaferQty,ChipQty,DefaultChipQty,SHIP_DATE"
                                     ",Site) values %1").arg(insert_value);
        my_local_query.exec(insert_sql);
        qDebug()<<my_local_query.lastQuery();
        qDebug()<<my_local_query.lastError().text();
    }

    if(source->select_date_type == "day"){
        if(source->data_name == tr("outputlist")){
            int day_count = source->start_date.daysTo(source->end_date)+1;
            for(int i=0;i<day_count;i++){

                date_struct_list.append(new date_struct(source->start_date.addDays(i),source->start_date.addDays(i)));

                QString start_date_txt =source->start_date.addDays(i).toString("yyyy-MM-dd");
                date_query_list.append(QString(" SHIP_DATE = '%1' ").arg(start_date_txt));
                Category_txt_list.append(source->start_date.addDays(i).toString("MM/dd"));
            }
        }else if(source->data_name ==  tr("total check")){
            int day_count = source->start_date.daysTo(source->end_date)+1;
              for(int i=0;i<day_count;i++){

                  date_struct_list.append(new date_struct(source->start_date.addDays(i),source->start_date.addDays(i)));

                  QString start_date_txt =source->start_date.addDays(i).toString("yyyy-MM-dd");
                  date_query_list.append(QString(" summary_date = '%1' ").arg(start_date_txt));
                  Category_txt_list.append(source->start_date.addDays(i).toString("MM/dd"));
              }


        }

    }else if(source->select_date_type == "month"){
        if(source->data_name == tr("outputlist")){
            int month_count = 0;
            while(true){
                if((source->start_date.addMonths(month_count).year() == source->end_date.year())
                        &&(source->start_date.addMonths(month_count).month() == source->end_date.month())){
                    break;
                }
                month_count++;
            }
            month_count++;

            for(int i=0;i<month_count;i++){

                QDate start_month_day = QDateTime(source->start_date.addMonths(i)).date();
                int year = start_month_day.year();
                int month = start_month_day.month();
                int dayofmonth = start_month_day.daysInMonth();
                QDate search_start_day(QDate(year,month,1));

                QDate search_end_day(QDate(year,month,dayofmonth));
                date_struct_list.append(new date_struct(search_start_day,search_end_day));

                QString start_date_txt =source->start_date.addMonths(i).toString("yyyy-MM-01");
                QString start_end_txt =source->start_date.addMonths(i).toString("yyyy-MM-")+QString("%1").arg(source->start_date.daysInMonth());
                date_query_list.append(QString(" SHIP_DATE >= '%1' AND SHIP_DATE <= '%2' ").arg(start_date_txt).arg(start_end_txt));
                Category_txt_list.append(source->start_date.addMonths(i).toString("yy/MM"));
            }

        }else if(source->data_name ==  tr("total check")){
            int month_count = 0;
                    while(true){
                        if((source->start_date.addMonths(month_count).year() == source->end_date.year())
                                &&(source->start_date.addMonths(month_count).month() == source->end_date.month())){
                            break;
                        }
                        month_count++;
                    }
                    month_count++;

                    for(int i=0;i<month_count;i++){

                        QDate start_month_day = QDateTime(source->start_date.addMonths(i)).date();
                        int year = start_month_day.year();
                        int month = start_month_day.month();
                        int dayofmonth = start_month_day.daysInMonth();
                        QDate search_start_day(QDate(year,month,1));

                        QDate search_end_day(QDate(year,month,dayofmonth));
                        date_struct_list.append(new date_struct(search_start_day,search_end_day));

                        QString start_date_txt =source->start_date.addMonths(i).toString("yyyy-MM-01");
                        QString start_end_txt =source->start_date.addMonths(i).toString("yyyy-MM-")+QString("%1").arg(source->start_date.daysInMonth());
                        date_query_list.append(QString(" summary_date >= '%1' AND summary_date <= '%2' ").arg(start_date_txt).arg(start_end_txt));
                        Category_txt_list.append(source->start_date.addMonths(i).toString("yy/MM"));
                    }

        }
    }
    date_struct_list.insert(0,new date_struct(date_struct_list.first()->start_date,date_struct_list.last()->end_date));


    QString qry_type_query;

    if(source->data_name == tr("outputlist")){
        if(source->select_unit_type == "wafer"){
            qry_type_query = "wafer";
        }else {
            qry_type_query = "chip";
        }
    }else if(source->data_name ==  tr("total check")){
        if(source->select_unit_type == "wafer"){
             qry_type_query = " qty_type IN ('wafer') ";
         }else {
             qry_type_query = " qty_type IN ('chip') ";
         }
    }

    if(source->data_name ==  tr("total check")){
        for(int i=0;i<source->types.count();i++){
            Lot_type_query = Lot_type_query.append(QString("'%1',").arg(source->types.at(i)));
        }
        Lot_type_query = Lot_type_query.remove(Lot_type_query.length()-1,1);
        popup_Lot_type_query = QString(" LOT_TYPE IN (%1)").arg(Lot_type_query);
        Lot_type_query = QString(" lot_type IN (%1)").arg(Lot_type_query);

    }
    if(source->data_name == tr("outputlist")){
        maker_bar_set(source->CSP_set,date_query_list," Category NOT IN ('TC_DPX','TC_MOLD','NJRC','WLP') ",qry_type_query);
        maker_bar_set(source->NJRC_set,date_query_list," Category IN ('NJRC') ",qry_type_query);
        maker_bar_set(source->WLP_set,date_query_list," Category IN ('WLP') ",qry_type_query);
        maker_bar_set(source->TC_set,date_query_list," Category IN ('TC_MOLD','TC_DPX') ",qry_type_query);
    }else if(source->data_name ==  tr("total check")){
        maker_bar_set_cheked(source->CSP_set,date_query_list,Lot_type_query," out_type IN ('CSP') ",qry_type_query);
        maker_bar_set_cheked(source->NJRC_set,date_query_list,Lot_type_query," out_type IN ('NJRC') ",qry_type_query);
        maker_bar_set_cheked(source->WLP_set,date_query_list,Lot_type_query," out_type IN ('WLP') ",qry_type_query);
        maker_bar_set_cheked(source->TC_set,date_query_list,Lot_type_query," out_type IN ('TC') ",qry_type_query);

    }


    for(int i=0;i<source->CSP_set->count();i++){
        qreal scatter_point_value = 0;
        scatter_point_value += source->CSP_set->at(i);
        scatter_point_value += source->NJRC_set->at(i);
        scatter_point_value += source->WLP_set->at(i);
        scatter_point_value += source->TC_set->at(i);
        source->scatterSeries->append(i,scatter_point_value);
    }


    source->stackerbarseries->append(source->CSP_set);
    source->stackerbarseries->append(source->NJRC_set);
    source->stackerbarseries->append(source->WLP_set);
    source->stackerbarseries->append(source->TC_set);

    source->chart->addSeries(source->stackerbarseries);
    source->chart->addSeries(source->scatterSeries);

    source->categoryaxis = new QBarCategoryAxis();

    source->stackerbarseries->setLabelsVisible(true);

    source->stackerbarseries->setLabelsFormat("@tag(@comma_long_value)");


    source->categoryaxis->append(Category_txt_list);

    source->valueaxis = new QValueAxis();

    source->chart->setAxisX(source->categoryaxis);
    source->chart->setAxisY(source->valueaxis);

    source->stackerbarseries->attachAxis(source->categoryaxis);
    source->stackerbarseries->attachAxis(source->valueaxis);
    source->scatterSeries->attachAxis(source->categoryaxis);
    source->scatterSeries->attachAxis(source->valueaxis);
    ui->main_layout->addWidget(source->chart_view);

//    if(source->data_name == tr("outputlist")){
//        connect(source->stackerbarseries,SIGNAL(clicked(int,QBarSet*)),this,SLOT(output_list_bar_click(int,QBarSet*)));

//    }

}

void production_machine_level2_widget::maker_bar_set(QBarSet *source_barset,QStringList date_query_list
                                                     ,QString out_type,QString qty_type)
{
    QSqlQuery query(my_localdb);
    QString query_txt;
    quint64 total_count = 0;
    for(int i=0;i<date_query_list.count();i++){
        if(qty_type == "wafer"){
            query_txt = QString("select SUM(WaferQty) as qty from ship_output_view where %1 AND %2 ")
                    .arg(out_type).arg(date_query_list.at(i));
            query.exec(query_txt);
            qDebug()<<query_txt;
            if(query.next()){
                source_barset->append(query.value("qty").toReal());
                total_count += source_barset->at(i);
            }
        }else if(qty_type =="chip" ){
            query_txt = QString("select SUM(ChipQty) as qty from ship_output_view where %1 AND %2 ")
                    .arg(out_type).arg(date_query_list.at(i));
            query.exec(query_txt);
            qDebug()<<query_txt;
        }

        if(query.next()){
            source_barset->append(query.value("qty").toReal());
            total_count += source_barset->at(i);
        }

    }
    source_barset->insert(0,total_count);
    source_barset->setProperty("total_count",total_count);
}

void production_machine_level2_widget::maker_bar_set_cheked(QBarSet *source_barset, QStringList date_query_list, QString Lot_type_query, QString out_type, QString qty_type)
{
    QSqlQuery query(my_mesdb);
     QString query_txt;
     quint64 total_count = 0;
     for(int i=0;i<date_query_list.count();i++){
         query_txt = QString("select SUM(qty) as qty from Production_app_summary where data_source IN ('%2') AND %3 AND %4 AND %5 AND %6 ")
                 .arg(source->data_name).arg(Lot_type_query).arg(out_type).arg(qty_type).arg(date_query_list.at(i));
         query.exec(query_txt);
         qDebug()<<query_txt;
         if(query.next()){
             source_barset->append(query.value("qty").toReal());
             total_count += source_barset->at(i);
         }
     }
     source_barset->insert(0,total_count);
     source_barset->setProperty("total_count",total_count);

}
QString production_machine_level2_widget::return_item(QString data)
{
    QStringList list =  data.split("=");
    QString data1 =list.at(1);
    data1 = data1.remove(0,1);
    return data1;
}

production_machine_level2_widget::~production_machine_level2_widget()
{
    delete ui;
}

void production_machine_level2_widget::on_zoomreset_btn_clicked()
{
    source->chart->zoomReset();
}

void production_machine_level2_widget::on_label_togle_btn_clicked()
{
    if(source->stackerbarseries->isLabelsVisible()){
        source->stackerbarseries->setLabelsVisible(false);
    }else {
        source->stackerbarseries->setLabelsVisible(true);
    }
}

void production_machine_level2_widget::on_total_label_togle_btn_clicked()
{
    if(source->scatterSeries->pointLabelsVisible()){
        source->scatterSeries->setPointLabelsVisible(false);
    }else {
        source->scatterSeries->setPointLabelsVisible(true);
    }
}

void production_machine_level2_widget::output_list_bar_click(int index, QBarSet *barset)
{
    QDate start_time = date_struct_list.at(index)->start_date;
    QDate end_datetime = date_struct_list.at(index)->end_date;

    production_machine_level2_popup *popup = new production_machine_level2_popup(start_time,end_datetime,popup_Lot_type_query,my_mesdb);
    popup->show();

}


date_struct::date_struct(QDate start_date, QDate end_date)
{
    this->start_date = start_date;
    this->end_date =end_date;
}


