#include "production_main.h"
#include "ui_production_main.h"

production_main::production_main(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::production_main)
{
    ui->setupUi(this);
    this->showMaximized();


    QString mydb_name = QString("MY_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    QString configini_str = qApp->applicationDirPath()+"/server.ini";
    QSettings settings(configini_str,QSettings::IniFormat);
    settings.beginGroup("setverset");
    QString server_ip = settings.value("serverip").toString();
    QString db_port = settings.value("dbport").toString();
    settings.endGroup();

    QProcess process;
    process.setWorkingDirectory(qApp->applicationDirPath());
    process.start("RegAsm.exe NS_Core_Com.dll");
    process.waitForFinished();
    QString output(process.readAllStandardOutput());
    qDebug()<<output;


    NS_core = new NS_Core_Com::NS_Main();
    qDebug()<<NS_core->init_NS_Core();

    if(!my_mesdb.contains(mydb_name)){
       my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
       my_mesdb.setHostName("10.20.10.101");
       my_mesdb.setPort(3306);
       my_mesdb.setUserName("EIS");
       my_mesdb.setPassword("wisolfab!");
       my_mesdb.setDatabaseName("fab");
    }else {
       my_mesdb = QSqlDatabase::database(mydb_name);
    }
    if(!my_mesdb.isOpen()){
        if(!my_mesdb.open()){
             qDebug()<<my_mesdb.lastError().text();
        }
    }
    QString mydb_name2 = QString("MY_MESDB2_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    if(!my_mesdb2.contains(mydb_name2)){
       my_mesdb2 = QSqlDatabase::addDatabase("QMYSQL",mydb_name2);
       my_mesdb2.setHostName("10.20.10.101");
       my_mesdb2.setPort(3306);
       my_mesdb2.setUserName("EIS");
       my_mesdb2.setPassword("wisolfab!");
       my_mesdb2.setDatabaseName("fab");
    }else {
       my_mesdb2 = QSqlDatabase::database(mydb_name2);
    }
    if(!my_mesdb2.isOpen()){
        if(!my_mesdb2.open()){
             qDebug()<<my_mesdb2.lastError().text();
        }
    }


    QString msdb_name = QString("MS_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    ms_mesdb = QSqlDatabase::addDatabase("QODBC",msdb_name);
    QString serverinfo = QString("DRIVER={SQL Server};Server=%1;Database=%2;Uid=%3;Port=1433;Pwd=%4").arg(DBMESSERVERIP).arg(DBMESNAME).arg(DBMESUSERNAME).arg(DBMESPW);
    ms_mesdb.setDatabaseName(serverinfo);
    if(!ms_mesdb.open()){
        qDebug()<<"fasle";
        qDebug()<<ms_mesdb.lastError().text();
    }else {
        qDebug()<<"open";
    }


    depostion_tab_init_bool = false;
    photo_tab_init_bool = false;
    eatching_tab_init_bool = false;
    probe_tab_init_bool = false;
    trimmer_tab_init_bool = false;

    QSettings production_settings("production_app.ini",QSettings::IniFormat);
    production_settings.beginGroup("settings");
    last_env = production_settings.value("last_env").toString();
    last_index = production_settings.value("last_index").toInt();
    if(last_env.length() == 0){
        last_env = tr("basic");
        production_settings.setValue("last_env",tr("basic"));
    }
    production_settings.endGroup();

    init_env();
    total_check_init_tab();
    output_list_init_tab();

}

production_main::~production_main()
{
    delete ui;
}

void production_main::chart_maker1(chart_maker_source source)
{
    if(source.chart_layout->count()>0){
         QChartView *chartview = qobject_cast<QChartView *> (source.chart_layout->itemAt(0)->widget());

         source.chart_layout->removeWidget(source.chart_layout->itemAt(0)->widget());
         chartview->deleteLater();
     }

     QString machine_code_list;
     foreach(QString str,source.machinecodelist){
         machine_code_list = machine_code_list.append("'"+str+"'");
         machine_code_list = machine_code_list.append(",");
     }
     QString querytxt;
     if(source.search_mode=="SiO2,trimmer"){
         machine_code_list= machine_code_list.remove(machine_code_list.length()-1,1);
         querytxt = QString("select SUM(qty) AS `qty`,machine_code,MAX(machine_name) as `machine_name`,operation_name from machine_output_summary "
                                    "where summary_date between '%1' AND '%2' AND machine_code IN (%3) AND operation_name IN ('%4','%5') "
                                    "group by machine_code,operation_name")
                                    .arg(source.search_start_datetime.toString("yyyyMMdd"))
                                    .arg(source.search_end_datetime.toString("yyyyMMdd"))
                                    .arg(machine_code_list)
                                    .arg(tr("SiO2 Trimmer"))
                                    .arg(tr("trimmer"));
     }else {
         machine_code_list= machine_code_list.remove(machine_code_list.length()-1,1);
         querytxt = QString("select SUM(qty) AS `qty`,machine_code,MAX(machine_name) as `machine_name` from machine_output_summary "
                                    "where summary_date between '%1' AND '%2' AND machine_code IN (%3)  group by machine_code")
                                    .arg(source.search_start_datetime.toString("yyyyMMdd"))
                                    .arg(source.search_end_datetime.toString("yyyyMMdd"))
                                    .arg(machine_code_list);
         qDebug()<<querytxt;
     }


     QSqlQuery query(my_mesdb2);
     query.exec(querytxt);
     QBarSeries *bar_series;
//     QBarSet *input_qty_bar_set;
     QBarSet *output_qty_bar_set;
     QScatterSeries *goal_count_bar;
     QBarCategoryAxis *x_categoty;
     QValueAxis *y_axis;
     bar_series = new QBarSeries();
     bar_series->setLabelsVisible(true);
//     input_qty_bar_set = new QBarSet("input_qty");
     output_qty_bar_set = new QBarSet(tr("output_qty"));
//     input_qty_bar_set->setLabelColor("black");
     output_qty_bar_set->setLabelColor("black");
     goal_count_bar = new QScatterSeries();
     goal_count_bar->setName(tr("goal_count"));
     goal_count_bar->setMarkerShape(QScatterSeries::MarkerShapeCircle);
     goal_count_bar->setColor("green");
     goal_count_bar->setMarkerSize(10);
     goal_count_bar->setPointLabelsVisible(true);
     goal_count_bar->setPointLabelsClipping(true);
     goal_count_bar->setPointLabelsFormat("@comma_long_value");
     x_categoty = new QBarCategoryAxis();
     y_axis = new QValueAxis();
 //    bar_series->append(input_qty_bar_set);
     bar_series->append(output_qty_bar_set);
     QList<QVariant> redbar_list;
     QStringList catagorylist;
     QStringList machinenamelists;
     int scatter_i = 0;
     int max_goal_count = 0;
     int inputsum_qty = 0;
     int outputsum_qty = 0;
     QVector<int> goal_point_list;
     QStringList operation_names;

     while(query.next()){
              if(source.search_mode=="SiO2,trimmer"){
                   catagorylist<<query.value("machine_name").toString()+"/"+query.value("operation_name").toString();
                   machinenamelists<<query.value("machine_name").toString();
                   operation_names<<query.value("operation_name").toString();
              }else {
                   catagorylist<<query.value("machine_name").toString();
                   machinenamelists<<query.value("machine_name").toString();
              }

//             input_qty_bar_set->append(query.value("INPUTSUM").toReal());
//             inputsum_qty +=  query.value("INPUTSUM").toInt();
             output_qty_bar_set->append(query.value("qty").toReal());
             outputsum_qty += query.value("qty").toInt();
//             if(query.value("INPUTSUM").toInt()!= query.value("qty").toInt()){
//                 redbar_list.append(output_qty_bar_set->count()-1);
//             }
             QSqlQuery query_1(my_mesdb);
             if(source.search_mode=="SiO2,trimmer"){
                 query_1.exec(QString("select goal_count from Production_app_machine_view_table where machine_name = '%1' AND operation_name = '%2' AND env_name ='%3' ")
                              .arg(query.value("machine_name").toString()).arg(query.value("operation_name").toString()).arg(ui->CB_env->currentText()));
             }else {
                 query_1.exec(QString("select goal_count from Production_app_machine_view_table where machine_name = '%1' AND env_name ='%2'")
                              .arg(query.value("machine_name").toString()).arg(ui->CB_env->currentText()));
                 qDebug()<<query_1.lastQuery();
             }
             if(query_1.next()){
                 QDateTime start_date = source.search_start_datetime;
                 QDateTime end_date = source.search_end_datetime;
                 int day_count = start_date.daysTo(end_date)+1;
                 int goal_count = day_count * query_1.value("goal_count").toInt();
                 if(max_goal_count < goal_count){
                     max_goal_count = goal_count;
                 }
                 goal_point_list.append(goal_count);
             }
     }


     int process_goal_count = 0;
     QSqlQuery query1(my_mesdb);
     QString querytxt1 = QString("select goal_count from Production_app_goal_count_list where part = '%1' AND process = '%2' AND env_name = '%3'")
             .arg(source.part).arg(source.process).arg(ui->CB_env->currentText());
     query1.exec(querytxt1);
     if(query1.next()){
         QDateTime start_date = source.search_start_datetime;
         QDateTime end_date = source.search_end_datetime;
         int day_count = start_date.daysTo(end_date)+1;
         process_goal_count = query1.value("goal_count").toInt();

         process_goal_count = process_goal_count *day_count;
         if(process_goal_count > max_goal_count){
             max_goal_count = process_goal_count;
         }
         goal_point_list.insert(0,process_goal_count);
         catagorylist.insert(0,"total");
         machinenamelists.insert(0,"total");
         operation_names.insert(0,"total");
//         input_qty_bar_set->insert(0,inputsum_qty);
         output_qty_bar_set->insert(0,outputsum_qty);
         for(int i=0;i<redbar_list.count();i++){
             int replace_value = redbar_list.at(i).toInt()+1;
             redbar_list.replace(i,QVariant(replace_value));
         }
//         if(inputsum_qty != outputsum_qty){
//             redbar_list.append(0);
//         }
     }
     for(int i=0;i<goal_point_list.count();i++){
         goal_count_bar->append(i,goal_point_list.at(i));
     }

     x_categoty->append(catagorylist);
 //    output_qty_bar_set->setProperty("redbar",redbar_list);



//     input_qty_bar_set->setProperty("search_start_time",source.search_start_datetime);
//     input_qty_bar_set->setProperty("search_end_time",source.search_end_datetime);
//     input_qty_bar_set->setProperty("categories",x_categoty->categories());

//     input_qty_bar_set->setProperty("part",source.part);
//     input_qty_bar_set->setProperty("process",source.process);
     output_qty_bar_set->setProperty("search_start_time",source.search_start_datetime);
     output_qty_bar_set->setProperty("search_end_time",source.search_end_datetime);
     output_qty_bar_set->setProperty("categories",x_categoty->categories());
     output_qty_bar_set->setProperty("machinenamelists",machinenamelists);
     output_qty_bar_set->setProperty("search_mode",source.search_mode);
     output_qty_bar_set->setProperty("operation_names",operation_names);

     output_qty_bar_set->setProperty("part",source.part);
     output_qty_bar_set->setProperty("process",source.process);
//     connect(input_qty_bar_set,SIGNAL(clicked(int)),this,SLOT(level1_barset_click(int)));
     connect(output_qty_bar_set,SIGNAL(clicked(int)),this,SLOT(level1_barset_click(int)));

     source.main_chart->setAxisX(x_categoty);
     source.main_chart->setAxisY(y_axis);

     source.main_chart->addSeries(bar_series);
     source.main_chart->addSeries(goal_count_bar);

     y_axis->setRange(0,max_goal_count+(max_goal_count*0.3));

     bar_series->attachAxis(x_categoty);
     bar_series->attachAxis(y_axis);

     goal_count_bar->attachAxis(x_categoty);
     goal_count_bar->attachAxis(y_axis);

     if(machine_code_list.count()>0){
         source.chart_layout->addWidget(source.chart_view);
     }
}

void production_main::deposition_init_tab()
{
    //시간 초기화.
    QDateTime current_datetime_start = QDateTime::currentDateTime().addDays(-1);
    current_datetime_start.setTime(QTime(8,0,0));
    QDateTime current_datetime_end = QDateTime::currentDateTime().addDays(-1);
    current_datetime_end.setTime(QTime(7,59,59));

    //스퍼터 초기화.
    ui->sputtering_start_datetime->setDateTime(current_datetime_start);
    ui->sputtering_end_datetime->setDateTime(current_datetime_end);
    on_sputtering_search_btn_clicked();

    //증착기.
    ui->deposition_start_datetime->setDateTime(current_datetime_start);
    ui->deposition_end_datetime->setDateTime(current_datetime_end);
    on_deposition_search_btn_clicked();

    //리프트오프.
    ui->lift_off_start_datetime->setDateTime(current_datetime_start);
    ui->lift_off_end_datetime->setDateTime(current_datetime_end);
    on_lift_off_search_btn_clicked();

}

void production_main::photo_init_tab()
{
    //시간 초기화.
    QDateTime current_datetime_start = QDateTime::currentDateTime().addDays(-1);
    current_datetime_start.setTime(QTime(8,0,0));
    QDateTime current_datetime_end = QDateTime::currentDateTime().addDays(-1);
    current_datetime_end.setTime(QTime(7,59,59));
    //포토 초기화.
    ui->photo_krf_start_datetime->setDateTime(current_datetime_start);
    ui->photo_krf_end_datetime->setDateTime(current_datetime_end);
    on_photo_krf_search_btn_clicked();

    ui->photo_ILINE_start_datetime->setDateTime(current_datetime_start);
    ui->photo_ILINE_end_datetime->setDateTime(current_datetime_end);
    on_photo_ILINE_search_btn_clicked();

    ui->photo_coating_start_datetime->setDateTime(current_datetime_start);
    ui->photo_coating_end_datetime->setDateTime(current_datetime_end);
    on_photo_coating_search_btn_clicked();

    ui->photo_dev_start_datetime->setDateTime(current_datetime_start);
    ui->photo_dev_end_datetime->setDateTime(current_datetime_end);
    on_photo_dev_search_btn_clicked();
}

void production_main::eatching_list_init_tab()
{
    //시간 초기화.
    QDateTime current_datetime_start = QDateTime::currentDateTime().addDays(-1);
    current_datetime_start.setTime(QTime(8,0,0));
    QDateTime current_datetime_end = QDateTime::currentDateTime().addDays(-1);
    current_datetime_end.setTime(QTime(7,59,59));
    //에칭 초기화.
    ui->eatching_etcher_start_datetime->setDateTime(current_datetime_start);
    ui->eatching_etcher_end_datetime->setDateTime(current_datetime_end);
    on_eatching_etcher_search_btn_clicked();

    ui->protective_D_start_datetime->setDateTime(current_datetime_start);
    ui->protective_D_end_datetime->setDateTime(current_datetime_end);
    on_protective_D_search_btn_clicked();

    ui->protective_E_start_datetime->setDateTime(current_datetime_start);
    ui->protective_E_end_datetime->setDateTime(current_datetime_end);
    on_protective_E_search_btn_clicked();

}
void production_main::probe_list_init_tab()
{
    //시간 초기화.
    QDateTime current_datetime_start = QDateTime::currentDateTime().addDays(-1);
    current_datetime_start.setTime(QTime(8,0,0));
    QDateTime current_datetime_end = QDateTime::currentDateTime().addDays(-1);
    current_datetime_end.setTime(QTime(7,59,59));

    ui->probe_last_start_datetime->setDateTime(current_datetime_start);
    ui->probe_last_end_datetime->setDateTime(current_datetime_end);
    on_probe_last_search_btn_clicked();

    ui->probe_total_check_start_datetime->setDateTime(current_datetime_start);
    ui->probe_total_check_end_datetime->setDateTime(current_datetime_end);
    on_probe_total_check_search_btn_clicked();
}
void production_main::trimmer_list_init_tab()
{
    //시간 초기화.
    QDateTime current_datetime_start = QDateTime::currentDateTime().addDays(-1);
    current_datetime_start.setTime(QTime(8,0,0));
    QDateTime current_datetime_end = QDateTime::currentDateTime().addDays(-1);
    current_datetime_end.setTime(QTime(7,59,59));
    ui->trimmer1_start_datetime->setDateTime(current_datetime_start);
    ui->trimmer1_end_datetime->setDateTime(current_datetime_end);
    on_trimmer1_search_btn_clicked();
    ui->trimmer2_start_datetime->setDateTime(current_datetime_start);
    ui->trimmer2_end_datetime->setDateTime(current_datetime_end);
    on_trimmer2_search_btn_clicked();

}



void production_main::level1_barset_click(int index)
{
    qDebug()<<index;
    QBarSet *sender_obj =  qobject_cast<QBarSet *>(this->sender());
    QString machine_name = sender_obj->property("machinenamelists").toStringList().at(index);
    QString search_mode = sender_obj->property("search_mode").toString();
    QDateTime start_date_time =  sender_obj->property("search_start_time").toDateTime();
    QDateTime start_end_time =  sender_obj->property("search_end_time").toDateTime();
    QString operation_name = "";
    if(search_mode == "SiO2,trimmer"){
        operation_name = sender_obj->property("operation_names").toStringList().at(index);
    }


    QString part = sender_obj->property("part").toString();
    QString process = sender_obj->property("process").toString();
    bool part_process = false;
    if(index == 0){
        part_process = true;
    }

    production_machine_level1_view *popup = new production_machine_level1_view(my_mesdb,ms_mesdb,my_mesdb2,machine_name,start_date_time
                                                                                   ,start_end_time,part_process,part,process,search_mode,operation_name,ui->CB_env->currentText());
    popup->show();

}

void production_main::on_sputtering_machine_table_btn_clicked()
{
    Production_machine_table_popup *popup  = new Production_machine_table_popup(my_mesdb,sputtering_source.part,sputtering_source.process,ui->CB_env->currentText());
    popup->show();
    on_sputtering_goal_count_editingFinished();
}
void production_main::on_deposition_machine_table_btn_clicked()
{
    Production_machine_table_popup *popup  = new Production_machine_table_popup(my_mesdb,deposition_source.part,deposition_source.process,ui->CB_env->currentText());
    popup->show();
    on_deposition_goal_count_editingFinished();
}
void production_main::on_lift_off_machine_table_btn_clicked()
{
    Production_machine_table_popup *popup  = new Production_machine_table_popup(my_mesdb,lift_off_source.part,lift_off_source.process,ui->CB_env->currentText());
    popup->show();
    on_lift_off_goal_count_editingFinished();

}
void production_main::on_photo_krf_machine_table_btn_clicked()
{
    Production_machine_table_popup *popup  = new Production_machine_table_popup(my_mesdb,photo_krf_source.part,photo_krf_source.process,ui->CB_env->currentText());
    popup->show();
    on_photo_krf_goal_count_editingFinished();
}

void production_main::on_photo_ILINE_machine_table_btn_clicked()
{
    Production_machine_table_popup *popup  = new Production_machine_table_popup(my_mesdb,photo_ILINE_source.part,photo_ILINE_source.process,ui->CB_env->currentText());
    popup->show();
    on_photo_ILINE_goal_count_editingFinished();
}
void production_main::on_photo_coating_machine_table_btn_clicked()
{
    Production_machine_table_popup *popup  = new Production_machine_table_popup(my_mesdb,photo_coating_source.part,photo_coating_source.process,ui->CB_env->currentText());
    popup->show();
    on_photo_coating_goal_count_editingFinished();
}
void production_main::on_photo_dev_machine_table_btn_clicked()
{
    Production_machine_table_popup *popup  = new Production_machine_table_popup(my_mesdb,photo_dev_source.part,photo_dev_source.process,ui->CB_env->currentText());
    popup->show();
    on_photo_dev_goal_count_editingFinished();
}
void production_main::on_eatching_etcher_machine_table_btn_clicked()
{
    Production_machine_table_popup *popup  = new Production_machine_table_popup(my_mesdb,eatching_etcher_source.part,eatching_etcher_source.process,ui->CB_env->currentText());
    popup->show();
    on_eatching_etcher_goal_count_editingFinished();
}

void production_main::on_protective_D_machine_table_btn_clicked()
{
    Production_machine_table_popup *popup  = new Production_machine_table_popup(my_mesdb,protective_D_source.part,protective_D_source.process,ui->CB_env->currentText());
    popup->show();
    on_protective_D_goal_count_editingFinished();
}
void production_main::on_protective_E_machine_table_btn_clicked()
{
    Production_machine_table_popup *popup  = new Production_machine_table_popup(my_mesdb,protective_E_source.part,protective_E_source.process,ui->CB_env->currentText());
    popup->show();
    on_protective_E_goal_count_editingFinished();
}
void production_main::on_probe_last_machine_table_btn_clicked()
{
    Production_machine_table_popup *popup  = new Production_machine_table_popup(my_mesdb,probe_last_source.part,probe_last_source.process,ui->CB_env->currentText());
    popup->show();
    on_probe_last_goal_count_editingFinished();
}
void production_main::on_probe_total_check_machine_table_btn_clicked()
{
    Production_machine_table_popup *popup  = new Production_machine_table_popup(my_mesdb,probe_total_check_source.part,probe_total_check_source.process,ui->CB_env->currentText());
    popup->show();
    on_probe_total_check_goal_count_editingFinished();
}
void production_main::on_trimmer1_machine_table_btn_clicked()
{
    Production_machine_table_popup *popup  = new Production_machine_table_popup(my_mesdb,trimmer1_source.part,trimmer1_source.process,ui->CB_env->currentText());
    popup->show();
    on_trimmer1_goal_count_editingFinished();

}
void production_main::on_trimmer2_machine_table_btn_clicked()
{
    Production_machine_table_popup *popup  = new Production_machine_table_popup(my_mesdb,trimmer2_source.part,trimmer2_source.process,trimmer2_source.search_mode,ui->CB_env->currentText());
    popup->show();
    on_trimmer2_goal_count_editingFinished();
}


chart_maker_source::chart_maker_source()
{
    chart_layout = 0;
    main_chart = 0;
    chart_view = 0;
}
void production_main::on_sputtering_search_btn_clicked()
{

    sputtering_source.search_start_datetime = ui->sputtering_start_datetime->dateTime();
    sputtering_source.search_end_datetime = ui->sputtering_end_datetime->dateTime();

    sputtering_source.part = tr("deposition");
    sputtering_source.process = tr("Sputtering");
    QSqlQuery query(my_mesdb);

    QString query_txt = QString("select * from Production_app_goal_count_list where process = '%1' AND env_name = '%2' ")
            .arg(sputtering_source.process).arg(ui->CB_env->currentText());

    query.exec(query_txt);
    if(query.next()){
        ui->sputtering_goal_count->setValue(query.value("goal_count").toInt());
    }else {
        ui->sputtering_goal_count->setValue(0);
    }

    query_txt = QString("select * from `Production_app_machine_view_table` where `machine_part` = '%1' AND `use` = '2' AND env_name = '%2' order by machine_name asc")
                                .arg(sputtering_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    sputtering_source.machinecodelist.clear();
    sputtering_source.machinenamelist.clear();
    while(query.next()){
         sputtering_source.machinecodelist<<query.value("machine_code").toString();
         sputtering_source.machinenamelist<<query.value("machine_name").toString();
    }
    sputtering_source.chart_layout = ui->Sputtering_chart_layout;

    sputtering_source.main_chart = new production_main_chart1();
    sputtering_source.chart_view = new production_main_chart_view1(sputtering_source.main_chart);
    ui->sputtering_out_view_btn->setProperty("child",QVariant::fromValue(ui->Sputtering_main));
    ui->sputtering_out_view_btn->setProperty("parent_splitter",QVariant::fromValue(ui->depostion_splitter));
    disconnect(ui->sputtering_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    connect(ui->sputtering_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    chart_maker1(sputtering_source);
}

void production_main::on_deposition_search_btn_clicked()
{
    deposition_source.search_start_datetime = ui->deposition_start_datetime->dateTime();
    deposition_source.search_end_datetime = ui->deposition_end_datetime->dateTime();


    deposition_source.part = tr("deposition");
    deposition_source.process = tr("vacuum");


    QSqlQuery query(my_mesdb);
    QString query_txt = QString("select * from Production_app_goal_count_list where process = '%1' AND env_name = '%2'")
            .arg(deposition_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    if(query.next()){
        ui->deposition_goal_count->setValue(query.value("goal_count").toInt());
    }else {
        ui->deposition_goal_count->setValue(0);
    }

    query_txt = QString("select * from `Production_app_machine_view_table` where `machine_part` = '%1' AND `use` = '2' AND env_name = '%2' order by machine_name asc")
                                .arg(deposition_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    deposition_source.machinecodelist.clear();
    deposition_source.machinenamelist.clear();
    while(query.next()){
         deposition_source.machinecodelist<<query.value("machine_code").toString();
         deposition_source.machinenamelist<<query.value("machine_name").toString();
    }
    deposition_source.chart_layout = ui->deposition_chart_layout;
    deposition_source.main_chart = new production_main_chart1();
    deposition_source.chart_view = new production_main_chart_view1(deposition_source.main_chart);

    ui->depostion_out_view_btn->setProperty("child",QVariant::fromValue(ui->Vacuum_main));
    ui->depostion_out_view_btn->setProperty("parent_splitter",QVariant::fromValue(ui->depostion_splitter));
    disconnect(ui->depostion_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    connect(ui->depostion_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));

    chart_maker1(deposition_source);
}
void production_main::on_lift_off_search_btn_clicked()
{

    lift_off_source.search_start_datetime = ui->lift_off_start_datetime->dateTime();
    lift_off_source.search_end_datetime = ui->lift_off_end_datetime->dateTime();


    lift_off_source.part = tr("deposition");
    lift_off_source.process = tr("lift_off");

    QSqlQuery query(my_mesdb);
    QString query_txt = QString("select * from Production_app_goal_count_list where process = '%1' AND env_name = '%2'")
            .arg(lift_off_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    if(query.next()){
        ui->lift_off_goal_count->setValue(query.value("goal_count").toInt());
    }else {
        ui->lift_off_goal_count->setValue(0);

    }
    query_txt = QString("select * from `Production_app_machine_view_table` where `machine_part` = '%1' AND `use` = '2' AND env_name = '%2' order by machine_name asc")
                                .arg(lift_off_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    lift_off_source.machinecodelist.clear();
    lift_off_source.machinenamelist.clear();
    while(query.next()){
         lift_off_source.machinecodelist<<query.value("machine_code").toString();
         lift_off_source.machinenamelist<<query.value("machine_name").toString();
    }
    lift_off_source.chart_layout = ui->lift_off_chart_layout;
    lift_off_source.main_chart = new production_main_chart1();
    lift_off_source.chart_view = new production_main_chart_view1(lift_off_source.main_chart);
    ui->lift_off_out_view_btn->setProperty("child",QVariant::fromValue(ui->liftoff_main));
    ui->lift_off_out_view_btn->setProperty("parent_splitter",QVariant::fromValue(ui->depostion_splitter));
    disconnect(ui->lift_off_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    connect(ui->lift_off_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    chart_maker1(lift_off_source);
}

void production_main::on_photo_krf_search_btn_clicked()
{
    photo_krf_source.search_start_datetime = ui->photo_krf_start_datetime->dateTime();
    photo_krf_source.search_end_datetime = ui->photo_krf_end_datetime->dateTime();

    photo_krf_source.part = tr("photo");
    photo_krf_source.process = tr("krf");

    QSqlQuery query(my_mesdb);
    QString query_txt = QString("select * from Production_app_goal_count_list where process = '%1' AND env_name = '%2' ")
            .arg(photo_krf_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    if(query.next()){
        ui->photo_krf_goal_count->setValue(query.value("goal_count").toInt());
    }else {
        ui->photo_krf_goal_count->setValue(0);
    }
    query_txt = QString("select * from `Production_app_machine_view_table` where `machine_part` = '%1' AND `use` = '2' AND env_name='%2' order by machine_name asc")
                                .arg(tr("krf")).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    photo_krf_source.machinecodelist.clear();
    photo_krf_source.machinenamelist.clear();
    while(query.next()){
         photo_krf_source.machinecodelist<<query.value("machine_code").toString();
         photo_krf_source.machinenamelist<<query.value("machine_name").toString();
    }
    photo_krf_source.chart_layout = ui->photo_krf_chart_layout;
    photo_krf_source.main_chart = new production_main_chart1();
    photo_krf_source.chart_view = new production_main_chart_view1(photo_krf_source.main_chart);
    ui->photo_krf_out_view_btn->setProperty("child",QVariant::fromValue(ui->photo_krf_main));
    ui->photo_krf_out_view_btn->setProperty("parent_splitter",QVariant::fromValue(ui->photo_spliter));
    disconnect(ui->photo_krf_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    connect(ui->photo_krf_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    chart_maker1(photo_krf_source);
}

void production_main::on_photo_ILINE_search_btn_clicked()
{
    photo_ILINE_source.search_start_datetime = ui->photo_ILINE_start_datetime->dateTime();
    photo_ILINE_source.search_end_datetime = ui->photo_ILINE_end_datetime->dateTime();

    photo_ILINE_source.part = tr("photo");
    photo_ILINE_source.process = tr("ILINE");

    QSqlQuery query(my_mesdb);
    QString query_txt = QString("select * from Production_app_goal_count_list where process = '%1' AND env_name = '%2'")
            .arg(photo_ILINE_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    if(query.next()){
        ui->photo_ILINE_goal_count->setValue(query.value("goal_count").toInt());
    }else {
        ui->photo_ILINE_goal_count->setValue(0);
    }
    query_txt = QString("select * from `Production_app_machine_view_table` where `machine_part` = '%1' AND `use` = '2' AND env_name= '%2' order by machine_name asc")
                                .arg(tr("ILINE")).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    photo_ILINE_source.machinecodelist.clear();
    photo_ILINE_source.machinenamelist.clear();
    while(query.next()){
         photo_ILINE_source.machinecodelist<<query.value("machine_code").toString();
         photo_ILINE_source.machinenamelist<<query.value("machine_name").toString();
    }
    photo_ILINE_source.chart_layout = ui->photo_ILINE_chart_layout;
    photo_ILINE_source.main_chart = new production_main_chart1();
    photo_ILINE_source.chart_view = new production_main_chart_view1(photo_ILINE_source.main_chart);
    ui->photo_ILINE_out_view_btn->setProperty("child",QVariant::fromValue(ui->photo_ILINE_main));
    ui->photo_ILINE_out_view_btn->setProperty("parent_splitter",QVariant::fromValue(ui->photo_spliter));
    disconnect(ui->photo_ILINE_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    connect(ui->photo_ILINE_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    chart_maker1(photo_ILINE_source);
}

void production_main::on_photo_coating_search_btn_clicked()
{
    photo_coating_source.search_start_datetime = ui->photo_coating_start_datetime->dateTime();
    photo_coating_source.search_end_datetime = ui->photo_coating_end_datetime->dateTime();

    photo_coating_source.part = tr("photo");
    photo_coating_source.process = tr("coating");

    QSqlQuery query(my_mesdb);
    QString query_txt = QString("select * from Production_app_goal_count_list where process = '%1' AND env_name= '%2' ")
            .arg(photo_coating_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    if(query.next()){
        ui->photo_coating_goal_count->setValue(query.value("goal_count").toInt());
    }else {
        ui->photo_coating_goal_count->setValue(0);
    }
    query_txt = QString("select * from `Production_app_machine_view_table` where `machine_part` = '%1' AND `use` = '2' AND env_name = '%2' order by machine_name asc")
                                .arg(tr("coating")).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    photo_coating_source.machinecodelist.clear();
    photo_coating_source.machinenamelist.clear();
    while(query.next()){
         photo_coating_source.machinecodelist<<query.value("machine_code").toString();
         photo_coating_source.machinenamelist<<query.value("machine_name").toString();
    }
    photo_coating_source.chart_layout = ui->photo_coating_chart_layout;
    photo_coating_source.main_chart = new production_main_chart1();
    photo_coating_source.chart_view = new production_main_chart_view1(photo_coating_source.main_chart);
    ui->photo_coating_out_view_btn->setProperty("child",QVariant::fromValue(ui->photo_coating_main));
    ui->photo_coating_out_view_btn->setProperty("parent_splitter",QVariant::fromValue(ui->photo_spliter));
    disconnect(ui->photo_coating_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    connect(ui->photo_coating_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    chart_maker1(photo_coating_source);
}

void production_main::on_photo_dev_search_btn_clicked()
{
    photo_dev_source.search_start_datetime = ui->photo_dev_start_datetime->dateTime();
    photo_dev_source.search_end_datetime = ui->photo_dev_end_datetime->dateTime();

    photo_dev_source.part = tr("photo");
    photo_dev_source.process = tr("dev");

    QSqlQuery query(my_mesdb);
    QString query_txt = QString("select * from Production_app_goal_count_list where process = '%1' AND env_name = '%2' ")
            .arg(photo_dev_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    if(query.next()){
        ui->photo_dev_goal_count->setValue(query.value("goal_count").toInt());
    }else {
        ui->photo_dev_goal_count->setValue(0);
    }
    query_txt = QString("select * from `Production_app_machine_view_table` where `machine_part` = '%1' AND `use` = '2' AND env_name= '%2' order by machine_name asc")
                                .arg(tr("dev")).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    photo_dev_source.machinecodelist.clear();
    photo_dev_source.machinenamelist.clear();
    while(query.next()){
         photo_dev_source.machinecodelist<<query.value("machine_code").toString();
         photo_dev_source.machinenamelist<<query.value("machine_name").toString();
    }
    photo_dev_source.chart_layout = ui->photo_dev_chart_layout;
    photo_dev_source.main_chart = new production_main_chart1();
    photo_dev_source.chart_view = new production_main_chart_view1(photo_dev_source.main_chart);
    ui->photo_dev_out_view_btn->setProperty("child",QVariant::fromValue(ui->photo_dev_main));
    ui->photo_dev_out_view_btn->setProperty("parent_splitter",QVariant::fromValue(ui->photo_spliter));
    disconnect(ui->photo_dev_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    connect(ui->photo_dev_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    chart_maker1(photo_dev_source);
}
void production_main::on_eatching_etcher_search_btn_clicked()
{
    eatching_etcher_source.search_start_datetime = ui->eatching_etcher_start_datetime->dateTime();
    eatching_etcher_source.search_end_datetime = ui->eatching_etcher_end_datetime->dateTime();

    eatching_etcher_source.part = tr("eatching");
    eatching_etcher_source.process = tr("etcher");

    QSqlQuery query(my_mesdb);
    QString query_txt = QString("select * from Production_app_goal_count_list where process = '%1' AND env_name= '%2'")
            .arg(eatching_etcher_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    if(query.next()){
        ui->eatching_etcher_goal_count->setValue(query.value("goal_count").toInt());
    }else {
        ui->eatching_etcher_goal_count->setValue(0);
    }
    query_txt = QString("select * from `Production_app_machine_view_table` where `machine_part` = '%1' AND `use` = '2' AND env_name = '%2' order by machine_name asc")
                                .arg(tr("etcher")).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    eatching_etcher_source.machinecodelist.clear();
    eatching_etcher_source.machinenamelist.clear();
    while(query.next()){
         eatching_etcher_source.machinecodelist<<query.value("machine_code").toString();
         eatching_etcher_source.machinenamelist<<query.value("machine_name").toString();
    }
    eatching_etcher_source.chart_layout = ui->eatching_etcher_chart_layout;
    eatching_etcher_source.main_chart = new production_main_chart1();
    eatching_etcher_source.chart_view = new production_main_chart_view1(eatching_etcher_source.main_chart);
    ui->eatching_etcher_out_view_btn->setProperty("child",QVariant::fromValue(ui->eatching_etcher_main));
    ui->eatching_etcher_out_view_btn->setProperty("parent_splitter",QVariant::fromValue(ui->eatching_splitter));
    disconnect(ui->eatching_etcher_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    connect(ui->eatching_etcher_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    chart_maker1(eatching_etcher_source);
}
void production_main::on_protective_D_search_btn_clicked()
{
    protective_D_source.search_start_datetime = ui->protective_D_start_datetime->dateTime();
    protective_D_source.search_end_datetime = ui->protective_D_end_datetime->dateTime();

    protective_D_source.part = tr("eatching");
    protective_D_source.process = tr("protective_Depostion");

    QSqlQuery query(my_mesdb);
    QString query_txt = QString("select * from Production_app_goal_count_list where process = '%1' AND env_name= '%2' ")
            .arg(protective_D_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    if(query.next()){
        ui->protective_D_goal_count->setValue(query.value("goal_count").toInt());
    }else {
        ui->protective_D_goal_count->setValue(0);
    }
    query_txt = QString("select * from `Production_app_machine_view_table` where `machine_part` = '%1' AND `use` = '2' AND env_name = '%2' order by machine_name asc")
                                .arg(tr("protective_Depostion")).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    protective_D_source.machinecodelist.clear();
    protective_D_source.machinenamelist.clear();
    while(query.next()){
         protective_D_source.machinecodelist<<query.value("machine_code").toString();
         protective_D_source.machinenamelist<<query.value("machine_name").toString();
    }
    protective_D_source.chart_layout = ui->protective_D_chart_layout;
    protective_D_source.main_chart = new production_main_chart1();
    protective_D_source.chart_view = new production_main_chart_view1(protective_D_source.main_chart);
    ui->protective_D_out_view_btn->setProperty("child",QVariant::fromValue(ui->protective_D_main));
    ui->protective_D_out_view_btn->setProperty("parent_splitter",QVariant::fromValue(ui->eatching_splitter));
    disconnect(ui->protective_D_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    connect(ui->protective_D_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    chart_maker1(protective_D_source);
}
void production_main::on_protective_E_search_btn_clicked()
{
    protective_E_source.search_start_datetime = ui->protective_E_start_datetime->dateTime();
    protective_E_source.search_end_datetime = ui->protective_E_end_datetime->dateTime();

    protective_E_source.part = tr("eatching");
    protective_E_source.process = tr("protective_Eatching");

    QSqlQuery query(my_mesdb);
    QString query_txt = QString("select * from Production_app_goal_count_list where process = '%1' AND env_name= '%2' ")
            .arg(protective_E_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    if(query.next()){
        ui->protective_E_goal_count->setValue(query.value("goal_count").toInt());
    }else {
        ui->protective_E_goal_count->setValue(0);
    }
    query_txt = QString("select * from `Production_app_machine_view_table` where `machine_part` = '%1' AND `use` = '2' AND env_name='%2' order by machine_name asc")
                                .arg(tr("protective_Eatching")).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    protective_E_source.machinecodelist.clear();
    protective_E_source.machinenamelist.clear();
    while(query.next()){
         protective_E_source.machinecodelist<<query.value("machine_code").toString();
         protective_E_source.machinenamelist<<query.value("machine_name").toString();
    }
    protective_E_source.chart_layout = ui->protective_E_chart_layout;
    protective_E_source.main_chart = new production_main_chart1();
    protective_E_source.chart_view = new production_main_chart_view1(protective_E_source.main_chart);
    ui->protective_E_out_view_btn->setProperty("child",QVariant::fromValue(ui->protective_E_main));
    ui->protective_E_out_view_btn->setProperty("parent_splitter",QVariant::fromValue(ui->eatching_splitter));
    disconnect(ui->protective_E_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    connect(ui->protective_E_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    chart_maker1(protective_E_source);
}
void production_main::on_probe_last_search_btn_clicked()
{
    probe_last_source.search_start_datetime = ui->probe_last_start_datetime->dateTime();
    probe_last_source.search_end_datetime = ui->probe_last_end_datetime->dateTime();

    probe_last_source.part = tr("probe");
    probe_last_source.process = tr("probe_last");

    QSqlQuery query(my_mesdb);
    QString query_txt = QString("select * from Production_app_goal_count_list where process = '%1' AND env_name='%2' ")
            .arg(probe_last_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    if(query.next()){
        ui->probe_last_goal_count->setValue(query.value("goal_count").toInt());
    }else {
        ui->probe_last_goal_count->setValue(0);
    }
    query_txt = QString("select * from `Production_app_machine_view_table` where `machine_part` = '%1' AND `use` = '2'  AND env_name='%2' order by machine_name asc")
                                .arg(tr("probe_last")).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    probe_last_source.machinecodelist.clear();
    probe_last_source.machinenamelist.clear();
    while(query.next()){
         probe_last_source.machinecodelist<<query.value("machine_code").toString();
         probe_last_source.machinenamelist<<query.value("machine_name").toString();
    }
    probe_last_source.chart_layout = ui->probe_last_chart_layout;
    probe_last_source.main_chart = new production_main_chart1();
    probe_last_source.chart_view = new production_main_chart_view1(probe_last_source.main_chart);
    ui->probe_last_out_view_btn->setProperty("child",QVariant::fromValue(ui->probe_last_main));
    ui->probe_last_out_view_btn->setProperty("parent_splitter",QVariant::fromValue(ui->probe_splitter));
    disconnect(ui->probe_last_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    connect(ui->probe_last_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    chart_maker1(probe_last_source);
}

void production_main::on_probe_total_check_search_btn_clicked()
{
    probe_total_check_source.search_start_datetime = ui->probe_total_check_start_datetime->dateTime();
    probe_total_check_source.search_end_datetime = ui->probe_total_check_end_datetime->dateTime();

    probe_total_check_source.part = tr("probe");
    probe_total_check_source.process = tr("probe_total_check");

    QSqlQuery query(my_mesdb);
    QString query_txt = QString("select * from Production_app_goal_count_list where process = '%1' AND env_name = '%2' ")
            .arg(probe_total_check_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    if(query.next()){
        ui->probe_total_check_goal_count->setValue(query.value("goal_count").toInt());
    }else {
        ui->probe_total_check_goal_count->setValue(0);
    }
    query_txt = QString("select * from `Production_app_machine_view_table` where `machine_part` = '%1' AND `use` = '2'AND env_name = '%2' order by machine_name asc")
                                .arg(tr("probe_total_check")).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    probe_total_check_source.machinecodelist.clear();
    probe_total_check_source.machinenamelist.clear();
    while(query.next()){
         probe_total_check_source.machinecodelist<<query.value("machine_code").toString();
         probe_total_check_source.machinenamelist<<query.value("machine_name").toString();
    }
    probe_total_check_source.chart_layout = ui->probe_total_check_chart_layout;
    probe_total_check_source.main_chart = new production_main_chart1();
    probe_total_check_source.chart_view = new production_main_chart_view1(probe_total_check_source.main_chart);
    ui->probe_total_check_out_view_btn->setProperty("child",QVariant::fromValue(ui->probe_total_check_main));
    ui->probe_total_check_out_view_btn->setProperty("parent_splitter",QVariant::fromValue(ui->probe_splitter));
    disconnect(ui->probe_total_check_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    connect(ui->probe_total_check_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    chart_maker1(probe_total_check_source);
}

void production_main::on_trimmer1_search_btn_clicked()
{
    trimmer1_source.search_start_datetime = ui->trimmer1_start_datetime->dateTime();
    trimmer1_source.search_end_datetime = ui->trimmer1_end_datetime->dateTime();

    trimmer1_source.part = "trimmer";
    trimmer1_source.process = tr("trimmer1");
    QSqlQuery query(my_mesdb);
    QString query_txt = QString("select * from Production_app_goal_count_list where process = '%1' AND env_name = '%2' ")
            .arg(trimmer1_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    if(query.next()){
        ui->trimmer1_goal_count->setValue(query.value("goal_count").toInt());
    }else {
        ui->trimmer1_goal_count->setValue(0);
    }
    query_txt = QString("select * from `Production_app_machine_view_table` where `machine_part` = '%1' AND `use` = '2'AND env_name = '%2' order by machine_name asc")
                                .arg(tr("trimmer1")).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    trimmer1_source.machinecodelist.clear();
    trimmer1_source.machinenamelist.clear();
    while(query.next()){
         trimmer1_source.machinecodelist<<query.value("machine_code").toString();
         trimmer1_source.machinenamelist<<query.value("machine_name").toString();
    }
    trimmer1_source.chart_layout = ui->trimmer1_chart_layout;
    trimmer1_source.main_chart = new production_main_chart1();
    trimmer1_source.chart_view = new production_main_chart_view1(trimmer1_source.main_chart);
    ui->trimmer1_out_view_btn->setProperty("child",QVariant::fromValue(ui->trimmer1_main));
    ui->trimmer1_out_view_btn->setProperty("parent_splitter",QVariant::fromValue(ui->trimmer_splitter));
    disconnect(ui->trimmer1_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    connect(ui->trimmer1_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    chart_maker1(trimmer1_source);
}

void production_main::on_trimmer2_search_btn_clicked()
{
    trimmer2_source.search_start_datetime = ui->trimmer2_start_datetime->dateTime();
    trimmer2_source.search_end_datetime = ui->trimmer2_end_datetime->dateTime();

    trimmer2_source.part = "trimmer";
    trimmer2_source.process = tr("trimmer2");
    QSqlQuery query(my_mesdb);
    QString query_txt = QString("select * from Production_app_goal_count_list where process = '%1' AND env_name = '%2' ")
            .arg(trimmer2_source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    if(query.next()){
        ui->trimmer2_goal_count->setValue(query.value("goal_count").toInt());
    }else {
        ui->trimmer2_goal_count->setValue(0);
    }
    query_txt = QString("select * from `Production_app_machine_view_table` where `machine_part` = '%1' AND `use` = '2' AND env_name = '%2' order by machine_name asc")
                                .arg(tr("trimmer2")).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    trimmer2_source.machinecodelist.clear();
    trimmer2_source.machinenamelist.clear();
    trimmer2_source.search_mode = "SiO2,trimmer";
    while(query.next()){
         trimmer2_source.machinecodelist<<query.value("machine_code").toString();
         trimmer2_source.machinenamelist<<query.value("machine_name").toString();
    }
    trimmer2_source.chart_layout = ui->trimmer2_chart_layout;
    trimmer2_source.main_chart = new production_main_chart1();
    trimmer2_source.chart_view = new production_main_chart_view1(trimmer2_source.main_chart);
    ui->trimmer2_out_view_btn->setProperty("child",QVariant::fromValue(ui->trimmer2_main));
    ui->trimmer2_out_view_btn->setProperty("parent_splitter",QVariant::fromValue(ui->trimmer_splitter));
    disconnect(ui->trimmer2_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    connect(ui->trimmer2_out_view_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    chart_maker1(trimmer2_source);
}

void production_main::on_sputtering_zoom_resetbtn_clicked()
{
      sputtering_source.main_chart->zoomReset();
}

void production_main::on_deposition_zoom_resetbtn_clicked()
{
    deposition_source.main_chart->zoomReset();
}

void production_main::on_lift_off_zoom_resetbtn_clicked()
{
    lift_off_source.main_chart->zoomReset();
}

void production_main::on_photo_krf_zoom_resetbtn_clicked()
{
     photo_krf_source.main_chart->zoomReset();
}
void production_main::on_photo_ILINE_zoom_resetbtn_clicked()
{
     photo_ILINE_source.main_chart->zoomReset();
}


void production_main::on_photo_coating_zoom_resetbtn_clicked()
{
    photo_coating_source.main_chart->zoomReset();
}


void production_main::on_photo_dev_zoom_resetbtn_clicked()
{
    photo_dev_source.main_chart->zoomReset();
}
void production_main::on_eatching_etcher_zoom_resetbtn_clicked()
{
    eatching_etcher_source.main_chart->zoomReset();
}

void production_main::on_protective_D_zoom_resetbtn_clicked()
{
    protective_D_source.main_chart->zoomReset();
}
void production_main::on_protective_E_zoom_resetbtn_clicked()
{
    protective_E_source.main_chart->zoomReset();
}


void production_main::on_probe_last_zoom_resetbtn_clicked()
{
    probe_last_source.main_chart->zoomReset();
}
void production_main::on_probe_total_check_zoom_resetbtn_clicked()
{
    probe_total_check_source.main_chart->zoomReset();
}

void production_main::on_trimmer1_zoom_resetbtn_clicked()
{
    trimmer1_source.main_chart->zoomReset();
}


void production_main::on_sputtering_goal_count_editingFinished()
{
    goal_count_edit_finish(sputtering_source,ui->sputtering_goal_count->value());
}
void production_main::on_deposition_goal_count_editingFinished()
{
    goal_count_edit_finish(deposition_source,ui->deposition_goal_count->value());
}
void production_main::on_lift_off_goal_count_editingFinished()
{
    goal_count_edit_finish(lift_off_source,ui->lift_off_goal_count->value());
}

void production_main::on_photo_krf_goal_count_editingFinished()
{
    goal_count_edit_finish(photo_krf_source,ui->photo_krf_goal_count->value());
}


void production_main::on_photo_ILINE_goal_count_editingFinished()
{
    goal_count_edit_finish(photo_ILINE_source,ui->photo_ILINE_goal_count->value());
}

void production_main::on_photo_coating_goal_count_editingFinished()
{
    goal_count_edit_finish(photo_coating_source,ui->photo_coating_goal_count->value());
}


void production_main::on_photo_dev_goal_count_editingFinished()
{
    goal_count_edit_finish(photo_dev_source,ui->photo_dev_goal_count->value());
}

void production_main::on_eatching_etcher_goal_count_editingFinished()
{
    goal_count_edit_finish(eatching_etcher_source,ui->eatching_etcher_goal_count->value());
}
void production_main::on_protective_D_goal_count_editingFinished()
{
    goal_count_edit_finish(protective_D_source,ui->protective_D_goal_count->value());
}

void production_main::on_protective_E_goal_count_editingFinished()
{
    goal_count_edit_finish(protective_E_source,ui->protective_E_goal_count->value());
}


void production_main::on_probe_last_goal_count_editingFinished()
{
    goal_count_edit_finish(probe_last_source,ui->probe_last_goal_count->value());
}

void production_main::on_probe_total_check_goal_count_editingFinished()
{
    goal_count_edit_finish(probe_total_check_source,ui->probe_total_check_goal_count->value());
}

void production_main::on_trimmer1_goal_count_editingFinished()
{
    goal_count_edit_finish(trimmer1_source,ui->trimmer1_goal_count->value());
}
void production_main::on_trimmer2_goal_count_editingFinished()
{
    goal_count_edit_finish(trimmer2_source,ui->trimmer2_goal_count->value());
}

void production_main::goal_count_edit_finish(chart_maker_source source, int value)
{
    QSqlQuery query(my_mesdb);
    QString query_txt = QString("select * from Production_app_goal_count_list where process = '%1' AND env_name= '%2' ")
            .arg(source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
    if(!query.next()){
        query_txt = QString("INSERT INTO `FAB`.`Production_app_goal_count_list` "
                            "(`part`, `process`, `goal_count`,`env_name`) VALUES ('%1', '%2', '0','%3');")
                .arg(source.part)
                .arg(source.process)
                .arg(ui->CB_env->currentText());
        query.exec(query_txt);
    }
    query_txt = QString("UPDATE `FAB`.`Production_app_goal_count_list` "
                                "SET `goal_count`='%1' WHERE  `part`='%2' AND `process`='%3' AND env_name = '%4' ")
            .arg(QString("%1").arg(value)).arg(source.part).arg(source.process).arg(ui->CB_env->currentText());
    query.exec(query_txt);
}
void production_main::out_view_chart(bool result)
{
    QPushButton *sender_obj =  qobject_cast<QPushButton *>(this->sender());
    production_out_viewer *popup = new production_out_viewer(sender_obj);
    popup->show();
}


//total_check_process
void production_main::total_check_init_tab()
{
    ui->total_check_spliter->setStretchFactor(1,1);
    total_viwer_spliter = new QSplitter(Qt::Vertical);
    ui->total_check_main_layout->addWidget(total_viwer_spliter);
    ui->total_check_search_start_date->setDate(QDate::currentDate().addDays(-1));
    ui->total_check_search_end_date->setDate(QDate::currentDate().addDays(-1));

}

void production_main::output_list_init_tab()
{
    ui->output_list_spliter->setStretchFactor(1,1);
    output_list_spliter = new QSplitter(Qt::Vertical);
    ui->output_list_main_layout->addWidget(output_list_spliter);
    ui->output_list_search_start_date->setDate(QDate::currentDate().addDays(-1));
    ui->output_list_search_end_date->setDate(QDate::currentDate().addDays(-1));
}


void production_main::on_total_check_search_btn_clicked()
{
     chart_maker_level2_source *source = new chart_maker_level2_source();
     source->data_name = tr("total check");
     source->start_date = ui->total_check_search_start_date->date();
     source->end_date = ui->total_check_search_end_date->date();
     if(ui->total_check_search_day->isChecked()){
         source->select_date_type = "day";
     }else{
         source->select_date_type = "month";
     }
     if(ui->total_check_search_typeA->isChecked()){
         source->types.append("A");
     }
     if(ui->total_check_search_typeB->isChecked()){
         source->types.append("B");
     }
     if(ui->total_check_search_typeC->isChecked()){
         source->types.append("C");
     }
     if(ui->total_check_search_typeD->isChecked()){
         source->types.append("D");
     }
     if(ui->total_check_search_typeM->isChecked()){
         source->types.append("M");
     }
     if(ui->total_check_search_typeP->isChecked()){
         source->types.append("P");
     }
     if(ui->total_check_search_typeS->isChecked()){
         source->types.append("S");
     }
     if(ui->total_check_search_wafer->isChecked()){
         source->select_unit_type = "wafer";
     }else {
         source->select_unit_type = "chip";
     }
     source->my_mesdb=my_mesdb;
     production_machine_level2_widget *chart_widget = new production_machine_level2_widget(source,NS_core);
     chart_widget->output_viwer_btn->setProperty("child",QVariant::fromValue(chart_widget));
     chart_widget->output_viwer_btn->setProperty("parent_splitter",QVariant::fromValue(total_viwer_spliter));
     chart_widget->del_btn->setProperty("del_widget",QVariant::fromValue(chart_widget));
     chart_widget->del_btn->setProperty("parent_splitter",QVariant::fromValue(total_viwer_spliter));
     connect(chart_widget->output_viwer_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
     connect(chart_widget->del_btn,SIGNAL(clicked(bool)),this,SLOT(del_btn_click(bool)));
     total_viwer_spliter->addWidget(chart_widget);
}

void production_main::on_total_check_search_day_clicked(bool checked)
{
    if(checked){
        ui->total_check_search_start_date->setDisplayFormat("yyyy-MM-dd");
        ui->total_check_search_end_date->setDisplayFormat("yyyy-MM-dd");
    }
}

void production_main::on_total_check_search_month_clicked(bool checked)
{
    if(checked){
        ui->total_check_search_start_date->setDisplayFormat("yyyy-MM");
        ui->total_check_search_end_date->setDisplayFormat("yyyy-MM");
    }
}
void production_main::on_output_list_search_day_clicked(bool checked)
{
    if(checked){
        ui->output_list_search_start_date->setDisplayFormat("yyyy-MM-dd");
        ui->output_list_search_end_date->setDisplayFormat("yyyy-MM-dd");
    }
}

void production_main::on_output_list_search_month_clicked(bool checked)
{
    if(checked){
        ui->output_list_search_start_date->setDisplayFormat("yyyy-MM");
        ui->output_list_search_end_date->setDisplayFormat("yyyy-MM");
    }
}

void production_main::del_btn_click(bool result)
{
    QPushButton *sender_obj =  qobject_cast<QPushButton *>(this->sender());
    QWidget *del_widget = sender_obj->property("del_widget").value<QWidget *>();
    del_widget->deleteLater();
}


void production_main::on_tabWidget_currentChanged(int index)
{
    if( (index== 0) && (!depostion_tab_init_bool)){
        depostion_tab_init_bool = true;
        deposition_init_tab();
    }else if((index == 1) && (!photo_tab_init_bool)){
        photo_tab_init_bool = true;
        photo_init_tab();
    }else if((index == 2) && (!eatching_tab_init_bool)){
        eatching_tab_init_bool = true;
        eatching_list_init_tab();
    }else if((index == 3) && (!probe_tab_init_bool)){
        probe_tab_init_bool = true;
        probe_list_init_tab();
    }else if((index == 4) && (!trimmer_tab_init_bool)){
        trimmer_tab_init_bool = true;
        trimmer_list_init_tab();
    }
    QSettings production_settings("production_app.ini",QSettings::IniFormat);
    production_settings.beginGroup("settings");
    production_settings.setValue("last_index",index);
    last_index = production_settings.value("last_index").toInt();
    production_settings.endGroup();
}

void production_main::on_output_list_search_btn_clicked()
{
    chart_maker_level2_source *source = new chart_maker_level2_source();
    source->data_name = tr("outputlist");
    source->start_date = ui->output_list_search_start_date->date();
    source->end_date = ui->output_list_search_end_date->date();
    if(ui->output_list_search_day->isChecked()){
        source->select_date_type = "day";
    }else{
        source->select_date_type = "month";
    }
    if(ui->output_list_search_typeA->isChecked()){
        source->types.append("A");
    }
    if(ui->output_list_search_typeB->isChecked()){
        source->types.append("B");
    }
    if(ui->output_list_search_typeC->isChecked()){
        source->types.append("C");
    }
    if(ui->output_list_search_typeD->isChecked()){
        source->types.append("D");
    }
    if(ui->output_list_search_typeM->isChecked()){
        source->types.append("M");
    }
    if(ui->output_list_search_typeP->isChecked()){
        source->types.append("P");
    }
    if(ui->output_list_search_typeS->isChecked()){
        source->types.append("S");
    }
    if(ui->output_list_search_wafer->isChecked()){
        source->select_unit_type = "wafer";
    }else {
        source->select_unit_type = "chip";
    }
    source->my_mesdb=my_mesdb;
    production_machine_level2_widget *chart_widget = new production_machine_level2_widget(source,NS_core);
    chart_widget->output_viwer_btn->setProperty("child",QVariant::fromValue(chart_widget));
    chart_widget->output_viwer_btn->setProperty("parent_splitter",QVariant::fromValue(output_list_spliter));
    chart_widget->del_btn->setProperty("del_widget",QVariant::fromValue(chart_widget));
    chart_widget->del_btn->setProperty("parent_splitter",QVariant::fromValue(output_list_spliter));
    connect(chart_widget->output_viwer_btn,SIGNAL(clicked(bool)),this,SLOT(out_view_chart(bool)));
    connect(chart_widget->del_btn,SIGNAL(clicked(bool)),this,SLOT(del_btn_click(bool)));
    output_list_spliter->addWidget(chart_widget);
}


void production_main::init_env()
{
    disconnect(ui->CB_env,SIGNAL(currentIndexChanged(QString)),this,SLOT(on_CB_env_currentIndexChanged(QString)));
    QSqlQuery query(my_mesdb);
    query.exec("select env_name from Production_app_machine_view_table group by env_name");
    while(query.next()){
        ui->CB_env->addItem(query.value("env_name").toString());
    }
    connect(ui->CB_env,SIGNAL(currentTextChanged(QString)),this,SLOT(on_CB_env_currentIndexChanged(QString)));
    ui->tabWidget->setCurrentIndex(last_index);
    ui->CB_env->setCurrentText(last_env);
    emit ui->CB_env->currentTextChanged(last_env);

}


void production_main::on_CB_env_currentIndexChanged(const QString &arg1)
{
    qDebug()<<arg1;

    depostion_tab_init_bool = false;
    photo_tab_init_bool= false;
    eatching_tab_init_bool= false;
    probe_tab_init_bool= false;

    QSettings production_settings("production_app.ini",QSettings::IniFormat);
    production_settings.beginGroup("settings");
    production_settings.setValue("last_env",arg1);
    last_env = production_settings.value("last_env").toString();
    production_settings.endGroup();

    emit ui->tabWidget->currentChanged(ui->tabWidget->currentIndex());
}




