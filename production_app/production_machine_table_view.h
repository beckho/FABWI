#ifndef PRODUCTION_MACHINE_TABLE_VIEW_H
#define PRODUCTION_MACHINE_TABLE_VIEW_H
#include <QObject>
#include <QWidget>
#include <QTableView>
#include <QKeyEvent>
#include <QApplication>
#include <QClipboard>
#include <QDebug>
#include <QHeaderView>
#include <QSqlQueryModel>
#include <QSqlRecord>
#include <QSqlQuery>
class production_machine_table_view : public QTableView
{
    Q_OBJECT
public:
    explicit production_machine_table_view(QWidget *parent = 0);
    bool copyheader_flag;
private:
    void keyPressEvent(QKeyEvent *event);
private slots:
    void slot_sortIndicatorChanged(int index,Qt::SortOrder sort);




};

#endif // PRODUCTION_MACHINE_TABLE_VIEW_H
