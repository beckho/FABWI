#ifndef PRODUCTION_MACHINE_LEVEL2_POPUP_H
#define PRODUCTION_MACHINE_LEVEL2_POPUP_H

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <production_app/production_machine_table_view.h>
#include <production_app/production_level2_table_model.h>
#include <QDate>
#include <QSqlQueryModel>
#include <QStandardItemModel>
namespace Ui {
class production_machine_level2_popup;
}

class production_machine_level2_popup : public QWidget
{
    Q_OBJECT

public:
    explicit production_machine_level2_popup(QDate start_date,QDate end_date,QString Lot_type,QSqlDatabase my_mesdb,QWidget *parent = 0);
    QDate start_date;
    QDate end_date;
    QString Lot_type;


    QSqlDatabase my_mesdb;
    production_level2_table_model *production_model;
    production_level2_table_model *lot_model;
    bool production_init_flag;
    bool lot_id_tab_init_flag;
    QStandardItemModel *procution_cb_model;
    QStandardItemModel *lot_cb_model;
    ~production_machine_level2_popup();

private slots:
    void on_tabWidget_currentChanged(int index);

    void on_production_search_btn_clicked();

    void on_lot_id_search_btn_clicked();

    void on_lot_prodcution_search_btn_clicked();

private:
    void prodcution_tab_init();
    void lot_id_tab_init();
    Ui::production_machine_level2_popup *ui;
};

#endif // PRODUCTION_MACHINE_LEVEL2_POPUP_H
