#ifndef CHART_MAKER_LEVEL1_SOURCE_H
#define CHART_MAKER_LEVEL1_SOURCE_H

#include <QObject>

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <QDateTime>
#include <production_app/production_main_chart1.h>
#include <production_app/production_main_chart_view1.h>
#include <QVBoxLayout>
#include <QBarSeries>
#include <QBarset>
#include <production_app/production_machine_level1_widget.h>
#include <QBarCategoryAxis>
#include <QValueAxis>
#include <QSqlQueryModel>
#include <QScatterSeries>
#include <QSplitter>
class chart_maker_level1_source : public QObject
{
    Q_OBJECT
public:
    QDateTime search_start_datetime;
    QDateTime search_end_datetime;
    QString machinename;
    QString Operation_name;
    QString Operation_name2;
    QString search_mode;

    QVBoxLayout *chart_layout;
    production_main_chart1 *main_chart;
    production_main_chart_view1 *chart_view;
    QBarSeries *barseries;
    QBarSet *input_qty_barset;
    QBarSet *output_qty_barset;
    QScatterSeries *goal_count_series;
    QBarCategoryAxis *catacorybar_axis;
    QValueAxis *value_axis;
    production_machine_level1_widget *main_widget;
    QSqlQueryModel *output_loss_lot_model;
    QSqlQueryModel *output_lot_model;
    QSqlQueryModel *defect_model;
    QSqlQueryModel *rework_model;
    QSqlQueryModel *probe_model;

    QString part;
    QString process;

    QString env_name;

    bool part_process;


    chart_maker_level1_source();
};

#endif // CHART_MAKER_LEVEL1_SOURCE_H
