#include "production_level2_table_model.h"

production_level2_table_model::production_level2_table_model(QObject *parent) :QSqlQueryModel(parent)
{

}

QVariant production_level2_table_model::data(const QModelIndex &index, int role) const
{


    if(index.column() == property("WAFER_QTY_index").toInt() && role == Qt::DisplayRole){
        qint64 value = record(index.row()).value("WAFER_QTY").toLongLong();
        return QString("%1").arg(value);
    }
    if(index.column() == property("CHIP_QTY_index").toInt()  && role == Qt::DisplayRole){
        qint64 value = record(index.row()).value("CHIP_QTY").toLongLong();
        return QString("%1").arg(value);
    }
    return QSqlQueryModel::data(index, role);
}
