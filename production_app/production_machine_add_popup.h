#ifndef PRODUCTION_MACHINE_ADD_POPUP_H
#define PRODUCTION_MACHINE_ADD_POPUP_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlRecord>
#include <QDateTime>
#include <QDebug>
#include <QSqlError>
#include <global_define.h>
namespace Ui {
class production_machine_add_popup;
}

class production_machine_add_popup : public QDialog
{
    Q_OBJECT

public:
    explicit production_machine_add_popup(QString part,QString part_machine,QSqlDatabase my_mesdb,QString search_mode,QString env_name,QWidget *parent = 0);

    QSqlDatabase ms_mesdb;
    QSqlDatabase my_mesdb;
    QSqlDatabase my_mesdb2;
    QSqlQueryModel *main_model;
    QString part;
    QString part_machine;
    QString query_txt;
    QString order_query_text;
    QString search_mode;
    QString env_name;
    ~production_machine_add_popup();

private slots:
    void on_search_btn_clicked();
    void SortOrder(int index,Qt::SortOrder order);

    void on_buttonBox_accepted();

private:
    Ui::production_machine_add_popup *ui;
};

#endif // PRODUCTION_MACHINE_ADD_POPUP_H
