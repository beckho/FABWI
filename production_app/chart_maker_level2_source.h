#ifndef CHART_MAKER_LEVEL2_SOURCE_H
#define CHART_MAKER_LEVEL2_SOURCE_H

#include <QObject>
#include <QWidget>
#include <QSqlDatabase>
#include <QStackedBarSeries>
#include <QCategoryAxis>
#include <QValueAxis>
#include <QDate>
#include <production_app/production_main_chart1.h>
#include <production_app/production_main_chart_view1.h>
#include <QBarCategoryAxis>
#include <QScatterSeries>
class chart_maker_level2_source: public QObject
{
    Q_OBJECT
public:
    QDate start_date;
    QDate end_date;
    QStringList types;
    QString select_date_type;
    QString select_unit_type;
    QSqlDatabase my_mesdb;
    production_main_chart1 *chart;
    production_main_chart_view1 *chart_view;
    QBarCategoryAxis *categoryaxis;
    QValueAxis *valueaxis;
    QStackedBarSeries *stackerbarseries;
    QScatterSeries *scatterSeries;
    QBarSet *CSP_set;
    QBarSet *NJRC_set;
    QBarSet *WLP_set;
    QBarSet *TC_set;
    QString data_name;
    chart_maker_level2_source();
};

#endif // CHART_MAKER_LEVEL2_SOURCE_H
