#ifndef PRODUCTION_MACHINE_LEVEL2_WIDGET_H
#define PRODUCTION_MACHINE_LEVEL2_WIDGET_H

#include <QWidget>
#include <production_app/chart_maker_level2_source.h>
#include <production_app/production_main_chart1.h>
#include <production_app/production_main_chart_view1.h>
#include <QDate>
#include <QBarSet>
#include <QDebug>
#include <QSqlQuery>
#include <QPushButton>
#include <production_app/production_machine_level2_popup.h>
#include <ns_core_com/ns_core_com.h>
class date_struct: public QObject {
    Q_OBJECT

public:
    date_struct(QDate start_date,QDate end_date);
    QDate start_date;
    QDate end_date;
};

namespace Ui {
class production_machine_level2_widget;
}

class production_machine_level2_widget : public QWidget
{
    Q_OBJECT

public:
    production_machine_level2_widget(chart_maker_level2_source *source,NS_Core_Com::NS_Main *NS_Core,QWidget *parent = 0);
    chart_maker_level2_source *source;
    production_main_chart1 *chart;
    production_main_chart1 *chart_view;
    QPushButton *output_viwer_btn;
    QPushButton *del_btn;
    QSqlDatabase my_mesdb;
    QSqlDatabase my_localdb;
    QString Lot_type_query;
    NS_Core_Com::NS_Main *NS_Core;
    QString popup_Lot_type_query;
    void maker_bar_set(QBarSet *source_barset, QStringList date_query_list
                       , QString out_type, QString qty_type);
    void maker_bar_set_cheked(QBarSet *source_barset, QStringList date_query_list
                       , QString Lot_type_query , QString out_type, QString qty_type);

    QVector<date_struct *> date_struct_list;
    QString return_item(QString data);

    ~production_machine_level2_widget();

private slots:
    void on_zoomreset_btn_clicked();

    void on_label_togle_btn_clicked();

    void on_total_label_togle_btn_clicked();

    void output_list_bar_click(int index,QBarSet* barset);

private:
    void chartmaker();
    Ui::production_machine_level2_widget *ui;
};

#endif // PRODUCTION_MACHINE_LEVEL2_WIDGET_H
