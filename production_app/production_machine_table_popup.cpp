#include "production_machine_table_popup.h"
#include "ui_production_machine_table_popup.h"

Production_machine_table_popup::Production_machine_table_popup(QSqlDatabase my_mesdb, QString part, QString machine_part, QString env_name, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Production_machine_table_popup)
{
    ui->setupUi(this);
    this->my_mesdb = my_mesdb;
    main_model = new production_machine_table_model(this,my_mesdb);
    this->env_name= env_name;
    main_model->setTable("Production_app_machine_view_table");
    QString filter_txt = QString("part = '%1' AND machine_part = '%2' AND env_name = '%3'").arg(part).arg(machine_part).arg(env_name);
    main_model->setFilter(filter_txt);
    ui->main_table->setModel(main_model);

    this->part = part;
    this->machine_part = machine_part;

    main_model->select();

}

Production_machine_table_popup::Production_machine_table_popup(QSqlDatabase my_mesdb, QString part, QString machine_part, QString search_mode, QString env_name, QWidget *parent):
    QWidget(parent),
    ui(new Ui::Production_machine_table_popup)
{

    ui->setupUi(this);
     this->search_mode = search_mode;
    this->my_mesdb = my_mesdb;
    main_model = new production_machine_table_model(this,my_mesdb);
    this->env_name= env_name;
    main_model->setTable("Production_app_machine_view_table");
    QString filter_txt = QString("part = '%1' AND machine_part = '%2' AND env_name = '%3'").arg(part).arg(machine_part).arg(env_name);
    main_model->setFilter(filter_txt);
    ui->main_table->setModel(main_model);

    this->part = part;
    this->machine_part = machine_part;

    main_model->select();


}

Production_machine_table_popup::~Production_machine_table_popup()
{
    delete ui;
}


void Production_machine_table_popup::on_machine_del_btn_clicked()
{
    int current_row = -1;
    QModelIndexList select_list= ui->main_table->selectionModel()->selectedIndexes();
    for(int i=0;i<select_list.count();i++){
        if(current_row != select_list.at(i).row()){
            QSqlRecord recode = main_model->record(select_list.at(i).row());
            recode.setGenerated(recode.indexOf("machine_name"),true);
            recode.setGenerated(recode.indexOf("machine_code"),true);
            recode.setGenerated(recode.indexOf("env_name"),true);
            main_model->removeRow(select_list.at(i).row());
        }
    }
    main_model->select();
}

void Production_machine_table_popup::on_machine_add_btn_clicked()
{
    production_machine_add_popup *popup = new production_machine_add_popup(part,machine_part,my_mesdb,search_mode,env_name);
    if(popup->exec()==QDialog::Accepted){
        main_model->select();
    }
}
