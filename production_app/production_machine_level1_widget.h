#ifndef PRODUCTION_MACHINE_LEVEL1_WIDGET_H
#define PRODUCTION_MACHINE_LEVEL1_WIDGET_H

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QDateTime>
#include <production_app/production_main_chart1.h>
#include <production_app/production_main_chart_view1.h>
#include <QVBoxLayout>
#include <QBarSeries>
#include <QBarset>
#include <QSqlRecord>
#include <QPushButton>

namespace Ui {
class production_machine_level1_widget;
}

class production_machine_level1_widget : public QWidget
{
    Q_OBJECT

public:
    explicit production_machine_level1_widget(QSqlDatabase my_mesdb,QSqlDatabase ms_mesdb,QSqlDatabase my_mesdb2,QObject *source, QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    QSqlDatabase my_mesdb2;
    QSqlDatabase ms_mesdb;
    QObject *source;
    QPushButton *out_viwer_btn;

    ~production_machine_level1_widget();
signals:
    void out_viwer();

private slots:
    void on_search_btn_clicked();


private:
    Ui::production_machine_level1_widget *ui;
};

#endif // PRODUCTION_MACHINE_LEVEL1_WIDGET_H
