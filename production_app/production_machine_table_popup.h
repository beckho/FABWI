#ifndef PRODUCTION_MACHINE_TABLE_POPUP_H
#define PRODUCTION_MACHINE_TABLE_POPUP_H

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlTableModel>
#include <QDebug>
#include <production_app/production_machine_table_model.h>
#include <QSqlRecord>
#include <production_app/production_machine_add_popup.h>
namespace Ui {
class Production_machine_table_popup;
}

class Production_machine_table_popup : public QWidget
{
    Q_OBJECT

public:
    Production_machine_table_popup(QSqlDatabase my_mesdb,QString part,QString machine_part,QString env_name,QWidget *parent = 0);
    Production_machine_table_popup(QSqlDatabase my_mesdb,QString part,QString machine_part,QString search_mode,QString env_name,QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    production_machine_table_model *main_model;
    QString part;
    QString machine_part;
    QString env_name;
    QString search_mode;
    ~Production_machine_table_popup();

private slots:


    void on_machine_del_btn_clicked();

    void on_machine_add_btn_clicked();

private:
    Ui::Production_machine_table_popup *ui;
};

#endif // PRODUCTION_MACHINE_TABLE_POPUP_H
