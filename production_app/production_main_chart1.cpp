#include "production_main_chart1.h"

production_main_chart1::production_main_chart1(QGraphicsItem *parent, Qt::WindowFlags wFlags)
    :QChart(QChart::ChartTypeCartesian, parent, wFlags)
{

}

QSqlDatabase production_main_chart1::getMy_mesdb() const
{
    return my_mesdb;
}

void production_main_chart1::setMy_mesdb(const QSqlDatabase &value)
{
    my_mesdb = value;
}
