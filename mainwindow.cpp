#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QDebug"
#include <eismain.h>
#include <e2r_every_report.h>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_action_everyday_report_triggered()
{
    E2R_every_report *report = new E2R_every_report();
    report->show();
}

void MainWindow::on_actionEISystem_triggered()
{
    EISmain *eismain = new EISmain("CSP");
    eismain->show();
}

void MainWindow::on_actionMessage_triggered()
{


}

void MainWindow::on_action_server_setting_triggered()
{
    Server_setting *setting_log = new Server_setting();
    setting_log->exec();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QString temp_dir_path = qApp->applicationDirPath()+"/temp/EIS/img/";
    QDir tempdir(temp_dir_path);
    QStringList dir_list = tempdir.entryList();
    for(int i=0;i<dir_list.size();i++){
        if(dir_list.at(i) != "." && dir_list.at(i) != ".."){
             QString rm_dir_path = temp_dir_path+ dir_list.at(i)+"/";
             QDir rm_dir(rm_dir_path);
             for(int j=0;j<rm_dir.entryList().size();j++){
                if(rm_dir.entryList().at(j) != "." && rm_dir.entryList().at(j) != ".."){
                    rm_dir.remove(rm_dir.entryList().at(j));
                }
             }
             tempdir.rmdir(dir_list.at(i));
        }
    }

    QString temp_dir_path2 = qApp->applicationDirPath()+"/temp/EIS/alarmimg/";
    QDir tempdir2(temp_dir_path2);
    QStringList dir_list2 = tempdir2.entryList();
    for(int i=0;i<dir_list2.size();i++){
        if(dir_list2.at(i) != "." && dir_list2.at(i) != ".."){
             QString rm_dir_path = temp_dir_path2+ dir_list2.at(i)+"/";
             QDir rm_dir(rm_dir_path);
             for(int j=0;j<rm_dir.entryList().size();j++){
                if(rm_dir.entryList().at(j) != "." && rm_dir.entryList().at(j) != ".."){
                    rm_dir.remove(rm_dir.entryList().at(j));
                }
             }
             tempdir2.rmdir(dir_list2.at(i));
        }
    }
}

void MainWindow::on_infrom_entet_btn_clicked()
{
    EISmain *eismain = new EISmain("CSP");
    eismain->show();
}

//void MainWindow::on_testbtn_clicked()
//{

//    //mail example code
//    SmtpClient smtp("mx.info.wisol.co.kr", 25, SmtpClient::TcpConnection);

//    MimeMessage message;

//    EmailAddress sender("bhkim@wisol.co.kr", "beckho");
//    message.setSender(&sender);

//    EmailAddress to("bhkim@wisol.co.kr", "beckho");
//    message.addRecipient(&to);

//    message.setSubject("SmtpClient for Qt - Demo");
//    MimeHtml html;

//    html.setHtml("<h1> Hello! </h1>"
//                 "<h2> This is the first image </h2>");

//    MimeInlineFile image1 (new QFile("1111.png"));

//    image1.setContentId("image1");
//    image1.setContentType("image/png");


//    message.addPart(&html);
//    message.addPart(&image1);


//    if (!smtp.connectToHost()) {
//        qDebug() << "Failed to connect to host!" << endl;
//    }
//    if (!smtp.sendMail(message)) {
//        qDebug() << "Failed to send mail!" << endl;

//    }
//    smtp.quit();

//}

void MainWindow::mail_contionfail(QByteArray msg)
{
    qDebug()<<msg;
}

void MainWindow::mail_fail(int a, int b, QByteArray msg)
{
    qDebug()<<msg;
}


void MainWindow::on_action_etching_analysor_triggered()
{
    etching_analysor *analysor = new etching_analysor();
    analysor->show();

}

void MainWindow::on_action_nikon_err_log_triggered()
{
    nikon_log_err_main *analysor = new nikon_log_err_main();
    analysor->show();

}

void MainWindow::on_action_hanile_triggered()
{
    hanile_log_main *analysor = new hanile_log_main();
    analysor->show();
}

void MainWindow::on_action_EVATEC_triggered()
{

}

void MainWindow::on_action_temp_humi_triggered()
{
    temp_humi_widget *popup = new temp_humi_widget();
    popup->show();

}

void MainWindow::on_action_asml_err_triggered()
{
    ASML_err_log_main *analysor = new ASML_err_log_main();
    analysor->show();
}

void MainWindow::on_action_worst_serarch_triggered()
{
    worst_search_excel_main *popup = new worst_search_excel_main();
    popup->show();

}

void MainWindow::on_OI_startup_clicked()
{
    QMessageBox *box = new QMessageBox();
    box->setText(tr("use oi system 2"));
    box->exec();
    QString OI_program_path = QString("\"%1/operating_ratio_program/OIservice.exe\"").arg(qApp->applicationDirPath());
    qDebug()<<OI_program_path;
    QProcess OI_program;
    OI_program.startDetached(OI_program_path);

}

void MainWindow::on_action_nikon_err_time_triggered()
{
    nikon_time_log *analysor = new nikon_time_log();
    analysor->show();
}

void MainWindow::on_actionMES_bridge_chart_triggered()
{
    bridge_chart_widget *bridgewidget = new bridge_chart_widget();
    bridgewidget->show();
}

void MainWindow::on_NIS_clicked()
{
   NISmainwindow *niswidget = new NISmainwindow("CSP");
   niswidget->show();
}



void MainWindow::on_Thin_film_manager_triggered()
{
    Thin_film_mainwindows *form  = new Thin_film_mainwindows();
    form->show();

}

void MainWindow::on_thin_monitering_tool_triggered()
{
    Thin_monitering_tool *form = new Thin_monitering_tool();
    form->show();

}

void MainWindow::on_Cost_reduction_triggered()
{
    cost_reduction_main * from = new cost_reduction_main();
    from->show();
}

void MainWindow::on_Cost_reduce_btn_clicked()
{
    cost_reduction_main * from = new cost_reduction_main();
    from->show();
}

void MainWindow::on_multi_depostion_work_sheet_btn_clicked()
{
    Thin_film_mainwindows *form  = new Thin_film_mainwindows();
    form->show();

}

void MainWindow::on_actionACT_triggered()
{
    Act_log_analysor *from = new Act_log_analysor();
    from->show();
}

void MainWindow::on_actionTEST_triggered()
{
#ifdef TEST
    QQuickView *viewer =new QQuickView();
    // The following are needed to make examples run without having to install the module
    // in desktop environments.
#ifdef Q_OS_WIN
    QString extraImportPath(QStringLiteral("%1/../../../../%2"));
#else
    QString extraImportPath(QStringLiteral("%1/../../../%2"));
#endif
    viewer->engine()->addImportPath(extraImportPath.arg(QGuiApplication::applicationDirPath(),
                                      QString::fromLatin1("qml")));
    QObject::connect(viewer->engine(), &QQmlEngine::quit, viewer, &QWindow::close);

    viewer->setTitle(QStringLiteral("QML F1 Legends"));
    viewer->setSource(QUrl("qrc:/qml_src/TEST/qmltest.qml"));
    viewer->setResizeMode(QQuickView::SizeRootObjectToView);
    viewer->show();
#endif
}

void MainWindow::on_actionDeshboard_triggered()
{
    deshboardmain *from  = new deshboardmain();
    from->show();
}

void MainWindow::on_Dashboard_btn_clicked()
{
    deshboardmain *from  = new deshboardmain();
    from->show();
}

void MainWindow::on_actionCD_SEM_triggered()
{
    Cdsemmain *from = new Cdsemmain();
    from->show();
}

void MainWindow::on_actionShincron_triggered()
{
    Shincron_log *from = new Shincron_log();
    from->show();

}

void MainWindow::on_production_moniter_action_triggered()
{
    production_main *from = new production_main;
    from->show();
}

void MainWindow::on_production_app_btn_clicked()
{
    production_main *from = new production_main;
    from->show();
}

void MainWindow::on_actionE_SPC_triggered()
{
    ESCP_main *from = new ESCP_main();
    from->show();
}

void MainWindow::on_change_histroy_manager_triggered()
{
    QString program_path = QString("\"%1/change_history.exe\"").arg(qApp->applicationDirPath());
    qDebug()<<program_path;
    QProcess change_history_program;
    change_history_program.startDetached(program_path);
}

void MainWindow::on_action_bridge_img_check_triggered()
{
    Bridge_img_check_main *from  = new Bridge_img_check_main();
    from->show();

}

void MainWindow::on_actionSAMCO_triggered()
{
    SOMCO_main *from = new SOMCO_main();
    from->show();
}

void MainWindow::on_suggestion_system_btn_clicked()
{
     QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/wisoleis_suggestion/#/suggestionmain/suggestion"));
}

void MainWindow::on_action_summary_triggered()
{
    QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/oirate/"));
}

void MainWindow::on_operation_ratio_btn_clicked()
{
    QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/oirate/"));
}

void MainWindow::on_Edu_action_triggered()
{
    EDUmain *eduwidget = new EDUmain();
    eduwidget->show();
}

void MainWindow::on_online_fdc_statue_btn_clicked()
{
    QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/onlinestatue/"));
}

void MainWindow::on_pushButton_clicked()
{
    QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/wsop/"));
}

void MainWindow::on_Dashboard_btn2_clicked()
{
    QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/yield_desh/"));
}

void MainWindow::on_OI_startup2_clicked()
{
    QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/oirate/"));
}

void MainWindow::on_actionVersion2_UI_triggered()
{
    Mainwindow2 *w2 = new Mainwindow2();
    w2->show();
}

void MainWindow::on_action_depostion_cpcpk_triggered()
{
    QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/web_cpcpk/#/cpcpkreport/deposition"));
}
