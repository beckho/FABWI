#include "mainwindow.h"
#include "mainwindow2.h"
#include <QApplication>
#include <QTranslator>
#include <QDebug>
#include <Thin_film_manager/thin_film_manager_input_lot_item.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator qtTranslator;
    qRegisterMetaType<thin_film_manager_input_lot_item>("thin_film_manager_input_lot_item");

    qtTranslator.load(":/lang/lang_ko.qm");
    a.installTranslator(&qtTranslator);
//    MainWindow w;
    qDebug()<<argv[1];
    if(argv[1] != NULL){
        if(*argv[1] == 0x33){
            NISmainwindow *niswidget = new NISmainwindow("CSP");
            niswidget->show();
        }else if(*argv[1] == 0x34){
            production_main *from = new production_main;
            from->show();
        }else if(*argv[1] == 0x35){
            Thin_film_mainwindows *form  = new Thin_film_mainwindows();
            form->show();
        }else if(*argv[1] == 0x36){
            CSPBoard *cspboard = new CSPBoard();
            cspboard->show();
        }

    }else {

        Mainwindow2 *w2 =new Mainwindow2() ;
        w2->show();

    }
//    w.show();


    return a.exec();
}
