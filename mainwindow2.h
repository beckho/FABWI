#ifndef MAINWINDOW2_H
#define MAINWINDOW2_H

#include <QMainWindow>
#include <QMainWindow>
#include <idix_gui.h>
#include <e2r_every_report.h>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrlQuery>
#include <server_setting.h>
#include <logsrc/etching_analysor.h>
#include <logsrc/nikon_log_err_main.h>
#include <logsrc/hanile_log_main.h>
#include <temp_humi_widget.h>
#include <logsrc/asml_err_log_main.h>
#include <worksupport/worst_search_excel_main.h>
#include <logsrc/nikon_time_log.h>
#include <operatingratio/operating_ratio_main.h>
#include <bridge_chart/bridge_chart_widget.h>
#include <NIS/nismainwindow.h>
#include <EDU/edumain.h>
#include <Thin_film_manager/thin_film_mainwindows.h>
#include <Thin_film_manager/thin_monitering_tool.h>
#include <Cost_Reduction/cost_reduction_main.h>
#include <logsrc/act_log_analysor.h>
#include <QtWidgets/QApplication>

#include <deshboard/deshboardmain.h>
#include <Cdsemmanager/cdsemmain.h>
#include <logsrc/shincron_log.h>
#include <production_app/production_main.h>
#include <EatchingSPC/escp_main.h>
#include <Bridge_img_check/bridge_img_check_main.h>
#include <SOMCO/somco_main.h>
#include <suggestion_system/suggestion_main.h>
#include <QDesktopWidget>
#include <QSettings>
#include <cspboard.h>
#include <wlpboard.h>
#include <TPRCalc/tprcalcmain.h>
#include <bprojectboard.h>
namespace Ui {
class Mainwindow2;
}

class Mainwindow2 : public QMainWindow
{
    Q_OBJECT

public:
    explicit Mainwindow2(QWidget *parent = 0);
    QSettings *settings;
    QSettings *regsettings;
    QString mainmode;


    ~Mainwindow2();

private slots:

    void yield_pcver_trigger(bool value);
    void yield_webver_trigger(bool value);

    void oi_ver1_trigger(bool value);
    void oi_ver2_trigger(bool value);

    void on_action_temp_humi_triggered();

    void on_action_worst_search_triggered();

    void on_Edu_action_triggered();

    void on_action_etching_analysor_triggered();

    void on_action_nikon_err_log_triggered();

    void on_action_nikon_err_time_triggered();

    void on_action_asml_err_triggered();

    void on_action_MES_bridge_chart_triggered();

    void on_actionACT_triggered();

    void on_actionCD_SEM_triggered();

    void on_actionShincron_triggered();

    void on_actionE_SPC_triggered();

    void on_action_change_histroy_manager_triggered();

    void on_action_machine_lead_time_triggered();

    void on_action_deposition_cpcpk_triggered();

    void on_actionCSP_toggled(bool arg1);

    void on_actionWLP_toggled(bool arg1);

    void clearLayout(QLayout* layout, bool deleteWidgets = true);



    void on_actionBPROJECT_toggled(bool arg1);

    void on_actionTPRCalc_triggered();

private:
    Ui::Mainwindow2 *ui;
};

#endif // MAINWINDOW2_H
