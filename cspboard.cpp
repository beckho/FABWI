#include "cspboard.h"
#include "ui_cspboard.h"

#include "QDebug"
#include <eismain.h>
#include <e2r_every_report.h>

CSPBoard::CSPBoard(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CSPBoard)
{
    ui->setupUi(this);
}

CSPBoard::~CSPBoard()
{
    delete ui;
}

void CSPBoard::on_infrom_entet_btn_clicked()
{
    EISmain *eismain = new EISmain("CSP");
    eismain->show();
}

void CSPBoard::on_Cost_reduce_btn_clicked()
{
    cost_reduction_main * from = new cost_reduction_main();
    from->show();
}

void CSPBoard::on_online_fdc_statue_btn_clicked()
{
     QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/onlinestatue/"));
}

void CSPBoard::on_SOP_manual_btn_clicked()
{
    QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/wsop/"));
}

void CSPBoard::on_suggestion_system_btn_clicked()
{
    QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/wisoleis_suggestion/#/suggestionmain/suggestion"));
}

void CSPBoard::on_Dashboard_btn_clicked()
{
    QMenu *fileMenu = new QMenu();
    QAction *pcver_action = new QAction("PC ver");
    QAction *webver_action = new QAction("Web ver");
    connect(pcver_action,SIGNAL(triggered(bool)),this,SLOT(yield_pcver_trigger(bool)));
    connect(webver_action,SIGNAL(triggered(bool)),this,SLOT(yield_webver_trigger(bool)));
    fileMenu->addAction(pcver_action);
    fileMenu->addAction(webver_action);
    fileMenu->popup(QCursor::pos());
}

void CSPBoard::on_OI_startup_clicked()
{
    QMenu *fileMenu = new QMenu();
    QAction *oi_service2 = new QAction("OI_ver2");
    QAction *oi_service1 = new QAction("OI_ver1");
    connect(oi_service2,SIGNAL(triggered(bool)),this,SLOT(oi_ver2_trigger(bool)));
    connect(oi_service1,SIGNAL(triggered(bool)),this,SLOT(oi_ver1_trigger(bool)));
    fileMenu->addAction(oi_service2);
    fileMenu->addAction(oi_service1);
    fileMenu->popup(QCursor::pos());
}

void CSPBoard::on_NIS_btn_clicked()
{
    NISmainwindow *niswidget = new NISmainwindow("CSP");
    niswidget->show();
}

void CSPBoard::on_production_app_btn_clicked()
{
    production_main *from = new production_main;
    from->show();
}

void CSPBoard::on_multi_depostion_work_sheet_btn_clicked()
{
    Thin_film_mainwindows *form  = new Thin_film_mainwindows();
    form->show();
}

void CSPBoard::yield_pcver_trigger(bool value)
{
    deshboardmain *from  = new deshboardmain();
    from->show();
}

void CSPBoard::yield_webver_trigger(bool value)
{
     QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/yield_desh/"));
}

void CSPBoard::oi_ver1_trigger(bool value)
{
    QMessageBox *box = new QMessageBox();
    box->setText(tr("use oi system 2"));
    box->exec();
    QString OI_program_path = QString("\"%1/operating_ratio_program/OIservice.exe\"").arg(qApp->applicationDirPath());
    qDebug()<<OI_program_path;
    QProcess OI_program;
    OI_program.startDetached(OI_program_path);
}

void CSPBoard::oi_ver2_trigger(bool value)
{
    QDesktopServices::openUrl(QUrl("http://10.20.10.101:8080/oirate/"));
}
